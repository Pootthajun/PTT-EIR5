﻿Imports System.IO
Public Class RenderImageFromPath
    Inherits System.Web.UI.Page


    Private ReadOnly Property RPT_Year As Integer
        Get
            Try
                Return Request.QueryString("RPT_Year")
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property RPT_No As Integer
        Get
            Try
                Return Request.QueryString("RPT_No")
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property DETAIL_ID As Integer
        Get
            Try
                Return Request.QueryString("DETAIL_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property IMG_ID As Integer
        Get
            Try
                Return Request.QueryString("IMG")
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property


    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim PathFile As String = BL.Picture_Path & "\" & RPT_Year & "\" + RPT_No + "\" + DETAIL_ID

        If IMG_ID <> 0 Then
            PathFile &= "_" & IMG_ID
        End If


        If PathFile = "" Then
            Response.Redirect("resources/images/Sample_40.png", True)
            Exit Sub
        End If
        Try
            Dim C As New Converter

            Dim F As FileStream = OpenFile(PathFile)
            Dim IMG As Byte() = C.StreamToByte(F)
            F.Close()
            Response.Clear()
            Response.BinaryWrite(IMG)
            Response.AddHeader("Content-Type", "image/png")

        Catch ex As Exception
            Response.Redirect("resources/images/Sample_40.png", True)
            Exit Sub
        End Try
    End Sub

End Class