﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Current_Status
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dashboard.BindDDlEquipment(ddl_Equipment, EIR_BL.Report_Type.All)
            BindData(ddl_Equipment.SelectedValue)
        End If
        lblUserFullname.Text = Session("USER_Full_Name")
    End Sub

    Private Sub BindData(ByVal Equipment As EIR_BL.Report_Type)

        Dim SQL As String = ""
        SQL &= "SELECT PLANT_ID,PLANT_Code,PLANT_Name," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 0 THEN 1 ELSE 0 END) Normal," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 1 THEN 1 ELSE 0 END) ClassC," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 2 THEN 1 ELSE 0 END) ClassB," & vbLf
        SQL &= " SUM(CASE CUR_LEVEL WHEN 3 THEN 1 ELSE 0 END) ClassA" & vbLf
        SQL &= " FROM" & vbLf
        SQL &= " (" & vbLf
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= "	 SELECT * FROM VW_DASHBOARD_CURRENT_ST " & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= "	 SELECT * FROM VW_DASHBOARD_CURRENT_RO " & vbNewLine
            Case EIR_BL.Report_Type.Lube_Oil_Report
                SQL &= "	 SELECT * FROM VW_DASHBOARD_CURRENT_LO " & vbNewLine
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                SQL &= "	 ------------------ PdMA ----------------------" & vbNewLine
                SQL &= "	 SELECT * FROM VW_DASHBOARD_CURRENT_PDMA " & vbNewLine
                SQL &= "	 UNION ALL " & vbNewLine
                SQL &= "	 ------------------ MTAP ----------------------" & vbNewLine
                SQL &= "	 Select * FROM VW_DASHBOARD_CURRENT_MTAP " & vbNewLine
            Case EIR_BL.Report_Type.Thermography_Report
                SQL &= "	 Select * FROM VW_DASHBOARD_CURRENT_THM " & vbNewLine
            Case EIR_BL.Report_Type.All
                '--------------------- ST -----------------------
                SQL &= "	 Select * FROM VW_DASHBOARD_CURRENT_STATUS " & vbNewLine
                '------------------------------------------------
        End Select
        SQL &= " )" & vbLf
        SQL &= " TAG" & vbLf
        SQL &= " GROUP BY PLANT_ID,PLANT_Code,PLANT_Name" & vbLf
        SQL &= " ORDER BY PLANT_ID" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Session("Dashboard_Current_Status") = DT


        rptChart.DataSource = DT
        rptChart.DataBind()

        rptData.DataSource = DT
        rptData.DataBind()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblNormal As Label = e.Item.FindControl("lblNormal")
        Dim lblClassC As Label = e.Item.FindControl("lblClassC")
        Dim lblClassB As Label = e.Item.FindControl("lblClassB")
        Dim lblClassA As Label = e.Item.FindControl("lblClassA")
        Dim lblTotal As Label = e.Item.FindControl("lblTotal")

        lblPlant.Text = e.Item.DataItem("PLANT_CODE")

        Dim Normal As Integer = 0
        Dim ClassC As Integer = 0
        Dim ClassB As Integer = 0
        Dim ClassA As Integer = 0
        If Not IsDBNull(e.Item.DataItem("Normal")) AndAlso e.Item.DataItem("Normal") <> 0 Then
            Normal = e.Item.DataItem("Normal")
            lblNormal.Text = FormatNumber(Normal, 0)
        Else
            lblNormal.Text = "-"
        End If
        If Not IsDBNull(e.Item.DataItem("ClassC")) AndAlso e.Item.DataItem("ClassC") <> 0 Then
            ClassC = e.Item.DataItem("ClassC")
            lblClassC.Text = FormatNumber(ClassC, 0)
        Else
            lblClassC.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassB")) AndAlso e.Item.DataItem("ClassB") <> 0 Then
            ClassB = e.Item.DataItem("ClassB")
            lblClassB.Text = FormatNumber(ClassB, 0)
        Else
            lblClassB.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassA")) AndAlso e.Item.DataItem("ClassA") <> 0 Then
            ClassA = e.Item.DataItem("ClassA")
            lblClassA.Text = FormatNumber(ClassA, 0)
        Else
            lblClassA.Text = "-"
        End If

        If Normal + ClassC + ClassB + ClassA <> 0 Then
            lblTotal.Text = FormatNumber(Normal + ClassC + ClassB + ClassA, 0)
        Else
            lblTotal.Text = "-"
        End If

        Select Case ddl_Equipment.SelectedValue
            Case EIR_BL.Report_Type.All
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_Plant.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_Plant_ST.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_Plant_RO.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
            Case EIR_BL.Report_Type.Lube_Oil_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_AllTag_LO.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_AllTag_PdMA.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
            Case EIR_BL.Report_Type.Thermography_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Current_Status_AllTag_THM.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID") & "';"
        End Select

    End Sub

    Protected Sub rptChart_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptChart.ItemDataBound

        '---------------- Legend Control ------------------
        If e.Item.ItemType = ListItemType.Footer Then
            Dim S As DataTable = rptChart.DataSource
            e.Item.Visible = Not IsNothing(S) AndAlso S.Rows.Count > 0
        End If

        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim ChartPlant As System.Web.UI.DataVisualization.Charting.Chart = e.Item.FindControl("ChartPlant")

        Dim ChartType As DataVisualization.Charting.SeriesChartType = ChartPlant.Series(0).ChartType
        ChartType = DataVisualization.Charting.SeriesChartType.Pie
        ChartPlant.Series(0).ChartType = ChartType

        Dim DT As DataTable = Session("Dashboard_Current_Status")
        Dim Normal As Double = e.Item.DataItem("Normal")
        Dim ClassC As Double = e.Item.DataItem("ClassC")
        Dim ClassB As Double = e.Item.DataItem("ClassB")
        Dim ClassA As Double = e.Item.DataItem("ClassA")

        Dim Url As String = ""

        Select Case ddl_Equipment.SelectedValue
            Case EIR_BL.Report_Type.All
                Url = "Dashboard_Current_Status_Plant.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                Url = "Dashboard_Current_Status_Plant_ST.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                Url = "Dashboard_Current_Status_Plant_RO.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
            Case EIR_BL.Report_Type.Lube_Oil_Report
                Url = "Dashboard_Current_Status_AllTag_LO.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                Url = "Dashboard_Current_Status_AllTag_PdMA.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
            Case EIR_BL.Report_Type.Thermography_Report
                Url = "Dashboard_Current_Status_AllTag_THM.aspx?PLANT_ID=" & e.Item.DataItem("PLANT_ID")
        End Select


        ChartPlant.Series("Series1").Url = Url

        Dim YValue As Double() = {Normal, ClassC, ClassB, ClassA}
        'Dim XValue As String() = {"Normal", "ClassC", "ClassB", "ClassA"}
        ChartPlant.Series("Series1").Points.DataBindY(YValue)
        ChartPlant.Series("Series1").Points(0).LegendText = "Normal"
        ChartPlant.Series("Series1").Points(1).LegendText = "ClassC"
        ChartPlant.Series("Series1").Points(2).LegendText = "ClassB"
        ChartPlant.Series("Series1").Points(3).LegendText = "ClassA"

        ChartPlant.Series("Series1").Points(0).ToolTip = "Normal : " & FormatNumber(Normal, 0) & " tag(s)"
        ChartPlant.Series("Series1").Points(1).ToolTip = "ClassC : " & FormatNumber(ClassC, 0) & " tag(s)"
        ChartPlant.Series("Series1").Points(2).ToolTip = "ClassB : " & FormatNumber(ClassB, 0) & " tag(s)"
        ChartPlant.Series("Series1").Points(3).ToolTip = "ClassA : " & FormatNumber(ClassA, 0) & " tag(s)"

        ChartPlant.Series("Series1").Points(0).Color = Drawing.Color.Green
        ChartPlant.Series("Series1").Points(1).Color = Drawing.Color.Yellow
        ChartPlant.Series("Series1").Points(2).Color = Drawing.Color.Orange
        ChartPlant.Series("Series1").Points(3).Color = Drawing.Color.Red

        ChartPlant.Titles(0).Text = e.Item.DataItem("PLANT_Code")
        ChartPlant.Titles(1).Text = "Total " & FormatNumber(Normal + ClassC + ClassB + ClassA, 0) & " tag(s)"
    End Sub

    Protected Sub ddl_Equipment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Equipment.SelectedIndexChanged
        BindData(ddl_Equipment.SelectedValue)
    End Sub
End Class