﻿Imports System.Data
Imports System.Data.SqlClient
Public Class LO_Routine_Plan
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim CV As New Converter

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Lube_Oil_Report

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetPlan(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindPlan()
        Dim SQL As String = "SELECT * FROM VW_REPORT_LO_HEADER " & vbLf

        Dim WHERE As String = ""
        If ddl_Search_Year.SelectedIndex > 0 Then
            WHERE &= " RPT_Year=" & ddl_Search_Year.Items(ddl_Search_Year.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Month.SelectedIndex > 0 Then
            WHERE &= " RPT_Month=" & ddl_Search_Month.Items(ddl_Search_Month.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_ReportType.SelectedIndex > 0 Then
            WHERE &= " RPT_Period_TYPE=" & ddl_Search_ReportType.Items(ddl_Search_ReportType.SelectedIndex).Value & " AND " & vbNewLine
        End If

        If WHERE <> "" Then
            SQL &= "WHERE " & WHERE.Substring(0, WHERE.Length - 6) & vbNewLine
        End If

        SQL &= " ORDER BY RPT_Year,RPT_Month" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try
        pnlBindingError.Visible = False

        Session("LO_Routine_Plan") = DT

        Navigation.SesssionSourceName = "LO_Routine_Plan"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlan
    End Sub

    Protected Sub rptPlan_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlan.ItemCommand
        Select Case e.CommandName
            Case "ToggleStatus"
                Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

                Dim RPT_Year As Integer = btnToggle.Attributes("RPT_Year")
                Dim RPT_No As Integer = btnToggle.Attributes("RPT_No")
                Try
                    BL.Drop_RPT_LO_Header(RPT_Year, RPT_No)
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindPlan()

                lblBindingSuccess.Text = "Delete plan successfully"
                pnlBindingSuccess.Visible = True
        End Select
    End Sub

    Protected Sub rptPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblYear As Label = e.Item.FindControl("lblYear")
        Dim lblMonth As Label = e.Item.FindControl("lblMonth")
        Dim lblTags As Label = e.Item.FindControl("lblTags")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblCode.Text = e.Item.DataItem("RPT_Code")
        lblYear.Text = e.Item.DataItem("RPT_Year")
        lblTags.Text = e.Item.DataItem("TOTAL_TAG")
        lblMonth.Text = CV.ToMonthNameEN(e.Item.DataItem("RPT_Month")).Substring(0, 3)
        '-------------------- Route----------------------
        lblTagType.Text = e.Item.DataItem("RPT_Period_Type_Name")

        lblStatus.Text = e.Item.DataItem("STEP_Name")
        lblStatus.ForeColor = BL.Get_Report_Step_Color(e.Item.DataItem("RPT_STEP"))
        btnToggle.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnToggle.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
    End Sub

    Protected Sub ResetPlan(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ClearPanelSearch()
        BindPlan()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListPlan.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False

        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")

        btnConstruct.Visible = USER_LEVEL = EIR_BL.User_Level.Administrator Or
                            USER_LEVEL = EIR_BL.User_Level.Inspector Or
                            USER_LEVEL = EIR_BL.User_Level.Approver


    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()

        Dim SQL As String = "SELECT ISNULL(MIN(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM RPT_LO_Header"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddl_Search_Year.Items.Clear()
        ddl_Search_Year.Items.Add("Choose a Year...")
        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddl_Search_Year.Items.Add(i)
        Next
        ddl_Search_Year.Text = Now.Year + 543

        '--------------- Bind Month--------------
        ddl_Search_Month.Items.Clear()
        ddl_Search_Month.Items.Add("Choose a Month...")
        For i As Integer = 1 To 12
            Dim Item As New ListItem(MonthEng(i), i)
            ddl_Search_Month.Items.Add(Item)
        Next
        ddl_Search_Month.SelectedIndex = 0

    End Sub

    Protected Sub ddl_Search_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Year.SelectedIndexChanged
        BindPlan()
    End Sub

    Protected Sub ddl_Search_Month_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Month.SelectedIndexChanged
        BindPlan()
    End Sub

    Protected Sub ddl_Search_ReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_ReportType.SelectedIndexChanged
        BindPlan()
    End Sub

#End Region

    Protected Sub btnConstruct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConstruct.Click

        ClearPanelEdit()
        '--------------- Bind Year ---------------
        ddl_Edit_Year.Items.Clear()
        Dim DefaultItem As New ListItem("---Select Year---", 0)
        ddl_Edit_Year.Items.Add(DefaultItem)
        For i As Integer = Now.Year - 1 To Now.Year + 1
            Dim Item As New ListItem(i + 543, i + 543)
            ddl_Edit_Year.Items.Add(Item)
        Next
        ddl_Edit_Year.SelectedIndex = 0

        pnlEdit.Visible = True
        btnConstruct.Visible = False

        '-----------------------------------
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ddl_Edit_Year.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Year"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim SQL As String = "SELECT * FROM RPT_LO_Header WHERE RPT_Year=" & ddl_Edit_Year.Items(ddl_Edit_Year.SelectedIndex).Value
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        Dim NeedToUpdate As Boolean = False
        For i As Integer = 1 To 12
            DT.DefaultView.RowFilter = "RPT_Month=" & i
            If DT.DefaultView.Count = 0 Then
                Dim DR As DataRow = DT.NewRow
                DR("RPT_Year") = ddl_Edit_Year.Items(ddl_Edit_Year.SelectedIndex).Value
                DR("RPT_No") = GetNewPlanNo(DR("RPT_Year"))
                DR("RPT_Month") = i
                Select Case i
                    Case 1, 4, 7, 10
                        DR("RPT_Period_Type") = EIR_BL.LO_Period_Type.Quaterly
                    Case Else
                        DR("RPT_Period_Type") = EIR_BL.LO_Period_Type.Monthy
                End Select
                Dim TheYear As Integer = ddl_Edit_Year.Items(ddl_Edit_Year.SelectedIndex).Value
                Dim StartDate As String = (TheYear - 543) & "-" & i.ToString.PadLeft(2, "0") & "-01"
                Dim EndDate As String = (TheYear - 543) & "-" & i.ToString.PadLeft(2, "0") & "-" & DateTime.DaysInMonth(TheYear, i).ToString.PadLeft(2, "0")
                'Dim StartDate As String = TheYear.ToString & "-" & i.ToString.PadLeft(2, "0") & "-01"
                'Dim EndDate As String = TheYear.ToString & "-" & i.ToString.PadLeft(2, "0") & "-" & DateTime.DaysInMonth(TheYear - 543, i).ToString.PadLeft(2, "0")
                DR("RPT_Period_Start") = Date.Parse(StartDate)
                DR("RPT_Period_End") = Date.Parse(EndDate)
                'DR("RPT_Subject") = xxxxxxxxxxxxxxx
                'DR("RPT_To") = xxxxxxxxxxxxxxx
                'DR("RPT_Cause") = xxxxxxxxxxxxxxx
                'DR("RPT_Result") = xxxxxxxxxxxxxxx
                'DR("RPT_By") = xxxxxxxxxxxxxxx
                'DR("RPT_Date") = xxxxxxxxxxxxxxx
                'DR("RPT_COL_By") = xxxxxxxxxxxxxxx
                'DR("RPT_COL_Date") = xxxxxxxxxxxxxxx
                'DR("RPT_COL_Comment") = xxxxxxxxxxxxxxx
                'DR("RPT_INSP_By") = xxxxxxxxxxxxxxx
                'DR("RPT_INSP_Date") = xxxxxxxxxxxxxxx
                'DR("RPT_INSP_Comment") = xxxxxxxxxxxxxxx
                'DR("RPT_ANL_By") = xxxxxxxxxxxxxxx
                'DR("RPT_ANL_Date") = xxxxxxxxxxxxxxx
                'DR("RPT_ANL_Comment") = xxxxxxxxxxxxxxx
                DR("RPT_STEP") = EIR_BL.Report_Step.New_Step
                'DR("RPT_LOCK_BY") = xxxxxxxxxxxxxxx
                'DR("Officer_Collector") = xxxxxxxxxxxxxxx
                'DR("Officer_Inspector") = xxxxxxxxxxxxxxx
                'DR("Officer_Engineer") = xxxxxxxxxxxxxxx
                'DR("Officer_Analyst") = xxxxxxxxxxxxxxx
                'DR("Created_Time") = Now
                'DR("Created_By") = Session("USER_ID")
                DR("Update_Time") = Now
                DR("Update_By") = Session("USER_ID")
                'DR("Result_FileName") = xxxxxxxxxxxxxxx
                DT.Rows.Add(DR)

                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT)
                DT.AcceptChanges()
                NeedToUpdate = True
            End If
        Next

        If NeedToUpdate Then
            ResetPlan(Nothing, Nothing)
            lblBindingSuccess.Text = "Construct successfully"
            pnlBindingSuccess.Visible = True
        Else
            lblBindingError.Text = "Plan for this year has already constructed"
            lblBindingError.Visible = True
        End If
    End Sub

    Private Function GetNewPlanNo(ByVal RPT_Year As Integer) As Integer
        Dim SQL As String = "SELECT IsNull(MIN(RPT_No),0)-1 FROM RPT_LO_Header" & vbNewLine
        SQL &= "  WHERE RPT_Year=" & RPT_Year
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Private Function GetNewDetailID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(DETAIL_ID),0)+1 FROM RPT_LO_Detail" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

End Class