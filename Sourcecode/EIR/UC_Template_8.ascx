﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Template_8.ascx.vb" Inherits="EIR.UC_Template_8" %>

<%@ Register Src="~/Summernote/UC_SummerNote.ascx" TagPrefix="uc1" TagName="UC_SummerNote" %>


<asp:Label ID="lblProperty" runat="server" Text="" Style="display: none;"></asp:Label>

<tr style="text-align: center;">
    <td style="vertical-align: top;width: 50%;">
        <table style="vertical-align: top; background-color: white;height :400px;">
            <tr style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td colspan="2" style="height: 300px; width: 50%;text-align: center;vertical-align: middle;">
                    <asp:ImageButton ImageUrl="RenderImage_ST_TA_Sector.aspx" ID="ImgPreview1"  runat="server" onError="this.src='resources/images/File_Sector.png'" AlternateText="..." Style="cursor: pointer; text-align: center; max-width: 100%; min-width: 50%; max-height: 300px;" />
                    <asp:Button ID="btnRefreshImage1" runat="server" Text="Update/Refresh" Style="display: none;" />
                 </td>
            </tr>
            <tr style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td style="border-style: solid; border-width: thin; border-color: #C0C0C0; vertical-align: top; ">Condition </td>
                <td>
                    <uc1:UC_SummerNote runat="server" ID="txtDetail1" />
                </td>
            </tr>

        </table>
    </td>
   <td style="vertical-align: top;width: 50%;">
        <table style="vertical-align: top;   background-color: white;height :400px;">
            <tr style="border-style: solid; border-width: thin; border-color: #C0C0C0">
               <td colspan="2" style="height: 300px; width: 50%;text-align: center;vertical-align: middle;">
                    <asp:ImageButton ImageUrl="RenderImage_ST_TA_Sector.aspx" ID="ImgPreview2" runat="server" onError="this.src='resources/images/File_Sector.png'" AlternateText="..." Style="cursor: pointer; text-align: center; max-width: 100%; min-width: 50%; max-height: 300px;" />
                    <asp:Button ID="btnRefreshImage2" runat="server" Text="Update/Refresh" Style="display: none;" />
                </td>
            </tr>
            <tr style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td style="vertical-align: top; "  ><span style="padding :1px;" >Condition</span> </td>
                <td>
                    <uc1:UC_SummerNote runat="server" ID="txtDetail2" />
                </td>
            </tr>
        </table>
    </td>
</tr>
