﻿Imports System.Data
Imports System.Data.SqlClient
Public Class LO_Oil_Type
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetOil(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindOil()

        Dim SQL As String = "SELECT LO_T.Oil_TYPE_ID,LO_T.Oil_TYPE_Name,Oil_Cat,LO_T.Active_Status," & vbLf
        SQL &= " LO_T.Update_By,LO_T.Update_Time,COUNT(TAG.Oil_TYPE_ID) TOTAL_TAG" & vbLf
        SQL &= " FROM MS_LO_Oil_Type LO_T" & vbLf
        SQL &= " LEFT JOIN MS_LO_TAG TAG ON LO_T.Oil_TYPE_ID=TAG.Oil_TYPE_ID" & vbLf
        SQL &= " GROUP BY LO_T.Oil_TYPE_ID,LO_T.Oil_TYPE_Name,Oil_Cat,LO_T.Active_Status,LO_T.Update_By,LO_T.Update_Time " & vbLf
        SQL &= " ORDER BY LO_T.Oil_TYPE_Name" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("Oil_TYPE") = DT

        Navigation.SesssionSourceName = "Oil_TYPE"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptOil
    End Sub

    Protected Sub rptType_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptOil.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim Oil_TYPE_ID As Integer = btnEdit.CommandArgument
        Select Case e.CommandName
            Case "Edit"
                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                txtTypeName.Focus()
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListOil.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT * FROM MS_LO_Oil_Type WHERE Oil_TYPE_ID=" & Oil_TYPE_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Oil-Type Not Found"
                    pnlBindingError.Visible = True
                    BindOil()
                    Exit Sub
                End If

                txtTypeName.Attributes("Oil_TYPE_ID") = DT.Rows(0).Item("Oil_TYPE_ID")
                txtTypeName.Text = DT.Rows(0).Item("Oil_TYPE_Name")
                If Not IsDBNull(DT.Rows(0).Item("Oil_Cat")) Then
                    ddlCat.SelectedIndex = DT.Rows(0).Item("Oil_Cat")
                End If
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")
                lblValidation.Text = ""

                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_LO_Oil_Type Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  Oil_TYPE_ID=" & Oil_TYPE_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindOil()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select


    End Sub

    Protected Sub rptOil_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptOil.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblName.Text = e.Item.DataItem("Oil_TYPE_Name")
        Select Case e.Item.DataItem("Oil_Cat").ToString
            Case "1"
                lblType.Text = "Mineral"
            Case "2"
                lblType.Text = "Synthetic"
        End Select
        lblTag.Text = e.Item.DataItem("TOTAL_TAG")
        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.CommandArgument = e.Item.DataItem("Oil_TYPE_ID")

    End Sub

    Protected Sub ResetOil(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindOil()
        '-----------------------------------
        ClearPanelEdit()
        '-----------------------------------
        pnlListOil.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtTypeName.Attributes("Oil_TYPE_ID") = "0"
        txtTypeName.Text = ""
        ddlCat.SelectedIndex = 0
        chkAvailable.Checked = True

        btnCreate.Visible = True
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True

        btnCreate.Visible = False

        txtTypeName.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListOil.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtTypeName.Text = "" Then
            lblValidation.Text = "Please insert Oil Name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddlCat.SelectedIndex < 1 Then
            lblValidation.Text = "Please select Oil Type"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim Oil_TYPE_ID As Integer = txtTypeName.Attributes("Oil_TYPE_ID")

        Dim SQL As String = "SELECT * FROM MS_LO_Oil_Type WHERE Oil_TYPE_Name='" & txtTypeName.Text.Replace("'", "''") & "' AND Oil_TYPE_ID<>" & Oil_TYPE_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Oil Name is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_LO_Oil_Type WHERE Oil_TYPE_ID=" & Oil_TYPE_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            Oil_TYPE_ID = GetNewTypeID()
            DR("Oil_TYPE_ID") = Oil_TYPE_ID
        Else
            DR = DT.Rows(0)
        End If

        DR("Oil_TYPE_Name") = txtTypeName.Text
        DR("Oil_Cat") = ddlCat.Items(ddlCat.SelectedIndex).Value
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetOil(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetNewTypeID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(Oil_TYPE_ID),0)+1 FROM MS_LO_Oil_Type "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

End Class