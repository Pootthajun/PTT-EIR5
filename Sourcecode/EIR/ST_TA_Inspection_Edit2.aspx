﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ST_TA_Inspection_Edit2.aspx.vb" Inherits="EIR.ST_TA_Inspection_Edit2" %>

<%@ Register Src="PageNavigation.ascx" TagName="PageNavigation" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="GL_TagInspection.ascx" TagName="GL_TagInspection" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- Page Head -->

    <asp:UpdatePanel ID="udp1" runat="server">
        <ContentTemplate>


            <h2>Create Turnaround & Shutdown Routine Report</h2>
            <asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" Text=""></asp:TextBox>
            <asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" Text=""></asp:TextBox>
            <asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" />
            <div class="clear"></div>
            <!-- End .clear -->


            <div class="content-box">
                <!-- Start Content Box -->

                <div class="content-box-header">

                    <h3>
                        <asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>


                    <ul class="content-box-tabs">

                        <li>
                            <asp:LinkButton ID="HTabHeader" runat="server">Report Header</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabDetail" runat="server" CssClass="default-tab current">Report Detail</asp:LinkButton></li>

                        <li>
                            <asp:LinkButton ID="HTabAsFound" runat="server">As Found </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabAfterClean" runat="server">After Clean </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabNDE" runat="server">NDE </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabRepair" runat="server">Repair </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabAfterRepair" runat="server">After Repair </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabFinal" runat="server">Final </asp:LinkButton></li>

                        <li>
                            <asp:LinkButton ID="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
                    </ul>

                    <div class="clear"></div>

                </div>
                <!-- End .content-box-header -->

                <div class="content-box-content">

                    <div class="tab-content current">
                        <fieldset>
                            <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->

                            <p style="font-weight: bold; font-size: 16px;">
                                <label class="column-left" style="width: 120px; font-size: 16px;">Report for: </label>
                                <asp:Label ID="lbl_Plant_Edit" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
                                > Year :
                                <asp:Label ID="lbl_Year_Edit" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>

                                >
                                    <asp:Label ID="lbl_Equipment_Edit" runat="server" Text="Equipment" CssClass="EditReportHeader"></asp:Label>


                            </p>



                            <h3>
                                <asp:Label ID="Label1" runat="server"></asp:Label></h3>



                            <div class="clear"></div>
                    </div>


                    <ul class="shortcut-buttons-set">
                        <li>
                            <asp:LinkButton ID="lnkAddInspection" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/image_add_48.png" alt="icon" /><br />
							            Add Inspection
							          </span>
                            </asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear all
							          </span>
                            </asp:LinkButton>
                            <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender"
                                runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all inspection for this report permanently?">
                            </cc1:ConfirmButtonExtender>
                        </li>
                        <li>
                            <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" /><br />
									    Reset this tab
									    </span>
                            </asp:LinkButton>
                        </li>

                        <li>
                            <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button" Visible ="false" >
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon"/><br />
									        Preview report
								        </span>
                            </asp:LinkButton>
                        </li>
                    </ul>

                    <table width="900" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <thead>
                            <tr>
                                <th colspan ="2" bgcolor="#EEEEEE" style="text-align: center;">Parts / Components</th>
                                <th bgcolor="#EEEEEE" style="text-align: center;">Condition</th>

                                <th bgcolor="#EEEEEE" style="text-align: center; width: 90px;">Date</th>
                                <%--<th bgcolor="#EEEEEE" style="text-align: center; width: 150px;">Tag No</th>
                                <th bgcolor="#EEEEEE" style="text-align: center; width: 200px;">Tag Name</th>--%>
                                <th bgcolor="#EEEEEE" style="text-align: center;">Conditions / Problems</th>
                                <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Time Start</th>
                                <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Time End</th>
                                <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Finished</th>
                                <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Action</th>

                            </tr>
                        </thead>
                        <asp:Repeater ID="rptComponents" runat="server">
                            <ItemTemplate>
                                <tbody>
                                    <tr id="trTag" runat="server" style="background-color:darkblue;">
                                        <td colspan="9">
                                            <asp:Label ID="lblGroupTag" runat="server" Style="font-weight: bold; color: white ;  "></asp:Label></td>
                                       
                                    </tr>

                                    <tr id="trStep" runat="server" style="background-color: lavender;">
                                        <td colspan="8">
                                            &nbsp&nbsp<asp:Label ID="lblStep" runat="server" Style="font-weight: bold; color: black;"></asp:Label></td>
                                        <td>
                                            <asp:ImageButton ID="btnAddInspection_In_Step" runat="server" CommandName="Add" ImageUrl="resources/images/icons/add_small.png" ToolTip="Add Daily Report For Step..." /></td>
                                    </tr>

                                    <tr>
                                        <td   runat="server"> <span >&nbsp&nbsp&nbsp&nbsp</span></td>
                                        <td id="td1" runat="server">
                                            <asp:Label ID="lblComponents" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                        <td id="td2" runat="server">
                                            <asp:Label ID="lblStatus" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>

                                        <td id="TD" runat="server">
                                            <asp:Label ID="lblRPT_Date" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                       <%-- <td>
                                            <asp:Label ID="lblTagNo" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                        <td>
                                            <asp:Label ID="lblTagName" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>--%>
                                        <td>
                                            <asp:Label ID="lblConditions" runat="server"  Style="margin-left :5px;"></asp:Label>

                                        </td>
                                        <td>
                                            <asp:Label ID="lblTimeStart" runat="server"></asp:Label>
                                            <asp:Label ID="lblActualStart" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTimeEnd" runat="server"></asp:Label>
                                            <asp:Label ID="lblActualEnd" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFinished" runat="server"></asp:Label>

                                        </td>
                                        <td style="text-align: left;">
                                            <asp:ImageButton ID="btnEdit" runat="server" CommandName="Action" ImageUrl="resources/images/icons/pencil.png" ToolTip="Upload/edit more detail" />
                                            <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" ImageUrl="resources/images/icons/cross.png" ToolTip="Clear detail for this inspection point" />
                                            <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this inspection permanently?" />
                                            <%-- <asp:Image ID="imgWarning" runat="server" style="cursor:help;" ImageUrl="resources/images/icons/alert.gif" ToolTip="Unable to autosave Please completed all require detail !!" />--%>
                                        </td>
                                </tbody>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>


                    <asp:Panel ID="dialogCreateReport" runat="server" Visible="False">


                        <div class="MaskDialog"></div>
                        <asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog_Picture" Style="width: 850px; margin-left: 100px; margin-top: 100px;">
                            <h3 style="width: 100%; text-align: left;">Create Daily Report for 
					                <asp:Label ID="lbl_Plant_dialog" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
                                > Year :
                                    <asp:Label ID="lbl_Year_dialog" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>

                                >
                                        <asp:Label ID="lbl_Equipment_dialog" runat="server" Text="Equipment" CssClass="EditReportHeader"></asp:Label>

                                <%--> Tag : 
                                        <asp:Label ID="lbl_Tag_Code_dialog" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>
                                        &nbsp;&nbsp;
                                        <asp:Label ID="lbl_Tag_Name_dialog" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>

                                        <asp:Button ID="btnAddTag" runat="server" Class="button" Text="Add Tag" />--%>


                            </h3>
                            <p></p>
                            <p style="font-weight: bold; font-size: 16px; margin-left: 20px;">
                                <label class="column-left" style="width: 200px; font-size: 16px;">Tag : </label>
                                <%--<asp:DropDownList CssClass="text-input small-input" ID="ddl_Tag" Width="250px"
                                            runat="server" Font-Bold="True">
                                        </asp:DropDownList>--%>
                                <asp:Label ID="lbl_Tag_Code_dialog" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>
                                &nbsp;&nbsp;
                                        <asp:Label ID="lbl_Tag_Name_dialog" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>

                                <asp:Button ID="btnAddTag" runat="server" Class="button" Text="Add Tag" />

                                <font color="red">**</font>
                            </p>
                            <p style="font-weight: bold; font-size: 16px; margin-left: 20px;">
                                <label class="column-left" style="width: 200px; font-size: 16px;">Report Date : </label>
                                <asp:TextBox runat="server" ID="txt_dialog_Date" Width="100px" CssClass="text-input small-input " PlaceHolder="..." Style="text-align: center;"></asp:TextBox>
                                <cc1:CalendarExtender ID="dialog_Date_Extender" runat="server"
                                    Format="yyyy-MM-dd" TargetControlID="txt_dialog_Date" PopupPosition="Right">
                                </cc1:CalendarExtender>
                                <font color="red">**</font>



                            </p>
                            <p style="font-weight: bold; font-size: 16px; margin-left: 20px;">
                                <label class="column-left" style="width: 200px; font-size: 16px;">Step : </label>
                                <asp:DropDownList CssClass="text-input small-input" ID="ddl_ST_TA_Step" Width="250px"
                                    runat="server" Font-Bold="True">
                                </asp:DropDownList>
                                <font color="red">**</font>
                            </p>

                            <p style="font-weight: bold; font-size: 16px; margin-left: 20px;">
                                <label class="column-left" style="width: 200px; font-size: 16px;">Inspection : </label>
                                <asp:Panel ID="pnl_Inspection" runat="server">
                                    <asp:Repeater ID="rpt_INSP" runat="Server">
                                        <ItemTemplate>
                                            <asp:Button ID="btnINSP" runat="server" BorderColor="#f4f4f4" BorderWidth="1px" Font-Size="10px"
                                                CommandName="Select" Style="cursor: pointer;" Text="INSP" Width="120px" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:Panel>
                                <font color="red">**</font>
                            </p>


                            <p style="font-weight: bold; font-size: 16px; margin-left: 20px;">
                                <label class="column-left" style="width: 200px; font-size: 16px;">Condition : </label>
                                <asp:Panel ID="pnl_Status" runat="server">
                                    <asp:Repeater ID="rpt_STATUS" runat="Server">
                                        <ItemTemplate>
                                            <asp:Button ID="btnStatus" runat="server" BorderColor="#f4f4f4" Font-Size="10px"
                                                BorderWidth="1px" CommandName="Select" Style="cursor: pointer;" Text="RES"
                                                Width="90px" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:Panel>
                                <font color="red">**</font>
                            </p>


                            <p style="font-weight: bold; font-size: 16px; margin-left: 20px;">
                                <label class="column-left" style="width: 200px; font-size: 16px;">Level : </label>
                                <asp:Panel ID="pnl_Level" runat="server">
                                    <asp:Repeater ID="rpt_LEVEL" runat="Server">
                                        <ItemTemplate>
                                            <asp:Button ID="btnLevel" runat="server" BorderColor="#f4f4f4" Font-Size="10px"
                                                BorderWidth="1px" CommandName="Select" Style="cursor: pointer;" Text="LEVEL"
                                                Width="80px" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </asp:Panel>
                                <font color="red">**</font>
                            </p>


                            <table cellpadding="0" cellspacing="0">
                                <tbody style="border-bottom: none;">
                                    <tr>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td style="border: none;">
                                            <asp:Panel ID="pnlValidation_dialog" runat="server" class="notification error png_bg">
                                                <asp:ImageButton ID="ImageButton5" runat="server" CssClass="close"
                                                    ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                                                <div>
                                                    <asp:Label ID="lblValidation_dialog" runat="server"></asp:Label>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                        <td style="text-align: right; border: none;">
                                            <asp:Button ID="btnClose_dialogCreateReport" runat="server" Class="button" Text="Close" />
                                            &nbsp;
                                <asp:Button ID="btnOK_dialogCreateReport" runat="server" Class="button" Text="Create Report" />

                                        </td>
                                    </tr>
                                </tbody>
                            </table>


                        </asp:Panel>

                    </asp:Panel>



                    <asp:Panel ID="dialogTag" runat="server" Visible="false">


                        <div class="MaskDialog"></div>
                        <asp:Panel ID="Panel2" runat="server" CssClass="Dialog_Picture" Style="width: 900px; margin-left: 100px; margin-top: 100px; height: 400px;">
                            <h3 style="width: 100%; text-align: left;">Select Tag Plant 
					             <asp:Label ID="lbl_Plant_dialog_Tag" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
                                >
                                    <asp:Label ID="lbl_Equipment_dialog_Tag" runat="server" Text="Equipment" CssClass="EditReportHeader"></asp:Label>


                            </h3>

                            <asp:Panel ID="pnl_dialog_tag" runat="server" Visible="false">
                                <table>
                                    <tr>
                                        <td>
                                            <p>
                                                <label class="column-left" style="width: 120px;">Define to Plant : </label>
                                                <asp:DropDownList CssClass="select"
                                                    ID="ddl_Edit_Plant" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </p>

                                        </td>
                                        <td>
                                            <p>
                                                <label class="column-left" style="width: 120px;">Define to Type : </label>
                                                <asp:DropDownList CssClass="select"
                                                    ID="ddl_Edit_Tag_Type" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>

                                            </p>

                                        </td>
                                    </tr>

                                </table>
                            </asp:Panel>


                            <p>
                                <table style="border: none;">
                                    <tbody>
                                        <tr>
                                            <%-- <td style="width: 80px; vertical-align: top;">
                                                        <label class="column-left" style="width: 100px;">Tag :</label></td>--%>
                                            <td style="vertical-align: top;">
                                                <%--SELECT TAG--%>
                                                <div class="content-box">
                                                    <!-- Start Content Box -->
                                                    <!-- End .content-box-header -->

                                                    <div class="content-box-content">
                                                        <asp:Panel ID="Panel1" runat="server">
                                                            <table>
                                                                <thead>
                                                                    <tr>
                                                                        <th><a href="#">Code </a></th>
                                                                        <th><a href="#">Tag Name </a></th>
                                                                        <%--<th><a href="#">Equipement-Type</a></th>--%>
                                                                        <th><a href="#">Status</a></th>
                                                                        <th><a href="#">Select</a></th>
                                                                    </tr>
                                                                </thead>

                                                                <asp:Repeater ID="rptTag" runat="server">
                                                                    <HeaderTemplate>

                                                                        <tbody>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>

                                                                            <td style="width: 100px;"><a href="javascript:;" id="ContentPlaceHolder1_rptTag_lblTagNo_0" style="font-weight: bold; color: gray;" onclick="document.getElementById(&#39;ContentPlaceHolder1_rptInspection_GLT_1_btnExpand_1&#39;).click();">
                                                                                <asp:Label ID="lblTagNo" runat="server"></asp:Label></a></td>
                                                                            <td>
                                                                                <asp:Label ID="lblTagName" runat="server"></asp:Label>
                                                                                <asp:Label ID="lblTo_Table" runat="server" Visible="false"></asp:Label>
                                                                            </td>
                                                                            <%--<td>
                                                                                        <asp:Label ID="lblTagType" runat="server"></asp:Label></td>--%>
                                                                            <td>
                                                                                <asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                                                            <td>
                                                                                <!-- Icons -->
                                                                                <asp:ImageButton ID="btnSelect" CommandName="Select" runat="server" ImageUrl="resources/images/icons/pencil.png" />

                                                                            </td>
                                                                        </tr>


                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </tbody>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                                <tfoot>
                                                                    <tr>
                                                                        <td colspan="6">
                                                                            <div class="bulk-actions align-left">
                                                                                <%--<asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>--%>
                                                                            </div>
                                                                            <uc2:PageNavigation ID="Navigation" runat="server" />
                                                                            <!-- End .pagination -->
                                                                            <div class="clear"></div>
                                                                        </td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>

                                                            <div class="clear"></div>



                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </p>


                            <table cellpadding="0" cellspacing="0">
                                <tbody style="border-bottom: none;">

                                    <tr>
                                        <td style="border: none;">
                                            <%--<asp:Panel ID="Panel6" runat="server" class="notification error png_bg">
                                                        <asp:ImageButton ID="ImageButton1" runat="server" CssClass="close"
                                                            ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                                                        <div>
                                                            <asp:Label ID="Label6" runat="server"></asp:Label>
                                                        </div>
                                                    </asp:Panel>--%>
                                        </td>
                                        <td style="text-align: right; border: none;">
                                            <asp:Button ID="btnClose_dialogTag" runat="server" Class="button" Text="Close" />
                                            <%--&nbsp;
                                <asp:Button ID="Button2" runat="server" Class="button" Text="Create Report" />--%>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>


                        </asp:Panel>

                    </asp:Panel>




                    <br>
                    <p align="right">
                        <asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
                        <asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" Visible ="false"  />
                    </p>



                    </fieldset>
                </div>
                <!-- End #tabDetail -->

                <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close"
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>


            </div>
            <!-- End .content-box-content -->

            </div>
            <!-- End .content-box -->


        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
