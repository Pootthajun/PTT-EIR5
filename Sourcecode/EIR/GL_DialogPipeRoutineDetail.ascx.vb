﻿Imports System.Data
Imports System.Data.SqlClient
Public Class GL_DialogPipeRoutineDetail
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE

    Public Event UpdateCompleted(ByRef sender As GL_DialogPipeRoutineDetail)

#Region "Dynamic Property"

    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return Me.Attributes("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            If value <> UNIQUE_POPUP_ID Then
                MY_PREVIEW1 = Nothing
                MY_PREVIEW2 = Nothing
            End If
            Me.Attributes("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Public Property RPT_Year() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_Year")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_Year")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_Year") = value
        End Set
    End Property

    Public Property RPT_No() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_No")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_No")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_No") = value
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If ddl_Tag.SelectedIndex > -1 Then
                Return ddl_Tag.Items(ddl_Tag.SelectedIndex).Value
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            BindTag()
            For i As Integer = 0 To ddl_Tag.Items.Count - 1
                If value = ddl_Tag.Items(i).Value Then
                    ddl_Tag.SelectedIndex = i
                End If
            Next
        End Set
    End Property

    Public Property PARTNO() As String
        Get
            Return txt_Part.Text
        End Get
        Set(ByVal value As String)
            BindPartNo()
            For i As Integer = 0 To ddl_Part.Items.Count - 1
                If ddl_Part.Items(i).Value = value Then
                    ddl_Part.SelectedIndex = i
                End If
            Next
            If ddl_Part.SelectedIndex = ddl_Part.Items.Count - 1 Then
                txt_Part.Text = value
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "focus", "document.getElementById('" & txt_Part.ClientID & "').select();", True)
            End If
        End Set
    End Property

    Public Property INSP_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_INSP.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnINSP As Button = item.FindControl("btnINSP")
                If btnINSP.Font.Underline Then
                    Return btnINSP.CommandArgument
                End If
            Next
            Return 0
        End Get
        Set(ByVal value As Integer)
            'FROM TAG
            BindInspection()
            For Each item As RepeaterItem In rpt_INSP.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnINSP As Button = item.FindControl("btnINSP")
                If btnINSP.CommandArgument = value Then
                    btnINSP.Font.Underline = True
                Else
                    btnINSP.Font.Underline = False
                End If
            Next
            STATUS_ID = 0
            PARTNO = ""

            ''-------------------Check Require Picture --------------
            'Dim RequiredPicture As Boolean = BL.IsInspectionRequirePicture(value)
            'TDImage.Visible = RequiredPicture
        End Set
    End Property

    Public Property STATUS_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_STATUS.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnStatus As Button = item.FindControl("btnStatus")
                If btnStatus.Font.Underline Then
                    Return btnStatus.CommandArgument
                End If
            Next
            Return 0
        End Get
        Set(ByVal value As Integer)
            BindStatus()
            For Each item As RepeaterItem In rpt_STATUS.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnStatus As Button = item.FindControl("btnStatus")
                If btnStatus.CommandArgument = value Then
                    btnStatus.Font.Underline = True
                Else
                    btnStatus.Font.Underline = False
                End If
            Next
            LEVEL_ID = LEVEL_ID
        End Set
    End Property

    Public Property LEVEL_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_LEVEL.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnLevel As Button = item.FindControl("btnLevel")
                If btnLevel.Font.Underline Then
                    Return btnLevel.CommandArgument
                End If
            Next
            Return -1
        End Get
        Set(ByVal value As Integer)
            BindLevel()
            For Each item As RepeaterItem In rpt_LEVEL.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnLevel As Button = item.FindControl("btnLevel")
                If btnLevel.CommandArgument = value Then
                    btnLevel.Font.Underline = True
                Else
                    btnLevel.Font.Underline = False
                End If
            Next
            '--------Update Color---------
            UpdateSelectedColor()
        End Set
    End Property

    Private Sub UpdateSelectedColor()

        Dim CSS As String = BL.Get_Inspection_Css_Box_By_Level(LEVEL_ID)

        '--------------Inspection---------------
        For Each item As RepeaterItem In rpt_INSP.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnINSP As Button = item.FindControl("btnINSP")
            If btnINSP.Font.Underline Then
                btnINSP.CssClass = CSS
            Else
                btnINSP.CssClass = "LevelDeselect"
            End If
        Next
        '--------------Status---------------
        For Each item As RepeaterItem In rpt_STATUS.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnStatus As Button = item.FindControl("btnStatus")
            If btnStatus.Font.Underline Then
                btnStatus.CssClass = CSS
            Else
                btnStatus.CssClass = "LevelDeselect"
            End If
        Next
        '---------------Level---------------
        For Each item As RepeaterItem In rpt_LEVEL.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnLevel As Button = item.FindControl("btnLevel")
            If btnLevel.Font.Underline Then
                btnLevel.CssClass = CSS
            Else
                btnLevel.CssClass = "LevelDeselect"
            End If
        Next
    End Sub

    Public Property DETAIL_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("DETAIL_ID")) Then
                Return 0
            Else
                Return Me.Attributes("DETAIL_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("DETAIL_ID") = value
        End Set
    End Property

    Public Property LAST_DETAIL_ID() As Integer
        Get
            If ddl_Last_Detail.SelectedIndex > -1 Then
                Return ddl_Last_Detail.Items(ddl_Last_Detail.SelectedIndex).Value
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            BindLastDetail()
            For i As Integer = 0 To ddl_Last_Detail.Items.Count - 1
                If value = ddl_Last_Detail.Items(i).Value Then
                    ddl_Last_Detail.SelectedIndex = i
                    Exit For
                End If
            Next
            ddl_Last_Detail_SelectedIndexChanged(ddl_Last_Detail, Nothing)
        End Set
    End Property

    Public Property INSP_Detail() As String
        Get
            Return txt_Detail.Text
        End Get
        Set(ByVal value As String)
            txt_Detail.Text = value
        End Set
    End Property

    Public Property INSP_Recomment() As String
        Get
            Return txt_Recomment.Text
        End Get
        Set(ByVal value As String)
            txt_Recomment.Text = value
        End Set
    End Property

    Public Property Fixed() As TriState
        Get
            Select Case True
                Case Equals(btnSolveYes.BackColor, Drawing.Color.Green)
                    Return TriState.True
                Case Equals(btnSolveNo.BackColor, Drawing.Color.OrangeRed)
                    Return TriState.False
                Case Else
                    Return TriState.UseDefault
            End Select
        End Get
        Set(ByVal value As TriState)
            Select Case value
                Case TriState.UseDefault
                    btnSolveYes.BackColor = Drawing.Color.White
                    btnSolveNo.BackColor = Drawing.Color.White
                    btnSolveYes.ForeColor = Drawing.Color.Black
                Case TriState.True
                    btnSolveYes.BackColor = Drawing.Color.Green
                    btnSolveYes.ForeColor = Drawing.Color.White
                    btnSolveNo.BackColor = Drawing.Color.White
                Case TriState.False
                    btnSolveYes.BackColor = Drawing.Color.White
                    btnSolveYes.ForeColor = Drawing.Color.Black
                    btnSolveNo.BackColor = Drawing.Color.OrangeRed
            End Select
        End Set
    End Property

    Public Property MY_PREVIEW1() As Byte()
        Get
            If BL.IsInspectionRequirePicture(INSP_ID) Then
                Return Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1")
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Byte())
            Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1") = value
            ImgPreview1.ImageUrl = "RenderPipeRoutineImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=1&"
        End Set
    End Property

    Public Property MY_PREVIEW2() As Byte()
        Get
            If BL.IsInspectionRequirePicture(INSP_ID) Then
                Return Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2")
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Byte())
            Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2") = value
            ImgPreview2.ImageUrl = "RenderPipeRoutineImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=2&"
        End Set
    End Property

    Protected Sub btnRefreshImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshImage.Click
        If Not IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_1")) Then
            MY_PREVIEW1 = Session("TempImage_" & UNIQUE_POPUP_ID & "_1")
            Session("TempImage_" & UNIQUE_POPUP_ID & "_1") = Nothing
        End If

        If Not IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_2")) Then
            MY_PREVIEW2 = Session("TempImage_" & UNIQUE_POPUP_ID & "_2")
            Session("TempImage_" & UNIQUE_POPUP_ID & "_2") = Nothing
        End If

        ImgPreview1.ImageUrl = "RenderPipeRoutineImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=1&"
        ImgPreview2.ImageUrl = "RenderPipeRoutineImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=2&"
    End Sub

    Public Property Disabled() As Boolean
        Get
            Return Not btnUpdate.Visible
        End Get
        Set(ByVal value As Boolean)
            btnRefreshImage.Visible = Not value
            ddl_Tag.Enabled = Not value
            pnl_Level.Enabled = Not value
            pnl_Inspection.Enabled = Not value
            ddl_Part.Enabled = Not value
            txt_Part.Enabled = Not value
            ddl_Last_Detail.Enabled = Not value
            pnl_Status.Enabled = Not value

            If Not value Then
                ImgPreview1.Attributes("OnClick") = "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "',1,document.getElementById('" & btnRefreshImage.ClientID & "'));"
                ImgPreview2.Attributes("OnClick") = "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "',2,document.getElementById('" & btnRefreshImage.ClientID & "'));"
                ImgPreview1.ToolTip = "Click to edit picture"
                ImgPreview2.ToolTip = "Click to edit picture"
            Else
                ImgPreview1.Attributes("OnClick") = "window.open(this.src);"
                ImgPreview2.Attributes("OnClick") = "window.open(this.src);"
                ImgPreview1.ToolTip = "Click to view picture"
                ImgPreview2.ToolTip = "Click to view picture"
            End If

            txt_Detail.ReadOnly = value
            txt_Recomment.ReadOnly = value
            btnUpdate.Visible = Not value
        End Set
    End Property
#End Region

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        MY_PREVIEW1 = Nothing
        MY_PREVIEW2 = Nothing
        CloseDialog()
    End Sub

    Public Sub CloseDialog()

        Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1") = Nothing
        Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2") = Nothing

        Disabled = False
        Me.Visible = False
    End Sub

    Public Sub ShowDialog(ByVal Init_RPT_Year As Integer,
                          ByVal Init_RPT_No As Integer,
                          Optional ByVal Init_TAG_ID As Integer = 0,
                          Optional ByVal Init_INSP_ID As Integer = 0)

        RPT_Year = Init_RPT_Year
        RPT_No = Init_RPT_No
        TAG_ID = Init_TAG_ID

        INSP_ID = Init_INSP_ID

        LEVEL_ID = -1
        STATUS_ID = 0

        INSP_Detail = ""
        INSP_Recomment = ""
        Fixed = TriState.UseDefault ' And Hidden/ Cloak

        DETAIL_ID = 0

        Disabled = False
        Me.Visible = True

    End Sub

    Public Sub ShowDialog(ByVal DETAIL_ID As Integer) ' For Case Edit

        Me.DETAIL_ID = 0

        Dim SQL As String = "SELECT * FROM RPT_PIPE_Routine_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to collect information for this inspection point');", True)
            CloseDialog()
            RaiseEvent UpdateCompleted(Me)
            Exit Sub
        End If

        Me.DETAIL_ID = DT.Rows(0).Item("DETAIL_ID")
        RPT_Year = DT.Rows(0).Item("RPT_Year")
        RPT_No = DT.Rows(0).Item("RPT_No")
        TAG_ID = DT.Rows(0).Item("TAG_ID")
        INSP_ID = DT.Rows(0).Item("INSP_ID")
        If Not IsDBNull(DT.Rows(0).Item("STATUS_ID")) Then
            STATUS_ID = DT.Rows(0).Item("STATUS_ID")
        End If
        If Not IsDBNull(DT.Rows(0).Item("ICLS_ID")) Then
            LEVEL_ID = DT.Rows(0).Item("ICLS_ID")
        End If
        If Not IsDBNull(DT.Rows(0).Item("COMP_NO")) Then
            PARTNO = DT.Rows(0).Item("COMP_NO")
        End If
        If Not IsDBNull(DT.Rows(0).Item("LAST_DETAIL_ID")) Then
            LAST_DETAIL_ID = DT.Rows(0).Item("LAST_DETAIL_ID")
        End If
        If Not IsDBNull(DT.Rows(0).Item("PROB_Detail")) Then
            INSP_Detail = DT.Rows(0).Item("PROB_Detail")
        End If
        If Not IsDBNull(DT.Rows(0).Item("PROB_Recomment")) Then
            INSP_Recomment = DT.Rows(0).Item("PROB_Recomment")
        End If
        txtUpdateBy.Text = DT.Rows(0).Item("Update_Name").ToString
        If Not IsDBNull(DT.Rows(0).Item("Fixed")) Then
            If DT.Rows(0).Item("Fixed") Then
                Fixed = TriState.True
            Else
                Fixed = TriState.False
            End If
        Else
            Fixed = TriState.UseDefault
        End If

        Disabled = False
        Me.Visible = True

        If DT.Rows(0).Item("Create_Flag") = "Main" Then DisableMainInformation()

    End Sub

    Private Sub DisableMainInformation()
        ddl_Tag.Enabled = False
        ddl_Last_Detail.Enabled = False
        pnl_Inspection.Enabled = False
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlValidation.Visible = False
    End Sub

    Private Sub BindTag()
        Dim SQL As String = "SELECT TAG_ID,TAG_CODE " & vbNewLine
        SQL &= " FROM VW_PIPE_TAG TAG" & vbNewLine
        SQL &= " WHERE TAG.ROUTE_ID=(SELECT ROUTE_ID FROM RPT_PIPE_Routine_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & ")" & vbNewLine
        SQL &= " ORDER BY TAG_CODE" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl_Tag.Items.Clear()
        ddl_Tag.Items.Add(New ListItem("", 0))
        ddl_Tag.SelectedIndex = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TAG_Code"), DT.Rows(i).Item("TAG_ID"))
            ddl_Tag.Items.Add(Item)
        Next
    End Sub

    Private Sub BindPartNo()
        Dim SQL As String = "SELECT DISTINCT ISNULL(COMP_NO,'') COMP_NO FROM RPT_PIPE_Routine_Detail" & vbNewLine
        SQL &= " WHERE TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID & " AND ISNULL(COMP_NO,'')<>''" & vbNewLine
        SQL &= " ORDER BY ISNULL(COMP_NO,'')" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl_Part.Items.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("COMP_NO"))
        Next
        ddl_Part.Items.Add(New ListItem("Insert new part...", ""))
        ddl_Part.SelectedIndex = ddl_Part.Items.Count - 1
    End Sub

    Private Sub BindInspection()
        Dim SQL As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbNewLine
        SQL &= " DECLARE @RPT_No As INT =" & RPT_No & vbNewLine
        SQL &= " DECLARE @TAG_ID As INT =" & TAG_ID & vbNewLine

        SQL &= " SELECT DISTINCT * FROM " & vbNewLine
        SQL &= " (" & vbNewLine
        SQL &= "    SELECT DISTINCT INSP.INSP_ID,DNSP.INSP_Name" & vbNewLine
        SQL &= "    FROM MS_PIPE_TAG " & vbNewLine
        SQL &= "    LEFT JOIN MS_ST_TAG_Inspection INSP ON INSP.TAG_TYPE_ID=8 --Hardcode For Pipe" & vbNewLine
        SQL &= "    LEFT JOIN MS_ST_Default_Inspection DNSP ON INSP.INSP_ID=DNSP.INSP_ID" & vbNewLine
        SQL &= "    WHERE TAG_ID = @TAG_ID " & vbNewLine
        SQL &= "    AND (" & vbNewLine
        SQL &= "    INSP.REF_INSP_ID IS NULL OR NOT EXISTS" & vbNewLine
        SQL &= "    (SELECT DETAIL_ID FROM RPT_PIPE_Routine_Detail WHERE RPT_Year=@RPT_Year AND RPT_No=@RPT_No AND TAG_ID=@TAG_ID" & vbNewLine
        SQL &= "    AND INSP_ID=INSP.REF_INSP_ID AND ISNULL(STATUS_ID,0)<>INSP.REF_STATUS_ID)" & vbNewLine
        SQL &= "    )" & vbNewLine
        SQL &= "    UNION ALL " & vbNewLine
        SQL &= "    ("
        SQL &= "    SELECT DISTINCT VW.INSP_ID,VW.INSP_Name" & vbNewLine
        SQL &= "    FROM VW_PIPE_ROUTINE_DETAIL VW" & vbNewLine
        SQL &= "    WHERE CURRENT_LEVEL IS NOT NULL AND RPT_Year=@RPT_Year AND RPT_No=@RPT_No AND TAG_ID=@TAG_ID" & vbNewLine
        SQL &= "    )" & vbNewLine
        SQL &= " ) INSP" & vbNewLine
        SQL &= "    ORDER BY INSP_ID" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rpt_INSP.DataSource = DT
        rpt_INSP.DataBind()
    End Sub

    Private Sub BindStatus()
        Dim SQL As String = "DECLARE @TAG_ID As INT =" & TAG_ID & vbNewLine
        SQL &= " DECLARE @INSP_ID As INT =" & INSP_ID & vbNewLine
        SQL &= " SELECT DISTINCT IST.STATUS_ID,IST.STATUS_Name, IST.STATUS_Order" & vbNewLine
        SQL &= " FROM MS_PIPE_TAG " & vbNewLine
        SQL &= " LEFT JOIN MS_ST_TAG_Inspection INSP ON INSP.TAG_TYPE_ID=" & EIR_PIPE.TAG_TYPE_PIPE & " --Hardcode For Pipe" & vbNewLine
        SQL &= " LEFT JOIN MS_ST_Default_Inspection_Status IST ON INSP.STATUS_ID=IST.STATUS_ID" & vbNewLine
        SQL &= " WHERE TAG_ID = @TAG_ID AND INSP_ID=@INSP_ID" & vbNewLine
        SQL &= " ORDER BY IST.STATUS_Order" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rpt_STATUS.DataSource = DT
        rpt_STATUS.DataBind()
    End Sub

    Private Sub BindLevel()

        Dim SQL As String = "SELECT * FROM ISPT_Class"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rpt_LEVEL.DataSource = DT
        rpt_LEVEL.DataBind()

    End Sub

    Private Sub BindLastDetail()
        Dim SQL As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbNewLine
        SQL &= " DECLARE @RPT_No As INT =" & RPT_No & vbNewLine
        SQL &= " DECLARE @TAG_ID As INT =" & TAG_ID & vbNewLine
        SQL &= " DECLARE @INSP_ID As INT =" & INSP_ID & vbNewLine
        SQL &= " DECLARE @PARTNO As varchar(100)='" & Replace(PARTNO, "'", "''") & "'" & vbNewLine

        SQL &= " SELECT DETAIL_ID,RPT_CODE,INSP_Name,ISNULL(CURRENT_STATUS_Name,'') STATUS_Name,CURRENT_LEVEL " & vbNewLine
        SQL &= " FROM VW_PIPE_ROUTINE_DETAIL VW" & vbNewLine

        SQL &= " WHERE dbo.UDF_RPT_Code(RPT_Year,RPT_No)=" & vbNewLine
        SQL &= " (	SELECT MAX(dbo.UDF_RPT_Code(RPT_Year,RPT_No)) " & vbNewLine
        SQL &= " FROM VW_PIPE_ROUTINE_DETAIL " & vbNewLine
        SQL &= " WHERE TAG_ID=@TAG_ID AND dbo.UDF_RPT_Code(RPT_Year,RPT_No)<dbo.UDF_RPT_Code(@RPT_Year,@RPT_No)" & vbNewLine
        SQL &= " ) AND INSP_ID=@INSP_ID AND TAG_ID=@TAG_ID " & vbNewLine
        SQL &= " ORDER BY STATUS_Name" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl_Last_Detail.Items.Clear()
        ddl_Last_Detail.Items.Add(New ListItem("", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Txt As String = DT.Rows(i).Item("INSP_Name") & " " & DT.Rows(i).Item("STATUS_Name")
            If Not IsDBNull(DT.Rows(i).Item("CURRENT_LEVEL")) Then
                Select Case CInt(DT.Rows(i).Item("CURRENT_LEVEL"))
                    Case 0
                        Txt &= IIf(INSP_ID = 12, " ZoneA", " Normal")
                    Case 1
                        Txt &= IIf(INSP_ID = 12, " ZoneB", " ClassC")
                    Case 2
                        Txt &= IIf(INSP_ID = 12, " ZoneC", " ClassB")
                    Case 3
                        Txt &= IIf(INSP_ID = 12, " ZoneD", " ClassA")
                End Select
            End If
            Dim Item As New ListItem(Txt, DT.Rows(i).Item("DETAIL_ID"))
            ddl_Last_Detail.Items.Add(Item)
        Next
    End Sub

    Protected Sub ddl_Part_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Part.SelectedIndexChanged
        If ddl_Part.SelectedIndex = ddl_Part.Items.Count - 1 Then
            txt_Part.Focus()
            txt_Part.Visible = True
        Else
            txt_Part.Text = ddl_Part.Items(ddl_Part.SelectedIndex).Text
            txt_Part.Visible = False
        End If
        '------------- Get Old-Last Detail Reference-----------
        LAST_DETAIL_ID = LAST_DETAIL_ID
    End Sub

    Protected Sub ddl_Last_Detail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Last_Detail.SelectedIndexChanged
        '---------------------------- New Code -------------------------
        MY_PREVIEW1 = PIPE.Get_ROUTINE_Image(RPT_Year, RPT_No, DETAIL_ID, 1)
        MY_PREVIEW2 = PIPE.Get_ROUTINE_Image(RPT_Year, RPT_No, DETAIL_ID, 2)

        pnlSolved.Visible = ddl_Last_Detail.SelectedIndex > 0

    End Sub

    Protected Sub rpt_INSP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_INSP.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnINSP As Button = e.Item.FindControl("btnINSP")
        btnINSP.Text = e.Item.DataItem("INSP_NAME")
        btnINSP.CssClass = "LevelDeselect"
        btnINSP.CommandArgument = e.Item.DataItem("INSP_ID")
    End Sub

    Protected Sub rpt_INSP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_INSP.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnINSP As Button = e.Item.FindControl("btnINSP")
        If INSP_ID <> btnINSP.CommandArgument Then INSP_ID = btnINSP.CommandArgument
    End Sub

    Protected Sub rpt_STATUS_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_STATUS.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnStatus As Button = e.Item.FindControl("btnStatus")
        btnStatus.Text = e.Item.DataItem("STATUS_Name")
        btnStatus.CssClass = "LevelDeselect"
        btnStatus.CommandArgument = e.Item.DataItem("STATUS_ID")

    End Sub

    Protected Sub rpt_STATUS_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_STATUS.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnStatus As Button = e.Item.FindControl("btnStatus")
        If STATUS_ID <> btnStatus.CommandArgument Then STATUS_ID = btnStatus.CommandArgument
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub rpt_LEVEL_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_LEVEL.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnLevel As Button = e.Item.FindControl("btnLevel")
        btnLevel.CommandArgument = e.Item.DataItem("ICLS_LEVEL")
        btnLevel.Text = e.Item.DataItem("ICLS_Description")
    End Sub

    Protected Sub rpt_LEVEL_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_LEVEL.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnLevel As Button = e.Item.FindControl("btnLevel")
        If LEVEL_ID <> btnLevel.CommandArgument Then LEVEL_ID = btnLevel.CommandArgument
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If TAG_ID < 1 Then
            lblValidation.Text = "Select Tag "
            pnlValidation.Visible = True
            Exit Sub
        End If

        If INSP_ID < 1 Then
            lblValidation.Text = "Select Inspection Point"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If pnlSolved.Visible AndAlso Fixed = TriState.UseDefault Then
            lblValidation.Text = "Select Solved Status"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If STATUS_ID < 1 Then
            lblValidation.Text = "Select Status"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If LEVEL_ID < 0 Then
            lblValidation.Text = "Select level"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If INSP_Detail = "" Then
            lblValidation.Text = "Select insert inspection detail"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If IsNothing(MY_PREVIEW1) OrElse MY_PREVIEW1.Length < 50 OrElse IsNothing(MY_PREVIEW2) OrElse MY_PREVIEW2.Length < 50 Then
            lblValidation.Text = "Select image"
            pnlValidation.Visible = True
            Exit Sub
        End If

        '-------------- เชครูปซ้ำ -------------------
        If LAST_DETAIL_ID > 0 Then
            Try
                Dim IMG1 As Byte() = PIPE.Get_ROUTINE_Image(LAST_DETAIL_ID, 1)
                Dim IMG2 As Byte() = PIPE.Get_ROUTINE_Image(LAST_DETAIL_ID, 2)
                If (Equals(IMG1, MY_PREVIEW1) And Equals(IMG2, MY_PREVIEW2)) Or (IMG1.Length = MY_PREVIEW1.Length And IMG2.Length = MY_PREVIEW2.Length) Then
                    Err.Raise(1, , "Select new images to update information")
                End If
            Catch ex As Exception
                lblValidation.Text = "Invalid parameter"
                pnlValidation.Visible = True
                Exit Sub
            End Try
        End If

        Dim DR As DataRow
        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable
        '----------------------------Check Update Reference---------------------
        If LAST_DETAIL_ID <> 0 Then ' ------------ Reference Old Problem --------
            SQL = "SELECT * FROM RPT_PIPE_Routine_Detail " & vbNewLine
            SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & vbNewLine
            SQL &= " AND LAST_DETAIL_ID=" & LAST_DETAIL_ID & vbNewLine
        ElseIf DETAIL_ID <> 0 Then  ' ------------ Reference Old Info --------
            SQL = "SELECT * FROM RPT_PIPE_Routine_Detail WHERE DETAIL_ID=" & DETAIL_ID & vbNewLine
        Else ' Detail_ID=0 ' ------------ New Info --------
            SQL = "SELECT * FROM RPT_PIPE_Routine_Detail " & vbNewLine
            SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID & " AND ICLS_ID IS NULL" & vbNewLine
            '--------- Add Fix Isuue Replacement Problem-----------
            SQL &= " AND ISNULL(LAST_DETAIL_ID,0)=0"
        End If

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR("Detail_ID") = PIPE.Get_New_Routine_DetailID()
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("TAG_ID") = TAG_ID
            DR("Create_Flag") = "New"
        Else
            DR = DT.Rows(0)
        End If

        '-----------------------------Update Detail------------------------
        DR("INSP_ID") = INSP_ID
        DR("STATUS_ID") = STATUS_ID
        DR("ICLS_ID") = LEVEL_ID

        If pnlSolved.Visible Then
            Select Case Fixed
                Case TriState.True
                    DR("Fixed") = True
                Case TriState.False
                    DR("Fixed") = False
                Case Else
                    DR("Fixed") = DBNull.Value
            End Select
        Else
            DR("Fixed") = DBNull.Value
        End If

        DR("PROB_Detail") = INSP_Detail
        DR("PROB_Recomment") = INSP_Recomment
        DR("COMP_NO") = PARTNO
        DR("LAST_DETAIL_ID") = LAST_DETAIL_ID
        DR("Update_By") = Session("USER_ID")
        DR("Update_Name") = Session("USER_Full_Name")
        DR("Update_Time") = Now
        '-------- Save Picture ---------
        If Not IsNothing(MY_PREVIEW1) Then
            If PIPE.Save_ROUTINE_Image(MY_PREVIEW1, RPT_Year, RPT_No, DR("DETAIL_ID"), 1) Then
                DR("Pic_Detail1") = True
            Else
                DR("Pic_Detail1") = False
                lblValidation.Text = "Unable to save left file"
                pnlValidation.Visible = True
                Exit Sub
            End If
        Else
            PIPE.Save_ROUTINE_Image(Nothing, RPT_Year, RPT_No, DR("DETAIL_ID"), 1)
            DR("Pic_Detail1") = False
        End If
        If Not IsNothing(MY_PREVIEW2) Then
            If PIPE.Save_ROUTINE_Image(MY_PREVIEW2, RPT_Year, RPT_No, DR("DETAIL_ID"), 2) Then
                DR("Pic_Detail2") = True
            Else
                DR("Pic_Detail2") = False
                lblValidation.Text = "Unable to save right file"
                pnlValidation.Visible = True
                Exit Sub
            End If
        Else
            PIPE.Save_ROUTINE_Image(Nothing, RPT_Year, RPT_No, DR("DETAIL_ID"), 2) '---------Force Delete ---------
            DR("Pic_Detail2") = False
        End If

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        DETAIL_ID = DR("Detail_ID")

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try
        '---------------------Additional Delete Default TAG Inspection ------------
        CloseDialog()
        RaiseEvent UpdateCompleted(Me)

    End Sub

    Protected Sub btnSolveYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSolveYes.Click
        If Fixed = TriState.True Then
            Fixed = TriState.UseDefault
        Else
            Fixed = TriState.True
        End If
    End Sub

    Protected Sub btnSolveNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSolveNo.Click
        If Fixed = TriState.False Then
            Fixed = TriState.UseDefault
        Else
            Fixed = TriState.False
        End If
    End Sub

End Class