﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="PIPE_Material_Code.aspx.vb" Inherits="EIR.PIPE_Material_Code" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>

	<!-- Page Head -->
			<h2>Pipe Material Code Setting </h2>
			
			
		<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
               <asp:Panel ID="pnlSearch" runat="server" CssClass="content-box-header" DefaultButton="btnSearch">
                <h3>Search Code Material</h3>    		            
                  
                   <asp:TextBox runat="server" ID="txt_Search_Name" AutoPostBack="True" style="position:relative; top:5px; left: 0px;"  PlaceHolder="Search Code/Name"
                        CssClass="text-input medium-input " Width="200px" MaxLength="50"></asp:TextBox>                                

                   <asp:Button ID="btnSearch" runat="server" Text="Search" Style="display:none;" />
                </asp:Panel>
             
              <div class="content-box-content">
                <div class="tab-content default-tab">
                  <!-- This is the target div. id must match the href of this div's tab -->
                  
                  <asp:Panel ID="pnlListService" runat="server">

                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">No.</a></th>
                        <th><a href="#">Material Code</a></th>
                        <th><a href="#">Description</a></th>
                        <th><a href="#">Total tag(s)</a></th>
                        <th><a href="#">Updated</a> </th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>
                   
                    <asp:Repeater ID="rptMaterial" runat="server">
                           <HeaderTemplate>                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                                  <tr>
                                    <td><asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblCode" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblName" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPipe" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblUpdateTime" runat="server"></asp:Label></td>
                                    <td align="left"><!-- Icons -->
                                        <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" ToolTip="View/Edit" />
                                        <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/del.png" ToolTip="Delete this material" />
                                        <cc1:ConfirmButtonExtender ID="cfbDelete" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this material permanently?" />
                                        </a>   
                                    </td>
                                  </tr>
                     
                             </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                    
                     <tfoot>
                      <tr>
                        <td colspan="7">
                            <div class="bulk-actions align-left">                             
                                <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                            </div>
                                <uc1:PageNavigation ID="Navigation" runat="server" />
                              <!-- End .pagination -->
                              <div class="clear"></div>      
                        </td>
                      </tr>
                    </tfoot>
                    
                  </table>
				  
				     <div class="clear"></div>
				  </asp:Panel>
				  
			    <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
                  
                  <asp:Panel ID="pnlEdit" runat="server">
                  
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Material Code Info</h3>
                    <div class="clear"></div>
                  </div>

                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                    <p>&nbsp;</p>
                    <p>
                        <label class="column-left" style="width:160px;" >Material Code  : </label>
                        <asp:TextBox runat="server" ID="txtCode" CssClass="text-input medium-input " MaxLength="5" style="position:relative; top:-5px;"></asp:TextBox>                                               
                    </p>       
                    <p>
                        <label class="column-left" style="width:160px;" >Description  : </label>
                        <asp:TextBox runat="server" ID="txtName" CssClass="text-input medium-input" Width="500px" MaxLength="200" style="position:relative; top:-5px;"></asp:TextBox>                                                      
                    </p>  
                    <asp:Panel ID="pnlValidation" runat="server" CssClass="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>
                    <p align="right">
                      <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                      <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                    </p>
                    </fieldset>

                  </asp:Panel>
                </div>
                <!-- End #tab1 -->
								
			  </div> <!-- End .content-box-content -->
		 </div>
	
		  
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
