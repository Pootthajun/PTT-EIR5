﻿Public Class UC_DocumentTreeNode
    Inherits System.Web.UI.UserControl

    Dim urlExpand As String = "resources/images/tree_expanded.png"
    Dim urlCollapsed As String = "resources/images/tree_collapsed.png"
    Dim urlNoChild As String = "resources/images/tree_nochild.png"

    Public Property NodeID As Integer
        Get
            Return lblID.Text
        End Get
        Set(value As Integer)
            lblID.Text = value
        End Set
    End Property

    Public Property ParentID As Integer
        Get
            Return lblID.Attributes("ParentID")
        End Get
        Set(value As Integer)
            lblID.Attributes("ParentID") = value
        End Set
    End Property

    Public ReadOnly Property ParentNode As UC_DocumentTreeNode
        Get
            Dim TreeList As UC_DocumentTreeList = Me.Parent.Parent.Parent
            Dim DT As DataTable = TreeList.CurrrentDocumentsData
            DT.DefaultView.RowFilter = "NodeID=" & Me.ParentID
            If DT.DefaultView.Count = 0 Then Return Nothing
            Dim rpt As Repeater = Me.Parent.Parent
            Dim Item As RepeaterItem = rpt.Items(DT.Rows.IndexOf(DT.DefaultView(0).Row))
            Return Item.FindControl("DocItem")
        End Get
    End Property

    Public Property NodeName As String
        Get
            Return lblName.Text
        End Get
        Set(value As String)
            lblName.Text = value
        End Set
    End Property
    Public Property NodeStatus As String
        Get
            Return lblNodeStatus.Text
        End Get
        Set(value As String)
            lblNodeStatus.Text = value
        End Set
    End Property

    Public Property Level As Integer
        Get
            Return tdIndent.Attributes("Level")
        End Get
        Set(value As Integer)
            tdIndent.Attributes("Level") = value
            tdIndent.Style("padding-left") = (value * IndextLevelPixel) & "px"
        End Set
    End Property

    Public Property IndextLevelPixel As Integer
        Get
            Return tdIndent.Attributes("IndextLevelPixel")
        End Get
        Set(value As Integer)
            tdIndent.Attributes("IndextLevelPixel") = value
        End Set
    End Property

    Public Property NoticeDate As Date
        Get
            If Not IsNothing(lblNoticeDate.Attributes("OADate")) AndAlso lblNoticeDate.Attributes("OADate") <> "" AndAlso lblNoticeDate.Attributes("OADate") <> 0 Then
                Return DateTime.FromOADate(CDbl(lblNoticeDate.Attributes("OADate")))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Date)
            If IsNothing(value) OrElse value.ToOADate = 0 Then
                lblNoticeDate.Text = ""
                lblNoticeDate.Attributes.Remove("OADate")
            Else
                Dim C As New Converter
                lblNoticeDate.Text = value.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("en-US")) 'C.DateToString(value, "dd MMM yyyy")
                lblNoticeDate.Attributes("OADate") = value.ToOADate
            End If
        End Set
    End Property

    Public Property FirstUploadDate As Date
        Get
            If Not IsNothing(lblCriticalDate.Attributes("OADate")) AndAlso lblCriticalDate.Attributes("OADate") <> "" AndAlso lblCriticalDate.Attributes("OADate") <> 0 Then
                Return DateTime.FromOADate(CDbl(lblCriticalDate.Attributes("OADate")))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Date)
            If IsNothing(value) OrElse value.ToOADate = 0 Then
                lblCriticalDate.Text = ""
                lblCriticalDate.Attributes.Remove("OADate")
            Else
                Dim C As New Converter
                lblCriticalDate.Text = value.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("en-US")) 'C.DateToString(value, "dd MMM yyyy")
                lblCriticalDate.Attributes("OADate") = value.ToOADate
            End If
        End Set
    End Property

    Public Property LastUpdate As Date
        Get
            If Not IsNothing(lblUpdate.Attributes("OADate")) AndAlso lblUpdate.Attributes("OADate") <> "" AndAlso lblUpdate.Attributes("OADate") <> 0 Then
                Return DateTime.FromOADate(CDbl(lblUpdate.Attributes("OADate")))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Date)
            If IsNothing(value) OrElse value.ToOADate = 0 Then
                lblUpdate.Text = ""
                lblUpdate.Attributes.Remove("OADate")
            Else
                Dim C As New Converter
                lblUpdate.Text = value.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("en-US")) 'C.DateToString(value, "dd MMM yyyy")
                lblUpdate.Attributes("OADate") = value.ToOADate
            End If
        End Set
    End Property

    Public Property IconURL As String
        Get
            Return imgIcon.ImageUrl
        End Get
        Set(value As String)
            imgIcon.ImageUrl = value
        End Set
    End Property

    Public ReadOnly Property Childs As List(Of UC_DocumentTreeNode)
        Get
            Return GetChildsNextLevel(Me)
        End Get
    End Property

    Public ReadOnly Property ChildsData As DataTable
        Get
            Dim TreeList As UC_DocumentTreeList = Me.Parent.Parent.Parent
            Dim DT As DataTable = TreeList.CurrrentDocumentsData
            DT.DefaultView.RowFilter = "ParentID=" & NodeID
            DT = DT.DefaultView.ToTable
            DT.DefaultView.RowFilter = ""
            Return DT
        End Get
    End Property

    Public ReadOnly Property ChildsAll As List(Of UC_DocumentTreeNode)
        Get
            Dim CH As New List(Of UC_DocumentTreeNode)
            For Each Node In Me.Childs
                GetAllChilds(Node, CH)
            Next
            Return CH
        End Get
    End Property

    Public ReadOnly Property ChildsDataAll As DataTable
        Get
            Dim TreeList As UC_DocumentTreeList = Me.Parent.Parent.Parent
            Dim DT As DataTable = TreeList.CurrrentDocumentsData
            Dim Result As DataTable = DT.Copy
            Result.Rows.Clear()

            DT.DefaultView.RowFilter = "NodeID=" & NodeID
            Dim StartIndex As Integer = DT.Rows.IndexOf(DT.DefaultView(0).Row) + 1
            Dim EndIndex As Integer = TreeList.GetNewChildPosition(NodeID) - 1
            For i As Integer = StartIndex To EndIndex
                Dim DR As DataRow = Result.NewRow
                DR.ItemArray = DT.Rows(i).ItemArray
                Result.Rows.Add(DR)
            Next
            Return Result
        End Get
    End Property

    Public ReadOnly Property IsExpanded As Boolean
        Get
            Return tdIndent.Attributes("Mode") = "Expanded" And Childs.Count > 0
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub PushObject(ByRef Arr As UC_DocumentTreeNode(), ByVal NextObj As UC_DocumentTreeNode)
        Array.Resize(Arr, Arr.Length + 1)
        Arr(Arr.Length - 1) = NextObj
    End Sub

    Public Sub Expand()
        Dim CH As List(Of UC_DocumentTreeNode) = Childs
        If CH.Count = 0 Then
            UpdateMode(ModeState.NoChild)
            Exit Sub
        End If
        For i As Integer = 0 To CH.Count - 1
            CH(i).SetExpandNodeNextLevel()
        Next
        UpdateMode(ModeState.Expanded) '----------- แยกกัน 2 ส่วน เพื่อให้เปิดปิดแค่ตัว Node เอง ไม่ได้ปิดลูกทั้งหมด ---------
    End Sub

    Public Sub SetExpandNodeNextLevel()
        Me.Visible = True
        Dim CH As List(Of UC_DocumentTreeNode) = Childs
        If CH.Count = 0 Then
            UpdateMode(ModeState.NoChild)
        ElseIf IsExpanded Then
            For i As Integer = 0 To CH.Count - 1
                CH(i).SetExpandNodeNextLevel()
            Next
        End If
    End Sub

    Public Sub Collapse()
        Dim CH As List(Of UC_DocumentTreeNode) = ChildsAll
        If CH.Count = 0 Then
            UpdateMode(ModeState.NoChild)
            Exit Sub
        End If
        For i As Integer = 0 To CH.Count - 1
            CH(i).Visible = False
        Next
        UpdateMode(ModeState.Collapsed)
    End Sub

    Public Enum ModeState
        NoChild = 0
        Expanded = 1
        Collapsed = 2
    End Enum

    Public Sub UpdateMode(ByVal Mode As ModeState)
        UpdateModeFlag(Mode)
        UpdateModeIcon(Mode)
    End Sub

    Public Sub UpdateModeFlag(ByVal Mode As ModeState)
        Select Case Mode
            Case ModeState.NoChild
                tdIndent.Attributes("Mode") = "NoChild"
            Case ModeState.Expanded
                tdIndent.Attributes("Mode") = "Expanded"
            Case ModeState.Collapsed
                tdIndent.Attributes("Mode") = "Collapsed"
        End Select
    End Sub

    Public Sub UpdateModeIcon(ByVal Mode As ModeState)
        Select Case Mode
            Case ModeState.NoChild
                imgMode.ImageUrl = urlNoChild
                imgMode.Style("cursor") = "default"
                imgMode.Attributes.Remove("onclick")
            Case ModeState.Expanded
                imgMode.ImageUrl = urlExpand
                imgMode.Style("cursor") = "pointer"
                imgMode.Attributes("onclick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
            Case ModeState.Collapsed
                imgMode.ImageUrl = urlCollapsed
                imgMode.Style("cursor") = "pointer"
                imgMode.Attributes("onclick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        End Select
    End Sub

    Private Sub btnExpand_Click(sender As Object, e As EventArgs) Handles btnExpand.Click
        If IsExpanded Then
            Collapse()
        Else
            Expand()
        End If
    End Sub

    Private Function GetChildsNextLevel(ByRef Node As UC_DocumentTreeNode) As List(Of UC_DocumentTreeNode)
        Dim _ch As New List(Of UC_DocumentTreeNode)
        Dim rpt As Repeater = Me.Parent.Parent
        For Each Item As RepeaterItem In rpt.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim _node As UC_DocumentTreeNode = Item.FindControl("DocItem")
            If _node.ParentID = NodeID And _node.Level = Level + 1 Then
                _ch.Add(_node)
            End If
        Next
        Return _ch
    End Function

    Private Sub GetAllChilds(ByRef Node As UC_DocumentTreeNode, ByRef StoredList As List(Of UC_DocumentTreeNode))
        StoredList.Add(Node)
        For Each _n In Node.Childs
            GetAllChilds(_n, StoredList)
        Next
    End Sub

End Class