﻿Public Class Dashboard_Improvement_Report_Plant_Export
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Month_F As Integer = Request.QueryString("Month_F")
            Dim Month_T As Integer = Request.QueryString("Month_T")
            Dim Year_F As Integer = Request.QueryString("Year_F")
            Dim Year_T As Integer = Request.QueryString("Year_T")
            Dim Equipment As Integer = Request.QueryString("Equipment")
            Dim Plant_ID As Integer = Request.QueryString("Plant_ID")
            UC_Dashboard_Improvement_Report_Plant.BindData(Plant_ID, Month_F, Month_T, Year_F, Year_T, Equipment)
        End If
    End Sub

End Class