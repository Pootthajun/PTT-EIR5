﻿
Imports System.IO
Public Class GL_Report
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        ''---------------- Save User Stat ---------------------
        'If Not IsNothing(Session("USER_ID")) Then
        '    BL.SaveLog(Session("USER_ID"), Session.SessionID, GetCurrentPageName, GetIPAddress)
        'End If

        If IsNothing(Request.QueryString("RPT_Year")) Or IsNothing(Request.QueryString("RPT_No")) Then
            Response.Redirect("Login.aspx", True)
        End If

        Dim RC As New ReportClass
        Dim OutputFile As String = ""
        If IsNumeric(Request.QueryString("Detail_ID")) Then
            OutputFile = RC.GetReport(Request.QueryString("RPT_Year"), Request.QueryString("RPT_No"), Request.QueryString("Detail_ID"))
        Else
            OutputFile = RC.GetReport(Request.QueryString("RPT_Year"), Request.QueryString("RPT_No"))
        End If

        If OutputFile = "" Then
            Dim ReportName As String = "INSP-E-" & Right(Request.QueryString("RPT_Year"), 2) & "-" & Request.QueryString("RPT_No").PadLeft(4, "0")
            Response.Write("Unable to genetate Report " & ReportName)
            Exit Sub
        End If

        Dim C As New Converter

        '------------------ Change Reading File Method -----------
        Dim FS As FileStream = File.Open(OutputFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
        Dim b As Byte() = C.StreamToByte(FS)
        FS.Close()
        FS.Dispose()

        Dim FN As String() = Split(OutputFile, "_")
        If FN.Length = 3 Then
            OutputFile = FN(1) & "_" & FN(2)
        End If

        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Disposition", "attachment; filename=" & OriginalFileName(OutputFile) & ".PDF")
        Response.BinaryWrite(b)
        Response.End()

    End Sub

End Class