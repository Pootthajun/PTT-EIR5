﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Stationary_OffRoutine_Edit2
    
    '''<summary>
    '''udp1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents udp1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''txt_Buffer_RPT_Year control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Buffer_RPT_Year As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_Buffer_RPT_No control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Buffer_RPT_No As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btn_Buffer_Refresh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_Buffer_Refresh As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''lblReportCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReportCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''HTabHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HTabHeader As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''HTabDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HTabDetail As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''HTabSummary control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HTabSummary As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lbl_Plant control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_Plant As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl_Route control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_Route As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl_Year control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_Year As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lbl_TAG control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_TAG As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlList As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lnkUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkUpload As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkClear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkClear As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lnkClear_ConfirmButtonExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkClear_ConfirmButtonExtender As Global.AjaxControlToolkit.ConfirmButtonExtender
    
    '''<summary>
    '''lnkPreview control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkPreview As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''pnlRemain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlRemain As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblTotalOld control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalOld As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblTotalUpdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalUpdate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''btnClearOld control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClearOld As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''confirmClearRemain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents confirmClearRemain As Global.AjaxControlToolkit.ConfirmButtonExtender
    
    '''<summary>
    '''rptRemain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptRemain As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''pnlNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlNew As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblTotalNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalNew As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''btnAddNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddNew As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''btnClearNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClearNew As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''confirmClearNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents confirmClearNew As Global.AjaxControlToolkit.ConfirmButtonExtender
    
    '''<summary>
    '''rptNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptNew As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''btn_Back control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_Back As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btn_Next control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_Next As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlEdit As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''UC_Detail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UC_Detail As Global.EIR.UC_ST_Detail
    
    '''<summary>
    '''lblTotalDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalDoc As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDoc As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''rptDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptDoc As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''lnkAddSection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkAddSection As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''TemplateSelector control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TemplateSelector As Global.EIR.UC_Select_Template
    
    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlValidation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlValidation As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''btnValidationClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnValidationClose As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''lblValidation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblValidation As Global.System.Web.UI.WebControls.Label
End Class
