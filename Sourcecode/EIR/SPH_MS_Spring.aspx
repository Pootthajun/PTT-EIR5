﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="SPH_MS_Spring.aspx.vb" Inherits="EIR.SPH_MS_Spring" %>


<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>

	<!-- Page Head -->
			<h2>Spring Hanger Setting </h2>
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
               <div class="content-box-header">
                <h3>Display condition </h3>
									
				    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Route" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    &nbsp;&nbsp;
                    <asp:Label ID="lbl1" runat="server" style="position:relative; top:5px;" Text="Spring No."></asp:Label>
    		            <asp:TextBox runat="server" ID="txt_Search_Spring" AutoPostBack="True" style="position:relative; top:5px; left: 0px;" 
                        CssClass="text-input small-input " Width="100px" MaxLength="50"></asp:TextBox>
                        &nbsp;&nbsp;
                     <asp:Label ID="Label1" runat="server" style="position:relative; top:5px;" Text="Pipe No."></asp:Label>
    		            <asp:TextBox runat="server" ID="txt_Search_Pipe" AutoPostBack="True" style="position:relative; top:5px; left: 0px;" 
                        CssClass="text-input small-input " Width="100px" MaxLength="50"></asp:TextBox>
                                
                     <div class="clear"></div>
                  </div>
              </div>

             
              <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                  <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <asp:Panel ID="pnlListSpringHanger" runat="server">
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">Plant</a></th>
                        <th><a href="#">Route</a></th> 
                        <th><a href="#">Spring No.</a></th>
                        <th><a href="#">Pipe No</a></th>                                               
                        <th><a href="#">Size</a></th>
                        <th><a href="#">Install-Oper.</a></th>
                        <th><a href="#">Cold-Hot</a></th>
                        <th><a href="#">Status</a></th>
                        <th><a href="#">Updated</a> </th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>
                   
                    <asp:Repeater ID="rptSpring" runat="server">
                           <HeaderTemplate>                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                                  <tr>
                                    <td><asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblRoute" runat="server"></asp:Label></td>   
                                    <td><asp:Label ID="lblSpringNo" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPipe" runat="server"></asp:Label></td>    
                                    <td align="center"><asp:Label ID="lblSize" runat="server"></asp:Label></td>
                                    <td align="center"><asp:Label ID="lblLoad" runat="server"></asp:Label></td>
                                    <td align="center"><asp:Label ID="lblLength" runat="server"></asp:Label></td>                                    
                                    <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblUpdateTime" runat="server"></asp:Label></td>
                                    <td><!-- Icons -->
                                        <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                        <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                        </a>   
                                    </td>
                                  </tr>
                     
                             </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                    
                     <tfoot>
                      <tr>
                        <td colspan="7">
                            <div class="bulk-actions align-left">                             
                                <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                            </div>
                                <uc1:PageNavigation ID="Navigation" runat="server" />
                              <!-- End .pagination -->
                              <div class="clear"></div>      
                        </td>
                      </tr>
                    </tfoot>
                    
                  </table>
				  
				     <div class="clear"></div>
				  </asp:Panel>
				  
			    <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
                  
                  <asp:Panel ID="pnlEdit" runat="server">
                  
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Spring Hanger </h3>
                    <div class="clear"></div>
                  </div>

                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                    <p>&nbsp;</p>
                      <p>
                        <label class="column-left" style="width:160px;" >Spring No : </label>
                        <asp:TextBox runat="server" ID="txtSpringNo" CssClass="text-input small-input " MaxLength="20"></asp:TextBox>                                               
                    </p>
                     <p>
                         <label class="column-left" style="width:160px;" >Pipe No : </label>
                         <asp:TextBox runat="server" ID="txtPipeNo" CssClass="text-input small-input " MaxLength="50"></asp:TextBox>  
                     </p>
                     <p>
                      <label class="column-left" style="width:160px;" >Plant : </label>
                         <asp:DropDownList CssClass="select" style="position:relative; top:5px;" ID="ddl_Edit_Plant" runat="server" AutoPostBack="True">
                        </asp:DropDownList>                    
                    </p>
                     <p>
                      <label class="column-left" style="width:160px;" >Route : </label>
                         <asp:DropDownList CssClass="select" style="position:relative; top:5px;" ID="ddl_Edit_Route" runat="server">
                        </asp:DropDownList>                    
                    </p>
                   <p>
				      <label class="column-left" style="width:160px;" >Size : </label>
					  <asp:TextBox runat="server" ID="txtInch" CssClass="text-input small-input " MaxLength="50"  Width="65px"></asp:TextBox> inch
					</p>
					<p>
				      <label class="column-left" style="width:160px;" >Load Install-Operation : </label>
					  <asp:TextBox runat="server" ID="txtLoadInstall" CssClass="text-input small-input " MaxLength="10" Width="65px"></asp:TextBox>	
					  -
					  <asp:TextBox runat="server" ID="txtLoadOperation" CssClass="text-input small-input " MaxLength="10" Width="65px"></asp:TextBox> kg.
					</p>
					<p>
				      <label class="column-left" style="width:160px;" >Length Cold-Hot : </label>
					  <asp:TextBox runat="server" ID="txtCold" CssClass="text-input small-input " MaxLength="10" Width="65px"></asp:TextBox>
					  -
					  <asp:TextBox runat="server" ID="txtHot" CssClass="text-input small-input " MaxLength="10" Width="65px"></asp:TextBox> mm.
					</p>
				    <p>
                      <label class="column-left" style="width:160px;" >Available : </label>
                        &nbsp;<asp:CheckBox ID="chkAvailable" runat="server" Text="" />
                    </p>
				    <p>
				      <label style="width:300px;" ></label>
				      <!-- End .clear -->
                    </p>
                    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>
                    <p align="right">
                      <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                      <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                    </p>
                    </fieldset></asp:Panel>
                </div>
                <!-- End #tab1 -->
								
			  </div> <!-- End .content-box-content -->
				
	
		  
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>