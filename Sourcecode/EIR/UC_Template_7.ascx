﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Template_7.ascx.vb" Inherits="EIR.UC_Template_7" %>

<%@ Register Src="~/Summernote/UC_SummerNote.ascx" TagPrefix="uc1" TagName="UC_SummerNote" %>


<asp:Label ID="lblProperty" runat="server" Text="" Style="display: none;"></asp:Label>

<tr  style="text-align: center; ">
    <td  style ="vertical-align :top;width: 50%;">
        <table style="background-color :white ;" >
            <tr  style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td colspan ="2" style =" height: 700px; width: 50%;text-align: center;vertical-align: middle;" >
                    <asp:ImageButton ImageUrl="RenderImage_ST_TA_Sector.aspx" ID="ImgPreview1" runat="server" onError="this.src='resources/images/File_Sector.png'" AlternateText="..." Style="cursor: pointer;text-align :center;max-width : 100%;min-width :50% ; max-height: 700px" />
                    <asp:Button ID="btnRefreshImage1" runat="server" Text="Update/Refresh" Style="visibility: hidden; width: 0px;" />
                </td>
            </tr>
            <tr  style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td style="border-style: solid; border-width: thin; border-color: #C0C0C0; vertical-align :top ; " class="auto-style1">Condition </td>
                <td colspan="1" style="border-top-color: #C0C0C0; border-left-color: #C0C0C0; border-width: thin; border-top-style: solid; border-bottom-style: solid; border-left-style: solid; border-bottom-color: #C0C0C0; background-color: #f9fefe; Width:100%;">
                    <uc1:UC_SummerNote runat="server" ID="txtDetail1" />
                </td>
            </tr>
        </table>
    </td>
    <td  style ="vertical-align :top ">
        <table style ="vertical-align :top;background-color :white ;" >
            <tr  style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td  colspan ="2"  style =" height: 300px;text-align: center;vertical-align: middle;"  >
                    <asp:ImageButton ImageUrl="RenderImage_ST_TA_Sector.aspx" ID="ImgPreview2" runat="server"  onError="this.src='resources/images/File_Sector.png'" AlternateText="..." Style="cursor: pointer;text-align :center;max-width : 100%;min-width :50% ;max-height : 300px; " />
                    <asp:Button ID="btnRefreshImage2" runat="server" Text="Update/Refresh" Style="display:none;" />
                </td>
            </tr>
            <tr  style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td style="border-style: solid; border-width: thin; border-color: #C0C0C0; vertical-align :top ; " class="auto-style1">Condition </td>
             <td colspan="1" style="border-top-color: #C0C0C0; border-left-color: #C0C0C0; border-width: thin; border-top-style: solid; border-bottom-style: solid; border-left-style: solid; border-bottom-color: #C0C0C0; background-color: #f9fefe; Width:100%;">
                    <uc1:UC_SummerNote runat="server" ID="txtDetail2" />
                </td></tr>

            </table>
            <table style ="vertical-align :top ;margin-top :20px ;background-color :white ; " >
            <tr   style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td  colspan ="2"  style =" height: 300px;text-align: center;vertical-align: middle;"  >
                    <asp:ImageButton ImageUrl="RenderImage_ST_TA_Sector.aspx" ID="ImgPreview3" runat="server"  onError="this.src='resources/images/File_Sector.png'" AlternateText="..." Style="cursor: pointer;text-align :center;max-width : 100%;min-width :50% ;max-height : 300px; " />
                    <asp:Button ID="btnRefreshImage3" runat="server" Text="Update/Refresh" Style="display:none;" />
                </td>
            </tr>
            <tr style="border-style: solid; border-width: thin; border-color: #C0C0C0">
                <td style="border-style: solid; border-width: thin; border-color: #C0C0C0; vertical-align :top ; "  class="auto-style1">Condition </td>
                <td colspan="1" style="border-top-color: #C0C0C0; border-left-color: #C0C0C0; border-width: thin; border-top-style: solid; border-bottom-style: solid; border-left-style: solid; border-bottom-color: #C0C0C0; background-color: #f9fefe; Width:100%;">
                    <uc1:UC_SummerNote runat="server" ID="txtDetail3" />
                </td></tr>
        </table>
    </td>

</tr>
