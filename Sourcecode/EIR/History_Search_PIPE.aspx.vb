﻿Imports System.Data.SqlClient
Public Class History_Search_PIPE
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ClearPanelSearch()
        End If
    End Sub

    Private Sub ClearPanelSearch()
        BL.BindDDlPlant(ddl_Plant) : ddl_Plant.Items(0).Text = ""
        BindDDlUser(ddl_User, False) : ddl_User.Items(0).Text = ""
    End Sub

    Public Sub BindDDlUser(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)

        Dim SQL As String = "select USER_ID, User_Prefix + ' ' + [User_Name] + ' ' + User_Surname as NAME from MS_User"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Equipement-Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("NAME"), DT.Rows(i).Item("USER_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

End Class