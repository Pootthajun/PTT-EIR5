﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_DocumentTreeNode.ascx.vb" Inherits="EIR.UC_DocumentTreeNode" %>
<tr>
    <td id="tdIndent" runat="server" Level="0" IndextLevelPixel="50" Mode="NoChild">
        <asp:Image ID="imgMode" runat="server" Width="16px" Height="16px" ImageUrl="resources/images/tree_nochild.png"></asp:Image>
        <asp:Image ID="imgIcon" runat="server"></asp:Image>
        <asp:Label ID="lblName" runat="server"></asp:Label>
        <asp:Label ID="lblID" runat="server" Visible="false" Text="0" ParentID="0"></asp:Label>    
        <asp:Button ID="btnExpand" runat="server" style="display:none;"></asp:Button> 
    </td>
    <td><asp:Label ID="lblNodeStatus" runat="server"></asp:Label></td>
    <td><asp:Label ID="lblNoticeDate" runat="server"></asp:Label></td>
    <td><asp:Label ID="lblCriticalDate" runat="server"></asp:Label></td>
    <td><asp:Label ID="lblUpdate" runat="server"></asp:Label></td>                            
</tr>            
