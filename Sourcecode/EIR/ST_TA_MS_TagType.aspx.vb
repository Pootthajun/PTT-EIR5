﻿
Imports System.Data
Imports System.Data.SqlClient
Public Class ST_TA_MS_TagType
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetTagType(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindTagType()


        Dim SQL As String = " "
        SQL &= " --==Turnaround==" & vbNewLine
        SQL &= " SELECT TagType.TAG_TYPE_ID,TAG_TYPE_Name,TotalTag,TagType.Active_Status," & vbNewLine
        SQL &= " Count(INSP.INSP_ID) TotalInspection" & vbNewLine
        SQL &= " FROM" & vbNewLine
        SQL &= " (  " & vbNewLine
        SQL &= " SELECT" & vbNewLine
        SQL &= " MS_ST_TAG_TYPE.TAG_TYPE_ID,TAG_TYPE_Name," & vbNewLine
        SQL &= " SUM(CASE MS_ST_TAG.TAG_TYPE_ID WHEN MS_ST_TAG_TYPE.TAG_TYPE_ID THEN 1 ELSE 0 END) TotalTag " & vbNewLine


        SQL &= " ,MS_ST_TAG_TYPE.Active_Status" & vbNewLine
        SQL &= " FROM MS_ST_TAG_TYPE" & vbNewLine

        SQL &= " Left Join(" & vbNewLine
        SQL &= "    Select  DISTINCT  'Ins' Group_Name, TAG_ID,MS_ST_TAG.TAG_TYPE_ID  FROM MS_ST_TAG" & vbNewLine
        SQL &= "    )  MS_ST_TAG  ON MS_ST_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine

        SQL &= " Group BY											" & vbNewLine
        SQL &= " MS_ST_TAG_TYPE.TAG_TYPE_ID,TAG_TYPE_Name,MS_ST_TAG_TYPE.Active_Status" & vbNewLine
        SQL &= " --  ---==-End Trunaround==--" & vbNewLine
        SQL &= " ) TagType " & vbNewLine
        SQL &= " Left Join (" & vbNewLine
        SQL &= " --รายชื่อ Tag ใน station & Turnaround" & vbNewLine
        SQL &= " Select DISTINCT 'Ins' Group_Name, TAG_TYPE_ID,INSP_ID FROM MS_ST_TAG_Inspection" & vbNewLine
        SQL &= " ) INSP" & vbNewLine
        SQL &= " ON TagType.TAG_TYPE_ID=INSP.TAG_TYPE_ID" & vbNewLine
        SQL &= " GROUP BY TagType.TAG_TYPE_ID,TAG_TYPE_Name,TotalTag," & vbNewLine
        SQL &= " TotalTag,TagType.Active_Status" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try
        Session("MS_ST_TagType") = DT

        Navigation.SesssionSourceName = "MS_ST_TagType"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTagType
    End Sub

    Protected Sub rptTagType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTagType.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTypeName As Label = e.Item.FindControl("lblTypeName")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblInspection As Label = e.Item.FindControl("lblInspection")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblTypeName.Text = e.Item.DataItem("TAG_TYPE_Name")
        lblTag.Text = e.Item.DataItem("TotalTag")
        lblInspection.Text = e.Item.DataItem("TotalInspection")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        btnEdit.Attributes("TypeID") = e.Item.DataItem("TAG_TYPE_ID")
    End Sub

    Protected Sub rptTagType_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTagType.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim TypeID As Integer = btnEdit.Attributes("TypeID")

        Select Case e.CommandName
            Case "Edit"
                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                'txtTypeName.ReadOnly = True
                txtTypeName.Focus()
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListTagType.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "Select * FROM MS_ST_TAG_TYPE WHERE TAG_TYPE_ID=" & TypeID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Equipment-Type Not Found"
                    pnlBindingError.Visible = True
                    BindTagType()
                    Exit Sub
                End If

                txtTypeName.Attributes("TypeID") = DT.Rows(0).Item("TAG_TYPE_ID")
                txtTypeName.Text = DT.Rows(0).Item("TAG_TYPE_Name")

                '----------------Bind Default Inspection-----------------------
                Dim INSP As DataTable = BL.Get_Stationary_TagType_Inspection(TypeID)
                LoadInspectionItem(INSP)

                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")

                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_ST_TAG_TYPE Set active_status=Case active_status When 1 Then 0 Else 1 End" & vbNewLine
                SQL &= " WHERE  TAG_TYPE_ID=" & TypeID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTagType()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select

    End Sub

    Protected Sub ResetTagType(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindTagType()
        '-----------------------------------
        ClearPanelEdit()
        pnlListTagType.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtTypeName.Text = ""
        txtTypeName.Attributes("TypeID") = "0"
        chkAvailable.Checked = True
        btnCreate.Visible = True

        '------------------Launch Default Form -----------------------
        'Inspection = BL.Get_TA_Default_Inspection
        'Inspection_Status = BL.Get_TA_Default_Inspection_Status

        Inspection = BL.Get_Stationary_Default_Inspection
        Inspection_Status = BL.Get_Stationary_Default_Inspection_Status

        rptInsp.DataSource = Inspection
        rptInsp.DataBind()
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True

        btnCreate.Visible = False
        txtTypeName.ReadOnly = False
        txtTypeName.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListTagType.Enabled = False

    End Sub


    Dim Inspection As New DataTable
    Dim Inspection_Status As New DataTable


    Protected Sub rptInsp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInsp.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Header
                Dim thTotalCol As HtmlTableCell = e.Item.FindControl("thTotalCol")
                Dim rptStatus As Repeater = e.Item.FindControl("rptStatus")
                Dim thStatusCol As HtmlTableCell = e.Item.FindControl("thStatusCol")

                thStatusCol.ColSpan = Inspection_Status.Rows.Count + 1
                thTotalCol.ColSpan = thStatusCol.ColSpan + 2

                AddHandler rptStatus.ItemDataBound, AddressOf rptStatusHeader_ItemDataBound
                rptStatus.DataSource = Inspection_Status
                rptStatus.DataBind()

            Case ListItemType.Item, ListItemType.AlternatingItem

                Dim lblNo As Label = e.Item.FindControl("lblNo")
                Dim lblInspItem As Label = e.Item.FindControl("lblInspItem")
                'Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")


                Dim rptInspItem As Repeater = e.Item.FindControl("rptInspItem")
                Dim ddl_Ref_Item As DropDownList = e.Item.FindControl("ddl_Ref_Item")
                Dim ddl_Ref_Status As DropDownList = e.Item.FindControl("ddl_Ref_Status")

                lblNo.Text = e.Item.ItemIndex + 1
                lblNo.Attributes("INSP_ID") = e.Item.DataItem("INSP_ID")
                lblInspItem.Text = e.Item.DataItem("INSP_Name")
                'lblTo_Table.Text = e.Item.DataItem("To_Table")

                ddl_Ref_Item.Items.Add(New ListItem("Alway Availble", 0))
                For i As Integer = 0 To Inspection.Rows.Count - 1
                    If Inspection.Rows(i).Item("INSP_ID") <> e.Item.DataItem("INSP_ID") Then
                        ddl_Ref_Item.Items.Add(New ListItem(Inspection.Rows(i).Item("INSP_Name"), Inspection.Rows(i).Item("INSP_ID")))
                    End If
                Next
                ddl_Ref_Status.Items.Add(New ListItem("Choose ..", 0))
                For i As Integer = 0 To Inspection_Status.Rows.Count - 1
                    ddl_Ref_Status.Items.Add(New ListItem(Inspection_Status.Rows(i).Item("STATUS_Name"), Inspection_Status.Rows(i).Item("STATUS_ID")))
                Next

                AddHandler rptInspItem.ItemDataBound, AddressOf rptInspItem_ItemDataBound
                rptInspItem.DataSource = Inspection_Status
                rptInspItem.DataBind()

            Case ListItemType.Footer

        End Select
    End Sub

    Private Sub rptStatusHeader_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        lblStatus.Text = e.Item.DataItem("STATUS_Name")
    End Sub

    Private Sub rptInspItem_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim chkUse As CheckBox = e.Item.FindControl("chkUse")
        chkUse.Attributes("STATUS_ID") = e.Item.DataItem("STATUS_ID")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtTypeName.Text = "" Then
            lblValidation.Text = "Please insert Type Name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim INSP As DataTable = GetCurrentInspectionItem()
        If INSP.Rows.Count = 0 Then
            lblValidation.Text = "Please Select at lease one Inspection Item"
            pnlValidation.Visible = True
            Exit Sub
        End If
        INSP.DefaultView.RowFilter = "(REF_INSP_ID Is Not NULL And REF_STATUS_ID Is NULL) Or (REF_INSP_ID Is NULL And REF_STATUS_ID Is Not NULL)"
        If INSP.DefaultView.Count > 0 Then
            Dim _dt As New DataTable
            Dim _da As New SqlDataAdapter("Select INSP_Name FROM MS_ST_Default_Inspection WHERE INSP_ID=" & INSP.DefaultView(0).Item("INSP_ID"), BL.ConnStr)
            _da.Fill(_dt)
            lblValidation.Text = "Please completed availibility condition For " & _dt.Rows(0).Item("INSP_Name")
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim TypeID As Integer = txtTypeName.Attributes("TypeID")
        Dim SQL As String = "Select * FROM MS_ST_TAG_TYPE WHERE TAG_TYPE_Name='" & txtTypeName.Text.Replace("'", "''") & "' AND TAG_TYPE_ID<>" & TypeID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Equipment Type Name is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        '--------------Update Equipment Type---------
        SQL = "SELECT * FROM MS_ST_TAG_TYPE WHERE TAG_TYPE_ID=" & TypeID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            TypeID = GetNewTypeID()
            DR("TAG_TYPE_ID") = TypeID
        Else
            DR = DT.Rows(0)
        End If

        DR("TAG_TYPE_Name") = txtTypeName.Text
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        '------------Update Inspection ---------
        Dim DT_INSP_Inspection As DataTable = INSP
        'DT_INSP_Inspection.DefaultView.RowFilter = " TO_TABLE ='MS_ST_Default_Inspection' "

        Dim Command As New SqlCommand
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Command
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM MS_ST_TAG_Inspection WHERE TAG_TYPE_ID=" & TypeID
            .ExecuteNonQuery()
            .Dispose()
        End With
        SQL = "SELECT * FROM MS_ST_TAG_Inspection WHERE 1=0"
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, Conn)
        DA.Fill(DT)

        For i As Integer = 0 To DT_INSP_Inspection.Rows.Count - 1
            Dim IR As DataRow = DT.NewRow
            IR("TAG_TYPE_ID") = TypeID
            IR("INSP_ID") = DT_INSP_Inspection.Rows(i).Item("INSP_ID")
            IR("STATUS_ID") = DT_INSP_Inspection.Rows(i).Item("STATUS_ID")
            IR("REF_INSP_ID") = DT_INSP_Inspection.Rows(i).Item("REF_INSP_ID")
            IR("REF_STATUS_ID") = DT_INSP_Inspection.Rows(i).Item("REF_STATUS_ID")
            IR("Update_By") = 0
            IR("Update_Time") = Now
            DT.Rows.Add(IR)
        Next
        cmd = New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try
        'INSP.Clear()


        ''------------Update TA Inspection ---------
        'Dim DT_INSP_TA_Inspection As DataTable = INSP
        'DT_INSP_TA_Inspection.DefaultView.RowFilter = " TO_TABLE ='MS_ST_Default_Inspection' "

        'Command = New SqlCommand
        'Conn = New SqlConnection(BL.ConnStr)
        'Conn.Open()
        'With Command
        '    .Connection = Conn
        '    .CommandType = CommandType.Text
        '    .CommandText = "DELETE FROM MS_ST_TAG_Inspection WHERE TAG_TYPE_ID=" & TypeID
        '    .ExecuteNonQuery()
        '    .Dispose()
        'End With
        'SQL = "SELECT * FROM MS_ST_TAG_Inspection WHERE 1=0"
        'DT = New DataTable
        'DA = New SqlDataAdapter(SQL, Conn)
        'DA.Fill(DT)

        'For i As Integer = 0 To DT_INSP_TA_Inspection.DefaultView.Count - 1
        '    Dim IR As DataRow = DT.NewRow
        '    IR("TAG_TYPE_ID") = TypeID
        '    IR("INSP_ID") = DT_INSP_TA_Inspection.DefaultView(i).Item("INSP_ID")
        '    IR("STATUS_ID") = DT_INSP_TA_Inspection.DefaultView(i).Item("STATUS_ID")
        '    IR("REF_INSP_ID") = DT_INSP_TA_Inspection.DefaultView(i).Item("REF_INSP_ID")
        '    IR("REF_STATUS_ID") = DT_INSP_TA_Inspection.DefaultView(i).Item("REF_STATUS_ID")
        '    IR("Update_By") = 0
        '    IR("Update_Time") = Now
        '    DT.Rows.Add(IR)
        'Next
        'cmd = New SqlCommandBuilder(DA)
        'Try
        '    DA.Update(DT)
        'Catch ex As Exception
        '    lblValidation.Text = "Invalid parameter"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End Try


        ResetTagType(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetCurrentInspectionItem() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("INSP_ID", GetType(Integer))
        DT.Columns.Add("STATUS_ID", GetType(Integer))
        DT.Columns.Add("REF_INSP_ID", GetType(Integer))
        DT.Columns.Add("REF_STATUS_ID", GetType(Integer))
        'DT.Columns.Add("TO_TABLE", GetType(String))

        For Each Item As RepeaterItem In rptInsp.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For

            Dim lblNo As Label = Item.FindControl("lblNo")
            Dim rptInspItem As Repeater = Item.FindControl("rptInspItem")
            Dim ddl_Ref_Item As DropDownList = Item.FindControl("ddl_Ref_Item")
            Dim ddl_Ref_Status As DropDownList = Item.FindControl("ddl_Ref_Status")

            Dim Tag_Class_ID As Integer = lblNo.Attributes("Tag_Class_ID")
            Dim INSP_ID As Integer = lblNo.Attributes("INSP_ID")
            'Dim lblTo_Table As Label = Item.FindControl("lblTo_Table")

            For Each SItem As RepeaterItem In rptInspItem.Items
                If SItem.ItemType <> ListItemType.Item And SItem.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim chkUse As CheckBox = SItem.FindControl("chkUse")
                Dim STATUS_ID As Integer = chkUse.Attributes("STATUS_ID")
                'Dim TO_TABLE As String = lblTo_Table.Text
                If chkUse.Checked Then
                    Dim DR As DataRow = DT.NewRow
                    DR("INSP_ID") = INSP_ID
                    DR("STATUS_ID") = STATUS_ID
                    'DR("TO_TABLE") = TO_TABLE
                    If ddl_Ref_Item.SelectedIndex > 0 Then DR("REF_INSP_ID") = ddl_Ref_Item.Items(ddl_Ref_Item.SelectedIndex).Value
                    If ddl_Ref_Status.SelectedIndex > 0 Then DR("REF_STATUS_ID") = ddl_Ref_Status.Items(ddl_Ref_Status.SelectedIndex).Value
                    DT.Rows.Add(DR)
                End If
            Next

        Next
        Return DT
    End Function

    Private Sub LoadInspectionItem(ByVal INSP As DataTable)

        For Each Item As RepeaterItem In rptInsp.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For

            Dim lblNo As Label = Item.FindControl("lblNo")
            Dim rptInspItem As Repeater = Item.FindControl("rptInspItem")
            Dim ddl_Ref_Item As DropDownList = Item.FindControl("ddl_Ref_Item")
            Dim ddl_Ref_Status As DropDownList = Item.FindControl("ddl_Ref_Status")

            Dim INSP_ID As Integer = lblNo.Attributes("INSP_ID")

            For Each SItem As RepeaterItem In rptInspItem.Items
                If SItem.ItemType <> ListItemType.Item And SItem.ItemType <> ListItemType.AlternatingItem Then Continue For

                Dim chkUse As CheckBox = SItem.FindControl("chkUse")
                Dim STATUS_ID As Integer = chkUse.Attributes("STATUS_ID")
                INSP.DefaultView.RowFilter = "INSP_ID=" & INSP_ID & " AND STATUS_ID=" & STATUS_ID
                chkUse.Checked = INSP.DefaultView.Count > 0
            Next

            For i As Integer = ddl_Ref_Item.Items.Count - 1 To 1 Step -1
                INSP.DefaultView.RowFilter = "INSP_ID=" & INSP_ID & " AND REF_INSP_ID=" & ddl_Ref_Item.Items(i).Value
                If INSP.DefaultView.Count > 0 Then
                    ddl_Ref_Item.SelectedIndex = i
                    Exit For
                End If
            Next

            For i As Integer = ddl_Ref_Status.Items.Count - 1 To 0 Step -1
                INSP.DefaultView.RowFilter = "INSP_ID=" & INSP_ID & " AND REF_STATUS_ID=" & ddl_Ref_Status.Items(i).Value
                If INSP.DefaultView.Count > 0 Then
                    ddl_Ref_Status.SelectedIndex = i
                    Exit For
                End If
            Next
        Next
    End Sub

    Private Function GetNewTypeID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(TAG_TYPE_ID),0)+1 FROM MS_ST_TAG_TYPE "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Session("Clipboard_MS_ST_Inspection") = GetCurrentInspectionItem()
        lblBindingSuccess.Text = "Settings copied"
        pnlBindingSuccess.Visible = True
    End Sub

    Protected Sub btnPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPaste.Click
        Try
            LoadInspectionItem(Session("Clipboard_MS_ST_Inspection"))
        Catch ex As Exception
            lblValidation.Text = "Unable to paste Settings"
            pnlValidation.Visible = True
            Exit Sub
        End Try

    End Sub

End Class