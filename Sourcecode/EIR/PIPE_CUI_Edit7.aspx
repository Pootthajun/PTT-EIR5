﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="PIPE_CUI_Edit7.aspx.vb" Inherits="EIR.PIPE_CUI_Edit7" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register Src="~/UC_PIPE_TAG.ascx" TagPrefix="pipe" TagName="UC_PIPE_TAG" %>
<%@ Register Src="~/UC_PIPE_POINT.ascx" TagPrefix="pipe" TagName="UC_PIPE_POINT" %>
<%@ Register src="GL_DialogInputValue.ascx" tagname="GL_DialogInputValue" tagprefix="uc1" %>
<%@ Register Src="~/UC_PIPE_CUI_ERO_Class.ascx" TagPrefix="pipe" TagName="UC_PIPE_CUI_ERO_Class" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />
    <link rel="stylesheet" href="resources/css/File_Album.css" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
    <!-- Page Head -->
	<h2>Corrosion Under Insulation Report</h2>
    <div class="clear"></div> <!-- End .clear -->
    <div class="content-box"><!-- Start Content Box -->
        <div class="content-box-header">
			<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label> :: <asp:Label ID="lblStep" runat="server"></asp:Label></h3>	
            <ul class="content-box-tabs">
                <asp:Repeater ID="rptTab" runat="server">
                    <ItemTemplate>
                        <li><a id="HTab" runat="server" >xxxxxxxxxxxx</a></li>
                    </ItemTemplate>
                </asp:Repeater>
			</ul>					
			<div class="clear"></div>					
		</div> <!-- End .content-box-header -->
        <div class="content-box-content">
            <div class="tab-content current">
                <p style="font-weight:bold; font-size:16px;">
				<label class="column-left" style="width:120px; font-size:16px;" >Report for: </label>
					<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader" ></asp:Label>	               						
					| Tag : <asp:Label ID="lbl_Tag" runat="server" Text="Tag" CssClass="EditReportHeader"></asp:Label>
					| Point : <asp:Label ID="lbl_Point" runat="server" Text="Point" CssClass="EditReportHeader" ></asp:Label>
                    | Report Date :
                    <asp:TextBox runat="server" ID="txt_Rpt_Date" Width="100px" CssClass="text-input small-input " PlaceHolder="..." style="text-align:center;"></asp:TextBox>  
                    <Ajax:CalendarExtender ID="txt_Rpt_Date_Extender" runat="server" 
                             Format="dd MMM yyyy" TargetControlID="txt_Rpt_Date" PopupPosition="Right" >
                    </Ajax:CalendarExtender>
                    <font color="red">**</font>              
				</p>	
                <div class="clear"></div><!-- End .clear -->

                 <!-- This is the target div. id must match the href of this div's tab -->
                <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                    <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                    <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                </asp:Panel>                

                <ul class="shortcut-buttons-set">						
						<li>
						    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="shortcut-button">
						        <span> 
							        <img src="resources/images/icons/delete32.png" alt="icon" /><br />
							        Remove this Step
							    </span>
						    </asp:LinkButton>
							<Ajax:ConfirmButtonExtender ID="cfm_Delete" 
                                runat="server" TargetControlID="lnkDelete" ConfirmText="Are you sure to remove this step permanently?">
                            </Ajax:ConfirmButtonExtender>
						</li>
                        <li>
						    <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
						        <span> 
							        <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							        Clear all
							    </span>
						    </asp:LinkButton>
							<Ajax:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                runat="server" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all inspection for this step permanently?">
                            </Ajax:ConfirmButtonExtender>
						</li>
						<li>
						    <a class="shortcut-button" href="javascript:window.location.href=window.location.href;">
							    <span>
								    <img src="resources/images/icons/refresh_48.png" alt="icon" /><br />
							        Reset this tab
							    </span>
						    </a>
						</li>								 
						<li>
						    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
							    <span>
								    <img src="resources/images/icons/print_48.png" alt="icon"/><br />
								    Preview report
							    </span>
						    </asp:LinkButton>
						</li>				        
				</ul>

                <pipe:UC_PIPE_TAG runat="server" id="Pipe_Info" />
                <pipe:UC_PIPE_POINT runat="server" id="Point_Info" />
                <pipe:UC_PIPE_CUI_ERO_Class runat="server" ID="CUI_Class" />

                <style type="text/css">
                .File_Zoom_Mask {
                    background-image:url('resources/images/icons/zoom_white_16.png');
                }

                .File_Command {
                    width:100%;
                    display: block; 
                    opacity:unset;
                    z-index:3
                }

                .File_Command input[type="image"] {
                    margin-top:5px;
                }

                .File_Image {
                    width:unset;
                    height:unset;
                    position:absolute;    
                    z-index:1;                
                }

                 .content-box-header {
                    height:unset;
                    }

                </style>

                <table cellpadding="0" cellspacing="0" class="propertyTable photograph_Sector">
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px;">
                            Visual Inspection
                        </td>
                        <td class="propertyGroup" style="border:none; padding-top:20px;">
                            Visual Inspection
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%; background-color:black; ">
                            <div class="File_Zoom_Mask" ID="ZoomMask1" runat="server" ></div>
                            <asp:Panel CssClass="File_Image" runat="server" ID="img_File1"></asp:Panel>                            
                            <div class="File_Command">
                                <asp:Button ID="btnRefresh1" runat="server" Text="" style="display:none;"/>
                                <asp:ImageButton id="btnEdit1" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" style="margin-right:10px;" />
                                <asp:ImageButton ID="btnDelete1" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left:10px;" />
                                <Ajax:ConfirmButtonExtender ID="btnDelete1_Confirm" runat="server" ConfirmText="Confirm to delete file?" TargetControlID="btnDelete1"></Ajax:ConfirmButtonExtender>
                            </div>  
                        </td>
                        <td style="width:50%; background-color:black; ">
                            <div class="File_Zoom_Mask" ID="ZoomMask2" runat="server" ></div>
                            <asp:Panel CssClass="File_Image" runat="server" ID="img_File2"></asp:Panel> 
                            <div class="File_Command">
                                <asp:Button ID="btnRefresh2" runat="server" Text="" style="display:none;"/>
                                <asp:ImageButton id="btnEdit2" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" style="margin-right:10px;" />
                                <asp:ImageButton ID="btnDelete2" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left:10px;" />
                                <Ajax:ConfirmButtonExtender ID="btnDelete2_Confirm" runat="server" ConfirmText="Confirm to delete file?" TargetControlID="btnDelete2"></Ajax:ConfirmButtonExtender>
                            </div>  
                        </td>
                    </tr>                                 
                    <tr>
                        <td>
                            <asp:TextBox runat="server" ID="txtFileDesc1" TextMode="MultiLine" Rows="3" Width="100%" PlaceHolder="..." ToolTip="Note any information here" MaxLength="500"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFileDesc2" TextMode="MultiLine" Rows="3" Width="100%"  PlaceHolder="..." ToolTip="Note any information here" MaxLength="500"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px;">
                            Visual Inspection
                        </td>
                        <td class="propertyGroup" style="border:none; padding-top:20px;">
                            Visual Inspection
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%; background-color:black;">
                            <div class="File_Zoom_Mask" ID="ZoomMask3" runat="server" ></div>
                            <asp:Panel CssClass="File_Image" runat="server" ID="img_File3"></asp:Panel>
                            <div class="File_Command">
                                <asp:Button ID="btnRefresh3" runat="server" Text="" style="display:none;"/>
                                <asp:ImageButton id="btnEdit3" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" style="margin-right:10px;" />
                                <asp:ImageButton ID="btnDelete3" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left:10px;" />
                                <Ajax:ConfirmButtonExtender ID="btnDelete3_Confirm" runat="server" ConfirmText="Confirm to delete file?" TargetControlID="btnDelete3"></Ajax:ConfirmButtonExtender>
                            </div> 
                        </td>
                        <td style="width:50%; background-color:black;">
                            <div class="File_Zoom_Mask" ID="ZoomMask4" runat="server" ></div>
                            <asp:Panel CssClass="File_Image" runat="server" ID="img_File4"></asp:Panel>
                            <div class="File_Command">
                                <asp:Button ID="btnRefresh4" runat="server" Text="" style="display:none;"/>
                                <asp:ImageButton id="btnEdit4" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" style="margin-right:10px;" />
                                <asp:ImageButton ID="btnDelete4" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left:10px;" />
                                <Ajax:ConfirmButtonExtender ID="btnDelete4_Confirm" runat="server" ConfirmText="Confirm to delete file?" TargetControlID="btnDelete4"></Ajax:ConfirmButtonExtender>
                            </div> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox runat="server" ID="txtFileDesc3" TextMode="MultiLine" Rows="3" Width="100%" PlaceHolder="..." ToolTip="Note any information here" MaxLength="500"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtFileDesc4" TextMode="MultiLine" Rows="3" Width="100%"  PlaceHolder="..." ToolTip="Note any information here" MaxLength="500"></asp:TextBox>
                        </td>
                    </tr>                   
                </table>              
					
                <h3 style="padding-top:20px;">Note: </h3>
				<asp:TextBox Width="100%" TextMode="MultiLine" ID="txt_Note"  Height="100px" CssClass="text-input" MaxLength="1000" runat="server"></asp:TextBox>
			
                   
                <h3 style="padding-top:20px;">Recommendation: </h3>
                <asp:TextBox ID="txt_Recomment" runat="server" CssClass="text-input" Height="100px" MaxLength="1000" TextMode="MultiLine" Width="100%"></asp:TextBox>
                   
                    <p align="right">
                        <asp:Button id="btnHome" runat="server" CssClass="button" Text="All CUI reports" />
                        <asp:Button id="btnBack" runat="server" CssClass="button" Text="Back to Thickness measurement" />
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save Only" />

                        <asp:Button ID="btnAfterRepair" runat="server" CssClass="button" Text="Back to After Repair" />
                        <asp:Button ID="btnCoating" runat="server" CssClass="button" Text="Back to Coating" />
                        <asp:Button ID="btnInsulating" runat="server" CssClass="button" Text="Back to Insulating" />
                                                
                        <asp:Button ID="btnPost" runat="server" CssClass="button" Text="Posted Report" />
                    </p>
                               
                    <!-- End .clear -->                              
            </div>

            <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                    ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                <div>
                    <asp:Label ID="lblValidation" runat="server"></asp:Label>                    
                </div>
            </asp:Panel>

            <uc1:GL_DialogInputValue ID="DialogInput" runat="server" Visible="false" />

        </div>
    </div>

</ContentTemplate>
</asp:UpdatePanel>  

</asp:Content>
