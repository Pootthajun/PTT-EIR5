﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_DialogCreateOffRoutine.ascx.vb" Inherits="EIR.GL_DialogCreateOffRoutine" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<div class="MaskDialog">
</div>
<%--<div style="width:100%; height:100%; position:fixed; background-color:red;">
</div>--%>
<asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog" style="top:200px; width:auto;">
    <h2>
        Create Report for
        <asp:DropDownList CssClass="select" ID="ddl_Plant" runat="server" AutoPostBack="True">
        </asp:DropDownList>
		
		<asp:DropDownList CssClass="select" ID="ddl_Area" runat="server" AutoPostBack="True">
        </asp:DropDownList>    
                        
        <asp:DropDownList ID="ddl_Tag" runat="server" AutoPostBack="True" 
            CssClass="select">
        </asp:DropDownList>
        
        <asp:Label ID="lbl_TagType" runat="server" CssClass="EditReportHeader" Text=""></asp:Label>
    </h2>
   <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                            <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                                ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                            <div>
                                <asp:Label ID="lblValidation" runat="server"></asp:Label>
                            </div>
    </asp:Panel>

    <div style="text-align:right; width:100%;">
         <asp:Button ID="btnClose" runat="server" Class="button" Text="Cancel" style="margin-right:10px;" />
        <asp:Button ID="btnCreate" runat="server" Class="button" Text="Create report" style="margin-right:5px;" />
    </div>
   

</asp:Panel>
<cc1:DragPanelExtender ID="pnlDialog_DragPanelExtender" runat="server" 
    DragHandleID="pnlDialog" Enabled="True" TargetControlID="pnlDialog">
</cc1:DragPanelExtender>