﻿Imports System.Data
Imports System.Data.SqlClient
Public Class SPH_MS_Spring
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetSpring(Nothing, Nothing)
            ClearPanelSearch()
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindSpring()

        Dim SQL As String = "  SELECT DISTINCT PLANT_Code,ROUTE_Code," & vbLf
        SQL &= " SPH_ID,SPH_No,Pipe_No,Size_Inch,Load_Install,Load_Oper,Length_Cold,Length_Hot," & vbLf
        SQL &= " SPH_MS_Spring.Active_Status, SPH_MS_Spring.Update_By, SPH_MS_Spring.Update_Time" & vbLf
        SQL &= " FROM SPH_MS_Spring  " & vbLf
        SQL &= " INNER JOIN SPH_MS_Route ON SPH_MS_Spring.ROUTE_ID=SPH_MS_Route.ROUTE_ID" & vbLf
        SQL &= " INNER JOIN MS_PLANT ON SPH_MS_Route.PLANT_ID=MS_PLANT.PLANT_ID" & vbLf

        Dim WHERE As String = ""

        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " MS_PLANT.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " SPH_MS_Route.ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " AND "
        End If
        If txt_Search_Spring.Text <> "" Then
            WHERE &= " SPH_No Like '%" & txt_Search_Spring.Text.Replace("'", "''") & "%' AND "
        End If
        If txt_Search_Pipe.Text <> "" Then
            WHERE &= " Pipe_No Like '%" & txt_Search_Pipe.Text.Replace("'", "''") & "%' AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= " ORDER BY PLANT_Code,ROUTE_Code,SPH_No,Pipe_No"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("SPH_MS_Spring") = DT

        Navigation.SesssionSourceName = "SPH_MS_Spring"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptSpring
    End Sub

    Protected Sub rptSpring_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptSpring.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblSpringNo As Label = e.Item.FindControl("lblSpringNo")
        Dim lblPipe As Label = e.Item.FindControl("lblPipe")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")

        Dim lblLoad As Label = e.Item.FindControl("lblLoad")
        Dim lblLength As Label = e.Item.FindControl("lblLength")
        Dim lblSize As Label = e.Item.FindControl("lblSize")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblSpringNo.Text = e.Item.DataItem("SPH_No")
        lblPipe.Text = e.Item.DataItem("Pipe_No")
        lblPlant.Text = e.Item.DataItem("PLANT_Code")
        lblRoute.Text = e.Item.DataItem("ROUTE_Code")

        lblLoad.Text = " - "
        lblLength.Text = " - "

        If Not IsDBNull(e.Item.DataItem("Load_Install")) Then
            lblLoad.Text = FormatNumber(e.Item.DataItem("Load_Install"), 1) & lblLoad.Text
        Else
            lblLoad.Text = "xxx" & lblLoad.Text
        End If
        If Not IsDBNull(e.Item.DataItem("Load_Oper")) Then
            lblLoad.Text &= FormatNumber(e.Item.DataItem("Load_Oper"), 1)
        Else
            lblLoad.Text &= "xxx"
        End If
        If Not IsDBNull(e.Item.DataItem("Length_Cold")) Then
            lblLength.Text = FormatNumber(e.Item.DataItem("Length_Cold"), 1) & lblLength.Text
        Else
            lblLength.Text = "xxx" & lblLength.Text
        End If
        If Not IsDBNull(e.Item.DataItem("Length_Hot")) Then
            lblLength.Text &= FormatNumber(e.Item.DataItem("Length_Hot"), 1)
        Else
            lblLength.Text &= "xxx"
        End If

        lblSize.Text = e.Item.DataItem("Size_Inch")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))
        btnEdit.Attributes("SPH_ID") = e.Item.DataItem("SPH_ID")

    End Sub

    Protected Sub rptSpring_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSpring.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim SPH_ID As Integer = btnEdit.Attributes("SPH_ID")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                lblUpdateMode.Text = "Update"
                lblUpdateMode.Attributes("SPH_ID") = SPH_ID
                '------------------------------------
                pnlListSpringHanger.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT DISTINCT SPH_MS_Spring.*,PLANT_ID " & vbLf
                SQL &= " FROM SPH_MS_Spring" & vbLf
                SQL &= " INNER JOIN SPH_MS_Route ON SPH_MS_Spring.ROUTE_ID=SPH_MS_Route.ROUTE_ID" & vbLf
                SQL &= " WHERE SPH_ID = " & SPH_ID & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Spring Hanger Not Found"
                    pnlBindingError.Visible = True
                    BindSpring()
                    Exit Sub
                End If

                txtSpringNo.Text = DT.Rows(0).Item("SPH_No")
                txtPipeNo.Text = DT.Rows(0).Item("Pipe_No")

                BL.BindDDlPlant(ddl_Edit_Plant, DT.Rows(0).Item("PLANT_ID"))
                BL.BindDDlSpringRoute(ddl_Edit_Route, DT.Rows(0).Item("PLANT_ID"), DT.Rows(0).Item("ROUTE_ID"))

                txtInch.Text = DT.Rows(0).Item("Size_Inch")
                If Not IsDBNull(DT.Rows(0).Item("Load_Install")) Then
                    txtLoadInstall.Text = FormatNumber(DT.Rows(0).Item("Load_Install"), 1)
                End If
                If Not IsDBNull(DT.Rows(0).Item("Load_Oper")) Then
                    txtLoadOperation.Text = FormatNumber(DT.Rows(0).Item("Load_Oper"), 1)
                End If
                If Not IsDBNull(DT.Rows(0).Item("Length_Cold")) Then
                    txtCold.Text = FormatNumber(DT.Rows(0).Item("Length_Cold"), 1)
                End If
                If Not IsDBNull(DT.Rows(0).Item("Length_Hot")) Then
                    txtHot.Text = FormatNumber(DT.Rows(0).Item("Length_Hot"), 1)
                End If

                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")

                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE SPH_MS_Spring Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE SPH_ID=" & SPH_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindSpring()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try

        End Select
    End Sub

    Protected Sub ResetSpring(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindSpring()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListSpringHanger.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()

        pnlEdit.Visible = False
        lblUpdateMode.Text = ""

        BL.BindDDlPlant(ddl_Edit_Plant)
        BL.BindDDlSpringRoute(ddl_Edit_Route)

        txtSpringNo.Text = ""
        txtPipeNo.Text = ""
        txtInch.Text = ""
        txtLoadInstall.Text = ""
        txtLoadOperation.Text = ""
        txtCold.Text = ""
        txtHot.Text = ""

        chkAvailable.Checked = True
        btnCreate.Visible = True

        '-------------InStall Javascript------------
        'ImplementJavaIntegerText(txtInch, True)
        ImplementJavaMoneyText(txtLoadInstall)
        ImplementJavaMoneyText(txtLoadOperation)
        ImplementJavaMoneyText(txtCold)
        ImplementJavaMoneyText(txtHot)

    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        BL.BindDDlPlant(ddl_Search_Plant)
        BL.BindDDlSpringRoute(ddl_Search_Route)
        BindSpring()
    End Sub


    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BL.BindDDlSpringRoute(ddl_Search_Route, ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value)
        BindSpring()
    End Sub

    Protected Sub Search_Route_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Route.SelectedIndexChanged, txt_Search_Spring.TextChanged, txt_Search_Pipe.TextChanged
        BindSpring()
    End Sub

#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        ddl_Edit_Plant.Focus()
        lblUpdateMode.Text = "Create"
        lblUpdateMode.Attributes("SPH_ID") = 0

        '-----------------------------------
        pnlListSpringHanger.Enabled = False
    End Sub

    Protected Sub ddl_Edit_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Plant.SelectedIndexChanged
        BL.BindDDlSpringRoute(ddl_Edit_Route, ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value)
        btnSave.Focus()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click


        If txtSpringNo.Text = "" Then
            lblValidation.Text = "Please insert Spring No."
            pnlValidation.Visible = True
            Exit Sub
        End If
        If ddl_Edit_Plant.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If ddl_Edit_Route.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Route"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtInch.Text = "" Then
            lblValidation.Text = "Please insert Size"
            pnlValidation.Visible = True
            Exit Sub
        End If
        'If txtLoadInstall.Text = "" Then
        '    lblValidation.Text = "Please insert Load-Install"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If
        'If txtLoadOperation.Text = "" Then
        '    lblValidation.Text = "Please insert Load-Operation"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If
        'If txtCold.Text = "" Then
        '    lblValidation.Text = "Please insert Length-Cold"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If
        'If txtHot.Text = "" Then
        '    lblValidation.Text = "Please insert Length-Hot"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        Dim SPH_ID As Integer = lblUpdateMode.Attributes("SPH_ID")

        Dim SQL As String = "SELECT * FROM SPH_MS_Spring WHERE SPH_No='" & txtSpringNo.Text.Replace("'", "''") & "' AND SPH_ID<>" & SPH_ID & " AND ROUTE_ID=" & ddl_Edit_Route.Items(ddl_Edit_Route.SelectedIndex).Value
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Spring No. is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM SPH_MS_Spring WHERE SPH_ID=" & SPH_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            SPH_ID = GetNewSpringID()
            DR("SPH_ID") = SPH_ID
        Else
            DR = DT.Rows(0)
        End If

        DR("SPH_No") = txtSpringNo.Text
        DR("Pipe_No") = txtPipeNo.Text
        DR("ROUTE_ID") = ddl_Edit_Route.Items(ddl_Edit_Route.SelectedIndex).Value

        DR("Size_Inch") = txtInch.Text
        If txtLoadInstall.Text <> "" Then
            DR("Load_Install") = txtLoadInstall.Text
        Else
            DR("Load_Install") = DBNull.Value
        End If
        If txtLoadOperation.Text <> "" Then
            DR("Load_Oper") = txtLoadOperation.Text
        Else
            DR("Load_Oper") = DBNull.Value
        End If

        If txtCold.Text <> "" Then
            DR("Length_Cold") = txtCold.Text
        Else
            DR("Length_Cold") = DBNull.Value
        End If

        If txtHot.Text <> "" Then
            DR("Length_Hot") = txtHot.Text
        Else
            DR("Length_Hot") = DBNull.Value
        End If

        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetSpring(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

        '------------ Focus Last Edit -----------
        DT = Session("SPH_MS_Spring")
        DT.DefaultView.RowFilter = "SPH_ID=" & SPH_ID
    End Sub

    Private Function GetNewSpringID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(SPH_ID),0)+1 FROM SPH_MS_Spring "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

End Class