﻿Imports System.Data
Imports System.Data.SqlClient
Public Class LAW_Document_Template_Setting
    Inherits System.Web.UI.Page

    Dim CL As New LawClass
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetForm(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindDocumentSetting()
        Dim DT As DataTable = CL.GetListDocumentTemplate()
        If DT.Rows.Count > 0 Then
            pnlBindingError.Visible = False

            Session("LAW_DOCUMENT_SETTING") = DT

            Navigation.SesssionSourceName = "LAW_DOCUMENT_SETTING"
            Navigation.RenderLayout()
        Else
            Session("LAW_DOCUMENT_SETTING") = Nothing
        End If
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptDocument
    End Sub

    Protected Sub rptDocument_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDocument.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblDocumentName As Label = e.Item.FindControl("lblDocumentName")
        Dim lblActiveStatus As Label = e.Item.FindControl("lblActiveStatus")
        Dim lblDocumentSettingID As Label = e.Item.FindControl("lblDocumentSettingID")

        lblDocumentName.Text = e.Item.DataItem("document_name")
        If e.Item.DataItem("active_status") = "Y" Then
            lblActiveStatus.Text = "Available"
            lblActiveStatus.ForeColor = Drawing.Color.Green
        Else
            lblActiveStatus.Text = "Unavailable"
            lblActiveStatus.ForeColor = Drawing.Color.OrangeRed
        End If

        lblDocumentSettingID.Text = e.Item.DataItem("document_setting_id")

    End Sub

    Private Sub rptDocument_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptDocument.ItemCommand
        Dim lblDocumentSettingID As Label = e.Item.FindControl("lblDocumentSettingID")

        Select Case e.CommandName
            Case "Edit"
                Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True
                lblUpdateMode.Text = "Update"


                '--------------Bind Value------------
                Dim DT As DataTable = CL.GetDataDocumentTemplate(lblDocumentSettingID.Text)
                If DT.Rows.Count > 0 Then
                    lblDocumentProcessSettingID.Text = DT.Rows(0)("document_setting_id")
                    txt_Document_Name.Text = DT.Rows(0)("document_name")
                    txtIntervalYear.Text = DT.Rows(0)("interval")

                    If DT.Rows(0)("active_status") = "Y" Then
                        chkAvailable.Checked = True
                    Else
                        chkAvailable.Checked = False
                    End If

                    '--------------Bind Paper List------------
                    Dim PaperDT As DataTable = CL.GetListDocumentSettingPaper(lblDocumentSettingID.Text)
                    If PaperDT.Rows.Count > 0 Then
                        rptPaperList.DataSource = PaperDT
                        rptPaperList.DataBind()
                        txt_Percent_Complete_TextChanged(Nothing, Nothing)
                    Else
                        rptPaperList.DataSource = Nothing
                        rptPaperList.DataBind()
                    End If
                End If
                DT.Dispose()
            Case "ToggleStatus"
                'Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
                '----เชคการใช้งานในระบบ
                Dim sql As String = "SELECT * FROM LAW_Document_Plan where document_setting_id=" & lblDocumentSettingID.Text
                Dim DT_Select As DataTable = BL.Execute_DataTable(sql)
                If DT_Select.Rows.Count > 0 Then
                    lblBindingError.Text = "Can not delete : Use in Master plan Setting "
                    pnlBindingError.Visible = True

                Else

                    If CL.DeleteDocumentSetting(lblDocumentSettingID.Text).ToLower = "true" Then
                        BindDocumentSetting()

                        lblBindingSuccess.Text = "Delete document successfully"
                        pnlBindingSuccess.Visible = True
                    End If
                End If



        End Select
    End Sub

    Protected Sub ResetForm(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        ClearPanelSearch()
        BindDocumentSetting()
        ''-----------------------------------
        ClearPanelEdit()
        ''----------------Bind Panel Search-------------------
        pnlListDocument.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        lblDocumentProcessSettingID.Text = "0"
        txt_Document_Name.Text = ""
        txtIntervalYear.Text = ""
        chkAvailable.Checked = True
        pnlEdit.Visible = False
        rptPaperList.DataSource = Nothing
        rptPaperList.DataBind()
        'txt_Percent_Complete_TextChanged(Nothing, Nothing)
        ImplementJavaOnlyNumberText(txtIntervalYear, "Left")

        lblUpdateMode.Text = ""

        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")

        btnCreate.Visible = USER_LEVEL = EIR_BL.User_Level.Administrator Or
                            USER_LEVEL = EIR_BL.User_Level.Inspector Or
                            USER_LEVEL = EIR_BL.User_Level.Approver
    End Sub


#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        '    'Dim SQL As String = "SELECT ISNULL(MIN(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM RPT_THM_Header"
        '    'Dim DT As New DataTable
        '    'Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        '    'DA.Fill(DT)
        '    'ddl_Search_Year.Items.Clear()
        '    'ddl_Search_Year.Items.Add("Choose a Year...")
        '    'For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
        '    '    ddl_Search_Year.Items.Add(i)
        '    'Next
        '    'ddl_Search_Year.Text = Now.Year + 543

        '    'BL.BindDDl_THM_Type(ddl_Search_Type, False)
        '    'BL.BindDDlPlant(ddl_Search_Plant, False)
        '    'BL.BindDDl_THM_Route(0, 0, ddl_Search_Route)
    End Sub



#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False
        lblUpdateMode.Text = "Create"
        ImplementJavaIntegerText(txtIntervalYear, False)
    End Sub




    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txt_Document_Name.Text.Trim = "" Then
            lblValidation.Text = "Please input Document Name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        For Each rItem As RepeaterItem In rptPaperList.Items
            Dim txt_Paper_Name As TextBox = rItem.FindControl("txt_Paper_Name")
            Dim txt_Percent_Complete As TextBox = rItem.FindControl("txt_Percent_Complete")
            Dim cmbOrganize As DropDownList = rItem.FindControl("cmbOrganize")

            If txt_Paper_Name.Text.Trim = "" Then
                lblValidation.Text = "Please input Paper Name"
                pnlValidation.Visible = True
                Exit Sub
            End If
            If cmbOrganize.SelectedValue = 0 Then
                lblValidation.Text = "Please select organize"
                pnlValidation.Visible = True
                Exit Sub
            End If

            If txt_Percent_Complete.Text.Trim = "" Then
                lblValidation.Text = "Please input Percent Complete"
                pnlValidation.Visible = True
                Exit Sub
            End If
        Next

        'Finding the FooterTemplate and access its controls
        Dim FooterTemplate As Control = rptPaperList.Controls(rptPaperList.Controls.Count - 1).Controls(0)
        Dim lbl_Total_Percent As Label = TryCast(FooterTemplate.FindControl("lbl_Total_Percent"), Label)
        If lbl_Total_Percent.Text <> "100" Then
            lblValidation.Text = "Invalid Percent Complete"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtIntervalYear.Text = "" Then
            txtIntervalYear.Text = 0
        End If
        Dim ret() As String = Split(CL.SaveDocumentTemplateSetting(lblDocumentProcessSettingID.Text, txt_Document_Name.Text, txtIntervalYear.Text, IIf(chkAvailable.Checked, "Y", "N"), BuiltPaperDT(), Session("USER_ID")), "|")
        If ret.Length = 2 Then
            If ret(0).ToLower = "true" Then
                ResetForm(Nothing, Nothing)
                lblBindingSuccess.Text = "Save successfully"
                pnlBindingSuccess.Visible = True
            End If
        End If
    End Sub

    Private Sub btnAddPaper_Click(sender As Object, e As EventArgs) Handles btnAddPaper.Click
        Dim dt As DataTable = BuiltPaperDT()
        Dim dr As DataRow = dt.NewRow
        dr("document_setting_paper_id") = 0
        dr("paper_name") = ""
        dr("law_organize_id") = "0"
        'dr("percent_complete") = DBNull.Value
        dt.Rows.Add(dr)

        If dt.Rows.Count > 0 Then
            rptPaperList.DataSource = dt
            rptPaperList.DataBind()

            txt_Percent_Complete_TextChanged(Nothing, Nothing)
        Else
            rptPaperList.DataSource = Nothing
            rptPaperList.DataBind()
        End If


    End Sub

    Private Function BuiltPaperDT() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("document_setting_paper_id")
        dt.Columns.Add("paper_name")
        dt.Columns.Add("law_organize_id", GetType(Integer))
        dt.Columns.Add("percent_complete", GetType(Integer))


        Dim dr As DataRow
        For Each rItem As RepeaterItem In rptPaperList.Items
            Dim lblNo As Label = rItem.FindControl("lblNo")
            Dim lblDocumentSettingPaperID As Label = rItem.FindControl("lblDocumentSettingPaperID")
            Dim txt_Paper_Name As TextBox = rItem.FindControl("txt_Paper_Name")
            Dim cmbOrganize As DropDownList = rItem.FindControl("cmbOrganize")
            Dim txt_Percent_Complete As TextBox = rItem.FindControl("txt_Percent_Complete")

            dr = dt.NewRow
            dr("document_setting_paper_id") = lblDocumentSettingPaperID.Text
            dr("paper_name") = txt_Paper_Name.Text
            dr("law_organize_id") = cmbOrganize.SelectedValue
            If IsNumeric(txt_Percent_Complete.Text) = True Then dr("percent_complete") = txt_Percent_Complete.Text

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function

    Private Sub rptPaperList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptPaperList.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Header

            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim lblNo As Label = e.Item.FindControl("lblNo")
                Dim lblDocumentSettingPaperID As Label = e.Item.FindControl("lblDocumentSettingPaperID")  '
                Dim txt_Paper_Name As TextBox = e.Item.FindControl("txt_Paper_Name")
                Dim txt_Percent_Complete As TextBox = e.Item.FindControl("txt_Percent_Complete")
                Dim cmbOrganize As DropDownList = e.Item.FindControl("cmbOrganize")
                Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

                lblNo.Text = e.Item.ItemIndex + 1 'e.Item.DataItem("no")
                lblDocumentSettingPaperID.Text = e.Item.DataItem("document_setting_paper_id")
                txt_Paper_Name.Text = e.Item.DataItem("paper_name")
                If Convert.IsDBNull(e.Item.DataItem("percent_complete")) = False Then
                    txt_Percent_Complete.Text = e.Item.DataItem("percent_complete")
                End If
                BL.BindDDLLawOrganize(cmbOrganize, e.Item.DataItem("law_organize_id"))
                ImplementJavaNumericText(txt_Percent_Complete, "Left")

                btnDelete.CommandArgument = e.Item.DataItem("document_setting_paper_id")
            Case ListItemType.Footer

        End Select
    End Sub

    Private Sub rptPaperList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptPaperList.ItemCommand
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Select Case e.CommandName
            Case "ToggleStatus"

                '----เชคการใช้งานในระบบ
                Dim sql As String = "SELECT * FROM LAW_Document_Plan_Paper where document_setting_paper_id=" & btnDelete.CommandArgument & ""
                Dim p_Select(1) As SqlParameter
                Dim DT_Select As DataTable = BL.Execute_DataTable(sql)
                If DT_Select.Rows.Count > 0 Then
                    lblValidation.Text = "Can not delete : Use in report "
                    pnlValidation.Visible = True

                Else

                    Dim dt As DataTable = BuiltPaperDT()
                    dt.Rows.RemoveAt(e.Item.ItemIndex)
                    If btnDelete.CommandArgument > 0 Then
                        CL.DeleteDocumentSettingPaper(btnDelete.CommandArgument)
                    End If

                    rptPaperList.DataSource = dt
                    rptPaperList.DataBind()
                    txt_Percent_Complete_TextChanged(Nothing, Nothing)


                End If

                'For i As Integer = dt.Rows.Count - 1 To 0 Step -1
                '    If e.Item.ItemIndex = i Then
                '        If CL.DeleteDocumentSettingPaper(dt.Rows(i)("document_setting_paper_id")) = "true" Then
                '            dt.Rows.RemoveAt(i)
                '        End If
                '    End If
                'Next

                'If dt.Rows.Count > 0 Then
                '    rptPaperList.DataSource = dt
                '    rptPaperList.DataBind()
                '    txt_Percent_Complete_TextChanged(Nothing, Nothing)
                'Else
                '    rptPaperList.DataSource = Nothing
                '    rptPaperList.DataBind()
                'End If
        End Select
    End Sub

    Protected Sub txt_Percent_Complete_TextChanged(sender As Object, e As EventArgs)

        'Finding the HeaderTemplate and access its controls
        'Dim HeaderTemplate As Control = rptProducts.Controls(0).Controls(0)
        'Dim lblHeader As Label = TryCast(HeaderTemplate.FindControl("lblHeader"), Label)
        'lblHeader.Text = "Header"

        'Finding the FooterTemplate and access its controls
        Dim FooterTemplate As Control = rptPaperList.Controls(rptPaperList.Controls.Count - 1).Controls(0)
        Dim lbl_Total_Percent As Label = TryCast(FooterTemplate.FindControl("lbl_Total_Percent"), Label)
        Dim dt As DataTable = BuiltPaperDT()
        If dt.Rows.Count > 0 Then
            If Convert.IsDBNull(dt.Compute("sum(percent_complete)", "")) = False Then
                lbl_Total_Percent.Text = dt.Compute("sum(percent_complete)", "")
            Else
                lbl_Total_Percent.Text = "0"
            End If
        Else
            lbl_Total_Percent.Text = "0"
        End If
        dt.Dispose()
    End Sub


End Class