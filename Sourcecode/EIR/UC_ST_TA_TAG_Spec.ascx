﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_ST_TA_TAG_Spec.ascx.vb" Inherits="EIR.UC_ST_TA_TAG_Spec" %>
<%@ Register Src="PageNavigation.ascx" TagName="PageNavigation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>

<table cellpadding="0" cellspacing="0" class="propertyTable">

    <tr>
        <td class="propertyGroup" style="width: 25%; font-weight: bold;">Tag Code
        </td>

        <td class="propertyGroup" style="border: none; border-top: 1px solid #CCCCCC; border-right: 1px solid #CCCCCC;" colspan="5">
            <asp:Label ID="lblCode" runat="server" Text="" Font-Bold="true"></asp:Label>
        </td>
    </tr>

    <tr>
        <td style="width: 25%;" class="propertyCaption">Plant <font color="red">**</font></td>
        <td style="width: 25%;" colspan="2">
            <asp:DropDownList ID="ddl_PLANT" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
        </td>
        <td style="width: 25%;" class="propertyCaption">Area <font color="red">**</font></td>
        <td style="width: 25%;" colspan="2">
            <asp:DropDownList ID="ddl_AREA" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="propertyCaption">Process <font color="red">**</font></td>
        <td colspan="2">
            <asp:DropDownList ID="ddl_PROCESS" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
        </td>
        <td class="propertyCaption">Tag No. <font color="red">**</font></td>
        <td colspan="2">
            <asp:TextBox ID="txt_TAG_No" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
    </tr>
        <tr>
        <td class="propertyCaption">Route <font color="red">**</font></td>
        <td colspan="2">
            <asp:DropDownList ID="ddl_Route" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
        </td>
        <td class="propertyCaption">Equipement-Type <font color="red">**</font></td>
        <td colspan="2">
            <asp:DropDownList ID="ddl_Equipement_Type" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
        </td>
    </tr>

        <tr>

        <td class="propertyCaption">Tag Name</td>
        <td colspan="5">
            <asp:TextBox ID="txt_Tag_Name" runat="server"   AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
         
    </tr>

    <tr>

               <td class="propertyCaption">Design Pressure</td>
        <td>
            <asp:TextBox ID="txt_Pressure_Design" runat="server" MaxLength="10" Style="text-align: left;" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td style="text-align: center;" class="propertyUnit">Bar.g</td> 
        <td class="propertyCaption">Initial Year <font color="red">**</font></td>
        <td colspan="2">
            <asp:TextBox ID="txt_Initial_Year" runat="server" MaxLength="4" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
    </tr>


    <tr id="trp1" runat="server">
        <td class="propertyCaption">Operating Pressure</td>
        <td>
            <asp:TextBox ID="txt_Pressure_Operating" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td style="text-align: center;" class="propertyUnit">Bar.g</td>
        <td class="propertyCaption">Norminal Thickness <font color="red">**</font></td>
        <td>
            <asp:TextBox ID="txt_Norminal_Thickness" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td style="text-align: center;" class="propertyUnit">mm</td>
    </tr>
    <tr id="trp2" runat="server">
        <td class="propertyCaption">Design Temperature</td>
        <td>
            <asp:TextBox ID="txt_Temperature_Design" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td style="text-align: center;" class="propertyUnit">° C</td>
        <td class="propertyCaption">Calculated Thickness <font color="red">**</font></td>
        <td>
            <asp:TextBox ID="txt_Calculated_Thickness" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td style="text-align: center;" class="propertyUnit">mm</td>
    </tr>
    <tr id="trp3" runat="server">
         <td class="propertyCaption">Operating Temperature</td>
        <td>
            <asp:TextBox ID="txt_Temperature_Operating" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td style="text-align: center;" class="propertyUnit">° C</td>
        <td class="propertyCaption">Insulation Thickness <font color="red">**</font></td>
        <td>
            <asp:TextBox ID="txt_Insulation_Thickness" runat="server" MaxLength="3" AutoPostBack="True" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td style="text-align: center;" class="propertyUnit">mm</td>
    </tr>
    <tr id="trp4" runat="server">
         <td class="propertyCaption">Size <font color="red">**</font></td>
        <td>
            <asp:TextBox ID="txt_Size" runat="server" MaxLength="5" AutoPostBack="True" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td style="text-align: center;" class="propertyUnit">inch</td>
        <td class="propertyCaption">Corrosion Allowance <font color="red">**</font></td>
        <td colspan="2">
            <asp:DropDownList ID="ddl_CA" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
        </td>
    </tr>
    <tr id="trp5" runat="server">
       
        
    </tr>
 
    <tr id="trp6" runat="server">
        <td class="propertyGroup propertyCaption" style="font-weight: bold;">Description</td>
        <td colspan="5">
            <asp:TextBox runat="server" ID="txt_Desc" TextMode="MultiLine" MaxLength="500"  AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
         
    </tr>
 </table>
