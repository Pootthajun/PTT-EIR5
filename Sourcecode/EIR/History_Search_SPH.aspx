﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="History_Search_SPH.aspx.vb" Inherits="EIR.History_Search_SPH" %>

<%@ Register Src="PageNavigation.ascx" TagName="PageNavigation" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UDPMain" runat="Server">
        <ContentTemplate>

            <!-- Page Head -->
            <h2>History Search Spring Hangers & Supports Reports</h2>

            <div class="clear"></div>
            <!-- End .clear -->

            <div class="content-box">
                <!-- Start Content Box -->
                <!-- End .content-box-header -->
                <div style="background-color: #efefef">

                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="200">Plant</td>
                            <td>
                                <asp:DropDownList CssClass="select" Style="font-weight: bold; width: 205px;"
                                    ID="ddl_Plant" runat="server" AutoPostBack="True">
                                </asp:DropDownList></td>
                            <td align="left">Class</td>
                            <td>
                                <table class="tb_class" style="top: 5px;">
	                                <tr>
		                                <td style="padding-right:40px;"> <asp:CheckBox runat="server" /> <span> OK </span></td>
		                                <td> <asp:CheckBox runat="server" /> <span> Abnormal </span></td>
	                                </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td width="200">Tag Code</td>
                            <td>
                                <asp:TextBox Width="190px" Font-Bold="true" ID="txt_Code" CssClass="text-input small-input" 
                                    MaxLength="50" runat="server"></asp:TextBox>
                            </td>
                            <td></td>    
                            <td>
                                <table class="tb_class" style="top: 5px;">
	                                <tr>
		                                <td> <asp:CheckBox runat="server" /> <span> Class A </span></td>
		                                <td> <asp:CheckBox runat="server" /> <span> Class B </span></td>
	                                </tr>
                                </table>
                            </td>                                     
                        </tr>

                        <tr>
                            <td align="left" width="120">Tag Name</td>
                            <td>
                                <asp:TextBox Width="190px" Font-Bold="true" ID="txt_Name" CssClass="text-input small-input" 
                                    MaxLength="50" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                            <td>
                                <table class="tb_class" style="top: 5px;">
	                                <tr>
		                                <td> <asp:CheckBox runat="server" /> <span> Class C </span></td>
		                                <td> <asp:CheckBox runat="server" /> <span> Class D </span></td>
	                                </tr>
                                </table>
                            </td>                            
                        </tr>

                        <tr>
                            <td width="200">Description/Remark</td>
                            <td align="left" colspan="3">
                                <asp:TextBox Width="640px" Font-Bold="true" CssClass="text-input small-input"
                                    ID="txt_Desc" MaxLength="500" runat="server"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td valign="top" width="200">Problem(s) occured between </td>
                            <td align="left">
                                <asp:TextBox runat="server" ID="txt_Prob_Start"
                                    Style="position: relative; left: 0px; top: 0px;" Font-Bold="true"
                                    CssClass="text-input small-input " Width="75px" MaxLength="15"
                                    AutoPostBack="True"></asp:TextBox>
                                <cc1:CalendarExtender ID="txt_Prob_Start_CalendarExtender" runat="server"
                                    Format="yyyy-MM-dd" TargetControlID="txt_Prob_Start">
                                </cc1:CalendarExtender>

                                &nbsp; to &nbsp; 
                                <asp:TextBox runat="server" ID="txt_Prob_End"
                                    Style="position: relative; left: 0px;" Font-Bold="true"
                                    CssClass="text-input small-input " Width="75px" MaxLength="15"
                                    AutoPostBack="True"></asp:TextBox>
                                <cc1:CalendarExtender ID="txt_Prob_End_CalendarExtender" runat="server"
                                    Format="yyyy-MM-dd" TargetControlID="txt_Prob_End">
                                </cc1:CalendarExtender>
                            </td>
                            <td class="auto-style1">Affiliate officer </td>
                            <td>
                                <asp:DropDownList CssClass="select" Style="width: 205px;" Font-Bold="true"
                                    ID="ddl_User" runat="server">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pnl_Inspection" runat="server" visible="false">
                        <tr>
                            <td width="200" valign="top">Contained Problem(s) for</td>
                            <td align="left" valign="top" style="vertical-align: top;">
                                <asp:Repeater ID="rptStationary" runat="server">
                                    <ItemTemplate>
                                        <label style="width: 180px; display: inline; float: left;">
                                            <asp:CheckBox ID="chbINSP" runat="server" Width="25px" Text="Inspection" AutoPostBack="True" />
                                            <asp:Label ID="lblINSP" runat="server" Width="150px" Font-Bold="false"></asp:Label>
                                        </label>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Repeater ID="rptRotating" runat="server">
                                    <ItemTemplate>
                                        <label style="width: 180px; display: inline; float: left;">
                                            <asp:CheckBox ID="chbINSP" runat="server" Width="25px" Text="Inspection" AutoPostBack="True" />
                                            <asp:Label ID="lblINSP" runat="server" Width="150px" Font-Bold="false"></asp:Label>
                                        </label>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="4" style="text-align: center">
                                <asp:LinkButton ID="btnClear" runat="server" CssClass="button" Text="Clear filter"></asp:LinkButton>
                                <asp:LinkButton ID="btnSearch" runat="server" CssClass="button" Text="Apply Filter"></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>               
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="ContentPlaceHolderHead">
    <style type="text/css">
        .auto-style1 {
            width: 373px;
        }

        .tb_class{            
            border: hidden;
            cellspacing: 0;
            cellpadding: 0;
            overflow: hidden;            
            position: relative;             
        }               
    </style>
</asp:Content>


