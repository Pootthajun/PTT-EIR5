﻿Public Class UC_Select_Template
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Public Event SelectTemplate(TemplateType As TemplateType)

    Public Sub ShowDialog()
        Me.Visible = True
    End Sub

    Public Sub CloseDialog()
        Me.Visible = False
    End Sub

    Private Sub btn_Close_Click(sender As Object, e As EventArgs) Handles btn_Close.Click
        CloseDialog()
    End Sub

#Region "Select Template"
    Private Sub lnkTemplate1_Click(sender As Object, e As EventArgs) Handles lnkTemplate1.Click
        SeleteTemplate(TemplateType.Template1)
    End Sub

    Private Sub lnkTemplate2_Click(sender As Object, e As EventArgs) Handles lnkTemplate2.Click
        SeleteTemplate(TemplateType.Template2)
    End Sub

    Private Sub lnkTemplate3_Click(sender As Object, e As EventArgs) Handles lnkTemplate3.Click
        SeleteTemplate(TemplateType.Template3)
    End Sub

    Private Sub lnkTemplate4_Click(sender As Object, e As EventArgs) Handles lnkTemplate4.Click
        SeleteTemplate(TemplateType.Template4)
    End Sub

    Private Sub lnkTemplate5_Click(sender As Object, e As EventArgs) Handles lnkTemplate5.Click
        SeleteTemplate(TemplateType.Template5)
    End Sub

    Private Sub lnkTemplate6_Click(sender As Object, e As EventArgs) Handles lnkTemplate6.Click
        SeleteTemplate(TemplateType.Template6)
    End Sub

    Private Sub lnkTemplate7_Click(sender As Object, e As EventArgs) Handles lnkTemplate7.Click
        SeleteTemplate(TemplateType.Template7)
    End Sub

    Private Sub lnkTemplate8_Click(sender As Object, e As EventArgs) Handles lnkTemplate8.Click
        SeleteTemplate(TemplateType.Template8)
    End Sub

    Private Sub SeleteTemplate(ByVal Template As TemplateType)
        RaiseEvent SelectTemplate(Template)
        CloseDialog()
    End Sub

#End Region

    Public Enum TemplateType
        Template1 = 1
        Template2 = 2
        Template3 = 3
        Template4 = 4
        Template5 = 5
        Template6 = 6
        Template7 = 7
        Template8 = 8

    End Enum

End Class