﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Master_User_Setting
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetUser(Nothing, Nothing)
            ClearPanelSearch()
            'Else
            '    RestorePassword(txt_Password.Text, txt_Confirm.Text)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    'Private Sub RestorePassword(ByVal Password As String, ByVal Confirm As String, Optional ByVal Key As String = "RestorePassword")

    '    Dim Script As String = "if(document.getElementById('" & txt_Password.ClientID & "'))document.getElementById('" & txt_Password.ClientID & "').value='" & Password & "';"
    '    Script &= "if(document.getElementById('" & txt_Confirm.ClientID & "'))document.getElementById('" & txt_Confirm.ClientID & "').value='" & Confirm & "';"
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), Key, Script, True)

    'End Sub

    Private Sub ClearPassword()
        Dim Script As String = "$('#" & txt_Password.ClientID & "').val('');"
        Script &= "$('#" & txt_Confirm.ClientID & "').val('');"

        Script = "setTimeout(function(){" & Script & "},500);"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "ClearPassword", Script, True)
    End Sub

    Private Sub BindUser()
        '------------------------------------------ แก้ View ------------
        Dim SQL As String = "SELECT DISTINCT * FROM VW_USER_ALL_GROUP " & vbNewLine
        Dim WHERE As String = ""

        If ddl_Search_Level.SelectedIndex > 0 Then
            WHERE &= " LEVEL_ID=" & ddl_Search_Level.Items(ddl_Search_Level.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_Plant.SelectedIndex > 0 And ddl_Search_Plant.Visible Then
            WHERE &= " USER_ID IN (SELECT USER_ID FROM MS_User_Coverage WHERE PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & ") AND "

        End If
        If ddl_Search_Position.SelectedIndex > 0 Then
            WHERE &= " POS_ID=" & ddl_Search_Position.Items(ddl_Search_Position.SelectedIndex).Value & " AND "
        End If

        '-------------------- Skill ----------------- 
        Dim Skill As String = ""
        If chk_Search_Stationary.Checked Then
            Skill &= " S_Stationary=1 OR "
        End If
        If chk_Search_Rotating.Checked Then
            Skill &= " S_Rotating=1 OR "
        End If
        If chk_Search_LubeOil.Checked Then
            Skill &= " S_LubeOil=1 OR "
        End If
        If chk_Search_PdMA.Checked Then
            Skill &= " S_PdMA=1 OR "
        End If
        If chk_Search_Thermography.Checked Then
            Skill &= " S_Thermography=1 OR "
        End If
        If Skill <> "" Then
            WHERE &= " (" & Skill.Substring(0, Skill.Length - 3) & ") AND "
            'Else
            '    WHERE &= " 1=0 AND "
        End If
        '----------------- แก้ตรงนี้ ------------------

        If Not chkSeeAll.Checked Then
            WHERE &= " Active_Status=1 AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= " ORDER BY User_Login "

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("MS_USER") = DT

        Navigation.SesssionSourceName = "MS_USER"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptUser
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptUser.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblUser As Label = e.Item.FindControl("lblUser")
        Dim lblFullname As Label = e.Item.FindControl("lblFullname")
        Dim lblPosition As Label = e.Item.FindControl("lblPosition")
        Dim lblLevel As Label = e.Item.FindControl("lblLevel")
        Dim lblCover As Label = e.Item.FindControl("lblCover")
        Dim lblSkill As Label = e.Item.FindControl("lblSkill")
        Dim lblVisit As Label = e.Item.FindControl("lblVisit")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblUser.Text = e.Item.DataItem("USER_Login")
        lblFullname.Text = e.Item.DataItem("USER_Full_Name")
        If Not IsDBNull(e.Item.DataItem("LEVEL_NAME")) Then
            lblLevel.Text = e.Item.DataItem("LEVEL_NAME")
            lblLevel.ForeColor = BL.Get_User_Level_Color(e.Item.DataItem("LEVEL_ID"))
        End If
        If Not IsDBNull(e.Item.DataItem("POS_NAME")) Then
            lblPosition.Text = e.Item.DataItem("POS_NAME")
        End If
        If Not IsDBNull(e.Item.DataItem("Coverage_Area")) Then
            If e.Item.DataItem("Coverage_Area") = "" Then
                lblCover.Text = "All"
            Else
                lblCover.Text = e.Item.DataItem("Coverage_Area")
            End If
        End If
        If Not IsDBNull(e.Item.DataItem("Last_Login")) Then
            lblVisit.Text = BL.ReportGridTime(e.Item.DataItem("Last_Login"))
        End If
        If Not IsDBNull(e.Item.DataItem("Update_Time")) Then
            lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))
        End If
        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        '----------------- Skill --------------
        Dim Skill As String = ""
        If e.Item.DataItem("S_Stationary") Then Skill &= "Stationary,"
        If e.Item.DataItem("S_Rotating") Then Skill &= "Rotating,"
        If e.Item.DataItem("S_LubeOil") Then Skill &= "Lube Oil,"
        If e.Item.DataItem("S_PdMA") Then Skill &= "PdMA,"
        If e.Item.DataItem("S_Thermography") Then Skill &= "Thermography,"
        If Skill <> "" Then
            Skill = Skill.Substring(0, Skill.Length - 1)
        End If
        lblSkill.Text = Skill
        '----------------- แก้ตรงนี้ --------------

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))
        btnEdit.Attributes("USER_ID") = e.Item.DataItem("USER_ID")

    End Sub

    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptUser.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim UserID As Integer = btnEdit.Attributes("USER_ID")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListUser.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT * FROM VW_USER_ALL_GROUP WHERE USER_ID=" & UserID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "USER Not Found"
                    pnlBindingError.Visible = True
                    BindUser()
                    Exit Sub
                End If

                txtPrefix.Text = DT.Rows(0).Item("User_Prefix")
                txtPrefix.Attributes("UserID") = UserID
                txtName.Text = DT.Rows(0).Item("User_Name")
                txtSurname.Text = DT.Rows(0).Item("User_Surname")

                For i As Integer = 0 To ddl_Edit_Gender.Items.Count - 1
                    If ddl_Edit_Gender.Items(i).Value = DT.Rows(0).Item("Gender") Then
                        ddl_Edit_Gender.SelectedIndex = i
                        Exit For
                    End If
                Next
                Dim LEVEL_ID As EIR_BL.User_Level = DT.Rows(0).Item("LEVEL_ID")
                BL.BindDDlUserLevel(ddl_Edit_Level, LEVEL_ID)

                If Not IsDBNull(DT.Rows(0).Item("POS_ID")) Then
                    BL.BindDDlUserPosition(ddl_Edit_Position, DT.Rows(0).Item("POS_ID"))
                End If

                txt_Login.Text = DT.Rows(0).Item("USER_Login")
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")

                'RestorePassword(DT.Rows(0).Item("USER_Password"), DT.Rows(0).Item("USER_Password"), "RestoreDBPassword")

                btnSave.Focus()

                '--------------- Coverage Plant ---------------
                SQL = "SELECT USER_ID,PLANT_ID FROM MS_User_Coverage WHERE USER_ID=" & UserID
                DA = New SqlDataAdapter(SQL, BL.ConnStr)
                DT = New DataTable
                DA.Fill(DT)
                For Each Item As RepeaterItem In rptPlant1.Items
                    If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                    Dim chk As CheckBox = Item.FindControl("chk")
                    DT.DefaultView.RowFilter = "PLANT_ID=" & chk.Attributes("PLANT_ID")
                    chk.Checked = DT.DefaultView.Count > 0
                Next
                For Each Item As RepeaterItem In rptPlant2.Items
                    If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                    Dim chk As CheckBox = Item.FindControl("chk")
                    DT.DefaultView.RowFilter = "PLANT_ID=" & chk.Attributes("PLANT_ID")
                    chk.Checked = DT.DefaultView.Count > 0
                Next
                '-----------------User Skill ------------------
                SQL = "SELECT S_Stationary,S_Rotating,S_LubeOil,S_PdMA,S_Thermography FROM MS_User_Skill WHERE USER_ID=" & UserID
                DA = New SqlDataAdapter(SQL, BL.ConnStr)
                DT = New DataTable
                DA.Fill(DT)

                If DT.Rows.Count > 0 Then
                    chkStationary.Checked = Not IsDBNull(DT.Rows(0).Item("S_Stationary")) AndAlso DT.Rows(0).Item("S_Stationary")
                    chkRotating.Checked = Not IsDBNull(DT.Rows(0).Item("S_Rotating")) AndAlso DT.Rows(0).Item("S_Rotating")
                    chkLubeOil.Checked = Not IsDBNull(DT.Rows(0).Item("S_LubeOil")) AndAlso DT.Rows(0).Item("S_LubeOil")
                    chkPdMA.Checked = Not IsDBNull(DT.Rows(0).Item("S_PdMA")) AndAlso DT.Rows(0).Item("S_PdMA")
                    chkThermography.Checked = Not IsDBNull(DT.Rows(0).Item("S_Thermography")) AndAlso DT.Rows(0).Item("S_Thermography")
                End If
                '-----------------แก้ตรงนี้ ------------------

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_USER Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  USER_ID=" & UserID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindUser()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select

    End Sub

    Protected Sub ResetUser(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindUser()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        ClearPanelSearch()

        pnlListUser.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""

        txtPrefix.Text = ""
        txtPrefix.Attributes("UserID") = "-1"
        txtName.Text = ""
        txtSurname.Text = ""

        ddl_Edit_Gender.SelectedIndex = 0
        BL.BindDDlUserPosition(ddl_Edit_Position)
        BL.BindDDlUserLevel(ddl_Edit_Level)

        '------------- Clear Plant ------------
        Dim DT As DataTable = BL.Get_Plant_List
        Dim DT1 As DataTable = DT.Copy
        Dim DT2 As DataTable = DT.Copy
        Dim TotalRecord As Integer = DT.Rows.Count
        For i As Integer = 1 To Math.Floor(TotalRecord / 2)
            DT1.Rows(DT1.Rows.Count - 1).Delete()
            DT1.AcceptChanges()
        Next
        For i As Integer = 1 To Math.Ceiling(TotalRecord / 2)
            DT2.Rows(0).Delete()
            DT2.AcceptChanges()
        Next
        rptPlant1.DataSource = DT1
        rptPlant1.DataBind()
        rptPlant2.DataSource = DT2
        rptPlant2.DataBind()

        '-----------------User Skill ------------------
        chkStationary.Checked = False
        chkRotating.Checked = False
        chkLubeOil.Checked = False
        chkPdMA.Checked = False
        chk_Search_Thermography.Checked = False
        '-----------------แก้ตรงนี้ ------------------

        txt_Login.Text = ""
        txt_Password.Text = ""
        txt_Confirm.Text = ""

        chkAvailable.Checked = True
        btnCreate.Visible = True

        ''-------------InStall Javascript------------
        ClearPassword()
    End Sub

    Protected Sub rptPlant_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlant1.ItemDataBound, rptPlant2.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim chk As CheckBox = e.Item.FindControl("chk")
        Dim lbl As Label = e.Item.FindControl("lbl")
        chk.Attributes("PLANT_ID") = e.Item.DataItem("PLANT_ID")
        lbl.Text = e.Item.DataItem("PLANT_CODE")
    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()

        BL.BindDDlUserLevel(ddl_Search_Level, True)
        BL.BindDDlPlant(ddl_Search_Plant)
        BL.BindDDlUserPosition(ddl_Search_Position)

        ddl_Search_Level.Visible = True
        ddl_Search_Plant.Visible = True
        ddl_Search_Position.Visible = True

        BindUser()
    End Sub

    '--------------------เพิ่ม Skill---------------
    Protected Sub ddl_Search_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged, ddl_Search_Position.SelectedIndexChanged, ddl_Search_Level.SelectedIndexChanged, chk_Search_Stationary.CheckedChanged, chk_Search_Rotating.CheckedChanged, chkSeeAll.CheckedChanged
        BindUser()
    End Sub

#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        txtPrefix.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListUser.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtName.Text = "" Or txtSurname.Text = "" Then
            lblValidation.Text = "Please insert Fullname"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Gender.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Gender"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Level.SelectedIndex = 0 Then
            lblValidation.Text = "Please select User Level"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Position.SelectedIndex = 0 Then
            lblValidation.Text = "Please select User Position"
            pnlValidation.Visible = True
            Exit Sub
        End If

        'If pnlEditPlant.Visible And ddl_Edit_Plant.SelectedIndex = 0 Then
        '    lblValidation.Text = "Please select Plant"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        'If pnlEditRoute.Visible And ddl_Edit_Route.SelectedIndex = 0 Then
        '    lblValidation.Text = "Please select Route"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        If txt_Login.Text.Length < 4 Then
            lblValidation.Text = "Please insert User Login minimun 4 charecters"
            pnlValidation.Visible = True
            Exit Sub
        End If

        '----------------- Check Password --------------
        If lblUpdateMode.Text = "Create" Or txt_Password.Text <> "" Or txt_Confirm.Text <> "" Then

            If txt_Password.Text <> txt_Confirm.Text Then
                lblValidation.Text = "Please matched confirm the same password."
                pnlValidation.Visible = True
                Exit Sub
            End If

            If txt_Password.Text.Length < 4 Then
                lblValidation.Text = "Please insert User Password minimun 4 charecters"
                pnlValidation.Visible = True
                Exit Sub
            End If

        End If




        Dim UserID As Integer = txtPrefix.Attributes("UserID")

        Dim SQL As String = "SELECT * FROM MS_USER WHERE USER_Login='" & txt_Login.Text.Replace("'", "''") & "' AND USER_ID<>" & UserID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Login Name is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_USER WHERE USER_ID=" & UserID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            UserID = GetNewUserID()
            DR("USER_ID") = UserID
        Else
            DR = DT.Rows(0)
        End If

        DR("User_Prefix") = txtPrefix.Text
        DR("User_Name") = txtName.Text
        DR("User_Surname") = txtSurname.Text
        DR("Gender") = ddl_Edit_Gender.Items(ddl_Edit_Gender.SelectedIndex).Value
        DR("POS_ID") = ddl_Edit_Position.Items(ddl_Edit_Position.SelectedIndex).Value
        DR("USER_Login") = txt_Login.Text

        If txt_Password.Text <> "" Then
            DR("USER_Password") = MD5(txt_Password.Text)
        End If

        DR("LEVEL_ID") = ddl_Edit_Level.Items(ddl_Edit_Level.SelectedIndex).Value
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        '------------Update Coverage Area ---------
        Dim Command As New SqlCommand
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Command
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM MS_User_Coverage WHERE USER_ID=" & UserID
            .ExecuteNonQuery()
            .Dispose()
        End With
        SQL = "SELECT * FROM MS_User_Coverage WHERE 1=0"
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, Conn)
        DA.Fill(DT)
        cmd = New SqlCommandBuilder(DA)
        For Each Item As RepeaterItem In rptPlant1.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim chk As CheckBox = Item.FindControl("chk")
            If chk.Checked Then
                Dim P As DataRow = DT.NewRow
                P("USER_ID") = UserID
                P("PLANT_ID") = chk.Attributes("PLANT_ID")
                P("Update_By") = Session("USER_ID")
                P("Update_Time") = Now
                DT.Rows.Add(P)
                DA.Update(DT)
                DT.AcceptChanges()
            End If
        Next
        For Each Item As RepeaterItem In rptPlant2.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim chk As CheckBox = Item.FindControl("chk")
            If chk.Checked Then
                Dim P As DataRow = DT.NewRow
                P("USER_ID") = UserID
                P("PLANT_ID") = chk.Attributes("PLANT_ID")
                P("Update_By") = Session("USER_ID")
                P("Update_Time") = Now
                DT.Rows.Add(P)
                DA.Update(DT)
                DT.AcceptChanges()
            End If
        Next
        '------------Update User Skill ---------
        SQL = "SELECT * FROM MS_User_Skill WHERE USER_ID=" & UserID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, Conn)
        DA.Fill(DT)
        Dim S As DataRow
        If DT.Rows.Count = 0 Then
            S = DT.NewRow
            S("USER_ID") = UserID
        Else
            S = DT.Rows(0)
        End If
        S("S_Stationary") = chkStationary.Checked
        S("S_Rotating") = chkRotating.Checked
        S("S_LubeOil") = chkLubeOil.Checked
        S("S_PdMA") = chkPdMA.Checked
        S("S_Thermography") = chkThermography.Checked
        If DT.Rows.Count = 0 Then DT.Rows.Add(S)
        cmd = New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            'lblValidation.Text = "Invalid parameter"
            'pnlValidation.Visible = True
            'Exit Sub
        End Try
        '-----------------แก้ข้างบน ------------------


        '--------------- Finish -------------------

        ResetUser(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetNewUserID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(USER_ID),0)+1 FROM MS_USER "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function
End Class