﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Rotating_OffRoutine_Edit4.aspx.vb" Inherits="EIR.Rotating_OffRoutine_Edit4" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     
<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>
	    
			<h2>Edit Rotating Off-Routine Report</h2>
			<asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" style="visibility:hidden;" />
            <div class="clear"></div> <!-- End .clear -->
		
		
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server">Tag Status</asp:LinkButton></li>						
						<li><asp:LinkButton id="HTabPhoto" runat="server">Photography Report</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabVibration" runat="server" CssClass="default-tab current">Vibration Measurement</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabDocument" runat="server">Reference Document</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p style="font-weight:bold;">
								<label class="column-left" style="width:120px;" >Report for: </label>
								<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
								<asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Tag <asp:Label ID="lbl_TAG" runat="server" Text="Tag" CssClass="EditReportHeader"></asp:Label>
								</p>
								
								<ul class="shortcut-buttons-set">
							      <li>
							      <asp:LinkButton ID="lnkUpload" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/image_add_48.png" alt="icon" /><br />
							            Upload
							          </span>
							      </asp:LinkButton>
							      </li>
							      <li>
							      <asp:LinkButton ID="lnkPaste" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/Paste.png" alt="icon" /><br />
							            Paste
							          </span>
							      </asp:LinkButton>
							      </li>
							      <li>
							        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear
							          </span>
							        </asp:LinkButton>
							          <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all inspection for this report permanently?">
                                      </cc1:ConfirmButtonExtender>
							      </li>
								  <li>
								    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" width="48" height="48" /><br />
									    Reset this tab
									    </span>
								    </asp:LinkButton>
								  </li>
								 
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								  </li>
					        </ul>
								
							   
                                 
                                    <table >
                                      <tr><td>&nbsp;</td></tr>
                                        <tr>
                                          <td style="text-align:center;"><h3>Vibration Measurement</h3></td>
                                        </tr>
                                     
                                       <tr>
                                         <td style="text-align:center;" valign="top">      
                                            <br>                                      
                                             <asp:ImageButton ID="imgPreview" runat="server" CommandName="UploadImage" 
                                                 ImageUrl="~/resources/images/Sample_40.png" Width="600px" />
                                             <asp:button ID="btnSaveImage" runat="server" Width="0px" Height="0px" style="visibility:hidden; position:absolute;" />
                                         </td>
                                       </tr>                                       
                                    </table>
                                
							<p align="center">
								<asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
								<asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
							</p>
								
							
								
							</fieldset>
				    </div>
				  <!-- End #tabDetail -->        
		          
		       	 <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
		         
	              
			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			

</ContentTemplate>
</asp:UpdatePanel>  	
</asp:Content>
