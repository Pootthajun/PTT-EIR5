﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="Dashboard_Improvement_Report_Export.aspx.vb" Inherits="EIR.Dashboard_Improvement_Report_Export" %>
<%@ Register src="UC_Dashboard_Improvement_Report.ascx" tagname="UC_Dashboard_Improvement_Report" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body onload="window.print();">
    <form id="form1" runat="server">
    <table align="center" >
        <tr valign="middle">
            <td>
                <uc1:UC_Dashboard_Improvement_Report ID="UC_Dashboard_Improvement_Report" runat="server" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
