// JavaScript Document

	
function ShowPreviewReport(y,n)
{
    //window.location.href = 'GL_Report.aspx?RPT_Year=' + y + '&RPT_No=' + n;
    window.open('GL_Report.aspx?RPT_Year=' + y + '&RPT_No=' + n);
}

function ShowPreviewReport_TA(y, n) {
    window.location.href = 'GL_ST_TA_Report.aspx?RPT_Year=' + y + '&RPT_No=' + n;
}

function ShowPreviewReportWithParam(p) {
    //window.location.href = 'GL_Report.aspx?' + p;
    window.open('GL_Report.aspx?' + p);
}

function ShowPreviewLOTag(y,n,d)
{
    window.location.href = 'GL_Report.aspx?RPT_Year=' + y + '&RPT_No=' + n + '&Detail_ID=' + d;
	}
	
function ShowDialogEditImage(UNIQUE_ID,IMAGE_ID,ButtonRefreshControl) //UNIQUE_ID=DailogUpdateImage UNIQUEID, Image_Id=1,2 ,
{
    window.open("GL_ImageEditor.aspx?IMAGE_ID=" + IMAGE_ID + "&UNIQUE_POPUP_ID=" + UNIQUE_ID + '&btn=' + ButtonRefreshControl.id , UNIQUE_ID + '_' + IMAGE_ID, "height=600,width=880");
}

function ShowDialogEditSVG(UNIQUE_POPUP_ID, CancelButton, OKButton, InputTextbox, DisplayTitle, DisplayType, DisplayDesc, DisplayTag, ReadOnly, t)
{
    var w = window.open("SVGEditor/GL_DialogCanvasEditor.aspx?UNIQUE_POPUP_ID=" + UNIQUE_POPUP_ID + "&CancelButton=" + CancelButton + '&OKButton=' + OKButton + "&InputTextbox=" + InputTextbox + "&DisplayTitle=" + DisplayTitle + "&DisplayType=" + DisplayType + "&DisplayDesc=" + DisplayDesc + "&DisplayTag=" + DisplayTag + "&_ReadOnly=" + ReadOnly + "&t=" + t, UNIQUE_POPUP_ID, "fullscreen='yes',height=" + $(window).height() + ",width=" + $(window).width() + ",scrollbars=no,resizable=no,titlebar=no,location=no");
  w.focus();
}

function ShowDialogEditDoc(UNIQUE_POPUP_ID, OKButton, DisplayTitle, DisplayType, DisplayDesc, DisplayTag, ReadOnly, t) {
    var w = window.open("DocumentManager/GL_DialogDocumentUploader.aspx?UNIQUE_POPUP_ID=" + UNIQUE_POPUP_ID + '&OKButton=' + OKButton + "&DisplayTitle=" + DisplayTitle + "&DisplayType=" + DisplayType + "&DisplayDesc=" + DisplayDesc + "&DisplayTag=" + DisplayTag + "&_ReadOnly=" + ReadOnly + "&t=" + t, UNIQUE_POPUP_ID, "fullscreen='yes',height=" + $(window).height() + ",width=" + $(window).width() + ",scrollbars=no,resizable=no,titlebar=no,location=no");
    w.focus();
}


function openPrintWindow(url, width, height) {
    window.open(url, "", "height=" + height + ",width=" + width + ",center=true,scrollbars,resizable");
}

function resize_PIPE_Report_Window() {
    var W = $('.photograph_Sector').parent().width() / 2;
    var H = W;
    $('.File_Zoom_Mask').width(W);
    $('.File_Zoom_Mask').height(H);
    $('.File_Image').width(W);
    $('.File_Image').height(H);
    $('.File_Command').width(W);
    $('.File_Command').css('margin-top', H);
}


function PrepareSummerNoteContent(TXT) {
    TXT = '#' + TXT;
    var rte = $('.note-editable').html();
    rte = html2rte(rte);
    $(TXT).text(rte);
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function rte2html(str) {
    return str.replaceAll("[lt]", "<").replaceAll("[gt]", ">");
}

function html2rte(str) {
    return str.replaceAll("<", "[lt]").replaceAll(">", "[gt]");
}
