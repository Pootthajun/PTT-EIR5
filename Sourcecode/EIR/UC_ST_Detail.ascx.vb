﻿Imports System.Data
Imports System.Data.SqlClient

Public Class UC_ST_Detail
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL

    Public Event Saved(ByRef sender As UC_ST_Detail)
    Public Event CloseRequested(ByRef sender As UC_ST_Detail)

#Region "Dynamic Property"
    Public Property RPT_Year() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_Year")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_Year")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_Year") = value
        End Set
    End Property

    Public Property RPT_No() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_No")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_No")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_No") = value
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If ddl_Tag.SelectedIndex > -1 Then
                Return ddl_Tag.Items(ddl_Tag.SelectedIndex).Value
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            BindTag()
            For i As Integer = 0 To ddl_Tag.Items.Count - 1
                If value = ddl_Tag.Items(i).Value Then
                    ddl_Tag.SelectedIndex = i
                End If
            Next
            ddl_Tag_SelectedIndexChanged(ddl_Tag, Nothing)
        End Set
    End Property

    Public Property INSP_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_INSP.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnINSP As Button = item.FindControl("btnINSP")
                If btnINSP.Font.Underline Then
                    Return btnINSP.CommandArgument
                End If
            Next
            Return 0
        End Get
        Set(ByVal value As Integer)
            'FROM TAG
            BindInspection()
            For Each item As RepeaterItem In rpt_INSP.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnINSP As Button = item.FindControl("btnINSP")
                If btnINSP.CommandArgument = value Then
                    btnINSP.Font.Underline = True
                Else
                    btnINSP.Font.Underline = False
                End If
            Next
            STATUS_ID = 0

            '-------------------Check Require Picture --------------
            Dim RequiredPicture As Boolean = BL.IsInspectionRequirePicture(value)
            TDImage.Visible = RequiredPicture
        End Set
    End Property

    Public Property STATUS_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_STATUS.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnStatus As Button = item.FindControl("btnStatus")
                If btnStatus.Font.Underline Then
                    Return btnStatus.CommandArgument
                End If
            Next
            Return 0
        End Get
        Set(ByVal value As Integer)
            BindStatus()
            For Each item As RepeaterItem In rpt_STATUS.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnStatus As Button = item.FindControl("btnStatus")
                If btnStatus.CommandArgument = value Then
                    btnStatus.Font.Underline = True
                Else
                    btnStatus.Font.Underline = False
                End If
            Next
            LEVEL_ID = LEVEL_ID
        End Set
    End Property

    Public Property LEVEL_ID() As EIR_BL.InspectionLevel
        Get
            For Each item As RepeaterItem In rpt_LEVEL.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnLevel As Button = item.FindControl("btnLevel")
                If btnLevel.Font.Underline Then
                    Return btnLevel.CommandArgument
                End If
            Next
            Return EIR_BL.InspectionLevel.Unknown
        End Get
        Set(ByVal value As EIR_BL.InspectionLevel)
            BindLevel()
            For Each item As RepeaterItem In rpt_LEVEL.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnLevel As Button = item.FindControl("btnLevel")
                If btnLevel.CommandArgument = value Then
                    btnLevel.Font.Underline = True
                Else
                    btnLevel.Font.Underline = False
                End If
            Next
            '--------Update Color---------
            UpdateSelectedColor()
        End Set
    End Property

    Private Sub UpdateSelectedColor()

        Dim CSS As String = "LevelDeselect"
        CSS = BL.Get_Inspection_Css_Box_By_Level(LEVEL_ID)

        '--------------Inspection---------------
        For Each item As RepeaterItem In rpt_INSP.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnINSP As Button = item.FindControl("btnINSP")
            If btnINSP.Font.Underline Then
                btnINSP.CssClass = CSS
            Else
                btnINSP.CssClass = "LevelDeselect"
            End If
        Next
        '--------------Status---------------
        For Each item As RepeaterItem In rpt_STATUS.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnStatus As Button = item.FindControl("btnStatus")
            If btnStatus.Font.Underline Then
                btnStatus.CssClass = CSS
            Else
                btnStatus.CssClass = "LevelDeselect"
            End If
        Next
        '---------------Level---------------
        For Each item As RepeaterItem In rpt_LEVEL.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnLevel As Button = item.FindControl("btnLevel")
            If btnLevel.Font.Underline Then
                btnLevel.CssClass = CSS
            Else
                btnLevel.CssClass = "LevelDeselect"
            End If
        Next
    End Sub

    Public Property DETAIL_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("DETAIL_ID")) Then
                Return 0
            Else
                Return Me.Attributes("DETAIL_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("DETAIL_ID") = value
        End Set
    End Property

    Public Property LAST_DETAIL_ID() As Integer
        Get
            If ddl_Last_Detail.SelectedIndex > -1 Then
                Return ddl_Last_Detail.Items(ddl_Last_Detail.SelectedIndex).Value
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            BindLastDetail()
            For i As Integer = 0 To ddl_Last_Detail.Items.Count - 1
                If value = ddl_Last_Detail.Items(i).Value Then
                    ddl_Last_Detail.SelectedIndex = i
                    Exit For
                End If
            Next
            ddl_Last_Detail_SelectedIndexChanged(ddl_Last_Detail, Nothing)
        End Set
    End Property

    Public Property INSP_Detail() As String
        Get
            Return txt_Detail.Text
        End Get
        Set(ByVal value As String)
            txt_Detail.Text = value
        End Set
    End Property

    Public Property INSP_Recomment() As String
        Get
            Return txt_Recomment.Text
        End Get
        Set(ByVal value As String)
            txt_Recomment.Text = value
        End Set
    End Property

    Public Property Fixed() As TriState
        Get
            Select Case True
                Case Equals(btnSolveYes.BackColor, Drawing.Color.Green)
                    Return TriState.True
                Case Equals(btnSolveNo.BackColor, Drawing.Color.OrangeRed)
                    Return TriState.False
                Case Else
                    Return TriState.UseDefault
            End Select
        End Get
        Set(ByVal value As TriState)
            Select Case value
                Case TriState.UseDefault
                    btnSolveYes.BackColor = Drawing.Color.White
                    btnSolveYes.ForeColor = Drawing.Color.Black
                    btnSolveNo.BackColor = Drawing.Color.White
                    btnSolveNo.ForeColor = Drawing.Color.Black
                Case TriState.True
                    btnSolveYes.BackColor = Drawing.Color.Green
                    btnSolveYes.ForeColor = Drawing.Color.White
                    btnSolveNo.BackColor = Drawing.Color.White
                    btnSolveNo.ForeColor = Drawing.Color.Black
                Case TriState.False
                    btnSolveYes.BackColor = Drawing.Color.White
                    btnSolveYes.ForeColor = Drawing.Color.Black
                    btnSolveNo.BackColor = Drawing.Color.OrangeRed
                    btnSolveNo.ForeColor = Drawing.Color.White
            End Select
        End Set
    End Property

    Public Property MY_PREVIEW1() As Byte()
        Get
            If BL.IsInspectionRequirePicture(INSP_ID) Then
                Return Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1")
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Byte())
            Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1") = value
            ImgPreview1.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=1&Class=" & EIR_BL.Tag_Class.Stationary
        End Set
    End Property

    Public Property MY_PREVIEW2() As Byte()
        Get
            If BL.IsInspectionRequirePicture(INSP_ID) Then
                Return Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2")
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Byte())
            If BL.IsInspectionRequirePicture(INSP_ID) Then
                Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2") = value
                ImgPreview2.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=2&Class=" & EIR_BL.Tag_Class.Stationary
            Else
                Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2") = Nothing
            End If
        End Set
    End Property

    Protected Sub btnRefreshImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshImage.Click
        If Not IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_1")) Then
            MY_PREVIEW1 = Session("TempImage_" & UNIQUE_POPUP_ID & "_1")
            Session("TempImage_" & UNIQUE_POPUP_ID & "_1") = Nothing
        End If

        If Not IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_2")) Then
            MY_PREVIEW2 = Session("TempImage_" & UNIQUE_POPUP_ID & "_2")
            Session("TempImage_" & UNIQUE_POPUP_ID & "_2") = Nothing
        End If

        ImgPreview1.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=1&Class=" & EIR_BL.Tag_Class.Stationary
        ImgPreview2.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=2&Class=" & EIR_BL.Tag_Class.Stationary
    End Sub

    Public Property Disabled() As Boolean
        Get
            Return Not btnSave.Visible
        End Get
        Set(ByVal value As Boolean)
            btnRefreshImage.Visible = Not value
            ddl_Tag.Enabled = Not value
            pnl_Level.Enabled = Not value
            pnl_Inspection.Enabled = Not value
            ddl_Last_Detail.Enabled = Not value
            pnl_Status.Enabled = Not value

            If Not value Then
                ImgPreview1.Attributes("OnClick") = "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "',1,document.getElementById('" & btnRefreshImage.ClientID & "'));"
                ImgPreview2.Attributes("OnClick") = "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "',2,document.getElementById('" & btnRefreshImage.ClientID & "'));"
                ImgPreview1.ToolTip = "Click to edit picture"
                ImgPreview2.ToolTip = "Click to edit picture"
            Else
                ImgPreview1.Attributes("OnClick") = "window.open(this.src);"
                ImgPreview2.Attributes("OnClick") = "window.open(this.src);"
                ImgPreview1.ToolTip = "Click to view picture"
                ImgPreview2.ToolTip = "Click to view picture"
            End If

            txt_Detail.ReadOnly = value
            txt_Recomment.ReadOnly = value
            btnSave.Visible = Not value
        End Set
    End Property

    Public Property SaveButton As Boolean
        Get
            Return pnlAction.Visible And btnSave.Visible
        End Get
        Set(value As Boolean)
            btnSave.Visible = value
            pnlAction.Visible = btnSave.Visible And btnClose.Visible
        End Set
    End Property

    Public Property CloseButton As Boolean
        Get
            Return pnlAction.Visible And btnClose.Visible
        End Get
        Set(value As Boolean)
            btnClose.Visible = value
            pnlAction.Visible = btnSave.Visible And btnClose.Visible
        End Set
    End Property
#End Region

#Region "Static Property"
    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return Me.Attributes("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            If value <> UNIQUE_POPUP_ID Then
                MY_PREVIEW1 = Nothing
                MY_PREVIEW2 = Nothing
            End If
            Me.Attributes("UNIQUE_POPUP_ID") = value
        End Set
    End Property
#End Region


    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        RaiseEvent CloseRequested(Me)
    End Sub


    Public Sub ShowDialog(ByVal Init_RPT_Year As Integer,
                          ByVal Init_RPT_No As Integer,
                          Optional ByVal Init_TAG_ID As Integer = 0,
                          Optional ByVal Init_INSP_ID As Integer = 0) ' For Case Add New

        RPT_Year = Init_RPT_Year
        RPT_No = Init_RPT_No
        TAG_ID = Init_TAG_ID


        INSP_ID = Init_INSP_ID

        LEVEL_ID = -1
        STATUS_ID = 0

        INSP_Detail = ""
        INSP_Recomment = ""
        Fixed = TriState.UseDefault ' And Hidden/ Cloak

        DETAIL_ID = 0
        LAST_DETAIL_ID = 0

        Disabled = False
        '------------ Fix Tag ------------
        If Init_TAG_ID <> 0 And TAG_ID = Init_TAG_ID Then
            ddl_Tag.Enabled = False
        End If

        Me.Visible = True

    End Sub

    Public Sub ShowDialog(ByVal DETAIL_ID As Integer) ' For Case Edit

        Me.DETAIL_ID = 0

        Dim SQL As String = "SELECT * FROM RPT_ST_DETAIL WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to collect information for this inspection point');", True)
            RaiseEvent CloseRequested(Me)
            Exit Sub
        End If

        Me.DETAIL_ID = DT.Rows(0).Item("DETAIL_ID")
        RPT_Year = DT.Rows(0).Item("RPT_Year")
        RPT_No = DT.Rows(0).Item("RPT_No")
        TAG_ID = DT.Rows(0).Item("TAG_ID")
        INSP_ID = DT.Rows(0).Item("INSP_ID")
        If Not IsDBNull(DT.Rows(0).Item("STATUS_ID")) Then
            STATUS_ID = DT.Rows(0).Item("STATUS_ID")
        End If
        If Not IsDBNull(DT.Rows(0).Item("ICLS_ID")) Then
            LEVEL_ID = DT.Rows(0).Item("ICLS_ID")
        End If
        If Not IsDBNull(DT.Rows(0).Item("LAST_DETAIL_ID")) Then
            LAST_DETAIL_ID = DT.Rows(0).Item("LAST_DETAIL_ID")
        End If
        If Not IsDBNull(DT.Rows(0).Item("PROB_Detail")) Then
            INSP_Detail = DT.Rows(0).Item("PROB_Detail")
        End If
        If Not IsDBNull(DT.Rows(0).Item("PROB_Recomment")) Then
            INSP_Recomment = DT.Rows(0).Item("PROB_Recomment")
        End If
        txtUpdateBy.Text = DT.Rows(0).Item("Update_Name").ToString
        If Not IsDBNull(DT.Rows(0).Item("Fixed")) Then
            If DT.Rows(0).Item("Fixed") Then
                Fixed = TriState.True
            Else
                Fixed = TriState.False
            End If
        Else
            Fixed = TriState.UseDefault
        End If

        Disabled = False
        Me.Visible = True

        If DT.Rows(0).Item("Create_Flag") = "Main" Then DisableMainInformation()

    End Sub

    Private Sub DisableMainInformation()
        ddl_Tag.Enabled = False
        ddl_Last_Detail.Enabled = False
        pnl_Inspection.Enabled = False
    End Sub

    Private Sub FindLastDetailPicture()
        Dim SQL As String = "EXEC dbo.SP_Get_Previous_Tag_Inspection_Pictures " & RPT_Year & "," & RPT_No & "," & TAG_ID & "," & INSP_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            MY_PREVIEW1 = Nothing
            MY_PREVIEW2 = Nothing
        Else
            If Not IsDBNull(DT.Rows(0).Item("PIC_Detail1")) Then MY_PREVIEW1 = DT.Rows(0).Item("PIC_Detail1")
            If Not IsDBNull(DT.Rows(0).Item("PIC_Detail2")) Then MY_PREVIEW2 = DT.Rows(0).Item("PIC_Detail2")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlValidation.Visible = False
    End Sub

    Private Sub BindTag()
        Dim SQL As String = "SELECT TAG_ID,TAG_CODE FROM " & vbNewLine
        SQL &= " VW_ALL_ACTIVE_ST_TAG TAG" & vbNewLine
        SQL &= " WHERE TAG.ROUTE_ID=(SELECT ROUTE_ID FROM RPT_ST_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & ")" & vbNewLine
        SQL &= " ORDER BY TAG_CODE" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl_Tag.Items.Clear()
        ddl_Tag.Items.Add(New ListItem("", 0))
        ddl_Tag.SelectedIndex = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TAG_Code"), DT.Rows(i).Item("TAG_ID"))
            ddl_Tag.Items.Add(Item)
        Next
    End Sub

    Private Sub BindInspection()
        Dim SQL As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbNewLine
        SQL &= " DECLARE @RPT_No As INT =" & RPT_No & vbNewLine
        SQL &= " DECLARE @TAG_ID As INT =" & TAG_ID & vbNewLine

        SQL &= " SELECT DISTINCT * FROM " & vbNewLine
        SQL &= " (" & vbNewLine
        SQL &= "    SELECT DISTINCT INSP.INSP_ID,DNSP.INSP_Name" & vbNewLine
        SQL &= "    FROM MS_ST_TAG " & vbNewLine
        SQL &= "    LEFT JOIN MS_ST_TAG_Inspection INSP ON MS_ST_TAG.TAG_TYPE_ID=INSP.TAG_TYPE_ID" & vbNewLine
        SQL &= "    LEFT JOIN MS_ST_Default_Inspection DNSP ON INSP.INSP_ID=DNSP.INSP_ID" & vbNewLine
        SQL &= "    WHERE TAG_ID = @TAG_ID " & vbNewLine
        SQL &= "    AND (" & vbNewLine
        SQL &= "    INSP.REF_INSP_ID IS NULL OR NOT EXISTS" & vbNewLine
        SQL &= "    (SELECT DETAIL_ID FROM RPT_ST_Detail WHERE RPT_Year=@RPT_Year AND RPT_No=@RPT_No AND TAG_ID=@TAG_ID" & vbNewLine
        SQL &= "    AND INSP_ID=INSP.REF_INSP_ID AND ISNULL(STATUS_ID,0)<>INSP.REF_STATUS_ID)" & vbNewLine
        SQL &= "    )" & vbNewLine
        SQL &= "    UNION ALL " & vbNewLine
        SQL &= "    ("
        SQL &= "    SELECT DISTINCT VW.INSP_ID,VW.INSP_Name" & vbNewLine
        SQL &= "    FROM VW_REPORT_ST_DETAIL VW" & vbNewLine
        SQL &= "    WHERE CURRENT_LEVEL IS NOT NULL AND RPT_Year=@RPT_Year AND RPT_No=@RPT_No AND TAG_ID=@TAG_ID" & vbNewLine
        SQL &= "    )" & vbNewLine
        SQL &= " ) INSP" & vbNewLine
        SQL &= "    ORDER BY INSP_ID" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rpt_INSP.DataSource = DT
        rpt_INSP.DataBind()
    End Sub

    Private Sub BindStatus()
        Dim SQL As String = "DECLARE @TAG_ID As INT =" & TAG_ID & vbNewLine
        SQL &= " DECLARE @INSP_ID As INT =" & INSP_ID & vbNewLine
        SQL &= " SELECT DISTINCT IST.STATUS_ID,IST.STATUS_Name, IST.STATUS_Order" & vbNewLine
        SQL &= " FROM MS_ST_TAG " & vbNewLine
        SQL &= " LEFT JOIN MS_ST_TAG_Inspection INSP ON MS_ST_TAG.TAG_TYPE_ID=INSP.TAG_TYPE_ID" & vbNewLine
        SQL &= " LEFT JOIN MS_ST_Default_Inspection_Status IST ON INSP.STATUS_ID=IST.STATUS_ID" & vbNewLine
        SQL &= " WHERE TAG_ID = @TAG_ID AND INSP_ID=@INSP_ID" & vbNewLine
        SQL &= " ORDER BY IST.STATUS_Order" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rpt_STATUS.DataSource = DT
        rpt_STATUS.DataBind()
    End Sub

    Private Sub BindLevel()

        Dim SQL As String = "SELECT * FROM ISPT_Class"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rpt_LEVEL.DataSource = DT
        rpt_LEVEL.DataBind()

    End Sub

    Private Sub BindLastDetail()
        Dim SQL As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbNewLine
        SQL &= " DECLARE @RPT_No As INT =" & RPT_No & vbNewLine
        SQL &= " DECLARE @TAG_ID As INT =" & TAG_ID & vbNewLine
        SQL &= " DECLARE @INSP_ID As INT =" & INSP_ID & vbNewLine

        SQL &= " SELECT DETAIL_ID,RPT_CODE,INSP_Name,ISNULL(CURRENT_STATUS_Name,'') STATUS_Name,CURRENT_LEVEL " & vbNewLine
        SQL &= " FROM VW_REPORT_ST_DETAIL VW" & vbNewLine
        SQL &= " WHERE dbo.UDF_RPT_Code(RPT_Year,RPT_No)=" & vbNewLine
        SQL &= " (	SELECT MAX(dbo.UDF_RPT_Code(RPT_Year,RPT_No)) " & vbNewLine
        SQL &= " FROM VW_REPORT_ST_DETAIL " & vbNewLine
        SQL &= " WHERE TAG_ID=@TAG_ID AND dbo.UDF_RPT_Code(RPT_Year,RPT_No)<dbo.UDF_RPT_Code(@RPT_Year,@RPT_No)" & vbNewLine
        SQL &= " ) AND INSP_ID=@INSP_ID AND TAG_ID=@TAG_ID " & vbNewLine
        SQL &= " ORDER BY STATUS_Name" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl_Last_Detail.Items.Clear()
        ddl_Last_Detail.Items.Add(New ListItem("", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Txt As String = DT.Rows(i).Item("INSP_Name") & " " & DT.Rows(i).Item("STATUS_Name")
            If Not IsDBNull(DT.Rows(i).Item("CURRENT_LEVEL")) Then
                Txt &= " " & BL.Get_Problem_Level_Name(DT.Rows(i).Item("CURRENT_LEVEL"))
            End If
            Dim Item As New ListItem(Txt, DT.Rows(i).Item("DETAIL_ID"))
            ddl_Last_Detail.Items.Add(Item)
        Next
    End Sub

    Protected Sub ddl_Tag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Tag.SelectedIndexChanged
        Dim SQL As String = "SELECT TAG_TYPE_ID,TAG_TYPE_Name FROM "
        SQL &= " VW_ALL_ACTIVE_ST_TAG TAG "
        SQL &= " WHERE TAG.TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 OrElse IsDBNull(DT.Rows(0).Item("TAG_TYPE_Name")) Then
            lbl_TagType.Text = ""
            lbl_TagType.Attributes("TAG_Type_ID") = 0
        Else
            lbl_TagType.Text = "(" & DT.Rows(0).Item("TAG_TYPE_Name") & ")"
            lbl_TagType.Attributes("TAG_Type_ID") = DT.Rows(0).Item("TAG_TYPE_ID")
        End If

        '------------ Relative Item--------
        INSP_ID = 0
    End Sub

    Protected Sub ddl_Last_Detail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Last_Detail.SelectedIndexChanged
        '---------------------------- New Code -------------------------
        MY_PREVIEW1 = BL.Get_ST_Image(RPT_Year, RPT_No, DETAIL_ID, 1)
        MY_PREVIEW2 = BL.Get_ST_Image(RPT_Year, RPT_No, DETAIL_ID, 2)

        If IsNothing(MY_PREVIEW1) Then
            MY_PREVIEW1 = BL.Get_ST_Image(ddl_Last_Detail.Items(ddl_Last_Detail.SelectedIndex).Value, 1)
        End If
        If IsNothing(MY_PREVIEW2) Then
            MY_PREVIEW2 = BL.Get_ST_Image(ddl_Last_Detail.Items(ddl_Last_Detail.SelectedIndex).Value, 2)
        End If

        'Set Last Detail Place Holder for Description and Recomment
        imgDetail.Visible = False
        imgRecomment.Visible = False

        If ddl_Last_Detail.SelectedIndex = 0 Then
            pnlSolved.Visible = False
        Else
            pnlSolved.Visible = True
            Dim DT As New DataTable
            Dim DA As New SqlDataAdapter("SELECT * FROM RPT_ST_DETAIL WHERE Detail_ID=" & ddl_Last_Detail.Items(ddl_Last_Detail.SelectedIndex).Value, BL.ConnStr)
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                imgDetail.Visible = True
                imgRecomment.Visible = True
                imgDetail.ToolTip = DT.Rows(0).Item("PROB_Detail").ToString
                imgRecomment.ToolTip = DT.Rows(0).Item("PROB_Recomment").ToString
            End If
        End If

    End Sub

    Protected Sub lnk_Last_Detail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_Last_Detail.Click

    End Sub

    Protected Sub rpt_INSP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_INSP.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnINSP As Button = e.Item.FindControl("btnINSP")
        btnINSP.Text = e.Item.DataItem("INSP_NAME")
        btnINSP.CssClass = "LevelDeselect"
        btnINSP.CommandArgument = e.Item.DataItem("INSP_ID")
    End Sub

    Protected Sub rpt_INSP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_INSP.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnINSP As Button = e.Item.FindControl("btnINSP")
        If INSP_ID <> btnINSP.CommandArgument Then INSP_ID = btnINSP.CommandArgument
    End Sub

    Protected Sub rpt_STATUS_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_STATUS.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnStatus As Button = e.Item.FindControl("btnStatus")
        btnStatus.Text = e.Item.DataItem("STATUS_Name")
        btnStatus.CssClass = "LevelDeselect"
        btnStatus.CommandArgument = e.Item.DataItem("STATUS_ID")

    End Sub

    Protected Sub rpt_STATUS_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_STATUS.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnStatus As Button = e.Item.FindControl("btnStatus")
        If STATUS_ID <> btnStatus.CommandArgument Then STATUS_ID = btnStatus.CommandArgument
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub rpt_LEVEL_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_LEVEL.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnLevel As Button = e.Item.FindControl("btnLevel")
        btnLevel.CommandArgument = e.Item.DataItem("ICLS_LEVEL")
        btnLevel.Text = e.Item.DataItem("ICLS_Description")
    End Sub

    Protected Sub rpt_LEVEL_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_LEVEL.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnLevel As Button = e.Item.FindControl("btnLevel")
        If LEVEL_ID <> btnLevel.CommandArgument Then LEVEL_ID = btnLevel.CommandArgument
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Save()
    End Sub

    Public Function Save() As Integer

        If TAG_ID < 1 Then
            lblValidation.Text = "Select Tag "
            pnlValidation.Visible = True
            ddl_Tag.Focus()
            Return 0
        End If

        If INSP_ID < 1 Then
            lblValidation.Text = "Select Inspection Point"
            pnlValidation.Visible = True
            ddl_Tag.Focus()
            Return 0
        End If

        If pnlSolved.Visible AndAlso Fixed = TriState.UseDefault Then
            lblValidation.Text = "Select Solve Status"
            pnlValidation.Visible = True
            ddl_Tag.Focus()
            Return 0
        End If

        If STATUS_ID < 1 Then
            lblValidation.Text = "Select Status"
            pnlValidation.Visible = True
            ddl_Tag.Focus()
            Return 0
        End If

        If LEVEL_ID < 0 Then
            lblValidation.Text = "Select level"
            pnlValidation.Visible = True
            ddl_Tag.Focus()
            Return 0
        End If

        If INSP_Detail = "" Then
            lblValidation.Text = "Select insert inspection detail"
            pnlValidation.Visible = True
            ddl_Tag.Focus()
            Return 0
        End If

        If BL.IsInspectionRequirePicture(INSP_ID) Then
            If IsNothing(MY_PREVIEW1) OrElse MY_PREVIEW1.Length < 50 OrElse IsNothing(MY_PREVIEW2) OrElse MY_PREVIEW2.Length < 50 Then
                lblValidation.Text = "Select image"
                pnlValidation.Visible = True
                ddl_Tag.Focus()
                Return 0
            End If
            '-------------- เชครูปซ้ำ -------------------
            If LAST_DETAIL_ID > 0 Then
                Try
                    Dim IMG1 As Byte() = BL.Get_ST_Image(LAST_DETAIL_ID, 1)
                    Dim IMG2 As Byte() = BL.Get_ST_Image(LAST_DETAIL_ID, 2)

                    If Equals(IMG1, MY_PREVIEW1) OrElse Equals(IMG2, MY_PREVIEW2) Then
                        Err.Raise(1, , "Select new images to update information")
                    End If
                Catch ex As Exception
                    lblValidation.Text = "Invalid parameter"
                    pnlValidation.Visible = True
                    ddl_Tag.Focus()
                    Return 0
                End Try
            End If
        End If

        Dim DR As DataRow
        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable
        '----------------------------Check Update Reference---------------------
        If LAST_DETAIL_ID <> 0 Then ' ------------ Reference Old Problem --------
            SQL = "SELECT * FROM RPT_ST_Detail " & vbNewLine
            SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & vbNewLine
            SQL &= " AND LAST_DETAIL_ID=" & LAST_DETAIL_ID & vbNewLine
        ElseIf DETAIL_ID <> 0 Then  ' ------------ Reference Old Info --------
            SQL = "SELECT * FROM RPT_ST_Detail WHERE DETAIL_ID=" & DETAIL_ID & vbNewLine
        Else ' Detail_ID=0 ' ------------ New Info --------

            '---------- กรณีปัญหาใหม่จะไม่ไป Update ใน Main เหมือน Routine อีกแล้ว-------------
            SQL = "SELECT * FROM RPT_ST_Detail " & vbNewLine
            SQL &= "WHERE 1=0"

            '    SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID & " AND ICLS_ID IS NULL" & vbNewLine
            '    '--------- Add Fix Isuue Replacement Problem-----------
            '    SQL &= " AND ISNULL(LAST_DETAIL_ID,0)=0"
        End If

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR("Detail_ID") = BL.Get_New_ST_DetailID()
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("TAG_ID") = TAG_ID
            DR("TAG_TYPE_ID") = lbl_TagType.Attributes("TAG_Type_ID")
            DR("Create_Flag") = "New"
        Else
            DR = DT.Rows(0)
        End If

        '-----------------------------Update Detail------------------------
        DR("INSP_ID") = INSP_ID
        DR("STATUS_ID") = STATUS_ID
        DR("ICLS_ID") = LEVEL_ID

        If pnlSolved.Visible Then
            Select Case Fixed
                Case TriState.True
                    DR("Fixed") = True
                Case TriState.False
                    DR("Fixed") = False
                Case Else
                    DR("Fixed") = DBNull.Value
            End Select
        Else
            DR("Fixed") = DBNull.Value
        End If

        DR("PROB_Detail") = INSP_Detail
        DR("PROB_Recomment") = INSP_Recomment
        DR("LAST_DETAIL_ID") = LAST_DETAIL_ID
        DR("Update_By") = Session("USER_ID")
        DR("Update_Name") = Session("USER_Full_Name")
        DR("Update_Time") = Now
        '-------- Save Picture ---------
        Dim Path As String = BL.Picture_Path & "\" & RPT_Year & "\" & RPT_No & "\" & DR("DETAIL_ID") & "_"
        If Not IsNothing(MY_PREVIEW1) Then
            If BL.Save_Picture_File(MY_PREVIEW1, RPT_Year, RPT_No, DR("DETAIL_ID"), 1) Then
                DR("Pic_Detail1") = True
            Else
                DR("Pic_Detail1") = False
                lblValidation.Text = "Unable to save left file"
                pnlValidation.Visible = True
                ddl_Tag.Focus()
                Return 0
            End If
        Else
            BL.Save_Picture_File(Nothing, RPT_Year, RPT_No, DR("DETAIL_ID"), 1) '---------Force Delete ---------
            DR("Pic_Detail1") = False
        End If
        If Not IsNothing(MY_PREVIEW2) Then
            If BL.Save_Picture_File(MY_PREVIEW2, RPT_Year, RPT_No, DR("DETAIL_ID"), 2) Then
                DR("Pic_Detail2") = True
            Else
                DR("Pic_Detail2") = False
                lblValidation.Text = "Unable to save right file"
                pnlValidation.Visible = True
                ddl_Tag.Focus()
                Return 0
            End If
        Else
            BL.Save_Picture_File(Nothing, RPT_Year, RPT_No, DR("DETAIL_ID"), 2) '---------Force Delete ---------
            DR("Pic_Detail2") = False
        End If

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            ddl_Tag.Focus()
            Return 0
        End Try

        DETAIL_ID = DR("Detail_ID")
        '---------------------Additional Delete Default TAG Inspection ------------
        Return DETAIL_ID
        RaiseEvent Saved(Me)

    End Function

    Protected Sub btnSolveYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSolveYes.Click
        If Fixed = TriState.True Then
            Fixed = TriState.UseDefault
        Else
            Fixed = TriState.True
        End If
    End Sub

    Protected Sub btnSolveNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSolveNo.Click
        If Fixed = TriState.False Then
            Fixed = TriState.UseDefault
        Else
            Fixed = TriState.False
        End If
    End Sub

End Class