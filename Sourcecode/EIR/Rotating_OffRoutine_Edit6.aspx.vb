﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports iTextSharp.text.pdf
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports PdfToImage
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Public Class Rotating_OffRoutine_Edit6
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Rotating_Off_Routine_Report

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("Rotating_OffRoutine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_RO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND RPT_Type_ID=" & RPT_Type_ID, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_RO_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_RO_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_RO_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")
            BindTabData()

        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_RO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                '.RPT_Type = RPT_Type_ID
                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
            Exit Sub
        End If

        ''---------------- Set Permission Visibility -------------
        'If USER_LEVEL = EIR_BL.User_Level.Administrator Then
        '    btn_Send_Collector.Visible = True
        '    btn_Send_Inspector.Visible = True
        '    btn_Send_Analyst.Visible = True
        '    btn_Posted.Visible = True
        'Else
        '    btn_Send_Collector.Visible = USER_LEVEL = EIR_BL.User_Level.Inspector
        '    btn_Send_Inspector.Visible = (USER_LEVEL = EIR_BL.User_Level.Collector) Or (USER_LEVEL = EIR_BL.User_Level.Analyst)
        '    btn_Send_Analyst.Visible = USER_LEVEL = EIR_BL.User_Level.Inspector
        '    btn_Posted.Visible = USER_LEVEL = EIR_BL.User_Level.Analyst
        'End If
        '---------------- Set Permission Visibility -------------
        If USER_LEVEL = EIR_BL.User_Level.Administrator Then
            btn_Send_Collector.Visible = True
            btn_Send_Inspector.Visible = False
            btn_Send_Analyst.Visible = True
            btn_Posted.Visible = True
        Else
            'btn_Send_Collector.Visible = USER_LEVEL = EIR_BL.User_Level.Inspector
            'btn_Send_Inspector.Visible = (USER_LEVEL = EIR_BL.User_Level.Collector) Or (USER_LEVEL = EIR_BL.User_Level.Analyst)
            'btn_Send_Analyst.Visible = USER_LEVEL = EIR_BL.User_Level.Inspector
            'btn_Posted.Visible = USER_LEVEL = EIR_BL.User_Level.Analyst

            btn_Send_Collector.Visible = USER_LEVEL = EIR_BL.User_Level.Approver
            btn_Send_Inspector.Visible = False
            btn_Send_Analyst.Visible = USER_LEVEL = EIR_BL.User_Level.Collector
            btn_Posted.Visible = USER_LEVEL = EIR_BL.User_Level.Approver
        End If
        pnl_Inspector.Visible = False


        pnl_Collector.Enabled = USER_LEVEL = EIR_BL.User_Level.Collector
        pnl_Inspector.Enabled = USER_LEVEL = EIR_BL.User_Level.Inspector
        pnl_Analyst.Enabled = USER_LEVEL = EIR_BL.User_Level.Approver


        '----------------- Get Issue -----------------
        SQL = "SELECT * FROM RPT_RO_Detail " & vbNewLine
        SQL &= "WHERE ICLS_ID IS NOT NULL AND STATUS_ID IS NOT NULL AND ICLS_ID IS NOT NULL AND" & vbNewLine
        SQL &= "RPT_RO_Detail.RPT_Year = " & RPT_Year & " And RPT_RO_Detail.RPT_No =" & RPT_No & vbNewLine
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            btn_Send_Collector.Enabled = False
            btn_Send_Inspector.Enabled = False
            btn_Send_Analyst.Enabled = False
            btn_Posted.Enabled = False
            Exit Sub
        End If

        '------------ Check Incomplete Inspection -------------
        SQL = " SELECT COUNT(1) Issue" & vbNewLine
        SQL &= " FROM RPT_RO_Detail " & vbNewLine
        SQL &= " LEFT JOIN VW_Not_Require_Inspection_Picture EXC ON RPT_RO_Detail.INSP_ID=EXC.INSP_ID" & vbNewLine
        SQL &= " WHERE RPT_RO_Detail.RPT_Year=" & RPT_Year & " AND RPT_RO_Detail.RPT_No=" & RPT_No & " AND EXC.INSP_ID IS NULL " & vbNewLine
        SQL &= " AND RPT_RO_Detail.ICLS_ID IS NOT NULL AND (isnull(PIC_Detail1,0) = 0 OR isnull(PIC_Detail2,0) = 0)" & vbNewLine

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("Issue")) AndAlso DT.Rows(0).Item("Issue") > 0 Then
            btn_Send_Collector.Enabled = False
            btn_Send_Inspector.Enabled = False
            btn_Send_Analyst.Enabled = False
            btn_Posted.Enabled = False
            Exit Sub
        End If

        '------------ Check Incomplete Document -------------
        SQL = "SELECT COUNT(1) FROM RO_Doc WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND DOC_Detail IS NULL"
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            btn_Send_Collector.Enabled = False
            btn_Send_Inspector.Enabled = False
            btn_Send_Analyst.Enabled = False
            btn_Posted.Enabled = False
            Exit Sub
        End If
    End Sub

    Private Sub BindTabData()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_RO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("Rotating_OffRoutine_Summary.aspx")
            Exit Sub
        End If

        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        lbl_Year.Text = RPT_Year
        '------------------ GET TAG ID------------------------------
        SQL = "SELECT TOP 1 TAG_ID FROM RPT_RO_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("Rotating_OffRoutine_Summary.aspx")
            Exit Sub
        End If
        lbl_TAG.Text = BL.Get_Tag_Code_Rotating(DT.Rows(0).Item("TAG_ID"))
        '------------------------------Header -----------------------------------


        SQL = "SELECT RPT_Result, " & vbNewLine
        SQL &= " RPT_RO_Header.RPT_COL_By,RPT_RO_Header.RPT_COL_Date,RPT_RO_Header.RPT_COL_Comment,COL.USER_Name + ' ' + COL.User_Surname  COL_NAME," & vbNewLine
        SQL &= " RPT_RO_Header.RPT_INSP_By,RPT_RO_Header.RPT_INSP_Date,RPT_RO_Header.RPT_INSP_Comment,INP.USER_Name + ' ' + INP.User_Surname INSP_NAME," & vbNewLine
        SQL &= " RPT_RO_Header.RPT_ANL_By,RPT_RO_Header.RPT_ANL_Date,RPT_RO_Header.RPT_ANL_Comment,ANL.USER_Name + ' ' + ANL.User_Surname ANL_NAME" & vbNewLine
        SQL &= " FROM RPT_RO_Header " & vbNewLine
        SQL &= " LEFT JOIN MS_User COL ON RPT_RO_Header.RPT_COL_By=COL.USER_ID" & vbNewLine
        SQL &= " LEFT JOIN MS_User INP ON RPT_RO_Header.RPT_INSP_By=INP.USER_ID" & vbNewLine
        SQL &= " LEFT JOIN MS_User ANL ON RPT_RO_Header.RPT_ANL_By=ANL.USER_ID" & vbNewLine
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable

        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        If Not IsDBNull(DT.Rows(0).Item("RPT_Result")) Then
            txt_RPT_Result.Text = DT.Rows(0).Item("RPT_Result")
        End If
        '--------- Collector --------
        If Not IsDBNull(DT.Rows(0).Item("COL_NAME")) Then
            txt_RPT_COL_By.Text = DT.Rows(0).Item("COL_NAME")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_COL_Date")) Then
            txt_RPT_COL_Date.Text = DT.Rows(0).Item("RPT_COL_Date")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_COL_Comment")) Then
            txt_RPT_COL_Comment.Text = DT.Rows(0).Item("RPT_COL_Comment")
        End If
        txt_RPT_COL_Comment.ReadOnly = USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Collector

        '--------- Inspector --------
        If Not IsDBNull(DT.Rows(0).Item("INSP_NAME")) Then
            txt_RPT_INSP_By.Text = DT.Rows(0).Item("INSP_NAME")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_INSP_Date")) Then
            txt_RPT_INSP_Date.Text = DT.Rows(0).Item("RPT_INSP_Date")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_INSP_Comment")) Then
            txt_RPT_INSP_Comment.Text = DT.Rows(0).Item("RPT_INSP_Comment")
        End If
        txt_RPT_INSP_Comment.ReadOnly = USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Inspector
        '--------- Approver --------
        If Not IsDBNull(DT.Rows(0).Item("ANL_NAME")) Then
            txt_RPT_ANL_By.Text = DT.Rows(0).Item("ANL_NAME")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_ANL_Date")) Then
            txt_RPT_ANL_Date.Text = DT.Rows(0).Item("RPT_ANL_Date")
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_ANL_Comment")) Then
            txt_RPT_ANL_Comment.Text = DT.Rows(0).Item("RPT_ANL_Comment")
        End If
        txt_RPT_ANL_Comment.ReadOnly = USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Approver

    End Sub

#Region "Navigator"

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDetail.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabPhoto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabPhoto.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabVibration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabVibration.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit4.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDocument.Click, btn_Back.Click
        Save_Summary(False)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit5.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

#End Region

#Region "Saving"
    Private Sub Save_Summary(Optional ByVal ReportSuccess As Boolean = True)

        Dim DT As New DataTable
        Dim SQL As String = "SELECT RPT_Year,RPT_No,RPT_Result,RPT_COL_By,RPT_COL_Date,RPT_COL_Comment,RPT_INSP_By,RPT_INSP_Date,RPT_INSP_Comment,"
        SQL &= " RPT_ANL_By,RPT_ANL_Date,RPT_ANL_Comment,Update_By,Update_Time"
        SQL &= " FROM RPT_RO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to save.\nThis report has been removed!'); window.location.href='Rotating_OffRoutine_Summary.aspx';", True)
            Exit Sub
        End If

        Dim NeedSave As Boolean = False
        '------------------- Save Result----------
        If IsDBNull(DT.Rows(0).Item("RPT_Result")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_Result") = txt_RPT_Result.Text
        Else
            If DT.Rows(0).Item("RPT_Result") <> txt_RPT_Result.Text Then
                NeedSave = True
                DT.Rows(0).Item("RPT_Result") = txt_RPT_Result.Text
            End If
        End If
        '------------------- Save Collector----------
        If IsDBNull(DT.Rows(0).Item("RPT_COL_Comment")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_COL_Comment") = txt_RPT_COL_Comment.Text
        Else
            If DT.Rows(0).Item("RPT_COL_Comment") <> txt_RPT_COL_Comment.Text And USER_LEVEL = EIR_BL.User_Level.Collector Then
                NeedSave = True
                DT.Rows(0).Item("RPT_COL_Comment") = txt_RPT_COL_Comment.Text
            End If
        End If
        '------------------- Save Inspector----------
        If IsDBNull(DT.Rows(0).Item("RPT_INSP_Comment")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_INSP_Comment") = txt_RPT_INSP_Comment.Text
        Else
            If DT.Rows(0).Item("RPT_INSP_Comment") <> txt_RPT_INSP_Comment.Text And USER_LEVEL = EIR_BL.User_Level.Inspector Then
                NeedSave = True
                DT.Rows(0).Item("RPT_INSP_Comment") = txt_RPT_INSP_Comment.Text
            End If
        End If
        '------------------- Save Approver----------
        If IsDBNull(DT.Rows(0).Item("RPT_ANL_Comment")) Then
            NeedSave = True
            DT.Rows(0).Item("RPT_ANL_Comment") = txt_RPT_ANL_Comment.Text
        Else
            If DT.Rows(0).Item("RPT_ANL_Comment") <> txt_RPT_ANL_Comment.Text And USER_LEVEL = EIR_BL.User_Level.Approver Then
                NeedSave = True
                DT.Rows(0).Item("RPT_ANL_Comment") = txt_RPT_ANL_Comment.Text
            End If
        End If

        If Not NeedSave Then
            If ReportSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('There is nothing to be saved!\nThe report does not changed');", True)
            End If
            Exit Sub
        End If

        DT.Rows(0).Item("Update_By") = Session("USER_ID")
        DT.Rows(0).Item("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        If ReportSuccess Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save successfully');", True)
        End If

    End Sub

#End Region

#Region "WorkFlow"

    Protected Sub btn_Send_Collector_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Send_Collector.Click

        Save_Summary(False)

        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE RPT_RO_Header set RPT_LOCK_BY=NULL,RPT_STEP=1,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE()" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Report has been sent to Collector!!'); window.location.href='Rotating_OffRoutine_Summary.aspx';", True)
    End Sub

    Protected Sub btn_Send_Inspector_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Send_Inspector.Click

        Save_Summary(False)

        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE RPT_RO_Header set RPT_LOCK_BY=NULL,RPT_STEP=2,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE()" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Report has been sent to Inspector!!'); window.location.href='Rotating_OffRoutine_Summary.aspx';", True)
    End Sub

    Protected Sub btn_Send_Analyst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Send_Analyst.Click

        Save_Summary(False)

        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE RPT_RO_Header set RPT_LOCK_BY=NULL,RPT_STEP=3,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE()" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Report has been sent to Approver!!'); window.location.href='Rotating_OffRoutine_Summary.aspx';", True)
    End Sub

    Protected Sub btn_Posted_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Posted.Click

        Save_Summary(False)

        Dim DefaultValue As String = Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & (Now.Year + 543).ToString.Substring(2, 2) & "_" & lbl_Plant.Text.Replace("#", "").Replace("_", "") & "_SERVICE_" & lbl_TAG.Text.Replace("#", "").Replace("_", "") & "_"
        DefaultValue &= UCase(Session("USER_Name")) & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "-" & RPT_No.ToString.PadLeft(4, "0") & ".PDF"

        DialogInput.ShowDialog("Please insert finalize report file name..", DefaultValue)

    End Sub

    Protected Sub DialogInput_AnswerDialog(ByVal Result As String) Handles DialogInput.AnswerDialog

        If Result = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Please insert file name to be saved');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If
        If Not BL.IsFormatFileName(Result) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('File name must not contained following excepted charactors /\:*?""<>|;');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT dbo.UDF_RPT_Code(RPT_Year,RPT_No) RPT_Code FROM RPT_RO_Header WHERE Result_FileName='" & Replace(Result, "'", "''") & "' AND RPT_Year<>" & RPT_Year & " AND RPT_No<>" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This file name is already exists. And has been reserved for report " & DT.Rows(0).Item("RPT_Code") & "');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If

        '---------------- Generate Posted Report-----------
        If Result.Length >= 4 AndAlso Right(Result, 4).ToUpper <> ".PDF".ToUpper Then
            Result = Result & ".PDF"
        ElseIf Result.Length < 4 Then
            Result = Result & ".PDF"
        End If

        Dim DestinationPath As String = BL.PostedReport_Path & "\" & Result
        Dim GenerateResult = BL.GeneratePostedReport(RPT_Year, RPT_No, DestinationPath)
        If Not GenerateResult.Success Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PostedReport", "alert('Unavailable to posted report/job !!\n" & GenerateResult.Message.Replace("'", """") & "');", True)
            DialogInput.Visible = False
            Exit Sub
        End If
        DialogInput.Visible = False
        '--------------------- Update Report Status ---------------------------------
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE RPT_RO_Header set RPT_LOCK_BY=NULL,RPT_STEP=4,Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE(),Result_FileName='" & Replace(Result, "'", "''") & "'" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Report has been Approved and job done!!'); ShowPreviewReport(" & RPT_Year & "," & RPT_No & "); window.location.href='Rotating_OffRoutine_Summary.aspx';", True)
    End Sub
#End Region

    Protected Sub btn_Buffer_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Buffer_Refresh.Click
        BindTabData()
    End Sub

    Protected Sub lnkSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        Save_Summary()

    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

#Region "GenerateReport Copy From Hardcode in GL_Report"
    Structure CreateReportResult
        Public Success As Boolean
        Public Message As String
    End Structure

    Public rptFormula As Formula

    Public Structure Formula
        Dim Header As String
        Dim DocDetail As String
        Dim Plant As String
        Dim ReportNo As String
        Dim Collected_By As String
        Dim Collected_Date As String
    End Structure
    Dim PDFListOffRoutine As String() = {}
#End Region

#Region "โคตรเซ็ง เขียนหลายที่เหมือนกันหมด" ' ------------------------------ Remain -------------------------------
    Public Sub AddImageToReport(ByVal PathFileName As String, ByVal FileName As String, Optional ByVal FileImage As Boolean = False)
        For i As Integer = 0 To 1000
            Dim CheckFileName As String = ""
            If FileImage = True Then
                CheckFileName = FileName & ".tif"
            ElseIf i = 0 Then
                CheckFileName = FileName & "1.tif"
            Else
                CheckFileName = FileName & i & ".tif"
            End If

            If (File.Exists(PathFileName & CheckFileName)) Then
                Using objImage As System.Drawing.Image = System.Drawing.Image.FromFile(PathFileName & CheckFileName)
                    If objImage.Width < objImage.Height Then
                        'แนวตั้ง
                        ConvertRptToPdf(PathFileName & CheckFileName, "2_SHOW_FILE")
                    Else
                        'แนวนอน
                        ConvertRptToPdf(PathFileName & CheckFileName, "2_SHOW_FILE_HORIZONTALLY")
                    End If
                End Using
                If FileImage = True Then
                    Exit For
                End If
            Else
                Exit For
            End If
        Next
    End Sub

    Public Sub PdfToImage(ByVal PathFileName As String)
        Dim converter As New PDFConvert
        Dim Converted As Boolean = False
        converter.OutputToMultipleFile = True
        converter.FirstPageToConvert = -1
        converter.LastPageToConvert = -1
        converter.FitPage = True
        converter.JPEGQuality = 10
        converter.OutputFormat = "tifflzw"

        Dim input As System.IO.FileInfo
        input = New FileInfo(PathFileName)
        Dim output As String
        'output = String.Format("{0}\\{1}{2}", input.Directory, input.Name, ".jpg")
        output = PathFileName.Replace(".pdf", ".tif")
        While (File.Exists(output))
            output = output.Replace(".tif", String.Format("{1}{0}", ".tif", DateTime.Now.Ticks))
        End While
        Converted = converter.Convert(input.FullName, output)
    End Sub

    Sub ConvertRptToPdf(ByVal PathFileName As String, ByVal ReportName As String)
        Dim dt As New DataTable
        Dim dr As DataRow
        dt.Columns.Add("Image", System.Type.GetType("System.Byte[]"))
        dr = dt.NewRow

        Dim fs As FileStream
        Dim br As BinaryReader

        fs = New FileStream(PathFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
        br = New BinaryReader(fs)

        Dim imgbyte() As Byte
        ReDim imgbyte(fs.Length - 1)
        imgbyte = br.ReadBytes(Convert.ToInt64((fs.Length)))
        dr(0) = imgbyte
        dt.Rows.Add(dr)
        br.Close()
        fs.Close()

        Dim logonInfo As New TableLogOnInfo
        logonInfo.ConnectionInfo.ServerName = ConfigurationManager.AppSettings.Item("Crystal_Login_ServerName").ToString
        logonInfo.ConnectionInfo.DatabaseName = ConfigurationManager.AppSettings.Item("Crystal_Login_Database").ToString
        logonInfo.ConnectionInfo.UserID = ConfigurationManager.AppSettings.Item("Crystal_Login_User").ToString
        logonInfo.ConnectionInfo.Password = ConfigurationManager.AppSettings.Item("Crystal_Login_Password").ToString

        Dim cc As New ReportDocument()
        Dim ConvertFileName = PathFileName.Replace(".tif", ".pdf")
        cc.Load(Server.MapPath("") & "\Report\Off_Routine_Report\" & ReportName & ".rpt")
        cc.DataDefinition.FormulaFields("Header").Text = "'" & rptFormula.Header & "'"
        cc.DataDefinition.FormulaFields("DocDetail").Text = "'" & rptFormula.DocDetail & "'"
        cc.DataDefinition.FormulaFields("Plant").Text = "'" & rptFormula.Plant & "'"
        cc.DataDefinition.FormulaFields("ReportNo").Text = "'" & rptFormula.ReportNo & "'"
        cc.DataDefinition.FormulaFields("Collected_By").Text = "'" & rptFormula.Collected_By & "'"
        cc.DataDefinition.FormulaFields("Collected_Date").Text = "'" & rptFormula.Collected_Date & "'"
        cc.Database.Tables(0).ApplyLogOnInfo(logonInfo)
        cc.SetDataSource(dt)
        cc.ExportToDisk(ExportFormatType.PortableDocFormat, ConvertFileName)
        BL.PushString(PDFListOffRoutine, ConvertFileName)

    End Sub

    Sub WordToPdf(ByVal Source As Object, ByVal Target As Object)
        'Dim MSdoc As New Microsoft.Office.Interop.Word.ApplicationClass
        'Try
        '    MSdoc.Visible = False
        '    MSdoc.Documents.Open(Source)
        '    MSdoc.Application.Visible = False
        '    MSdoc.WindowState = Microsoft.Office.Interop.Word.WdWindowState.wdWindowStateMinimize

        '    Dim format As Object = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF

        '    MSdoc.ActiveDocument.SaveAs(Target, format)
        'Catch e As Exception
        '    Response.Write(e.Message)
        'Finally
        '    If Not IsNothing(MSdoc) Then
        '        MSdoc.Documents.Close()
        '    End If
        '    MSdoc.Quit()
        'End Try
    End Sub

    Sub ExcelToPdf(ByVal Source As Object, ByVal Target As Object)
        Dim MSexcel As New Microsoft.Office.Interop.Excel.Application
        Try
            MSexcel.Visible = False
            MSexcel.Workbooks.Open(Source)
            MSexcel.Application.Visible = False
            MSexcel.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized

            Dim format As Object = Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF
            MSexcel.ActiveWorkbook.ExportAsFixedFormat(format, Target)

        Catch e As Exception
        Finally
            If Not MSexcel Is Nothing Then
                MSexcel.Workbooks.Close()
            End If
            MSexcel.Quit()
        End Try

    End Sub

    Public Sub CreateTextImageReport(ByVal Text As String, ByVal DestinationPath As String)
        '------------------CalculateSize--------------------
        Dim Font As New Font("Tahoma", 11, FontStyle.Regular, GraphicsUnit.Point)
        Dim X As Bitmap = Bitmap.FromFile(Server.MapPath("") & "\resources\images\Vertical.png")
        Dim G As Graphics = Graphics.FromImage(X)
        Dim S As SizeF = G.MeasureString(Text, Font, New Size(595, 842), StringFormat.GenericTypographic)

        Dim oGraphic As Graphics = Graphics.FromImage(X)
        oGraphic.MeasureString(Text, Font)
        oGraphic.SmoothingMode = SmoothingMode.AntiAlias
        Dim gpath As New GraphicsPath

        gpath.AddString(Text, Font.FontFamily, Font.Style, Font.Size, New Rectangle(5, 5, S.Width, S.Height), StringFormat.GenericTypographic)

        Dim B As Brush = Brushes.Black
        oGraphic.FillPath(Brushes.Black, gpath)

        X.Save(DestinationPath, System.Drawing.Imaging.ImageFormat.Tiff)
        X.Dispose()

    End Sub
#End Region

End Class