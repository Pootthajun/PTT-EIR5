﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Current_Status_AllTag_PdMA.aspx.vb" Inherits="EIR.Dashboard_Current_Status_AllTag_PdMA" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>


			<!-- Page Head -->
			<h2>PdMA & MTap Current Tag Status for <asp:LinkButton ID="lblPlant" runat="server"></asp:LinkButton></h2>
        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
			  <tr>
				<td colspan="2">
				    <h3 style="color: #0066CC;">Chart Type&nbsp;
				        <asp:DropDownList ID="ddl_ChartType" runat="server" AutoPostBack="true" >
				        <asp:ListItem Value="Pie" Selected="True"></asp:ListItem>
				        <asp:ListItem Value="Doughnut"></asp:ListItem>				
				        <asp:ListItem Value="Funnel"></asp:ListItem>
				        <asp:ListItem Value="Pyramid"></asp:ListItem>
                        </asp:DropDownList>
                    </h3> 
                    <asp:LinkButton ID="lblBack" runat="server"></asp:LinkButton>              
                </td>
			  </tr>
			  <tr>
				<td style="vertical-align:top; text-align:center;">
				    <asp:Chart ID="ChartPdMA" runat="server" Height="300px" Width="300px" CssClass="ChartHighligh">
                                <titles>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="All tag(s) in this plant xxx"></asp:Title>
                                    <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title2" Text="xxx tag(s)" Docking="Bottom"></asp:Title>
                                </titles>
                                <series>
                                    <asp:Series ChartType="Pie" Name="Series1" ShadowColor="" Palette="Bright" 
                                        LabelForeColor="White">
                                        <emptypointstyle isvisibleinlegend="False" />
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                </chartareas>
                            </asp:Chart>
                </td>
			      <td style="vertical-align:top;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                              <td colspan="8" style="text-align:center; background-color:#006607; color:White;">
                                  PdMA
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Tag</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Power Quality</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Insulation</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Power Circuit</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Stator</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Rotor</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Air Gap</td>  
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Last Report</td>                            
                          </tr>
                          <asp:Repeater ID="rptPdMA" runat="server" >
                            <ItemTemplate>
                                <tr>
                                    <td style="border-left:1px solid #eeeeee; text-align:left;">
                                        <asp:Label ID="lblTag" Text="-" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblPwq" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblIns" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblPwc" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblSta" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblRot" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblAir" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblLastReport" runat="server" style="color:#666666">-</a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                          </asp:Repeater>
                          
                      </table>
                      <br />
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                              <td colspan="8" style="text-align:center; background-color:#006607; color:White;">
                                  MTap
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Tag</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Power Quality</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Insulation</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Power Circuit</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Stator</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Rotor</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Air Gap</td>  
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Last Report</td>                            
                          </tr>
                          <asp:Repeater ID="rptMTap" runat="server" >
                            <ItemTemplate>
                                <tr>
                                    <td style="border-left:1px solid #eeeeee; text-align:left;">
                                        <asp:Label ID="lblMTapTag" Text="-" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblMTapPwq" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblMTapIns" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblMTapPwc" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblMTapSta" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblMTapRot" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblMTapAir" runat="server" style="color:#666666">-</a>
                                    </td>
                                    <td style="text-align:center; ">
                                        <a ID="lblMTapLastReport" runat="server" style="color:#666666">-</a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                          </asp:Repeater>                         
                      </table>
                  </td>
		      </tr>
			</table>

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>