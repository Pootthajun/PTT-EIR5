﻿Public Class GL_DownloadFile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("FilePathToDownload")) Or IsNothing(Session("FileTypeToDownload")) Then Exit Sub
        If Not IO.File.Exists(Session("FilePathToDownload")) Then Exit Sub
        Dim INFO As System.IO.FileInfo = New System.IO.FileInfo(Session("FilePathToDownload"))
        Response.Clear()
        Response.AddHeader("Content-Disposition", "attachment; filename=" & Session("FilePathToDownload")) ' Actual Path
        Response.AddHeader("Content-Length", INFO.Length.ToString())
        Response.ContentType = Session("FileTypeToDownload")
        Response.WriteFile(INFO.FullName)
    End Sub

End Class