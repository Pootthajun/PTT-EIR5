﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Detail_Plant_RO.aspx.vb" Inherits="EIR.Dashboard_Detail_Plant_RO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UDP1" runat="server">
        <ContentTemplate>
			<h2>
                <asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
			</h2>
        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
        	    <tr>
        	        <td>
        	            <asp:LinkButton ID="lblBack" runat="server" Tooltip="Back to see on this plant" Text="Back to see on this plant"></asp:LinkButton>
        	        </td>
        	    </tr>
			    <tr>
				    <td colspan="2">
				        <h3 style="color: #0066CC;">Chart Type&nbsp; 
				            <asp:DropDownList ID="ddl_ChartType" runat="server" AutoPostBack="true" >
				                <asp:ListItem Value="Pie" Selected="True"></asp:ListItem>
				                <asp:ListItem Value="Doughnut"></asp:ListItem>				
				                <asp:ListItem Value="Funnel"></asp:ListItem>
				                <asp:ListItem Value="Pyramid"></asp:ListItem>
                            </asp:DropDownList>
                        </h3>         
                    </td>
			    </tr>
			    <tr>
				<td style="vertical-align:top;">
				    <asp:Chart ID="ChartMain" runat="server" Width="500px" Height="400px" CssClass="ChartHighligh" >
                        <legends>
                            <asp:Legend Name="Legend1" DockedToChartArea="ChartArea1"></asp:Legend>
                        </legends>
                        <titles>
                            <asp:Title Font="Microsoft Sans Serif, 12pt, style=Bold" Name="Title1" Text="For xxx" ForeColor="#000099"></asp:Title>
                            <asp:Title Docking="Bottom" Name="Title2" Text="xxx Tag" Font="Microsoft Sans Serif, 9.75pt"></asp:Title>
                        </titles>
                        <Series>
                            <asp:Series Name="Series1" ChartType="Pie" Palette="Bright" ShadowColor="" Legend="Legend1" Font="Microsoft Sans Serif, 8.25pt, style=Bold">
                                <points>
                                    <asp:DataPoint BackGradientStyle="None" BackSecondaryColor="" Color="Yellow" 
                                        Font="Tahoma, 8.25pt" Label="#VAL" LegendText="Class C" MapAreaAttributes="" 
                                        ToolTip="" Url="" YValues="10" />
                                    <asp:DataPoint BackGradientStyle="None" BackSecondaryColor="" 
                                        Color="255, 128, 0" Font="Tahoma, 8.25pt" Label="#VAL" LegendText="Class B" 
                                        MapAreaAttributes="" ToolTip="" Url="" YValues="20" />
                                    <asp:DataPoint BackGradientStyle="None" BackSecondaryColor="" Color="Red" 
                                        Font="Tahoma, 8.25pt" Label="#VAL" LabelForeColor="Maroon" LegendText="Class A" 
                                        MapAreaAttributes="" ToolTip="" Url="" YValues="30" />
                                </points>
                                <emptypointstyle isvisibleinlegend="False" />
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1">
                            </asp:ChartArea>
                        </ChartAreas>
                        </asp:Chart>
                </td>
                <td style="vertical-align:top; text-align:left;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="text-align:center; background-color:#003366; color:White;">Tag</td>                               
                            <td style="text-align:center; background-color:#003366; color:White;">Current</td>
                        </tr>
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="tbTag" runat="server" style="cursor:pointer; border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                  <td style="text-align:center;">
                                      <asp:Label ID="lblTag" runat="server"></asp:Label></td>
                                  <td style="text-align:center;">
                                      <asp:Label ID="lblLevel" runat="server" CssClass="TextNormal"></asp:Label></td>                              
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </td>
		      </tr>
			</table>
			<div style="visibility: hidden">
                <asp:Label ID="lblMonth" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblYear" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblPlantName" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblReport" runat="server" Text="Label"></asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
