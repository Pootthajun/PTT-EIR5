﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LAW_DocTestView.aspx.vb" Inherits="EIR.LAW_DocTestView" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register src="~/UC_ImageEditor.ascx" tagname="UC_ImageEditor" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
  <!-- For User Control UC_ImageEditor -->
  <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/lib/jquery.1.10.2.min.js")%>" ></script>
  <!-- For User Control UC_ImageEditor -->

    <!-- Main Stylesheet -->
		<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="all" />
</head>
<body>
    <form id="form1" runat="server">
        <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server"></cc1:ToolkitScriptManager>
        <div>
            <asp:Button ID="btnShowImageEditor" runat="server" Text="Image Editor" />
            <uc1:UC_ImageEditor ID="UC_ImageEditor1" runat="server" />
            
        </div>
        
    </form>
</body>
</html>
