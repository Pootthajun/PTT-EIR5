﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Dashboard_Annual_Progress_Export.aspx.vb" Inherits="EIR.Dashboard_Annual_Progress_Export" %>

<%@ Register src="UC_Dashboard_Annual_Progress.ascx" tagname="UC_Dashboard_Annual_Progress" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body onload="window.print();">
    <form id="form1" runat="server">
        <table align="center" >
            <tr valign="middle">
                <td>
                    <uc1:UC_Dashboard_Annual_Progress ID="UC_Dashboard_Annual_Progress" runat="server" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
