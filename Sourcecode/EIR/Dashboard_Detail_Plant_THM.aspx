﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Detail_Plant_THM.aspx.vb" Inherits="EIR.Dashboard_Detail_Plant_THM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style type="text/css">
        .header2 {
            text-align:center; 
            background-color:#3399ff; 
            color:#333333;
            font-weight:bold;
        }
        .tagItem td {
            color:#666666;
            text-align:center;
            cursor:pointer;
        }
        .tagItem .tag {
            border-left:1px solid #eeeeee; 
            border-right:1px solid #eeeeee; 
            text-align:left;
        }

        .tagItem:hover td {
            background-color:#ace4fa; 
        }
    </style>

    <asp:UpdatePanel ID="UDP1" runat="server">
        <ContentTemplate>
			<h2>
                <asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
			</h2>
        	<table cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
        	    <tr>
        	        <td>
        	            <asp:LinkButton ID="lblBack" runat="server" Tooltip="Back to see on this plant" Text="Back to see on this plant"></asp:LinkButton>
        	        </td>
        	    </tr>
			    <tr>
                  <td style="vertical-align:top; text-align:left;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:auto;">
                      <tr>
                        <td class="LevelNormal" style="text-align:center; font-weight:bold;" colspan="7">          
                            <asp:Label ID="lblHead" runat="server" ></asp:Label>
                        </td>
                      </tr>
                      <tr>
                          <td class="header2">Tag</td>
                          <td class="header2">Equipement Type</td>
                          <td class="header2">Report No</td>
                          <td class="header2">Status</td>                    
                      </tr>
                      <asp:Repeater ID="rptData" runat="server" >
                        <ItemTemplate>                          
                            <tr class="tagItem" id="tr" runat="server">
                                <td class="tag">
                                    <asp:Label ID="lblTag" runat="server" Text="-"></asp:Label>
                                </td>
                                <td class="tag">
                                    <asp:Label ID="lblEqm" runat="server" Text="-"></asp:Label>
                                </td>
                                 <td class="tag">
                                    <asp:Label ID="lblReport" runat="server" Text="-"></asp:Label>
                                </td>
                                <td style="text-align:center;">
                                    <asp:Label ID="lblStatus" runat="server" Text="-"></asp:Label>                                    
                                </td>
                            </tr>
                        </ItemTemplate>
                      </asp:Repeater>
                    </table>
                 </td>
		       </tr>
			</table>
			<div style="visibility: hidden">
                <asp:Label ID="lblMonth" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblYear" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblPlantName" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblReport" runat="server" Text="Label"></asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
