﻿Public Class Dashboard_Annual_Progress
    Inherits System.Web.UI.Page

    Dim Dashboard As New DashboardClass
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dashboard.BindDDlYear(ddl_Year, Date.Now.Year)
            Dashboard.BindDDlEquipmentAnnualProgess(ddl_Equipment, EIR_BL.Report_Type.Stationary_Routine_Report)
            BL.BindDDlPlant(ddl_Plant)
            BindData()
        End If

    End Sub

    Private Sub BindData()
        UC_Dashboard_Annual_Progress.BindData(ddl_Year.SelectedValue, ddl_Equipment.SelectedValue, ddl_Plant.SelectedValue, ddl_Plant.SelectedItem.Text)
    End Sub

    Protected Sub ALL_ddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Year.SelectedIndexChanged, ddl_Equipment.SelectedIndexChanged, ddl_Plant.SelectedIndexChanged
        BindData()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim url As String = "Dashboard_Annual_Progress_Export.aspx?Year=" & ddl_Year.SelectedValue & "&Equipment=" & ddl_Equipment.SelectedValue & "&Plant=" & ddl_Plant.SelectedValue & "&Plant_Name=" & ddl_Plant.SelectedItem.Text
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Export", "openPrintWindow('" & url & "',1000,700);", True)
        BindData()
    End Sub

End Class