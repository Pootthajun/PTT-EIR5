﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Master_ST_Tag
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetTag(Nothing, Nothing)
            ClearPanelSearch()
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindTag()

        Dim SQL As String = "SELECT AREA_CODE+'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_No,TAG_Name,TAG_TYPE_Name,MS_ST_TAG.Active_Status,MS_ST_TAG.Update_Time" & vbNewLine
        SQL &= "   FROM MS_ST_TAG" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_TAG_TYPE ON  MS_ST_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_Route ON MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Area ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Process ON MS_ST_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine
        Dim WHERE As String = ""

        If ddl_Search_Tag_Type.SelectedIndex > 0 Then
            WHERE &= " MS_ST_TAG_TYPE.TAG_TYPE_ID=" & ddl_Search_Tag_Type.Items(ddl_Search_Tag_Type.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " MS_PLANT.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " MS_ST_Route.ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_Area.SelectedIndex > 0 Then
            WHERE &= " MS_Area.AREA_ID=" & ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value & " AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= "ORDER BY AREA_CODE+'-'+PROC_CODE +'-' + TAG_NO" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("MS_ST_TAG") = DT

        Navigation.SesssionSourceName = "MS_ST_TAG"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblTagNo.Text = e.Item.DataItem("TAG_CODE")
        lblTagName.Text = e.Item.DataItem("TAG_Name")
        lblTagType.Text = e.Item.DataItem("TAG_TYPE_Name")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

    End Sub

    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim TAG_ID As Integer = btnEdit.Attributes("TAG_ID")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True
                pnlListTag.Enabled = False
                lblUpdateMode.Text = "Update"


                '--------------Bind Value------------
                Dim SQL As String = "SELECT MS_ST_TAG.* ,MS_PLANT.PLANT_ID" & vbNewLine
                SQL &= " FROM MS_ST_TAG LEFT JOIN MS_ST_ROUTE ON MS_ST_TAG.ROUTE_ID=MS_ST_ROUTE.ROUTE_ID" & vbNewLine
                SQL &= " LEFT JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_ROUTE.PLANT_ID" & vbNewLine
                SQL &= " LEFT JOIN MS_ST_Tag_Type ON MS_ST_TAG.TAG_TYPE_ID=MS_ST_Tag_Type.TAG_TYPE_ID" & vbNewLine
                SQL &= " WHERE MS_ST_TAG.TAG_ID=" & TAG_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "TAG Not Found"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If

                Dim PLANT_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then PLANT_ID = DT.Rows(0).Item("PLANT_ID")
                Dim ROUTE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("ROUTE_ID")) Then ROUTE_ID = DT.Rows(0).Item("ROUTE_ID")
                Dim AREA_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("AREA_ID")) Then AREA_ID = DT.Rows(0).Item("AREA_ID")
                Dim PROC_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PROC_ID")) Then PROC_ID = DT.Rows(0).Item("PROC_ID")
                Dim TAG_TYPE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("TAG_TYPE_ID")) Then TAG_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")

                BL.BindDDlPlant(ddl_Edit_Plant, PLANT_ID)
                BL.BindDDl_ST_Route(PLANT_ID, ddl_Edit_Route, ROUTE_ID)
                BL.BindDDlArea(PLANT_ID, ddl_Edit_Area, AREA_ID)
                BL.BindDDlProcess(ddl_Edit_Process, PROC_ID)
                BL.BindDDlTagType("ST", ddl_Edit_Type, TAG_TYPE_ID)
                txtTagNo.Text = DT.Rows(0).Item("TAG_NO")
                txtTagNo.Attributes("TagID") = TAG_ID
                txtTagName.Text = DT.Rows(0).Item("TAG_NAME")

                txtDesc.Text = DT.Rows(0).Item("TAG_Description")
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")

                btnSave.Focus()

            Case "ToggleStatus"

                Dim SQL As String = "UPDATE MS_ST_TAG Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  TAG_ID=" & TAG_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTag()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select

    End Sub

    Protected Sub ResetTag(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindTag()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------

        pnlListTag.Enabled = True
    End Sub


    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""

        BL.BindDDlPlant(ddl_Edit_Plant, False)
        BL.BindDDl_ST_Route(0, ddl_Edit_Route)
        BL.BindDDlArea(0, ddl_Edit_Area)
        BL.BindDDlProcess(ddl_Edit_Process)
        BL.BindDDlTagType("ST", ddl_Edit_Type)
        txtTagNo.Text = ""
        txtTagNo.Attributes("TagID") = "0"
        txtTagName.Text = ""

        txtDesc.Text = ""
        chkAvailable.Checked = True
        btnCreate.Visible = True

    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        BL.BindDDlTagType("ST", ddl_Search_Tag_Type)
        BL.BindDDlPlant(ddl_Search_Plant, False)
        BL.BindDDl_ST_Route(0, ddl_Search_Route)
        BL.BindDDlArea(0, ddl_Search_Area)
    End Sub

    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BL.BindDDl_ST_Route(ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Route)
        BL.BindDDlArea(ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Area)
        BindTag()
    End Sub

    Protected Sub ddl_Edit_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Plant.SelectedIndexChanged
        BL.BindDDl_ST_Route(ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value, ddl_Edit_Route)
        BL.BindDDlArea(ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value, ddl_Edit_Area)
        BindTag()
        btnSave.Focus()
    End Sub

    Protected Sub ddl_Search_Area_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Area.SelectedIndexChanged
        BindTag()
    End Sub

    Protected Sub ddl_Search_Tag_Type_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Tag_Type.SelectedIndexChanged
        BindTag()
    End Sub

    Protected Sub ddl_Search_Route_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Route.SelectedIndexChanged
        BindTag()
    End Sub

#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        ddl_Edit_Area.Enabled = True
        ddl_Edit_Process.Enabled = True
        txtTagNo.ReadOnly = False
        ddl_Edit_Area.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListTag.Enabled = False
    End Sub

    Protected Sub ddl_Edit_Area_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Area.SelectedIndexChanged

    End Sub

    Protected Sub ddl_Edit_Process_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Process.SelectedIndexChanged

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ddl_Edit_Area.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Area"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If ddl_Edit_Process.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Process"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtTagNo.Text = "" Then
            lblValidation.Text = "Please insert Tag number"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtTagName.Text = "" Then
            lblValidation.Text = "Please insert Tag name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Type.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Equipement-Type"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Plant.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Route.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Route"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim TagID As Integer = txtTagNo.Attributes("TagID")

        Dim SQL As String = "SELECT * FROM MS_ST_TAG WHERE AREA_ID=" & ddl_Edit_Area.Items(ddl_Edit_Area.SelectedIndex).Value & " AND PROC_ID=" & ddl_Edit_Process.Items(ddl_Edit_Process.SelectedIndex).Value & " AND TAG_No='" & txtTagNo.Text.Replace("'", "''") & "' AND TAG_ID<>" & TagID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        SQL = "SELECT * FROM MS_ST_TAG WHERE TAG_ID=" & TagID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            TagID = GetNewTagID()
            DR("TAG_ID") = TagID
        Else
            DR = DT.Rows(0)
        End If

        DR("AREA_ID") = ddl_Edit_Area.Items(ddl_Edit_Area.SelectedIndex).Value
        DR("PROC_ID") = ddl_Edit_Process.Items(ddl_Edit_Process.SelectedIndex).Value
        DR("TAG_No") = txtTagNo.Text '.ToString.PadLeft(4, "0")
        DR("ROUTE_ID") = ddl_Edit_Route.Items(ddl_Edit_Route.SelectedIndex).Value
        DR("TAG_Name") = txtTagName.Text
        DR("TAG_TYPE_ID") = ddl_Edit_Type.Items(ddl_Edit_Type.SelectedIndex).Value
        DR("TAG_Description") = txtDesc.Text
        DR("TAG_Order") = TagID
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetTag(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

        '------------ Focus Last Edit -----------
        DT = Session("MS_ST_TAG")
        DT.DefaultView.RowFilter = "TAG_ID=" & TagID
        If DT.DefaultView.Count > 0 Then
            Dim RowIndex As Integer = DT.Rows.IndexOf(DT.DefaultView(0).Row)
            DT.DefaultView.RowFilter = ""
            Navigation.CurrentPage = Math.Ceiling((RowIndex + 1) / Navigation.PageSize)
        End If
    End Sub

    Private Function GetNewTagID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(TAG_ID),0)+1 FROM MS_ST_TAG "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function


End Class