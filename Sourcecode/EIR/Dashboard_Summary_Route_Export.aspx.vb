﻿Public Class Dashboard_Summary_Route_Export
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Plant_ID As Integer = Request.QueryString("Plant_ID")
            Dim Route_ID As Integer = Request.QueryString("Route_ID")
            Dim Route_Name As String = Request.QueryString("Route_Name")
            Dim Month_F As Integer = Request.QueryString("Month_F")
            Dim Month_T As Integer = Request.QueryString("Month_T")
            Dim Year_F As Integer = Request.QueryString("Year_F")
            Dim Year_T As Integer = Request.QueryString("Year_T")
            Dim Equipment As Integer = Request.QueryString("Equipment")
            UC_Dashboard_Summary_Route1.BindData(Plant_ID, Route_ID, Route_Name, Month_F, Month_T, Year_F, Year_T, Equipment, True)
        End If
    End Sub

End Class