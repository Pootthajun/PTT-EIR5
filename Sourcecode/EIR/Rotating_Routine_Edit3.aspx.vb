﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Rotating_Routine_Edit3
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Rotating_Routine_Report

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("Rotating_Routine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_RO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND RPT_Type_ID=" & RPT_Type_ID, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='Rotating_Routine_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_RO_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_RO_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_RO_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")

            BindTabData()
            Mapper.Visible = False
        End If

        pnlValidation.Visible = False

        '----------------- Implement Jacascript -----------------
        If Not IsPostBack Then
            Dim Script As String = " document.getElementById('" & txtPosX.ClientID & "').value=document.getElementById('" & pnlISO.ClientID & "').style.left.toString().replace('px','');"
            Script &= " document.getElementById('" & txtPosY.ClientID & "').value=document.getElementById('" & pnlISO.ClientID & "').style.top.toString().replace('px','');"
            pnlISO.Attributes("onmousemove") = Script
        End If
        pnlISO.Style.Item("left") = txtPosX.Text & "px"
        pnlISO.Style.Item("top") = txtPosY.Text & "px"

        '----------------- Setup Table ISO ----------------------
        If Not IsPostBack Then
            For i As Integer = 1 To 66
                Dim btn As Button = pnlISO.FindControl("btnISO_" & i.ToString.PadLeft(2, "0"))
                btn.ToolTip = btn.Text
                btn.Style.Item("cursor") = "pointer"
                btn.CssClass = "button_ISO btnISO_" & btn.Text.Substring(0, 1)
            Next
        End If

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_RO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Rotating_Routine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY

                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='Rotating_Routine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Rotating_Routine_Summary.aspx'", True)
            Exit Sub
        End If

    End Sub

    Private Sub BindTabData()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_RO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("Rotating_Routine_Summary.aspx")
            Exit Sub
        End If

        Mapper.PLANT_ID = DT.Rows(0).Item("PLANT_ID")
        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        lbl_Year.Text = RPT_Year
        If Not IsDBNull(DT.Rows(0).Item("RPT_Round")) Then
            lbl_Round.Text = DT.Rows(0).Item("RPT_Round")
        Else
            lbl_Round.Text = ""
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_Period_Start")) Then
            lbl_Period.Text = BL.ReportGridTime(DT.Rows(0).Item("RPT_Period_Start"))
        Else
            lbl_Period.Text = ""
        End If
        '------------------------------Header -----------------------------------

        '---------------------------Bind Vibration ------------------------------
        DT = New DataTable
        SQL = "SELECT DISTINCT RPT.* FROM RPT_Rotating_Routine_Import RPT" & vbNewLine
        SQL &= "INNER JOIN RPT_RO_Detail ON RPT.RPT_Year=RPT_RO_Detail.RPT_Year AND RPT.RPT_No=RPT_RO_Detail.RPT_No AND INSP_ID=12 AND RPT_RO_Detail.ICLS_ID>1 AND RPT.TAG_ID=RPT_RO_Detail.TAG_ID " & vbNewLine
        SQL &= "WHERE RPT.RPT_Year=" & RPT_Year & " AND RPT.RPT_No=" & RPT_No

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        BatchTaglist = BL.Get_Vibration_Tag_Rotating_By_Report(RPT_Year, RPT_No)
        rptVibration.DataSource = DT
        rptVibration.DataBind()
        btnUpdateColor_Click(btnUpdateColor, New EventArgs)
    End Sub


    Protected Sub btnISO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnISO_01.Click, btnISO_02.Click, btnISO_03.Click, btnISO_04.Click, btnISO_05.Click, btnISO_06.Click, btnISO_07.Click, btnISO_08.Click, btnISO_09.Click, btnISO_10.Click, btnISO_11.Click, btnISO_12.Click, btnISO_13.Click, btnISO_14.Click, btnISO_15.Click, btnISO_16.Click, btnISO_17.Click, btnISO_18.Click, btnISO_19.Click, btnISO_20.Click, btnISO_21.Click, btnISO_22.Click, btnISO_23.Click, btnISO_24.Click, btnISO_25.Click, btnISO_26.Click, btnISO_27.Click, btnISO_28.Click, btnISO_29.Click, btnISO_30.Click, btnISO_31.Click, btnISO_32.Click, btnISO_33.Click, btnISO_34.Click, btnISO_35.Click, btnISO_36.Click, btnISO_37.Click, btnISO_38.Click, btnISO_39.Click, btnISO_40.Click, btnISO_41.Click, btnISO_42.Click, btnISO_43.Click, btnISO_44.Click, btnISO_45.Click, btnISO_46.Click, btnISO_47.Click, btnISO_48.Click, btnISO_49.Click, btnISO_50.Click, btnISO_51.Click, btnISO_52.Click, btnISO_53.Click, btnISO_54.Click, btnISO_55.Click, btnISO_56.Click, btnISO_57.Click, btnISO_58.Click, btnISO_59.Click, btnISO_60.Click, btnISO_61.Click, btnISO_62.Click, btnISO_63.Click, btnISO_64.Click, btnISO_65.Click, btnISO_66.Click
        Dim btnISO As Button = sender

        For Each Item As RepeaterItem In rptVibration.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim _txt As TextBox = Item.FindControl("txtZone")
            If _txt.BorderStyle = BorderStyle.Solid Then
                _txt.Text = btnISO.Text.Substring(0, 2)
                _txt.BorderStyle = BorderStyle.None
            End If
        Next
        pnlISO.Visible = False
        btnUpdateColor_Click(btnUpdateColor, e)

    End Sub

    Protected Sub btnUpdateColor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateColor.Click
        For Each Item As RepeaterItem In rptVibration.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim _txt As TextBox = Item.FindControl("txtZone")
            Dim TDZone As HtmlTableCell = Item.FindControl("TDZone")
            If _txt.Text.Length >= 2 Then
                Select Case _txt.Text.Substring(0, 1)
                    Case "A", "B", "C", "D"
                        TDZone.Attributes("class") = "btnISO_" & _txt.Text.Substring(0, 1)
                        _txt.Text = _txt.Text.Substring(0, 2)
                        _txt.CssClass = "btnISO_" & _txt.Text.Substring(0, 1)
                    Case Else
                        TDZone.Attributes("class") = ""
                        _txt.CssClass = ""
                End Select
            Else
                TDZone.Attributes("class") = ""
                _txt.CssClass = ""
            End If
        Next
    End Sub

    Private Function Current_Vibration() As DataTable
        Dim DT As New DataTable

        DT.Columns.Add("RPT_Year")
        DT.Columns.Add("RPT_No")
        DT.Columns.Add("TAG_ID")
        DT.Columns.Add("Vibration_Sererity")
        DT.Columns.Add("Location")
        DT.Columns.Add("Position")
        DT.Columns.Add("Data_Type")
        DT.Columns.Add("Overall_Value")
        DT.Columns.Add("Unit")
        DT.Columns.Add("Percent_Change")
        DT.Columns.Add("Analysis_Comment")
        DT.Columns.Add("PROB_Recomment")
        DT.Columns.Add("Responsible")
        DT.Columns.Add("File_Extension")
        DT.Columns.Add("Upload_By")
        DT.Columns.Add("Upload_Time")

        Dim TAGList As DataTable = BL.Get_Vibration_Tag_Rotating_By_Report(RPT_Year, RPT_No)

        For Each Item As RepeaterItem In rptVibration.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For

            Dim chk_Delete As CheckBox = Item.FindControl("chk_Delete")
            If chk_Delete.Checked Then Continue For

            Dim TRClass As HtmlTableRow = Item.FindControl("TRClass")
            Dim ddlTag As DropDownList = Item.FindControl("ddlTag")

            Dim txtZone As TextBox = Item.FindControl("txtZone")
            Dim txtLocation As TextBox = Item.FindControl("txtLocation")
            Dim txtPosition As TextBox = Item.FindControl("txtPosition")
            Dim txtDataType As TextBox = Item.FindControl("txtDataType")
            Dim txtOverall As TextBox = Item.FindControl("txtOverall")
            Dim txtUnit As TextBox = Item.FindControl("txtUnit")
            Dim txtPercentChanged As TextBox = Item.FindControl("txtPercentChanged")
            Dim txtAnalysis As TextBox = Item.FindControl("txtAnalysis")
            Dim txtComment As TextBox = Item.FindControl("txtComment")
            Dim btnDelete As HtmlInputButton = Item.FindControl("btnDelete")

            Dim DR As DataRow = DT.NewRow
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No

            DR("TAG_ID") = ddlTag.Items(ddlTag.SelectedIndex).Value

            If txtZone.Text <> "" Then
                DR("Vibration_Sererity") = txtZone.Text
            End If

            DR("Location") = txtLocation.Text
            DR("Position") = txtPosition.Text
            DR("Data_Type") = txtDataType.Text

            If txtOverall.Text <> "" Then
                DR("Overall_Value") = CDbl(txtOverall.Text)
            End If
            DR("Unit") = txtUnit.Text
            If txtPercentChanged.Text <> "" Then
                DR("Percent_Change") = CInt(txtPercentChanged.Text)
            End If
            DR("Analysis_Comment") = txtAnalysis.Text
            DR("PROB_Recomment") = txtComment.Text
            DR("Responsible") = txtComment.Attributes("Responsible")
            DR("Upload_By") = Session("USER_ID")
            DR("Upload_Time") = Now
            DT.Rows.Add(DR)
        Next

        Return DT
    End Function

    Protected Sub rptVibration_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptVibration.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnDelete As HtmlInputButton = e.Item.FindControl("btnDelete")
        Dim btnMapISO As ImageButton = e.Item.FindControl("btnMapISO")

        Select Case e.CommandName
            'Case "Delete"

            '    BatchTaglist = BL.Get_Vibration_Tag_By_Report(RPT_Year, RPT_No)
            '    Dim DT As DataTable = Current_Vibration()
            '    DT.Rows.RemoveAt(e.Item.ItemIndex)
            '    rptVibration.DataSource = DT
            '    rptVibration.DataBind()
            '    btnUpdateColor_Click(btnUpdateColor, e)

            Case "Map"

                lnkISO_Click(lnkISO, New EventArgs)
                For Each Item As RepeaterItem In rptVibration.Items
                    If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                    Dim _txt As TextBox = Item.FindControl("txtZone")
                    If Not Equals(Item, e.Item) Then
                        ClearISOTextSelection(_txt)
                    Else
                        SetISOTextSelection(_txt)
                    End If
                Next
        End Select

    End Sub

    Private Sub ClearISOTextSelection(ByRef txt As TextBox)
        txt.BorderStyle = BorderStyle.None
        txt.BorderWidth = 2
    End Sub

    Private Sub SetISOTextSelection(ByRef txt As TextBox)
        txt.BorderStyle = BorderStyle.Solid
        txt.BorderWidth = 2
        txt.BorderColor = Drawing.Color.RoyalBlue
    End Sub

    Dim BatchTaglist As DataTable

    Protected Sub rptVibration_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptVibration.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim TRClass As HtmlTableRow = e.Item.FindControl("TRClass")
        Dim ddlTag As DropDownList = e.Item.FindControl("ddlTag")

        Dim txtZone As TextBox = e.Item.FindControl("txtZone")
        Dim txtLocation As TextBox = e.Item.FindControl("txtLocation")
        Dim txtPosition As TextBox = e.Item.FindControl("txtPosition")
        Dim txtDataType As TextBox = e.Item.FindControl("txtDataType")
        Dim txtOverall As TextBox = e.Item.FindControl("txtOverall")
        Dim txtUnit As TextBox = e.Item.FindControl("txtUnit")
        Dim txtPercentChanged As TextBox = e.Item.FindControl("txtPercentChanged")
        Dim txtAnalysis As TextBox = e.Item.FindControl("txtAnalysis")
        Dim txtComment As TextBox = e.Item.FindControl("txtComment")
        Dim btnDelete As HtmlInputButton = e.Item.FindControl("btnDelete")
        Dim btnMapISO As ImageButton = e.Item.FindControl("btnMapISO")

        ddlTag.Items.Add(New ListItem("", 0))
        ddlTag.SelectedIndex = 0

        For i As Integer = 0 To BatchTaglist.Rows.Count - 1
            ddlTag.Items.Add(New ListItem(BatchTaglist.Rows(i).Item("TAG_CODE"), BatchTaglist.Rows(i).Item("TAG_ID")))
            If e.Item.DataItem("TAG_ID") = BatchTaglist.Rows(i).Item("TAG_ID") Then
                ddlTag.SelectedIndex = ddlTag.Items.Count - 1
            End If
        Next

        'lblZone
        If Not IsDBNull(e.Item.DataItem("Vibration_Sererity")) Then
            txtZone.Text = e.Item.DataItem("Vibration_Sererity")
        End If

        If Not IsDBNull(e.Item.DataItem("Location")) Then
            txtLocation.Text = e.Item.DataItem("Location")
        End If

        If Not IsDBNull(e.Item.DataItem("Position")) Then
            txtPosition.Text = e.Item.DataItem("Position")
        End If

        If Not IsDBNull(e.Item.DataItem("Data_Type")) Then
            txtDataType.Text = e.Item.DataItem("Data_Type")
        End If

        If Not IsDBNull(e.Item.DataItem("Overall_Value")) AndAlso IsNumeric(e.Item.DataItem("Overall_Value")) Then
            txtOverall.Text = FormatNumber(e.Item.DataItem("Overall_Value"))
        End If

        If Not IsDBNull(e.Item.DataItem("Unit")) Then
            txtUnit.Text = e.Item.DataItem("Unit")
        End If

        If Not IsDBNull(e.Item.DataItem("Percent_Change")) AndAlso IsNumeric(e.Item.DataItem("Percent_Change")) Then
            txtPercentChanged.Text = FormatNumber(e.Item.DataItem("Percent_Change"), 2)
        End If

        If Not IsDBNull(e.Item.DataItem("Analysis_Comment")) Then
            txtAnalysis.Text = e.Item.DataItem("Analysis_Comment")
        End If

        If Not IsDBNull(e.Item.DataItem("PROB_Recomment")) Then
            txtComment.Text = e.Item.DataItem("PROB_Recomment")
        End If

        'Responsible
        If Not IsDBNull(e.Item.DataItem("Responsible")) Then
            txtComment.Attributes("Responsible") = e.Item.DataItem("Responsible")
        End If

        '----------------- From eMonitor Tooltip --------------------
        Dim ToolTip As String = ""
        Dim DT As DataTable = rptVibration.DataSource
        If DT.Columns.Contains("CATEGORY") AndAlso Not IsDBNull(e.Item.DataItem("CATEGORY")) AndAlso e.Item.DataItem("CATEGORY") <> "" Then
            ToolTip &= e.Item.DataItem("CATEGORY") & " "
        End If

        If ToolTip <> "" Then
            TRClass.Attributes("Title") = ToolTip
        End If

        '--------------------- Implement Javascript ------------------
        ImplementJavaMoneyText(txtPercentChanged)
        ImplementJavaMoneyText(txtOverall)
        ImplementJavaVibrationZone(txtZone)

        btnMapISO.Attributes("OnMouseDown") = "document.getElementById('" & txtPosX.ClientID & "').value=event.clientX-400+document.documentElement.scrollLeft;"
        btnMapISO.Attributes("OnMouseDown") &= "document.getElementById('" & txtPosY.ClientID & "').value=event.clientY-140+document.documentElement.scrollTop;"

        '------------------ For Deleting-----------------
        Dim chk_Delete As CheckBox = e.Item.FindControl("chk_Delete")
        Dim Script As String = "document.getElementById('" & TRClass.ClientID & "').style.display='none'; "
        Script &= "document.getElementById('" & TRClass.ClientID & "').style.height='0px'; "
        Script &= "document.getElementById('" & chk_Delete.ClientID & "').checked=true; "
        btnDelete.Attributes("onClick") = Script

    End Sub

    Protected Sub btnAddRow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRow.Click
        Dim DT As DataTable = Current_Vibration()

        BatchTaglist = BL.Get_Vibration_Tag_Rotating_By_Report(RPT_Year, RPT_No)

        Dim DR As DataRow = DT.NewRow

        DR("RPT_Year") = RPT_Year
        DR("RPT_No") = RPT_No
        DR("TAG_ID") = 0
        DR("Vibration_Sererity") = ""
        DR("Location") = ""
        DR("Position") = ""
        DR("Data_Type") = ""
        DR("Overall_Value") = ""
        DR("Unit") = ""
        DR("Percent_Change") = ""
        DR("Analysis_Comment") = ""
        DR("PROB_Recomment") = ""
        DR("Responsible") = ""
        DR("File_Extension") = ""
        DR("Upload_By") = Session("USER_ID")
        DR("Upload_Time") = Now

        DT.Rows.Add(DR)

        rptVibration.DataSource = DT
        rptVibration.DataBind()
        ' btnUpdateColor_Click(btnUpdateColor, e)
    End Sub

    Protected Sub btnClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearAll.Click, lnkClear.Click
        rptVibration.DataSource = Nothing
        rptVibration.DataBind()
    End Sub

#Region "Navigator"

    Protected Sub btn_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Next.Click
        SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_Routine_Edit4.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub btn_Back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Back.Click
        SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_Routine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_Routine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDetail.Click
        SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_Routine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
        SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_Routine_Edit4.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub
#End Region

    Protected Sub btn_Buffer_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Buffer_Refresh.Click, lnkRefresh.Click
        BindTabData()
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click
        Try
            'PTClass.Drop_RPT_RO_Detail(Session("User_ID"), RPT_Year, RPT_No)
            Dim Com As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            Com.CommandType = CommandType.Text
            Com.Connection = Conn
            Com.CommandText = "DELETE FROM RPT_Rotating_Routine_Import WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            Com.ExecuteNonQuery()
            Com.Dispose()
            Conn.Close()
        Catch ex As Exception
            Exit Sub
        End Try
        BindTabData()
    End Sub

    Protected Sub lnkISO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkISO.Click, btnISO.Click
        pnlISO.Visible = True
        pnlISO.Style.Item("left") = txtPosX.Text & "px"
        pnlISO.Style.Item("top") = txtPosY.Text & "px"
    End Sub

    Protected Sub btnCloseISO_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCloseISO.Click
        pnlISO.Visible = False
        For Each Item As RepeaterItem In rptVibration.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim _txt As TextBox = Item.FindControl("txtZone")
            ClearISOTextSelection(_txt)
        Next
    End Sub

    Private Sub SaveData(ByVal Sender As Object)
        Dim CUR As DataTable = Current_Vibration()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim COMM As New SqlCommand
        With COMM
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM RPT_Rotating_Routine_Import WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            .ExecuteNonQuery()
            .Dispose()
        End With

        Dim DA As New SqlDataAdapter("SELECT * FROM RPT_Rotating_Routine_Import WHERE 1=0", Conn)
        Dim DT As New DataTable
        DA.Fill(DT)


        For i As Integer = 0 To CUR.Rows.Count - 1
            Dim DR As DataRow = DT.NewRow
            For j As Integer = 0 To CUR.Columns.Count - 1
                If Not IsDBNull(CUR.Rows(i).Item(j)) Then
                    DR(CUR.Columns(j).ColumnName) = CUR.Rows(i).Item(j)
                End If
            Next
            DT.Rows.Add(DR)
            Try
                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT)
                DT.AcceptChanges()
            Catch ex As Exception
                lblValidation.Text = "Invalid parameter"
                pnlValidation.CssClass = "notification error png_bg"
                pnlValidation.Visible = True
                pnlValidation.Focus()
                Exit Sub
            End Try
        Next
        Conn.Close()

        Select Case True
            Case Equals(Sender, btnSave) Or Equals(Sender, lnkSave)

                BindTabData()

                pnlValidation.CssClass = "notification success png_bg"
                pnlValidation.Visible = True
                lblValidation.Text = "Save successfully!!"
                pnlValidation.Focus()

        End Select

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, lnkSave.Click
        SaveData(sender)
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub


    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

#Region "eMonitor"

    Public Property UNIQUEPOPUPID() As String
        Get
            Return ViewState("UNIQUEPOPUPID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUEPOPUPID") = value
        End Set
    End Property

    Protected Sub btnEMonitor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEMonitor.Click

        Dim DT As New DataTable
        Dim SQL As String = "SELECT DISTINCT TAG_ID,TAG_CODE" & vbLf
        SQL &= " FROM VW_Report_RO_Detail " & vbLf
        SQL &= " WHERE INSP_ID=12 AND CURRENT_LEVEL>1 AND" & vbLf
        SQL &= " RPT_Year =" & RPT_Year & " And RPT_No = " & RPT_No & vbLf
        SQL &= " ORDER BY TAG_Code" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        UNIQUEPOPUPID = Now.ToOADate.ToString.Replace(".", "")
        Dim Col() As String = {"TAG_ID", "TAG_Code"}
        DT.DefaultView.RowFilter = "TAG_ID IS NOT NULL"
        DT.DefaultView.Sort = "TAG_Code"
        DT = DT.DefaultView.ToTable(True, Col)
        Session("Mapping_eMonitor_" & UNIQUEPOPUPID) = DT

        Mapper.ShowDialog(UNIQUEPOPUPID)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Focus", "document.getElementById('" & Mapper.ControlToFocus() & "').focus();", True)

    End Sub

    Protected Sub Mapper_AnswerDialog() Handles Mapper.AnswerDialog

        Dim DT As DataTable = Session("Mapping_eMonitor_" & UNIQUEPOPUPID)
        If IsNothing(UNIQUEPOPUPID) Then
            Alert(Me.Page, "Failed to interface with eMonitor !!")
            Exit Sub
        End If

        If IsNothing(DT) OrElse DT.Rows.Count = 0 Then
            Alert(Me.Page, "System got nothing from eMonitor !!")
            rptVibration.DataSource = DT
            rptVibration.DataBind()
            Exit Sub
        End If

        '--------------------- Bind Existing Complement Data----------------
        Dim Vibration As DataTable = Current_Vibration()
        For i As Integer = 0 To DT.Rows.Count - 1

            Dim Filter As String = "TAG_ID='" & DT.Rows(i).Item("TAG_ID").ToString.Replace("'", "''") & "' "
            Filter &= " AND LOCATION='" & DT.Rows(i).Item("LOCATION").ToString.Replace("'", "''") & "' "
            Filter &= " AND POSITION='" & DT.Rows(i).Item("POSITION").ToString.Replace("'", "''") & "' "
            'Filter &= " AND CATEGORY='" & DT.Rows(i).Item("CATEGORY").ToString.Replace("'", "''") & "' "
            Filter &= " AND DATA_TYPE='" & DT.Rows(i).Item("DATA_TYPE").ToString.Replace("'", "''") & "' "
            Filter &= " AND UNIT='" & DT.Rows(i).Item("UNIT").ToString.Replace("'", "''") & "' "

            'Vibration.DefaultView.RowFilter = "TAG_ID=" & DT.Rows(i).Item("TAG_ID") & " AND Position='" & DT.Rows(i).Item("Position").ToString.Replace("'", "''") & "'"
            Vibration.DefaultView.RowFilter = Filter

            If Vibration.DefaultView.Count > 0 Then
                'DT.Rows(i).Item("Vibration_Sererity") = Vibration.DefaultView(0).Item("Vibration_Sererity")
                DT.Rows(i).Item("Analysis_Comment") = Vibration.DefaultView(0).Item("Analysis_Comment")
                DT.Rows(i).Item("PROB_Recomment") = Vibration.DefaultView(0).Item("PROB_Recomment")
                DT.Rows(i).Item("Responsible") = Vibration.DefaultView(0).Item("Responsible")
            End If
        Next

        BatchTaglist = BL.Get_Vibration_Tag_Rotating_By_Report(RPT_Year, RPT_No)
        rptVibration.DataSource = DT
        rptVibration.DataBind()
        btnUpdateColor_Click(Nothing, Nothing)
    End Sub

    Protected Sub Mapper_CancelDialog() Handles Mapper.CancelDialog

    End Sub

#End Region

End Class