﻿Imports System.Data.SqlClient
Imports System.IO

Public Class THM_Command
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        DoCommand()
        Response.Write("Success")
    End Sub


    Private Sub DoCommand()
        Dim BL As New EIR_BL
        '----------------------- Get All Detail Information ---------------
        Dim SQL As String = "SELECT dbo.UDF_RPT_Code(RPT_Year,RPT_No) RPT_Code,THM.*" & vbNewLine
        SQL &= " FROM RPT_THM_Detail THM" & vbNewLine
        'SQL &= " WHERE LEN(Result_FileName)<>14" & vbNewLine
        SQL &= " ORDER BY TAG_Code"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim ServerMapPath As String = ConfigurationManager.AppSettings("ServerMapPath").ToString
        Dim PostedReport As String = BL.PostedReport_Path & "\THM"
        Dim OldFolder As String =ServerMapPath & "\Report\THM_Routine_Report\Tag"

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim RPT_Code As String = DT.Rows(i).Item("RPT_Code")
            Dim OldFileName As String = DT.Rows(i).Item("Result_FileName").ToString

            '---------------If does not upload file Skip this loop------
            If Trim(OldFileName) = "" Then
                DT.Rows(i).Item("Result_FileName") = DBNull.Value
                Continue For
            End If

            DT.DefaultView.RowFilter = "RPT_Code='" & RPT_Code & "'"
            Dim FileIndex As Integer = 0
            For j As Integer = 0 To DT.DefaultView.Count - 1
                If Not IsDBNull(DT.DefaultView(j).Item("Result_FileName")) AndAlso DT.DefaultView(j).Item("Result_FileName") = OldFileName Then
                    FileIndex = j
                    Exit For
                End If
            Next
            FileIndex = FileIndex + 1


            '---------------Check Existing Old File---------------------------
            Dim OldFilePath As String = OldFolder & "\" & RPT_Code & "\" & OldFileName
            '---------------If Not Existing Skip This Loop--------------------
            If Not File.Exists(OldFilePath) Then
                DT.Rows(i).Item("Result_FileName") = DBNull.Value
                Continue For
            End If
            '---------------Move Physical File To New Folder------------------
            Dim NewFileName As String = RPT_Code & "-" & FileIndex.ToString.PadLeft(3, "0")
            Dim NewFilePath As String = PostedReport & "\" & RPT_Code & "\" & NewFileName
            '-------------- Create Folder First ---------------
            Dim NewFolder As String = PostedReport & "\" & RPT_Code
            If Not Directory.Exists(NewFolder) Then
                Directory.CreateDirectory(NewFolder)
            End If
            Try
                File.Move(OldFilePath, NewFilePath)
            Catch ex As Exception
                '---------------If Error Skip This Loop--------------------
                Continue For
            End Try
            '---------------Check The File Alrady Existed In New Folder-------
            '---------------If Not Existing Skip This Loop--------------------
            If Not File.Exists(NewFilePath) Then
                Continue For
            End If
            '---------------Update Database To New FileName-------------------
            DT.Rows(i).Item("Result_FileName") = NewFileName
        Next
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        DT.AcceptChanges()

        '-------------- Delete Empty Old Folder---------
        Dim TAG() As String = Directory.GetDirectories(OldFolder)
        For i As Integer = 0 To TAG.Length - 1
            Dim Files() As String = Directory.GetFiles(TAG(i))
            If Files.Length = 0 Then
                Try
                    Directory.Delete(TAG(i))
                Catch ex As Exception
                End Try
            End If
        Next

    End Sub
End Class