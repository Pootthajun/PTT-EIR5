﻿Imports System.IO
Imports System.Drawing

Public Class DocRender
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim C As New Converter
    Dim DefaultImageUrl As String = "../resources/images/NoImage.png"

    Private ReadOnly Property MODE As String
        Get
            Try
                Return Request.QueryString("MODE").ToString
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public ReadOnly Property UNIQUE_POPUP_ID() As String
        Get
            Try
                Return Me.Request.QueryString("UNIQUE_POPUP_ID")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()

        Dim Obj As FileAttachment = Session(UNIQUE_POPUP_ID)

        Select Case MODE
            Case "RENDER_THUMBNAIL"
                Select Case Obj.Extension
                    Case FileAttachment.ExtensionType.PDF
                        Response.Redirect("Images/FileIcons/PDF.png")
                    Case FileAttachment.ExtensionType.DOC
                        Response.Redirect("Images/FileIcons/DOC.png")
                    Case FileAttachment.ExtensionType.XLS
                        Response.Redirect("Images/FileIcons/XLS.png")
                    Case FileAttachment.ExtensionType.PPT
                        Response.Redirect("Images/FileIcons/PPT.png")
                    Case Else
                        Response.Redirect(DefaultImageUrl)
                End Select
            Case "SESSION_TO_FILE"
                Response.BinaryWrite(Obj.Content)
                Response.AddHeader("Content-Type", BL.Get_FILE_EXTENSION_CONTENT_TYPE(Obj.Extension))

                Dim FileTitle As String = ""
                If Obj.Title = "" Then
                    FileTitle = "Untitled"
                Else
                    FileTitle = Obj.Title
                    '------------ Check If Filename already have Extension ------------
                    Dim FileExtensionName As String = OriginalFileType(FileTitle).ToUpper
                    Select Case Obj.Extension
                        Case FileAttachment.ExtensionType.DOC
                            If FileExtensionName <> "DOC" And FileExtensionName <> "DOCX" Then
                                FileTitle &= ".DOC"
                            End If
                        Case FileAttachment.ExtensionType.PDF
                            If FileExtensionName <> "PDF" Then
                                FileTitle &= ".PDF"
                            End If
                        Case FileAttachment.ExtensionType.XLS
                            If FileExtensionName <> "XLS" And FileExtensionName <> "XLS" Then
                                FileTitle &= ".XLS"
                            End If
                        Case FileAttachment.ExtensionType.PPT
                            If FileExtensionName <> "PPT" And FileExtensionName <> "PPTX" Then
                                FileTitle &= ".PPT"
                            End If
                    End Select
                End If

                Response.AddHeader("Content-Disposition", "attachment; filename=" & FileTitle)
        End Select
    End Sub


End Class