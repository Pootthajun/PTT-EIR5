﻿Imports System.IO

Public Class GL_DialogDocumentUploader
    Inherits System.Web.UI.Page

    Private ReadOnly Property UNIQUE_POPUP_ID() As String
        Get
            If Not IsNothing(Request.QueryString("UNIQUE_POPUP_ID")) Then
                Return Request.QueryString("UNIQUE_POPUP_ID")
            Else
                Return ""
            End If
        End Get
    End Property

    Private Property DisplayTitle As Boolean
        Get
            Return pTitle.Visible
        End Get
        Set(value As Boolean)
            pTitle.Visible = value
        End Set
    End Property

    Private Property DisplayType As Boolean
        Get
            Return pType.Visible
        End Get
        Set(value As Boolean)
            pType.Visible = value
        End Set
    End Property

    Private Property DisplayDesc As Boolean
        Get
            Return pDesc.Visible
        End Get
        Set(value As Boolean)
            pDesc.Visible = value
        End Set
    End Property

    Private Property _ReadOnly As Boolean
        Get
            Return pnlInfo.Enabled
        End Get
        Set(value As Boolean)
            pnlInfo.Enabled = Not value
            btnSave.Visible = Not value
        End Set
    End Property

    Private ReadOnly Property OKButton As String
        Get
            Try
                Return Request.QueryString("OKButton")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private ReadOnly Property TMP_File As FileAttachment
        Get
            Return Session(UNIQUE_POPUP_ID)
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If UNIQUE_POPUP_ID = "" OrElse IsNothing(Session(UNIQUE_POPUP_ID)) Then
            Alert(Me.Page, "Invalid Information")
            CloseTopWindow(Page)
            Exit Sub
        End If

        If Not IsPostBack Then
            BindData()

            If Not IsNothing(Request.QueryString("DisplayTitle")) Then
                DisplayTitle = CBool(Request.QueryString("DisplayTitle"))
            End If
            If Not IsNothing(Request.QueryString("DisplayType")) Then
                DisplayType = CBool(Request.QueryString("DisplayType"))
            End If
            If Not IsNothing(Request.QueryString("DisplayDesc")) Then
                DisplayDesc = CBool(Request.QueryString("DisplayDesc"))
            End If
            If Not IsNothing(Request.QueryString("_ReadOnly")) Then
                _ReadOnly = CBool(Request.QueryString("_ReadOnly"))
            End If

            ImplementJavascriptFirstTime()
        End If
    End Sub

#Region "Implement Javascript"

    Private Sub ImplementJavascriptFirstTime()
        lnkUpload.Attributes("onclick") = "$('#" & ful.ClientID & "').click();"
        view_Nothing.Attributes("onclick") = "$('#" & ful.ClientID & "').click();"
        ful.Attributes("onchange") = "$('#" & btnUpload.ClientID & "').click();"
    End Sub

#End Region

    Private Sub PreviewFile()

        Dim BL As New EIR_BL
        Dim C As New Converter

        view_PDF.Visible = False
        view_Word.Visible = False
        view_Excel.Visible = False
        view_PowerPoint.Visible = False
        view_Nothing.Visible = True

        Select Case TMP_File.Extension
            Case FileAttachment.ExtensionType.PDF
                view_PDF.Visible = True
                '-------------- Save to temp ----------------
                Dim Path As String = BL.ServerMapPath & "\Temp\" & UNIQUE_POPUP_ID & ".pdf"

                If File.Exists(Path) Then
                    Try
                        DeleteFile(Path)
                    Catch ex As Exception
                    End Try
                End If

                Dim F As FileStream = File.Open(Path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
                F.Write(TMP_File.Content, 0, TMP_File.Content.Length)
                F.Close()

                Dim html As String = "<object data='../Temp/" & UNIQUE_POPUP_ID & ".pdf' type='application/pdf'></object>"
                view_PDF.InnerHtml = html

            Case FileAttachment.ExtensionType.DOC
                view_Word.Visible = True
            Case FileAttachment.ExtensionType.XLS
                view_Excel.Visible = True
            Case FileAttachment.ExtensionType.PPT
                view_PowerPoint.Visible = True
            Case Else
                Exit Sub
        End Select

    End Sub

    Private Sub PreviewLastEdit()
        Dim BL As New EIR_BL
        Dim LastEdit As String = ""
        If TMP_File.LastEditTime = DateTime.FromOADate(0) Then
            LastEdit = "New attachment"
        Else
            LastEdit = TMP_File.LastEditTime.ToString("dd-MMM-yyyy HH:mm")
            If TMP_File.LastEditBy = Session("USER_ID") Then
                LastEdit &= vbNewLine & " by you"
            Else
                Dim UT As DataTable = BL.GetUserDetail(TMP_File.LastEditBy)
                If UT.Rows.Count > 0 Then
                    LastEdit &= vbNewLine & UT.Rows(0).Item("User_Full_Name").ToString
                End If
            End If
        End If
        txtLastEdit.Text = LastEdit
    End Sub

    Private Sub BindData()

        Dim BL As New EIR_BL
        Dim Path As String = BL.ServerMapPath & "\Temp\" & UNIQUE_POPUP_ID
        If File.Exists(Path) Then
            Try
                DeleteFile(Path)
            Catch ex As Exception
            End Try
        End If

        Select Case TMP_File.Extension
            Case FileAttachment.ExtensionType.JPEG,
                FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.PNG,
                 FileAttachment.ExtensionType.TIFF, FileAttachment.ExtensionType.SVG
                '------------------ Redirect To SVGEditor -----------------
                Dim URL As String = "../SVGEditor/GL_DialogCanvasEditor.aspx?"
                For i As Integer = 0 To Request.QueryString.Count - 1
                    URL &= Request.QueryString.GetKey(i) & "=" & Request.QueryString.GetValues(i).ToString & "&"
                Next
                Response.Redirect(URL)
            Case Else
                PreviewFile()
        End Select

        '------------ Bind Detail For This Part--------------
        txtTitle.Text = TMP_File.Title
        txtDesc.Text = TMP_File.Description
        BL.BindDDl_AttachmentType(ddlType, TMP_File.DocType)

        PreviewLastEdit()

    End Sub

    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        If IsNothing(ful.FileContent) Then Exit Sub

        Dim C As New Converter

        Select Case ful.PostedFile.ContentType.ToLower()
            Case "application/pdf"
                TMP_File.Extension = FileAttachment.ExtensionType.PDF

            Case "application/msword",
                 "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                TMP_File.Extension = FileAttachment.ExtensionType.DOC

            Case "application/vnd.ms-excel",
                "application/excel",
                "application/vnd.ms-excel",
                "application/x-excel",
                "application/x-msexce",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                TMP_File.Extension = FileAttachment.ExtensionType.XLS

            Case "application/vnd.ms-powerpoint",
                 "application/vnd.openxmlformats-officedocument.presentationml.presentation"
                TMP_File.Extension = FileAttachment.ExtensionType.PPT

            Case Else
                Message_ToaStr("This function support only : pdf, doc, excel, power point ", ToaStrMode.Warning, ToaStrPositon.TopFullWidth, Me.Page)
                Exit Sub
        End Select

        If txtTitle.Text = "" Then txtTitle.Text = ful.FileName
        TMP_File.Content = C.StreamToByte(ful.FileContent)
        TMP_File.LastEditTime = Now
        TMP_File.LastEditBy = Session("USER_ID")

        PreviewLastEdit()
        PreviewFile()

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If TMP_File.Content.Length < 100 Then
            Message_ToaStr("Please select file to upload", ToaStrMode.Danger, ToaStrPositon.BottomFullWidth, Me.Page)
            Exit Sub
        End If

        Select Case TMP_File.Extension
            Case FileAttachment.ExtensionType.JPEG,
                FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.PNG,
                FileAttachment.ExtensionType.TIFF, FileAttachment.ExtensionType.SVG,
                FileAttachment.ExtensionType.Unknow

                Message_ToaStr("This function support only : pdf, doc, excel, power point ", ToaStrMode.Danger, ToaStrPositon.BottomFullWidth, Me.Page)
                Exit Sub
        End Select

        If OKButton <> "" Then
            Dim Script As String = "top.opener.document.getElementById('" & OKButton & "').click();"
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "RefreshToOpener", Script, True)
        End If

        With TMP_File
            .Title = txtTitle.Text
            .Description = txtDesc.Text
            .DocType = ddlType.Items(ddlType.SelectedIndex).Value
        End With

        CloseTopWindow(Page)

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        '------------------ User Create New File Attachment ------------------
        If Not IsNothing(TMP_File) AndAlso TMP_File.LastEditTime = DateTime.FromOADate(0) Then
            Session(UNIQUE_POPUP_ID) = Nothing 'Clear Memory
        End If
        CloseTopWindow(Page)
    End Sub
End Class