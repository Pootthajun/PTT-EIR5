﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GL_DialogDocumentUploader.aspx.vb" Inherits="EIR.GL_DialogDocumentUploader" %>


<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
  <meta name="apple-mobile-web-app-capable" content="yes"/>

  <meta name="description" content="TiT Document Uploader">
  <meta name="author" content="">
  <title>PTT-RBI :: TiT Document Uploader</title>
  <link rel="icon" type="image/png" href="images/favicon.ico"/>
  <!-- Stylesheets -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap-extend.css">
  <link rel="stylesheet" href="css/site.css">

  <link rel="stylesheet" href="css/toastr.css">
  <link rel="stylesheet" href="css/toastr_advance.css">
  <!-- Plugins -->
 <!-- Fonts -->
  <link rel="stylesheet" href="css/fonts/font-awesome/font-awesome.css" />
  <link rel="stylesheet" href="css/fonts/web-icons/web-icons.css" />
  
  <link rel='stylesheet' href="css/font.css" />
  <link rel='stylesheet' href="css/rbi-extent.css" />

  <link rel='stylesheet' href="css/extent.css" />

  <script src="js/modernizr.js"></script>
  <script src="js/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body>
<form id="formMain" runat="server">
<asp:ScriptManager ID="scm" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="udp" runat="server">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>
    <ContentTemplate>


  <nav class="site-navbar navbar navbar-default navbar-fixed-top ">
    
      <div class="navbar-container container-fluid"> 
          <div class="collapse navbar-collapse navbar-collapse-group">                
                <ul class="nav navbar-toolbar navbar-left">
                  <li>
                      <a href="javascript:;" id="lnkUpload" runat="server">
                          <i class="icon fa-cloud-upload margin-right-10"></i>
                          Upload document (pdf, doc, excel, power point) 
                      </a>
                      <asp:Button ID="btnUpload" runat="server" style="display:none;" />
                      <asp:FileUpload ID="ful" runat="server" style="display:none;"  />
                  </li>
                  
                </ul>
              </div>
    </div>
  </nav>
  
  
  <!-- Page -->
    <div class="Media-Placeholder ">
      <div class="preview" id="view_PDF" runat="server" >
          <object data="../Temp/Test.pdf" type="application/pdf"></object>
      </div>      
      <div class="preview" id="view_Excel" runat="server" >
          <img src="Images/FileIcons/XLS.png" style="margin-top: 10%; width:100px;" />
      </div>
       <div class="preview" id="view_Word" runat="server">
          <img src="Images/FileIcons/DOC.png" style="margin-top: 10%; width:100px;" />          
      </div>
      <div class="preview" id="view_PowerPoint" runat="server">
          <img src="Images/FileIcons/PPT.png" style="margin-top: 10%; width:100px;" />
      </div>
        
        <div class="btn btn-info margin-10 margin-left-40" id="view_Nothing" runat="server">            
         
            Click to upload file <i class="icon wb-arrow-up"></i>
       
        </div>
    </div>

   <asp:Panel id="pnlInfo" runat="server" CssClass="page Form-Placeholder" style="background-color:white;">

      <h3 class="panel-title padding-bottom-0">File Info</h3>

    <div class="margin-top-0 padding-top-0">
      <!-- Panel Form Elements -->

        <div class="panel-body container-fluid">
                   
            <asp:Panel ID="pTitle" runat="server" CssClass="col-sm-12 margin-bottom-20 padding-left-0">
                <label class="control-label">Title</label>
                <asp:TextBox CssClass="form-control" ID="txtTitle" MaxLength="200" runat="server"></asp:TextBox>
                <%--<span class="help-block">A block of help text that breaks onto a new line and may extend
                  beyond one line.</span>--%>
            </asp:Panel>

            <asp:Panel ID="pType" runat="server" CssClass="col-sm-12 margin-bottom-20 padding-left-0">
                  <label class="control-label">Type</label>
                  <asp:DropDownList CssClass="form-control" ID="ddlType" runat="server"></asp:DropDownList>	          
            </asp:Panel>

            <asp:Panel ID="pDesc" runat="server" CssClass="col-sm-12 margin-bottom-20 padding-left-0">
                <label class="control-label">Description</label>
                 <asp:TextBox CssClass="form-control" ID="txtDesc" MaxLength="4000" Height="80px" TextMode="MultiLine" runat="server"></asp:TextBox>
            </asp:Panel>

           <div class="col-sm-12 margin-bottom-20 padding-left-0">          
                <label class="control-label">Last Edit</label>
                <asp:TextBox CssClass="form-control inpi" ID="txtLastEdit" ReadOnly="True" runat="server" TextMode="MultiLine"></asp:TextBox>								
            </div>

            <div class="col-sm-12 margin-top-20 padding-left-0">
                <div class="btn-group btn-group-justified">
                    <asp:LinkButton CssClass="btn btn-sm btn-danger" ID="btnClose" runat="server"><i class='icon wb-close'></i> Close</asp:LinkButton>
                    <asp:LinkButton ID="btnSave" runat="server" class="btn btn-sm btn-success"><i class='icon wb-check'></i> Save</asp:LinkButton>
                </div>                       
      
                
             </div>

        </div>

    </asp:Panel>

   </ContentTemplate>
</asp:UpdatePanel>

  <!-- End Page -->
 
  <script src="../js/jquery-3.4.1.min.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/jquery-asScroll.js"></script>
  <script src="js/jquery.mousewheel.js"></script>
  <script src="js/jquery.asScrollable.all.js"></script>
  <script src="js/jquery-asHoverScroll.js"></script>
  <!-- Plugins -->
  <script src="js/switchery.min.js"></script>
  <script src="js/jquery-slidePanel.js"></script>
  <script src="js/toastr.js"></script>
  <!-- Scripts -->
  <script src="core.js"></script>
  <script src="js/site.js"></script>
  <script src="js/menu.js"></script>
  <script src="js/menubar.js"></script>
  <script src="js/config-colors.js"></script>
  <script src="js/asscrollable.js"></script>
  <script src="js/slidepanel.js"></script>
  <script src="js/switchery.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
</form>
</body>
</html>