﻿Imports System.Data.SqlClient
Public Class PIPE_Material_Code
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            ResetMaterialCode(Nothing, Nothing)
            ClearPanelSearch()
        End If

    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub Search(sender As Object, e As EventArgs) Handles btnSearch.Click, txt_Search_Name.TextChanged
        BindMaterialCode()
    End Sub

    Private Sub BindMaterialCode()
        Dim SQL As String = " Select MAT.MAT_CODE_ID,MAT.MAT_CODE,MAT.MAT_CODE_Name,MAT.Update_By,MAT.Update_Time," & vbLf
        SQL &= " COUNT(TAG.TAG_ID) TotalTag" & vbLf
        SQL &= " FROM MS_PIPE_MATERIAL_CODE MAT" & vbLf
        SQL &= " LEFT JOIN " & vbLf
        SQL &= "(SELECT DISTINCT MS_PIPE_POINT.TAG_ID, MS_PIPE_POINT.MAT_CODE_ID" & vbLf
        SQL &= " 	FROM MS_PIPE_TAG " & vbLf
        SQL &= " 	INNER JOIN MS_PIPE_POINT On  MS_PIPE_TAG.TAG_ID=MS_PIPE_POINT.TAG_ID" & vbLf
        SQL &= " 	WHERE MS_PIPE_TAG.Active_Status=1" & vbLf
        SQL &= " ) TAG ON MAT.MAT_CODE_ID=TAG.MAT_CODE_ID" & vbLf

        Dim WHERE As String = ""
        If txt_Search_Name.Text <> "" Then
            WHERE &= " (MAT.MAT_CODE Like '%" & txt_Search_Name.Text.Replace("'", "''") & "%' OR MAT.MAT_CODE_Name LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%') AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= " GROUP By MAT.MAT_CODE_ID,MAT.MAT_CODE,MAT.MAT_CODE_Name,MAT.Update_By,MAT.Update_Time" & vbLf
        SQL &= " ORDER BY MAT.MAT_CODE" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("PIPE_Material_Code") = DT

        Navigation.SesssionSourceName = "PIPE_Material_Code"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptMaterial
    End Sub

    Protected Sub rptMaterial_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMaterial.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblPipe As Label = e.Item.FindControl("lblPipe")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim cfbDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfbDelete")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblCode.Text = e.Item.DataItem("MAT_CODE").ToString
        lblName.Text = e.Item.DataItem("MAT_CODE_Name").ToString

        If Not IsDBNull(e.Item.DataItem("TotalTag")) AndAlso e.Item.DataItem("TotalTag") > 0 Then
            lblPipe.Text = FormatNumber(e.Item.DataItem("TotalTag"), 0)
            cfbDelete.Enabled = False
            btnDelete.Visible = False
        Else
            lblPipe.Text = "-"
            cfbDelete.Enabled = True
            btnDelete.Visible = True
        End If

        If Not IsDBNull(e.Item.DataItem("Update_Time")) Then
            lblUpdateTime.Text = C.DateToString(e.Item.DataItem("Update_Time"), "dd MMM yyyy")
        Else
            lblUpdateTime.Text = "-"
        End If
        btnEdit.CommandArgument = e.Item.DataItem("MAT_CODE_ID")

    End Sub

    Private Sub rptMaterial_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptMaterial.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim MAT_CODE_ID As Integer = btnEdit.CommandArgument

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                lblUpdateMode.Text = "Update"
                lblUpdateMode.Attributes("MAT_CODE_ID") = MAT_CODE_ID
                '------------------------------------
                pnlListService.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT MAT_CODE_ID,MAT_CODE,MAT_CODE_Name" & vbLf
                SQL &= " FROM MS_PIPE_Material_Code" & vbLf
                SQL &= " WHERE MAT_CODE_ID = " & MAT_CODE_ID & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Material Code Not Found"
                    pnlBindingError.Visible = True
                    BindMaterialCode()
                    Exit Sub
                End If

                txtCode.Text = DT.Rows(0).Item("MAT_CODE").ToString
                txtName.Text = DT.Rows(0).Item("MAT_CODE_Name").ToString

                btnSave.Focus()

            Case "Delete"
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = "DELETE FROM MS_PIPE_Material_Code WHERE MAT_CODE_ID=" & MAT_CODE_ID
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindMaterialCode()

                lblBindingSuccess.Text = "Delete successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try
        End Select

    End Sub

    Protected Sub ResetMaterialCode(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindMaterialCode()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListService.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtCode.Text = ""
        txtName.Text = ""
        btnCreate.Visible = True
    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        txt_Search_Name.Text = ""
        BindMaterialCode()
    End Sub
#End Region

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        lblUpdateMode.Text = "Create"
        lblUpdateMode.Attributes("MAT_CODE_ID") = 0

        '-----------------------------------
        pnlListService.Enabled = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtCode.Text = "" Then
            lblValidation.Text = "Please insert material code"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtName.Text = "" Then
            lblValidation.Text = "Please inser description"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim MAT_CODE_ID As Integer = lblUpdateMode.Attributes("MAT_CODE_ID")

        Dim SQL As String = "SELECT * FROM MS_PIPE_Material_Code WHERE MAT_CODE='" & txtCode.Text.Replace("'", "''") & "' AND MAT_CODE_ID<>" & MAT_CODE_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This code is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_PIPE_Material_Code WHERE MAT_CODE_ID=" & MAT_CODE_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            MAT_CODE_ID = GetNewMaterialCodeID()
            DR("MAT_CODE_ID") = MAT_CODE_ID
        Else
            DR = DT.Rows(0)
        End If

        DR("MAT_CODE") = txtCode.Text
        DR("MAT_CODE_Name") = txtName.Text

        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now


        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetMaterialCode(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True


    End Sub

    Private Function GetNewMaterialCodeID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(MAT_CODE_ID),0)+1 FROM MS_PIPE_Material_Code "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

End Class