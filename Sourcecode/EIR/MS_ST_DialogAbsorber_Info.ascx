﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MS_ST_DialogAbsorber_Info.ascx.vb" Inherits="EIR.MS_ST_DialogAbsorber_Info" %>
<table cellpadding="0" cellspacing="0" >
            <tbody>
                
        <tr>					            
					        
		<td class="propertyCaption">Initial Year</td>					        
		<td colspan="2"><asp:TextBox ID="txt_Initial_Year" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>
		<td class="propertyCaption">Operating Pressure</td>
        <td><asp:TextBox ID="txt_OPERATING_PRESSURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
	</tr>	

        <tr>					            
					        
		<td class="propertyCaption">Code Stamp</td>					        
		<td colspan="2"><asp:TextBox ID="txt_CODE_STAMP" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>
		<td class="propertyCaption">Pressure Drop Through Internals</td>
        <td><asp:TextBox ID="txt_PRESSURE_DROP_THROUGH_INTERNALS" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
	</tr>	
        <tr>
            <td class="propertyCaption">Corrosion Allowance</td>
            <td><asp:TextBox ID="txt_CORROSION_ALLOWANCE" runat="server" MaxLength="50"></asp:TextBox></td>
            <td width="5%" style="text-align:center;">mm</td>
            <td class="propertyCaption">Static Vapor Head</td>
            <td><asp:TextBox ID="txt_STATIC_VAPOR_HEAD" runat="server" MaxLength="10"></asp:TextBox></td>
			<td style="text-align:center;">Bar.g</td>
        </tr>
        <tr>
            <td class="propertyCaption">Joint Efficiency</td>
            <td><asp:TextBox ID="txt_JOINT_EFFICIENCY" runat="server" MaxLength="50"></asp:TextBox></td>
            <td width="5%" style="text-align:center;">%</td>
            <td class="propertyCaption">Contents</td>
		<td colspan="2"><asp:TextBox ID="txt_CONTENTS" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>
        
        </tr>
        <tr>
            <td class="propertyCaption">Post Weld Heat Treatment</td>
            <td><asp:TextBox ID="txt_POST_WELD_HEAT_TREATMENT" runat="server" MaxLength="10"></asp:TextBox></td>
		<td style="text-align:center;">° C</td>
            <td class="propertyCaption">Liquid Specific Gravity</td>
            		<td colspan="2"><asp:TextBox ID="txt_LIQUID_SPECIFIC_GRAVITY" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>

        </tr>
        <tr>					            
		<td class="propertyCaption">Radiography</td>
		<td>
           <asp:TextBox ID="txt_RADIOGRAPHY_MIN" runat="server" MaxLength="10"></asp:TextBox>
           </td>
		<td style="text-align:center;">° C</td>
            <td class="propertyCaption">Length Between Tangent Line</td>
		<td><asp:TextBox ID="txt_LENGTH_BETWEEN_TANGENT_LINE" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">mm</td>
	</tr>	
    <tr>					            
		<td class="propertyCaption">Pneumatic Test Press</td>
		<td><asp:TextBox ID="txt_PNEUMATIC_TEST_PRESS" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
            <td class="propertyCaption">Shell Inside Diameter</td>
		<td><asp:TextBox ID="txt_SHELL_INSIDE_DIAMETER" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">mm</td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Hydro Test Pressure (At Shop)</td>
		<td><asp:TextBox ID="txt_HYDRO_TEST_PRESSURE_AT_SHOP" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
            <td class="propertyCaption">Type Of Heads</td>
		<td><asp:TextBox ID="txt_TYPE_OF_HEADS" runat="server" MaxLength="10"></asp:TextBox></td>
		<td style="text-align:center;">ELLIP</td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Hydro Test Pressure (At Field)</td>
		<td><asp:TextBox ID="txt_HYDRO_TEST_PRESSURE_AT_FIELD" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
            <td class="propertyCaption">W.I.N.D Load</td>
		<td><asp:TextBox ID="txt_WIND_LOAD" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">m/s <span style="font-size :9px" >(ASCE 7-95)</span> </td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Design Temperature</td>
		<td><asp:TextBox ID="txt_DESIGN_TEMPERATURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
		<td style="text-align:center;">° C</td>
            <td class="propertyCaption">Seismic Load</td>
		<td><asp:TextBox ID="txt_SEISMIC_LOAD" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">m/s <span style="font-size :9px" >(ASCE 7-95)</span> </td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Min Design Metal Temperature</td>
		<td><asp:TextBox ID="txt_MIN_DESIGN_METAL_TEMPERATURE" runat="server" MaxLength="10"></asp:TextBox></td>
		<td style="text-align:center;">° C</td>
            <td class="propertyCaption">Insulation Thickness</td>
		<td><asp:TextBox ID="txt_INSULATION_THICKNESS" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">mm</td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Design Internal Pressure</td>
		<td><asp:TextBox ID="txt_DESIGN_INTERNAL_PRESSURE" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
            <td class="propertyCaption">Fire Proofing Thickness</td>
		<td><asp:TextBox ID="txt_FIRE_PROOFING_THICKNESS" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">mm</td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Design External Pressure</td>
		<td><asp:TextBox ID="txt_DESIGN_EXTERNAL_PRESSURE" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
            <td class="propertyCaption">Refractory Lining Thickness Volume</td>
		<td><asp:TextBox ID="txt_REFRACTORY_LINING_THICKNESS" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">mm</td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Operating Temperature</td>
		<td><asp:TextBox ID="txt_OPERATING_TEMPERATURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
		<td style="text-align:center;">° C</td>
            <td class="propertyCaption">Volume</td>
        <td><asp:TextBox ID="txt_VOLUME" runat="server" MaxLength="10"></asp:TextBox></td>
		        <td style="text-align:center;"><p>m<sup>3</sup></p> </td>
	</tr>

            </tbody>
</table>