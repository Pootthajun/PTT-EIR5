﻿Public Class Dashboard_Improvement_Report_Export
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Month_F As Integer = Request.QueryString("Month_F")
            Dim Month_T As Integer = Request.QueryString("Month_T")
            Dim Year_F As Integer = Request.QueryString("Year_F")
            Dim Year_T As Integer = Request.QueryString("Year_T")
            Dim Equipment As Integer = Request.QueryString("Equipment")
            UC_Dashboard_Improvement_Report.BindData(Month_F, Month_T, Year_F, Year_T, Equipment, True)
        End If
    End Sub

End Class