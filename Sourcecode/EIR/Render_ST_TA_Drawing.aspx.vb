﻿Public Class Render_ST_TA_Drawing
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim C As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Select Case Request.QueryString("Mode").ToLower
            Case "session"
                Dim UNIQUE_POPUP_ID As String = Request.QueryString("UNIQUE_POPUP_ID")
                Dim ImageID As Integer = Request.QueryString("ImageID")
                Dim DrawingList As List(Of EIR_BL.ST_TA_DrawingDetail) = Session("ST_TA_TAG_" & UNIQUE_POPUP_ID)
                Dim S As Byte() = DrawingList(ImageID).File_Data
                Response.Clear()
                Response.BinaryWrite(S)
                Response.AddHeader("content-type", DrawingList(ImageID).File_Type)
            Case "PIPE_CUI_Edit1".ToLower
                Try
                    Dim UNIQUE_POPUP_ID As String = Request.QueryString("UNIQUE_POPUP_ID")
                    Dim ImageID As Integer = Request.QueryString("ImageID")
                    Dim Obj As EIR_BL.PipeDrawingDetail = Session("PIPE_CUI_Edit1_" & ImageID & "_" & UNIQUE_POPUP_ID)
                    Response.Clear()
                    Response.BinaryWrite(Obj.File_Data)
                    Response.AddHeader("content-type", Obj.File_Type)
                Catch ex As Exception
                    Response.Redirect("Resources/Images/Sample_40.png")
                End Try
            Case "PIPE_CUI_Edit2".ToLower
                Try
                    Dim UNIQUE_POPUP_ID As String = Request.QueryString("UNIQUE_POPUP_ID")
                    Dim ImageID As Integer = Request.QueryString("ImageID")
                    Dim Obj As EIR_BL.PipeDrawingDetail = Session("PIPE_CUI_Edit2_" & ImageID & "_" & UNIQUE_POPUP_ID)
                    Response.Clear()
                    Response.BinaryWrite(Obj.File_Data)
                    Response.AddHeader("content-type", Obj.File_Type)
                Catch ex As Exception
                    Response.Redirect("Resources/Images/Sample_40.png")
                End Try
        End Select
    End Sub

End Class