﻿Imports System.IO
Public Class RenderImageFromBinary
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim S As String = Request.QueryString("S")

        If S = "" Then
            Response.Redirect("resources/images/Sample_40.png", True)
            Exit Sub
        End If

        Try
            Dim C As New Converter
            Dim IMG As Byte() = C.StreamToByte(Session(S))
            Response.Clear()
            Response.BinaryWrite(IMG)
        Catch ex As Exception
            Response.Redirect("resources/images/Sample_40.png", True)
            Exit Sub
        End Try

    End Sub

End Class