﻿Option Strict Off
Imports System.Data.SqlClient
Imports EIR

Public Class PIPE_Tag
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE
    Dim C As New Converter

    Private Property UNIQUE_POPUP_ID() As String
        Get
            Return ViewState("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Private Property TAG_ID As Integer
        Get
            Try
                Return ViewState("TAG_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private Property POINT_MAIN_PREVIEW As FileAttachment
        Get
            Return Session(txtMainPreviewID.Text)
        End Get
        Set(value As FileAttachment)
            If IsNothing(value) Then
                txtMainPreviewID.Text = GenerateNewUniqueID()
            Else
                txtMainPreviewID.Text = value.UNIQUE_ID
            End If
            Session(txtMainPreviewID.Text) = value
            Render_Point_Main_Preview()
        End Set
    End Property

    Private Property POINT_SUB_PREVIEW As List(Of FileAttachment)
        Get
            Return Session("SUB_PREVIEW_" & UNIQUE_POPUP_ID)
        End Get
        Set(value As List(Of FileAttachment))
            Session("SUB_PREVIEW_" & UNIQUE_POPUP_ID) = value
        End Set
    End Property

    Private Property SELECTED_POINT_INDEX As Integer
        Get
            For Each item As RepeaterItem In rptPoint.Items
                If item.ItemType <> ListItemType.Item And item.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim trPoint As HtmlTableRow = item.FindControl("trPoint")
                If trPoint.Attributes("class") = "selected" Then
                    Return item.ItemIndex
                End If
            Next
            Return -1
        End Get
        Set(value As Integer)
            For Each item As RepeaterItem In rptPoint.Items
                If item.ItemType <> ListItemType.Item And item.ItemType <> ListItemType.AlternatingItem Then Continue For

                Dim trPoint As HtmlTableRow = item.FindControl("trPoint")
                If item.ItemIndex = value Then
                    trPoint.Attributes("class") = "selected"
                Else
                    trPoint.Attributes("class") = ""
                End If
            Next
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            UNIQUE_POPUP_ID = Now.ToOADate.ToString.Replace(".", "")
            ResetTag(Nothing, Nothing)
            ClearPanelSearch()
            ImplementJavascriptControl()
        End If

        StoreJS()
    End Sub

    Private Sub StoreJS()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "StoreJSPageLoad1", "resizeTabPoint();", True)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "StoreJSPageLoad2", "$(document).ready(resizeTabPoint); window.onresize = resizeTabPoint;", True)
    End Sub

#Region "Validator"

    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
        pnlBindingPointError.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnBindingPointError_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingPointError.Click
        pnlBindingPointError.Visible = False
    End Sub


#End Region

    Private Sub ImplementJavascriptControl()
        ImplementJavaNumericText(txt_Search_Size, "Center")
        ImplementJavaNumericText(txt_search_Insulation_Thickness, "Center")
    End Sub

    Private Sub BindTag()

        Dim SQL As String = ""
        SQL &= " Select TAG_ID,DCS_Code,Use_DCS,Default_Code,TAG_Code,PIPE_Class,PLANT_ID,PLANT_Code,AREA_ID,AREA_Code,ROUTE_ID,ROUTE_Code" & vbLf
        SQL &= " ,PROC_ID,PROC_Code,Line_No,Manual_Class,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,PRS_ID,PRS_Code" & vbLf
        SQL &= " ,CA_ID,CA_DEPTH_MM,Serial_No,IN_ID,IN_Code,IN_Thickness,Active_Status" & vbLf
        SQL &= " ,Component_Type,Component_Type_Name,Size" & vbLf
        SQL &= " ,SERVICE_ID,SERVICE_Name,Initial_Year,Total_Point,Loop_No" & vbLf
        SQL &= " FROM VW_PIPE_TAG" & vbLf

        Dim WHERE As String = ""

        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Area.SelectedIndex > 0 Then
            WHERE &= " AREA_ID=" & ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Process.SelectedIndex > 0 Then
            WHERE &= " PROC_ID=" & ddl_Search_Process.Items(ddl_Search_Process.SelectedIndex).Value & " And "
        End If
        If txt_Search_LineNo.Text <> "" Then
            WHERE &= " Line_No Like '%" & txt_Search_LineNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txt_Search_Size.Text <> "" And IsNumeric(txt_Search_Size.Text) Then
            WHERE &= " Size = " & txt_Search_Size.Text.Replace(",", "") & " AND "
        End If
        If ddl_Search_Material.SelectedIndex > 0 Then
            WHERE &= " MAT_CODE_ID=" & ddl_Search_Material.Items(ddl_Search_Material.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Pressure.SelectedIndex > 0 Then
            WHERE &= " PRS_ID=" & ddl_Search_Pressure.Items(ddl_Search_Pressure.SelectedIndex).Value & " And "
        End If
        If ddl_Search_CA.SelectedIndex > 0 Then
            WHERE &= " CA_ID = " & ddl_Search_CA.Items(ddl_Search_CA.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_Service.SelectedIndex > 0 Then
            WHERE &= " SERVICE_ID=" & ddl_Search_Service.Items(ddl_Search_Service.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Insulation.SelectedIndex > 0 Then
            WHERE &= " IN_ID=" & ddl_Search_Insulation.Items(ddl_Search_Insulation.SelectedIndex).Value & " And "
        End If
        If txt_search_Insulation_Thickness.Text <> "" And IsNumeric(txt_search_Insulation_Thickness.Text) Then
            WHERE &= " IN_Thickness = " & txt_search_Insulation_Thickness.Text.Replace(",", "") & " AND "
        End If
        If txt_Search_Code.Text <> "" Then
            WHERE &= " (TAG_Code Like '%" & txt_Search_Code.Text.Replace("'", "''") & "%' OR Serial_No LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%' OR Default_Code LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%') AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= "   ORDER BY TAG_Code" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("PIPE_Tag") = DT

        pnlSearch.Visible = True
        Navigation.SesssionSourceName = "PIPE_Tag"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Private Sub rptTag_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        Dim lblMaterial As Label = e.Item.FindControl("lblMaterial")
        Dim lblPoint As Label = e.Item.FindControl("lblPoint")
        Dim lblService As Label = e.Item.FindControl("lblService")
        Dim lblLoopNo As Label = e.Item.FindControl("lblLoopNo")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblCode.Text = e.Item.DataItem("TAG_Code").ToString
        lblPlant.Text = e.Item.DataItem("PLANT_Code").ToString
        lblRoute.Text = e.Item.DataItem("ROUTE_Code").ToString
        lblService.Text = e.Item.DataItem("Total_Point").ToString
        lblMaterial.Text = e.Item.DataItem("MAT_CODE").ToString & " (" & e.Item.DataItem("MAT_CODE_Name").ToString & ")"
        lblService.Text = e.Item.DataItem("SERVICE_Name").ToString
        lblLoopNo.Text = e.Item.DataItem("Loop_No").ToString

        If e.Item.DataItem("Total_Point") = 0 Then
            lblPoint.Text = "-"
        Else
            lblPoint.Text = FormatNumber(e.Item.DataItem("Total_Point"), 0)
        End If

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        btnEdit.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

    End Sub

    Private Sub rptTag_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        TAG_ID = btnEdit.Attributes("TAG_ID")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                TAG_ID = btnEdit.Attributes("TAG_ID")

                '------------ Bind Tag Data-----------
                Pipe_Info.BindData(TAG_ID)
                If Pipe_Info.TAG_ID = 0 Then
                    lblBindingError.Text = "TAG Not found"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If

                lblTagCode.Text = Pipe_Info.TAG_CODE

                Dim DT As DataTable = PIPE.Get_Tag_Info(TAG_ID)
                If Not IsDBNull(DT.Rows(0).Item("ROUTE_ID")) Then
                    PIPE.BindDDl_Route(Pipe_Info.PLANT_ID, ddl_Route, DT.Rows(0).Item("ROUTE_ID"))
                Else
                    PIPE.BindDDl_Route(Pipe_Info.PLANT_ID, ddl_Route)
                End If

                '------------ Bind Tag File-----------
                'Pipe_File.UNIQUE_ID = GenerateNewUniqueID() '------------ Already Set at ClearPanelEdit------
                '-------- เก็บ List File ใส่ Session ---------
                Dim TagFiles As List(Of FileAttachment) = PIPE.Get_Point_FileAttachments(TAG_ID, 0)
                Dim Pics As New List(Of FileAttachment)
                Dim Docs As New List(Of FileAttachment)
                For i As Integer = 0 To TagFiles.Count - 1
                    Select Case TagFiles(i).Extension
                        Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG, FileAttachment.ExtensionType.PNG,
                             FileAttachment.ExtensionType.SVG, FileAttachment.ExtensionType.TIFF
                            Pics.Add(TagFiles(i))
                        Case FileAttachment.ExtensionType.DOC, FileAttachment.ExtensionType.PDF, FileAttachment.ExtensionType.PPT,
                             FileAttachment.ExtensionType.XLS
                            Docs.Add(TagFiles(i))
                    End Select
                    Session(TagFiles(i).UNIQUE_ID) = TagFiles(i)
                Next
                '--------- Pic Files-----------
                Session(Pipe_File.UNIQUE_ID) = Pics
                Pipe_File.Attachments = Pics
                Pipe_File.BindData()
                '--------- Doc Files-----------
                Session(Pipe_Doc.UNIQUE_ID) = Docs
                Pipe_Doc.Attachments = Docs
                Pipe_Doc.BindData()
                '---------- Load Preview ------
                POINT_MAIN_PREVIEW = PIPE.GET_POINT_PREVIEW(TAG_ID, 0)
                SELECTED_POINT_INDEX = -1
                PreviewSelectedPoint()

                '------------ Bind Point Data------------------
                Dim PT As DataTable = PIPE.Get_Point_Param(TAG_ID)
                PT.Columns.Add("UNIQUE_ID", GetType(String)) '----------- เก็บ Unique_ID ของ Point -----------
                PT.DefaultView.RowFilter = "POINT_ID<>0"
                PT = PT.DefaultView.ToTable
                PT.DefaultView.RowFilter = ""

                '------------ Generate New Point Signature And Bind Files-----
                For i As Integer = 0 To PT.Rows.Count - 1
                    Dim UNIQUE_ID As String = GenerateNewUniqueID()
                    PT.Rows(i).Item("UNIQUE_ID") = UNIQUE_ID '----------- Named Point ---------
                    '-------- Get List Point Files-------------
                    Dim PointFiles As List(Of FileAttachment) = PIPE.Get_Point_FileAttachments(TAG_ID, PT.Rows(i).Item("POINT_ID"))
                    '-------- เก็บ List File ใส่ Session ---------
                    Session(UNIQUE_ID) = PointFiles '----------- Collect Point File Collection ---------
                    For j As Integer = 0 To PointFiles.Count - 1
                        Session(PointFiles(j).UNIQUE_ID) = PointFiles(j)
                    Next
                Next
                '------------ Get Point Preview ---------------
                For i As Integer = 0 To PT.Rows.Count - 1
                    Dim PREVIEW_ID As String = PT.Rows(i).Item("PREVIEW_ID").ToString
                    If PREVIEW_ID = "" Then
                        PREVIEW_ID = GenerateNewUniqueID()
                        PT.Rows(i).Item("PREVIEW_ID") = PREVIEW_ID
                    End If
                    Session(PREVIEW_ID) = PIPE.GET_POINT_PREVIEW(TAG_ID, PT.Rows(i).Item("POINT_ID"))
                    If Not IsNothing(Session(PREVIEW_ID)) Then
                        POINT_SUB_PREVIEW.Add(Session(PREVIEW_ID))
                    End If
                Next

                '------------ Bind All Point Information ------
                rptPoint.DataSource = PT
                rptPoint.DataBind()

                Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
                chkAvailable.Checked = btnToggle.ImageUrl = "resources/images/icons/tick.png"

                pnlEdit.Visible = True
                btnCreate.Visible = False
                pnlSearch.Visible = False
                lblUpdateMode.Text = "Update"

            Case "ToggleStatus"

                Dim SQL As String = "UPDATE MS_PIPE_TAG Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  TAG_ID=" & TAG_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTag()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True

            Case "Delete"

                Dim TotalReport = PIPE.Get_CUI_ReportHeader_By_Tag(TAG_ID).Rows.Count
                TotalReport += PIPE.Get_ERO_ReportHeader_By_Tag(TAG_ID).Rows.Count
                If TotalReport > 0 Then
                    lblBindingError.Text = "This Tag is already existing in some inspection report. Unable to delete this tag"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If

                Try
                    PIPE.Drop_TAG(TAG_ID)
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try


                Dim PageIndex As Integer = Navigation.CurrentPage
                BindTag()
                Navigation.CurrentPage = PageIndex

                lblBindingSuccess.Text = "Delete successfully"
                pnlBindingSuccess.Visible = True


        End Select
    End Sub

    Protected Sub ResetTag(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindTag()
        '-----------------------------------
        ClearPanelEdit()
    End Sub

    Private Sub ClearPanelEdit()

        '------------ Tag Info -------------
        Pipe_Info.BindData(0)
        PIPE.BindDDl_Route(0, ddl_Route)
        '------------ Pic File -------------
        Pipe_File.UNIQUE_ID = GenerateNewUniqueID()
        Pipe_File.Attachments = New List(Of FileAttachment)
        Pipe_File.BindData()
        '------------ Doc File --------------
        Pipe_Doc.UNIQUE_ID = GenerateNewUniqueID()
        Pipe_Doc.Attachments = New List(Of FileAttachment)
        Pipe_Doc.BindData()
        '------------ Main Preview ----------
        txtMainPreviewID.Text = GenerateNewUniqueID()
        POINT_MAIN_PREVIEW = Nothing
        '------------ Sub Preview -----------
        POINT_SUB_PREVIEW = New List(Of FileAttachment)
        '------------ Point Info ------------
        Dim PointData As DataTable = BlankPointDataTable()
        rptPoint.DataSource = PointData
        rptPoint.DataBind()
        '------------ Point Preview ----------
        ClEAR_SUB_PREVIEW()
        '------------ Change Tab -----------
        ChangeTab(MyTab.Tag)

        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        chkAvailable.Checked = True
        btnCreate.Visible = True

        '-------------Point Data------------
        ClearPanelEditPoint()
    End Sub

    Private Function PointData() As DataTable
        Dim DT As DataTable = BlankPointDataTable()
        For Each item As RepeaterItem In rptPoint.Items
            If item.ItemType <> ListItemType.Item And item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim trPoint As HtmlTableRow = item.FindControl("trPoint")
            Dim DR As DataRow = DT.NewRow
            For i As Integer = 0 To DT.Columns.Count - 1
                Dim Param As String = DT.Columns(i).ColumnName
                Select Case DT.Columns(i).DataType
                    Case GetType(String)
                        DR(Param) = trPoint.Attributes(Param)
                    Case GetType(Integer)
                        If IsNothing(trPoint.Attributes(Param)) OrElse trPoint.Attributes(Param) = "" Then
                            DR(Param) = DBNull.Value
                        Else
                            DR(Param) = CInt(trPoint.Attributes(Param))
                        End If
                    Case GetType(Double)
                        If IsNothing(trPoint.Attributes(Param)) OrElse trPoint.Attributes(Param) = "" Then
                            DR(Param) = DBNull.Value
                        Else
                            DR(Param) = CDbl(trPoint.Attributes(Param))
                        End If
                End Select
            Next
            DT.Rows.Add(DR)
        Next
        '-------------- Also Get POINT_SUB_PREVIEW ----------------
        POINT_SUB_PREVIEW = New List(Of FileAttachment)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim PREVIEW_ID As String = DT.Rows(i).Item("PREVIEW_ID").ToString
            Dim Attachment As FileAttachment = Session(PREVIEW_ID)
            If Not IsNothing(Attachment) Then
                POINT_SUB_PREVIEW.Add(Attachment)
            End If
        Next

        Return DT
    End Function

    Private Sub Render_Point_Main_Preview()

        Dim Thumnail As String = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=" & txtMainPreviewID.Text & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")

        If Not IsNothing(POINT_MAIN_PREVIEW) Then
            pnlMainPreview.BackImageUrl = Thumnail
            aZoomMaskMainPreview.HRef = Thumnail
            aZoomMaskMainPreview.Attributes("onclick") = ""
            lblTitleMainPreview.Text = POINT_MAIN_PREVIEW.Title
        Else
            pnlMainPreview.BackImageUrl = ""
            aZoomMaskMainPreview.HRef = "javascript:;"
            aZoomMaskMainPreview.Attributes("onclick") = "document.getElementById('" & btnEditMainPreview.ClientID & "').click();"
            lblTitleMainPreview.Text = ""
        End If

        divTitleMainPreview.Visible = lblTitleMainPreview.Text <> ""
        btnDeleteMainPreview.Visible = Not IsNothing(POINT_MAIN_PREVIEW)

    End Sub

    Private Sub Render_Point_Sub_Preview()

        Dim Thumnail As String = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=" & txtPointPreviewID.Text & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
        Dim Preview As FileAttachment = Session(txtPointPreviewID.Text)
        If Not IsNothing(Preview) Then
            pnlPointPreview.BackImageUrl = Thumnail
            aZoomMaskPointPreview.HRef = Thumnail
            aZoomMaskPointPreview.Attributes("onclick") = ""
            lblTitlePointPreview.Text = Preview.Title
        Else
            pnlPointPreview.BackImageUrl = pnlMainPreview.BackImageUrl
            aZoomMaskPointPreview.HRef = "javascript:;"
            aZoomMaskPointPreview.Attributes("onclick") = "document.getElementById('" & btnRefreshPointPreview.ClientID & "').click();"
            lblTitlePointPreview.Text = ""
        End If
        divTitlePointPreview.Visible = lblTitleMainPreview.Text <> ""
        btnDeletePointPreview.Visible = Not IsNothing(Preview)

    End Sub

    Private Sub btnEditMainPreview_Click(sender As Object, e As ImageClickEventArgs) Handles btnEditMainPreview.Click
        Dim Obj As FileAttachment = POINT_MAIN_PREVIEW
        If IsNothing(Obj) Then
            '----------- Create New ------------
            Obj = New FileAttachment
            Obj.UNIQUE_ID = txtMainPreviewID.Text
            Obj.Extension = FileAttachment.ExtensionType.SVG
            Obj.DocType = FileAttachment.AttachmentType.EquipementPreview
            Session(Obj.UNIQUE_ID) = Obj
        End If
        ShowDialogEditSVG(Me.Page, Obj.UNIQUE_ID, "", btnRefreshMainPreview.ClientID, "")
    End Sub

    Private Sub btnDeleteMainPreview_Click(sender As Object, e As ImageClickEventArgs) Handles btnDeleteMainPreview.Click
        POINT_MAIN_PREVIEW = Nothing
        Render_Point_Main_Preview()
    End Sub

    Private Sub btnRefreshMainPreview_Click(sender As Object, e As EventArgs) Handles btnRefreshMainPreview.Click
        Render_Point_Main_Preview()
    End Sub

    Private Sub btnEditPointPreview_Click(sender As Object, e As ImageClickEventArgs) Handles btnEditPointPreview.Click
        Dim Obj As FileAttachment = Session(txtPointPreviewID.Text)
        If IsNothing(Obj) Then
            '----------- Create New ------------
            Obj = New FileAttachment
            If Not IsNothing(POINT_MAIN_PREVIEW) Then
                Obj.Content = POINT_MAIN_PREVIEW.Content
            End If
            Obj.UNIQUE_ID = txtPointPreviewID.Text
            Obj.Extension = FileAttachment.ExtensionType.SVG
            Obj.DocType = FileAttachment.AttachmentType.EquipementPreview
            Session(Obj.UNIQUE_ID) = Obj
        End If
        ShowDialogEditSVG(Me.Page, Obj.UNIQUE_ID, "", btnRefreshPointPreview.ClientID, "")
    End Sub

    Private Sub btnDeletePointPreview_Click(sender As Object, e As ImageClickEventArgs) Handles btnDeletePointPreview.Click
        Session(txtPointPreviewID.Text) = Nothing
        Render_Point_Sub_Preview()
    End Sub

    Private Sub btnRefreshPointPreview_Click(sender As Object, e As EventArgs) Handles btnRefreshPointPreview.Click
        Render_Point_Sub_Preview()
    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        BL.BindDDlPlant(ddl_Search_Plant)
        BL.BindDDlArea(ddl_Search_Area)
        PIPE.BindDDl_Route(0, ddl_Search_Route)
        BL.BindDDlProcess(ddl_Search_Process)
        txt_Search_LineNo.Text = ""
        txt_Search_Size.Text = ""
        PIPE.BindDDl_Material_Code(ddl_Search_Material)
        PIPE.BindDDl_Pressure_Code(ddl_Search_Pressure)
        PIPE.BindDDl_CorrosionAllowance(ddl_Search_CA)
        PIPE.BindDDl_Service(ddl_Search_Service)
        PIPE.BindDDl_Insulation_Code(ddl_Search_Insulation)
        txt_search_Insulation_Thickness.Text = ""

        txt_Search_Code.Text = ""
    End Sub

    Private Sub Search_Changed(sender As Object, e As EventArgs) Handles ddl_Search_Route.SelectedIndexChanged, ddl_Search_Area.SelectedIndexChanged, ddl_Search_Process.SelectedIndexChanged,
        ddl_Search_Pressure.SelectedIndexChanged, ddl_Search_CA.SelectedIndexChanged, ddl_Search_Service.SelectedIndexChanged, ddl_Search_Material.SelectedIndexChanged, ddl_Search_Insulation.SelectedIndexChanged,
        txt_search_Insulation_Thickness.TextChanged, txt_Search_LineNo.TextChanged, txt_Search_Size.TextChanged, txt_Search_Code.TextChanged
        BindTag()
    End Sub

    Private Sub ddl_Search_Plant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        Dim PLANT_ID As Integer = ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value
        Dim AREA_ID As Integer = ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value
        Dim ROUTE_ID As Integer = ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value
        If PLANT_ID = 0 Then
            BL.BindDDlArea(ddl_Search_Area, AREA_ID)
        Else
            BL.BindDDlArea(PLANT_ID, ddl_Search_Area, AREA_ID)
        End If
        PIPE.BindDDl_Route(PLANT_ID, ddl_Search_Route, ROUTE_ID)

        BindTag()
    End Sub

#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False
        lblUpdateMode.Text = "Create"
        '-----------------------------------
        pnlSearch.Visible = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        '----------- Save TAG ------------
        Dim Message As String = Pipe_Info.ValidateIncompleteMessage
        If Message <> "" Then
            '---------- Change To Tab Tag------------
            HTabTag_Click(Nothing, Nothing)
            lblValidation.Text = Message
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Route.SelectedIndex < 1 Then
            '---------- Change To Tab Tag------------
            HTabTag_Click(Nothing, Nothing)
            lblValidation.Text = "Select Routine Route"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim PT As DataTable = PointData()
        PT.DefaultView.RowFilter = "Point_Name='Main'"
        If PT.DefaultView.Count > 0 Then
            '---------- Change To Tab Point------------
            HTabPoint_Click(Nothing, Nothing)
            lblValidation.Text = "Avoid point 'Main'"
            pnlValidation.Visible = True
            Exit Sub
        End If
        '--------------- Check Duplicate Name -------
        For i As Integer = 0 To PT.Rows.Count - 1
            PT.DefaultView.RowFilter = "Point_Name='" & PT.Rows(i).Item("Point_Name").ToString.Replace("'", "''") & "'"
            If PT.DefaultView.Count > 1 Then
                '---------- Change To Tab Point------------
                HTabPoint_Click(Nothing, Nothing)
                lblValidation.Text = "Point name is duplicated"
                pnlValidation.Visible = True
                Exit Sub
            End If
        Next

        Try
            TAG_ID = Pipe_Info.SaveData()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        Dim SQL As String = "SELECT * FROM MS_PIPE_TAG WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow = DT.Rows(0)
        DR("ROUTE_ID") = ddl_Route.Items(ddl_Route.SelectedIndex).Value
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try
        '---------- Save Main Preview ----------------
        If IsNothing(POINT_MAIN_PREVIEW) Then
            PIPE.DROP_POINT_PREVIEW(TAG_ID, 0)
        Else
            PIPE.SAVE_POINT_PREVIEW(TAG_ID, 0, POINT_MAIN_PREVIEW)
        End If

        '----------- Remove Unused Point---------------
        PT.DefaultView.RowFilter = "POINT_ID>0" '---------- Except PointID IN -1(New Point), 0(Main) -------------
        PT = PT.DefaultView.ToTable.Copy
        Dim PointDB As DataTable = PIPE.Get_Point_Param(TAG_ID)
        PointDB.DefaultView.RowFilter = "POINT_ID>0" '---------- Except PointID IN 0(Main) -------------
        PointDB = PointDB.DefaultView.ToTable.Copy
        For i As Integer = 0 To PointDB.Rows.Count - 1
            PT.DefaultView.RowFilter = "POINT_ID=" & PointDB.Rows(i).Item("POINT_ID")
            If PT.DefaultView.Count = 0 Then
                PIPE.DROP_POINT(TAG_ID, PointDB.Rows(i).Item("POINT_ID"))
            End If
        Next
        PointDB.Dispose()
        '------------- Save Main Tag File Attachments--------------
        Dim Pics As List(Of FileAttachment) = Session(Pipe_File.UNIQUE_ID)
        Dim Docs As List(Of FileAttachment) = Session(Pipe_Doc.UNIQUE_ID)
        '------------- Merge Pic Into Doc -----------
        For i As Integer = 0 To Docs.Count - 1
            Pics.Add(Docs(i))
        Next

        PIPE.Save_FILE(TAG_ID, 0, Pics, True)
        '----------------- Clear Memory-----------
        For i As Integer = Pics.Count - 1 To 0 Step -1
            Dim F As FileAttachment = Pics(i)
            Pics.Remove(F)
            F = Nothing
        Next
        Pics = Nothing

        '-------------- Save Point --------------------------------
        PT = PointData()
        For i As Integer = 0 To PT.Rows.Count - 1
            SQL = "SELECT * FROM MS_PIPE_POINT WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & PT.Rows(i).Item("POINT_ID")
            DT = New DataTable
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DA.Fill(DT)
            Dim POINT_ID As Integer = PT.Rows(i).Item("POINT_ID")
            If DT.Rows.Count = 0 Then
                DR = DT.NewRow
                DT.Rows.Add(DR)
                POINT_ID = PIPE.Get_New_Point_ID(TAG_ID)
                DR("TAG_ID") = TAG_ID
                DR("POINT_ID") = POINT_ID
            Else
                DR = DT.Rows(0)
            End If
            DR("POINT_Name") = PT.Rows(i).Item("POINT_Name").ToString
            If Not IsDBNull(PT.Rows(i).Item("Component_Type")) AndAlso PT.Rows(i).Item("Component_Type") <> 0 Then
                DR("Component_Type") = PT.Rows(i).Item("Component_Type")
            Else
                DR("Component_Type") = DBNull.Value
            End If
            DR("Serial_No") = PT.Rows(i).Item("Serial_No").ToString
            If Not IsDBNull(PT.Rows(i).Item("Size")) Then
                DR("Size") = PT.Rows(i).Item("Size")
            Else
                DR("Size") = DBNull.Value
            End If
            DR("Location_From") = PT.Rows(i).Item("Location_From").ToString
            DR("Location_To") = PT.Rows(i).Item("Location_To").ToString
            If Not IsDBNull(PT.Rows(i).Item("MAT_CODE_ID")) AndAlso PT.Rows(i).Item("MAT_CODE_ID") <> 0 Then
                DR("MAT_CODE_ID") = PT.Rows(i).Item("MAT_CODE_ID")
            Else
                DR("MAT_CODE_ID") = DBNull.Value
            End If
            DR("Schedule") = PT.Rows(i).Item("Schedule").ToString
            If Not IsDBNull(PT.Rows(i).Item("PRS_ID")) AndAlso PT.Rows(i).Item("PRS_ID") <> 0 Then
                DR("PRS_ID") = PT.Rows(i).Item("PRS_ID")
            Else
                DR("PRS_ID") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("IN_ID")) AndAlso PT.Rows(i).Item("IN_ID") <> 0 Then
                DR("IN_ID") = PT.Rows(i).Item("IN_ID")
            Else
                DR("IN_ID") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("IN_Thickness")) Then
                DR("IN_Thickness") = PT.Rows(i).Item("IN_Thickness")
            Else
                DR("IN_Thickness") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Initial_Year")) Then
                DR("Initial_Year") = PT.Rows(i).Item("Initial_Year")
            Else
                DR("Initial_Year") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Norminal_Thickness")) Then
                DR("Norminal_Thickness") = PT.Rows(i).Item("Norminal_Thickness")
            Else
                DR("Norminal_Thickness") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("CA_ID")) Then
                DR("CA_ID") = PT.Rows(i).Item("CA_ID")
            Else
                DR("CA_ID") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Calculated_Thickness")) Then
                DR("Calculated_Thickness") = PT.Rows(i).Item("Calculated_Thickness")
            Else
                DR("Calculated_Thickness") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Pressure_Design")) Then
                DR("Pressure_Design") = PT.Rows(i).Item("Pressure_Design")
            Else
                DR("Pressure_Design") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Pressure_Operating")) Then
                DR("Pressure_Operating") = PT.Rows(i).Item("Pressure_Operating")
            Else
                DR("Pressure_Operating") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Temperature_Design")) Then
                DR("Temperature_Design") = PT.Rows(i).Item("Temperature_Design")
            Else
                DR("Temperature_Design") = DBNull.Value
            End If
            If Not IsDBNull(PT.Rows(i).Item("Temperature_Operating")) Then
                DR("Temperature_Operating") = PT.Rows(i).Item("Temperature_Operating")
            Else
                DR("Temperature_Operating") = DBNull.Value
            End If
            DR("P_ID_No") = PT.Rows(i).Item("P_ID_No").ToString
            DR("Update_By") = Session("USER_ID")
            DR("Update_Time") = Now

            '----------- Update Point ----------
            cmd = New SqlCommandBuilder(DA)
            DA.Update(DT)

            '------------ Save File ------------
            Dim Attachments As List(Of FileAttachment) = Session(PT.Rows(i).Item("UNIQUE_ID"))
            PIPE.Save_FILE(TAG_ID, POINT_ID, Attachments, True)
        Next
        '---------- Save Point Preview ----------------
        For i As Integer = 0 To PT.Rows.Count - 1
            Dim Preview As FileAttachment = Session(PT.Rows(i).Item("PREVIEW_ID"))
            If Not IsNothing(Preview) Then
                PIPE.SAVE_POINT_PREVIEW(TAG_ID, PT.Rows(i).Item("POINT_ID"), Preview)
            Else
                PIPE.DROP_POINT_PREVIEW(TAG_ID, PT.Rows(i).Item("POINT_ID"))
            End If
        Next
        '---------- Remove Unused Preview --------------
        SQL = "SELECT PREVIEW.POINT_ID " & vbLf
        SQL &= " From MS_PIPE_POINT_Preview PREVIEW" & vbLf
        SQL &= " LEFT Join MS_PIPE_POINT POINT On POINT.TAG_ID=PREVIEW.TAG_ID And POINT.POINT_ID=PREVIEW.POINT_ID" & vbLf
        SQL &= " Where PREVIEW.TAG_ID = " & TAG_ID & " And POINT.POINT_ID Is NULL" & vbLf
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            PIPE.DROP_POINT_PREVIEW(TAG_ID, DT.Rows(i).Item("POINT_ID"))
        Next


        '----------------- Clear Memory For Attachment-----------
        For i As Integer = 0 To PT.Rows.Count - 1
            Dim Attachments As List(Of FileAttachment) = Session(PT.Rows(i).Item("UNIQUE_ID"))
            For j As Integer = Attachments.Count - 1 To 0 Step -1
                Dim F As FileAttachment = Attachments(j)
                Attachments.Remove(F)
                F = Nothing
            Next
            Attachments = Nothing
        Next
        '----------------- Clear Memory For Preview-----------
        ClEAR_SUB_PREVIEW()

        ResetTag(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

        BindTag()

    End Sub

#Region "Tab Control"
    Private Sub ChangeTab(ByVal Tab As MyTab)
        Select Case Tab
            Case MyTab.Tag
                TabTag.Visible = True
                TabPoint.Visible = False

                HTabTag.CssClass = "Default-tab current"
                HTabPoint.CssClass = ""

                pnlSaveTag.Visible = True
            Case MyTab.Point
                TabTag.Visible = False
                TabPoint.Visible = True

                HTabTag.CssClass = ""
                HTabPoint.CssClass = "Default-tab current"

                pnlSaveTag.Visible = Not pnlEditPoint.Visible
        End Select
    End Sub

    Enum MyTab
        Tag = 0
        Point = 1
    End Enum

    Private Sub HTabTag_Click(sender As Object, e As EventArgs) Handles HTabTag.Click
        ChangeTab(MyTab.Tag)
    End Sub

    Private Sub HTabPoint_Click(sender As Object, e As EventArgs) Handles HTabPoint.Click
        ChangeTab(MyTab.Point)
    End Sub
#End Region

    Private Sub Pipe_Info_PropertyChangedByUser(ByRef Sender As UC_PIPE_TAG, Prop As UC_PIPE_TAG.PropertyItem) Handles Pipe_Info.PropertyChangedByUser
        Select Case Prop
            Case UC_PIPE_TAG.PropertyItem.PLANT
                lblTagCode.Text = Pipe_Info.TAG_CODE
                PIPE.BindDDl_Route(Pipe_Info.PLANT_ID, ddl_Route, ddl_Route.Items(ddl_Route.SelectedIndex).Value)
            Case UC_PIPE_TAG.PropertyItem.AREA
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_PIPE_TAG.PropertyItem.LOOP_NO
            Case UC_PIPE_TAG.PropertyItem.PROCESS
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_PIPE_TAG.PropertyItem.LINE_NO
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_PIPE_TAG.PropertyItem.SERVICE
            Case UC_PIPE_TAG.PropertyItem.PIPE_SIZE
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_PIPE_TAG.PropertyItem.SCHEDULE
            Case UC_PIPE_TAG.PropertyItem.INITIAL_YEAR
            Case UC_PIPE_TAG.PropertyItem.LOCATION_FROM
            Case UC_PIPE_TAG.PropertyItem.LOCATION_TO
            Case UC_PIPE_TAG.PropertyItem.NORMINAL_THICKNESS
            Case UC_PIPE_TAG.PropertyItem.CALCULATED_THICKNESS
            Case UC_PIPE_TAG.PropertyItem.INSULATION_ID
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_PIPE_TAG.PropertyItem.INSULATION_THICKNESS
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_PIPE_TAG.PropertyItem.DESIGN_PRESSURE
            Case UC_PIPE_TAG.PropertyItem.OPERATING_PRESSURE
            Case UC_PIPE_TAG.PropertyItem.DESIGN_TEMPERATURE
            Case UC_PIPE_TAG.PropertyItem.OPERATING_TEMPERATURE
            Case UC_PIPE_TAG.PropertyItem.MATERIAL
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_PIPE_TAG.PropertyItem.PRESSURE_CODE
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_PIPE_TAG.PropertyItem.CORROSION_ALLOWANCE
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_PIPE_TAG.PropertyItem.SERIAL_NO
                lblTagCode.Text = Pipe_Info.TAG_CODE
            Case UC_PIPE_TAG.PropertyItem.PIPE_CLASS
            Case UC_PIPE_TAG.PropertyItem.P_ID_No
        End Select
    End Sub

#Region "TabPoint"

    Private Function BlankPointDataTable() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("POINT_ID", GetType(Integer))
        DT.Columns.Add("POINT_Name", GetType(String))
        DT.Columns.Add("Component_Type", GetType(Integer))
        DT.Columns.Add("Component_Type_Name", GetType(String))
        DT.Columns.Add("Serial_No", GetType(String))
        DT.Columns.Add("Size", GetType(String))
        DT.Columns.Add("Location_From", GetType(String))
        DT.Columns.Add("Location_To", GetType(String))
        DT.Columns.Add("MAT_CODE_ID", GetType(Integer))
        DT.Columns.Add("Schedule", GetType(String))
        DT.Columns.Add("PRS_ID", GetType(Integer))
        DT.Columns.Add("IN_ID", GetType(Integer))
        DT.Columns.Add("IN_Thickness", GetType(Integer))
        DT.Columns.Add("Initial_Year", GetType(Integer))
        DT.Columns.Add("Norminal_Thickness", GetType(Double))
        DT.Columns.Add("CA_ID", GetType(Integer))
        DT.Columns.Add("Calculated_Thickness", GetType(Double))
        DT.Columns.Add("Pressure_Design", GetType(Double))
        DT.Columns.Add("Pressure_Operating", GetType(Double))
        DT.Columns.Add("Temperature_Design", GetType(Double))
        DT.Columns.Add("Temperature_Operating", GetType(Double))
        DT.Columns.Add("P_ID_No", GetType(String))
        DT.Columns.Add("UNIQUE_ID", GetType(String)) '----------- เก็บ Unique_ID ของ UC_PictureAlbum.Attachments -----------
        DT.Columns.Add("PREVIEW_ID", GetType(String))
        Return DT
    End Function

    Private Sub rptPoint_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptPoint.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim trPoint As HtmlTableRow = e.Item.FindControl("trPoint")
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblComponent As Label = e.Item.FindControl("lblComponent")
        Dim lblFile As Label = e.Item.FindControl("lblFile")
        Dim lblLife As Label = e.Item.FindControl("lblLife")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim btnDelete_Confirm As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("btnDelete_Confirm")
        Dim btnPreview As Button = e.Item.FindControl("btnPreview")

        lblNo.Text = e.Item.ItemIndex + 1
        lblName.Text = e.Item.DataItem("POINT_Name").ToString
        lblComponent.Text = e.Item.DataItem("Component_Type_Name").ToString
        Try
            Dim Attachements As List(Of FileAttachment) = Session(e.Item.DataItem("UNIQUE_ID").ToString)
            If Attachements.Count = 0 Then
                lblFile.Text = "-"
            Else
                lblFile.Text = Attachements.Count
            End If
        Catch ex As Exception
            lblFile.Text = "-"
        End Try
        lblLife.Text = "-" '------ Hardcode--------
        btnDelete_Confirm.ConfirmText = "Are you sure you want To delete " & lblName.Text & " ?"
        trPoint.Attributes("onclick") = "document.getElementById('" & btnPreview.ClientID & "').click();"

        '--------------- Collect Parameter --------------
        Dim BT As DataTable = BlankPointDataTable()
        Dim DT As DataTable = rptPoint.DataSource
        For i As Integer = 0 To BT.Columns.Count - 1
            Dim Param As String = BT.Columns(i).ColumnName
            If DT.Columns.IndexOf(Param) > -1 Then
                trPoint.Attributes(Param) = e.Item.DataItem(Param).ToString
            End If
        Next

        'POINT_ID
        'POINT_Name
        'Component_Type
        'Component_Type_Name
        'Serial_No
        'Size
        'Location_From
        'Location_To
        'MAT_CODE_ID
        'Schedule
        'PRS_ID
        'IN_ID
        'IN_Thickness
        'Initial_Year
        'Norminal_Thickness
        'CA_ID
        'Calculated_Thickness
        'Pressure_Design
        'Pressure_Operating
        'Temperature_Design
        'Temperature_Operating
        'P_ID_No
        'UNIQUE_ID
        'PREVIEW_ID
    End Sub

    Private Sub PreviewSelectedPoint()
        If SELECTED_POINT_INDEX = -1 Then
            '------------------- Preview Main Tag-------------------
            Dim Preview As FileAttachment = POINT_MAIN_PREVIEW
            If IsNothing(Preview) Then
                pointOverview.BackImageUrl = ""
            Else
                pointOverview.BackImageUrl = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=" & txtMainPreviewID.Text & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
            End If
        Else
            Dim trPoint As HtmlTableRow = rptPoint.Items(SELECTED_POINT_INDEX).FindControl("trPoint")
            Dim PREVIEW_ID As String = trPoint.Attributes("PREVIEW_ID")
            Dim Preview As FileAttachment = Session(PREVIEW_ID)
            '-------------- If Point has no preview use main preview-------------
            If IsNothing(Preview) Then
                If IsNothing(POINT_MAIN_PREVIEW) Then
                    pointOverview.BackImageUrl = ""
                Else
                    pointOverview.BackImageUrl = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=" & POINT_MAIN_PREVIEW.UNIQUE_ID & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
                End If
            Else
                pointOverview.BackImageUrl = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=" & PREVIEW_ID & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
            End If
        End If
        lnkPointPreview.HRef = pointOverview.BackImageUrl
    End Sub

    Private Sub rptPoint_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptPoint.ItemCommand
        Select Case e.CommandName
            Case "Preview"
                If SELECTED_POINT_INDEX = e.Item.ItemIndex Then
                    SELECTED_POINT_INDEX = -1
                Else
                    SELECTED_POINT_INDEX = e.Item.ItemIndex
                End If
                '---------- Preview ---------
                PreviewSelectedPoint()
            Case "Edit"
                ClearPanelEditPoint()
                Dim DR As DataRow = PointData().Rows(e.Item.ItemIndex)
                '------------ Bind Data To Point_Info --------------
                With Point_Info
                    .POINT_ID = DR("POINT_ID")
                    .POINT_NAME = DR("POINT_NAME")
                    If Not IsDBNull(DR("Component_Type")) Then .COMPONENT_TYPE_ID = DR("Component_Type")
                    .SERIAL_NO = DR("Serial_No").ToString
                    If Not IsDBNull(DR("Size")) Then .PIPE_SIZE = DR("Size")
                    .LOCATION_FROM = DR("Location_From").ToString
                    .LOCATION_TO = DR("Location_To").ToString
                    If Not IsDBNull(DR("MAT_CODE_ID")) Then .MAT_CODE_ID = DR("MAT_CODE_ID")
                    .SCHEDULE = DR("Schedule").ToString
                    If Not IsDBNull(DR("PRS_ID")) Then .PRESSURE_ID = DR("PRS_ID")
                    If Not IsDBNull(DR("IN_ID")) Then .INSULATION_ID = DR("IN_ID")
                    If Not IsDBNull(DR("IN_Thickness")) Then .INSULATION_THICKNESS = DR("IN_Thickness")
                    If Not IsDBNull(DR("Initial_Year")) Then .INITIAL_YEAR = DR("Initial_Year")
                    If Not IsDBNull(DR("Norminal_Thickness")) Then .NORMINAL_THICKNESS = DR("Norminal_Thickness")
                    If Not IsDBNull(DR("CA_ID")) Then .CA_ID = DR("CA_ID")
                    If Not IsDBNull(DR("Calculated_Thickness")) Then .CALCULATED_THICKNESS = DR("Calculated_Thickness")
                    If Not IsDBNull(DR("Pressure_Design")) Then .PRESSURE_DESIGN = DR("Pressure_Design")
                    If Not IsDBNull(DR("Pressure_Operating")) Then .PRESSURE_OPERATING = DR("Pressure_Operating")
                    If Not IsDBNull(DR("Temperature_Design")) Then .TEMPERATURE_DESIGN = DR("Temperature_Design")
                    If Not IsDBNull(DR("Temperature_Operating")) Then .TEMPERATURE_OPERATING = DR("Temperature_Operating")
                    .P_ID_No = DR("P_ID_No").ToString
                    .UNIQUE_ID = DR("UNIQUE_ID")
                End With
                Point_File.UNIQUE_ID = DR("UNIQUE_ID")
                Point_File.BindData()
                '------------ Update Sub Preview ----------------
                txtPointPreviewID.Text = DR("PREVIEW_ID")
                Render_Point_Sub_Preview()

                '------------ Toggle Show/Hide Panel--
                pnlEditPoint.Visible = True
                pnlListPoint.Visible = False
                pnlSaveTag.Visible = False
                lblEditPoint.Text = "Edit point"
            Case "Delete"
                Dim DT As DataTable = PointData()
                '------------ Clear Point_File-----------
                Dim Attachments As List(Of FileAttachment) = Session(DT.Rows(e.Item.ItemIndex).Item("UNIQUE_ID"))
                While Attachments.Count > 0
                    Dim F As FileAttachment = Attachments(0)
                    Attachments.Remove(F)
                    F = Nothing
                End While
                '------------ Remove Preview -----------------
                Dim trPoint As HtmlTableRow = e.Item.FindControl("trPoint")
                DETACH_SUB_PREVIEW(trPoint.Attributes("PREVIEW_ID"))
                '------------ Delete From Point_Data----------
                DT.Rows.RemoveAt(e.Item.ItemIndex)
                rptPoint.DataSource = DT
                rptPoint.DataBind()
        End Select
    End Sub

    Private Sub ClearPanelEditPoint()

        '------------ Point Info -------------
        Point_Info.UNIQUE_ID = GenerateNewUniqueID()
        Point_Info.BindData(TAG_ID, -1)
        Point_Info.RaisePropertyChangedByUser = False '------- Prevent Auto Postback ----------
        '------------ Point File -------------
        Point_File.UNIQUE_ID = Point_Info.UNIQUE_ID
        Point_File.Attachments = New List(Of FileAttachment)
        Point_File.BindData()
        '------------ Point _Preview ---------
        txtPointPreviewID.Text = ""
        '------------ Toggle Show/Hide Panel--
        pnlEditPoint.Visible = False
        pnlListPoint.Visible = True
        pnlSaveTag.Visible = True
        lblEditPoint.Text = "Add new point"


    End Sub

    Private Sub btnAddPoint_Click(sender As Object, e As EventArgs) Handles btnAddPoint.Click
        ClearPanelEditPoint()
        pnlEditPoint.Visible = True
        pnlListPoint.Visible = False
        pnlSaveTag.Visible = False

        '---------- Set Default Main point value----------
        With Point_Info
            .SERIAL_NO = Pipe_Info.SERIAL_NO
            .MAT_CODE_ID = Pipe_Info.MAT_CODE_ID
            .INSULATION_ID = Pipe_Info.INSULATION_ID
            .PRESSURE_ID = Pipe_Info.PRESSURE_ID
            .PIPE_SIZE = Pipe_Info.PIPE_SIZE
            .SCHEDULE = Pipe_Info.SCHEDULE
            .LOCATION_FROM = Pipe_Info.LOCATION_FROM
            .LOCATION_TO = Pipe_Info.LOCATION_TO
            .INITIAL_YEAR = Pipe_Info.INITIAL_YEAR
            .NORMINAL_THICKNESS = Pipe_Info.NORMINAL_THICKNESS
            .CALCULATED_THICKNESS = Pipe_Info.CALCULATED_THICKNESS
            .INSULATION_THICKNESS = Pipe_Info.INSULATION_THICKNESS
            .CA_ID = Pipe_Info.CA_ID
            .PRESSURE_DESIGN = Pipe_Info.PRESSURE_DESIGN
            .PRESSURE_OPERATING = Pipe_Info.PRESSURE_OPERATING
            .TEMPERATURE_DESIGN = Pipe_Info.TEMPERATURE_DESIGN
            .TEMPERATURE_OPERATING = Pipe_Info.TEMPERATURE_OPERATING
        End With
        txtPointPreviewID.Text = GenerateNewUniqueID()
        Render_Point_Sub_Preview()
    End Sub

    Private Sub btnOKPoint_Click(sender As Object, e As EventArgs) Handles btnOKPoint.Click

        '------------- Copy Validate From Point Info.Save------------
        Dim Msg As String = Point_Info.ValidateIncompleteMessage()
        If Msg <> "" Then
            pnlBindingPointError.Visible = True
            lblBindingPointError.Text = Msg
            Exit Sub
        End If

        '--------------- Check Duplicate--------------
        Dim PT As DataTable = PointData()
        PT.DefaultView.RowFilter = "POINT_NAME='" & Point_Info.POINT_NAME.Replace("'", "''") & "' AND UNIQUE_ID<>'" & Point_Info.UNIQUE_ID.Replace("'", "''") & "'"
        If PT.DefaultView.Count > 0 Then
            pnlBindingPointError.Visible = True
            lblBindingPointError.Text = "This point name is already existed"
            Exit Sub
        End If
        PT.DefaultView.RowFilter = "UNIQUE_ID='" & Point_Info.UNIQUE_ID.Replace("'", "''") & "'"
        Dim PR As DataRow
        If PT.DefaultView.Count = 0 Then
            PR = PT.NewRow
            PT.Rows.Add(PR)
        Else
            PR = PT.DefaultView(0).Row
        End If
        PT.DefaultView.RowFilter = ""

        With Point_Info
            PR("POINT_ID") = .POINT_ID
            PR("POINT_Name") = .POINT_NAME
            If .COMPONENT_TYPE_ID <> 0 Then
                PR("Component_Type") = .COMPONENT_TYPE_ID
                PR("Component_Type_Name") = .COMPONENT_TYPE_NAME
            Else
                PR("Component_Type") = DBNull.Value
                PR("Component_Type_Name") = DBNull.Value
            End If
            PR("Serial_No") = .SERIAL_NO
            If IsNumeric(.PIPE_SIZE) Then
                PR("Size") = .PIPE_SIZE
            Else
                PR("Size") = ""
            End If
            PR("Location_From") = .LOCATION_FROM
            PR("Location_To") = .LOCATION_TO
            If .MAT_CODE_ID <> 0 Then
                PR("MAT_CODE_ID") = .MAT_CODE_ID
            Else
                PR("MAT_CODE_ID") = DBNull.Value
            End If
            PR("Schedule") = .SCHEDULE
            If .PRESSURE_ID <> 0 Then
                PR("PRS_ID") = .PRESSURE_ID
            Else
                PR("PRS_ID") = DBNull.Value
            End If
            If .INSULATION_ID <> 0 Then
                PR("IN_ID") = .INSULATION_ID
            Else
                PR("IN_ID") = DBNull.Value
            End If
            If IsNumeric(.INSULATION_THICKNESS) Then
                PR("IN_Thickness") = .INSULATION_THICKNESS
            Else
                PR("IN_Thickness") = DBNull.Value
            End If
            If IsNumeric(.INITIAL_YEAR) Then
                PR("Initial_Year") = .INITIAL_YEAR
            Else
                PR("Initial_Year") = DBNull.Value
            End If
            If IsNumeric(.NORMINAL_THICKNESS) Then
                PR("Norminal_Thickness") = .NORMINAL_THICKNESS
            Else
                PR("Norminal_Thickness") = DBNull.Value
            End If
            If .CA_ID <> 0 Then
                PR("CA_ID") = .CA_ID
            Else
                PR("CA_ID") = DBNull.Value
            End If
            If IsNumeric(.CALCULATED_THICKNESS) Then
                PR("Calculated_Thickness") = .CALCULATED_THICKNESS
            Else
                PR("Calculated_Thickness") = DBNull.Value
            End If
            If IsNumeric(.PRESSURE_DESIGN) Then
                PR("Pressure_Design") = .PRESSURE_DESIGN
            Else
                PR("Pressure_Design") = DBNull.Value
            End If
            If IsNumeric(.PRESSURE_OPERATING) Then
                PR("Pressure_Operating") = .PRESSURE_OPERATING
            Else
                PR("Pressure_Operating") = DBNull.Value
            End If
            If IsNumeric(.TEMPERATURE_DESIGN) Then
                PR("Temperature_Design") = .TEMPERATURE_DESIGN
            Else
                PR("Temperature_Design") = DBNull.Value
            End If
            If IsNumeric(.TEMPERATURE_OPERATING) Then
                PR("Temperature_Operating") = .TEMPERATURE_OPERATING
            Else
                PR("Temperature_Operating") = DBNull.Value
            End If
            PR("P_ID_No") = .P_ID_No
            PR("UNIQUE_ID") = .UNIQUE_ID
            '------------- Keep Preview ----------------------
            PR("PREVIEW_ID") = txtPointPreviewID.Text
            Dim Obj As FileAttachment = Session(txtPointPreviewID.Text)
            If Not IsNothing(Obj) Then
                UPDATE_SUB_PREVIEW(Obj)
            Else
                DETACH_SUB_PREVIEW(txtPointPreviewID.Text)
            End If
            '------------- End Preview -----------------------
        End With
        '------------- BindRepeater ----------------------
        rptPoint.DataSource = PT
        rptPoint.DataBind()

        pnlEditPoint.Visible = False
        pnlListPoint.Visible = True
        pnlSaveTag.Visible = True

        '------------ Preview Saved Point ---------------
        SELECTED_POINT_INDEX = -1
        For Each item As RepeaterItem In rptPoint.Items
            If item.ItemType <> ListItemType.Item And item.ItemType <> ListItemType.AlternatingItem Then Continue For
            'PR("POINT_ID")
            Dim trPoint As HtmlTableRow = item.FindControl("trPoint")
            If trPoint.Attributes("POINT_ID") = PR("POINT_ID") Then
                SELECTED_POINT_INDEX = item.ItemIndex
                Exit For
            End If
        Next
        PreviewSelectedPoint()

    End Sub

    Private Sub UPDATE_SUB_PREVIEW(ByRef Attachment As FileAttachment)
        For i As Integer = 0 To POINT_SUB_PREVIEW.Count - 1
            If POINT_SUB_PREVIEW(i).UNIQUE_ID = Attachment.UNIQUE_ID Then
                '--------------- Update Existing ---------------
                POINT_SUB_PREVIEW(i) = Attachment
                Exit Sub
            End If
        Next
        '------------- Append Existing ---------------- 
        POINT_SUB_PREVIEW.Add(Attachment)
    End Sub

    Private Sub DETACH_SUB_PREVIEW(ByRef UNIQUE_ID As String)
        For i As Integer = 0 To POINT_SUB_PREVIEW.Count - 1
            Dim Attachment As FileAttachment = Session(UNIQUE_ID)
            POINT_SUB_PREVIEW.Remove(Attachment)
            Attachment = Nothing
        Next
    End Sub

    Private Sub ClEAR_SUB_PREVIEW()
        For i As Integer = POINT_SUB_PREVIEW.Count - 1 To 0 Step -1
            Dim Attachment As FileAttachment = POINT_SUB_PREVIEW(i)
            POINT_SUB_PREVIEW.Remove(Attachment)
            Attachment = Nothing
        Next
    End Sub

    Private Sub btnCancelPoint_Click(sender As Object, e As EventArgs) Handles btnCancelPoint_Bottom.Click, btnCancelPoint_Top.Click
        pnlEditPoint.Visible = False
        pnlListPoint.Visible = True
        pnlSaveTag.Visible = True
    End Sub




#End Region


End Class