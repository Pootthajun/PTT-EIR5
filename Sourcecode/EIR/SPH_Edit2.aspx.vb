﻿Imports System.Data
Imports System.Data.SqlClient
Public Class SPH_Edit2
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Public Property RPT_ID() As Integer
        Get
            If IsNumeric(ViewState("RPT_ID")) Then
                Return ViewState("RPT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_ID = Request.QueryString("RPT_ID")

            If RPT_ID = 0 Then
                Response.Redirect("SPH_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM SPH_RPT_Header WHERE RPT_ID=" & RPT_ID, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='SPH_Summary.aspx'", True)
                    Exit Sub
                End If

                '---------------- Get RPT_Year + RPT_No
                RPT_Year = DT.Rows(0)("RPT_Year")
                RPT_No = DT.Rows(0)("RPT_No")
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE SPH_RPT_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_ID=" & RPT_ID
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE SPH_RPT_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_ID=" & RPT_ID
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE SPH_RPT_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_ID=" & RPT_ID
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")
            BindTabData()
        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_SPH_REPORT_HEADER WHERE RPT_ID=" & RPT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='SPH_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reason\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='SPH_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='SPH_Summary.aspx'", True)
            Exit Sub
        End If
    End Sub

    Private Sub BindTabData()
        Dim SQL As String = "SELECT COUNT(1) FROM SPH_RPT_Detail WHERE RPT_ID=" & RPT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        lbl_Spring.Text = FormatNumber(DT.Rows(0)(0), 0)

        '------------------------------Header -----------------------------------
        SQL = "SELECT VW.* " & vbLf
        SQL &= " FROM VW_SPH_REPORT_HEADER VW " & vbLf
        SQL &= " WHERE RPT_ID=" & RPT_ID & vbLf
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("SPH_Summary.aspx")
            Exit Sub
        End If

        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        lbl_Year.Text = RPT_Year

        '---------------------------Bind Vibration ------------------------------
        DT = New DataTable
        'SQL = " SELECT SPH.SPH_ID,DETAIL_ID,SPH_No,Pipe_No," & vbLf
        'SQL &= " Size_Inch,Load_Install,Load_Oper,Length_Cold,Length_Hot," & vbLf
        'SQL &= " Detail.INSP_Level, Detail.Corrosion, Detail.Comment" & vbLf
        'SQL &= " FROM SPH_RPT_Header Header" & vbLf
        'SQL &= " INNER JOIN SPH_RPT_Detail Detail ON Header.RPT_ID=Detail.RPT_ID" & vbLf
        'SQL &= " INNER JOIN SPH_MS_Spring SPH ON Detail.SPH_ID=SPH.SPH_ID" & vbLf
        'SQL &= " WHERE Header.RPT_ID = " & RPT_ID & vbLf
        'SQL &= " ORDER BY Pipe_No,SPH_No" & vbLf

        SQL = " SELECT SPH.SPH_ID,DETAIL_ID,SPH_No,Pipe_No," & vbLf
        SQL &= " Length_Cold,Length_Hot," & vbLf
        SQL &= " Detail.INSP_Level, Detail.Corrosion,Detail.PossibleCause, Detail.Comment" & vbLf
        SQL &= " FROM SPH_RPT_Header Header" & vbLf
        SQL &= " INNER JOIN SPH_RPT_Detail Detail ON Header.RPT_ID=Detail.RPT_ID" & vbLf
        SQL &= " INNER JOIN SPH_MS_Spring SPH ON Detail.SPH_ID=SPH.SPH_ID" & vbLf
        SQL &= " WHERE Header.RPT_ID = " & RPT_ID & vbLf
        SQL &= " ORDER BY Pipe_No,SPH_No" & vbLf

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        rptINSP.DataSource = DT
        rptINSP.DataBind()

    End Sub

    Protected Sub rptINSP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptINSP.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Select Case e.CommandName
            Case "Save" '----------------- Auto Save -----------------
                Dim txtINSP As TextBox = e.Item.FindControl("txtINSP")
                Dim txtCorrosion As TextBox = e.Item.FindControl("txtCorrosion")
                Dim txtCause As TextBox = e.Item.FindControl("txtCause")
                Dim txtComment As TextBox = e.Item.FindControl("txtComment")

                Dim SQL As String = "SELECT * FROM SPH_RPT_Detail WHERE DETAIL_ID=" & e.CommandArgument
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    DT.Rows(0).Item("INSP_Level") = txtINSP.Text
                    DT.Rows(0).Item("Corrosion") = txtCorrosion.Text
                    DT.Rows(0).Item("PossibleCause") = txtCause.Text
                    DT.Rows(0).Item("Comment") = txtComment.Text

                    Dim cmb As New SqlCommandBuilder(DA)
                    DA.Update(DT)

                End If

                Dim lblResult As Label = e.Item.FindControl("lblResult")
                Dim lblCold As Label = e.Item.FindControl("lblCold")
                Dim lblHot As Label = e.Item.FindControl("lblHot")
                lblResult.Text = BL.SPH_CalculateTravelStatus(lblCold.Text, lblHot.Text, txtINSP.Text)
                Select Case lblResult.Text
                    Case "Unknow"
                        lblResult.ForeColor = Drawing.Color.Black
                        txtINSP.ForeColor = Drawing.Color.Black
                    Case "Accepted"
                        lblResult.ForeColor = Drawing.Color.Green
                        txtINSP.ForeColor = Drawing.Color.Green
                    Case "Adjusted"
                        lblResult.ForeColor = Drawing.Color.Red
                        txtINSP.ForeColor = Drawing.Color.Red
                End Select

        End Select
    End Sub

    Protected Sub rptINSP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptINSP.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblPipe As Label = e.Item.FindControl("lblPipe")
        Dim lblSpring As Label = e.Item.FindControl("lblSpring")
        'Dim lblSize As Label = e.Item.FindControl("lblSize")
        'Dim lblInst As Label = e.Item.FindControl("lblInst")
        'Dim lblOper As Label = e.Item.FindControl("lblOper")
        Dim lblCold As Label = e.Item.FindControl("lblCold")
        Dim lblHot As Label = e.Item.FindControl("lblHot")
        Dim txtINSP As TextBox = e.Item.FindControl("txtINSP")
        Dim lblResult As Label = e.Item.FindControl("lblResult")
        Dim txtCorrosion As TextBox = e.Item.FindControl("txtCorrosion")
        Dim txtCause As TextBox = e.Item.FindControl("txtCause")
        Dim txtComment As TextBox = e.Item.FindControl("txtComment")
        Dim btnUpdate As Button = e.Item.FindControl("btnUpdate")

        lblNo.Text = e.Item.ItemIndex + 1
        lblPipe.Text = e.Item.DataItem("Pipe_No")
        lblSpring.Text = e.Item.DataItem("SPH_No")
        'If Not IsDBNull(e.Item.DataItem("Size_Inch")) Then
        '    lblSize.Text = FormatNumber(e.Item.DataItem("Size_Inch"), 1)
        'End If
        'If Not IsDBNull(e.Item.DataItem("Load_Install")) Then
        '    lblInst.Text = FormatNumber(e.Item.DataItem("Load_Install"), 1)
        'End If
        'If Not IsDBNull(e.Item.DataItem("Load_Oper")) Then
        '    lblOper.Text = FormatNumber(e.Item.DataItem("Load_Oper"), 1)
        'End If
        If Not IsDBNull(e.Item.DataItem("Length_Cold")) Then
            lblCold.Text = FormatNumber(e.Item.DataItem("Length_Cold"), 1)
        End If
        If Not IsDBNull(e.Item.DataItem("Length_Hot")) Then
            lblHot.Text = FormatNumber(e.Item.DataItem("Length_Hot"), 1)
        End If
        'INSP_Level
        If Not IsDBNull(e.Item.DataItem("INSP_Level")) Then
            txtINSP.Text = e.Item.DataItem("INSP_Level")
        End If

        If Not IsDBNull(e.Item.DataItem("Corrosion")) Then
            txtCorrosion.Text = e.Item.DataItem("Corrosion")
        End If
        If Not IsDBNull(e.Item.DataItem("PossibleCause")) Then
            txtCause.Text = e.Item.DataItem("PossibleCause")
        End If
        If Not IsDBNull(e.Item.DataItem("Comment")) Then
            txtComment.Text = e.Item.DataItem("Comment")
        End If

        lblResult.Text = BL.SPH_CalculateTravelStatus(e.Item.DataItem("Length_Cold"), e.Item.DataItem("Length_Hot"), e.Item.DataItem("INSP_Level"))
        Select Case lblResult.Text
            Case "Unknow"
                lblResult.ForeColor = Drawing.Color.Black
                txtINSP.ForeColor = Drawing.Color.Black
            Case "Accepted"
                lblResult.ForeColor = Drawing.Color.Green
                txtINSP.ForeColor = Drawing.Color.Green
            Case "Adjusted"
                lblResult.ForeColor = Drawing.Color.Red
                txtINSP.ForeColor = Drawing.Color.Red
        End Select

        ImplementJavaSpringHangerInspection(txtINSP)
        txtINSP.Attributes("onchange") &= "document.getElementById('" & btnUpdate.ClientID & "').click();"
        txtCorrosion.Attributes("onchange") = "document.getElementById('" & btnUpdate.ClientID & "').click();"
        txtCause.Attributes("onchange") &= "document.getElementById('" & btnUpdate.ClientID & "').click();"
        txtComment.Attributes("onchange") &= "document.getElementById('" & btnUpdate.ClientID & "').click();"

        btnUpdate.CommandArgument = e.Item.DataItem("DETAIL_ID")

    End Sub

    Protected Sub btn_Buffer_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Buffer_Refresh.Click, lnkRefresh.Click
        BindTabData()
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click
        Try
            'PTClass.Drop_RPT_Detail(Session("User_ID"), RPT_Year, RPT_No)
            Dim Com As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            Com.CommandType = CommandType.Text
            Com.Connection = Conn
            Com.CommandText = "UPDATE SPH_RPT_Detail set INSP_Level='',Corrosion='' WHERE RPT_ID=" & RPT_ID
            Com.ExecuteNonQuery()
            Com.Dispose()
            Conn.Close()
        Catch ex As Exception
            Exit Sub
        End Try
        BindTabData()
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

#Region "Navigator"

    Protected Sub btn_Back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Back.Click, HTabHeader.Click
        'SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit1.aspx?RPT_ID=" & RPT_ID & "';", True)
    End Sub

    Protected Sub HTabPicture_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabPicture.Click, btn_Next.Click
        'SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit3.aspx?RPT_ID=" & RPT_ID & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
        'SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit4.aspx?RPT_ID=" & RPT_ID & "';", True)
    End Sub
#End Region

#Region "Saving"
    'Private Sub SaveData(ByVal Sender As Object)

    'End Sub
#End Region

End Class