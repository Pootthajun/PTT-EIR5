﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="PIPE_Routine_Edit2.aspx.vb" Inherits="EIR.PIPE_Routine_Edit2" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="GL_DialogPipeRoutineDetail.ascx" tagname="GL_DialogPipeRoutineDetail" tagprefix="uc2" %>
<%@ Register src="GL_PipeRoutineTag.ascx" tagname="GL_PipeRoutineTag" tagprefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- Page Head -->
       
<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>

	    
			<h2>Edit PIPE Routine Report</h2>

		
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server" CssClass="default-tab current">Report Detail</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p style="font-weight:bold;">
								<label class="column-left" style="width:120px;" >Report for: </label>
								<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
								<asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Round <asp:Label ID="lbl_Round" runat="server" Text="Round" CssClass="EditReportHeader"></asp:Label>
								    | Period <asp:Label ID="lbl_Period" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								</p>
								
								<ul class="shortcut-buttons-set">
							      <li>
							      <asp:LinkButton ID="lnkUpload" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/image_add_48.png" alt="icon" /><br />
							            Add Inspection
							          </span>
							      </asp:LinkButton>
							      </li>
							      <li>
							        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear all
							          </span>
							        </asp:LinkButton>
							          <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all inspection for this report permanently?">
                                      </cc1:ConfirmButtonExtender>
							      </li>
								  <li>
								    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" /><br />
									    Reset this tab
									    </span>
								    </asp:LinkButton>
								  </li>
								 
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon"/><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								  </li>
					        </ul>
								
								<table width="900" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
								   <thead>
                                    <tr>
                                      <th bgcolor="#EEEEEE" style="text-align:center; ">Tag</th>
                                      <th rowspan="2" bgcolor="#EEEEEE" style="text-align:center; width:100px;">Inspection(s)</th>
                                      <th colspan="2" bgcolor="#EEEEEE" style="text-align:center; width:125px;">Problem(s)</span></th>
                                      <th rowspan="2" bgcolor="#EEEEEE" style="text-align:center; width:75px;">Trace</th>
                                      <th rowspan="2" bgcolor="#EEEEEE" style="text-align:center; width:100px;">Current</th>
                                      <th rowspan="2" bgcolor="#EEEEEE" style="text-align:center; width:50px;">Action</th>
                                    </tr>
                                    <tr>
                                      <th bgcolor="#EEEEEE" style="text-align:center;">
                                          <asp:TextBox ID="txtSearchTag" runat="server" MaxLength="30" Height="12px" style="position:relative; top:2px;" Width="180px" CssClass="text-input small-input " ></asp:TextBox>
                                          <asp:button ID="btnSearchTag" runat="server" Text="Search" CssClass="button" /></th>
                                      <th bgcolor="#EEEEEE" style="text-align:center; width:75px;">Remain</th>
                                      <th bgcolor="#EEEEEE" style="text-align:center; width:50px;">New</th>
                                     </tr>
								    </thead>
								</table>
								<asp:Repeater ID="rptInspection" runat="server">
                                 <ItemTemplate>                                 
                                
                                    <uc1:GL_PipeRoutineTag ID="GLT" runat="server" />
                                    
                                  </ItemTemplate>
                                </asp:Repeater>                                
                                 
                               </table>
								
							<br>
							<p align="right" style="margin-top:20px;">
								<asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
								<asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
							</p>

							</fieldset>
				    </div>
				  <!-- End #tabDetail -->        
		          
		          <uc2:GL_DialogPipeRoutineDetail ID="DialogDetail" runat="server" />
		          
		       	 <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
		         
	              
			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			

</ContentTemplate>
</asp:UpdatePanel>  

</asp:Content>
