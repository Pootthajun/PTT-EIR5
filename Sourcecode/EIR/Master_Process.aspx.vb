﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Master_Process
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetProcess(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindProcess()

        Dim SQL As String = "SELECT " & vbNewLine
        SQL &= " MS_Process.PROC_ID," & vbNewLine
        SQL &= " PROC_Code," & vbNewLine
        SQL &= " PROC_Name," & vbNewLine
        SQL &= "  COUNT(TAG_ID) TotalTag," & vbNewLine
        SQL &= " PROC_Description," & vbNewLine
        SQL &= " MS_Process.active_status," & vbNewLine
        SQL &= " MS_Process.Update_By," & vbNewLine
        SQL &= " MS_Process.Update_Time" & vbNewLine
        SQL &= " FROM MS_Process " & vbNewLine
        SQL &= " LEFT JOIN (SELECT * FROM MS_ST_TAG WHERE Active_Status=1 UNION ALL SELECT * FROM MS_RO_TAG WHERE Active_Status=1) MS_TAG ON MS_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine
        SQL &= " Group BY " & vbNewLine
        SQL &= " MS_Process.PROC_ID," & vbNewLine
        SQL &= " PROC_Code," & vbNewLine
        SQL &= " PROC_Name," & vbNewLine
        SQL &= " PROC_Description," & vbNewLine
        SQL &= " MS_Process.active_status," & vbNewLine
        SQL &= " MS_Process.Update_By," & vbNewLine
        SQL &= " MS_Process.Update_Time" & vbNewLine


        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("MS_Process") = DT

        Navigation.SesssionSourceName = "MS_Process"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptProcess
    End Sub

    Protected Sub rptProcess_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptProcess.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblProcessCode As Label = e.Item.FindControl("lblProcessCode")
        Dim lblProcessName As Label = e.Item.FindControl("lblProcessName")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblDesc As Label = e.Item.FindControl("lblDesc")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblProcessCode.Text = e.Item.DataItem("PROC_Code")
        lblProcessName.Text = e.Item.DataItem("PROC_NAME")
        lblTag.Text = e.Item.DataItem("TotalTag")
        lblDesc.Text = BL.ReportGridDescription(e.Item.DataItem("PROC_Description"), 20)
        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.Attributes("Process_ID") = e.Item.DataItem("PROC_ID")

    End Sub

    Protected Sub rptProcess_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptProcess.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim Process_ID As Integer = btnEdit.Attributes("Process_ID")
        Select Case e.CommandName
            Case "Edit"
                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                'txtProcessCode.ReadOnly = True
                txtProcessName.Focus()
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListProcess.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT * FROM MS_Process WHERE PROC_ID=" & Process_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Process Not Found"
                    pnlBindingError.Visible = True
                    BindProcess()
                    Exit Sub
                End If

                txtProcessCode.Text = DT.Rows(0).Item("PROC_CODE")
                txtProcessCode.Attributes("ProcessID") = DT.Rows(0).Item("PROC_ID")
                txtProcessName.Text = DT.Rows(0).Item("PROC_Name")
                txtDesc.Text = DT.Rows(0).Item("PROC_Description")
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")

                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_Process Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  PROC_ID=" & Process_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindProcess()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select


    End Sub

    Protected Sub ResetProcess(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindProcess()
        '-----------------------------------
        ClearPanelEdit()

        pnlListProcess.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtProcessCode.Text = ""
        txtProcessCode.Attributes("ProcessID") = "0"
        txtProcessName.Text = ""
        txtDesc.Text = ""
        chkAvailable.Checked = True

        btnCreate.Visible = True

    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True

        btnCreate.Visible = False

        txtProcessCode.ReadOnly = False
        txtProcessCode.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListProcess.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtProcessCode.Text = "" Then
            lblValidation.Text = "Please insert Process Code"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtProcessName.Text = "" Then
            lblValidation.Text = "Please insert Process Name"
            pnlValidation.Visible = True
            Exit Sub
        End If


        Dim Process_ID As Integer = txtProcessCode.Attributes("ProcessID")

        Dim SQL As String = "SELECT * FROM MS_PROCESS WHERE PROC_Code='" & txtProcessCode.Text.Replace("'", "''") & "' AND PROC_ID<>" & Process_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Process Code is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_Process WHERE PROC_ID=" & Process_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            Process_ID = GetNewProcessID()
            DR("PROC_ID") = Process_ID
        Else
            DR = DT.Rows(0)
        End If

        DR("PROC_ID") = Process_ID
        DR("PROC_Code") = txtProcessCode.Text
        DR("PROC_Name") = txtProcessName.Text
        DR("PROC_Description") = txtDesc.Text
        DR("PROC_Order") = Process_ID
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID") ' Remain
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetProcess(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetNewProcessID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(PROC_ID),0)+1 FROM MS_Process "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function


End Class