﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Public Class UC_Dashboard_Improvement_Report_Plant
    Inherits System.Web.UI.UserControl

    Dim Dashboard As New DashboardClass
    Dim BL As New EIR_BL

    Public Property Eqm() As String
        Get
            If Not IsNothing(ViewState("Eqm")) Then
                Return ViewState("Eqm")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("Eqm") = value
        End Set
    End Property

    Public Property PlantID() As Integer
        Get
            If Not IsNothing(ViewState("PLANT_ID")) AndAlso IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
        End Set
    End Property

    Public Property YearF() As Integer
        Get
            If Not IsNothing(ViewState("YearF")) AndAlso IsNumeric(ViewState("YearF")) Then
                Return ViewState("YearF")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("YearF") = value
        End Set
    End Property

    Public Property YearT() As Integer
        Get
            If Not IsNothing(ViewState("YearT")) AndAlso IsNumeric(ViewState("YearT")) Then
                Return ViewState("YearT")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("YearT") = value
        End Set
    End Property

    Public Property MonthF() As Integer
        Get
            If Not IsNothing(ViewState("MonthF")) AndAlso IsNumeric(ViewState("MonthF")) Then
                Return ViewState("MonthF")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("MonthF") = value
        End Set
    End Property

    Public Property MonthT() As Integer
        Get
            If Not IsNothing(ViewState("MonthT")) AndAlso IsNumeric(ViewState("MonthT")) Then
                Return ViewState("MonthT")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("MonthT") = value
        End Set
    End Property

    Public Sub BindData(ByVal Plant_ID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As Integer)
        Dim SQL As String = ""
        Dim DT_Dashboard As New DataTable
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        Eqm = Equipment
        PlantID = Plant_ID
        MonthF = Month_F
        MonthT = Month_T
        YearF = Year_F
        YearT = Year_T

        If Year_F > 2500 Then
            Year_F = Year_F - 543
        End If
        If Year_T > 2500 Then
            Year_T = Year_T - 543
        End If

        DT_Dashboard.Columns.Add("Month")
        DT_Dashboard.Columns.Add("Year")
        DT_Dashboard.Columns.Add("Improvement")

        Dim culture As New CultureInfo("en-us", False)
        Dim StrDate As String = ""
        StrDate = Year_F & CStr(Month_F).PadLeft(2, "0") & "01"
        Dim Date_Start As Date = Date.ParseExact(StrDate, "yyyyMMdd", culture)

        StrDate = Year_T & CStr(Month_T).PadLeft(2, "0") & "01"
        Dim Date_End As Date = Date.ParseExact(StrDate, "yyyyMMdd", culture)

        Do While Date_Start <= Date_End
            Dim Dr As DataRow
            Dr = DT_Dashboard.NewRow
            Dr("Month") = Date_Start.Month
            Dr("Year") = Date_Start.Year
            Dr("Improvement") = 0
            DT_Dashboard.Rows.Add(Dr)
            Date_Start = DateAdd(DateInterval.Month, 1, Date_Start)
        Loop

        DT = Dashboard.ImprovementPlant(Plant_ID, Month_F, Month_T, Year_F, Year_T, Equipment)

        For i As Integer = 0 To DT_Dashboard.Rows.Count - 1
            DT.DefaultView.RowFilter = "MM = " & DT_Dashboard.Rows(i).Item("Month").ToString & " AND YY = " & DT_Dashboard.Rows(i).Item("Year").ToString
            If DT.DefaultView.Count > 0 Then
                Dim DT_Temp As New DataTable
                DT_Temp = DT.DefaultView.ToTable
                DT_Dashboard.Rows(i).Item("Improvement") = DT_Temp.Rows(0).Item("Improvement").ToString
            End If
            DT.DefaultView.RowFilter = ""

            Dim Year As Integer = DT_Dashboard.Rows(i).Item("Year")
            If Year < 2500 Then
                Year = Year + 543
            End If

            DT_Dashboard.Rows(i).Item("Year") = Year
        Next

        Session("Dashboard_Improvement_Report_Plant") = DT_Dashboard

        If DT_Dashboard.Rows.Count > 6 Then
            Dim NewWidth As Unit = Unit.Pixel(77 * DT_Dashboard.Rows.Count)
            ChartMain.Width = NewWidth
        End If


        DisplayChart(ChartMain, New EventArgs, Plant_ID, Month_F, Month_T, Year_F, Year_T, Equipment)

        rptData.DataSource = DT_Dashboard
        rptData.DataBind()

    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs, ByVal Plant_ID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As Integer)
        Dim DisplayText As String = ""

        Dim PlantName As String = ""
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL = "select PLANT_Name from MS_Plant where PLANT_ID = " & Plant_ID
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            PlantName = DT.Rows(0).Item("PLANT_Name").ToString
        End If

        If Year_F < 2500 Then
            Year_F = Year_F + 543
        End If
        If Year_T < 2500 Then
            Year_T = Year_T + 543
        End If

        If Year_F = Year_T Then
            If Month_F = Month_T Then
                DisplayText = "on  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F
            Else
                DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_F
            End If
        Else
            DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_T
        End If

        Dim F As New System.Drawing.Font("Microsoft Sans Serif", 12, Drawing.FontStyle.Bold)

        ChartMain.Titles("Title1").Font = F
        ChartMain.Titles("Title1").ForeColor = Drawing.Color.Navy
        ChartMain.Titles("Title1").Text = "Problem  improved  Completely  for  " & Dashboard.FindEquipmentName(Equipment) & vbNewLine & vbNewLine & DisplayText & "  on  " & PlantName
        ChartMain.Titles("Title1").Alignment = Drawing.ContentAlignment.TopCenter

        ChartMain.ChartAreas("ChartArea1").AxisY.TitleFont = F
        ChartMain.ChartAreas("ChartArea1").AxisY.TitleForeColor = Drawing.Color.Navy
        ChartMain.ChartAreas("ChartArea1").AxisY.Title = "Problem(s)"
        ChartMain.ChartAreas("ChartArea1").AxisX.MajorGrid.LineColor = Drawing.Color.Silver
        ChartMain.ChartAreas("ChartArea1").AxisY.MajorGrid.LineColor = Drawing.Color.Silver
        ChartMain.Series("Series1").Points.Clear()
        ChartMain.Series("Series1").ChartType = DataVisualization.Charting.SeriesChartType.Column

        DT = New DataTable
        DT = Session("Dashboard_Improvement_Report_Plant")
        For i As Integer = 0 To DT.Rows.Count - 1
            ChartMain.Series("Series1").Points.AddXY(DT.Rows(i).Item("Month").ToString & "/" & DT.Rows(i).Item("Year").ToString, DT.Rows(i).Item("Improvement"))

            Dim Tooltip As String = DT.Rows(i).Item("Improvement").ToString & " issues improved"
            ChartMain.Series("Series1").Points(i).ToolTip = Tooltip

            Dim Url As String = ""
            Select Case Eqm
                Case EIR_BL.Report_Type.Stationary_Routine_Report, EIR_BL.Report_Type.Rotating_Routine_Report
                    Url = "Dashboard_Improvement_Report_Tag.aspx?PLANT_ID=" & PlantID & "&MM=" & DT.Rows(i).Item("Month").ToString & "&YY=" & DT.Rows(i).Item("Year").ToString & "&Eqm=" & Eqm & "&Month_F=" & MonthF & "&Month_T=" & MonthT & "&Year_F=" & YearF & "&Year_T=" & YearT
                Case EIR_BL.Report_Type.Lube_Oil_Report
                    Url = "Dashboard_Improvement_Report_Tag_LO.aspx?PLANT_ID=" & PlantID & "&MM=" & DT.Rows(i).Item("Month").ToString & "&YY=" & DT.Rows(i).Item("Year").ToString & "&Eqm=" & Eqm & "&Month_F=" & MonthF & "&Month_T=" & MonthT & "&Year_F=" & YearF & "&Year_T=" & YearT & "&Improve=" & DT.Rows(i).Item("Improvement").ToString
                Case EIR_BL.Report_Type.PdMA_MTap_Report
                    Url = "Dashboard_Improvement_Report_Tag_PdMA.aspx?PLANT_ID=" & PlantID & "&MM=" & DT.Rows(i).Item("Month").ToString & "&YY=" & DT.Rows(i).Item("Year").ToString & "&Eqm=" & Eqm & "&Month_F=" & MonthF & "&Month_T=" & MonthT & "&Year_F=" & YearF & "&Year_T=" & YearT & "&Improve=" & DT.Rows(i).Item("Improvement").ToString
                Case EIR_BL.Report_Type.Thermography_Report
                    Url = "Dashboard_Improvement_Report_Tag_THM.aspx?PLANT_ID=" & PlantID & "&MM=" & DT.Rows(i).Item("Month").ToString & "&YY=" & DT.Rows(i).Item("Year").ToString & "&Eqm=" & Eqm & "&Month_F=" & MonthF & "&Month_T=" & MonthT & "&Year_F=" & YearF & "&Year_T=" & YearT
            End Select

            ChartMain.Series("Series1").Points(i).Url = Url
        Next

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblMonth As Label = e.Item.FindControl("lblMonth")
        Dim lblYear As Label = e.Item.FindControl("lblYear")
        Dim lblImprovement As Label = e.Item.FindControl("lblImprovement")

        Dim Improvement As Integer = 0

        lblMonth.Text = e.Item.DataItem("Month")
        lblYear.Text = e.Item.DataItem("Year")

        If Not IsDBNull(e.Item.DataItem("Improvement")) AndAlso e.Item.DataItem("Improvement") <> 0 Then
            Improvement = e.Item.DataItem("Improvement")
            lblImprovement.Text = FormatNumber(Improvement, 0)
        Else
            lblImprovement.Text = "-"
        End If

        Select Case Eqm
            Case EIR_BL.Report_Type.Stationary_Routine_Report, EIR_BL.Report_Type.Rotating_Routine_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Improvement_Report_Tag.aspx?PLANT_ID=" & PlantID & "&MM=" & e.Item.DataItem("Month") & "&YY=" & e.Item.DataItem("Year") & "&Eqm=" & Eqm & "&Month_F=" & MonthF & "&Month_T=" & MonthT & "&Year_F=" & YearF & "&Year_T=" & YearT & "';"
            Case EIR_BL.Report_Type.Lube_Oil_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Improvement_Report_Tag_LO.aspx?PLANT_ID=" & PlantID & "&MM=" & e.Item.DataItem("Month") & "&YY=" & e.Item.DataItem("Year") & "&Eqm=" & Eqm & "&Month_F=" & MonthF & "&Month_T=" & MonthT & "&Year_F=" & YearF & "&Year_T=" & YearT & "&Improve=" & Improvement & "';"
            Case EIR_BL.Report_Type.PdMA_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Improvement_Report_Tag_PdMA.aspx?PLANT_ID=" & PlantID & "&MM=" & e.Item.DataItem("Month") & "&YY=" & e.Item.DataItem("Year") & "&Eqm=" & Eqm & "&Month_F=" & MonthF & "&Month_T=" & MonthT & "&Year_F=" & YearF & "&Year_T=" & YearT & "&Improve=" & Improvement & "';"
            Case EIR_BL.Report_Type.Thermography_Report
                tbTag.Attributes("onclick") = "window.location.href='Dashboard_Improvement_Report_Tag_THM.aspx?PLANT_ID=" & PlantID & "&MM=" & e.Item.DataItem("Month") & "&YY=" & e.Item.DataItem("Year") & "&Eqm=" & Eqm & "&Month_F=" & MonthF & "&Month_T=" & MonthT & "&Year_F=" & YearF & "&Year_T=" & YearT & "';"
        End Select
    End Sub

End Class