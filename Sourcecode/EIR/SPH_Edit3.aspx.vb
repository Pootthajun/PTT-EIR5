﻿Imports System.Data
Imports System.Data.SqlClient
Public Class SPH_Edit3
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property

    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Public Property RPT_ID() As Integer
        Get
            If IsNumeric(ViewState("RPT_ID")) Then
                Return ViewState("RPT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

#Region "Static Property"
    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return ViewState("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUE_POPUP_ID") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_ID = Request.QueryString("RPT_ID")
            UNIQUE_POPUP_ID = Now.ToOADate.ToString.Replace(".", "")

            If RPT_ID = 0 Then
                Response.Redirect("SPH_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM SPH_RPT_Header WHERE RPT_ID=" & RPT_ID, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='SPH_Summary.aspx'", True)
                    Exit Sub
                End If
                '---------------- Get RPT_Year + RPT_No
                RPT_Year = DT.Rows(0)("RPT_Year")
                RPT_No = DT.Rows(0)("RPT_No")
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE SPH_RPT_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_ID=" & RPT_ID
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE SPH_RPT_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_ID=" & RPT_ID
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE SPH_RPT_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_ID=" & RPT_ID
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")
            BindTabData()

        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_SPH_REPORT_HEADER WHERE RPT_ID=" & RPT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='SPH_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reason\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='SPH_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='SPH_Summary.aspx'", True)
            Exit Sub
        End If
    End Sub

    Private Sub BindTabData()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT VW.* " & vbLf
        SQL &= " FROM VW_SPH_REPORT_HEADER VW " & vbLf
        SQL &= " WHERE RPT_ID=" & RPT_ID & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("SPH_Summary.aspx")
            Exit Sub
        End If

        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")

        SQL = "SELECT COUNT(1) FROM SPH_RPT_Detail WHERE RPT_ID=" & RPT_ID
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        lbl_Spring.Text = FormatNumber(DT.Rows(0)(0), 0)

        lbl_Year.Text = RPT_Year

        '---------------------------Bind Photo ------------------------------
        DT = New DataTable
        SQL = " SELECT SPH.SPH_ID,DETAIL_ID,SPH_No," & vbLf
        SQL &= " dbo.UDF_SPH_CalculateTravelPoint(Length_Cold,Length_Hot,INSP_Level) TavelPoint," & vbLf
        SQL &= " dbo.UDF_SPH_CalculateTravelLength(Length_Cold,Length_Hot,INSP_Level) TavelLength," & vbLf

        SQL &= " Corrosion,PossibleCause,Detail.Comment" & vbLf
        SQL &= " FROM SPH_RPT_Header Header" & vbLf
        SQL &= " INNER JOIN SPH_RPT_Detail Detail ON Header.RPT_ID=Detail.RPT_ID" & vbLf
        SQL &= " INNER JOIN SPH_MS_Spring SPH ON Detail.SPH_ID=SPH.SPH_ID" & vbLf

        SQL &= " WHERE Header.RPT_ID =" & RPT_ID & vbLf
        SQL &= " ORDER BY Pipe_No,SPH_No" & vbLf

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        rptINSP.DataSource = DT
        rptINSP.DataBind()

    End Sub

    Protected Sub rptINSP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptINSP.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Select Case e.CommandName
            Case "Upload"
                '--------------- Open Image Dialog ----------------
                Dim imgLeft As ImageButton = e.Item.FindControl("imgLeft")
                Dim imgRight As ImageButton = e.Item.FindControl("imgRight")
                Dim btnSaveImage1 As Button = e.Item.FindControl("btnSaveImage1")
                Dim btnSaveImage2 As Button = e.Item.FindControl("btnSaveImage2")
                Select Case e.CommandArgument
                    Case "1"
                        Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1") = BL.Get_SPH_Image(btnSaveImage1.CommandArgument, "1")
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "',1,document.getElementById('" & btnSaveImage1.ClientID & "'));", True)
                    Case "2"
                        Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2") = BL.Get_SPH_Image(btnSaveImage2.CommandArgument, "2")
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "',2,document.getElementById('" & btnSaveImage2.ClientID & "'));", True)
                End Select

            Case "SaveImage" '------------Occur when image has updated only --------------

                'Dim btnUpdate As Button = e.Item.FindControl("btnUpdate")
                Dim imgLeft As ImageButton = e.Item.FindControl("imgLeft")
                Dim imgRight As ImageButton = e.Item.FindControl("imgRight")
                Dim lblNo As Label = e.Item.FindControl("lblNo")
                Dim DETAIL_ID As Integer = lblNo.Attributes("DETAIL_ID")
                If IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_" & e.CommandArgument)) Then Exit Sub


                '------------------------ Save ------------------------
                Dim SQL As String = "SELECT DETAIL_ID,PIC_Detail" & e.CommandArgument & " FROM SPH_RPT_Detail WHERE DETAIL_ID=" & DETAIL_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    DT.Rows(0).Item("PIC_Detail" & e.CommandArgument) = Session("TempImage_" & UNIQUE_POPUP_ID & "_" & e.CommandArgument)
                    Dim cmb As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                End If
                If e.CommandArgument = 1 Then
                    imgLeft.ImageUrl = "Render_SPH_Picture.aspx?D=" & DETAIL_ID & "&S=1&t=" & Now.ToOADate.ToString.Replace(".", "")
                Else
                    imgRight.ImageUrl = "Render_SPH_Picture.aspx?D=" & DETAIL_ID & "&S=2&t=" & Now.ToOADate.ToString.Replace(".", "")
                End If
        End Select
    End Sub

    Protected Sub rptINSP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptINSP.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblCorrosion As Label = e.Item.FindControl("lblCorrosion")

        Dim imgLeft As ImageButton = e.Item.FindControl("imgLeft")
        Dim imgRight As ImageButton = e.Item.FindControl("imgRight")

        lblNo.Text = e.Item.ItemIndex + 1
        lblName.Text = e.Item.DataItem("SPH_No")

        Dim DisplayColor As Drawing.Color = Drawing.Color.Black
        If Not IsDBNull(e.Item.DataItem("TavelLength")) AndAlso e.Item.DataItem("TavelLength") <> 0 AndAlso Not IsDBNull(e.Item.DataItem("TavelPoint")) Then
            lblStatus.Text = "(" & e.Item.DataItem("TavelPoint") & " " & FormatNumber(e.Item.DataItem("TavelLength"), 1) & " mm.)"
            Select Case True
                Case InStr(e.Item.DataItem("TavelPoint"), "Cold") > 0
                    DisplayColor = Drawing.Color.Blue
                Case InStr(e.Item.DataItem("TavelPoint"), "Hot") > 0
                    DisplayColor = Drawing.Color.Red
            End Select
        End If
        lblNo.ForeColor = DisplayColor
        lblName.ForeColor = DisplayColor
        lblStatus.ForeColor = DisplayColor
        lblCorrosion.ForeColor = DisplayColor

        If Not IsDBNull(e.Item.DataItem("Corrosion")) AndAlso e.Item.DataItem("Corrosion") <> "" AndAlso e.Item.DataItem("Corrosion") <> "-" Then
            lblCorrosion.Text = " Corrosion : " & e.Item.DataItem("Corrosion")
        End If

        '------------------ Set Image ------------------
        Dim UNIQUE_POPUP_ID As String = Now.ToOADate.ToString.Replace("'", "''")
        imgLeft.ImageUrl = "Render_SPH_Picture.aspx?D=" & e.Item.DataItem("DETAIL_ID") & "&S=1&t=" & UNIQUE_POPUP_ID
        imgRight.ImageUrl = "Render_SPH_Picture.aspx?D=" & e.Item.DataItem("DETAIL_ID") & "&S=2&t=" & UNIQUE_POPUP_ID

        'Dim btnSaveImage1 As Button = e.Item.FindControl("btnSaveImage1") : btnSaveImage1.CommandArgument = e.Item.DataItem("DETAIL_ID")
        'Dim btnSaveImage2 As Button = e.Item.FindControl("btnSaveImage2") : btnSaveImage2.CommandArgument = e.Item.DataItem("DETAIL_ID")

        lblNo.Attributes("DETAIL_ID") = e.Item.DataItem("DETAIL_ID")
    End Sub

    Protected Sub btn_Buffer_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Buffer_Refresh.Click, lnkRefresh.Click
        BindTabData()
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click
        Try
            Dim Com As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            Com.CommandType = CommandType.Text
            Com.Connection = Conn
            Com.CommandText = "UPDATE SPH_RPT_Detail set PossibleCause='',Comment='',PIC_Detail1=Null,PIC_Detail2=Null WHERE RPT_ID=" & RPT_ID
            Com.ExecuteNonQuery()
            Com.Dispose()
            Conn.Close()
        Catch ex As Exception
            Exit Sub
        End Try
        BindTabData()
    End Sub


    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

#Region "Navigator"

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        'SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit1.aspx?RPT_ID=" & RPT_ID & "';", True)
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Back.Click, HTabDetail.Click
        'SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit2.aspx?RPT_ID=" & RPT_ID & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click, btn_Next.Click
        'SaveData(sender)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='SPH_Edit4.aspx?RPT_ID=" & RPT_ID & "';", True)
    End Sub
#End Region

#Region "Saving"
    'Private Sub SaveData(ByVal Sender As Object)

    'End Sub
#End Region

End Class