﻿Option Strict Off
Imports System.Data.SqlClient
Imports EIR

Public Class ST_TA_TAG_Spec
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim C As New Converter

    Private Property UNIQUE_POPUP_ID() As String
        Get
            Return ViewState("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Private Property TAG_ID As Integer
        Get
            Try
                Return ViewState("TAG_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            UNIQUE_POPUP_ID = Now.ToOADate.ToString.Replace(".", "")
            ResetTag(Nothing, Nothing)
            ClearPanelSearch()

        End If

        'StoreJS()
    End Sub



#Region "Validator"

    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub




#End Region



    Private Sub BindTag()

        Dim SQL As String = " SELECT * FROM (" & vbNewLine
        SQL &= "   Select AREA_CODE+'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TAG.ROUTE_ID,MS_ST_TAG.AREA_ID,MS_ST_TAG.Update_Time" & vbNewLine
        SQL &= "   , 'MS_ST_TAG' To_Table " & vbNewLine
        SQL &= "   FROM MS_ST_TAG" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_TAG_TYPE ON  MS_ST_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_Route ON MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Area ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Process ON MS_ST_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine

        SQL &= " ) TB " & vbNewLine
        SQL &= " WHERE TAG_TYPE_ID in (" & BL.TAG_TYPE_For_TA & ") " & vbNewLine

        Dim WHERE As String = ""

        If ddl_Search_Tag_Type.SelectedIndex > 0 Then
            WHERE &= " TB.TAG_TYPE_ID=" & ddl_Search_Tag_Type.Items(ddl_Search_Tag_Type.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " TB.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " TB.ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Area.SelectedIndex > 0 Then
            WHERE &= " TB.AREA_ID=" & ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value & " And "
        End If

        If WHERE <> "" Then
            SQL &= " AND " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= "ORDER BY AREA_CODE+'-'+PROC_CODE +'-' + TAG_NO" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("MS_ST_TA_TAG") = DT
        pnlSearch.Visible = True
        Navigation.SesssionSourceName = "MS_ST_TA_TAG"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblTagNo.Text = e.Item.DataItem("TAG_CODE")
        lblTagName.Text = e.Item.DataItem("TAG_Name")
        lblTagType.Text = e.Item.DataItem("TAG_TYPE_Name")
        lblTo_Table.Text = e.Item.DataItem("To_Table")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

    End Sub


    Private Sub rptTag_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")


        TAG_ID = btnEdit.Attributes("TAG_ID")

        Select Case e.CommandName
            Case "Edit"

                btnCreate.Visible = False
                pnlEdit.Visible = True

                lblUpdateMode.Text = "Update"

                '------------------------------------
                'pnlListTag.Enabled = False
                '------------ Bind Tag Data-----------
                Dim SQL As String = ""
                SQL = " "
                SQL &= " SELECT MS_ST_TAG.* ,MS_PLANT.PLANT_ID" & vbNewLine
                SQL &= " FROM MS_ST_TAG LEFT JOIN MS_ST_ROUTE ON MS_ST_TAG.ROUTE_ID=MS_ST_ROUTE.ROUTE_ID" & vbNewLine
                SQL &= " LEFT JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_ROUTE.PLANT_ID" & vbNewLine
                SQL &= " LEFT JOIN MS_ST_Tag_Type ON MS_ST_TAG.TAG_TYPE_ID=MS_ST_Tag_Type.TAG_TYPE_ID" & vbNewLine
                SQL &= " WHERE MS_ST_TAG.TAG_ID=" & TAG_ID

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "TAG Not Found"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If

                ClearPanelEdit()

                lblTagCode.Text = Tag_Info.TAG_CODE

                DT = New DataTable

                Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
                chkAvailable.Checked = btnToggle.ImageUrl = "resources/images/icons/tick.png"

                pnlEdit.Visible = True
                btnCreate.Visible = False
                pnlSearch.Visible = False
                lblUpdateMode.Text = "Update"

        End Select
    End Sub

    Protected Sub ResetTag(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindTag()
        '-----------------------------------
        ClearPanelEdit()

    End Sub

    Private Sub ClearPanelEdit()

        '------------ Tag Info -------------
        Tag_Info.BindData(0)
        BL.BindDDl_ST_Route(0, ddl_Search_Route)


        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        chkAvailable.Checked = True
        btnCreate.Visible = True

        '-------------Point Data------------
        ClearPanelEditPoint()
    End Sub




#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        BL.BindDDlST_TA_TagType(ddl_Search_Tag_Type)
        BL.BindDDlPlant(ddl_Search_Plant, False)
        BL.BindDDl_ST_Route(0, ddl_Search_Route)
        BL.BindDDlArea(0, ddl_Search_Area)

    End Sub

    Private Sub ddl_Search_Plant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        Dim PLANT_ID As Integer = ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value
        Dim AREA_ID As Integer = ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value
        Dim ROUTE_ID As Integer = ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value
        If PLANT_ID = 0 Then
            BL.BindDDlArea(ddl_Search_Area, AREA_ID)
        Else
            BL.BindDDlArea(PLANT_ID, ddl_Search_Area, AREA_ID)
        End If
        BL.BindDDl_ST_Route(PLANT_ID, ddl_Search_Route, ROUTE_ID)

        BindTag()
    End Sub


    Protected Sub Search_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Area.SelectedIndexChanged, ddl_Search_Tag_Type.SelectedIndexChanged, ddl_Search_Route.SelectedIndexChanged
        BindTag()
    End Sub


#End Region



    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False
        lblUpdateMode.Text = "Create"
        '-----------------------------------
        pnlSearch.Visible = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        '----------- Save TAG ------------
        Dim Message As String = Tag_Info.ValidateIncompleteMessage
        If Message <> "" Then
            '---------- Change To Tab Tag------------
            lblValidation.Text = Message
            pnlValidation.Visible = True
            Exit Sub
        End If

        'If ddl_Route.SelectedIndex < 1 Then
        '    '---------- Change To Tab Tag------------
        '    lblValidation.Text = "Select Routine Route"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If




        Try
            TAG_ID = Tag_Info.SaveData()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        Dim SQL As String = "SELECT * FROM MS_ST_TAG WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow = DT.Rows(0)
        'DR("ROUTE_ID") = ddl_Route.Items(ddl_Route.SelectedIndex).Value
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try



        '-------------- Save Spec --------------------------------
        SQL = "SELECT * FROM MS_ST_Spec WHERE TAG_ID=" & TAG_ID
        DT = New DataTable
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
            DR("TAG_ID") = TAG_ID
        Else
            DR = DT.Rows(0)
            End If

        If Not IsDBNull(Tag_Info.SIZE) Then
            DR("Size") = Tag_Info.SIZE
        Else
            DR("Size") = DBNull.Value
            End If

        If Not IsDBNull(Tag_Info.INSULATION_THICKNESS) Then
            DR("IN_Thickness") = Tag_Info.INSULATION_THICKNESS
        Else
            DR("IN_Thickness") = DBNull.Value
            End If
        If Not IsDBNull(Tag_Info.INITIAL_YEAR) Then
            DR("Initial_Year") = Tag_Info.INITIAL_YEAR
        Else
            DR("Initial_Year") = DBNull.Value
            End If
        If Not IsDBNull(Tag_Info.NORMINAL_THICKNESS) Then
            DR("Norminal_Thickness") = Tag_Info.NORMINAL_THICKNESS
        Else
            DR("Norminal_Thickness") = DBNull.Value
            End If
        If Not IsDBNull(Tag_Info.CA_ID) Then
            DR("CA_ID") = Tag_Info.CA_ID
        Else
            DR("CA_ID") = DBNull.Value
            End If
        If Not IsDBNull(Tag_Info.CALCULATED_THICKNESS) Then
            DR("Calculated_Thickness") = Tag_Info.CALCULATED_THICKNESS
        Else
            DR("Calculated_Thickness") = DBNull.Value
            End If
        If Not IsDBNull(Tag_Info.PRESSURE_DESIGN) Then
            DR("Pressure_Design") = Tag_Info.PRESSURE_DESIGN
        Else
            DR("Pressure_Design") = DBNull.Value
            End If
        If Not IsDBNull(Tag_Info.PRESSURE_OPERATING) Then
            DR("Pressure_Operating") = Tag_Info.PRESSURE_OPERATING
        Else
            DR("Pressure_Operating") = DBNull.Value
            End If
        If Not IsDBNull(Tag_Info.TEMPERATURE_DESIGN) Then
            DR("Temperature_Design") = Tag_Info.TEMPERATURE_DESIGN
        Else
            DR("Temperature_Design") = DBNull.Value
            End If
        If Not IsDBNull(Tag_Info.TEMPERATURE_OPERATING) Then
            DR("Temperature_Operating") = Tag_Info.TEMPERATURE_OPERATING
        Else
                DR("Temperature_Operating") = DBNull.Value
        End If
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

            '----------- Update Point ----------
            cmd = New SqlCommandBuilder(DA)
            DA.Update(DT)

        '------------ Save File ------------
        'Dim Attachments As List(Of FileAttachment) = Session(PT.Rows(i).Item("UNIQUE_ID"))
        'PIPE.Save_FILE(TAG_ID, POINT_ID, Attachments, True)



        ResetTag(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

        BindTag()

    End Sub


    Private Sub Tag_Info_PropertyChangedByUser(ByRef Sender As UC_ST_TA_TAG_Spec, Prop As UC_ST_TA_TAG_Spec.PropertyItem) Handles Tag_Info.PropertyChangedByUser
        Select Case Prop
            Case UC_ST_TA_TAG_Spec.PropertyItem.PLANT
                lblTagCode.Text = Tag_Info.TAG_CODE
                'BL.BindDDl_ST_Route(Tag_Info.PLANT_ID, ddl_Route, ddl_Route.Items(ddl_Route.SelectedIndex).Value)
            Case UC_ST_TA_TAG_Spec.PropertyItem.AREA
                lblTagCode.Text = Tag_Info.TAG_CODE
            Case UC_ST_TA_TAG_Spec.PropertyItem.PROCESS
                lblTagCode.Text = Tag_Info.TAG_CODE
            Case UC_ST_TA_TAG_Spec.PropertyItem.INITIAL_YEAR
            Case UC_ST_TA_TAG_Spec.PropertyItem.NORMINAL_THICKNESS
            Case UC_ST_TA_TAG_Spec.PropertyItem.CALCULATED_THICKNESS

            Case UC_ST_TA_TAG_Spec.PropertyItem.INSULATION_THICKNESS
                lblTagCode.Text = Tag_Info.TAG_CODE
            Case UC_ST_TA_TAG_Spec.PropertyItem.DESIGN_PRESSURE
            Case UC_ST_TA_TAG_Spec.PropertyItem.OPERATING_PRESSURE
            Case UC_ST_TA_TAG_Spec.PropertyItem.DESIGN_TEMPERATURE
            Case UC_ST_TA_TAG_Spec.PropertyItem.OPERATING_TEMPERATURE
            Case UC_ST_TA_TAG_Spec.PropertyItem.CORROSION_ALLOWANCE
                lblTagCode.Text = Tag_Info.TAG_CODE
        End Select
    End Sub

#Region "TabPoint"




    Private Sub ClearPanelEditPoint()


        '------------ Point Info -------------
        Tag_Info.UNIQUE_ID = GenerateNewUniqueID()
        Tag_Info.BindData(TAG_ID)
        Tag_Info.RaisePropertyChangedByUser = True  '------- Prevent Auto Postback ----------


        pnlSaveTag.Visible = True


    End Sub






#End Region


End Class