﻿Imports System.Data
Imports System.Data.SqlClient

Public Class ST_TA_Inspection_Plan
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim C As New Converter

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Turnaround_Inspection_Reports

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            HideValidator()
            ClearPanelSearch()
            BindPlan()
            If Request.QueryString("StatusPlan") <> Nothing Then
                If (Request.QueryString("StatusPlan") = "Success") Then 'Success
                    lblBindingSuccess.Text = "Save successfully"
                    pnlBindingSuccess.Visible = True
                Else                                                    'Fail  กรณีกดแก้ไข Plan แล้วไม่เจอรายการข้อมูล
                    lblBindingError.Text = "Plan Period Not Found"
                    pnlBindingError.Visible = True
                End If
            End If


        End If


    End Sub

    Private Sub HideValidator()
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub



    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        Response.Redirect("ST_TA_Inspection_Plan_Edit.aspx?Mode=Create")
    End Sub

    Private Sub BindPlan()
        Dim SQL As String = "SELECT dbo.UDF_RPT_Code(RPT_ST_TA_Header.RPT_Year,RPT_ST_TA_Header.RPT_No) RPT_Code,RPT_ST_TA_Header.RPT_Year,RPT_ST_TA_Header.RPT_No,PLANT_Code,TAG_TYPE_Name,RPT_Round,RPT_Period_Start,RPT_Period_End," & vbNewLine
        SQL &= " CASE RPT_ST_TA_Header.RPT_STEP WHEN 0 THEN CASE WHEN RPT_Period_Start<GETDATE() THEN  'New' ELSE 'Waiting' END ELSE STEP_NAME END STEP_NAME," & vbNewLine
        SQL &= " RPT_ST_TA_Header.RPT_STEP,(SELECT COUNT(*) FROM RPT_ST_TA_Detail WHERE RPT_Year=RPT_ST_TA_Header.RPT_Year And RPT_No=RPT_ST_TA_Header.RPT_No ) CountTag" & vbNewLine
        SQL &= " FROM RPT_ST_TA_Header " & vbNewLine
        SQL &= " LEFT JOIN MS_Plant ON RPT_ST_TA_Header.PLANT_ID=MS_Plant.PLANT_ID AND MS_Plant.Active_Status=1" & vbNewLine
        SQL &= " INNER JOIN MS_Report_Step ON RPT_ST_TA_Header.RPT_STEP=MS_Report_Step.RPT_STEP" & vbNewLine
        SQL &= " LEFT JOIN MS_ST_TAG_TYPE ON RPT_ST_TA_Header.TAG_TYPE_ID  = MS_ST_TAG_TYPE.TAG_TYPE_ID " & vbNewLine
        SQL &= "  LEFT JOIN RPT_ST_TA_TAG ON RPT_ST_TA_Header.RPT_Year =  RPT_ST_TA_TAG.RPT_Year  AND  RPT_ST_TA_Header.RPT_No =  RPT_ST_TA_TAG.RPT_No  " & vbNewLine

        SQL &= " WHERE RPT_ST_TA_Header.RPT_Type_ID=" & RPT_Type_ID & " And " & vbNewLine

        If ddl_Search_Year.SelectedIndex > 0 Then
            SQL &= " RPT_ST_TA_Header.RPT_Year=" & ddl_Search_Year.Items(ddl_Search_Year.SelectedIndex).Value & " And " & vbNewLine
        End If
        If ddl_Search_Plant.SelectedIndex > 0 Then
            SQL &= " MS_PLANT.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " And " & vbNewLine
        End If

        SQL = SQL.Substring(0, SQL.Length - 6) & vbNewLine



        SQL &= " Group BY " & vbNewLine
        SQL &= " RPT_ST_TA_Header.RPT_Year, RPT_ST_TA_Header.RPT_No, RPT_ST_TA_Header.RPT_Year" & vbNewLine
        SQL &= " ,RPT_ST_TA_Header.RPT_No,PLANT_Code,RPT_Round,RPT_Period_Start,RPT_Period_End," & vbNewLine
        SQL &= " RPT_ST_TA_Header.RPT_STEP, STEP_NAME,TAG_TYPE_Name" & vbNewLine

        SQL &= " ORDER BY RPT_Period_Start,RPT_Period_End,PLANT_Code,TAG_TYPE_Name"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try
        pnlBindingError.Visible = False

        Session("MN_Stationary_Plan") = DT

        Navigation.SesssionSourceName = "MN_Stationary_Plan"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlan
    End Sub

    Protected Sub rptPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblYear As Label = e.Item.FindControl("lblYear")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblType As Label = e.Item.FindControl("lblType")

        'Dim txtRound As TextBox = e.Item.FindControl("txtRound")
        Dim lblStart As Label = e.Item.FindControl("lblStart")
        Dim lblEnd As Label = e.Item.FindControl("lblEnd")
        Dim lblEstimate As Label = e.Item.FindControl("lblEstimate")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblCountTag As Label = e.Item.FindControl("lblCountTag")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblCode.Text = e.Item.DataItem("RPT_Code")
        lblYear.Text = e.Item.DataItem("RPT_Year")
        lblPlant.Text = e.Item.DataItem("PLANT_Code")
        lblType.Text = e.Item.DataItem("TAG_TYPE_Name")

        lblStart.Text = BL.ReportProgrammingDate(e.Item.DataItem("RPT_Period_Start"))
        lblEnd.Text = BL.ReportProgrammingDate(e.Item.DataItem("RPT_Period_End"))

        lblEstimate.Text = FormatNumber(DateDiff(DateInterval.Day, e.Item.DataItem("RPT_Period_Start"), e.Item.DataItem("RPT_Period_End")) + 1, 0)
        lblStatus.Text = e.Item.DataItem("STEP_Name")
        lblStatus.ForeColor = BL.Get_Report_Step_Color(e.Item.DataItem("RPT_STEP"))
        lblCountTag.Text = e.Item.DataItem("CountTag")

        btnEdit.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnEdit.Attributes("RPT_No") = e.Item.DataItem("RPT_No")

        btnEdit.Visible = e.Item.DataItem("RPT_STEP") < EIR_BL.Report_Step.Inspecting_Step
        btnToggle.Visible = e.Item.DataItem("RPT_STEP") < EIR_BL.Report_Step.Inspecting_Step

        '--------------- Round Editor -----------------
        'Dim lblRound As Label = e.Item.FindControl("lblRound")
        Dim btnUpdateRound As Button = e.Item.FindControl("btnUpdateRound")
        'If Not IsDBNull(e.Item.DataItem("RPT_Round")) Then
        '    txtRound.Text = e.Item.DataItem("RPT_Round")
        'End If
        'ImplementJavaIntegerText(txtRound, False)
        'txtRound.Style.Item("text-align") = "center"
        'txtRound.Attributes("onchange") = "document.getElementById('" & btnUpdateRound.ClientID & "').click();"
    End Sub

    Protected Sub rptPlan_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlan.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim RPT_Year As Integer = btnEdit.Attributes("RPT_Year")
        Dim RPT_No As Integer = btnEdit.Attributes("RPT_No")

        Select Case e.CommandName
            Case "Edit"

                Response.Redirect("ST_TA_Inspection_Plan_Edit.aspx?Mode=Edit&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "")

            'Case "Round"

            '    Dim txtRound As TextBox = e.Item.FindControl("txtRound")
            '    Dim SQL As String = "Update RPT_ST_TA_Header set RPT_Round="
            '    If txtRound.Text <> "" Then
            '        SQL &= txtRound.Text
            '    Else
            '        SQL = "NULL"
            '        txtRound.Text = ""
            '    End If
            '    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            '    Dim Conn As New SqlConnection(BL.ConnStr)
            '    Conn.Open()
            '    Dim Comm As New SqlCommand
            '    With Comm
            '        .CommandType = CommandType.Text
            '        .CommandText = SQL
            '        .Connection = Conn
            '        .ExecuteNonQuery()
            '        .Dispose()
            '    End With
            '    Conn.Close()
            '    Conn.Dispose()

            Case "ToggleStatus"
                'Try
                '    BL.Drop_RPT_ST_TA_Header(RPT_Year, RPT_No)
                'Catch ex As Exception
                '    lblBindingError.Text = "Invalid parameter"
                '    pnlBindingError.Visible = True
                '    Exit Sub
                'End Try
                'BindPlan()

                'lblBindingSuccess.Text = "Delete plan successfully"
                'pnlBindingSuccess.Visible = True
        End Select

    End Sub



#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()

        Dim SQL As String = "SELECT ISNULL(MIN(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM RPT_ST_TA_Header"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddl_Search_Year.Items.Clear()
        ddl_Search_Year.Items.Add("Choose a Year...")
        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddl_Search_Year.Items.Add(i)
        Next
        ddl_Search_Year.Text = Now.Year + 543

        BL.BindDDlPlant(ddl_Search_Plant)
    End Sub

    Protected Sub ddl_Search_Year_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Year.SelectedIndexChanged
        BindPlan()
    End Sub

    Private Sub ddl_Search_Plant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BindPlan()
    End Sub


#End Region


End Class