﻿Imports System.Data
Imports System.Data.SqlClient
Public Class THM_Routine_Summary
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim CV As New Converter

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Thermography_Report

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            ClearPanelSearch()
            SetUserAuthorization()
        End If

    End Sub


#Region "Hide Validator"
    Private Sub HideValidator()
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub
#End Region

    Private Sub SetUserAuthorization()
        'btnConstruct.Visible = USER_LEVEL = EIR_BL.User_Level.Inspector Or USER_LEVEL Or USER_LEVEL = EIR_BL.User_Level.Administrator ' Only Inspector Can Create Plan 
    End Sub

    Private Sub ClearPanelSearch()
        BL.BindDDlReportStep(ddl_Search_Status)
        Dim SQL As String = "SELECT ISNULL(MIN(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM RPT_THM_Header"

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddl_Search_Year.Items.Clear()
        ddl_Search_Year.Items.Add("Choose a Year...")
        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddl_Search_Year.Items.Add(i)
        Next
        ddl_Search_Year.Text = Now.Year + 543

        BL.BindDDl_THM_Type(ddl_Search_Type)
        BL.BindDDlPlant(ddl_Search_Plant)
        BL.BindDDl_THM_Route(ddl_Search_Type.SelectedValue, ddl_Search_Plant.SelectedValue, ddl_Search_Route)

        If USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Viewer Then
            If Not IsNothing(Request.QueryString("Editable")) AndAlso Request.QueryString("Editable") = "True" Then
                chk_Search_Edit.Checked = True
            End If
        Else
            chk_Search_Edit.Visible = False
            lblEditable.Visible = False
        End If

        BindPlan()
    End Sub

    Private Sub BindPlan()
        Dim SQL As String = ""

        SQL &= "  SELECT * FROM VW_REPORT_THM_HEADER " & vbLf
        SQL &= "  " & vbLf

        Dim WHERE As String = ""
        If ddl_Search_Year.SelectedIndex > 0 Then
            WHERE &= " RPT_Year=" & ddl_Search_Year.Items(ddl_Search_Year.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Type.SelectedIndex > 0 Then
            WHERE &= " THM_TYPE_ID=" & ddl_Search_Type.Items(ddl_Search_Type.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If txt_Search_Start.Text <> "" Then
            WHERE &= " RPT_Period_Start>='" & txt_Search_Start.Text.Replace("'", "''") & "' AND " & vbNewLine
        End If
        If txt_Search_End.Text <> "" Then
            WHERE &= " RPT_Period_Start<='" & txt_Search_End.Text.Replace("'", "''") & "' AND " & vbNewLine
        End If
        If ddl_Search_Status.SelectedIndex > 0 Then
            WHERE &= " RPT_STEP=" & ddl_Search_Status.Items(ddl_Search_Status.SelectedIndex).Value & " AND " & vbNewLine
        End If

        If WHERE <> "" Then
            SQL &= "WHERE " & WHERE.Substring(0, WHERE.Length - 6) & vbNewLine
        End If

        SQL &= " ORDER BY RPT_Year,THM_TYPE_NAME,PLANT_ID,ROUTE_ID" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try
        pnlBindingError.Visible = False

        Session("THM_Routine_Summary") = DT
        Navigation.SesssionSourceName = "THM_Routine_Summary"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlan
    End Sub

    Protected Sub rptPlan_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlan.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim RPT_Year As Integer = btnEdit.Attributes("RPT_Year")
        Dim RPT_No As Integer = btnEdit.Attributes("RPT_No")
        Dim Plant As String = ""
        Dim Route As String = ""
        Dim RouteID As String = ""

        '------- Check First --------
        Dim SQL As String = "SELECT * FROM VW_REPORT_THM_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            lblBindingError.Text = "This report has been removed."
            pnlBindingError.Visible = True
            Exit Sub
        Else
            Plant = DT.Rows(0).Item("PLANT_Code").ToString
            Route = DT.Rows(0).Item("ROUTE_Code").ToString
            RouteID = DT.Rows(0).Item("ROUTE_ID").ToString
        End If

        '---------------------- Set Available -----------------------
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim RPT_STEP As EIR_BL.Report_Step = lblStatus.Attributes("RPT_STEP")
        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
        Dim RPT_LOCK_BY As Integer = lblStatus.Attributes("RPT_LOCK_BY")

        With BL.ReportPermissionManager
            .RPT_STEP = RPT_STEP
            .USER_LEVEL = USER_LEVEL
            .USER_ID = Session("USER_ID")
            .RPT_LOCK_BY = RPT_LOCK_BY

            If Not .CanEdit Then
                lblBindingError.Text = "This report cannot be edited due to follow these reason<br>" & vbNewLine
                lblBindingError.Text &= "<li>Permission exception due to conflict report step<br>" & vbNewLine
                lblBindingError.Text &= "<li>This report has been locked by others"
                pnlBindingError.Visible = True
                Exit Sub
            End If
        End With

        ''-------------Update Activated Report-----------

        '---------------------------- Add Report Header Info----------------------
        SQL = "SELECT * FROM RPT_THM_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            lblBindingError.Text = "This report cannot be edited due to follow these reason<br>" & vbNewLine
            lblBindingError.Text &= "<li>Report is not found<br>" & vbNewLine
            pnlBindingError.Visible = True
            Exit Sub
        End If

        Dim OldReportNo As Integer = RPT_No
        If RPT_No <= 0 Then
            RPT_No = BL.GetNewReportNumber(RPT_Year)
            DT.Rows(0).Item("RPT_No") = RPT_No
        End If

        '--------------------- Set Starter Value --------------
        Dim Period As Date = DT.Rows(0).Item("RPT_Period_Start")
        Dim THM_TYPE_NAME As String = "อุปกรณ์"
        If IsDBNull(DT.Rows(0).Item("RPT_Subject")) Then
            Select Case CType(DT.Rows(0).Item("THM_TYPE_ID"), EIR_BL.THM_Type)
                Case EIR_BL.THM_Type.INSTRUMENT
                    THM_TYPE_NAME = "INSTRUMENT"
                Case EIR_BL.THM_Type.ELECTRICAL
                    THM_TYPE_NAME = "MCC"
                Case EIR_BL.THM_Type.PROCESS
                    THM_TYPE_NAME = "PROCESS"
                Case EIR_BL.THM_Type.POWER_TURBINE_EXHAUST
                    THM_TYPE_NAME = "POWER TURBINE EXHAUST"
            End Select
            DT.Rows(0).Item("RPT_Subject") = "รายงานการตรวจสอบ " & THM_TYPE_NAME & " " & Plant & " " & Route
        End If
        If IsDBNull(DT.Rows(0).Item("RPT_To")) Then
            DT.Rows(0).Item("RPT_To") = "ผจ.ตร / คุณชนินทร์"
        End If
        If IsDBNull(DT.Rows(0).Item("RPT_Cause")) Then
            DT.Rows(0).Item("RPT_Cause") = "ทีมงาน ตร. ตรวจสอบ " & THM_TYPE_NAME & " ด้วย Thermography เพื่อประเมินสภาพอุปกรณ์ประจาปี " & DT.Rows(0).Item("RPT_Year")
        End If
        If IsDBNull(DT.Rows(0).Item("RPT_Result")) Then
            DT.Rows(0).Item("RPT_Result") = ""
        End If
        If IsDBNull(DT.Rows(0).Item("RPT_STEP")) OrElse DT.Rows(0).Item("RPT_STEP") <= 0 Then
            DT.Rows(0).Item("RPT_STEP") = 1
        End If
        If IsDBNull(DT.Rows(0).Item("Created_By")) Then
            DT.Rows(0).Item("Created_By") = Session("USER_ID")
        End If
        If IsDBNull(DT.Rows(0).Item("Created_Time")) Then
            DT.Rows(0).Item("Created_Time") = Now
        End If

        If Session("USER_ID") <> 0 Then
            If IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                DT.Rows(0).Item("RPT_LOCK_BY") = Session("USER_ID")
            End If
        End If

        Dim CMD As New SqlCommandBuilder(DA)
        DA.Update(DT)

        If RPT_STEP <> 4 Then
            '---------------------------- Add Report Detail Info----------------------
            BL.Construct_THM_Report_Detail(RPT_Year, RPT_No, RouteID)
        End If

        UpdateLastStatus(RPT_Year, RPT_No)

        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='THM_Routine_Edit1.aspx?" & Param & "';", True)
    End Sub

    Protected Sub rptPlan_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlan.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRptNo As Label = e.Item.FindControl("lblRptNo")
        Dim lblTotal As Label = e.Item.FindControl("lblTotal")
        Dim lblNA As Label = e.Item.FindControl("lblNA")
        Dim lblNormal As Label = e.Item.FindControl("lblNormal")
        Dim lblAbnormal As Label = e.Item.FindControl("lblAbnormal")
        Dim lblPlanStart As Label = e.Item.FindControl("lblPlanStart")
        Dim lblActualStart As Label = e.Item.FindControl("lblActualStart")

        Dim imgLock As Image = e.Item.FindControl("imgLock")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnCreate As ImageButton = e.Item.FindControl("btnCreate")
        Dim btnReport As HtmlAnchor = e.Item.FindControl("btnReport")
        Dim cfbCreate As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfbCreate")

        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        Dim lblRound As Label = e.Item.FindControl("lblRound")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        lblRptNo.Text = e.Item.DataItem("RPT_Code")
        lblType.Text = e.Item.DataItem("THM_TYPE_Name")
        If Not IsDBNull(e.Item.DataItem("PLANT_Code")) Then
            lblPlant.Text = e.Item.DataItem("PLANT_Code")
        End If
        If Not IsDBNull(e.Item.DataItem("ROUTE_Code")) Then
            lblRoute.Text = e.Item.DataItem("ROUTE_Code")
        End If
        If Not IsDBNull(e.Item.DataItem("RPT_Round")) Then
            lblRound.Text = e.Item.DataItem("RPT_Round")
        End If

        lblStatus.Text = e.Item.DataItem("STEP_Name")
        lblStatus.ForeColor = BL.Get_Report_Step_Color(e.Item.DataItem("RPT_STEP"))

        lblRptNo.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        lblRptNo.Attributes("RPT_No") = e.Item.DataItem("RPT_No")

        If e.Item.DataItem("RPT_No") > 0 Then
            lblNormal.Text = FormatNumber(e.Item.DataItem("Normal"), 0)
            lblAbnormal.Text = FormatNumber(e.Item.DataItem("Abnormal"), 0)
            lblNA.Text = FormatNumber(e.Item.DataItem("TOTAL_TAG") - (e.Item.DataItem("Abnormal") + e.Item.DataItem("Normal")), 0)
        Else
            lblNormal.Text = "-"
            lblAbnormal.Text = "-"
            lblNA.Text = "-"
        End If

        lblTotal.Text = FormatNumber(e.Item.DataItem("TOTAL_TAG"), 0)

        lblStatus.Attributes("RPT_STEP") = e.Item.DataItem("RPT_STEP")
        If Not IsDBNull(e.Item.DataItem("RPT_LOCK_BY")) Then
            lblStatus.Attributes("RPT_LOCK_BY") = e.Item.DataItem("RPT_LOCK_BY")
        Else
            lblStatus.Attributes("RPT_LOCK_BY") = -1
        End If

        lblStatus.ForeColor = BL.Get_Report_Step_Color(e.Item.DataItem("RPT_STEP"))
        lblPlanStart.Text = BL.ReportGridTime(e.Item.DataItem("RPT_Period_Start"))
        If Not IsDBNull(e.Item.DataItem("Created_Time")) Then
            lblActualStart.Text = BL.ReportGridTime(e.Item.DataItem("Created_Time"))
            If Int(CDate(e.Item.DataItem("Created_Time")).ToOADate) > Int(CDate(e.Item.DataItem("RPT_Period_Start")).ToOADate) Then
                lblActualStart.CssClass = BL.Get_Inspection_Css_Text_By_Level(EIR_BL.InspectionLevel.ClassB)
                lblActualStart.ToolTip = "Late"
            Else
                lblActualStart.CssClass = BL.Get_Inspection_Css_Text_By_Level(EIR_BL.InspectionLevel.Normal)
                lblActualStart.ToolTip = "On-Time"
            End If
        End If

        '------------------------- Set for Action Premission -------------------------
        btnEdit.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnEdit.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
        btnCreate.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnCreate.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
        btnReport.Attributes("onClick") = "ShowPreviewReport(" & e.Item.DataItem("RPT_Year") & "," & e.Item.DataItem("RPT_No") & ");"

        Dim RPT_STEP As EIR_BL.Report_Step = e.Item.DataItem("RPT_STEP")
        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
        Dim RPT_LOCK_BY As Integer = lblStatus.Attributes("RPT_LOCK_BY")

        With BL.ReportPermissionManager
            .RPT_STEP = RPT_STEP
            .USER_LEVEL = USER_LEVEL
            .USER_ID = Session("USER_ID")
            .RPT_LOCK_BY = RPT_LOCK_BY

            If .CanEdit And e.Item.DataItem("RPT_No") < 0 And RPT_STEP = EIR_BL.Report_Step.New_Step Then
                btnCreate.Visible = True
                cfbCreate.TargetControlID = btnCreate.ID
                cfbCreate.ConfirmText = "Do you want to start create this report!?"
                btnEdit.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = False
            ElseIf .CanEdit And RPT_LOCK_BY = -1 Then
                btnEdit.Visible = True
                btnEdit.ImageUrl = btnCreate.ImageUrl
                btnCreate.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = False
            ElseIf .CanEdit And RPT_LOCK_BY = Session("USER_ID") Then
                btnEdit.Visible = True
                btnCreate.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = False
            ElseIf .CanEdit And Session("USER_ID") = 0 Then
                btnEdit.ImageUrl = btnCreate.ImageUrl
                btnEdit.Visible = True
                btnCreate.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = RPT_LOCK_BY <> -1
                '---------------- Add Locked By Detail- ------------------
                'If Not IsDBNull(e.Item.DataItem("Lock_By_Name")) Then
                '    imgLock.ToolTip = e.Item.DataItem("Lock_By_Name")
                'End If
            ElseIf RPT_LOCK_BY > 0 And RPT_LOCK_BY <> Session("USER_ID") Then
                btnEdit.Visible = False
                btnCreate.Visible = False
                imgLock.Visible = True
                'If Not IsDBNull(e.Item.DataItem("Lock_By_Name")) Then
                '    imgLock.ToolTip = e.Item.DataItem("Lock_By_Name")
                'End If
            Else
                btnCreate.Visible = False
                btnEdit.Visible = False
                '------------ Set Lock Status ------------
                imgLock.Visible = True
                '---------------- Add Locked Detail-------------------
                imgLock.ToolTip = "Lock by workflow"
            End If
            btnReport.Visible = .CanPreview
        End With

    End Sub
        Protected Sub Search_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Year.SelectedIndexChanged, ddl_Search_Type.SelectedIndexChanged, ddl_Search_Plant.SelectedIndexChanged, ddl_Search_Status.SelectedIndexChanged, chk_Search_Edit.CheckedChanged, ddl_Search_Route.SelectedIndexChanged, txt_Search_Start.TextChanged, txt_Search_End.TextChanged
        BindPlan()
    End Sub

    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Type.SelectedIndexChanged, ddl_Search_Plant.SelectedIndexChanged
        BL.BindDDl_THM_Route(ddl_Search_Type.Items(ddl_Search_Type.SelectedIndex).Value, ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Route)
        BindPlan()
    End Sub


    Sub UpdateLastStatus(ByVal RPT_YEAR As Integer, ByVal RPT_NO As Integer)
        Dim SQL As String = ""
        SQL &= "DECLARE @RPT_YEAR AS INT = " & RPT_YEAR & ";" & vbNewLine
        SQL &= "DECLARE @RPT_NO AS INT = " & RPT_NO & ";" & vbNewLine
        SQL &= "SELECT TAG.TAG_ID,TAG_CODE,TAG_NAME,Last_TAG_STATUS" & vbNewLine
        SQL &= "FROM " & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,TAG_CODE,TAG_NAME" & vbNewLine
        SQL &= "	FROM RPT_THM_Detail " & vbNewLine
        SQL &= "	WHERE RPT_Year = @RPT_YEAR AND RPT_NO = @RPT_NO" & vbNewLine
        SQL &= ") TAG" & vbNewLine
        SQL &= "LEFT JOIN" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "	SELECT TAG_ID,TAG_STATUS AS Last_TAG_STATUS FROM " & vbNewLine
        SQL &= "	(" & vbNewLine
        SQL &= "		SELECT TAG_ID,RPT_Year,TAG_STATUS," & vbNewLine
        SQL &= "		ROW_NUMBER() OVER (PARTITION BY TAG_ID ORDER BY RPT_Year DESC,RPT_NO DESC) ROW_NUM" & vbNewLine
        SQL &= "		FROM RPT_THM_Detail " & vbNewLine
        SQL &= "		WHERE RPT_Year <= @RPT_YEAR AND RPT_NO < " & RPT_NO & " AND TAG_STATUS IS NOT NULL" & vbNewLine
        SQL &= "	) FILTER WHERE ROW_NUM = 1" & vbNewLine
        SQL &= ") ST" & vbNewLine
        SQL &= "ON TAG.TAG_ID = ST.TAG_ID" & vbNewLine



        Dim DT_Tag As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT_Tag)

        SQL = "SELECT * FROM RPT_THM_Detail WHERE RPT_Year=" & RPT_YEAR & " AND RPT_No=" & RPT_NO
        Dim DT As New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        For i As Integer = 0 To DT.Rows.Count - 1
            DT_Tag.DefaultView.RowFilter = "TAG_ID=" & DT.Rows(i).Item("TAG_ID")
            Dim DR As DataRow = DT.Rows(i)
            If DT_Tag.DefaultView.Count > 0 Then
                DR("Last_TAG_STATUS") = DT_Tag.Rows(i).Item("Last_TAG_STATUS")
            Else
                DR("Last_TAG_STATUS") = DBNull.Value
            End If
            Try
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT)
                DT.AcceptChanges()
            Catch : End Try
        Next
    End Sub

End Class