﻿Public Class GL_DialogInputValue
    Inherits System.Web.UI.UserControl

    Public Event CancelDialog()
    Public Event AnswerDialog(ByVal Result As String)

    Public ReadOnly Property Caption() As String
        Get
            Return lblCaption.Text
        End Get
    End Property

    Public Sub ShowDialog(ByVal Caption As String, Optional ByVal DefaultValue As String = "") ' For Case Edit
        Enable()
        lblCaption.Text = Caption
        txtInput.Text = DefaultValue
        txtInput.Focus()
        Me.Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Disable()
        RaiseEvent CancelDialog()
        Me.Visible = False
    End Sub

    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Disable()
        RaiseEvent AnswerDialog(txtInput.Text)
        Me.Visible = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Enable()
        End If
    End Sub

    Public Sub Enable()
        txtInput.Enabled = True
        btnOk.Enabled = True
        btnCancel.Enabled = True
    End Sub

    Public Sub Disable()
        txtInput.Enabled = False
        btnOk.Enabled = False
        btnCancel.Enabled = False
    End Sub

End Class