﻿Imports System
Imports System.Web
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Drawing
Imports System.Collections.Generic

Public Class EIR_PIPE
    Dim BL As New EIR_BL

    Public Const TAG_TYPE_PIPE As Integer = 8

    Public Enum Report_Step
        NA = 0
        Before_Remove = 1
        After_Remove = 2
        Thickness_Measurement = 3
        After_Repair = 4
        Coating_Inspection = 5
        Insulating_Inspection = 6
        Final = 7
    End Enum

    Public Function Get_REPORT_Step_Color(ByVal ReportStep As Report_Step) As Color
        Select Case ReportStep
            Case Report_Step.NA
                Return Color.Gray
            Case Report_Step.Before_Remove
                Return Color.SteelBlue
            Case Report_Step.After_Remove
                Return Color.Violet
            Case Report_Step.Thickness_Measurement
                Return Color.Red
            Case Report_Step.After_Repair
                Return Color.Orange
            Case Report_Step.Coating_Inspection
                Return Color.Brown
            Case Report_Step.Insulating_Inspection
                Return Color.MediumSeaGreen
            Case Report_Step.Final
                Return Color.Green
        End Select
    End Function

    Public Sub BindDDL_ReportOfficer(ByRef cmb As DropDownList,
                                   ByVal RPT_Year As Integer,
                                   ByVal RPT_No As Integer,
                                   ByVal RPT_Step As Report_Step,
                                   ByVal Role As EIR_BL.User_Level,
                                   Optional ByVal User_Full_Name As String = "")


        Dim HeaderTable As String = ""
        Select Case RPT_Step
            Case Report_Step.Before_Remove
                HeaderTable = "RPT_PIPE_Before_Remove"
            Case Report_Step.After_Remove
                HeaderTable = "RPT_PIPE_After_Remove"
            Case Report_Step.Thickness_Measurement
                HeaderTable = "RPT_PIPE_TM_Header"
            Case Report_Step.After_Repair
                HeaderTable = "RPT_PIPE_After_Repair"
            Case Report_Step.Coating_Inspection
                HeaderTable = "RPT_PIPE_Coating"
            Case Report_Step.Insulating_Inspection
                HeaderTable = "RPT_PIPE_Insulating"
            Case Report_Step.Final
                HeaderTable = "RPT_PIPE_Final_Report"
        End Select
        '--------------------------------- PIPE
        Dim Sql As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbLf
        Sql &= " DECLARE @RPT_No AS INT =" & RPT_No & vbLf
        Dim RoleName As String = ""
        Select Case Role
            Case EIR_BL.User_Level.Collector
                Sql &= " SELECT 0 POS_ID, ISNULL(RPT_Outsource,'') User_Full_Name FROM " & HeaderTable & vbLf
                RoleName = "Creater"
            Case EIR_BL.User_Level.Inspector
                Sql &= " SELECT 0 POS_ID, ISNULL(RPT_Engineer,'') User_Full_Name FROM " & HeaderTable & vbLf
                RoleName = "Evaluator"
            Case EIR_BL.User_Level.Approver
                Sql &= " SELECT 0 POS_ID, ISNULL(RPT_Approver,'') User_Full_Name FROM " & HeaderTable & vbLf
                RoleName = "Approver"
        End Select
        Sql &= " WHERE RPT_Year=@RPT_Year AND RPT_No=@RPT_No" & vbLf
        Dim DTPipe As New DataTable
        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        DA.Fill(DTPipe)

        '--------------------------------- USER
        Sql = "SELECT P.POS_ID,ISNULL(U.User_Prefix,'') + ISNULL(U.USER_Name,'') + ' ' + ISNULL(U.User_Surname,'') User_Full_Name" & vbLf
        Sql &= " FROM MS_USER U " & vbLf
        Sql &= " INNER JOIN MS_User_Coverage C ON U.USER_ID=C.USER_ID" & vbLf
        Sql &= " INNER JOIN MS_User_Position P ON U.POS_ID=P.POS_ID" & vbLf
        Sql &= " INNER JOIN MS_User_Skill S ON U.USER_ID=S.USER_ID" & vbLf
        Sql &= " WHERE S.S_Stationary=1 " & vbLf
        Select Case Role
            Case EIR_BL.User_Level.Collector
                Sql &= " AND U.POS_ID=0"
                RoleName = "Creater"
            Case EIR_BL.User_Level.Inspector
                Sql &= " AND U.POS_ID IN (1,2,3)" & vbLf
            Case EIR_BL.User_Level.Approver
                Sql &= " AND U.POS_ID IN (2,3)" & vbLf
        End Select
        Dim DTUser As New DataTable
        DA = New SqlDataAdapter(Sql, BL.ConnStr)
        DA.Fill(DTUser)

        DTPipe.Merge(DTUser)

        Dim Col() As String = {"User_Full_Name"}
        Dim DT As DataTable = DTPipe.DefaultView.ToTable(True, "User_Full_Name")
        DTPipe.Dispose()
        DA.Dispose()

        cmb.Items.Clear()
        cmb.Items.Add(New ListItem("...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).Item("User_Full_Name").ToString.Trim = "" Then Continue For
            Dim Item As New ListItem(DT.Rows(i).Item("User_Full_Name"), DT.Rows(i).Item("User_Full_Name"))
            cmb.Items.Add(Item)
            If User_Full_Name = DT.Rows(i).Item("User_Full_Name") Then
                cmb.SelectedIndex = cmb.Items.Count - 1
            End If
        Next
    End Sub

    Public Sub BindDDl_Tag(ByRef ddl As DropDownList, Optional ByVal PLANT_ID As Integer = -1, Optional ByVal AREA_ID As Integer = -1, Optional ByVal TAGE_ID As Integer = 0, Optional ByVal OnlyAvailable As Boolean = False)
        Dim SQL As String = "SELECT TAG_ID,TAG_Code " & vbLf
        SQL &= " FROM VW_PIPE_TAG" & vbLf
        Dim Filter As String = ""
        If PLANT_ID <> -1 Then
            Filter &= " PLANT_ID=" & PLANT_ID & " AND "
        End If
        If AREA_ID <> -1 Then
            Filter &= " AREA_ID=" & AREA_ID & " AND "
        End If
        If OnlyAvailable Then
            Filter &= " Active_Status=1 AND "
        End If
        If Filter <> "" Then SQL &= " WHERE " & Filter.Substring(0, Filter.Length - 4) & vbLf
        SQL &= " ORDER BY TAG_Code"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TAG_Code"), DT.Rows(i).Item("TAG_ID"))
            ddl.Items.Add(Item)
        Next
        If TAGE_ID <> 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = TAGE_ID.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Point(ByRef ddl As DropDownList, ByVal TAG_ID As Integer, Optional ByVal POINT_ID As Integer = -1, Optional ByVal IncludedMain As Boolean = True)
        Dim SQL As String = "SELECT TAG_ID,POINT_ID,POINT_Name " & vbLf
        SQL &= " FROM MS_PIPE_POINT" & vbLf
        SQL &= " WHERE TAG_ID=" & TAG_ID & vbLf
        If POINT_ID <> -1 Then
            SQL &= " AND POINT_ID=" & POINT_ID & vbLf
        End If
        If Not IncludedMain Then
            SQL &= " AND POINT_ID<>0" & vbLf
        End If
        SQL &= " ORDER BY POINT_Name"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("POINT_Name"), DT.Rows(i).Item("POINT_ID"))
            ddl.Items.Add(Item)
        Next
        If POINT_ID <> 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = POINT_ID.ToString Then
                    ddl.SelectedIndex = i
                    Exit Sub
                End If
            Next
            ddl.SelectedIndex = 0
        End If
    End Sub

    Public Sub BindDDl_Service(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Service WHERE ISNULL(SERVICE_CODE,'')<>'' AND active_status=1 ORDER BY SERVICE_Name"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("SERVICE_CODE").ToString, DT.Rows(i).Item("SERVICE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Pressure_Code(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Pressure WHERE active_status=1 ORDER BY PRS_Code"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PRS_Code") & " (" & DT.Rows(i).Item("PRS_Force_LB") & " lb)", DT.Rows(i).Item("PRS_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_CorrosionAllowance(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_CA ORDER BY CA_ID"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As ListItem = Nothing
            If DT.Rows(i).Item("CA_ID") = 1 Then
                Item = New ListItem(DT.Rows(i).Item("CA_Code") & " (none)", DT.Rows(i).Item("CA_ID"))
            Else
                Item = New ListItem(DT.Rows(i).Item("CA_Code") & " (" & CDbl(DT.Rows(i).Item("CA_DEPTH_MM")) & " mm)", DT.Rows(i).Item("CA_ID"))
            End If
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Insulation_Code(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Insulation WHERE active_status=1 ORDER BY IN_Code"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("IN_Code"), DT.Rows(i).Item("IN_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_TM_Instrument_Type(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_TM_Instrument_Type ORDER BY INSM_Type_ID"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Instrument Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("INSM_Type_Name"), DT.Rows(i).Item("INSM_Type_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Sub BindDDl_Plant(ByRef ddl As DropDownList, Optional ByVal PLANT_ID As Integer = 0)
        Dim SQL As String = "SELECT DISTINCT MS_PLANT.PLANT_ID,MS_PLANT.PLANT_Code" & vbLf
        SQL &= " FROM MS_PLANT INNER JOIN MS_PIPE_TAG On MS_PLANT.PLANT_ID=MS_PIPE_TAG.PLANT_ID" & vbLf
        SQL &= " WHERE MS_PLANT.active_status=1 AND MS_PIPE_TAG.Active_Status=1 ORDER BY PLANT_CODE" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Plant...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PLANT_CODE"), DT.Rows(i).Item("PLANT_ID"))
            ddl.Items.Add(Item)
        Next
        If PLANT_ID <> 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = PLANT_ID.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Route(ByVal PLANT_ID As Integer, ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)

        Dim SQL As String = "SELECT * FROM MS_ST_Route WHERE active_status=1 AND PLANT_ID=" & PLANT_ID & " ORDER BY ROUTE_CODE"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("ROUTE_CODE"), DT.Rows(i).Item("ROUTE_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Public Sub BindDDl_Area(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT DISTINCT MS_AREA.AREA_ID,MS_AREA.AREA_Code" & vbLf
        SQL &= " FROM MS_AREA INNER JOIN MS_PIPE_TAG On MS_AREA.AREA_ID=MS_PIPE_TAG.AREA_ID " & vbLf
        SQL &= " WHERE MS_AREA.active_status=1 AND MS_PIPE_TAG.Active_Status=1 ORDER BY AREA_CODE" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose an Area...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("AREA_CODE"), DT.Rows(i).Item("AREA_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Area(ByVal PLANT_ID As Integer, ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT DISTINCT MS_AREA.AREA_ID,MS_AREA.AREA_Code" & vbLf
        SQL &= " FROM MS_AREA INNER JOIN MS_PIPE_TAG On MS_AREA.AREA_ID=MS_PIPE_TAG.AREA_ID " & vbLf
        SQL &= " WHERE MS_AREA.PLANT_ID=" & PLANT_ID & " AND MS_AREA.active_status=1 AND MS_PIPE_TAG.Active_Status=1 ORDER BY AREA_CODE" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose an Area...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("AREA_CODE"), DT.Rows(i).Item("AREA_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Process(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT MS_PROCESS.PROC_ID,PROC_Code" & vbLf
        SQL &= " FROM MS_PROCESS INNER JOIN MS_PIPE_TAG On MS_PROCESS.PROC_ID=MS_PIPE_TAG.PROC_ID" & vbLf
        SQL &= " WHERE MS_PROCESS.active_status=1 AND MS_PIPE_TAG.Active_Status=1 ORDER BY PROC_Code" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Process...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PROC_Code"), DT.Rows(i).Item("PROC_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    'Public Sub BindDDl_Material(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
    '    Dim SQL As String = "SELECT * FROM MS_Material WHERE active_status='Y' ORDER BY material_name"
    '    Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
    '    Dim DT As New DataTable
    '    DA.Fill(DT)

    '    ddl.Items.Clear()
    '    ddl.Items.Add(New ListItem("Choose Material...", 0))
    '    For i As Integer = 0 To DT.Rows.Count - 1
    '        Dim Item As New ListItem(DT.Rows(i).Item("material_name"), DT.Rows(i).Item("MAT_CODE_ID"))
    '        ddl.Items.Add(Item)
    '    Next
    '    If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
    '        For i As Integer = 0 To ddl.Items.Count - 1
    '            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
    '                ddl.SelectedIndex = i
    '                Exit For
    '            End If
    '        Next
    '    End If
    'End Sub

    Public Sub BindDDl_Material_Code(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Material_Code ORDER BY MAT_CODE"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("MAT_CODE") & " (" & DT.Rows(i).Item("MAT_CODE_Name") & ")", DT.Rows(i).Item("MAT_CODE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub


    Public Sub BindDDl_Material_Type(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_MATERIAL_TYPE WHERE active_status='Y' ORDER BY material_type_name"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Material Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("material_type_name"), DT.Rows(i).Item("material_type_id"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    'Public Sub BindDDl_Media(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
    '    Dim SQL As String = "SELECT * FROM MS_PIPE_Media WHERE active_status=1 ORDER BY MD_Code"
    '    Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
    '    Dim DT As New DataTable
    '    DA.Fill(DT)

    '    ddl.Items.Clear()
    '    ddl.Items.Add(New ListItem("Choose Media...", 0))
    '    For i As Integer = 0 To DT.Rows.Count - 1
    '        Dim Name As String = DT.Rows(i).Item("MD_Code")
    '        If Not IsDBNull(DT.Rows(i).Item("MD_Name")) AndAlso DT.Rows(i).Item("MD_Name") <> "" Then
    '            Name &= " : " & DT.Rows(i).Item("MD_Name")
    '        End If
    '        Dim Item As New ListItem(Name, DT.Rows(i).Item("MD_ID"))
    '        ddl.Items.Add(Item)
    '    Next
    '    If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
    '        For i As Integer = 0 To ddl.Items.Count - 1
    '            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
    '                ddl.SelectedIndex = i
    '                Exit For
    '            End If
    '        Next
    '    End If
    'End Sub

    Public Sub BindDDl_Insulation(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Insulation WHERE active_status=1 ORDER BY IN_Code"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Insulation...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Name As String = DT.Rows(i).Item("IN_Code")
            If Not IsDBNull(DT.Rows(i).Item("IN_Name")) AndAlso DT.Rows(i).Item("IN_Name") <> "" Then
                Name &= " : " & DT.Rows(i).Item("IN_Name")
            End If
            Dim Item As New ListItem(Name, DT.Rows(i).Item("IN_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Component_Type(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_PIPE_Component_Type WHERE active_status=1 ORDER BY Component_Type_Name"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Component Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Component_Type_Name"), DT.Rows(i).Item("Component_Type"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_Serial(ByRef ddl As DropDownList, Optional ByVal SelectedValue As String = "")
        ddl.Items.Clear()
        ddl.Items.Add("")
        For i As Integer = 1 To 9
            Dim Item As New ListItem(i.ToString.PadLeft(2, "0"), i.ToString.PadLeft(2, "0"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_TM_Instrument(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = 0)
        Dim SQL As String = "SELECT * FROM MS_TM_Instrument WHERE active_status=1 ORDER BY INSM_NAME"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Instrument...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("INSM_NAME"), DT.Rows(i).Item("INSM_ID"))
            ddl.Items.Add(Item)
        Next
        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub Save_FILE(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer, ByVal Attachment As FileAttachment)
        Dim SQL As String = "SELECT * FROM MS_PIPE_FILE" & vbLf
        SQL &= " WHERE TAG_ID = " & TAG_ID & " And POINT_ID = " & POINT_ID & " And UNIQUE_ID ='" & Attachment.UNIQUE_ID & "'" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
            DR("TAG_ID") = TAG_ID
            DR("POINT_ID") = POINT_ID
            DR("UNIQUE_ID") = Attachment.UNIQUE_ID
        Else
            DR = DT.Rows(0)
        End If
        DR("AFT_ID") = Attachment.DocType
        DR("File_Title") = Attachment.Title
        DR("File_Desc") = Attachment.Description
        DR("File_TAG") = Attachment.Tag
        '--------------- Get Dimension --------
        Dim C As New Converter
        Dim SVG As New SVG_API
        Dim IMG As System.Drawing.Image = Nothing
        Dim SVGContent As Byte() = {}
        Dim DocContent As Byte() = {}
        Select Case Attachment.Extension
            Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG,
                 FileAttachment.ExtensionType.PNG, FileAttachment.ExtensionType.TIFF
                IMG = System.Drawing.Image.FromStream(C.ByteToStream(Attachment.Content)) '---------------- Prepare Image Content---------------

                ''เพิ่ม ,Converter.EncodeType._UTF8  แก้ไข convert SVG
                SVGContent = C.StringToByte(SVG.ImageToSVG(IMG), Converter.EncodeType._UTF8) '---------------- Prepare SVG Content----------------------------
                DR("File_Width") = IMG.Width
                DR("File_Height") = IMG.Height

            Case FileAttachment.ExtensionType.SVG
                'เพิ่ม ,Converter.EncodeType._UTF8  แก้ไข convert SVG
                IMG = SVG.SVGToImage(C.ByteToString(Attachment.Content, Converter.EncodeType._UTF8)) '---------------- Prepare Image Content---------------
                SVGContent = Attachment.Content '---------------- Prepare SVG Content------------------------------------------
                DR("File_Width") = IMG.Width
                DR("File_Height") = IMG.Height

            Case FileAttachment.ExtensionType.DOC, FileAttachment.ExtensionType.PDF,
                  FileAttachment.ExtensionType.PPT, FileAttachment.ExtensionType.XLS
                DocContent = Attachment.Content
            Case Else
                Exit Sub
        End Select

        DR("EXT_ID") = Attachment.Extension
        DR("Update_By") = Attachment.LastEditBy
        DR("Update_Time") = Attachment.LastEditTime

        '------------------ Save Physical File--------------
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG\"
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image\"
        Dim DocPath As String = BL.Picture_Path & "\Pipe\Doc\"
        If Not Directory.Exists(SVGPath) Then Directory.CreateDirectory(SVGPath)
        If Not Directory.Exists(ImagePath) Then Directory.CreateDirectory(ImagePath)
        If Not Directory.Exists(DocPath) Then Directory.CreateDirectory(DocPath)

        SVGPath &= Attachment.UNIQUE_ID
        ImagePath &= Attachment.UNIQUE_ID
        DocPath &= Attachment.UNIQUE_ID
        '---------------- Delete Destination -------------
        If File.Exists(SVGPath) Then
            Try
                DeleteFile(SVGPath)
            Catch : End Try
        End If
        If File.Exists(ImagePath) Then
            Try
                DeleteFile(ImagePath)
            Catch : End Try
        End If
        If File.Exists(DocPath) Then
            Try
                DeleteFile(DocPath)
            Catch : End Try
        End If
        '---------------- Save SVG -------------------------
        Select Case Attachment.Extension
            Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG,
            FileAttachment.ExtensionType.PNG, FileAttachment.ExtensionType.TIFF, FileAttachment.ExtensionType.SVG

                Try
                    IMG.Save(ImagePath)
                    Dim F As FileStream = File.Open(SVGPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
                    F.Write(SVGContent, 0, SVGContent.Length)
                    F.Close()
                    F.Dispose()
                Catch ex As Exception
                    Exit Sub
                End Try

            Case FileAttachment.ExtensionType.DOC, FileAttachment.ExtensionType.PDF,
                 FileAttachment.ExtensionType.PPT, FileAttachment.ExtensionType.XLS

                Try
                    Dim st As FileStream = File.Open(DocPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
                    st.Write(DocContent, 0, DocContent.Length)
                    st.Close()
                    st.Dispose()
                Catch ex As Exception
                    Exit Sub
                End Try

        End Select

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
    End Sub

    Public Sub Save_FILE(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer, ByVal Attachments As List(Of FileAttachment), Optional ByVal RemoveOutboundList As Boolean = False)

        Dim SQL As String = "SELECT * FROM MS_PIPE_FILE " & vbLf
        SQL &= "WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If RemoveOutboundList Then
            For i As Integer = DT.Rows.Count - 1 To 0 Step -1
                Dim UNIQUE_ID As String = DT.Rows(i).Item("UNIQUE_ID")
                Dim Existed As Boolean = False
                For j As Integer = 0 To Attachments.Count - 1
                    If Attachments(j).UNIQUE_ID = UNIQUE_ID Then
                        Existed = True
                        Exit For
                    End If
                Next
                If Not Existed Then
                    Try
                        Drop_File(TAG_ID, POINT_ID, UNIQUE_ID)
                        DT.Rows(i).Delete()
                        'cmd = New SqlCommandBuilder(DA)
                        'DA.Update(DT)
                        DT.AcceptChanges()
                    Catch ex As Exception
                        '-------------------- Some File Are Referenced ---------------
                    End Try
                End If
            Next
        End If
        '------------- Save File -----------
        For i As Integer = 0 To Attachments.Count - 1
            Save_FILE(TAG_ID, POINT_ID, Attachments(i))
        Next

    End Sub

    Public Function Get_ReportStep(ByVal ReportType As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = "Select * FROM MS_PIPE_CUI_ERO_Report_Step" & vbLf
        Select Case ReportType
            Case EIR_BL.Report_Type.Pipe_CUI_Reports
                SQL &= "WHERE CUI=1" & vbLf
            Case EIR_BL.Report_Type.Pipe_ERO_Reports
                SQL &= "WHERE ERO=1" & vbLf
        End Select
        SQL &= "ORDER BY RPT_STEP"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_ReportStep_Name(ByVal RPT_Step As Report_Step) As String
        Dim SQL As String = "Select STEP_Name FROM MS_PIPE_CUI_ERO_Report_Step" & vbLf
        SQL &= "WHERE RPT_STEP=" & CInt(RPT_Step) & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Try
            Return DT.Rows(0).Item("STEP_Name")
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Sub BindDDl_ReportStep(ByRef ddl As DropDownList, ByVal ReportType As EIR_BL.Report_Type, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim DT As DataTable = Get_ReportStep(ReportType)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Step...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("STEP_Name"), DT.Rows(i).Item("RPT_STEP"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Function Get_Corrosion_Depth_mm(ByVal CA_ID As Integer) As Double
        Select Case CA_ID
            Case 2
                Return 1.5
            Case 3
                Return 3
            Case 4
                Return 6
            Case Else
                Return 0
        End Select
    End Function

    Public Function Get_Empty_Measurement_DataTable() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("Point", GetType(String))
        For i As Integer = 1 To 9
            DT.Columns.Add("Sub" & i, GetType(Single))
        Next
        Return DT
    End Function

    Public Function Get_Thickness_Mesaurement_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim DT As DataTable = Get_Empty_Measurement_DataTable()
        Dim DB As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM RPT_PIPE_TM_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DB)

        For p As Integer = Asc("A") To Asc("H")
            Dim DR As DataRow = DT.NewRow
            DR("Point") = Chr(p)
            For i As Integer = 1 To 9
                DB.DefaultView.RowFilter = "Measure_Point='" & Chr(p) & i & "'"
                If DB.DefaultView.Count > 0 AndAlso IsNumeric(DB.DefaultView(0).Item("Thickness")) Then
                    DR("Sub" & i) = DB.DefaultView(0).Item("Thickness")
                End If
            Next
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Public Function Get_Previous_Thickness_Mesaurement_Detail(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer, ByVal RPT_Date As Date) As DataTable
        Dim SQL As String = "EXEC dbo.SP_Get_Previous_PIPE_TM_Report_Detail " & TAG_ID & "," & POINT_ID & ",'" & RPT_Date.Year & "-" & RPT_Date.Month & "-" & RPT_Date.Day & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Code(ByVal AREA_Code As String, ByVal PROC_Code As String, ByVal Line_No As String, ByVal Size As Double,
                    ByVal Manual_Class As String, ByVal MAT_Code As String, ByVal PRS_Code As String, ByVal CA_Code As String,
                    ByVal SERIAL_NO As String, ByVal IN_Code As String, ByVal IN_Thickness As Integer) As String

        Dim DT As New DataTable
        Dim SQL As String = "Select dbo.UDF_PIPE_Tag_Code('" & AREA_Code.Replace("'", "''") & "','" & PROC_Code.Replace("'", "''") & "','" & Line_No.Replace("'", "''") & "',"
        If Size <= 0 Then
            SQL &= "Null,"
        Else
            SQL &= Size & ","
        End If
        SQL &= "'" & Manual_Class.Replace("'", "''") & "','" & MAT_Code.Replace("'", "''") & "','" & PRS_Code.Replace("'", "''") & "',"
        SQL &= "'" & CA_Code.Replace("'", "''") & "',"
        SQL &= "'" & SERIAL_NO.Replace("'", "''") & "','" & IN_Code.Replace("'", "''") & "',"
        If IN_Thickness <= 0 Then
            SQL &= "Null)"
        Else
            SQL &= IN_Thickness & ")"
        End If

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT.Rows(0)(0)

    End Function

    Public Function Get_Class(ByVal MAT_Code As String, ByVal PRS_Code As String, ByVal CA_Code As String, ByVal SERIAL_NO As String) As String

        Dim DT As New DataTable
        Dim SQL As String = "Select dbo.UDF_PIPE_Class('" & MAT_Code.Replace("'", "''") & "','" & PRS_Code.Replace("'", "''") & "',"
        SQL &= "'" & CA_Code.Replace("'", "''") & "',"
        SQL &= "'" & SERIAL_NO.Replace("'", "''") & "')"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT.Rows(0)(0)

    End Function


    Public Function Get_Tag_Info(ByVal TAG_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM VW_PIPE_TAG WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Tag_Code_By_ID(ByVal TAG_ID As Integer) As String
        Dim DT As New DataTable
        Dim SQL As String = "Select TAG_Code FROM MS_PIPE_TAG WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("TAG_Code")) Then
            Return DT.Rows(0).Item("TAG_Code")
        Else
            Return ""
        End If
    End Function

    Public Function Get_Mat_Code_By_ID(ByVal MAT_CODE_ID As Integer) As String
        Dim DT As New DataTable
        Dim SQL As String = "Select MAT_CODE FROM MS_PIPE_MATERIAL_CODE WHERE MAT_CODE_ID=" & MAT_CODE_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("MAT_CODE")) Then
            Return DT.Rows(0).Item("MAT_CODE")
        Else
            Return ""
        End If
    End Function

    Public Function Get_New_Point_ID(ByVal TAG_ID As Integer) As String
        Dim DT As New DataTable
        Dim SQL As String = "Select ISNULL(MAX(POINT_ID),0)+1 P FROM MS_PIPE_POINT WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

    Public Function Get_Point_Param(ByVal TAG_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM VW_PIPE_POINT WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Point_Param(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM VW_PIPE_POINT WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_FileAttachment_By_ID(ByVal UNIQUE_ID As String) As FileAttachment
        Dim DT As New DataTable
        Dim SQL As String = "SELECT * FROM MS_PIPE_File WHERE UNIQUE_ID='" & UNIQUE_ID.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Return Nothing

        '-------------- Check Path----------------
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG"
        If Not Directory.Exists(SVGPath) Then
            Directory.CreateDirectory(SVGPath)
        End If
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
        If Not Directory.Exists(ImagePath) Then
            Directory.CreateDirectory(ImagePath)
        End If
        Dim DocPath As String = BL.Picture_Path & "\Pipe\Doc"
        If Not Directory.Exists(DocPath) Then
            Directory.CreateDirectory(DocPath)
        End If

        Dim C As New Converter
        Dim Attachment As New FileAttachment
        Dim DR As DataRow = DT.Rows(0)

        Dim SVG As New SVG_API

        With Attachment
            .UNIQUE_ID = DR("UNIQUE_ID").ToString
            .Title = DR("File_Title").ToString
            .Description = DR("File_Desc").ToString
            .Tag = DR("File_TAG").ToString
            .Extension = DR("EXT_ID")
            .DocType = DR("AFT_ID")

            '-------------- Get .Content
            Select Case .Extension
                Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG, FileAttachment.ExtensionType.PNG,
                             FileAttachment.ExtensionType.SVG, FileAttachment.ExtensionType.TIFF

                    Dim Path As String = SVGPath & "\" & DR("UNIQUE_ID").ToString
                    If File.Exists(Path) Then
                        .Content = C.StringToByte(SVG.GetSVGContent(Path), Converter.EncodeType._UTF8)
                    ElseIf File.Exists(ImagePath & "\" & DR("UNIQUE_ID").ToString) Then
                        Path = ImagePath & "\" & DR("UNIQUE_ID").ToString
                        Dim F As Stream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                        Dim IMG As System.Drawing.Image = System.Drawing.Image.FromStream(F)
                        .Content = C.StringToByte(SVG.ImageToSVG(IMG), Converter.EncodeType._UTF8)
                        F.Dispose()
                        IMG.Dispose()
                    End If

                Case FileAttachment.ExtensionType.DOC, FileAttachment.ExtensionType.PDF,
                     FileAttachment.ExtensionType.PPT, FileAttachment.ExtensionType.XLS

                    Dim Path As String = DocPath & "\" & DR("UNIQUE_ID").ToString
                    If Not File.Exists(Path) Then Return Nothing
                    Dim st As FileStream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                    .Content = C.StreamToByte(st)
                    st.Close()

            End Select

            .LastEditTime = DR("Update_Time")
            .LastEditBy = DR("Update_By")
        End With
        Return Attachment
    End Function

    Public Function Get_Point_File(ByVal TAG_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM MS_PIPE_File WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Point_File(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM MS_PIPE_File WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Point_FileAttachments(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer) As List(Of FileAttachment)

        Dim DT As DataTable = Get_Point_File(TAG_ID, POINT_ID)
        Dim Attachments As New List(Of FileAttachment)
        Dim C As New Converter
        Dim SVG As New SVG_API

        '------------- Sort Attachment ----------
        DT = DT.DefaultView.ToTable.Copy
        DT.DefaultView.Sort = ""

        '-------------- Check Path----------------
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG"
        If Not Directory.Exists(SVGPath) Then
            Directory.CreateDirectory(SVGPath)
        End If
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
        If Not Directory.Exists(ImagePath) Then
            Directory.CreateDirectory(ImagePath)
        End If
        Dim DocPath As String = BL.Picture_Path & "\Pipe\Doc"
        If Not Directory.Exists(DocPath) Then
            Directory.CreateDirectory(DocPath)
        End If

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Attachment As New FileAttachment
            With Attachment
                .UNIQUE_ID = DT.Rows(i).Item("UNIQUE_ID").ToString
                .Title = DT.Rows(i).Item("File_Title").ToString
                .Description = DT.Rows(i).Item("File_Desc").ToString
                .Tag = DT.Rows(i).Item("File_TAG").ToString
                .Extension = DT.Rows(i).Item("EXT_ID")
                .DocType = DT.Rows(i).Item("AFT_ID")

                Select Case .Extension
                    Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG, FileAttachment.ExtensionType.PNG,
                             FileAttachment.ExtensionType.SVG, FileAttachment.ExtensionType.TIFF

                        'Dim Path As String = SVGPath & "\" & DT.Rows(i)("UNIQUE_ID").ToString
                        'If File.Exists(Path) Then
                        '    .Content = C.StringToByte(SVG.GetSVGContent(Path))
                        'ElseIf File.Exists(ImagePath & "\" & DT.Rows(i)("UNIQUE_ID").ToString) Then
                        '    Path = ImagePath & "\" & DT.Rows(i)("UNIQUE_ID").ToString
                        '    Dim F As Stream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                        '    Dim IMG As System.Drawing.Image = System.Drawing.Image.FromStream(F)
                        '    .Content = C.StringToByte(SVG.ImageToSVG(IMG))
                        '    F.Dispose()
                        '    IMG.Dispose()
                        'End If

                        Dim Path As String = SVGPath & "\" & DT.Rows(i)("UNIQUE_ID").ToString
                        If File.Exists(Path) Then
                            .Content = C.StringToByte(SVG.GetSVGContent(Path), Converter.EncodeType._UTF8)
                        ElseIf File.Exists(ImagePath & "\" & DT.Rows(i)("UNIQUE_ID").ToString) Then
                            Path = ImagePath & "\" & DT.Rows(i)("UNIQUE_ID").ToString
                            Dim F As Stream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            Dim IMG As System.Drawing.Image = System.Drawing.Image.FromStream(F)
                            .Content = C.StringToByte(SVG.ImageToSVG(IMG), Converter.EncodeType._UTF8)
                            F.Dispose()
                            IMG.Dispose()
                        End If

                    Case FileAttachment.ExtensionType.DOC, FileAttachment.ExtensionType.PDF,
                     FileAttachment.ExtensionType.PPT, FileAttachment.ExtensionType.XLS

                        Dim Path As String = DocPath & "\" & DT.Rows(i)("UNIQUE_ID").ToString
                        If Not File.Exists(Path) Then Continue For
                        Dim st As FileStream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                        .Content = C.StreamToByte(st)
                        st.Close()

                End Select

                .LastEditTime = DT.Rows(i).Item("Update_Time")
                .LastEditBy = DT.Rows(i).Item("Update_By")
            End With
            Attachments.Add(Attachment)
        Next
        Return Attachments

    End Function

#Region "Point Preview"


    Public Function GET_POINT_PREVIEW(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer) As FileAttachment
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM MS_PIPE_POINT_Preview WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Return Nothing

        Dim C As New Converter
        Dim SVG As New SVG_API
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG"
        If Not Directory.Exists(SVGPath) Then
            Directory.CreateDirectory(SVGPath)
        End If
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
        If Not Directory.Exists(ImagePath) Then
            Directory.CreateDirectory(ImagePath)
        End If
        Dim Attachment As New FileAttachment
        With Attachment
            .UNIQUE_ID = DT.Rows(0).Item("UNIQUE_ID").ToString
            .Title = DT.Rows(0).Item("File_Title").ToString
            .Description = DT.Rows(0).Item("File_Desc").ToString
            .Tag = DT.Rows(0).Item("File_TAG").ToString
            .Extension = DT.Rows(0).Item("EXT_ID")
            .DocType = FileAttachment.AttachmentType.EquipementPreview


            'Dim Path As String = SVGPath & "\" & DT.Rows(0)("UNIQUE_ID").ToString
            'If File.Exists(Path) Then
            '    .Content = C.StringToByte(SVG.GetSVGContent(Path))
            'ElseIf File.Exists(ImagePath & "\" & DT.Rows(0)("UNIQUE_ID").ToString) Then
            '    Path = ImagePath & "\" & DT.Rows(0)("UNIQUE_ID").ToString
            '    Dim F As Stream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            '    Dim IMG As System.Drawing.Image = System.Drawing.Image.FromStream(F)
            '    .Content = C.StringToByte(SVG.ImageToSVG(IMG))
            '    F.Dispose()
            '    IMG.Dispose()
            'End If

            Dim Path As String = SVGPath & "\" & DT.Rows(0)("UNIQUE_ID").ToString
            If File.Exists(Path) Then
                .Content = C.StringToByte(SVG.GetSVGContent(Path), Converter.EncodeType._UTF8)
            ElseIf File.Exists(ImagePath & "\" & DT.Rows(0)("UNIQUE_ID").ToString) Then
                Path = ImagePath & "\" & DT.Rows(0)("UNIQUE_ID").ToString
                Dim F As Stream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim IMG As System.Drawing.Image = System.Drawing.Image.FromStream(F)
                .Content = C.StringToByte(SVG.ImageToSVG(IMG), Converter.EncodeType._UTF8)
                F.Dispose()
                IMG.Dispose()
            End If


            .LastEditTime = DT.Rows(0).Item("Update_Time")
            .LastEditBy = DT.Rows(0).Item("Update_By")
        End With
        Return Attachment
    End Function

    Public Sub SAVE_POINT_PREVIEW(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer, ByVal Attachment As FileAttachment)
        DROP_POINT_PREVIEW(TAG_ID, POINT_ID)

        Dim SQL As String = "SELECT * FROM MS_PIPE_POINT_Preview" & vbLf
        SQL &= " WHERE 1=0"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow = DT.NewRow
        DT.Rows.Add(DR)
        DR("TAG_ID") = TAG_ID
        DR("POINT_ID") = POINT_ID
        DR("UNIQUE_ID") = Attachment.UNIQUE_ID
        DR("File_Title") = Attachment.Title
        DR("File_Desc") = Attachment.Description
        DR("File_TAG") = Attachment.Tag
        '--------------- Get Dimension --------
        Dim C As New Converter
        Dim SVG As New SVG_API
        Dim IMG As System.Drawing.Image = Nothing
        Dim SVGContent As Byte() = {}
        Select Case Attachment.Extension
            Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG,
                 FileAttachment.ExtensionType.PNG, FileAttachment.ExtensionType.TIFF
                IMG = System.Drawing.Image.FromStream(C.ByteToStream(Attachment.Content)) '---------------- Prepare Image Content---------------

                ''เพิ่ม ,Converter.EncodeType._UTF8  แก้ไข convert SVG
                SVGContent = C.StringToByte(SVG.ImageToSVG(IMG), Converter.EncodeType._UTF8) '---------------- Prepare SVG Content----------------------------
            Case FileAttachment.ExtensionType.SVG
                ''เพิ่ม ,Converter.EncodeType._UTF8  แก้ไข convert SVG
                IMG = SVG.SVGToImage(C.ByteToString(Attachment.Content, Converter.EncodeType._UTF8)) '---------------- Prepare Image Content---------------
                SVGContent = Attachment.Content '---------------- Prepare SVG Content------------------------------------------
        End Select
        DR("File_Width") = IMG.Width
        DR("File_Height") = IMG.Height
        DR("EXT_ID") = Attachment.Extension
        DR("Update_By") = Attachment.LastEditBy
        DR("Update_Time") = Attachment.LastEditTime

        '------------------ Save Physical File--------------
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG\" & Attachment.UNIQUE_ID
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image\" & Attachment.UNIQUE_ID
        '---------------- Save SVG -------------------------
        Try
            IMG.Save(ImagePath)
            Dim F As FileStream = File.Open(SVGPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
            F.Write(SVGContent, 0, SVGContent.Length)
            F.Close()
            F.Dispose()
        Catch ex As Exception
            Exit Sub
        End Try

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
    End Sub

    Public Sub DROP_POINT_PREVIEW(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer)
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM MS_PIPE_POINT_Preview WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        Dim UNIQUE_ID As String = DT.Rows(0).Item("UNIQUE_ID")
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM MS_PIPE_POINT_Preview WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID & " And UNIQUE_ID='" & UNIQUE_ID.Replace("'", "''") & "'"
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
        '-------- Delete File -------------
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG\" & UNIQUE_ID
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image\" & UNIQUE_ID
        Dim DocPath As String = BL.Picture_Path & "\Pipe\Doc\" & UNIQUE_ID
        Try
            If File.Exists(SVGPath) Then DeleteFile(SVGPath)
        Catch : End Try
        Try
            If File.Exists(ImagePath) Then DeleteFile(ImagePath)
        Catch : End Try
        Try
            If File.Exists(DocPath) Then DeleteFile(DocPath)
        Catch : End Try
    End Sub
#End Region

    Public Function Get_All_Report_Step(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = ""
        SQL &= " DECLARE @RPT_Year AS INT =" & RPT_Year & vbLf
        SQL &= " Declare @RPT_No As INT=" & RPT_No & vbLf
        SQL &= " " & vbLf
        SQL &= " Select RPT.RPT_STEP,STEP_Name,RPT_Date,ReportPage,StartPage,EndPage FROM" & vbLf
        SQL &= " (" & vbLf
        SQL &= " Select 1 RPT_STEP,RPT_Date,2 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_Before_Remove WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 2 RPT_STEP,RPT_Date,2 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_After_Remove WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 3 RPT_STEP,RPT_Date,1 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_TM_Header WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 4 RPT_STEP,RPT_Date,1 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_After_Repair WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 5 RPT_STEP,RPT_Date,1 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_Coating WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 6 RPT_STEP,RPT_Date,1 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_Insulating WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " UNION ALL" & vbLf
        SQL &= " Select 7 RPT_STEP,RPT_Date,1 ReportPage,0 StartPage,0 EndPage FROM RPT_PIPE_Final_Report WHERE RPT_Year=@RPT_Year And RPT_No=@RPT_No" & vbLf
        SQL &= " " & vbLf
        SQL &= " ) RPT INNER JOIN MS_PIPE_CUI_ERO_Report_Step STEP ON RPT.RPT_STEP=STEP.RPT_STEP" & vbLf
        SQL &= " ORDER BY RPT.RPT_STEP"

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        '------------ Calculate Page For Report-----------
        Dim cur As Integer = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            cur += 1
            DT.Rows(i).Item("StartPage") = cur
            For j As Integer = 1 To DT.Rows(i).Item("ReportPage") - 1
                cur += 1
            Next
            DT.Rows(i).Item("EndPage") = cur
        Next

        Return DT
    End Function

    Public Function Get_CUI_ReportHeader_By_Tag(ByVal TAG_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM RPT_PIPE_CUI_Header WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_ERO_ReportHeader_By_Tag(ByVal TAG_ID As Integer) As DataTable
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM RPT_PIPE_ERO_Header WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Public Sub SAVE_RPT_FILE(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step, ByVal ImageID As Integer, ByRef Attachment As FileAttachment)
        If IsNothing(Attachment) Then Exit Sub

        Dim TableName As String = ""
        Dim SQL As String = "Select TAG_ID,POINT_ID FROM " & vbLf
        Select Case RPT_Step
            Case Report_Step.Before_Remove
                TableName = "RPT_PIPE_Before_Remove"
            Case Report_Step.After_Remove
                TableName = "RPT_PIPE_After_Remove"
            Case Report_Step.Thickness_Measurement
                TableName = "RPT_PIPE_TM_Header"
            Case Report_Step.After_Repair
                TableName = "RPT_PIPE_After_Repair"
            Case Report_Step.Coating_Inspection
                TableName = "RPT_PIPE_Coating"
            Case Report_Step.Insulating_Inspection
                TableName = "RPT_PIPE_Insulating"
            Case Report_Step.Final
                TableName = "RPT_PIPE_Final_Report"
        End Select
        SQL &= TableName & " TB " & vbLf
        SQL &= " INNER JOIN ( " & vbLf
        SQL &= " Select RPT_Year,RPT_No,TAG_ID,POINT_ID FROM RPT_PIPE_CUI_Header " & vbLf
        SQL &= " UNION ALL " & vbLf
        SQL &= " Select RPT_Year,RPT_No,TAG_ID,POINT_ID FROM RPT_PIPE_ERO_Header " & vbLf
        SQL &= " ) Header On TB.RPT_Year=Header.RPT_Year And TB.RPT_No=Header.RPT_No" & vbLf
        SQL &= " WHERE TB.RPT_Year=" & RPT_Year & " And TB.RPT_No=" & RPT_No
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        Dim TAG_ID As Integer = DT.Rows(0).Item("TAG_ID")
        Dim POINT_ID As Integer = DT.Rows(0).Item("POINT_ID")
        Save_FILE(TAG_ID, POINT_ID, Attachment)

        SQL = "UPDATE " & TableName & " Set IMG" & ImageID & "='" & Attachment.UNIQUE_ID.Replace("'", "''") & "'"
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim COM As New SqlCommand
        Dim CON As New SqlConnection(BL.ConnStr)
        CON.Open()
        With COM
            .Connection = CON
            .CommandType = CommandType.Text
            .CommandText = SQL
            .ExecuteNonQuery()
            .Dispose()
        End With
        CON.Close()
        CON.Dispose()
    End Sub

#Region "Drop PIPE POINT FILE"
    Public Sub Drop_TAG(ByVal TAG_ID As Integer)
        '---------- Drop CUI Report-------------------
        Drop_RPT_CUI_Header(TAG_ID)
        '---------- Drop ERO Report-------------------
        Drop_RPT_ERO_Header(TAG_ID)
        '---------- Drop Routine Report---------------
        '---------- Drop Off-Routine Report-----------
        '---------- Drop Measurement Point -----------
        DROP_POINT(TAG_ID)
        '---------- Drop Tag Info --------------------
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM MS_PIPE_TAG WHERE TAG_ID=" & TAG_ID
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub DROP_POINT(ByVal TAG_ID As Integer)
        Drop_FILE(TAG_ID)
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM MS_PIPE_POINT WHERE TAG_ID=" & TAG_ID
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub DROP_POINT(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer)
        Drop_FILE(TAG_ID, POINT_ID)
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM MS_PIPE_POINT WHERE TAG_ID=" & TAG_ID & " And POINT_ID=" & POINT_ID
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub Drop_FILE_BY_ID(ByVal UNIQUE_ID As String)
        Dim SQL As String = "SELECT TAG_ID,POINT_ID FROM MS_PIPE_FILE WHERE UNIQUE_ID='" & UNIQUE_ID.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            DA.Dispose()
            DT.Dispose()
            Exit Sub
        End If
        Drop_File(DT.Rows(0).Item("TAG_ID"), DT.Rows(0).Item("POINT_ID"), UNIQUE_ID)
        DA.Dispose()
        DT.Dispose()
    End Sub

    Public Sub Drop_FILE(ByVal TAG_ID As Integer)
        Dim SQL As String = "SELECT POINT_ID,UNIQUE_ID FROM MS_PIPE_FILE WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            '-------- Delete File -------------
            Drop_File(TAG_ID, DT.Rows(i).Item("POINT_ID"), DT.Rows(i).Item("UNIQUE_ID"))
        Next
        DA.Dispose()
        DT.Dispose()
    End Sub

    Public Sub Drop_FILE(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer)
        Dim SQL As String = "SELECT UNIQUE_ID FROM MS_PIPE_FILE WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            '-------- Delete File -------------
            Drop_File(TAG_ID, POINT_ID, DT.Rows(i).Item("UNIQUE_ID").ToString)
        Next
        DA.Dispose()
        DT.Dispose()
    End Sub

    Public Sub Drop_File(ByVal TAG_ID As Integer, ByVal POINT_ID As Integer, ByVal UNIQUE_ID As String)

        DROP_POINT_PREVIEW(TAG_ID, POINT_ID)

        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM MS_PIPE_FILE WHERE TAG_ID=" & TAG_ID & " AND POINT_ID=" & POINT_ID & " And UNIQUE_ID='" & UNIQUE_ID.Replace("'", "''") & "'"
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
        '-------- Delete File -------------
        Dim SVGPath As String = BL.Picture_Path & "\Pipe\SVG\" & UNIQUE_ID
        Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image\" & UNIQUE_ID
        Dim DocPath As String = BL.Picture_Path & "\Pipe\Doc\" & UNIQUE_ID
        Try
            If File.Exists(SVGPath) Then DeleteFile(SVGPath)
        Catch : End Try
        Try
            If File.Exists(ImagePath) Then DeleteFile(ImagePath)
        Catch : End Try
        Try
            If File.Exists(DocPath) Then DeleteFile(DocPath)
        Catch : End Try

    End Sub
#End Region

#Region "Drop Report"
    Public Sub Drop_RPT_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step, ByVal ImageID As Integer)

        Dim SQL As String = "Select * From "
        Select Case RPT_Step
            Case Report_Step.Before_Remove
                SQL &= "RPT_PIPE_Before_Remove" & vbLf
            Case Report_Step.After_Remove
                SQL &= "RPT_PIPE_After_Remove" & vbLf
            Case Report_Step.Thickness_Measurement
                SQL &= "RPT_PIPE_TM_Header" & vbLf
            Case Report_Step.After_Repair
                SQL &= "RPT_PIPE_After_Repair" & vbLf
            Case Report_Step.Coating_Inspection
                SQL &= "RPT_PIPE_Coating" & vbLf
            Case Report_Step.Insulating_Inspection
                SQL &= "RPT_PIPE_Insulating" & vbLf
            Case Report_Step.Final
                SQL &= "RPT_PIPE_Final_Report" & vbLf
        End Select
        SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        If IsDBNull(DT.Rows(0).Item("IMG" & ImageID)) Then Exit Sub
        Dim UNIQUE_ID As String = DT.Rows(0).Item("IMG" & ImageID)
        Try
            Drop_FILE_BY_ID(UNIQUE_ID)
        Catch : End Try
        DT.Rows(0).Item("IMG" & ImageID) = DBNull.Value
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        DT.Dispose()
        DA.Dispose()
    End Sub

    Public Sub Drop_RPT_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step)
        Dim TotalImage As Integer = 1
        Select Case RPT_Step
            Case Report_Step.Thickness_Measurement
                TotalImage = 2
            Case Else
                TotalImage = 4
        End Select
        For i As Integer = 1 To TotalImage
            Drop_RPT_Image(RPT_Year, RPT_No, RPT_Step, i)
        Next
    End Sub

    Public Sub Drop_RPT_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.Final)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.Insulating_Inspection)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.Coating_Inspection)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.After_Repair)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.Thickness_Measurement)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.After_Remove)
        Drop_RPT_Image(RPT_Year, RPT_No, Report_Step.Before_Remove)

        '---------- Delete Folder-----------
        Dim Path As String = BL.Picture_Path & "\" & RPT_Year & "\" & RPT_No
        If Directory.Exists(Path) Then
            Try
                My.Computer.FileSystem.DeleteDirectory(Path, FileIO.DeleteDirectoryOption.DeleteAllContents)
            Catch : End Try
        End If
    End Sub

    Public Sub Drop_RPT_By_Step(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step)
        '------------ Drop Image-------------
        Drop_RPT_Image(RPT_Year, RPT_No, RPT_Step)
        '------------ Drop From Database ----
        Dim Table As String() = {}
        Select Case RPT_Step
            Case Report_Step.Before_Remove
                BL.PushString(Table, "RPT_PIPE_Before_Remove")
            Case Report_Step.After_Remove
                BL.PushString(Table, "RPT_PIPE_After_Remove")
            Case Report_Step.Thickness_Measurement
                BL.PushString(Table, "RPT_PIPE_TM_Detail")
                BL.PushString(Table, "RPT_PIPE_TM_Header")
            Case Report_Step.After_Repair
                BL.PushString(Table, "RPT_PIPE_After_Repair")
            Case Report_Step.Coating_Inspection
                BL.PushString(Table, "RPT_PIPE_Coating")
            Case Report_Step.Insulating_Inspection
                BL.PushString(Table, "RPT_PIPE_Insulating")
            Case Report_Step.Final
                BL.PushString(Table, "RPT_PIPE_Final_Report")
        End Select

        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = ""
            For i As Integer = 0 To Table.Length - 1
                Drop_RPT_Image(RPT_Year, RPT_No, RPT_Step)
                .CommandText &= "DELETE FROM " & Table(i) & " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf
            Next
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
        Table = Nothing
    End Sub

    Public Sub Drop_RPT_TM_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = ""
            .CommandText &= "DELETE FROM RPT_PIPE_TM_Detail WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub Drop_RPT_By_Tag(ByVal TAG_ID As Integer, ByVal RPT_Step As Report_Step)
        Dim DT As DataTable = Get_CUI_ReportHeader_By_Tag(TAG_ID)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_By_Step(DT.Rows(i).Item("RPT_Year"), DT.Rows(i).Item("RPT_No"), RPT_Step)
        Next
    End Sub


#Region "CUI_Header"
    Public Sub Drop_RPT_CUI_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Final)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Insulating_Inspection)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Coating_Inspection)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.After_Repair)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Thickness_Measurement)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.After_Remove)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Before_Remove)

        '---------Drop Posted Report File-----------
        Dim Path As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT Result_FileName FROM RPT_PIPE_CUI_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("Result_FileName")) AndAlso DT.Rows(0).Item("Result_FileName") <> "" Then
            Path = BL.PostedReport_Path & "\" & DT.Rows(0).Item("Result_FileName")
            If File.Exists(Path) Then
                Try : My.Computer.FileSystem.DeleteFile(Path) : Catch : End Try
            End If
        End If
        DT.Dispose()
        DA.Dispose()

        '--------- Drop Header------------
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM RPT_PIPE_CUI_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()

    End Sub

    Public Sub Drop_RPT_CUI_Header(ByVal TAG_ID As Integer)
        Dim DT As DataTable = Get_CUI_ReportHeader_By_Tag(TAG_ID)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_CUI_Header(DT.Rows(i).Item("RPT_Year"), DT.Rows(i).Item("RPT_No"))
        Next
    End Sub

    Public Sub Update_CUI_Header_Step(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step, ByVal USER_ID As Integer)
        Dim SQL As String = "UPDATE RPT_PIPE_CUI_Header SET" & vbLf
        SQL &= " RPT_Step=" & CInt(RPT_Step) & "," & vbLf
        SQL &= " Update_By=" & USER_ID & "," & vbLf
        SQL &= " Update_Time=GetDate()" & vbLf
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = SQL
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
#End Region

#Region "EROSION Header"

    Public Sub Drop_RPT_ERO_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Final)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Insulating_Inspection)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Coating_Inspection)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.After_Repair)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Thickness_Measurement)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.After_Remove)
        Drop_RPT_By_Step(RPT_Year, RPT_No, Report_Step.Before_Remove)

        '---------Drop Posted Report File-----------
        Dim Path As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT Result_FileName FROM RPT_PIPE_ERO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("Result_FileName")) AndAlso DT.Rows(0).Item("Result_FileName") <> "" Then
            Path = BL.PostedReport_Path & "\" & DT.Rows(0).Item("Result_FileName")
            If File.Exists(Path) Then
                Try : My.Computer.FileSystem.DeleteFile(Path) : Catch : End Try
            End If
        End If
        DT.Dispose()
        DA.Dispose()

        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM RPT_PIPE_ERO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()

    End Sub

    Public Sub Drop_RPT_ERO_Header(ByVal TAG_ID As Integer)
        Dim DT As DataTable = Get_ERO_ReportHeader_By_Tag(TAG_ID)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_ERO_Header(DT.Rows(i).Item("RPT_Year"), DT.Rows(i).Item("RPT_No"))
        Next
    End Sub

    Public Sub Update_ERO_Header_Step(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step, ByVal USER_ID As Integer)
        Dim SQL As String = "UPDATE RPT_PIPE_ERO_Header SET" & vbLf
        SQL &= " RPT_Step=" & CInt(RPT_Step) & "," & vbLf
        SQL &= " Update_By=" & USER_ID & "," & vbLf
        SQL &= " Update_Time=GetDate()" & vbLf
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = SQL
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
#End Region

    Public Function Is_Existing_Step(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As Report_Step) As Boolean
        Dim SQL As String = "SELECT dbo.UDF_PIPE_CUI_ERO_EXISING_STEP(" & RPT_Year & "," & RPT_No & "," & RPT_Step & ")"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

#End Region

#Region "Routine Report"

    Public Sub Construct_Routine_Report_Detail(ByVal By As Integer, ByVal RPT_Year As Integer, ByVal RPT_No As Integer, Optional ByVal TAG_ID As Integer = 0)
        Dim SQL As String = "EXEC dbo.SP_New_PIPE_Routine_Report " & RPT_Year & "," & RPT_No & ","
        If TAG_ID = 0 Then
            SQL &= "NULL"
        Else
            SQL &= TAG_ID
        End If
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim LastData As New DataTable ' ----------- New Data -----------
        DA.Fill(LastData)

        SQL = "Select * FROM RPT_PIPE_Routine_Detail WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Dim DT As New DataTable ' ----------- Current Data -----------
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        '--------------- Compare --------------
        Dim ColInsp() As String = {"TAG_ID", "INSP_ID"}

        '---------- Find Point To Be Remove ------------------
        DT.DefaultView.RowFilter = ""
        Dim OldKey As DataTable = DT.DefaultView.ToTable(True, ColInsp).Copy
        For i As Integer = 0 To OldKey.Rows.Count - 1
            LastData.DefaultView.RowFilter = "TAG_ID=" & OldKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & OldKey.Rows(i).Item("INSP_ID")
            If LastData.DefaultView.Count = 0 Then '----------- Must Remove From DT --------------
                DT.DefaultView.RowFilter = "TAG_ID=" & OldKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & OldKey.Rows(i).Item("INSP_ID")
                While DT.DefaultView.Count > 0
                    DT.DefaultView(0).Row.Delete()
                    Dim cmd As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                    DT.AcceptChanges()
                End While
            End If
        Next

        '---------- Find New Tag And Inspection Point --------
        LastData.DefaultView.RowFilter = ""
        Dim NewKey As DataTable = LastData.DefaultView.ToTable(True, ColInsp).Copy



        For i As Integer = 0 To NewKey.Rows.Count - 1
            DT.DefaultView.RowFilter = "TAG_ID=" & NewKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & NewKey.Rows(i).Item("INSP_ID")
            If DT.DefaultView.Count = 0 Then '----------- Add New Key From DT --------------
                LastData.DefaultView.RowFilter = "TAG_ID=" & NewKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & NewKey.Rows(i).Item("INSP_ID")
                Dim StrartDetailID As Integer = Get_New_Routine_DetailID()
                For r As Integer = 0 To LastData.DefaultView.Count - 1
                    Dim DR As DataRow = DT.NewRow
                    DR("DETAIL_ID") = StrartDetailID + r
                    DR("RPT_Year") = RPT_Year
                    DR("RPT_No") = RPT_No
                    DR("TAG_ID") = LastData.DefaultView(r).Item("TAG_ID")
                    'DR("TAG_TYPE_ID") = LastData.DefaultView(r).Item("TAG_TYPE_ID")
                    DR("INSP_ID") = LastData.DefaultView(r).Item("INSP_ID")
                    If Not IsDBNull(LastData.DefaultView(r).Item("STATUS_ID")) Then
                        DR("STATUS_ID") = LastData.DefaultView(r).Item("STATUS_ID")
                    End If
                    If Not IsDBNull(LastData.DefaultView(r).Item("COMP_NO")) Then
                        DR("COMP_NO") = LastData.DefaultView(r).Item("COMP_NO")
                    End If
                    DR("PROB_Detail") = ""
                    DR("PROB_Recomment") = ""
                    If Not IsDBNull(LastData.DefaultView(r).Item("LAST_DETAIL_ID")) Then
                        DR("LAST_DETAIL_ID") = LastData.DefaultView(r).Item("LAST_DETAIL_ID")
                    End If
                    If Not IsDBNull(LastData.DefaultView(r).Item("Responsible")) Then
                        DR("Responsible") = LastData.DefaultView(r).Item("Responsible")
                    End If
                    DR("ICLS_ID") = DBNull.Value
                    DR("Create_Flag") = "Main"
                    DR("Responsible") = ""
                    DR("PIC_Detail1") = False
                    DR("PIC_Detail2") = False
                    DR("Detail1") = ""
                    DR("Detail2") = ""
                    '----------------------------------------------                   
                    DT.Rows.Add(DR)
                Next
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT)
            End If
        Next

    End Sub

    Public Sub BindCmbReportOfficer(ByRef cmb As DropDownList,
                                   ByVal RPT_Year As Integer,
                                   ByVal RPT_No As Integer,
                                   ByVal PLANT_ID As Integer,
                                   ByVal Role As EIR_BL.User_Level,
                                   Optional ByVal User_Full_Name As String = "")

        Dim Type_Filter As String = "S.S_Stationary=1"
        Dim HeaderTable As String = "RPT_PIPE_Routine_Header"

        Dim SQL As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbLf
        SQL &= " DECLARE @RPT_No AS INT =" & RPT_No & vbLf
        SQL &= " DECLARE @PLANT_ID As INT =" & PLANT_ID & vbLf
        SQL &= " SELECT DISTINCT * FROM" & vbLf
        SQL &= " (SELECT P.POS_ID,ISNULL(U.User_Prefix,'') + ISNULL(U.USER_Name,'') + ' ' + ISNULL(U.User_Surname,'') User_Full_Name" & vbLf
        SQL &= " FROM MS_USER U " & vbLf
        SQL &= " INNER JOIN MS_User_Coverage C ON U.USER_ID=C.USER_ID" & vbLf
        SQL &= " INNER JOIN MS_User_Position P ON U.POS_ID=P.POS_ID" & vbLf
        SQL &= " INNER JOIN MS_User_Skill S ON U.USER_ID=S.USER_ID" & vbLf

        Dim RoleName As String = ""
        Select Case Role
            Case EIR_BL.User_Level.Collector '----------- Collected By -----------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID AND " & Type_Filter & vbLf
                'SQL &= " AND U.LEVEL_ID IN (1,2) " & vbLf ' (Collector,Inspector)
                'SQL &= " AND U.POS_ID IN (0,1) " & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Collector,'') User_Full_Name FROM " & HeaderTable & vbLf

                RoleName = "Collector"

            Case EIR_BL.User_Level.Inspector '----------- Inspected By -----------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID AND " & Type_Filter & vbLf
                'SQL &= " AND U.LEVEL_ID IN (2) " & vbLf ' (Inspector)
                SQL &= " AND U.POS_ID IN (1,2,3)" & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Inspector,'') User_Full_Name FROM " & HeaderTable & vbLf

                RoleName = "Inspector"

            Case EIR_BL.User_Level.Engineer '---------------- Engineer ---------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID AND " & Type_Filter & vbLf
                SQL &= " AND U.POS_ID =2" & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Engineer,'') User_Full_Name FROM " & HeaderTable & vbLf

                RoleName = "Engineer"

            Case EIR_BL.User_Level.Approver  '----------- Approved By -----------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID AND " & Type_Filter & vbLf
                SQL &= " AND U.POS_ID IN (2,3)" & vbLf
                SQL &= " AND U.LEVEL_ID IN (3) " & vbLf ' (Approver)
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Analyst,'') User_Full_Name FROM " & HeaderTable & vbLf

                RoleName = "Approver"

        End Select
        SQL &= " WHERE RPT_Year=@RPT_Year AND RPT_No=@RPT_No" & vbLf
        SQL &= " ) A" & vbLf
        SQL &= " WHERE User_Full_Name<>''" & vbLf
        SQL &= " ORDER BY POS_ID DESC,User_Full_Name ASC" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim Col() As String = {"User_Full_Name"}
        DT = DT.DefaultView.ToTable(True, "User_Full_Name")

        cmb.Items.Clear()
        cmb.Items.Add(New ListItem("Choose " & RoleName & "...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("User_Full_Name"), DT.Rows(i).Item("User_Full_Name"))
            cmb.Items.Add(Item)
            If User_Full_Name = DT.Rows(i).Item("User_Full_Name") Then
                cmb.SelectedIndex = cmb.Items.Count - 1
            End If
        Next
    End Sub

    Public Function Get_New_Routine_DetailID() As Integer
        Dim SQL As String = "Select IsNull(MAX(Detail_ID),0)+1 FROM RPT_PIPE_Routine_Detail "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Public Function Get_ROUTINE_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DETAIL_ID As Integer, ByVal IMG_POS As Integer) As Byte()
        Dim SQL As String = "Select PIC_Detail" & IMG_POS & " FROM RPT_PIPE_Routine_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) AndAlso DT.Rows(0).Item(0) Then
            Dim ImgPath As String = BL.Picture_Path & "\" & RPT_Year & "\" & RPT_No & "\" & DETAIL_ID & "_" & IMG_POS
            If File.Exists(ImgPath) Then
                Dim C As New Converter
                Dim F As FileStream = File.Open(ImgPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim S As Byte() = C.StreamToByte(F)
                F.Close()
                F.Dispose()
                Return S
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public Function Get_ROUTINE_Image(ByVal DETAIL_ID As Integer, ByVal IMG_POS As Integer) As Byte()
        Dim SQL As String = "Select PIC_Detail" & IMG_POS & ",RPT_Year,RPT_No FROM RPT_PIPE_Routine_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) AndAlso DT.Rows(0).Item(0) Then
            Dim ImgPath As String = BL.Picture_Path & "\" & DT.Rows(0).Item("RPT_Year") & "\" & DT.Rows(0).Item("RPT_No") & "\" & DETAIL_ID & "_" & IMG_POS
            If File.Exists(ImgPath) Then
                Dim C As New Converter
                Dim F As FileStream = File.Open(ImgPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim S As Byte() = C.StreamToByte(F)
                F.Close()
                F.Dispose()
                Return S
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public Function Save_ROUTINE_Image(ByVal Pic_Detail As Byte(), ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DETAIL_ID As Integer, ByVal IMG_POS As Integer) As Boolean
        Dim Path As String = BL.Picture_Path & "\" & RPT_Year
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If
        Path &= "\" & RPT_No
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If
        Path &= "\" & DETAIL_ID & "_" & IMG_POS
        Dim CountProcess As Integer = 0
        If File.Exists(Path) Then
            '-------------- Changed------------
            While File.Exists(Path)
                CountProcess += 1
                If CountProcess > 20 Then Return False
                Try
                    Kill(Path)
                Catch ex As Exception
                    Threading.Thread.Sleep(500)
                End Try
            End While
        End If
        If IsNothing(Pic_Detail) Then Return True

        CountProcess = 0
        Do While True
            Try
                CountProcess += 1
                If CountProcess > 20 Then Return False

                Dim F As FileStream = File.Open(Path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
                F.Write(Pic_Detail, 0, Pic_Detail.Length)
                F.Close()
                Return True
            Catch ex As Exception
                Threading.Thread.Sleep(500)
            End Try
        Loop
        Return False
    End Function

    Public Function Check_Previous_Incomplete_Routine_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, Optional ByVal TAG_ID As Integer = 0) As DataTable
        Dim SQL As String = "EXEC dbo.SP_Get_Previous_Incompleted_PIPE_ROUTINE_Report " & RPT_Year & "," & RPT_No & ","
        If TAG_ID <> 0 Then
            SQL &= TAG_ID
        Else
            SQL &= "NULL"
        End If
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Sub DROP_ROUTINE_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        DROP_ROUTINE_Detail(RPT_Year, RPT_No)

        Dim SQL As String = "DELETE FROM RPT_PIPE_Routine_Header " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        BL.Execute_Command(SQL)

    End Sub

    Public Sub DROP_ROUTINE_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim SQL As String = "Select * FROM RPT_PIPE_Routine_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            DROP_ROUTINE_Picture(DT.Rows(i).Item("DETAIL_ID"))
        Next
        SQL = "DELETE FROM RPT_PIPE_Routine_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        BL.Execute_Command(SQL)
    End Sub

    Public Sub DROP_ROUTINE_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal TAG_ID As Integer)
        Dim SQL As String = "Select * FROM RPT_PIPE_Routine_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " And TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            DROP_ROUTINE_Picture(DT.Rows(i).Item("DETAIL_ID"))
        Next
        SQL = "DELETE FROM RPT_PIPE_Routine_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " And TAG_ID=" & TAG_ID
        BL.Execute_Command(SQL)
    End Sub

    Public Sub DROP_ROUTINE_Picture(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "Select * FROM RPT_PIPE_Routine_Detail " & vbNewLine
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            DT.Rows(0).Item("PIC_Detail1") = False
            DT.Rows(0).Item("PIC_Detail2") = False
            DT.Rows(0).Item("Detail1") = ""
            DT.Rows(0).Item("Detail2") = ""
            '-------------------- Delete File ---------------
            For i As Integer = 1 To 2
                Dim FileName As String = BL.Picture_Path & "\" & DT.Rows(0).Item("RPT_Year") & "\" & DT.Rows(0).Item("RPT_No") & "\" & DETAIL_ID & "_" & i
                If File.Exists(FileName) Then
                    Try
                        Kill(FileName)
                    Catch
                    End Try
                End If
            Next
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
            DT.AcceptChanges()
        End If
    End Sub

    Public Sub DROP_ROUTINE_Detail_CheckFlag(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "Select * FROM RPT_PIPE_Routine_Detail " & vbNewLine
        SQL &= "WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("Create_Flag") = "New" Then
                DROP_ROUTINE_Picture(DETAIL_ID)
                SQL = "DELETE FROM RPT_PIPE_Routine_Detail " & vbNewLine
                SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
                BL.Execute_Command(SQL)
            Else
                Dim DR As DataRow = DT.Rows(0)
                DR("PROB_Detail") = ""
                DR("PROB_Recomment") = ""
                DR("ICLS_ID") = DBNull.Value
                DR("Create_Flag") = "Main"
                DR("Responsible") = ""
                DROP_ROUTINE_Picture(DETAIL_ID)
                BL.Execute_Command(SQL)
            End If
        End If
    End Sub

#End Region

End Class
