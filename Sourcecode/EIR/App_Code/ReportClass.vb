﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports iTextSharp.text.pdf
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Join.LeftJoin
Imports Microsoft.VisualBasic
Imports System.Configuration

Public Class ReportClass

    Dim BL As New EIR_BL
    Dim Visual As New DataVisualizedImage
    Dim RPT_Year As Integer = 0
    Dim RPT_No As Integer = 0
    Dim PDFListOffRoutine As String() = {}
    Public rptFormula As Formula

    Public Structure Formula
        Dim Header As String
        Dim DocDetail As String
        Dim Plant As String
        Dim Collected_By As String
        Dim Collected_Date As String
    End Structure

    Public ReadOnly Property ServerMapPath() As String
        Get
            Return ConfigurationManager.AppSettings.Item("ServerMapPath").ToString
        End Get
    End Property

    Public Function CrystalLogonInfo() As TableLogOnInfo

        Dim logonInfo As New TableLogOnInfo
        logonInfo.ConnectionInfo.ServerName = ConfigurationManager.AppSettings.Item("Crystal_Login_ServerName").ToString
        logonInfo.ConnectionInfo.DatabaseName = ConfigurationManager.AppSettings.Item("Crystal_Login_Database").ToString
        logonInfo.ConnectionInfo.UserID = ConfigurationManager.AppSettings.Item("Crystal_Login_User").ToString
        logonInfo.ConnectionInfo.Password = ConfigurationManager.AppSettings.Item("Crystal_Login_Password").ToString
        logonInfo.ConnectionInfo.IntegratedSecurity = False
        Return logonInfo
    End Function

    Public Function GetReport(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, Optional ByVal Detail_ID As Integer = 0) As String 'ถ้า Create Report สำเร็จ Return เป็น Path เต็มของ Report ที่ Create ได้ ถ้าไม่สำเร็จ Return ""
        'REport Type หาข้างใน

        '------------------- Initiate UNIQUE_ID -------------------------
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        '----------------------------------------------------------------

        If RPT_Year = 0 Or RPT_No = 0 Then Return ""

        Dim sql As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable

        sql &= "SELECT RPT_STEP,RPT_Type_ID,Result_FileName FROM RPT_ST_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbNewLine
        sql &= "UNION ALL" & vbNewLine
        sql &= "SELECT RPT_STEP,RPT_Type_ID,Result_FileName FROM RPT_RO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbNewLine
        sql &= "UNION ALL" & vbNewLine
        sql &= "SELECT RPT_STEP," & EIR_BL.Report_Type.Lube_Oil_Report & " as RPT_Type_ID,Result_FileName FROM RPT_LO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbNewLine
        sql &= "UNION ALL" & vbNewLine
        sql &= "SELECT RPT_STEP," & EIR_BL.Report_Type.PdMA_Report & " as RPT_Type_ID,Result_FileName FROM RPT_PdMA_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbNewLine
        sql &= "UNION ALL" & vbNewLine
        sql &= "SELECT RPT_STEP," & EIR_BL.Report_Type.Spring_Hanger_Report & " as RPT_Type_ID,Result_FileName FROM SPH_RPT_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbNewLine
        sql &= "UNION ALL" & vbNewLine
        sql &= "SELECT RPT_STEP," & EIR_BL.Report_Type.MTap_Report & " as RPT_Type_ID,Result_FileName FROM RPT_MTAP_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbNewLine
        sql &= "UNION ALL" & vbNewLine
        sql &= "SELECT RPT_STEP," & EIR_BL.Report_Type.Thermography_Report & " as RPT_Type_ID,Result_FileName FROM RPT_THM_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbNewLine
        sql &= "UNION ALL" & vbNewLine
        sql &= "SELECT RPT_STEP," & EIR_BL.Report_Type.Pipe_CUI_Reports & " as RPT_Type_ID,Result_FileName FROM RPT_PIPE_CUI_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbNewLine
        sql &= "UNION ALL" & vbNewLine
        sql &= "SELECT RPT_STEP," & EIR_BL.Report_Type.Pipe_ERO_Reports & " as RPT_Type_ID,Result_FileName FROM RPT_PIPE_ERO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbNewLine
        sql &= "UNION ALL" & vbNewLine
        sql &= "SELECT RPT_STEP," & EIR_BL.Report_Type.Pipe_Routine_Reports & " as RPT_Type_ID,Result_FileName FROM RPT_PIPE_ROUTINE_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbNewLine

        DA = New SqlDataAdapter(sql, BL.ConnStr)
        DA.Fill(DT)
        Dim RPT_Type_ID As EIR_BL.Report_Type = DT.Rows(0)("RPT_Type_ID")

        '----------------- If Report Posted-----------------
        If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) And Not IsDBNull(DT.Rows(0).Item("Result_FileName")) Then

            Dim FileName As String = BL.PostedReport_Path & "\" & DT.Rows(0).Item("Result_FileName")

            Select Case RPT_Type_ID
                Case EIR_BL.Report_Type.Pipe_CUI_Reports,
                     EIR_BL.Report_Type.Pipe_ERO_Reports
                    If DT.Rows(0).Item("RPT_STEP") > 2 And System.IO.File.Exists(FileName) Then
                        Return FileName
                    End If
                Case Else
                    If DT.Rows(0).Item("RPT_STEP") = 4 And System.IO.File.Exists(FileName) Then
                        Return FileName
                    End If
            End Select
        End If

        '---------------- Generate Report---------------
        Select Case RPT_Type_ID
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                Return Create_Stationary_Routine_Report(RPT_Year, RPT_No)
            Case EIR_BL.Report_Type.Stationary_Off_Routine_Report
                Return Create_Stationary_OffRoutine_Report(RPT_Year, RPT_No)
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                Return Create_Rotating_Routine_Report(RPT_Year, RPT_No)
            Case EIR_BL.Report_Type.Lube_Oil_Report
                If Detail_ID <> 0 Then
                    Dim Result As String = Create_Lube_Oil_Report_Tag(RPT_Year, RPT_No, Detail_ID)
                    'Dim S As String() = Split(Result, "_")
                    'If S.Length = 6 Then
                    '    Result = S(1) & "_" & S(2) & "_" & S(3) & "_" & S(4).Replace(".pdf.PDF", ".PDF")
                    'End If
                    Return Result
                Else
                    Return Create_Lube_Oil_Report(RPT_Year, RPT_No)
                End If
            Case EIR_BL.Report_Type.PdMA_Report, EIR_BL.Report_Type.MTap_Report
                If Detail_ID <> 0 Then
                    Return Create_PdMA_Report(RPT_Year, RPT_No, Detail_ID, RPT_Type_ID)
                Else
                    Return Create_PdMA_Report(RPT_Year, RPT_No, RPT_Type_ID)
                End If
            Case EIR_BL.Report_Type.Thermography_Report
                Return Create_THM_Report(RPT_Year, RPT_No)
            Case EIR_BL.Report_Type.Spring_Hanger_Report
                Return Create_Spring_Hanger_Report(RPT_Year, RPT_No)
            Case EIR_BL.Report_Type.Pipe_CUI_Reports
                Return Create_PIPE_CUI_Report(RPT_Year, RPT_No, Detail_ID)
            Case EIR_BL.Report_Type.Pipe_ERO_Reports
                Return Create_PIPE_ERO_Report(RPT_Year, RPT_No, Detail_ID)
            Case EIR_BL.Report_Type.Pipe_Routine_Reports
                Return Create_PIPE_ROUTINE_Report(RPT_Year, RPT_No)
        End Select
        Return ""
    End Function

    Public Function Create_Stationary_Routine_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As String
        '------------------- Initiate UNIQUE_ID -------------------------
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        '----------------------------------------------------------------
        If RPT_Year = 0 Or RPT_No = 0 Then Return False

        Dim FolderName As String = "Stationary_Routine_Report"

        Dim PDFList As String() = {}

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT_VW_REPORT_ST_HEADER As New DataTable
        Dim DT_VW_REPORT_ST_DETAIL As New DataTable

        SQL = "select * from VW_REPORT_ST_HEADER where RPT_Year = " & RPT_Year & " and RPT_No = " & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT_VW_REPORT_ST_HEADER)
        SQL = "select * from VW_REPORT_ST_DETAIL where RPT_Year = " & RPT_Year & " and RPT_No = " & RPT_No
        SQL &= "order by ISSUE,TAG_CODE,INSP_ID,DETAIL_ID"

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT_VW_REPORT_ST_DETAIL)

        Dim ReportName() As String = {"1_ST_INSPECTION", "2_ROUTINE INSPECTION", "3_TRACE SLOVE", "5_ST_PHOTO"}

        For i As Integer = 0 To ReportName.Length - 1

            Dim cc As New ReportDocument()
            Dim TempName As String = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & ReportName(i) & ".pdf"
            cc.Load(ServerMapPath & "\Report\" & FolderName & "\" & ReportName(i) & ".rpt")
            cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)

            Select Case ReportName(i).ToUpper
                Case "1_ST_INSPECTION"
                    SQL = "select SUM(rec) as Tag" & vbNewLine
                    SQL &= " ,SUM(NewIssue) as NewIssue" & vbNewLine
                    SQL &= " ,SUM(Issue) as Issue" & vbNewLine
                    SQL &= " ,SUM(Success) as Success" & vbNewLine
                    SQL &= " ,SUM(NonSuccess) as NonSuccess" & vbNewLine
                    SQL &= " ,SUM(PicNewIssue) as PicNewIssue" & vbNewLine
                    SQL &= " ,SUM(PicIssue) as PicIssue" & vbNewLine
                    SQL &= " ,SUM(PicSuccess) as PicSuccess" & vbNewLine
                    SQL &= " ,SUM(PicNonSuccess) as PicNonSuccess" & vbNewLine
                    SQL &= " from (" & vbNewLine
                    SQL &= " select 1 as Rec" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 1 THEN 1 ELSE 0 END) AS NewIssue" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 2 THEN 1 ELSE 0 END) AS Issue" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 3 THEN 1 ELSE 0 END) AS Success" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 4 THEN 1 ELSE 0 END) AS NonSuccess" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 1 THEN CASE WHEN CURRENT_PICTURE1 = 1 THEN 1 ELSE 0 END " & vbNewLine
                    SQL &= " + CASE WHEN CURRENT_PICTURE2 = 1 THEN 1 ELSE 0 END ELSE 0 END) as PicNewIssue" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 2 THEN CASE WHEN CURRENT_PICTURE1 = 1 THEN 1 ELSE 0 END " & vbNewLine
                    SQL &= " + CASE WHEN CURRENT_PICTURE2 = 1 THEN 1 ELSE 0 END ELSE 0 END) as PicIssue" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 3 THEN CASE WHEN CURRENT_PICTURE1 = 1 THEN 1 ELSE 0 END " & vbNewLine
                    SQL &= " + CASE WHEN CURRENT_PICTURE2 = 1 THEN 1 ELSE 0 END ELSE 0 END) as PicSuccess" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 4 THEN CASE WHEN CURRENT_PICTURE1 = 1 THEN 1 ELSE 0 END " & vbNewLine
                    SQL &= " + CASE WHEN CURRENT_PICTURE2 = 1 THEN 1 ELSE 0 END ELSE 0 END) as PicNonSuccess" & vbNewLine
                    SQL &= " from VW_REPORT_ST_DETAIL where RPT_Year = " & RPT_Year & " and RPT_No = " & RPT_No & vbNewLine
                    SQL &= " GROUP BY TAG_Code" & vbNewLine
                    SQL &= " ) as ALL_TAG" & vbNewLine

                    Dim DT As New DataTable
                    DA = New SqlDataAdapter(SQL, BL.ConnStr)
                    DA.Fill(DT)

                    cc.SetDataSource(DT_VW_REPORT_ST_HEADER)

                    cc.DataDefinition.FormulaFields("NewIssue").Text = DT.Rows(0).Item("NewIssue").ToString
                    cc.DataDefinition.FormulaFields("Issue").Text = DT.Rows(0).Item("Issue").ToString
                    cc.DataDefinition.FormulaFields("Success").Text = DT.Rows(0).Item("Success").ToString
                    cc.DataDefinition.FormulaFields("NonSuccess").Text = DT.Rows(0).Item("NonSuccess").ToString
                    cc.DataDefinition.FormulaFields("PicNewIssue").Text = DT.Rows(0).Item("PicNewIssue").ToString
                    cc.DataDefinition.FormulaFields("PicIssue").Text = DT.Rows(0).Item("PicIssue").ToString
                    cc.DataDefinition.FormulaFields("PicSuccess").Text = DT.Rows(0).Item("PicSuccess").ToString
                    cc.DataDefinition.FormulaFields("PicNonSuccess").Text = DT.Rows(0).Item("PicNonSuccess").ToString
                    cc.DataDefinition.FormulaFields("Tag").Text = DT.Rows(0).Item("Tag").ToString

                    DT = New DataTable
                    DT_VW_REPORT_ST_DETAIL.DefaultView.RowFilter = "ISSUE = 1"
                    DT = DT_VW_REPORT_ST_DETAIL.DefaultView.ToTable
                    DT_VW_REPORT_ST_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("New Issue").SetDataSource(DT)

                    DT = New DataTable
                    DT_VW_REPORT_ST_DETAIL.DefaultView.RowFilter = "ISSUE = 2"
                    DT = DT_VW_REPORT_ST_DETAIL.DefaultView.ToTable
                    DT_VW_REPORT_ST_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("Non Modify").SetDataSource(DT)

                    DT = New DataTable
                    DT_VW_REPORT_ST_DETAIL.DefaultView.RowFilter = "ISSUE = 3"
                    DT = DT_VW_REPORT_ST_DETAIL.DefaultView.ToTable
                    DT_VW_REPORT_ST_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("Success").SetDataSource(DT)

                    DT = New DataTable
                    DT_VW_REPORT_ST_DETAIL.DefaultView.RowFilter = "ISSUE = 4"
                    DT = DT_VW_REPORT_ST_DETAIL.DefaultView.ToTable
                    DT_VW_REPORT_ST_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("Non Success").SetDataSource(DT)
                    cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc.Close()
                    cc.Dispose()

                    BL.PushString(PDFList, TempName)
                Case "2_ROUTINE INSPECTION"
                    Dim DT As DataTable = JoinDataTable(DT_VW_REPORT_ST_HEADER, DT_VW_REPORT_ST_DETAIL, "RPT_No", "RPT_No")
                    cc.SetDataSource(DT)
                    cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc.Close()
                    cc.Dispose()

                    BL.PushString(PDFList, TempName)
                Case "3_TRACE SLOVE"
                    Dim DT As DataTable = JoinDataTable(DT_VW_REPORT_ST_HEADER, DT_VW_REPORT_ST_DETAIL, "RPT_No", "RPT_No")
                    DT.DefaultView.RowFilter = "CURRENT_LEVEL is not null or LAST_LEVEL is not null"
                    Dim DT_FILTER As DataTable = DT.DefaultView.ToTable
                    DT.DefaultView.RowFilter = ""
                    cc.SetDataSource(DT_FILTER)
                    cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc.Close()
                    cc.Dispose()

                    BL.PushString(PDFList, TempName)
                Case "5_ST_PHOTO"
                    SQL = " SELECT VW_REPORT_ST_HEADER.RPT_Year, VW_REPORT_ST_HEADER.RPT_No," & vbNewLine
                    SQL &= " PLANT_Code,ROUTE_Code, VW_REPORT_ST_HEADER.RPT_CODE," & vbNewLine
                    SQL &= " Officer_Collector, Created_Time,TAG_ID,TAG_CODE,INSP_ID," & vbNewLine
                    SQL &= " PROBLEM_DETAIL, PROBLEM_RECOMMENT, ISSUE, LEVEL_DESC, PICTURE1, PICTURE2," & vbNewLine
                    SQL &= " CASE WHEN ISNULL(CURRENT_PICTURE1,0) = 0 THEN" & vbNewLine
                    SQL &= "    CASE WHEN ISNULL(LAST_PICTURE1,0) = 1 THEN" & vbNewLine
                    SQL &= "        LAST_DETAIL_ID" & vbNewLine
                    SQL &= "    ELSE" & vbNewLine
                    SQL &= "        NULL" & vbNewLine
                    SQL &= "    END" & vbNewLine
                    SQL &= " ELSE" & vbNewLine
                    SQL &= "    DETAIL_ID" & vbNewLine
                    SQL &= " END as P1," & vbNewLine
                    SQL &= " CASE WHEN ISNULL(CURRENT_PICTURE2,0) = 0 THEN" & vbNewLine
                    SQL &= "    CASE WHEN ISNULL(LAST_PICTURE2,0) = 1 THEN" & vbNewLine
                    SQL &= "        LAST_DETAIL_ID" & vbNewLine
                    SQL &= "    ELSE" & vbNewLine
                    SQL &= "        NULL" & vbNewLine
                    SQL &= "    END" & vbNewLine
                    SQL &= " ELSE" & vbNewLine
                    SQL &= "    DETAIL_ID" & vbNewLine
                    SQL &= " END as P2" & vbNewLine
                    SQL &= " FROM   VW_REPORT_ST_HEADER LEFT JOIN VW_REPORT_ST_DETAIL" & vbNewLine
                    SQL &= " on VW_REPORT_ST_HEADER.RPT_Year = VW_REPORT_ST_DETAIL.RPT_Year" & vbNewLine
                    SQL &= " and VW_REPORT_ST_HEADER.RPT_No = VW_REPORT_ST_DETAIL.RPT_No" & vbNewLine
                    SQL &= " WHERE VW_REPORT_ST_HEADER.RPT_Year = " & RPT_Year & vbNewLine
                    SQL &= " And VW_REPORT_ST_HEADER.RPT_No = " & RPT_No & vbNewLine
                    SQL &= " AND (Last_Level IS NOT NULL OR CURRENT_LEVEL IS NOT NULL)" & vbNewLine
                    SQL &= " and ISSUE <> 0 and HasPICTURE = 1" & vbNewLine
                    SQL &= " and INSP_ID not in (select INSP_ID from VW_Not_Require_Inspection_Picture)"
                    SQL &= "order by ISSUE,TAG_CODE,INSP_ID,DETAIL_ID"

                    Dim DT As New DataTable
                    DA = New SqlDataAdapter(SQL, BL.ConnStr)
                    DA.Fill(DT)

                    If DT.Rows.Count > 0 Then
                        For x As Integer = 0 To DT.Rows.Count - 1
                            If DT.Rows(x).Item("P1").ToString <> "" Then
                                Dim Image As Byte() = BL.Get_ST_Image(RPT_Year, RPT_No, DT.Rows(x).Item("P1"), 1)
                                If Image IsNot Nothing Then
                                    DT.Rows(x).Item("PICTURE1") = ResizeImage(Image)
                                End If
                            End If
                            If DT.Rows(x).Item("P2").ToString <> "" Then
                                Dim Image As Byte() = BL.Get_ST_Image(RPT_Year, RPT_No, DT.Rows(x).Item("P2"), 2)
                                If Image IsNot Nothing Then
                                    DT.Rows(x).Item("PICTURE2") = ResizeImage(Image)
                                End If
                            End If
                        Next

                        cc.SetDataSource(DT)
                        cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                        cc.Close()
                        cc.Dispose()
                        BL.PushString(PDFList, TempName)
                    End If

            End Select



        Next

        Dim OutputFile As String = UNIQUE_ID & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "_" & RPT_No.ToString.PadLeft(4, "0") & ".pdf"
        BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
        Return ServerMapPath & "\Temp\" & OutputFile

    End Function

    Public Function Create_Stationary_OffRoutine_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As String

        '------------------- Initiate UNIQUE_ID -------------------------
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        '----------------------------------------------------------------
        If RPT_Year = 0 Or RPT_No = 0 Then Return False

        Dim PDFList As String() = {}

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT_REPORT_HEADER As New DataTable
        Dim DT_REPORT_DETAIL As New DataTable

        SQL = " SELECT RPT_CODE,RPT_Year,RPT_No,PLANT_ID,PLANT_Code,AREA_Code,Tag_Code,Tag_Name,RPT_Period_Start,RPT_Period_End,RPT_STEP,STEP_Name" & vbLf
        SQL &= " ,Created_By,Created_Time,Creator_Name,Creator_Level" & vbLf
        SQL &= " ,RPT_Subject,RPT_To,RPT_Cause,RPT_Result,RPT_By,Reporter_Name,Reporter_Level" & vbLf
        SQL &= " ,RPT_COL_By,Collector_Name,Collector_Level" & vbLf
        SQL &= " ,RPT_INSP_By,Inspector_Name,Inspector_Level" & vbLf
        SQL &= " ,RPT_ANL_By,Analyst_Name,Analyst_Level" & vbLf
        SQL &= " ,Conclusion_Recomment" & vbLf
        SQL &= " ,RPT_LOCK_BY,Lock_Name,Lock_Level,Update_Time" & vbLf
        SQL &= " ,Officer_Collector,Officer_Inspector,Inspector_Position,Officer_Engineer,Officer_Analyst" & vbLf
        SQL &= " FROM VW_REPORT_ST_OffRoutine_HEADER" & vbLf
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbLf
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT_REPORT_HEADER)

        SQL = "select * from VW_REPORT_ST_OffRoutine_DETAIL where RPT_Year = " & RPT_Year & " and RPT_No = " & RPT_No
        SQL &= "order by DETAIL_ID"
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT_REPORT_DETAIL)

        Dim ReportName() As String = {"1_COVER", "2_Problem"}
        For i As Integer = 0 To ReportName.Length - 1
            Dim cc As New ReportDocument()
            Dim TempName As String = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & ReportName(i) & ".pdf"
            cc.Load(ServerMapPath & "\Report\Stationary_Off_Routine_Report\" & ReportName(i) & ".rpt")
            cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)

            Select Case ReportName(i).ToUpper
                Case "1_COVER"
                    SQL = " SELECT SUM(CASE WHEN Create_Flag = 'New' THEN 1 ELSE 0 END) NewIssue" & vbLf
                    SQL &= " ,SUM(Case When CURRENT_Fixed=0 Then 1 Else 0 End) Issue" & vbLf
                    SQL &= " ,SUM(CASE WHEN CURRENT_Fixed=1 AND CURRENT_LEVEL=0 THEN 1 ELSE 0 END) Success" & vbLf
                    SQL &= " ,SUM(Case When CURRENT_Fixed=1 And CURRENT_LEVEL>0 Then 1 Else 0 End) NonSuccess" & vbLf
                    SQL &= " from VW_REPORT_ST_OffRoutine_DETAIL" & vbLf
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                    Dim DT As New DataTable
                    DA = New SqlDataAdapter(SQL, BL.ConnStr)
                    DA.Fill(DT)

                    cc.SetDataSource(DT_REPORT_HEADER)

                    cc.DataDefinition.FormulaFields("NewIssue").Text = DT.Rows(0).Item("NewIssue").ToString
                    cc.DataDefinition.FormulaFields("Issue").Text = DT.Rows(0).Item("Issue").ToString
                    cc.DataDefinition.FormulaFields("Success").Text = DT.Rows(0).Item("Success").ToString
                    cc.DataDefinition.FormulaFields("NonSuccess").Text = DT.Rows(0).Item("NonSuccess").ToString

                    DT = New DataTable
                    DT_REPORT_DETAIL.DefaultView.RowFilter = "Create_Flag = 'New'"
                    DT = DT_REPORT_DETAIL.DefaultView.ToTable
                    cc.Subreports("New Issue").SetDataSource(DT)
                    DT_REPORT_DETAIL.DefaultView.RowFilter = ""

                    DT = New DataTable
                    DT_REPORT_DETAIL.DefaultView.RowFilter = "ISSUE = 2"
                    DT = DT_REPORT_DETAIL.DefaultView.ToTable
                    cc.Subreports("Non Modify").SetDataSource(DT)
                    DT_REPORT_DETAIL.DefaultView.RowFilter = ""

                    DT = New DataTable
                    DT_REPORT_DETAIL.DefaultView.RowFilter = "CURRENT_Fixed=1 AND CURRENT_LEVEL=0"
                    DT = DT_REPORT_DETAIL.DefaultView.ToTable
                    cc.Subreports("Success").SetDataSource(DT)
                    DT_REPORT_DETAIL.DefaultView.RowFilter = ""

                    DT = New DataTable
                    DT_REPORT_DETAIL.DefaultView.RowFilter = "CURRENT_Fixed=1 AND CURRENT_LEVEL>0"
                    DT = DT_REPORT_DETAIL.DefaultView.ToTable
                    cc.Subreports("Non Success").SetDataSource(DT)
                    DT_REPORT_DETAIL.DefaultView.RowFilter = ""

                    cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc.Close()
                    cc.Dispose()

                    BL.PushString(PDFList, TempName)
                Case "2_PROBLEM"

                    SQL = " SELECT Detail.DETAIL_ID," & vbLf
                    SQL &= " Header.RPT_Year,Header.RPT_No,PLANT_Code,AREA_Code,Header.RPT_CODE,Header.TAG_CODE,INSP_Name,CURRENT_STATUS_Name,CURRENT_STATUS_ID" & vbLf
                    SQL &= " ,LAST_LEVEL,LAST_LEVEL_Desc,CURRENT_LEVEL,CURRENT_LEVEL_Desc,CURRENT_Fixed,INSP_ID,PROBLEM_DETAIL,PROBLEM_RECOMMENT" & vbLf
                    SQL &= " ,CASE WHEN Create_Flag = 'New' THEN 1" & vbLf
                    SQL &= " 	WHEN CURRENT_Fixed=0 THEN 2" & vbLf
                    SQL &= " 	WHEN CURRENT_Fixed=1 AND CURRENT_LEVEL=0 THEN 3" & vbLf
                    SQL &= " 	WHEN CURRENT_Fixed=1 AND CURRENT_LEVEL>0 THEN 4" & vbLf
                    SQL &= " END Issue" & vbLf
                    SQL &= " ,CASE WHEN Create_Flag = 'New' THEN 'ปัญหาใหม่' " & vbLf
                    SQL &= " 	WHEN CURRENT_Fixed=0 THEN 'ปัญหาที่ยังไม่ได้รับการแก้ไข' " & vbLf
                    SQL &= " 	WHEN CURRENT_Fixed=1 AND CURRENT_LEVEL=0 THEN 'ปัญหาที่แก้ไขเสร็จสมบุูรณ์'" & vbLf
                    SQL &= " 	WHEN CURRENT_Fixed=1 AND CURRENT_LEVEL>0 THEN 'ปัญหายังแก้ไขไม่สมบุูรณ์'" & vbLf
                    SQL &= " END IssueText" & vbLf
                    SQL &= " ,Created_Time,Officer_Collector, CAST(NULL AS Image) PICTURE1,CAST(NULL AS Image) PICTURE2" & vbLf
                    SQL &= " ,SECTOR.Sector_ID,Template_ID," & vbLf
                    SQL &= "  CAST(NULL AS VARCHAR(4000)) Text_1,CAST(NULL AS Image) PIC_Detail1," & vbLf
                    SQL &= "  CAST(NULL AS VARCHAR(4000)) Text_2,CAST(NULL AS Image) PIC_Detail2," & vbLf
                    SQL &= "  CAST(NULL AS VARCHAR(4000)) Text_3,CAST(NULL AS Image) PIC_Detail3," & vbLf
                    SQL &= "  CAST(NULL AS VARCHAR(4000)) Text_4,CAST(NULL AS Image) PIC_Detail4" & vbLf
                    SQL &= " " & vbLf
                    SQL &= " from VW_REPORT_ST_OffRoutine_DETAIL Detail" & vbLf
                    SQL &= " INNER JOIN VW_REPORT_ST_OffRoutine_HEADER Header ON Header.RPT_Year=Detail.RPT_Year AND Header.RPT_No=Detail.RPT_No" & vbLf
                    SQL &= " LEFT JOIN RPT_Template_Sector SECTOR ON Detail.DETAIL_ID=SECTOR.DETAIL_ID" & vbLf
                    SQL &= " " & vbLf
                    SQL &= " WHERE Header.RPT_Year=" & RPT_Year & " And Header.RPT_No=" & RPT_No & vbLf
                    SQL &= " ORDER BY Detail.DETAIL_ID,Issue,Sector_ID" & vbLf

                    Dim DT As New DataTable
                    DA = New SqlDataAdapter(SQL, BL.ConnStr)
                    DA.Fill(DT)

                    If DT.Rows.Count = 0 Then Continue For
                    '---------- Load Image ---------
                    Dim SVG As New SVG_API
                    Dim C As New Converter
                    For d As Integer = 0 To DT.Rows.Count - 1
                        '-------- For Inspection Detail -------
                        For j As Integer = 1 To 2
                            Dim IMG As Object = BL.Get_ST_Image(DT.Rows(d).Item("DETAIL_ID"), j)
                            If Not IsNothing(IMG) Then DT.Rows(d).Item("PICTURE" & j) = ResizeImage(IMG)
                        Next
                        '-------- For Template Sector -------
                        If Not IsDBNull(DT.Rows(d).Item("Sector_ID")) Then
                            Dim Sector_ID As Integer = DT.Rows(d).Item("Sector_ID")
                            For File_ID As Integer = 1 To 4
                                Dim Attachmant As FileAttachment = BL.Get_Stationary_Template_File(Sector_ID, File_ID)

                                If IsNothing(Attachmant) Then
                                    Continue For
                                End If

                                '-------- Set Default RichText Font ----------
                                Dim FileDesc As String = "<font face='Arial Unicode MS'>" & Attachmant.Description & "</font>"
                                FileDesc = FileDesc.Replace("<br />", "<br>").Replace("<br/>", "<br>")


                                Select Case Attachmant.Extension
                                    Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG,
                                        FileAttachment.ExtensionType.PNG, FileAttachment.ExtensionType.TIFF
                                        DT.Rows(d).Item("PIC_Detail" & File_ID) = Attachmant.Content
                                        DT.Rows(d).Item("Text_" & File_ID) = FileDesc
                                    Case FileAttachment.ExtensionType.SVG
                                        DT.Rows(d).Item("PIC_Detail" & File_ID) = C.ImageToByte(SVG.SVGToImage(C.ByteToString(Attachmant.Content, Converter.EncodeType._UTF8)))
                                        DT.Rows(d).Item("Text_" & File_ID) = FileDesc
                                    Case FileAttachment.ExtensionType.DOC, FileAttachment.ExtensionType.PDF,
                                        FileAttachment.ExtensionType.PPT, FileAttachment.ExtensionType.XLS
                                End Select

                            Next
                        End If
                    Next

                    TempName = ServerMapPath & "\Temp\" & GenerateNewUniqueID() & "_" & ReportName(i) & ".pdf"
                    cc.SetDataSource(DT)
                    cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc.Close()
                    cc.Dispose()

                    BL.PushString(PDFList, TempName)
            End Select
        Next

        Dim OutputFile As String = UNIQUE_ID & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "_" & RPT_No.ToString.PadLeft(4, "0") & ".pdf"
        'BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile.Replace(".pdf", "").Replace(".PDF", "") & ".pdf")   '--แก้ไข Off route ไม่มี file type ต่อท้ายบน server
        BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
        Return ServerMapPath & "\Temp\" & OutputFile

    End Function

    Public Function Create_Rotating_Routine_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As String
        '------------------- Initiate UNIQUE_ID -------------------------
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        '----------------------------------------------------------------
        If RPT_Year = 0 Or RPT_No = 0 Then Return False

        Dim FolderName As String = "Rotating_Routine_Report"


        Dim PDFList As String() = {}
        Dim ReportName() As String = {"1_RO_INSPECTION", "2_VISUAL INSPECTION_MP", "2_VISUAL INSPECTION_FF", "3_VIBRATION ANALYSIS", "4_RO_PHOTO"}

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT_VW_REPORT_RO_HEADER As New DataTable
        Dim DT_VW_REPORT_RO_DETAIL As New DataTable
        Dim DT_VW_RO_IMPORT As New DataTable

        SQL = "Select * from VW_REPORT_RO_HEADER where RPT_Year = " & RPT_Year & " And RPT_No = " & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT_VW_REPORT_RO_HEADER)
        SQL = "Select * from VW_REPORT_RO_DETAIL where RPT_Year = " & RPT_Year & " And RPT_No = " & RPT_No
        SQL &= "order by ISSUE,TAG_CODE,INSP_ID,DETAIL_ID"
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT_VW_REPORT_RO_DETAIL)
        SQL = "Select * from RPT_Rotating_Routine_Import where RPT_Year = " & RPT_Year & " And RPT_No = " & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT_VW_RO_IMPORT)

        For i As Integer = 0 To ReportName.Length - 1

            Dim cc As New ReportDocument()
            Dim TempName As String = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & ReportName(i) & ".pdf"
            cc.Load(ServerMapPath & "\Report\" & FolderName & "\" & ReportName(i) & ".rpt")
            cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)

            Select Case ReportName(i).ToUpper
                Case "1_RO_INSPECTION"
                    SQL = "Select SUM(rec) As Tag" & vbNewLine
                    SQL &= " ,SUM(Case When Value > 0 And Machine > 0 Then 1 Else 0 End) As AbNormal" & vbNewLine
                    SQL &= " ,SUM(NewIssue) As NewIssue" & vbNewLine
                    SQL &= " ,SUM(Issue) As Issue" & vbNewLine
                    SQL &= " ,SUM(Success) As Success" & vbNewLine
                    SQL &= " ,SUM(NonSuccess) As NonSuccess" & vbNewLine
                    SQL &= " ,SUM(PicNewIssue) As PicNewIssue" & vbNewLine
                    SQL &= " ,SUM(PicIssue) As PicIssue" & vbNewLine
                    SQL &= " ,SUM(PicSuccess) As PicSuccess" & vbNewLine
                    SQL &= " ,SUM(PicNonSuccess) As PicNonSuccess" & vbNewLine
                    SQL &= " from (" & vbNewLine
                    SQL &= " Select 1 As Rec" & vbNewLine
                    SQL &= " ,SUM(Case When INSP_ID  = 12 And CURRENT_LEVEL > 1  Then 1 Else 0 End) +"
                    SQL &= " SUM(Case When INSP_ID In (13,14,15,16,19,25,26,27,28,29,30,31) And CURRENT_LEVEL > 0 Then 1 Else 0 End) As Value"
                    SQL &= " ,SUM(Case When INSP_ID  = 11 And CURRENT_STATUS_ID = 9 Then 1 Else 0 End) As Machine"
                    SQL &= " ,SUM(Case When ISSUE = 1 Then 1 Else 0 End) As NewIssue" & vbNewLine
                    SQL &= " ,SUM(Case When ISSUE = 2 Then 1 Else 0 End) As Issue" & vbNewLine
                    SQL &= " ,SUM(Case When ISSUE = 3 Then 1 Else 0 End) As Success" & vbNewLine
                    SQL &= " ,SUM(Case When ISSUE = 4 Then 1 Else 0 End) As NonSuccess" & vbNewLine
                    SQL &= " ,SUM(Case When ISSUE = 1 Then Case When CURRENT_PICTURE1 = 1 Then 1 Else 0 End " & vbNewLine
                    SQL &= " + Case When CURRENT_PICTURE2 = 1 Then 1 Else 0 End Else 0 End) As PicNewIssue" & vbNewLine
                    SQL &= " ,SUM(Case When ISSUE = 2 Then Case When CURRENT_PICTURE1 = 1 Then 1 Else 0 End " & vbNewLine
                    SQL &= " + Case When CURRENT_PICTURE2 = 1 Then 1 Else 0 End Else 0 End) As PicIssue" & vbNewLine
                    SQL &= " ,SUM(Case When ISSUE = 3 Then Case When CURRENT_PICTURE1 = 1 Then 1 Else 0 End " & vbNewLine
                    SQL &= " + Case When CURRENT_PICTURE2 = 1 Then 1 Else 0 End Else 0 End) As PicSuccess" & vbNewLine
                    SQL &= " ,SUM(Case When ISSUE = 4 Then Case When CURRENT_PICTURE1 = 1 Then 1 Else 0 End " & vbNewLine
                    SQL &= " + Case When CURRENT_PICTURE2 = 1 Then 1 Else 0 End Else 0 End) As PicNonSuccess" & vbNewLine
                    SQL &= " from VW_REPORT_RO_DETAIL where RPT_Year = " & RPT_Year & " And RPT_No = " & RPT_No & vbNewLine
                    SQL &= " GROUP BY TAG_Code" & vbNewLine
                    SQL &= " ) As ALL_TAG" & vbNewLine

                    Dim DT As New DataTable
                    DA = New SqlDataAdapter(SQL, BL.ConnStr)
                    DA.Fill(DT)

                    cc.SetDataSource(DT_VW_REPORT_RO_HEADER)

                    cc.DataDefinition.FormulaFields("NewIssue").Text = DT.Rows(0).Item("NewIssue").ToString
                    cc.DataDefinition.FormulaFields("Issue").Text = DT.Rows(0).Item("Issue").ToString
                    cc.DataDefinition.FormulaFields("Success").Text = DT.Rows(0).Item("Success").ToString
                    cc.DataDefinition.FormulaFields("NonSuccess").Text = DT.Rows(0).Item("NonSuccess").ToString
                    cc.DataDefinition.FormulaFields("PicNewIssue").Text = DT.Rows(0).Item("PicNewIssue").ToString
                    cc.DataDefinition.FormulaFields("PicIssue").Text = DT.Rows(0).Item("PicIssue").ToString
                    cc.DataDefinition.FormulaFields("PicSuccess").Text = DT.Rows(0).Item("PicSuccess").ToString
                    cc.DataDefinition.FormulaFields("PicNonSuccess").Text = DT.Rows(0).Item("PicNonSuccess").ToString
                    cc.DataDefinition.FormulaFields("Tag").Text = DT.Rows(0).Item("Tag").ToString
                    cc.DataDefinition.FormulaFields("Abnormal").Text = DT.Rows(0).Item("AbNormal").ToString
                    cc.DataDefinition.FormulaFields("Normal").Text = CInt(DT.Rows(0).Item("Tag").ToString) - CInt(DT.Rows(0).Item("AbNormal").ToString)

                    DT = New DataTable
                    DT_VW_REPORT_RO_DETAIL.DefaultView.RowFilter = "ISSUE = 1"
                    DT = DT_VW_REPORT_RO_DETAIL.DefaultView.ToTable
                    DT_VW_REPORT_RO_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("New Issue").SetDataSource(DT)

                    DT = New DataTable
                    DT_VW_REPORT_RO_DETAIL.DefaultView.RowFilter = "ISSUE = 2"
                    DT = DT_VW_REPORT_RO_DETAIL.DefaultView.ToTable
                    DT_VW_REPORT_RO_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("Non Modify").SetDataSource(DT)

                    DT = New DataTable
                    DT_VW_REPORT_RO_DETAIL.DefaultView.RowFilter = "ISSUE = 3"
                    DT = DT_VW_REPORT_RO_DETAIL.DefaultView.ToTable
                    DT_VW_REPORT_RO_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("Success").SetDataSource(DT)

                    DT = New DataTable
                    DT_VW_REPORT_RO_DETAIL.DefaultView.RowFilter = "ISSUE = 4"
                    DT = DT_VW_REPORT_RO_DETAIL.DefaultView.ToTable
                    DT_VW_REPORT_RO_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("Non Success").SetDataSource(DT)
                    cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc.Close()
                    cc.Dispose()
                    BL.PushString(PDFList, TempName)
                Case "2_VISUAL INSPECTION_MP"
                    Dim DT_TEMP As DataTable = JoinDataTable(DT_VW_REPORT_RO_HEADER, DT_VW_REPORT_RO_DETAIL, "RPT_No", "RPT_No")
                    DT_TEMP.DefaultView.RowFilter = "TAG_TYPE_ID <> 9"
                    Dim DT As DataTable = DT_TEMP.DefaultView.ToTable
                    If DT.Rows.Count > 0 Then
                        cc.SetDataSource(DT)
                        cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                        cc.Close()
                        cc.Dispose()
                        BL.PushString(PDFList, TempName)
                    End If
                    DT_TEMP.DefaultView.RowFilter = ""
                Case "2_VISUAL INSPECTION_FF"
                    Dim DT_TEMP As DataTable = JoinDataTable(DT_VW_REPORT_RO_HEADER, DT_VW_REPORT_RO_DETAIL, "RPT_No", "RPT_No")
                    DT_TEMP.DefaultView.RowFilter = "TAG_TYPE_ID = 9"
                    Dim DT As DataTable = DT_TEMP.DefaultView.ToTable
                    If DT.Rows.Count > 0 Then
                        cc.SetDataSource(DT)
                        cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                        cc.Close()
                        cc.Dispose()
                        BL.PushString(PDFList, TempName)
                    End If
                    DT_TEMP.DefaultView.RowFilter = ""
                Case "3_VIBRATION ANALYSIS"
                    If DT_VW_RO_IMPORT.Rows.Count > 0 Then
                        SQL = "Select HD.RPT_Code ,HD.RPT_Year,HD.RPT_No,HD.PLANT_Code," & vbNewLine
                        SQL &= " HD.ROUTE_Code,HD.Created_Time,HD.Officer_Collector," & vbNewLine
                        SQL &= " Position,Vibration_Sererity,Overall_Value,Unit," & vbNewLine
                        SQL &= " Percent_Change,Analysis_Comment,PROB_Recomment," & vbNewLine
                        SQL &= " PROC_Code, AREA_Code, TAG_No, TAG_Name, Data_Type, Location" & vbNewLine
                        SQL &= " FROM VW_Report_RO_Header HD" & vbNewLine
                        SQL &= " LEFT JOIN RPT_Rotating_Routine_Import RPT " & vbNewLine
                        SQL &= " On HD.RPT_Year=RPT.RPT_Year And HD.RPT_No=RPT.RPT_No" & vbNewLine
                        SQL &= " LEFT JOIN MS_RO_TAG TAG On RPT.TAG_ID=TAG.TAG_ID" & vbNewLine
                        SQL &= " LEFT JOIN MS_Process [PROC] On [PROC].PROC_ID=TAG.PROC_ID" & vbNewLine
                        SQL &= " LEFT JOIN MS_Area Area On Area.Area_ID=TAG.Area_ID" & vbNewLine
                        SQL &= " where HD.RPT_Year = " & RPT_Year & " And HD.RPT_No = " & RPT_No & vbNewLine
                        SQL &= " ORDER BY RPT.Row_ID"

                        Dim DT As New DataTable
                        DA = New SqlDataAdapter(SQL, BL.ConnStr)
                        DA.Fill(DT)
                        cc.SetDataSource(DT)
                        cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                        cc.Close()
                        cc.Dispose()
                        BL.PushString(PDFList, TempName)
                    End If

                Case "4_RO_PHOTO"
                    SQL = " Select VW_REPORT_RO_HEADER.RPT_Year, VW_REPORT_RO_HEADER.RPT_No," & vbNewLine
                    SQL &= " PLANT_Code,ROUTE_Code, VW_REPORT_RO_HEADER.RPT_CODE," & vbNewLine
                    SQL &= " Officer_Collector, Created_Time,TAG_ID,TAG_CODE,INSP_ID," & vbNewLine
                    SQL &= " PROBLEM_DETAIL, PROBLEM_RECOMMENT, ISSUE, LEVEL_DESC, PICTURE1, PICTURE2," & vbNewLine
                    SQL &= " Case When ISNULL(CURRENT_PICTURE1,0) = 0 Then" & vbNewLine
                    SQL &= "    Case When ISNULL(LAST_PICTURE1,0) = 1 Then" & vbNewLine
                    SQL &= "        LAST_DETAIL_ID" & vbNewLine
                    SQL &= "    Else" & vbNewLine
                    SQL &= "        NULL" & vbNewLine
                    SQL &= "    End" & vbNewLine
                    SQL &= " Else" & vbNewLine
                    SQL &= "    DETAIL_ID" & vbNewLine
                    SQL &= " End As P1," & vbNewLine
                    SQL &= " Case When ISNULL(CURRENT_PICTURE2,0) = 0 Then" & vbNewLine
                    SQL &= "    Case When ISNULL(LAST_PICTURE2,0) = 1 Then" & vbNewLine
                    SQL &= "        LAST_DETAIL_ID" & vbNewLine
                    SQL &= "    Else" & vbNewLine
                    SQL &= "        NULL" & vbNewLine
                    SQL &= "    End" & vbNewLine
                    SQL &= " Else" & vbNewLine
                    SQL &= "    DETAIL_ID" & vbNewLine
                    SQL &= " End As P2" & vbNewLine
                    SQL &= " FROM   VW_REPORT_RO_HEADER LEFT JOIN VW_REPORT_RO_DETAIL" & vbNewLine
                    SQL &= " On VW_REPORT_RO_HEADER.RPT_Year = VW_REPORT_RO_DETAIL.RPT_Year" & vbNewLine
                    SQL &= " And VW_REPORT_RO_HEADER.RPT_No = VW_REPORT_RO_DETAIL.RPT_No" & vbNewLine
                    SQL &= " WHERE VW_REPORT_RO_HEADER.RPT_Year = " & RPT_Year & vbNewLine
                    SQL &= " And VW_REPORT_RO_HEADER.RPT_No = " & RPT_No & vbNewLine
                    SQL &= " And (Last_Level Is Not NULL Or CURRENT_LEVEL Is Not NULL)" & vbNewLine
                    SQL &= " And ISSUE <> 0 And HasPICTURE = 1" & vbNewLine
                    SQL &= " And INSP_ID Not In (Select INSP_ID from VW_Not_Require_Inspection_Picture)"
                    SQL &= "order by ISSUE,TAG_CODE,INSP_ID,DETAIL_ID"

                    Dim DT As New DataTable
                    DA = New SqlDataAdapter(SQL, BL.ConnStr)
                    DA.Fill(DT)

                    If DT.Rows.Count > 0 Then
                        For x As Integer = 0 To DT.Rows.Count - 1
                            If DT.Rows(x).Item("P1").ToString <> "" Then
                                Dim Image As Byte() = BL.Get_RO_Image(RPT_Year, RPT_No, DT.Rows(x).Item("P1"), 1)
                                If Image IsNot Nothing Then
                                    DT.Rows(x).Item("PICTURE1") = ResizeImage(Image)
                                End If
                            End If
                            If DT.Rows(x).Item("P2").ToString <> "" Then
                                Dim Image As Byte() = BL.Get_RO_Image(RPT_Year, RPT_No, DT.Rows(x).Item("P2"), 2)
                                If Image IsNot Nothing Then
                                    DT.Rows(x).Item("PICTURE2") = ResizeImage(Image)
                                End If
                            End If
                        Next
                        cc.SetDataSource(DT)
                        cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                        cc.Close()
                        cc.Dispose()
                        BL.PushString(PDFList, TempName)
                    End If

            End Select
        Next

        Dim OutputFile As String = UNIQUE_ID & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "_" & RPT_No.ToString.PadLeft(4, "0") & ".pdf"
        BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
        Return ServerMapPath & "\Temp\" & OutputFile

    End Function

    Public Function Create_Rotating_Off_Routine_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As String
        Return ""
    End Function

    Public Function Create_Spring_Hanger_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As String
        '------------------- Initiate UNIQUE_ID -------------------------
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        '----------------------------------------------------------------
        If RPT_Year = 0 Or RPT_No = 0 Then Return False
        Dim FolderName As String = "SpringHanger_Routine_Report"


        Dim PDFList As String() = {}
        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter

        Dim RPT_ID As Integer = BL.GetSpringReportID(RPT_Year, RPT_No)
        If RPT_ID = 0 Then Return ""

        For i As Integer = 1 To 3
            Dim cc As New ReportDocument()
            Dim ReportName As String = i
            Dim TempName As String = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & ReportName & ".pdf"
            cc.Load(ServerMapPath & "\Report\" & FolderName & "\" & ReportName & ".rpt")
            cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
            cc.SetParameterValue("@ReportId", RPT_ID)
            cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
            cc.Close()
            cc.Dispose()
            BL.PushString(PDFList, TempName) '--------------- Finish Header ------------
        Next
        Dim OutputFile As String = UNIQUE_ID & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "_" & RPT_No.ToString.PadLeft(4, "0") & ".pdf"
        BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
        Return ServerMapPath & "\Temp\" & OutputFile

    End Function

    Public Function Create_Lube_Oil_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As String
        '------------------- Initiate UNIQUE_ID -------------------------
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        '----------------------------------------------------------------
        If RPT_Year = 0 Or RPT_No = 0 Then Return False

        Dim FolderName As String = "Lube_Oil_Routine_Report"


        Dim PDFList As String() = {}

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim ReportName As String = "0_LO_INSPECTION_COVER"
        Dim TempName As String = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & ReportName & ".pdf"



        SQL = "Select HEADER.*,DETAIL.DETAIL_ID,DETAIL.LO_TAG_NO,LO_TAG_TYPE_Name,DETAIL.PLANT_Code," & vbLf
        SQL &= " DETAIL.TAN_Abnormal, DETAIL.OX_Abnormal, DETAIL.VANISH_Abnormal, DETAIL.Is_Abnormal" & vbLf
        SQL &= " FROM VW_REPORT_LO_HEADER HEADER " & vbLf
        SQL &= " LEFT JOIN VW_REPORT_LO_DETAIL DETAIL On HEADER.RPT_Year=DETAIL.RPT_Year And HEADER.RPT_No=DETAIL.RPT_No" & vbLf
        SQL &= " WHERE HEADER.RPT_Year = " & RPT_Year & " And HEADER.RPT_No = " & RPT_No & vbLf
        SQL &= " ORDER BY DETAIL.PLANT_ID,DETAIL.LO_TAG_NO" & vbLf

        Dim DT As New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        '-------------- Add Radar Image-------------
        DT.Columns.Add("Radar", GetType(Byte()))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim B As Byte() = Visual.Get_LubeOil_RadarImage(DT.Rows(i).Item("Detail_ID"))
            DT.Rows(i).Item("Radar") = B
        Next

        Dim cc As New ReportDocument()
        cc.Load(ServerMapPath & "\Report\" & FolderName & "\" & ReportName & ".rpt")
        cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
        cc.SetDataSource(DT)
        cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
        cc.Close()
        cc.Dispose()

        BL.PushString(PDFList, TempName) '--------------- Finish Header ------------
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Detail As String = Create_Lube_Oil_Report_Tag(RPT_Year, RPT_No, DT.Rows(i).Item("DETAIL_ID"))
            If Detail <> "" Then BL.PushString(PDFList, Detail)
        Next

        Dim OutputFile As String = UNIQUE_ID & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "_" & RPT_No.ToString.PadLeft(4, "0") & ".pdf"
        BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
        Return ServerMapPath & "\Temp\" & OutputFile

    End Function

    Public Function Create_Lube_Oil_Report_Tag(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal Detail_ID As Integer) As String

        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "") & "_" & Detail_ID

        Dim SQL As String = "Select VW_REPORT_LO_DETAIL.*,Officer_Collector" & vbLf
        SQL &= " FROM VW_REPORT_LO_DETAIL INNER JOIN RPT_LO_Header " & vbLf
        SQL &= " On VW_REPORT_LO_DETAIL.RPT_Year=RPT_LO_Header.RPT_Year And VW_REPORT_LO_DETAIL.RPT_No=RPT_LO_Header.RPT_No" & vbLf
        SQL &= " WHERE Detail_ID = " & Detail_ID & vbLf
        SQL &= " ORDER BY PLANT_ID,LO_TAG_NO" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Return ""
        '---------------- SUB REPORT ----------------

        Dim PARAMETER(4) As String
        PARAMETER(0) = "VANISH"
        PARAMETER(1) = "PART_COUNT"
        PARAMETER(2) = "TAN"
        PARAMETER(3) = "OX"
        PARAMETER(4) = "WATER"

        Dim DT_VARNISH As New DataTable
        Dim DT_PART As New DataTable
        Dim DT_TAN As New DataTable
        Dim DT_OX As New DataTable
        Dim DT_WATER As New DataTable

        For i As Int32 = 0 To PARAMETER.Length - 1
            SQL = ""
            SQL &= "Select REC.REC_NO,LO_TAG_ID," & PARAMETER(i) & "_Value," & PARAMETER(i) & "_Date FROM" & vbCrLf
            SQL &= "(" & vbCrLf
            SQL &= "Select 1 As REC_NO UNION ALL Select 2 As REC_NO UNION ALL Select 3 As REC_NO UNION ALL Select 4 As REC_NO UNION ALL Select 5 As REC_NO UNION ALL Select 6 As REC_NO" & vbCrLf
            SQL &= ") REC" & vbCrLf
            SQL &= "LEFT JOIN" & vbCrLf
            SQL &= "(" & vbCrLf
            'SQL &= "Select TOP 6 ROW_NUMBER() OVER (ORDER BY " & PARAMETER(i) & "_Date) REC_NO,LO_TAG_ID," & PARAMETER(i) & "_Value," & PARAMETER(i) & "_Date FROM RPT_LO_Detail WHERE LO_TAG_ID = (Select LO_TAG_ID FROM RPT_LO_Detail WHERE DETAIL_ID = " & Detail_ID & ") " & vbCrLf
            'SQL &= "And (" & PARAMETER(i) & "_Value Is Not NULL Or " & PARAMETER(i) & "_Date Is Not NULL)" & vbCrLf
            SQL &= "Select ROW_NUMBER() OVER (ORDER BY " & PARAMETER(i) & "_Date) REC_NO,* FROM" & vbCrLf
            SQL &= "(Select TOP 6 LO_TAG_ID," & PARAMETER(i) & "_Value," & PARAMETER(i) & "_Date FROM RPT_LO_Detail" & vbCrLf
            SQL &= "WHERE LO_TAG_ID = (Select LO_TAG_ID FROM RPT_LO_Detail WHERE DETAIL_ID = " & Detail_ID & ") And" & vbCrLf
            SQL &= "(" & PARAMETER(i) & "_Value Is Not NULL Or " & PARAMETER(i) & "_Date Is Not NULL) ORDER BY " & PARAMETER(i) & "_Date DESC) TB" & vbCrLf

            SQL &= ") DATA" & vbCrLf
            SQL &= "On REC.REC_NO = DATA.REC_NO" & vbCrLf
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            Select Case i
                Case 0
                    DA.Fill(DT_VARNISH)
                    DT_VARNISH.DefaultView.RowFilter = "LO_TAG_ID Is Not NULL"
                    DT_VARNISH = DT_VARNISH.DefaultView.ToTable
                Case 1
                    DA.Fill(DT_PART)
                    DT_PART.DefaultView.RowFilter = "LO_TAG_ID Is Not NULL"
                    DT_PART = DT_PART.DefaultView.ToTable
                Case 2
                    DA.Fill(DT_TAN)
                    DT_TAN.DefaultView.RowFilter = "LO_TAG_ID Is Not NULL"
                    DT_TAN = DT_TAN.DefaultView.ToTable
                Case 3
                    DA.Fill(DT_OX)
                    DT_OX.DefaultView.RowFilter = "LO_TAG_ID Is Not NULL"
                    DT_OX = DT_OX.DefaultView.ToTable
                Case 4
                    DA.Fill(DT_WATER)
                    DT_WATER.DefaultView.RowFilter = "LO_TAG_ID Is Not NULL"
                    DT_WATER = DT_WATER.DefaultView.ToTable
            End Select
        Next

        '-------------- Add Radar Image-------------
        DT.Columns.Add("Radar", GetType(Byte()))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim B As Byte() = Visual.Get_LubeOil_RadarImage(DT.Rows(i).Item("Detail_ID"))
            DT.Rows(i).Item("Radar") = B
        Next

        Dim cc As New ReportDocument()
        Dim ReportName As String = ""
        Dim TempName As String = ""
        Select Case DT.Rows(0).Item("LO_TAG_TYPE")
            Case EIR_BL.LO_Tag_Type.Balance_Of_Plant
                ReportName = "1_LO_BOP_INSPECTION" 'มี แค่ Particle Count และ Water
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & ReportName & ".pdf"
                cc.Load(ServerMapPath & "\Report\Lube_Oil_Routine_Report\" & ReportName & ".rpt")
                cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                cc.SetDataSource(DT)

                cc.Subreports("VANISH").SetDataSource(DT_VARNISH)
                cc.Subreports("PART_COUNT").SetDataSource(DT_PART)
            Case EIR_BL.LO_Tag_Type.Critical_Machine

                ReportName = "2_LO_CM_QUT_INSPECTION" ' มี ทั้งหมด
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & ReportName & ".pdf"
                cc.Load(ServerMapPath & "\Report\Lube_Oil_Routine_Report\" & ReportName & ".rpt")
                cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                cc.SetDataSource(DT)

                cc.Subreports("VANISH").SetDataSource(DT_VARNISH)
                cc.Subreports("PART_COUNT").SetDataSource(DT_PART)
                cc.Subreports("TAN").SetDataSource(DT_TAN)
                cc.Subreports("OX").SetDataSource(DT_OX)
                cc.Subreports("WATER").SetDataSource(DT_WATER)
        End Select

        cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
        cc.Close()
        cc.Dispose()
        Return TempName

    End Function

    Public Function Create_PdMA_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal Detail_ID As Integer, ByVal ReportType As EIR_BL.Report_Type) As String
        Dim Type As String = ""
        Select Case ReportType
            Case EIR_BL.Report_Type.PdMA_Report
                Type = "PDMA"
            Case EIR_BL.Report_Type.MTap_Report
                Type = "MTAP"
        End Select

        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "") & "_" & Detail_ID
        Dim SQL As String = "Select * FROM VW_REPORT_" & Type & "_DETAIL "
        SQL &= " WHERE TAG_Mode > 0 And RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " And DETAIL_ID=" & Detail_ID & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Return ""

        Dim cc As New ReportDocument()
        Dim ReportName As String = ""

        If Not IsDBNull(DT.Rows(0).Item("TAG_MODE")) AndAlso DT.Rows(0).Item("TAG_MODE") = 1 Then
            '--------------- Online -------------
            '------Image1-4----------
            For i As Integer = 1 To 4
                Select Case ReportType
                    Case EIR_BL.Report_Type.PdMA_Report
                        DT.Rows(0).Item("Image" & i) = BL.Get_PDMA_Image(RPT_Year, RPT_No, Detail_ID, "N_Img" & i)
                    Case EIR_BL.Report_Type.MTap_Report
                        DT.Rows(0).Item("Image" & i) = BL.Get_MTAP_Image(RPT_Year, RPT_No, Detail_ID, "N_Img" & i)
                End Select
            Next
            ReportName = "3_PDMA_ONLINE"
        Else
            '--------------- Offline ------------
            '------Image 5 ----------
            Select Case ReportType
                Case EIR_BL.Report_Type.PdMA_Report
                    DT.Rows(0).Item("Image5") = BL.Get_PDMA_Image(RPT_Year, RPT_No, Detail_ID, "F_Img5")
                Case EIR_BL.Report_Type.MTap_Report
                    DT.Rows(0).Item("Image5") = BL.Get_MTAP_Image(RPT_Year, RPT_No, Detail_ID, "F_Img5")
            End Select
            ReportName = "2_PDMA_OFFLINE"
        End If

        Dim TempName As String = ServerMapPath & "\Temp\" & UNIQUE_ID & "-" & ReportName & ".pdf"
        Try
            cc.Load(ServerMapPath & "\Report\PdMA_Routine_Report\" & ReportName & ".rpt")
            cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
            cc.SetDataSource(DT)
            cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
            cc.Close()
            cc.Dispose()
        Catch ex As Exception
            TempName = ""
        End Try
        Return TempName

    End Function

    Public Function Create_PdMA_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal ReportType As EIR_BL.Report_Type) As String

        Dim Type As String = ""
        Select Case ReportType
            Case EIR_BL.Report_Type.PdMA_Report
                Type = "PDMA"
            Case EIR_BL.Report_Type.MTap_Report
                Type = "MTAP"
        End Select


        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        Dim SQL As String = ""
        SQL &= " Select HEADER.*,DETAIL_ID,TAG_CODE,TAG_Mode,Is_Abnormal" & vbLf
        SQL &= " FROM VW_REPORT_" & Type & "_HEADER HEADER " & vbLf
        SQL &= " LEFT JOIN VW_REPORT_" & Type & "_DETAIL DETAIL " & vbLf
        SQL &= " On HEADER.RPT_Year = DETAIL.RPT_Year And HEADER.RPT_No = DETAIL.RPT_No " & vbLf
        SQL &= " WHERE Header.RPT_Year=" & RPT_Year & " And HEADER.RPT_No =" & RPT_No & vbLf
        'SQL &= " And TAG_Mode > 0 ORDER BY DETAIL.DETAIL_ID" & vbLf
        SQL &= " ORDER BY DETAIL.DETAIL_ID" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Return ""
        Dim PDFList As String() = {}
        Dim TempName As String = ""
        '------------------------------------------------ ใบปะหน้า 1 --------------------------------------------------
        Dim Cover1 As DataTable = DT.Copy
        Cover1.Rows.Clear()
        Cover1.AcceptChanges()
        Cover1.Columns.Add("PROB_Code")
        Cover1.Columns.Add("PROB_Detail")
        Cover1.Columns.Add("PROB_Recomment")
        Cover1.Columns.Add("Responsible")
        Cover1.Columns.Add("Is_NA", GetType(Integer))

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim DR As DataRow = Cover1.NewRow
            DR.ItemArray = DT.Rows(i).ItemArray
            Cover1.Rows.Add(DR)

            Dim tmp As DataTable = BL.Get_PDMA_Problem_Recomment(DT.Rows(i).Item("DETAIL_ID"))
            If tmp.Rows.Count > 0 Then
                For j As Integer = 0 To tmp.Rows.Count - 1
                    DR.ItemArray = DT.Rows(i).ItemArray
                    DR("PROB_Code") = tmp.Rows(j).Item("PROB_Code").ToString
                    DR("PROB_Detail") = tmp.Rows(j).Item("PROB_Detail").ToString
                    DR("PROB_Recomment") = tmp.Rows(j).Item("PROB_Recomment").ToString
                    DR("Responsible") = tmp.Rows(j).Item("Responsible").ToString
                Next
            End If
            If IsDBNull(DR("TAG_Mode")) OrElse DR("TAG_Mode") = 0 Then
                DR("Is_NA") = 1
            Else
                DR("Is_NA") = 0
            End If
        Next

        TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PdMA_Cover1.pdf"
        Try
            Dim cc As New ReportDocument()
            cc.Load(ServerMapPath & "\Report\PdMA_Routine_Report\0_PDMA_INSPECTION.rpt")
            cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
            cc.SetDataSource(Cover1)
            cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
            cc.Close()
            cc.Dispose()
            BL.PushString(PDFList, TempName)
        Catch ex As Exception

        End Try

        '------------------------------------------------ ใบปะหน้า 2 --------------------------------------------------
        SQL = " Select DETAIL.* ,HEADER.Officer_Collector,Officer_Analyst,Created_Time" & vbLf
        SQL &= " FROM VW_REPORT_" & Type & "_DETAIL DETAIL " & vbLf
        SQL &= " LEFT JOIN VW_REPORT_" & Type & "_HEADER HEADER" & vbLf
        SQL &= " On DETAIL.RPT_Year = HEADER.RPT_Year And DETAIL.RPT_No = HEADER.RPT_No" & vbLf
        SQL &= " WHERE DETAIL.RPT_Year=" & RPT_Year & " And DETAIL.RPT_No=" & RPT_No & vbLf
        SQL &= " And TAG_Mode > 0" & vbLf
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PdMA_Cover2.pdf"
        Try
            Dim cc1 As New ReportDocument()
            cc1.Load(ServerMapPath & "\Report\PdMA_Routine_Report\1_PDMA_INSPECTION_TAG.rpt")
            cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
            cc1.SetDataSource(DT)
            cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
            cc1.Close()
            cc1.Dispose()
            BL.PushString(PDFList, TempName)
        Catch ex As Exception

        End Try

        '--------------------------- รายละเอียดทีละ TAG --------------------------
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Result As String = Create_PdMA_Report(RPT_Year, RPT_No, DT.Rows(i).Item("DETAIL_ID"), ReportType)
            If Result <> "" Then
                BL.PushString(PDFList, Result)
            End If
        Next

        Dim OutputFile As String = UNIQUE_ID & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "_" & RPT_No.ToString.PadLeft(4, "0") & ".pdf"
        BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
        Return ServerMapPath & "\Temp\" & OutputFile
    End Function

    Public Function Create_THM_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As String
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        Dim SQL As String = ""
        SQL &= " Select *" & vbLf
        SQL &= " FROM VW_REPORT_THM_HEADER" & vbLf
        SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No =" & RPT_No & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Return ""
        Dim PDFList As String() = {}
        Dim TempName As String = ""
        '------------------------------------------------ ใบปะหน้า  --------------------------------------------------
        Dim Cover1 As DataTable = DT.Copy
        Cover1.Rows.Clear()
        Cover1.AcceptChanges()
        Cover1.Columns.Add("TAG_CODE")
        Cover1.Columns.Add("PROB_Recomment")
        Cover1.Columns.Add("Responsible")

        Dim DT_Tag As DataTable = BL.Get_THM_Problem_Recomment(RPT_Year, RPT_No)
        If DT_Tag.Rows.Count > 0 Then
            For i As Integer = 0 To DT_Tag.Rows.Count - 1
                Dim DR As DataRow = Cover1.NewRow
                DR.ItemArray = DT.Rows(0).ItemArray
                DR("TAG_CODE") = DT_Tag.Rows(i).Item("TAG_CODE").ToString
                DR("PROB_Recomment") = DT_Tag.Rows(i).Item("PROB_Recomment").ToString
                DR("Responsible") = DT_Tag.Rows(i).Item("Responsible").ToString
                Cover1.Rows.Add(DR)
            Next
        Else
            Dim DR As DataRow = Cover1.NewRow
            DR.ItemArray = DT.Rows(0).ItemArray
            Cover1.Rows.Add(DR)
        End If


        TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_THM_Cover1.pdf"
        Try
            Dim cc As New ReportDocument()
            cc.Load(ServerMapPath & "\Report\THM_Routine_Report\0_THM_INSPECTION.rpt")
            cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
            cc.SetDataSource(Cover1)
            cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
            cc.Close()
            cc.Dispose()
            BL.PushString(PDFList, TempName)

        Catch ex As Exception

        End Try

        '------------------------------------------------ TAG --------------------------------------------------
        SQL = " Select LEFT(Result_FileName,14) As Folder,Result_FileName FROM RPT_THM_Detail WHERE ISNULL(Result_FileName,'') <> ''" & vbLf
        SQL &= " AND RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbLf
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        For i As Integer = 0 To DT.Rows.Count - 1
            Try
                Dim Result As String = BL.PostedReport_Path & "\THM\" & DT.Rows(i).Item("Folder").ToString & "\" & DT.Rows(i).Item("Result_FileName").ToString
                Dim NewResult As String = ServerMapPath & "\Temp\" & UNIQUE_ID & DT.Rows(i).Item("Result_FileName").ToString
                File.Copy(Result, NewResult)
                BL.PushString(PDFList, NewResult)
            Catch ex As Exception
                'ถ้าไม่มีไฟล์ใน Folder Result ก็ไม่ต้อง Merge File นั้น
            End Try
        Next

        Dim OutputFile As String = UNIQUE_ID & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "_" & RPT_No.ToString.PadLeft(4, "0") & ".pdf"
        BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
        Return ServerMapPath & "\Temp\" & OutputFile
    End Function

    Public Function Create_PIPE_CUI_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As EIR_PIPE.Report_Step) As String
        Dim SQL As String = ""
        Dim PIPE As New EIR_PIPE
        Dim C As New Converter
        Select Case RPT_Step
            Case EIR_PIPE.Report_Step.Before_Remove
                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Visual_Inspection,Recommendation,Information_for_Scaffolding_Team" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= " FROM VW_PIPE_CUI_Before_Remove" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_CUI_Before_Remove_Inspection.pdf"

                '----------- Set Paging On Report Header-------------
                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")

                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\CUI\1_Before_Remove_Inspection.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch Ex As Exception
                    Dim S As String = Ex.Message
                End Try

                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_CUI_Before_Remove_VT.pdf"
                Try
                    Dim cc2 As New ReportDocument()
                    cc2.Load(ServerMapPath & "\Report\Pipe\CUI\1_Before_Remove_VT.rpt")
                    cc2.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc2.SetDataSource(DT)
                    cc2.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc2.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("EndPage"))
                    Else
                        cc2.SetParameterValue("CurrentPage", "")
                    End If
                    cc2.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc2.Close()
                    cc2.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch Ex As Exception
                    Dim S As String = Ex.Message
                End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_BeforeRemove.pdf"

                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.After_Remove
                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Visual_Inspection,Recommendation,Information_for_Scaffolding_Team" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= " FROM VW_PIPE_CUI_After_Remove" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_CUI_After_Remove_Inspection.pdf"

                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")

                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\CUI\2_After_Remove_Inspection.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_CUI_After_Remove_VT.pdf"
                Try
                    Dim cc2 As New ReportDocument()
                    cc2.Load(ServerMapPath & "\Report\Pipe\CUI\2_After_Remove_VT.rpt")
                    cc2.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc2.SetDataSource(DT)
                    cc2.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc2.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("EndPage"))
                    Else
                        cc2.SetParameterValue("CurrentPage", "")
                    End If
                    cc2.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc2.Close()
                    cc2.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_AfterRemove.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.Thickness_Measurement

                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_Code," & vbLf
                SQL &= " POINT_Name,TAG_Code,MAT_CODE_Name,Component_Type_Name,INSM_Name,INSM_Serial," & vbLf
                SQL &= " CAST(Size AS VARCHAR) Size,Required_Thickness_Mode," & vbLf
                SQL &= " CAST(Calculated_Thickness AS VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST('' AS VARCHAR) Required_Thickness," & vbLf
                SQL &= " DATEPART(YYYY,RPT_Date)-Initial_Year Diff_Year," & vbLf
                SQL &= " IMG1,CAST(NULL AS Image) IMG1_File,IMG1_Desc," & vbLf
                SQL &= " IMG2,CAST(NULL AS Image) IMG2_File,IMG2_Desc," & vbLf
                SQL &= " CAST(StepWeight_Thickness1 As VARCHAR) StepWeight_Thickness1," & vbLf
                SQL &= " CAST(StepWeight_Thickness2 As VARCHAR) StepWeight_Thickness2," & vbLf
                SQL &= " CAST(StepWeight_Thickness3 As VARCHAR) StepWeight_Thickness3," & vbLf
                SQL &= " CAST(StepWeight_Thickness4 As VARCHAR) StepWeight_Thickness4," & vbLf
                SQL &= " CAST(Measurement1 As VARCHAR) Measurement1," & vbLf
                SQL &= " CAST(Measurement2 As VARCHAR) Measurement2," & vbLf
                SQL &= " CAST(Measurement3 As VARCHAR) Measurement3," & vbLf
                SQL &= " CAST(Measurement4 As VARCHAR) Measurement4," & vbLf
                SQL &= " CAST(Velocity As VARCHAR) Velocity" & vbLf
                SQL &= " " & vbLf
                SQL &= " ,'' A1,'' A2,'' A3,'' A4,'' A5,'' A6,'' A7,'' A8,'' A9" & vbLf
                SQL &= " ,'' B1,'' B2,'' B3,'' B4,'' B5,'' B6,'' B7,'' B8,'' B9" & vbLf
                SQL &= " ,'' C1,'' C2,'' C3,'' C4,'' C5,'' C6,'' C7,'' C8,'' C9" & vbLf
                SQL &= " ,'' D1,'' D2,'' D3,'' D4,'' D5,'' D6,'' D7,'' D8,'' D9" & vbLf
                SQL &= " ,'' E1,'' E2,'' E3,'' E4,'' E5,'' E6,'' E7,'' E8,'' E9" & vbLf
                SQL &= " ,'' F1,'' F2,'' F3,'' F4,'' F5,'' F6,'' F7,'' F8,'' F9" & vbLf
                SQL &= " ,'' G1,'' G2,'' G3,'' G4,'' G5,'' G6,'' G7,'' G8,'' G9" & vbLf
                SQL &= " ,'' H1,'' H2,'' H3,'' H4,'' H5,'' H6,'' H7,'' H8,'' H9" & vbLf
                SQL &= " ,'' Min_1,'' Max_1,'' AVG_1" & vbLf
                SQL &= " ,'' Min_2,'' Max_2,'' AVG_2" & vbLf
                SQL &= " ,'' Min_3,'' Max_3,'' AVG_3" & vbLf
                SQL &= " ,'' Min_4,'' Max_4,'' AVG_4" & vbLf
                SQL &= " ,'' Min_5,'' Max_5,'' AVG_5" & vbLf
                SQL &= " ,'' Min_6,'' Max_6,'' AVG_6" & vbLf
                SQL &= " ,'' Min_7,'' Max_7,'' AVG_7" & vbLf
                SQL &= " ,'' Min_8,'' Max_8,'' AVG_8" & vbLf
                SQL &= " ,'' Min_9,'' Max_9,'' AVG_9" & vbLf
                SQL &= " ,CAST(Min_Thickness AS VARCHAR) Min_Thickness" & vbLf
                SQL &= " ,CAST(Last_Measure_Year AS VARCHAR) Last_Measure_Year" & vbLf
                SQL &= " ,CAST(Last_Thickness AS VARCHAR) Last_Thickness" & vbLf
                SQL &= " ,CAST(Corrosion_Rate AS VARCHAR) Corrosion_Rate" & vbLf
                SQL &= " ,CAST(Remain_Life AS VARCHAR) Remain_Life" & vbLf
                SQL &= " ,Use_Term,Note,RPT_STEP" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= " FROM VW_PIPE_CUI_TM_Header UTM" & vbLf

                SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""
                Dim DR As DataRow = DT.Rows(0)

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 2
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If Not IsDBNull(DR("Norminal_Thickness")) Then DR("Norminal_Thickness") = FormatNumericTextLimitPlace(DR("Norminal_Thickness"), True, 2)
                If Not IsDBNull(DR("Corrosion_Allowance")) Then DR("Corrosion_Allowance") = FormatNumericTextLimitPlace(DR("Corrosion_Allowance"), True, 2)

                '-------------- Get Required Thickness ---------------
                If IsNumeric(DR("Required_Thickness_Mode")) Then
                    Select Case DR("Required_Thickness_Mode")
                        Case 1 'Norminal_Thickness -Corrosion_Allowance
                            If Not IsDBNull(DR("Norminal_Thickness")) And Not IsDBNull(DR("Corrosion_Allowance")) Then
                                DR("Required_Thickness") = CDbl(DR("Norminal_Thickness")) - CDbl(DR("Corrosion_Allowance"))
                            Else
                                DR("Required_Thickness") = DBNull.Value
                            End If
                        Case 2 '
                            If Not IsDBNull(DR("Calculated_Thickness")) And Not IsDBNull(DR("Corrosion_Allowance")) Then
                                DR("Required_Thickness") = CDbl(DR("Calculated_Thickness")) + CDbl(DR("Corrosion_Allowance"))
                            Else
                                DR("Required_Thickness") = DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(DR("Calculated_Thickness")) Then
                                DR("Required_Thickness") = CDbl(DR("Calculated_Thickness"))
                            Else
                                DR("Required_Thickness") = DBNull.Value
                            End If
                    End Select
                End If

                If Not IsDBNull(DR("StepWeight_Thickness1")) Then DR("StepWeight_Thickness1") = FormatNumericTextLimitPlace(DR("StepWeight_Thickness1"), True, 2)
                If Not IsDBNull(DR("StepWeight_Thickness2")) Then DR("StepWeight_Thickness2") = FormatNumericTextLimitPlace(DR("StepWeight_Thickness2"), True, 2)
                If Not IsDBNull(DR("StepWeight_Thickness3")) Then DR("StepWeight_Thickness3") = FormatNumericTextLimitPlace(DR("StepWeight_Thickness3"), True, 2)
                If Not IsDBNull(DR("StepWeight_Thickness4")) Then DR("StepWeight_Thickness4") = FormatNumericTextLimitPlace(DR("StepWeight_Thickness4"), True, 2)
                If Not IsDBNull(DR("Measurement1")) Then DR("Measurement1") = FormatNumericTextLimitPlace(DR("Measurement1"), True, 2)
                If Not IsDBNull(DR("Measurement2")) Then DR("Measurement2") = FormatNumericTextLimitPlace(DR("Measurement2"), True, 2)
                If Not IsDBNull(DR("Measurement3")) Then DR("Measurement3") = FormatNumericTextLimitPlace(DR("Measurement3"), True, 2)
                If Not IsDBNull(DR("Measurement4")) Then DR("Measurement4") = FormatNumericTextLimitPlace(DR("Measurement4"), True, 2)
                If Not IsDBNull(DR("Velocity")) Then DR("Velocity") = FormatNumericTextLimitPlace(DR("Velocity"), True, 2)

                If Not IsDBNull(DR("Min_Thickness")) Then DR("Min_Thickness") = FormatNumericTextLimitPlace(DR("Min_Thickness"), True, 2)
                If Not IsDBNull(DR("Last_Measure_Year")) Then DR("Last_Measure_Year") = CInt(DR("Last_Measure_Year"))
                If Not IsDBNull(DR("Last_Thickness")) Then DR("Last_Thickness") = FormatNumericTextLimitPlace(DR("Last_Thickness"), True, 2)
                If Not IsDBNull(DR("Corrosion_Rate")) Then DR("Corrosion_Rate") = FormatNumericTextLimitPlace(DR("Corrosion_Rate"), True, 2)
                If Not IsDBNull(DR("Remain_Life")) Then DR("Remain_Life") = FormatNumber(DR("Remain_Life"), 0)

                '---------- Get Measurement Point ---------------
                Dim DB As DataTable = PIPE.Get_Thickness_Mesaurement_Detail(RPT_Year, RPT_No)
                For i As Integer = 0 To DB.Rows.Count - 1 '--------A-H
                    Dim P1 As String = DB.Rows(i).Item("Point")
                    For j As Integer = 1 To DB.Columns.Count - 1
                        Dim P2 As String = DB.Columns(j).ColumnName.ToUpper.Replace("SUB", "")
                        If Not IsDBNull(DB.Rows(i).Item(j)) Then DR(P1 & P2) = FormatNumericTextLimitPlace(DB.Rows(i).Item(j), True, 2)
                    Next
                Next
                '----------- Get Measurement Summary ------------
                For i As Integer = 1 To 9
                    Dim AVG As Object = DB.Compute("AVG(Sub" & i & ")", "")
                    If Not IsDBNull(AVG) Then
                        DR("AVG_" & i) = FormatNumericTextLimitPlace(AVG, True, 2)
                    Else
                        DR("AVG_" & i) = DBNull.Value
                    End If

                    Dim MIN As Object = DB.Compute("MIN(Sub" & i & ")", "")
                    If Not IsDBNull(MIN) Then
                        DR("Min_" & i) = FormatNumericTextLimitPlace(MIN, True, 2)
                    Else
                        DR("Min_" & i) = DBNull.Value
                    End If

                    Dim MAX As Object = DB.Compute("MAX(Sub" & i & ")", "")
                    If Not IsDBNull(MAX) Then
                        DR("Max_" & i) = FormatNumericTextLimitPlace(MAX, True, 2)
                    Else
                        DR("Max_" & i) = DBNull.Value
                    End If
                Next

                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                Dim TempPath As String = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & DR("RPT_Code") & "_TM.pdf"

                Dim ReportName As String = ""
                If IsDBNull(DR("Use_Term")) OrElse DR("Use_Term") = 2 Then
                    ReportName = ServerMapPath & "\Report\Pipe\CUI\3_TM_Measurement_Long.rpt"
                Else
                    ReportName = ServerMapPath & "\Report\Pipe\CUI\3_TM_Measurement_Short.rpt"
                End If

                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")

                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ReportName)
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempPath)
                    cc1.Close()
                    cc1.Dispose()
                Catch : End Try

                Return TempPath

            Case EIR_PIPE.Report_Step.After_Repair

                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Note,Recommendation" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= "  FROM VW_PIPE_CUI_After_Repair" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_CUI_After_Repair_Inspection.pdf"


                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")

                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\CUI\4_After_Repair_Inspection.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_AfterRepair.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.Coating_Inspection

                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Note,Recommendation" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= "  FROM VW_PIPE_CUI_Coating" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_CUI_Coating_Inspection.pdf"

                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")

                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\CUI\5_Coating_Inspection.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If

                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_Coating_Inspection.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.Insulating_Inspection

                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Note,Recommendation" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= "  FROM VW_PIPE_CUI_Insulating" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_CUI_Insulating_Inspection.pdf"

                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")
                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\CUI\6_Insulating_Inspection.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_Insulating_Inspection.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.Final

                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Note,Recommendation" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= "  FROM VW_PIPE_CUI_Final_Report" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_CUI_Final_Report.pdf"

                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")
                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\CUI\7_Final_Report.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_Final_Report.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.NA 'All Report

                SQL = "SELECT Result_FileName FROM VW_PIPE_CUI_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND Finished=1"
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 AndAlso DT.Rows(0).Item("Result_FileName").ToString <> "" Then
                    Dim Path As String = BL.PostedReport_Path & "\" & DT.Rows(0).Item("Result_FileName")
                    If File.Exists(Path) Then Return Path
                End If


                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim PDFList As String() = {}
                For i As Integer = 0 To STP.Rows.Count - 1
                    Try

                        Dim ReportFileName As String = Create_PIPE_CUI_Report(RPT_Year, RPT_No, STP.Rows(i).Item("RPT_STEP"))
                        If ReportFileName <> "" AndAlso File.Exists(ReportFileName) Then
                            BL.PushString(PDFList, ReportFileName)
                        End If
                    Catch ex As Exception
                        Return ex.Message
                    End Try
                Next
                If PDFList.Length = 0 Then Return ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                Dim OutputFile As String = UNIQUE_ID & "_PIPE_CUI_Report.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile
        End Select

        Return ""
    End Function

    Public Function Create_PIPE_ERO_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RPT_Step As EIR_PIPE.Report_Step) As String
        Dim SQL As String = ""
        Dim PIPE As New EIR_PIPE
        Dim C As New Converter
        Select Case RPT_Step
            Case EIR_PIPE.Report_Step.Before_Remove
                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Visual_Inspection,Recommendation,Information_for_Scaffolding_Team" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= " FROM VW_PIPE_ERO_Before_Remove" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_ERO_Before_Remove_Inspection.pdf"

                '----------- Set Paging On Report Header-------------
                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")


                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\ERO\1_Before_Remove_Inspection.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_ERO_Before_Remove_VT.pdf"
                Try
                    Dim cc2 As New ReportDocument()
                    cc2.Load(ServerMapPath & "\Report\Pipe\ERO\1_Before_Remove_VT.rpt")
                    cc2.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc2.SetDataSource(DT)
                    cc2.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc2.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("EndPage"))
                    Else
                        cc2.SetParameterValue("CurrentPage", "")
                    End If
                    cc2.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc2.Close()
                    cc2.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_BeforeRemove.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.After_Remove
                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Visual_Inspection,Recommendation,Information_for_Scaffolding_Team" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= " FROM VW_PIPE_ERO_After_Remove" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_ERO_After_Remove_Inspection.pdf"

                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")

                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\ERO\2_After_Remove_Inspection.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_ERO_After_Remove_VT.pdf"
                Try
                    Dim cc2 As New ReportDocument()
                    cc2.Load(ServerMapPath & "\Report\Pipe\ERO\2_After_Remove_VT.rpt")
                    cc2.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc2.SetDataSource(DT)
                    cc2.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc2.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("EndPage"))
                    Else
                        cc2.SetParameterValue("CurrentPage", "")
                    End If
                    cc2.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc2.Close()
                    cc2.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_AfterRemove.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.Thickness_Measurement

                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_Code," & vbLf
                SQL &= " POINT_Name,TAG_Code,MAT_CODE_Name,Component_Type_Name,INSM_Name,INSM_Serial," & vbLf
                SQL &= " CAST(Size AS VARCHAR) Size,Required_Thickness_Mode," & vbLf
                SQL &= " CAST(Calculated_Thickness AS VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST('' AS VARCHAR) Required_Thickness," & vbLf
                SQL &= " DATEPART(YYYY,RPT_Date)-Initial_Year Diff_Year," & vbLf
                SQL &= " IMG1,CAST(NULL AS Image) IMG1_File,IMG1_Desc," & vbLf
                SQL &= " IMG2,CAST(NULL AS Image) IMG2_File,IMG2_Desc," & vbLf
                SQL &= " CAST(StepWeight_Thickness1 As VARCHAR) StepWeight_Thickness1," & vbLf
                SQL &= " CAST(StepWeight_Thickness2 As VARCHAR) StepWeight_Thickness2," & vbLf
                SQL &= " CAST(StepWeight_Thickness3 As VARCHAR) StepWeight_Thickness3," & vbLf
                SQL &= " CAST(StepWeight_Thickness4 As VARCHAR) StepWeight_Thickness4," & vbLf
                SQL &= " CAST(Measurement1 As VARCHAR) Measurement1," & vbLf
                SQL &= " CAST(Measurement2 As VARCHAR) Measurement2," & vbLf
                SQL &= " CAST(Measurement3 As VARCHAR) Measurement3," & vbLf
                SQL &= " CAST(Measurement4 As VARCHAR) Measurement4," & vbLf
                SQL &= " CAST(Velocity As VARCHAR) Velocity" & vbLf
                SQL &= " " & vbLf
                SQL &= " ,'' A1,'' A2,'' A3,'' A4,'' A5,'' A6,'' A7,'' A8,'' A9" & vbLf
                SQL &= " ,'' B1,'' B2,'' B3,'' B4,'' B5,'' B6,'' B7,'' B8,'' B9" & vbLf
                SQL &= " ,'' C1,'' C2,'' C3,'' C4,'' C5,'' C6,'' C7,'' C8,'' C9" & vbLf
                SQL &= " ,'' D1,'' D2,'' D3,'' D4,'' D5,'' D6,'' D7,'' D8,'' D9" & vbLf
                SQL &= " ,'' E1,'' E2,'' E3,'' E4,'' E5,'' E6,'' E7,'' E8,'' E9" & vbLf
                SQL &= " ,'' F1,'' F2,'' F3,'' F4,'' F5,'' F6,'' F7,'' F8,'' F9" & vbLf
                SQL &= " ,'' G1,'' G2,'' G3,'' G4,'' G5,'' G6,'' G7,'' G8,'' G9" & vbLf
                SQL &= " ,'' H1,'' H2,'' H3,'' H4,'' H5,'' H6,'' H7,'' H8,'' H9" & vbLf
                SQL &= " ,'' Min_1,'' Max_1,'' AVG_1" & vbLf
                SQL &= " ,'' Min_2,'' Max_2,'' AVG_2" & vbLf
                SQL &= " ,'' Min_3,'' Max_3,'' AVG_3" & vbLf
                SQL &= " ,'' Min_4,'' Max_4,'' AVG_4" & vbLf
                SQL &= " ,'' Min_5,'' Max_5,'' AVG_5" & vbLf
                SQL &= " ,'' Min_6,'' Max_6,'' AVG_6" & vbLf
                SQL &= " ,'' Min_7,'' Max_7,'' AVG_7" & vbLf
                SQL &= " ,'' Min_8,'' Max_8,'' AVG_8" & vbLf
                SQL &= " ,'' Min_9,'' Max_9,'' AVG_9" & vbLf
                SQL &= " ,CAST(Min_Thickness AS VARCHAR) Min_Thickness" & vbLf
                SQL &= " ,CAST(Last_Measure_Year AS VARCHAR) Last_Measure_Year" & vbLf
                SQL &= " ,CAST(Last_Thickness AS VARCHAR) Last_Thickness" & vbLf
                SQL &= " ,CAST(Corrosion_Rate AS VARCHAR) Corrosion_Rate" & vbLf
                SQL &= " ,CAST(Remain_Life AS VARCHAR) Remain_Life" & vbLf
                SQL &= " ,Use_Term,Note,RPT_STEP" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= " FROM VW_PIPE_ERO_TM_Header UTM" & vbLf

                SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""
                Dim DR As DataRow = DT.Rows(0)

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 2
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If Not IsDBNull(DR("Norminal_Thickness")) Then DR("Norminal_Thickness") = FormatNumericTextLimitPlace(DR("Norminal_Thickness"), True, 2)
                If Not IsDBNull(DR("Corrosion_Allowance")) Then DR("Corrosion_Allowance") = FormatNumericTextLimitPlace(DR("Corrosion_Allowance"), True, 2)

                '-------------- Get Required Thickness ---------------
                If IsNumeric(DR("Required_Thickness_Mode")) Then
                    Select Case DR("Required_Thickness_Mode")
                        Case 1 'Norminal_Thickness -Corrosion_Allowance
                            If Not IsDBNull(DR("Norminal_Thickness")) And Not IsDBNull(DR("Corrosion_Allowance")) Then
                                DR("Required_Thickness") = CDbl(DR("Norminal_Thickness")) - CDbl(DR("Corrosion_Allowance"))
                            Else
                                DR("Required_Thickness") = DBNull.Value
                            End If
                        Case 2 '
                            If Not IsDBNull(DR("Calculated_Thickness")) And Not IsDBNull(DR("Corrosion_Allowance")) Then
                                DR("Required_Thickness") = CDbl(DR("Calculated_Thickness")) + CDbl(DR("Corrosion_Allowance"))
                            Else
                                DR("Required_Thickness") = DBNull.Value
                            End If
                        Case 3
                            If Not IsDBNull(DR("Calculated_Thickness")) Then
                                DR("Required_Thickness") = CDbl(DR("Calculated_Thickness"))
                            Else
                                DR("Required_Thickness") = DBNull.Value
                            End If
                    End Select
                End If

                If Not IsDBNull(DR("StepWeight_Thickness1")) Then DR("StepWeight_Thickness1") = FormatNumericTextLimitPlace(DR("StepWeight_Thickness1"), True, 2)
                If Not IsDBNull(DR("StepWeight_Thickness2")) Then DR("StepWeight_Thickness2") = FormatNumericTextLimitPlace(DR("StepWeight_Thickness2"), True, 2)
                If Not IsDBNull(DR("StepWeight_Thickness3")) Then DR("StepWeight_Thickness3") = FormatNumericTextLimitPlace(DR("StepWeight_Thickness3"), True, 2)
                If Not IsDBNull(DR("StepWeight_Thickness4")) Then DR("StepWeight_Thickness4") = FormatNumericTextLimitPlace(DR("StepWeight_Thickness4"), True, 2)
                If Not IsDBNull(DR("Measurement1")) Then DR("Measurement1") = FormatNumericTextLimitPlace(DR("Measurement1"), True, 2)
                If Not IsDBNull(DR("Measurement2")) Then DR("Measurement2") = FormatNumericTextLimitPlace(DR("Measurement2"), True, 2)
                If Not IsDBNull(DR("Measurement3")) Then DR("Measurement3") = FormatNumericTextLimitPlace(DR("Measurement3"), True, 2)
                If Not IsDBNull(DR("Measurement4")) Then DR("Measurement4") = FormatNumericTextLimitPlace(DR("Measurement4"), True, 2)
                If Not IsDBNull(DR("Velocity")) Then DR("Velocity") = FormatNumericTextLimitPlace(DR("Velocity"), True, 2)

                If Not IsDBNull(DR("Min_Thickness")) Then DR("Min_Thickness") = FormatNumericTextLimitPlace(DR("Min_Thickness"), True, 2)
                If Not IsDBNull(DR("Last_Measure_Year")) Then DR("Last_Measure_Year") = CInt(DR("Last_Measure_Year"))
                If Not IsDBNull(DR("Last_Thickness")) Then DR("Last_Thickness") = FormatNumericTextLimitPlace(DR("Last_Thickness"), True, 2)
                If Not IsDBNull(DR("Corrosion_Rate")) Then DR("Corrosion_Rate") = FormatNumericTextLimitPlace(DR("Corrosion_Rate"), True, 2)
                If Not IsDBNull(DR("Remain_Life")) Then DR("Remain_Life") = FormatNumber(DR("Remain_Life"), 0)

                '---------- Get Measurement Point ---------------
                Dim DB As DataTable = PIPE.Get_Thickness_Mesaurement_Detail(RPT_Year, RPT_No)
                For i As Integer = 0 To DB.Rows.Count - 1 '--------A-H
                    Dim P1 As String = DB.Rows(i).Item("Point")
                    For j As Integer = 1 To DB.Columns.Count - 1
                        Dim P2 As String = DB.Columns(j).ColumnName.ToUpper.Replace("SUB", "")
                        If Not IsDBNull(DB.Rows(i).Item(j)) Then DR(P1 & P2) = FormatNumericTextLimitPlace(DB.Rows(i).Item(j), True, 2)
                    Next
                Next
                '----------- Get Measurement Summary ------------
                For i As Integer = 1 To 9
                    Dim AVG As Object = DB.Compute("AVG(Sub" & i & ")", "")
                    If Not IsDBNull(AVG) Then
                        DR("AVG_" & i) = FormatNumericTextLimitPlace(AVG, True, 2)
                    Else
                        DR("AVG_" & i) = DBNull.Value
                    End If

                    Dim MIN As Object = DB.Compute("MIN(Sub" & i & ")", "")
                    If Not IsDBNull(MIN) Then
                        DR("Min_" & i) = FormatNumericTextLimitPlace(MIN, True, 2)
                    Else
                        DR("Min_" & i) = DBNull.Value
                    End If

                    Dim MAX As Object = DB.Compute("MAX(Sub" & i & ")", "")
                    If Not IsDBNull(MAX) Then
                        DR("Max_" & i) = FormatNumericTextLimitPlace(MAX, True, 2)
                    Else
                        DR("Max_" & i) = DBNull.Value
                    End If
                Next

                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                Dim TempPath As String = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & DR("RPT_Code") & "_TM.pdf"

                Dim ReportName As String = ""
                If IsDBNull(DR("Use_Term")) OrElse DR("Use_Term") = 2 Then
                    ReportName = ServerMapPath & "\Report\Pipe\ERO\3_TM_Measurement_Long.rpt"
                Else
                    ReportName = ServerMapPath & "\Report\Pipe\ERO\3_TM_Measurement_Short.rpt"
                End If

                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")

                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ReportName)
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempPath)
                    cc1.Close()
                    cc1.Dispose()
                Catch ex As Exception
                End Try

                Return TempPath

            Case EIR_PIPE.Report_Step.After_Repair

                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Note,Recommendation" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= "  FROM VW_PIPE_ERO_After_Repair" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_ERO_After_Repair_Inspection.pdf"


                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")

                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\ERO\4_After_Repair_Inspection.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_AfterRepair.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.Insulating_Inspection

                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Note,Recommendation" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= "  FROM VW_PIPE_ERO_Insulating" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_ERO_Insulating_Inspection.pdf"

                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")
                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\ERO\6_Insulating_Inspection.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_Insulating_Inspection.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.Final

                SQL = " SELECT RPT_Code,RPT_Year,RPT_No,RPT_Date,PLANT_ID,PLANT_Code,TAG_ID ,POINT_ID,POINT_Name,TAG_Code," & vbLf
                SQL &= " Component_Type,Component_Type_Name,MAT_CODE_ID,MAT_CODE,MAT_CODE_Name,IN_ID,IN_Code," & vbLf
                SQL &= " Schedule,SERVICE_ID,SERVICE_Name, Remain_Life,P_ID_No,Location_From,Location_To," & vbLf
                SQL &= " " & vbLf
                SQL &= " CAST(Size As VARCHAR) Size," & vbLf
                SQL &= " CAST(Norminal_Thickness As VARCHAR) Norminal_Thickness," & vbLf
                SQL &= " CAST(Calculated_Thickness As VARCHAR) Calculated_Thickness," & vbLf
                SQL &= " CAST(CA_DEPTH_MM As VARCHAR) Corrosion_Allowance," & vbLf
                SQL &= " CAST(Pressure_Design As VARCHAR) Pressure_Design," & vbLf
                SQL &= " CAST(Pressure_Operating As VARCHAR) Pressure_Operating," & vbLf
                SQL &= " CAST(Temperature_Design As VARCHAR) Temperature_Design," & vbLf
                SQL &= " CAST(Temperature_Operating As VARCHAR) Temperature_Operating" & vbLf

                SQL &= " ,CAST(NULL As Image) IMG1_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG2_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG3_File" & vbLf
                SQL &= " ,CAST(NULL As Image) IMG4_File" & vbLf

                SQL &= " ,Note,Recommendation" & vbLf
                SQL &= " ,IMG1,IMG1_Desc,IMG2,IMG2_Desc,IMG3,IMG3_Desc,IMG4,IMG4_Desc" & vbLf
                SQL &= " ,RPT_STEP,Step_Name" & vbLf
                SQL &= " ,Finised,Started_Time,Finished_Time,RPT_LOCK_BY,Lock_By_Name" & vbLf
                SQL &= " ,RPT_Outsource,RPT_Engineer,RPT_Approver" & vbLf
                SQL &= " " & vbLf
                SQL &= "  FROM VW_PIPE_ERO_Final_Report" & vbLf
                SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then Return ""

                Dim ImagePath As String = BL.Picture_Path & "\Pipe\Image"
                For i As Integer = 1 To 4
                    Try
                        Dim _uid As String = DT.Rows(0).Item("IMG" & i).ToString
                        Dim _image_path As String = ImagePath & "\" & _uid
                        Dim B As Byte() = Nothing
                        If File.Exists(_image_path) Then
                            Dim ST As FileStream = File.Open(_image_path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                            B = C.StreamToByte(ST)
                            ST.Close()
                        ElseIf DT.Rows(0).Item("IMG" & i).ToString <> "" Then
                            Dim FA As FileAttachment = PIPE.Get_FileAttachment_By_ID(_uid)
                            B = FA.Content
                        End If
                        DT.Rows(0).Item("IMG" & i & "_File") = B
                    Catch : End Try
                Next

                If IsNumeric(DT.Rows(0).Item("Size")) Then DT.Rows(0).Item("Size") = FormatNumericText(DT.Rows(0).Item("Size"))

                If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then DT.Rows(0).Item("Norminal_Thickness") = FormatNumericText(DT.Rows(0).Item("Norminal_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then DT.Rows(0).Item("Calculated_Thickness") = FormatNumericText(DT.Rows(0).Item("Calculated_Thickness"))
                If Not IsDBNull(DT.Rows(0).Item("Corrosion_Allowance")) Then DT.Rows(0).Item("Corrosion_Allowance") = FormatNumericText(DT.Rows(0).Item("Corrosion_Allowance"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then DT.Rows(0).Item("Pressure_Design") = FormatNumericText(DT.Rows(0).Item("Pressure_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then DT.Rows(0).Item("Pressure_Operating") = FormatNumericText(DT.Rows(0).Item("Pressure_Operating"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then DT.Rows(0).Item("Temperature_Design") = FormatNumericText(DT.Rows(0).Item("Temperature_Design"))
                If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then DT.Rows(0).Item("Temperature_Operating") = FormatNumericText(DT.Rows(0).Item("Temperature_Operating"))

                Dim PDFList As String() = {}
                Dim TempName As String = ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_PIP_ERO_Final_Report.pdf"

                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim TotalPage As Integer = STP.Compute("SUM(ReportPage)", "")
                Try
                    Dim cc1 As New ReportDocument()
                    cc1.Load(ServerMapPath & "\Report\Pipe\ERO\7_Final_Report.rpt")
                    cc1.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                    cc1.SetDataSource(DT)
                    cc1.SetParameterValue("TotalPage", TotalPage)
                    STP.DefaultView.RowFilter = "RPT_STEP=" & RPT_Step
                    If STP.DefaultView.Count > 0 Then
                        cc1.SetParameterValue("CurrentPage", STP.DefaultView(0).Item("StartPage"))
                    Else
                        cc1.SetParameterValue("CurrentPage", "")
                    End If
                    cc1.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc1.Close()
                    cc1.Dispose()
                    BL.PushString(PDFList, TempName)
                Catch : End Try

                Dim OutputFile As String = UNIQUE_ID & "_" & DT.Rows(0).Item("RPT_Code") & "_Final_Report.pdf"
                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile

            Case EIR_PIPE.Report_Step.NA 'All Report

                SQL = "SELECT Result_FileName FROM VW_PIPE_ERO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND Finished=1"
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 AndAlso DT.Rows(0).Item("Result_FileName").ToString <> "" Then
                    Dim Path As String = BL.PostedReport_Path & "\" & DT.Rows(0).Item("Result_FileName")
                    If File.Exists(Path) Then Return Path
                End If


                Dim STP As DataTable = PIPE.Get_All_Report_Step(RPT_Year, RPT_No)
                Dim PDFList As String() = {}
                For i As Integer = 0 To STP.Rows.Count - 1
                    Try
                        BL.PushString(PDFList, Create_PIPE_ERO_Report(RPT_Year, RPT_No, STP.Rows(i).Item("RPT_STEP")))
                    Catch ex As Exception
                    End Try
                Next
                If PDFList.Length = 0 Then Return ""
                Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                Dim OutputFile As String = UNIQUE_ID & "_PIPE_ERO_Report.pdf"

                BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
                Return ServerMapPath & "\Temp\" & OutputFile
        End Select

        Return ""
    End Function

    Public Function Create_PIPE_ROUTINE_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As String
        '------------------- Initiate UNIQUE_ID -------------------------
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        '----------------------------------------------------------------
        If RPT_Year = 0 Or RPT_No = 0 Then Return False

        Dim PDFList As String() = {}

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim VW_PIPE_ROUTINE_HEADER As New DataTable
        Dim VW_PIPE_ROUTINE_DETAIL As New DataTable

        SQL = "select * from VW_PIPE_ROUTINE_HEADER where RPT_Year = " & RPT_Year & " and RPT_No = " & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(VW_PIPE_ROUTINE_HEADER)
        SQL = "select * from VW_PIPE_ROUTINE_DETAIL where RPT_Year = " & RPT_Year & " and RPT_No = " & RPT_No
        SQL &= "order by ISSUE,TAG_CODE,INSP_ID,DETAIL_ID"

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(VW_PIPE_ROUTINE_DETAIL)

        Dim ReportName() As String = {"1_ST_INSPECTION", "2_ROUTINE INSPECTION", "3_TRACE SLOVE", "5_ST_PHOTO"}

        For i As Integer = 0 To ReportName.Length - 1

            Dim cc As New ReportDocument()
            Dim TempName As String = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & ReportName(i) & ".pdf"
            cc.Load(ServerMapPath & "\Report\PIPE\ROUTINE\" & ReportName(i) & ".rpt")
            cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)

            Select Case ReportName(i).ToUpper
                Case "1_ST_INSPECTION"
                    SQL = "select SUM(rec) as Tag" & vbNewLine
                    SQL &= " ,SUM(NewIssue) as NewIssue" & vbNewLine
                    SQL &= " ,SUM(Issue) as Issue" & vbNewLine
                    SQL &= " ,SUM(Success) as Success" & vbNewLine
                    SQL &= " ,SUM(NonSuccess) as NonSuccess" & vbNewLine
                    SQL &= " ,SUM(PicNewIssue) as PicNewIssue" & vbNewLine
                    SQL &= " ,SUM(PicIssue) as PicIssue" & vbNewLine
                    SQL &= " ,SUM(PicSuccess) as PicSuccess" & vbNewLine
                    SQL &= " ,SUM(PicNonSuccess) as PicNonSuccess" & vbNewLine
                    SQL &= " from (" & vbNewLine
                    SQL &= " select 1 as Rec" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 1 THEN 1 ELSE 0 END) AS NewIssue" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 2 THEN 1 ELSE 0 END) AS Issue" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 3 THEN 1 ELSE 0 END) AS Success" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 4 THEN 1 ELSE 0 END) AS NonSuccess" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 1 THEN CASE WHEN CURRENT_PICTURE1 = 1 THEN 1 ELSE 0 END " & vbNewLine
                    SQL &= " + CASE WHEN CURRENT_PICTURE2 = 1 THEN 1 ELSE 0 END ELSE 0 END) as PicNewIssue" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 2 THEN CASE WHEN CURRENT_PICTURE1 = 1 THEN 1 ELSE 0 END " & vbNewLine
                    SQL &= " + CASE WHEN CURRENT_PICTURE2 = 1 THEN 1 ELSE 0 END ELSE 0 END) as PicIssue" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 3 THEN CASE WHEN CURRENT_PICTURE1 = 1 THEN 1 ELSE 0 END " & vbNewLine
                    SQL &= " + CASE WHEN CURRENT_PICTURE2 = 1 THEN 1 ELSE 0 END ELSE 0 END) as PicSuccess" & vbNewLine
                    SQL &= " ,SUM(CASE WHEN ISSUE = 4 THEN CASE WHEN CURRENT_PICTURE1 = 1 THEN 1 ELSE 0 END " & vbNewLine
                    SQL &= " + CASE WHEN CURRENT_PICTURE2 = 1 THEN 1 ELSE 0 END ELSE 0 END) as PicNonSuccess" & vbNewLine
                    SQL &= " from VW_PIPE_ROUTINE_DETAIL where RPT_Year = " & RPT_Year & " and RPT_No = " & RPT_No & vbNewLine
                    SQL &= " GROUP BY TAG_Code" & vbNewLine
                    SQL &= " ) as ALL_TAG" & vbNewLine

                    Dim DT As New DataTable
                    DA = New SqlDataAdapter(SQL, BL.ConnStr)
                    DA.Fill(DT)

                    cc.SetDataSource(VW_PIPE_ROUTINE_HEADER)

                    cc.DataDefinition.FormulaFields("NewIssue").Text = DT.Rows(0).Item("NewIssue").ToString
                    cc.DataDefinition.FormulaFields("Issue").Text = DT.Rows(0).Item("Issue").ToString
                    cc.DataDefinition.FormulaFields("Success").Text = DT.Rows(0).Item("Success").ToString
                    cc.DataDefinition.FormulaFields("NonSuccess").Text = DT.Rows(0).Item("NonSuccess").ToString
                    cc.DataDefinition.FormulaFields("PicNewIssue").Text = DT.Rows(0).Item("PicNewIssue").ToString
                    cc.DataDefinition.FormulaFields("PicIssue").Text = DT.Rows(0).Item("PicIssue").ToString
                    cc.DataDefinition.FormulaFields("PicSuccess").Text = DT.Rows(0).Item("PicSuccess").ToString
                    cc.DataDefinition.FormulaFields("PicNonSuccess").Text = DT.Rows(0).Item("PicNonSuccess").ToString
                    cc.DataDefinition.FormulaFields("Tag").Text = DT.Rows(0).Item("Tag").ToString

                    DT = New DataTable
                    VW_PIPE_ROUTINE_DETAIL.DefaultView.RowFilter = "ISSUE = 1"
                    DT = VW_PIPE_ROUTINE_DETAIL.DefaultView.ToTable
                    VW_PIPE_ROUTINE_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("New Issue").SetDataSource(DT)

                    DT = New DataTable
                    VW_PIPE_ROUTINE_DETAIL.DefaultView.RowFilter = "ISSUE = 2"
                    DT = VW_PIPE_ROUTINE_DETAIL.DefaultView.ToTable
                    VW_PIPE_ROUTINE_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("Non Modify").SetDataSource(DT)

                    DT = New DataTable
                    VW_PIPE_ROUTINE_DETAIL.DefaultView.RowFilter = "ISSUE = 3"
                    DT = VW_PIPE_ROUTINE_DETAIL.DefaultView.ToTable
                    VW_PIPE_ROUTINE_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("Success").SetDataSource(DT)

                    DT = New DataTable
                    VW_PIPE_ROUTINE_DETAIL.DefaultView.RowFilter = "ISSUE = 4"
                    DT = VW_PIPE_ROUTINE_DETAIL.DefaultView.ToTable
                    VW_PIPE_ROUTINE_DETAIL.DefaultView.RowFilter = ""
                    cc.Subreports("Non Success").SetDataSource(DT)
                    cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc.Close()
                    cc.Dispose()

                    BL.PushString(PDFList, TempName)
                Case "2_ROUTINE INSPECTION"
                    Dim DT As New DataTable
                    DT = JoinDataTable(VW_PIPE_ROUTINE_HEADER, VW_PIPE_ROUTINE_DETAIL, "RPT_No", "RPT_No")
                    cc.SetDataSource(DT)
                    cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc.Close()
                    cc.Dispose()

                    BL.PushString(PDFList, TempName)
                Case "3_TRACE SLOVE"
                    Dim DT As DataTable = JoinDataTable(VW_PIPE_ROUTINE_HEADER, VW_PIPE_ROUTINE_DETAIL, "RPT_No", "RPT_No")
                    DT.DefaultView.RowFilter = "CURRENT_LEVEL is not null or LAST_LEVEL is not null"
                    Dim DT_FILTER As DataTable = DT.DefaultView.ToTable
                    DT.DefaultView.RowFilter = ""
                    cc.SetDataSource(DT_FILTER)
                    cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc.Close()
                    cc.Dispose()

                    BL.PushString(PDFList, TempName)
                Case "5_ST_PHOTO"
                    SQL = " SELECT VW_PIPE_ROUTINE_HEADER.RPT_Year, VW_PIPE_ROUTINE_HEADER.RPT_No," & vbNewLine
                    SQL &= " PLANT_Code,ROUTE_Code, VW_PIPE_ROUTINE_HEADER.RPT_CODE," & vbNewLine
                    SQL &= " Officer_Collector, Created_Time,TAG_ID,TAG_CODE,INSP_ID," & vbNewLine
                    SQL &= " PROBLEM_DETAIL, PROBLEM_RECOMMENT, ISSUE, LEVEL_DESC, PICTURE1, PICTURE2," & vbNewLine
                    SQL &= " CASE WHEN ISNULL(CURRENT_PICTURE1,0) = 0 THEN" & vbNewLine
                    SQL &= "    CASE WHEN ISNULL(LAST_PICTURE1,0) = 1 THEN" & vbNewLine
                    SQL &= "        LAST_DETAIL_ID" & vbNewLine
                    SQL &= "    ELSE" & vbNewLine
                    SQL &= "        NULL" & vbNewLine
                    SQL &= "    END" & vbNewLine
                    SQL &= " ELSE" & vbNewLine
                    SQL &= "    DETAIL_ID" & vbNewLine
                    SQL &= " END as P1," & vbNewLine
                    SQL &= " CASE WHEN ISNULL(CURRENT_PICTURE2,0) = 0 THEN" & vbNewLine
                    SQL &= "    CASE WHEN ISNULL(LAST_PICTURE2,0) = 1 THEN" & vbNewLine
                    SQL &= "        LAST_DETAIL_ID" & vbNewLine
                    SQL &= "    ELSE" & vbNewLine
                    SQL &= "        NULL" & vbNewLine
                    SQL &= "    END" & vbNewLine
                    SQL &= " ELSE" & vbNewLine
                    SQL &= "    DETAIL_ID" & vbNewLine
                    SQL &= " END as P2" & vbNewLine
                    SQL &= " FROM   VW_PIPE_ROUTINE_HEADER LEFT JOIN VW_PIPE_ROUTINE_Detail" & vbNewLine
                    SQL &= " on VW_PIPE_ROUTINE_HEADER.RPT_Year = VW_PIPE_ROUTINE_Detail.RPT_Year" & vbNewLine
                    SQL &= " and VW_PIPE_ROUTINE_HEADER.RPT_No = VW_PIPE_ROUTINE_Detail.RPT_No" & vbNewLine
                    SQL &= " WHERE VW_PIPE_ROUTINE_HEADER.RPT_Year = " & RPT_Year & vbNewLine
                    SQL &= " And VW_PIPE_ROUTINE_HEADER.RPT_No = " & RPT_No & vbNewLine
                    SQL &= " AND (Last_Level IS NOT NULL OR CURRENT_LEVEL IS NOT NULL)" & vbNewLine
                    SQL &= " and ISSUE <> 0 and HasPICTURE = 1" & vbNewLine
                    SQL &= "order by ISSUE,TAG_CODE,INSP_ID,DETAIL_ID"

                    Dim DT As New DataTable
                    DA = New SqlDataAdapter(SQL, BL.ConnStr)
                    DA.Fill(DT)

                    Dim PIPE As New EIR_PIPE
                    If DT.Rows.Count > 0 Then
                        For x As Integer = 0 To DT.Rows.Count - 1
                            If DT.Rows(x).Item("P1").ToString <> "" Then
                                Dim Image As Byte() = PIPE.Get_ROUTINE_Image(RPT_Year, RPT_No, DT.Rows(x).Item("P1"), 1)
                                If Image IsNot Nothing Then
                                    DT.Rows(x).Item("PICTURE1") = ResizeImage(Image)
                                End If
                            End If
                            If DT.Rows(x).Item("P2").ToString <> "" Then
                                Dim Image As Byte() = PIPE.Get_ROUTINE_Image(RPT_Year, RPT_No, DT.Rows(x).Item("P2"), 2)
                                If Image IsNot Nothing Then
                                    DT.Rows(x).Item("PICTURE2") = ResizeImage(Image)
                                End If
                            End If
                        Next

                        cc.SetDataSource(DT)
                        cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                        cc.Close()
                        cc.Dispose()
                        BL.PushString(PDFList, TempName)
                    End If

            End Select
        Next

        Dim OutputFile As String = UNIQUE_ID & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "_" & RPT_No.ToString.PadLeft(4, "0") & ".pdf"
        BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
        Return ServerMapPath & "\Temp\" & OutputFile

    End Function

    Private Function ResizeImage(ByVal ImageData As Byte()) As Byte()
        On Error Resume Next

        Dim C As New Converter
        Dim fs As Stream = C.ByteToStream(ImageData)
        Dim MyImg = Image.FromStream(fs, True)
        fs.Close()
        fs = Nothing

        'Dim bm As New Bitmap(MyImg)
        'MyImg.Dispose()

        'Dim width As Integer = MyImg.Width
        'Dim height As Integer = MyImg.Height

        Dim width As Integer = 280 'image width. 
        Dim height As Integer = 190 'image height
        Dim thumb As New Bitmap(width, height)
        Dim g As Graphics = Graphics.FromImage(thumb)
        g.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
        g.DrawImage(MyImg, New Rectangle(0, 0, width, height), New Rectangle(0, 0, MyImg.Width, MyImg.Height), GraphicsUnit.Pixel)
        Dim B As Byte() = C.ImageToByte(thumb)
        g.Dispose()
        MyImg.Dispose()
        thumb.Dispose()
        Return B
    End Function

    Public Function JoinDataTable(ByVal Dt1 As System.Data.DataTable, ByVal Dt2 As System.Data.DataTable, ByVal argKey1 As String, ByVal argKey2 As String) As System.Data.DataTable
        Dim joinDt As New DataTable
        If Dt1.Rows.Count > 0 And Dt2.Rows.Count > 0 Then
            joinDt = Join.LeftJoin.Join(Dt1, Dt2, Dt1.Columns(argKey1), Dt2.Columns(argKey2))
        End If
        Return joinDt
    End Function

    Public Function Create_ST_TA_Daily_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, Optional ByVal param_Report As String = "", Optional ByVal Param_Daily As String = "") As String
        '------------------- Initiate UNIQUE_ID -------------------------
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        '----------------------------------------------------------------
        If RPT_Year = 0 Or RPT_No = 0 Then Return False

        Dim FolderName As String = "Turnaround_Daily_Report"

        Dim PDFList As String() = {}



        Dim lblRPT_Date_AsFound As String = ""
        Dim lblRPT_Date_AfterClean As String = ""
        Dim lblRPT_Date_NDE As String = ""
        Dim lblRPT_Date_Repair As String = ""
        Dim lblRPT_Date_AfterRepair As String = ""
        Dim lblRPT_Date_Final As String = ""

        Dim SQL As String = ""
        SQL &= "  					DECLARE @RPT_Year As integer = " & RPT_Year & "																																						" & vbNewLine
        SQL &= "  					DECLARE @RPT_No As integer = " & RPT_No & "																																							" & vbNewLine
        SQL &= "           			Select DISTINCT																																										" & vbNewLine
        SQL &= "      				Detail_Step.DETAIL_ID																																									" & vbNewLine
        SQL &= "      				--,Detail_Step.DETAIL_STEP_ID																																								" & vbNewLine
        SQL &= "      				,Detail_Step.TAG_ID																																										" & vbNewLine
        SQL &= "      				,Detail_Step.INSP_ID																																									" & vbNewLine
        SQL &= "      				,INSP.INSP_Name																																											" & vbNewLine
        SQL &= "      				,Detail_Step.INSP_STATUS_ID																																								" & vbNewLine
        SQL &= "      				,INSP.STATUS_Name																																										" & vbNewLine
        SQL &= "      				,Detail.Tag_Type_ID																																										" & vbNewLine
        SQL &= "      				,MS_ST_TAG_TYPE.TAG_TYPE_Name																																							" & vbNewLine
        SQL &= "      				,VW_ST_TA_TAG.TAG_CODE ,VW_ST_TA_TAG.TAG_Name																																			" & vbNewLine
        SQL &= "           			FROM																																												" & vbNewLine
        SQL &= "      				RPT_ST_TA_Detail_Step  AS Detail_Step  																																					" & vbNewLine
        SQL &= "      				LEFT JOIN 																																												" & vbNewLine
        SQL &= "      				 (                                                      																																" & vbNewLine
        SQL &= "                    	    Select MS_ST_TAG_Inspection.* , 'MS_ST_TAG_Inspection' To_Table ,MS_ST_Default_Inspection.INSP_Name       ,	MS_ST_Default_Inspection_Status.STATUS_Name       					" & vbNewLine
        SQL &= "                    	    FROM MS_ST_TAG_Inspection                                                                                        																" & vbNewLine
        SQL &= "                    	    LEFT JOIN MS_ST_Default_Inspection On MS_ST_Default_Inspection.INSP_ID = MS_ST_TAG_Inspection.INSP_ID            																" & vbNewLine
        SQL &= "                    	    LEFT JOIN MS_ST_Default_Inspection_Status On MS_ST_Default_Inspection_Status.STATUS_ID = MS_ST_TAG_Inspection.STATUS_ID          												" & vbNewLine
        SQL &= "                    	   																																													" & vbNewLine
        'SQL &= "      				    UNION ALL                                                                                                        																	" & vbNewLine
        'SQL &= "                          Select  MS_ST_TA_TAG_Inspection.* ,'MS_ST_TA_TAG_Inspection' To_Table ,MS_ST_TA_Default_Inspection.INSP_Name ,	MS_ST_TA_Default_Inspection_Status.STATUS_Name					" & vbNewLine
        'SQL &= "                    	    FROM MS_ST_TA_TAG_Inspection                                                                                     																" & vbNewLine
        'SQL &= "                    	    LEFT JOIN MS_ST_TA_Default_Inspection On MS_ST_TA_Default_Inspection.INSP_ID = MS_ST_TA_TAG_Inspection.INSP_ID   																" & vbNewLine
        'SQL &= "                    	    LEFT JOIN MS_ST_TA_Default_Inspection_Status On MS_ST_TA_Default_Inspection_Status.STATUS_ID = MS_ST_TA_TAG_Inspection.STATUS_ID          										" & vbNewLine
        SQL &= "                        ) AS INSP  																																										" & vbNewLine
        SQL &= "           			 ON  Detail_Step.INSP_ID = INSP.INSP_ID AND  Detail_Step.INSP_STATUS_ID = INSP.STATUS_ID 																							" & vbNewLine
        SQL &= "      				 LEFT JOIN RPT_ST_TA_DETAIL Detail ON  Detail.DETAIL_ID = Detail_Step.DETAIL_ID																											" & vbNewLine
        SQL &= "      				 LEFT JOIN MS_ST_TA_Step ON  MS_ST_TA_Step.STEP_ID = Detail_Step.STEP_ID																												" & vbNewLine
        SQL &= "      				 LEFT JOIN MS_ST_TAG_TYPE ON  MS_ST_TAG_TYPE.TAG_TYPE_ID = Detail.TAG_TYPE_ID																											" & vbNewLine
        SQL &= "      				 LEFT JOIN VW_ST_TA_TAG On VW_ST_TA_TAG.TAG_ID=Detail.TAG_ID And VW_ST_TA_TAG.TAG_TYPE_ID=Detail.TAG_TYPE_ID																			" & vbNewLine
        SQL &= "  																																																			" & vbNewLine
        SQL &= "      	WHERE 	Detail_Step.RPT_Year =@RPT_Year AND Detail_Step.RPT_No =@RPT_No																																" & vbNewLine

        If param_Report <> "" Then
            SQL &= param_Report
        End If


        SQL &= "         ORDER BY TAG_ID,INSP.INSP_Name,INSP.STATUS_Name, INSP_ID																																			" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT_List As New DataTable
        DA.Fill(DT_List)

        DT_List.Columns.Add("AsFound")
        DT_List.Columns.Add("AfterClean")
        DT_List.Columns.Add("NDE")
        DT_List.Columns.Add("Repair")
        DT_List.Columns.Add("AfterRepair")
        DT_List.Columns.Add("Final")

        DT_List.Columns.Add("Class_AsFound")
        DT_List.Columns.Add("Class_AfterClean")
        DT_List.Columns.Add("Class_NDE")
        DT_List.Columns.Add("Class_Repair")
        DT_List.Columns.Add("Class_AfterRepair")
        DT_List.Columns.Add("Class_Final")


        Dim DR As DataRow
        For j As Integer = 0 To DT_List.Rows.Count - 1
            DR = DT_List.Rows(j)
            Dim DT_Date_Step As DataTable = DT_Daily_Date(RPT_Year, RPT_No, DT_List.Rows(j).Item("TAG_ID"), DT_List.Rows(j).Item("INSP_ID"), DT_List.Rows(j).Item("INSP_STATUS_ID"), Param_Daily)
            DT_Date_Step.DefaultView.RowFilter = "1=1" & Param_Daily
            DT_Date_Step = DT_Date_Step.DefaultView.ToTable
            lblRPT_Date_AsFound = ""
            lblRPT_Date_AfterClean = ""
            lblRPT_Date_NDE = ""
            lblRPT_Date_Repair = ""
            lblRPT_Date_AfterRepair = ""
            lblRPT_Date_Final = ""

            If (DT_Date_Step.Rows.Count > 0) Then
                For i As Integer = 0 To DT_Date_Step.Rows.Count - 1

                    Select Case DT_Date_Step.Rows(i).Item("STEP_ID")
                        Case EIR_BL.ST_TA_STEP.As_Found
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_AsFound += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " "
                            End If
                        Case EIR_BL.ST_TA_STEP.After_Clean
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_AfterClean += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " "
                            End If
                        Case EIR_BL.ST_TA_STEP.NDE
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_NDE += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " "
                            End If
                        Case EIR_BL.ST_TA_STEP.Repair
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_Repair += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " "
                            End If
                        Case EIR_BL.ST_TA_STEP.After_Repair
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_AfterRepair += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " "
                            End If
                        Case EIR_BL.ST_TA_STEP.Final
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_Final += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                            End If

                    End Select


                Next

                DR("AsFound") = lblRPT_Date_AsFound
                DR("AfterClean") = lblRPT_Date_AfterClean
                DR("NDE") = lblRPT_Date_NDE
                DR("Repair") = lblRPT_Date_Repair
                DR("AfterRepair") = lblRPT_Date_AfterRepair
                DR("Final") = lblRPT_Date_Final

                '---Class--
                'DR("Class_AsFound")
                'DR("Class_AfterClean")
                'DR("Class_NDE")
                'DR("Class_Repair")
                'DR("Class_AfterRepair")
                'DR("Class_Final")


                For i As Integer = 1 To 6
                    DT_Date_Step.DefaultView.RowFilter = "STEP_ID=" & i & " AND STATUS_ID IS NOT NULL AND RPT_DATE IS NOT NULL "
                    Dim DT_RowFilter = DT_Date_Step.DefaultView.ToTable()
                    If (DT_Date_Step.DefaultView.Count > 0) Then

                        Dim Max_CLASS As Integer = DT_Date_Step.DefaultView(0).Item("STATUS_ID") '-- สถานะล่าสุด

                        Select Case i
                            Case EIR_BL.ST_TA_STEP.As_Found
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_AsFound") = "white"

                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_AsFound") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_AsFound") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_AsFound") = "red"
                                    End If
                                Else
                                    DR("Class_AsFound") = "white"
                                End If
                            Case EIR_BL.ST_TA_STEP.After_Clean
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_AfterClean") = "white"

                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_AfterClean") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_AfterClean") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_AfterClean") = "red"
                                    End If
                                Else
                                    DR("Class_AfterClean") = "white"
                                End If
                            Case EIR_BL.ST_TA_STEP.NDE
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_NDE") = "white"

                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_NDE") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_NDE") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_NDE") = "red"
                                    End If
                                Else
                                    DR("Class_NDE") = "white"
                                End If
                            Case EIR_BL.ST_TA_STEP.Repair
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_Repair") = "white"

                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_Repair") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_Repair") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_Repair") = "red"
                                    End If
                                Else
                                    DR("Class_Repair") = "white"
                                End If
                            Case EIR_BL.ST_TA_STEP.After_Repair
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_AfterRepair") = "white"

                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_AfterRepair") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_AfterRepair") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_AfterRepair") = "red"
                                    End If
                                Else
                                    DR("Class_AfterRepair") = "white"
                                End If
                            Case EIR_BL.ST_TA_STEP.Final
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_Final") = "white"
                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_Final") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_Final") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_Final") = "red"
                                    End If
                                Else
                                    DR("Class_Final") = "white"
                                End If

                        End Select


                    End If
                Next



            End If

        Next


        Dim ReportName() As String = {"1_ST_TA_LIST"}

        For i As Integer = 0 To ReportName.Length - 1

            Dim cc As New ReportDocument()
            Dim TempName As String = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & ReportName(i) & ".pdf"
            cc.Load(ServerMapPath & "\Report\" & FolderName & "\" & ReportName(i) & ".rpt")
            cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)

            Select Case ReportName(i).ToUpper
                Case "1_ST_TA_LIST"

                    cc.SetDataSource(DT_List)

                    Dim SQL_Hesder As String = ""
                    SQL_Hesder = " Select * From VW_REPORT_ST_TA_HEADER "
                    SQL_Hesder &= "  Where RPT_Year = " & RPT_Year & " And RPT_No = " & RPT_No & " "
                    Dim DT_Hesder As New DataTable
                    Dim DA_Hesder As SqlDataAdapter = New SqlDataAdapter(SQL_Hesder, BL.ConnStr)
                    DA_Hesder.Fill(DT_Hesder)


                    'cc.DataDefinition.FormulaFields("TAG_NO").Text = DT.Rows(0).Item("NewIssue").ToString
                    'cc.DataDefinition.FormulaFields("TAG_TYPE").Text = DT.Rows(0).Item("RPT_Type_Name").ToString
                    If (DT_Hesder.Rows.Count > 0) Then
                        cc.SetParameterValue("Created_Time", DT_Hesder.Rows(0).Item("Created_Time"))

                        cc.SetParameterValue("RPT_Year", RPT_Year)
                        cc.SetParameterValue("RPT_No", RPT_No)
                        cc.SetParameterValue("RPT_CODE", DT_Hesder.Rows(0).Item("RPT_CODE").ToString)

                        cc.SetParameterValue("Officer_Collector", DT_Hesder.Rows(0).Item("Officer_Collector").ToString)
                        cc.SetParameterValue("Officer_Inspector", DT_Hesder.Rows(0).Item("Officer_Inspector").ToString)
                        cc.SetParameterValue("Inspector_Position", DT_Hesder.Rows(0).Item("Inspector_Position").ToString)
                        cc.SetParameterValue("Officer_Engineer", DT_Hesder.Rows(0).Item("Officer_Engineer").ToString)
                        cc.SetParameterValue("Officer_Analyst", DT_Hesder.Rows(0).Item("Officer_Analyst").ToString)

                        cc.SetParameterValue("RPT_Type_Name", DT_Hesder.Rows(0).Item("RPT_Type_Name").ToString)
                        cc.SetParameterValue("PLANT_Code", DT_Hesder.Rows(0).Item("PLANT_Code").ToString)
                        cc.SetParameterValue("RPT_Subject", DT_Hesder.Rows(0).Item("RPT_Subject").ToString)
                        cc.SetParameterValue("RPT_To", DT_Hesder.Rows(0).Item("RPT_To").ToString)
                        cc.SetParameterValue("RPT_Cause", DT_Hesder.Rows(0).Item("RPT_Cause").ToString)
                    Else
                        cc.SetParameterValue("RPT_Year", RPT_Year)
                        cc.SetParameterValue("RPT_No", RPT_No)
                        cc.SetParameterValue("RPT_CODE", "")

                        cc.SetParameterValue("RPT_Type_Name", "")
                        cc.SetParameterValue("PLANT_Code", "")
                        cc.SetParameterValue("RPT_Subject", "")
                        cc.SetParameterValue("RPT_Cause", "")
                        cc.SetParameterValue("RPT_To", "")
                        cc.SetParameterValue("Officer_Collector", "")
                        cc.SetParameterValue("Officer_Inspector", "")
                        cc.SetParameterValue("Inspector_Position", "")
                        cc.SetParameterValue("Officer_Engineer", "")
                        cc.SetParameterValue("Officer_Analyst", "")
                        cc.SetParameterValue("Created_Time", DBNull.Value)

                    End If



                    cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                    cc.Close()
                    cc.Dispose()
                    BL.PushString(PDFList, TempName)

            End Select



        Next

        '-- รายละเอียดของ Sector--
        SQL = ""
        SQL &= " SELECT * FROM ( SELECT  ROW_NUMBER() OVER(ORDER BY RPT_ST_TA_Detail_Step.Tag_ID ,RPT_ST_TA_Detail_Step.INSP_ID ,  RPT_ST_TA_Detail_Step.INSP_STATUS_ID  ,RPT_ST_TA_Detail_Step.STEP_ID  ) AS Row , " & vbNewLine
        SQL &= " RPT_ST_TA_Detail_Step.*, VW_ST_TA_TAG.TAG_CODE ,VW_ST_TA_TAG.TAG_Name ,VW_ST_TA_TAG.TAG_TYPE_Name,MS_Plant.plant_Name,RPT_ST_TA_Detail_Sector.sector_id,Template_ID																	" & vbNewLine
        SQL &= " ,'INSP-E-' + Substring(convert(varchar,2017),3,4)  +  REPLACE(STR(56, 4), SPACE(1), '0')		 RPT_CODE	,MS_ST_Default_Inspection_Status.STATUS_Name																" & vbNewLine

        SQL &= " ,CASE WHEN ISNULL(MS_Template_Sector.PIC_Detail1, 0) = 0 THEN NULL ELSE CONVERT(image, '') END AS PIC_Detail1		" & vbNewLine
        SQL &= " ,CASE WHEN ISNULL(MS_Template_Sector.PIC_Detail2, 0) = 0 THEN NULL ELSE CONVERT(image, '') END AS PIC_Detail2		" & vbNewLine
        SQL &= " ,CASE WHEN ISNULL(MS_Template_Sector.PIC_Detail3, 0) = 0 THEN NULL ELSE CONVERT(image, '') END AS PIC_Detail3		" & vbNewLine
        SQL &= " ,CASE WHEN ISNULL(MS_Template_Sector.PIC_Detail4, 0) = 0 THEN NULL ELSE CONVERT(image, '') END AS PIC_Detail4		" & vbNewLine

        SQL &= " ,CASE WHEN ISNULL(MS_Template_Sector.PIC_Detail1,0) = 0 THEN NULL ELSE PIC_Detail1 END as P1					" & vbNewLine
        SQL &= " ,CASE WHEN ISNULL(MS_Template_Sector.PIC_Detail2,0) = 0 THEN NULL ELSE PIC_Detail2 END as P2					" & vbNewLine
        SQL &= " ,CASE WHEN ISNULL(MS_Template_Sector.PIC_Detail3,0) = 0 THEN NULL ELSE PIC_Detail3 END as P3					" & vbNewLine
        SQL &= " ,CASE WHEN ISNULL(MS_Template_Sector.PIC_Detail4,0) = 0 THEN NULL ELSE PIC_Detail4 END as P4					" & vbNewLine
        SQL &= " ,Text_1,Text_2,Text_3,Text_4,Text_5,Text_6,Text_7																" & vbNewLine
        SQL &= " FROM   RPT_ST_TA_Detail_Step               " & vbNewLine

        SQL &= " Left Join RPT_ST_TA_Detail_Sector ON RPT_ST_TA_Detail_Step.DETAIL_STEP_ID= RPT_ST_TA_Detail_Sector.DETAIL_STEP_ID              " & vbNewLine
        SQL &= " Left Join MS_Template_Sector On RPT_ST_TA_Detail_Sector.Sector_ID = MS_Template_Sector.Sector_ID 				                    " & vbNewLine
        SQL &= " Left Join VW_ST_TA_TAG On VW_ST_TA_TAG.Tag_id=	RPT_ST_TA_Detail_Step.Tag_id	                                                    " & vbNewLine
        SQL &= " Left Join MS_Plant On 		MS_Plant.PLANT_ID =		VW_ST_TA_TAG.PLANT_ID	                                                        " & vbNewLine
        SQL &= " Left Join MS_ST_Default_Inspection ON MS_ST_Default_Inspection.INSP_ID=RPT_ST_TA_Detail_Step.INSP_ID                                " & vbNewLine
        SQL &= " Left Join 	MS_ST_Default_Inspection_Status On MS_ST_Default_Inspection_Status.STATUS_ID = RPT_ST_TA_Detail_Step.INSP_STATUS_ID	    " & vbNewLine

        SQL &= " where RPT_ST_TA_Detail_Step.RPT_Year = " & RPT_Year & " And RPT_ST_TA_Detail_Step.RPT_NO = " & RPT_No & " And RPT_DATE Is Not NULL                          " & vbNewLine
        SQL &= " ) AS TB " & vbNewLine
        SQL &= " WHERE 1=1 " & vbNewLine

        If Param_Daily <> "" Then
            SQL &= Param_Daily
        End If
        SQL &= " ORDER BY Tag_ID ,INSP_ID ,  INSP_STATUS_ID                                                     " & vbNewLine

        Dim DT As New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)


        If DT.Rows.Count > 0 Then


            Last_DETAIL_STEP_ID = DT.Rows(0).Item("DETAIL_STEP_ID").ToString()
            For i As Integer = 0 To DT.Rows.Count - 1

                Dim ReportName_Sector As String = "3_ST_PHOTO_0" & DT.Rows(i).Item("Template_ID").ToString()

                If Not IsDBNull(DT.Rows(i).Item("Template_ID")) Then
                    ReportName_Sector = "3_ST_PHOTO_0" & DT.Rows(i).Item("Template_ID").ToString()
                Else
                    ReportName_Sector = "3_ST_PHOTO_Null"
                End If

                Dim cc As New ReportDocument()
                Dim TempName As String = ServerMapPath & "\Temp\" & i & "_" & UNIQUE_ID & "_" & ReportName_Sector & ".pdf"
                cc.Load(ServerMapPath & "\Report\" & FolderName & "\" & ReportName_Sector & ".rpt")
                cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)


                If DT.Rows(i).Item("P1").ToString <> "" Then
                    Dim Image As Byte() = BL.Get_ST_TA_As_Found_Image_Template(RPT_Year, RPT_No, DT.Rows(i).Item("Sector_ID"), 1)
                    If Image IsNot Nothing Then
                        DT.Rows(i).Item("PIC_Detail1") = ResizeImage(Image)
                    End If
                End If
                If DT.Rows(i).Item("P2").ToString <> "" Then
                    Dim Image As Byte() = BL.Get_ST_TA_As_Found_Image_Template(RPT_Year, RPT_No, DT.Rows(i).Item("Sector_ID"), 2)
                    If Image IsNot Nothing Then
                        DT.Rows(i).Item("PIC_Detail2") = ResizeImage(Image)
                    End If
                End If

                If DT.Rows(i).Item("P3").ToString <> "" Then
                    Dim Image As Byte() = BL.Get_ST_TA_As_Found_Image_Template(RPT_Year, RPT_No, DT.Rows(i).Item("Sector_ID"), 3)

                    If Image IsNot Nothing Then
                        DT.Rows(i).Item("PIC_Detail3") = ResizeImage(Image)
                    End If
                End If

                If DT.Rows(i).Item("P4").ToString <> "" Then
                    Dim Image As Byte() = BL.Get_ST_TA_As_Found_Image_Template(RPT_Year, RPT_No, DT.Rows(i).Item("Sector_ID"), 4)

                    If Image IsNot Nothing Then
                        DT.Rows(i).Item("PIC_Detail4") = ResizeImage(Image)
                    End If
                End If


                DT.DefaultView.RowFilter = "Row=" & DT.Rows(i).Item("Row")
                Dim DT_Sector As New DataTable
                DT_Sector = DT.DefaultView.ToTable
                cc.SetDataSource(DT_Sector)

                cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                cc.Close()
                cc.Dispose()
                BL.PushString(PDFList, TempName)

                '============================FOOTER========================================
                If (i + 1 <= DT.Rows.Count) Then
                    Dim Next_DETAIL_STEP_ID As String = ""
                    If (i + 1 = DT.Rows.Count) Then
                        Next_DETAIL_STEP_ID = DT.Rows(i).Item("DETAIL_STEP_ID") + 1
                    Else
                        Next_DETAIL_STEP_ID = DT.Rows(i + 1).Item("DETAIL_STEP_ID").ToString()
                    End If

                    If Last_DETAIL_STEP_ID <> Next_DETAIL_STEP_ID Then
                        Last_DETAIL_STEP_ID = Next_DETAIL_STEP_ID

                        cc = New ReportDocument()
                        TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & "Footer_" & DT.Rows(i).Item("DETAIL_STEP_ID") & ".pdf"
                        cc.Load(ServerMapPath & "\Report\" & FolderName & "\4_ST_FOOTER.rpt")
                        cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)


                        SQL = "  Select DISTINCT VW_REPORT_ST_TA_HEADER.RPT_CODE , RPT_ST_TA_Detail_Step.*, VW_ST_TA_TAG.TAG_CODE ,VW_ST_TA_TAG.TAG_Name ,MS_Plant.plant_Name                    	" & vbNewLine
                        SQL &= " ,VW_ST_TA_TAG.TAG_TYPE_Name                                                                                                               	" & vbNewLine
                        SQL &= "  ,MS_ST_Default_Inspection_Status.STATUS_Name                                                                                             	" & vbNewLine
                        SQL &= " 	, Condition_Problem                                                                                                                     	" & vbNewLine
                        SQL &= "      , Possible_Cause                                                                                                                     	" & vbNewLine
                        SQL &= "      ,RPT_ST_TA_Detail_Step.Officer_Collector ,RPT_ST_TA_Detail_Step.Officer_Inspector  ,RPT_ST_TA_Detail_Step.Officer_Engineer ,RPT_ST_TA_Detail_Step.Officer_Analyst														                                                            	" & vbNewLine
                        SQL &= " From RPT_ST_TA_Detail_Step                                                                                                                	" & vbNewLine
                        SQL &= " Left Join VW_ST_TA_TAG ON VW_ST_TA_TAG.Tag_id=	RPT_ST_TA_Detail_Step.Tag_id			                                                	" & vbNewLine
                        SQL &= " Left Join MS_Plant ON 		MS_Plant.PLANT_ID =		VW_ST_TA_TAG.PLANT_ID	                                                            	" & vbNewLine
                        SQL &= " Left Join 	MS_ST_Default_Inspection_Status ON MS_ST_Default_Inspection_Status.STATUS_ID = RPT_ST_TA_Detail_Step.INSP_STATUS_ID         	" & vbNewLine
                        SQL &= " LEFT JOIN VW_REPORT_ST_TA_HEADER ON VW_REPORT_ST_TA_HEADER.RPT_Year=RPT_ST_TA_Detail_Step.RPT_Year AND VW_REPORT_ST_TA_HEADER.RPT_NO=RPT_ST_TA_Detail_Step.RPT_NO         	" & vbNewLine

                        SQL &= "  WHERE RPT_ST_TA_Detail_Step.RPT_Year = " & RPT_Year & " And RPT_ST_TA_Detail_Step.RPT_No = " & RPT_No & " And RPT_ST_TA_Detail_Step.DETAIL_STEP_ID = " & DT.Rows(i).Item("DETAIL_STEP_ID") & "          	" & vbNewLine
                        Dim DT_Comment As New DataTable
                        DA = New SqlDataAdapter(SQL, BL.ConnStr)
                        DA.Fill(DT_Comment)
                        cc.SetDataSource(DT_Comment)

                        cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                        cc.Close()
                        cc.Dispose()
                        BL.PushString(PDFList, TempName)



                        '-----ถ้า Step NDE 4 แสดงรายงาน CUI
                        '============================CUI========================================
                        If DT_Sector.Rows.Count > 0 Then
                            If DT_Sector.Rows(0).Item("STEP_ID") = 3 Then

                                cc = New ReportDocument()
                                TempName = ServerMapPath & "\Temp\" & UNIQUE_ID & "_" & "CUI_Short_" & DT.Rows(i).Item("TAG_ID") & "_" & i & ".pdf"
                                cc.Load(ServerMapPath & "\Report\" & FolderName & "\5_CUI_Short.rpt")
                                cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                                Dim SQL_Main As String = ""

                                SQL_Main &= "   Select   DISTINCT  MS_ST_TAG_UTM.TAG_UTM_ID , MS_ST_TAG_UTM.TAG_ID , Location_Name, X_Start , X_End , Point_Code , Point_number , Y_Template  " & vbLf
                                SQL_Main &= "   , Y_Template_Name , Active_Status    " & vbLf
                                SQL_Main &= "   ,MS_ST_Default_Inspection.INSP_Name, MS_ST_Default_Inspection_Status.STATUS_Name,RPT_ST_TA_Corrosion_Remaining.Use_Term  " & vbLf
                                SQL_Main &= "     From MS_ST_TAG_UTM   " & vbLf
                                SQL_Main &= "    Left Join RPT_ST_TA_Detail_Step On MS_ST_TAG_UTM.TAG_ID=RPT_ST_TA_Detail_Step.TAG_ID " & vbLf
                                SQL_Main &= "    Left Join MS_ST_Default_Inspection On MS_ST_Default_Inspection.INSP_ID=RPT_ST_TA_Detail_Step.INSP_ID  " & vbLf
                                SQL_Main &= "    Left Join 	MS_ST_Default_Inspection_Status On MS_ST_Default_Inspection_Status.STATUS_ID = RPT_ST_TA_Detail_Step.INSP_STATUS_ID	 " & vbLf
                                SQL_Main &= "    LEFT JOIN RPT_ST_TA_Corrosion_Remaining ON RPT_ST_TA_Corrosion_Remaining.TAG_UTM_ID =MS_ST_TAG_UTM.TAG_UTM_ID	 " & vbLf
                                SQL_Main &= "   WHERE RPT_ST_TA_Detail_Step.TAG_ID=" & DT.Rows(i).Item("TAG_ID")
                                SQL_Main &= "  And RPT_ST_TA_Detail_Step.RPT_YEAR =" & RPT_Year & " And RPT_ST_TA_Detail_Step.RPT_NO=" & RPT_No & " " & vbLf
                                SQL_Main &= " And RPT_ST_TA_Detail_Step.INSP_ID=" & DT.Rows(i).Item("INSP_ID") & " And RPT_ST_TA_Detail_Step.INSP_STATUS_ID=" & DT.Rows(i).Item("INSP_STATUS_ID") & " " & vbLf

                                Dim DA_Main As New SqlDataAdapter(SQL_Main, BL.ConnStr)
                                Dim DT_Main As New DataTable
                                DA_Main.Fill(DT_Main)

                                Dim SQL_UTM As String = ""
                                SQL_UTM &= "  SELECT * FROM  RPT_ST_TA_Corrosion_Remaining  WHERE RPT_YEAR =" & RPT_Year & " AND RPT_NO=" & RPT_No & " "
                                Dim DA_UTM As New SqlDataAdapter(SQL_UTM, BL.ConnStr)
                                Dim DT_UTM As New DataTable
                                DA_UTM.Fill(DT_UTM)

                                Dim SQL_Point As String = ""
                                SQL_Point &= "  SELECT * FROM RPT_ST_TA_CUI_POINT WHERE RPT_YEAR =" & RPT_Year & " AND RPT_NO=" & RPT_No & " "

                                Dim DA_Point As New SqlDataAdapter(SQL_Point, BL.ConnStr)
                                Dim DT_Point As New DataTable
                                DA_Point.Fill(DT_Point)
                                If DT_Main.Rows.Count > 0 Then
                                    Try

                                        cc.SetDataSource(DT_Main)
                                        cc.Subreports("UTM A-D").SetDataSource(DT_Point)
                                        cc.Subreports("UTM A-H").SetDataSource(DT_Point)
                                        cc.Subreports("CUI_Short_SubReport").SetDataSource(DT_UTM)
                                        cc.Subreports("CUI_Long_SubReport").SetDataSource(DT_UTM)

                                        cc.SetParameterValue("Created_Time", DT.Rows(0).Item("Created_Time"))
                                        cc.SetParameterValue("RPT_CODE", DT.Rows(0).Item("RPT_CODE").ToString)
                                        cc.SetParameterValue("TAG_CODE", DT.Rows(0).Item("TAG_CODE").ToString)
                                        cc.SetParameterValue("TAG_NAME", DT.Rows(0).Item("TAG_NAME").ToString)
                                        cc.SetParameterValue("Plant_Name", DT.Rows(0).Item("Plant_Name").ToString)


                                        cc.ExportToDisk(ExportFormatType.PortableDocFormat, TempName)
                                        cc.Close()
                                        cc.Dispose()
                                        BL.PushString(PDFList, TempName)

                                    Catch ex As Exception

                                    End Try
                                End If
                            End If
                        End If

                    End If





                End If
                ''=======================================

            Next

        End If







        Dim OutputFile As String = UNIQUE_ID & "_INSP-E-" & RPT_Year.ToString.Substring(2, 2) & "_" & RPT_No.ToString.PadLeft(4, "0") & ".pdf"
        BL.MergePDF(PDFList, ServerMapPath & "\Temp\" & OutputFile)
        Return ServerMapPath & "\Temp\" & OutputFile

    End Function

    Dim Last_DETAIL_STEP_ID As String = ""


    Function DT_Daily_Date(ByRef _RPT_Year As Integer, ByRef _RPT_No As Integer, ByRef _TAG_ID As Integer, ByRef _INSP_ID As Integer, ByRef _INSP_STATUS_ID As Integer, Optional ByVal Param_Daily As String = "") As DataTable
        Dim Sql As String = ""
        Sql &= "   DECLARE @RPT_Year As integer = " & _RPT_Year & "									" & vbNewLine
        Sql &= "   DECLARE @RPT_No As integer = " & _RPT_No & "										" & vbNewLine
        Sql &= "   DECLARE @TAG_ID As integer = " & _TAG_ID & "										" & vbNewLine
        Sql &= "   DECLARE @INSP_ID As integer = " & _INSP_ID & "										" & vbNewLine
        Sql &= "   DECLARE @INSP_STATUS_ID As integer = " & _INSP_STATUS_ID & "								" & vbNewLine
        Sql &= "   SELECT * FROM ( SELECT DISTINCT  DETAIL_STEP_ID ,													" & vbNewLine
        Sql &= "          STEP_ID														" & vbNewLine
        Sql &= "         ,RPT_DATE														" & vbNewLine
        Sql &= "         ,STATUS_ID														" & vbNewLine
        Sql &= "         ,MAX(STATUS_ID) MAX_CLASS,INSP_ID	,INSP_STATUS_ID											" & vbNewLine
        Sql &= "     FROM RPT_ST_TA_Detail_Step											" & vbNewLine
        Sql &= "     WHERE RPT_Year = @RPT_Year AND RPT_No = @RPT_No					" & vbNewLine
        Sql &= "     AND TAG_ID=@TAG_ID													" & vbNewLine
        Sql &= "     AND INSP_ID =@INSP_ID												" & vbNewLine
        Sql &= "     AND INSP_STATUS_ID= @INSP_STATUS_ID								" & vbNewLine

        Sql &= "     GROUP BY STEP_ID  ,RPT_DATE ,STATUS_ID	, DETAIL_STEP_ID,INSP_ID	,INSP_STATUS_ID								" & vbNewLine
        Sql &= "     )	TB								" & vbNewLine
        Sql &= "     WHERE 1=1								" & vbNewLine

        If Param_Daily <> "" Then
            Sql &= Param_Daily
        End If

        Sql &= "     ORDER BY RPT_DATE 	DESC						" & vbNewLine

        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)


        Return DT
    End Function



End Class
