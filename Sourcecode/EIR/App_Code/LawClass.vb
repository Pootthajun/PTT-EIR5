﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
'Imports Ionic.Zip


Public Class LawClass

    Dim BL As New EIR_BL


    Public ReadOnly Property UploadTempPath() As String
        Get
            Dim ret As String = ConfigurationManager.AppSettings.Item("ServerMapPath").ToString

            If ret.EndsWith("\") = False Then
                ret += "\Temp\"
            End If

            If Directory.Exists(ret) = False Then
                Directory.CreateDirectory(ret)
            End If

            Return ret
        End Get
    End Property
    Public ReadOnly Property LawDocumentPath As String
        Get
            Dim ret As String = ConfigurationManager.AppSettings.Item("LawDocumentPath").ToString
            If ret.EndsWith("\") = False Then
                ret = ret & "\"
            End If
            Return ret
        End Get
    End Property

    Public ReadOnly Property LawDocumentDownloadURL As String
        Get
            Return ConfigurationManager.AppSettings.Item("LawDocumentDownloadURL").ToString
        End Get
    End Property

#Region "Folder"

    Public Function GetRootFolder() As DataTable
        Dim RootFolderID As Integer = 1

        Dim sql As String = "select f.folder_id, f.folder_name, f.folder_description, f.parent_id,"
        sql += " (select count(folder_id) from LAW_Folder where parent_id=f.folder_id and folder_id<>" & RootFolderID & ") child_node"
        sql += " from LAW_Folder f"
        sql += " where f.folder_id=" & RootFolderID

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        Return dt
    End Function

    Private Function GetFolderPathDataTable(FolderID As Integer) As DataTable
        Dim RootFolderID As Integer = 1
        Dim sql As String = " with ParentFolder as " & Environment.NewLine
        sql += " ("
        sql += "	Select f.folder_id, f.folder_name, f.folder_description, f.parent_id" & Environment.NewLine
        sql += "	from LAW_Folder f" & Environment.NewLine
        sql += "	where f.folder_id=" & FolderID & Environment.NewLine
        sql += "	union all" & Environment.NewLine
        sql += "	Select f.folder_id, f.folder_name, f.folder_description, f.parent_id" & Environment.NewLine
        sql += "	from LAW_Folder f" & Environment.NewLine
        sql += "	inner join ParentFolder p on f.folder_id=p.parent_id" & Environment.NewLine
        sql += "	where f.folder_id<>" & RootFolderID & Environment.NewLine
        sql += "	)" & Environment.NewLine
        sql += "Select folder_id, folder_name, folder_description, parent_id from ParentFolder " & Environment.NewLine

        If FolderID <> RootFolderID Then
            sql += "union all " & Environment.NewLine
            sql += " Select folder_id, folder_name, folder_description, parent_id " & Environment.NewLine
            sql += " from LAW_Folder " & Environment.NewLine
            sql += " where folder_id=" & RootFolderID
        End If

        sql += " order by folder_id"

        Dim dt As DataTable = BL.Execute_DataTable(sql)

        Return dt
    End Function

    Public Function GetFolderPath(FolderID As Integer) As String
        Dim ret As String = ""
        Dim dt As DataTable = GetFolderPathDataTable(FolderID)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                ret += dr("folder_name") & "/"
            Next
        End If

        Return ret
    End Function

    Public Function GetFolderDetail(FolderID As Integer) As DataTable
        Dim sql As String = "select f.folder_id, f.folder_name, f.folder_description, f.parent_id, "
        sql += " (select count(folder_id) from LAW_Folder where parent_id=f.folder_id ) + " & vbNewLine
        sql += " (select count(document_id) from LAW_Document where folder_id=f.folder_id) child_node " & vbNewLine
        sql += " from LAW_Folder f"
        sql += " where f.folder_id=" & FolderID

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        Return dt
    End Function

    Public Function GetChildNode(ParentID As Integer) As DataTable
        Dim sql As String = "select f.folder_id, f.folder_name, f.folder_description, f.parent_id," & vbNewLine
        sql += " (select count(folder_id) from LAW_Folder where parent_id=f.folder_id ) + " & vbNewLine
        sql += " (select count(document_id) from LAW_Document where folder_id=f.folder_id) child_node," & vbNewLine
        sql += " '" & TreeviewNodeType.FolderMenu.ToString & "' node_type, '' file_ext, '' original_file_name, '' document_path," & vbNewLine
        sql += " '' full_name, "
        sql += " null notice_date, null critical_date, null actual_date, '" & TreeviewNodeType.FolderMenu & "' group_order "
        sql += " from LAW_Folder f" & vbNewLine
        sql += " where f.parent_id=" & ParentID & vbNewLine
        sql += " union all" & vbNewLine
        sql += " select f.document_id, f.document_name, f.document_description, f.folder_id parent_id, 0 child_node, " & vbNewLine
        sql += " '" & TreeviewNodeType.DocumentMenu.ToString & "' node_type, f.file_ext, f.original_file_name, f.document_path, " & vbNewLine
        sql += " f.document_path + convert(varchar,f.document_id) + f.file_ext full_name,"
        sql += " f.notice_date, f.critical_date, f.actual_date, '" & TreeviewNodeType.DocumentMenu & "' group_order"
        sql += " from LAW_Document f " & vbNewLine
        sql += " where f.folder_id=" & ParentID
        sql += " order by group_order, folder_name "

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        Return dt
    End Function

    Public Function GetAllNodeFolder() As DataTable
        Dim sql As String = "select folder_id node_id, folder_name node_name, parent_id "
        sql += " from law_folder "
        sql += " where folder_id<>0"

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        Return dt
    End Function

    Public Function SaveFolder(FolderID As Integer, FolderName As String, FolderDesc As String, ParentID As Integer, UpdateBy As Integer) As String
        Dim ret As String = "false"
        Dim sql As String = "select folder_id from law_folder where folder_name='" & FolderName.Replace("'", "''") & "' and parent_id=" & ParentID & " and folder_id<>" & FolderID
        Dim dt As DataTable = BL.Execute_DataTable(sql)
        If dt.Rows.Count > 0 Then
            ret = "false|Folder Name is already exists"
            Return ret
        End If

        If FolderID > 0 Then
            sql = "update law_folder "
            sql += " set folder_name='" & FolderName.Replace("'", "''") & "'"
            sql += ", folder_description='" & FolderDesc.Replace("'", "''") & "'"
            sql += ", parent_id=" & ParentID
            sql += ", update_time=getdate()"
            sql += ", update_by= " & UpdateBy
            sql += " output inserted.folder_id"
            sql += " where folder_id=" & FolderID
        Else
            sql = "insert into law_folder (folder_name, folder_description, parent_id," & UpdateBy & ")"
            sql += " output inserted.folder_id"
            sql += " values('" & FolderName.Replace("'", "''") & "', '" & FolderDesc.Replace("'", "''") & "', " & ParentID & ", " & UpdateBy & ")"
        End If

        dt = BL.Execute_DataTable(sql)
        If dt.Rows.Count > 0 Then
            FolderID = Convert.ToInt16(dt.Rows(0)("folder_id"))
            ret = (FolderID > 0).ToString.ToLower & "|" & FolderID
        End If

        Return ret
    End Function

    Public Function DeleteFolder(FolderID As Integer) As String
        Dim ret As String = "false"
        Dim dt As New DataTable
        dt.Columns.Add("node_id")   'Folder Node or Document Node
        dt.Columns.Add("node_name")
        dt.Columns.Add("node_desc")
        dt.Columns.Add("node_type")
        dt.Columns.Add("node_level")
        dt.Columns.Add("parent_id")

        dt = BuiltChildNodeDataTable(dt, FolderID, 1)
        dt.DefaultView.Sort = "node_level desc"

        Dim sql As String = ""

        Dim nlDt As New DataTable
        nlDt = dt.DefaultView.ToTable(True, "node_level")
        For Each nlDr As DataRow In nlDt.Rows
            'Delete Document
            dt.DefaultView.RowFilter = "node_level=" & nlDr("node_level") & " and node_type = '" & TreeviewNodeType.DocumentMenu.ToString & " '"
            Dim dDt As New DataTable
            dDt = dt.DefaultView.ToTable()
            If dDt.Rows.Count > 0 Then
                For Each dDr As DataRow In dDt.Rows
                    DeleteFile(dDr("node_id"))

                    sql = "delete from law_document where document_id='" & dDr("node_id") & "'"
                    BL.Execute_Command(sql)
                Next
            End If
            dDt.Dispose()
            dt.DefaultView.RowFilter = ""

            'Delete Folder
            dt.DefaultView.RowFilter = "node_level=" & nlDr("node_level") & " and node_type = '" & TreeviewNodeType.FolderMenu.ToString & " '"
            If dt.DefaultView.Count > 0 Then
                For Each drv As DataRowView In dt.DefaultView
                    sql = "delete from law_folder where folder_id='" & drv("node_id") & "'"
                    BL.Execute_Command(sql)
                Next
            End If
            dt.DefaultView.RowFilter = ""
        Next
        nlDt.Dispose()

        sql = "delete from law_folder where folder_id= '" & FolderID & "'"
        BL.Execute_Command(sql)

        ret = "true"

        Return ret
    End Function


    Private Function BuiltChildNodeDataTable(dt As DataTable, ParentID As Integer, NodeLevel As Integer) As DataTable
        Dim cDt As DataTable = GetChildNode(ParentID)
        If cDt.Rows.Count > 0 Then
            For Each cDr As DataRow In cDt.Rows
                Dim dr As DataRow = dt.NewRow
                dr("node_id") = cDr("folder_id")
                dr("node_name") = cDr("folder_name")
                dr("node_desc") = cDr("folder_description")
                dr("node_type") = cDr("node_type")
                dr("node_level") = NodeLevel
                dr("parent_id") = ParentID
                dt.Rows.Add(dr)

                dt = BuiltChildNodeDataTable(dt, cDr("folder_id"), NodeLevel + 1)
            Next
        End If
        cDt.Dispose()

        Return dt
    End Function



    Private Function BuiltDateChildData(dt As DataTable, ParentID As Integer) As DataTable
        Dim cDt As DataTable = GetChildNode(ParentID)
        If cDt.Rows.Count > 0 Then
            For Each cDr As DataRow In cDt.Rows
                Dim dr As DataRow = dt.NewRow
                dr("node_id") = cDr("folder_id")
                dr("node_name") = cDr("folder_name")
                dr("node_desc") = cDr("folder_description")
                dr("node_type") = cDr("node_type")
                dr("parent_id") = ParentID
                dr("notice_date") = cDr("notice_date")
                dr("critical_date") = cDr("critical_date")
                dr("actual_date") = cDr("actual_date")
                dr("full_name") = cDr("full_name")
                dt.Rows.Add(dr)

                dt = BuiltDateChildData(dt, cDr("folder_id"))
            Next
        End If

        Return dt
    End Function

    Public Function GetFolderNodeDateData(FolderID As Integer) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("node_id", GetType(Integer))
        dt.Columns.Add("node_name")
        dt.Columns.Add("node_desc")
        dt.Columns.Add("node_type")
        dt.Columns.Add("parent_id", GetType(Integer))
        dt.Columns.Add("notice_date", GetType(Date))
        dt.Columns.Add("critical_date", GetType(Date))
        dt.Columns.Add("actual_date", GetType(Date))
        dt.Columns.Add("full_name")
        dt = BuiltDateChildData(dt, FolderID)

        Dim ret As New DataTable

        If dt.Rows.Count > 0 Then
            dt.DefaultView.RowFilter = "node_type = '" & TreeviewNodeType.DocumentMenu.ToString & "'"
            If dt.DefaultView.Count > 0 Then
                Dim NoticeDate As Date = Nothing
                Dim CriticalDate As Date = Nothing
                Dim ActualDate As Date = Nothing
                Dim TempCheckComplete As String = ""

                For Each drv As DataRowView In dt.DefaultView
                    'Check Blank File
                    If Convert.IsDBNull(drv("full_name")) = True OrElse File.Exists(drv("full_name")) = False Then
                        'ถ้ายังไม่มีไฟล์ แล้วถึงตรวจสอบตามวันที่
                        If Convert.IsDBNull(drv("notice_date")) = False Then
                            If NoticeDate >= Convert.ToDateTime(drv("notice_date")) Then
                                NoticeDate = Convert.ToDateTime(drv("notice_date"))
                            ElseIf NoticeDate.Year = 1 Then
                                NoticeDate = Convert.ToDateTime(drv("notice_date"))
                            End If
                        End If

                        If Convert.IsDBNull(drv("critical_date")) = False Then
                            If CriticalDate >= Convert.ToDateTime(drv("critical_date")) Then
                                CriticalDate = Convert.ToDateTime(drv("critical_date"))
                            ElseIf CriticalDate.Year = 1 Then
                                CriticalDate = Convert.ToDateTime(drv("critical_date"))
                            End If
                        End If
                        TempCheckComplete += "N, "
                    Else
                        'ถ้ามีไฟล์แล้ว
                        TempCheckComplete += "Y, "
                    End If


                    If Convert.IsDBNull(drv("actual_date")) = False Then
                        If ActualDate >= Convert.ToDateTime(drv("actual_date")) Then
                            ActualDate = Convert.ToDateTime(drv("actual_date"))
                        ElseIf ActualDate.Year = 1 Then
                            ActualDate = Convert.ToDateTime(drv("actual_date"))
                        End If
                    End If
                Next

                ret.Columns.Add("notice_date", GetType(Date))
                ret.Columns.Add("critical_date", GetType(Date))
                ret.Columns.Add("actual_date", GetType(Date))
                ret.Columns.Add("is_complete")

                Dim dr As DataRow = ret.NewRow
                dr("notice_date") = NoticeDate
                dr("critical_date") = CriticalDate
                dr("actual_date") = ActualDate
                dr("is_complete") = IIf(TempCheckComplete.IndexOf("N") > -1, "N", "Y")

                ret.Rows.Add(dr)
            End If
            dt.DefaultView.RowFilter = ""
        End If
        dt.Dispose()

        Return ret
    End Function



    Public Function CopyFolder(FromFolderID As Integer, ToParentFolderID As Integer, UpdateBy As Integer, CopyFile As Boolean) As String
        Dim ret As String = "false"

        Dim FromDt As DataTable = GetFolderDetail(FromFolderID)
        If FromDt.Rows.Count = 0 Then
            Return "false|Folder Not Found"
        End If

        Dim sql As String = ""
        Dim FromDr As DataRow = FromDt.Rows(0)

        'ถ้า Copy ไปยัง Folder เดียวกัน ให้ชื่อมันต่อท้ายด้วย _Copy
        Dim NewFolderName As String = FromDr("folder_name") & IIf(Convert.ToInt32(FromDr("parent_id")) = ToParentFolderID, "_Copy", "")
        Dim IsDup As Boolean = False
        Do
            'ตรวจสอบชื่อ Folder ซ้ำ
            sql = "select folder_id from law_folder "
            sql += " where folder_name='" & NewFolderName & "' and parent_id= " & ToParentFolderID
            Dim tmp As DataTable = BL.Execute_DataTable(sql)
            If tmp.Rows.Count > 0 Then
                IsDup = True
                NewFolderName += "_Copy"
            Else
                IsDup = False
            End If
            tmp.Dispose()
        Loop While IsDup = True

        sql = "insert into law_folder (folder_name, folder_description, parent_id,update_by)"
        sql += " output inserted.folder_id"
        sql += " values('" & NewFolderName & "','" & FromDr("folder_description") & "'," & ToParentFolderID & "," & UpdateBy & ")"

        Dim FolderID As Integer = 0
        Dim fDt As DataTable = BL.Execute_DataTable(sql)
        If fDt.Rows.Count > 0 Then
            FolderID = Convert.ToInt16(fDt.Rows(0)("folder_id"))
            ret = CopyChildNode(FromFolderID, FolderID, UpdateBy, CopyFile)

            If ret.ToLower = "true" Then
                ret += "|" & FolderID
            End If
        End If

        Return ret
    End Function

    Private Function CopyChildNode(FromFolderID As Integer, ToFolderID As Integer, UpdateBy As Integer, CopyFile As Boolean) As String
        Dim ret As String = "false"

        Dim dt As DataTable = GetChildNode(FromFolderID)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim Sql As String = ""
                If dr("node_type") = TreeviewNodeType.FolderMenu.ToString Then
                    Sql = "insert into law_folder (folder_name, folder_description, parent_id,update_by)"
                    Sql += " output inserted.folder_id"
                    Sql += " values('" & dr("folder_name") & "','" & dr("folder_description") & "'," & ToFolderID & "," & UpdateBy & ")"

                    Dim fDt As DataTable = BL.Execute_DataTable(Sql)
                    If fDt.Rows.Count > 0 Then
                        Dim FolderID As Integer = Convert.ToInt16(fDt.Rows(0)("folder_id"))   'ค่านี้จะเป็น Parent ID สำหรับ Folder ใหม่ ซึ่งจะเปลี่ยนไปเรื่อยๆ ตามการ Loop
                        ret = CopyChildNode(dr("folder_id"), FolderID, UpdateBy, CopyFile)
                        If ret <> "true" Then
                            Exit For
                        End If
                    Else
                        ret = "false|Folder Not found"
                        Exit For
                    End If
                Else
                    Dim desc As String = "null"
                    If Not IsDBNull(dr("folder_description")) Then
                        desc = "'" & dr("folder_description") & "'"
                    End If

                    Sql = "insert into law_document (folder_id,document_name, document_description, document_path, notice_date, critical_date, actual_date,  update_time, update_by)"
                    Sql += " output inserted.document_id"
                    Sql += " values(" & ToFolderID & ",'" & dr("folder_name") & "'," & desc & ",'" & dr("document_path") & "', null,null,null,  getdate()," & UpdateBy & " )"

                    Dim dDt As DataTable = BL.Execute_DataTable(Sql)
                    If dDt.Rows.Count > 0 Then
                        If CopyFile Then
                            Dim dDr As DataRow = dDt.Rows(0)

                            Dim DocumentID As Integer = Convert.ToInt16(dDr("document_id"))   'ค่านี้จะเป็น Parent ID สำหรับ Folder ใหม่ ซึ่งจะเปลี่ยนไปเรื่อยๆ ตามการ Loop
                            Dim FromFileName As String = dr("document_path") & dr("folder_id") & dr("file_ext")
                            If File.Exists(FromFileName) Then
                                Dim FilePath As String = dr("document_path") & DocumentID & dr("file_ext")
                                Try
                                    File.Copy(FromFileName, FilePath)
                                Catch ex As Exception
                                End Try
                                ret = "true"
                            Else
                                ret = "false|File " & FromFileName & " not found"
                            End If
                        Else
                            ret = "true"
                        End If
                    Else
                        ret = "false|Folder not found"
                        Exit For
                    End If
                End If
            Next
        Else
            ret = "true"
        End If

        Return ret
    End Function

    Public Function BuiltDownloadFolder(FolderID As Integer, CopyFolderName As String, UserName As String) As String
        Dim ret As String = ""
        Try
            Dim dt As New DataTable
            dt.Columns.Add("node_id")
            dt.Columns.Add("node_name")
            dt.Columns.Add("file_ext")
            dt.Columns.Add("node_type")
            dt.Columns.Add("path_name")
            dt.Columns.Add("full_name")

            dt = BuiltDocumentChildData(dt, FolderID)

            If dt.Rows.Count > 0 Then
                Dim RootPathName As String = GetFolderPath(FolderID)
                Dim TempPathName As String = UploadTempPath & UserName & "\" & CopyFolderName & "\"
                TempPathName = TempPathName.Replace(",", "").Replace("(", "").Replace(")", "")
                If Directory.Exists(TempPathName) = False Then
                    Directory.CreateDirectory(TempPathName)
                End If

                dt.DefaultView.RowFilter = "node_type='" & TreeviewNodeType.FolderMenu.ToString & "'"
                For Each dr As DataRowView In dt.DefaultView
                    Dim DirPathName As String = TempPathName & dr("path_name").Replace(RootPathName, "") & dr("node_name")
                    DirPathName = DirPathName.Replace(",", "").Replace("(", "").Replace(")", "")

                    Dim dInfo As New DirectoryInfo(DirPathName)
                    If dInfo.Exists = False Then
                        dInfo.Create()
                    End If
                Next
                dt.DefaultView.RowFilter = ""


                dt.DefaultView.RowFilter = "node_type='" & TreeviewNodeType.DocumentMenu.ToString & "'"
                For Each dr As DataRowView In dt.DefaultView
                    If Convert.IsDBNull(dr("full_name")) = False Then
                        If IO.File.Exists(dr("full_name")) = True Then
                            Dim StrFileByte() As Byte = File.ReadAllBytes(dr("full_name"))

                            Dim TempFile As String = TempPathName & dr("path_name").Replace(RootPathName, "").Replace("/", "\") & dr("node_name") & dr("file_ext")
                            TempFile = TempFile.Replace(",", "").Replace("(", "").Replace(")", "")
                            Dim fInfo As New FileInfo(TempFile)
                            If Directory.Exists(fInfo.DirectoryName) = False Then
                                Directory.CreateDirectory(fInfo.DirectoryName)
                            End If

                            If fInfo.Exists = False Then
                                Using tmp As Stream = fInfo.OpenWrite
                                    tmp.Write(StrFileByte, 0, StrFileByte.Length)
                                    tmp.Flush()
                                End Using
                            End If

                            'File.Copy(dr("full_name"), TempFile)
                        End If
                    End If
                Next
                dt.DefaultView.RowFilter = ""
                'zip.AddSelectedFiles("*.*", TempPathName, CopyFolderName, True)

                ret = TempPathName
            End If
        Catch ex As Exception

        End Try
        Return ret
    End Function

    Private Function BuiltDocumentChildData(dt As DataTable, ParentID As Long) As DataTable
        Dim cDt As DataTable = GetChildNode(ParentID)
        If cDt.Rows.Count > 0 Then
            For Each cDr As DataRow In cDt.Rows
                Dim dr As DataRow = dt.NewRow
                dr("node_id") = cDr("folder_id")
                dr("node_name") = cDr("folder_name")
                dr("file_ext") = cDr("file_ext")
                dr("node_type") = cDr("node_type")
                dr("path_name") = GetFolderPath(ParentID)
                dr("full_name") = cDr("full_name")
                dt.Rows.Add(dr)

                dt = BuiltDocumentChildData(dt, cDr("folder_id"))
            Next
        End If
        cDt.Dispose()

        Return dt
    End Function
#End Region

#Region "Document "

    Public Function SaveLawDocument(DocumentID As Integer, FolderID As Integer, DocName As String, DocDesc As String, OriginalFileName As String, NoticeDate As DateTime, CriticalDate As DateTime, ActualDate As DateTime, UpdateBy As Integer, TempFilePath As String) As String
        Dim ret As String = "false"

        'Check Duplicate Document Name
        Dim sql As String = "select document_id "
        sql += " from law_document "
        sql += " where document_name='" & DocName.Replace("'", "''") & "' "
        sql += " and folder_id= " & FolderID
        sql += " and document_id<> " & DocumentID

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        If dt.Rows.Count > 0 Then
            ret = "false|Document is already exists"
            Return ret
        End If

        Dim LawDocPath As String = LawDocumentPath()
        If LawDocPath.EndsWith("\") = False Then
            LawDocPath += "\"
        End If

        Dim fInfo As FileInfo
        Dim FileExt As String = ""
        If File.Exists(TempFilePath) = True Then
            fInfo = New FileInfo(TempFilePath)
            FileExt = fInfo.Extension
        End If

        If DocumentID = 0 Then
            sql = "insert into law_document (folder_id,document_name, document_description, notice_date, critical_date, actual_date, document_path, original_file_name, file_ext, update_time,update_by) " & Environment.NewLine
            sql += " output inserted.document_id " & Environment.NewLine
            sql += " values(" & FolderID & ",'" & DocName.Replace("'", "''") & "','" & DocDesc.Replace("'", "''") & "'," & ToQueryDate(NoticeDate) & "," & ToQueryDate(CriticalDate) & "," & ToQueryDate(ActualDate) & ", '" & LawDocPath & "','" & OriginalFileName.Replace("'", "''") & "','" & FileExt & "', getdate()," & UpdateBy & ")"
        Else
            sql = " update law_document " & Environment.NewLine
            sql += " set document_name='" & DocName.Replace("'", "''") & "' " & Environment.NewLine
            sql += " , document_description='" & DocDesc.Replace("'", "''") & "' " & Environment.NewLine
            sql += " , folder_id =" & FolderID & Environment.NewLine
            sql += " , notice_date = " & ToQueryDate(NoticeDate) & Environment.NewLine
            sql += " , critical_date = " & ToQueryDate(CriticalDate) & Environment.NewLine
            sql += " , actual_date =" & ToQueryDate(ActualDate) & Environment.NewLine
            sql += " , document_path ='" & LawDocPath.Replace("'", "''") & "' " & Environment.NewLine
            sql += " , original_file_name ='" & OriginalFileName.Replace("'", "''") & "' " & Environment.NewLine
            sql += " , file_ext ='" & FileExt & "' " & Environment.NewLine
            sql += " , update_time = getdate() " & Environment.NewLine
            sql += " , update_by =" & UpdateBy & Environment.NewLine
            sql += " output inserted.document_id " & Environment.NewLine
            sql += " where document_id=" & DocumentID
        End If

        dt = BL.Execute_DataTable(sql)
        If dt.Rows.Count > 0 Then
            If File.Exists(TempFilePath) = True Then
                ret = "true|" & dt.Rows(0)("document_id")
            Else
                ret = "true|" & dt.Rows(0)("document_id")
            End If
        End If
        dt.Dispose()

        Return ret
    End Function

    Public Function DeleteDocument(DocumentID As Integer) As String
        Dim ret As String = "false"
        Try
            DeleteFile(DocumentID)
            Dim Sql As String = "delete from law_document where document_id='" & DocumentID & "'"
            BL.Execute_Command(Sql)

            ret = "true"

        Catch ex As Exception
            ret = "false|Exception " & ex.Message & vbNewLine & ex.StackTrace
        End Try

        Return ret
    End Function

    Public Function DeleteFile(DocumentID As Integer) As String
        Dim sql As String = " select document_path, file_ext from LAW_Document where document_id='" & DocumentID & "'"
        Dim dt As DataTable = BL.Execute_DataTable(sql)
        If dt.Rows.Count > 0 Then
            If Convert.IsDBNull(dt.Rows(0)("document_path")) = False Then
                Dim FilePath As String = dt.Rows(0)("document_path")
                If FilePath.EndsWith("\") = False Then
                    FilePath = FilePath & "\"
                End If
                If Convert.IsDBNull(dt.Rows(0)("file_ext")) = False Then
                    Dim FileExt As String = dt.Rows(0)("file_ext")
                    If FileExt.StartsWith(".") = False Then
                        FileExt = "." & FileExt
                    End If

                    Dim FileName As String = FilePath & DocumentID & "" & FileExt

                    If File.Exists(FileName) = True Then
                        Try
                            File.SetAttributes(FileName, FileAttributes.Normal)
                            File.Delete(FileName)
                        Catch ex As Exception

                        End Try
                    End If
                End If
            End If
        End If

        Return "true"
    End Function

    Public Function GetDocumentDetail(DocumentID As Integer) As DataTable
        Dim sql As String = "select d.folder_id, d.document_name, d.document_description, d.notice_date, d.critical_date,  d.actual_date, "
        sql += " d.original_file_name file_name, "
        sql += " d.document_path + convert(varchar,d.document_id) + d.file_ext file_path, d.original_file_name "
        sql += " from law_document d "
        sql += " where d.document_id=" & DocumentID

        Dim dt As DataTable = BL.Execute_DataTable(sql)

        Return dt
    End Function

    Public Function GetIconImageUrl(FileExt As String) As String
        Dim ret As String = "resources/images/icons/icon_file.png"
        Select Case FileExt.ToLower
            Case ".pdf"
                ret = "resources/images/icons/file_extension_pdf.png"
            Case ".xls", ".xlsx"
                ret = "resources/images/icons/file_extension_xls.png"
            Case ".txt"
                ret = "resources/images/icons/file_extension_txt.png"
            Case ".tif"
                ret = "resources/images/icons/file_extension_tif.png"
            Case ".jpg"
                ret = "resources/images/icons/file_extension_jpg.png"
            Case ".gif"
                ret = "resources/images/icons/file_extension_gif.png"
            Case ".png"
                ret = "resources/images/icons/file_extension_png.png"
            Case ".doc", ".docx"
                ret = "resources/images/icons/file_extension_doc.png"
            Case ".vsd", ".vsdx"
                ret = "resources/images/icons/file_extension_vsd.png"
            Case ".pptx", ".ppt"
                ret = "resources/images/icons/file_extension_pps.png"
        End Select
        Return ret
    End Function
#End Region

#Region "DocumentProcessSetting"
    Public Function SaveDocumentTemplateSetting(DocumentProcessSettingID As Long, DocumentName As String, IntervalYear As Integer, ActiveStatus As String, PaperDT As DataTable, UpdateBy As Integer) As String
        Dim ret As String = "false"
        Try
            Dim sql As String = ""
            If DocumentProcessSettingID = 0 Then
                sql = " insert into law_document_setting(document_name,interval,  active_status, Update_Time, Update_By) " & Environment.NewLine
                sql += " output inserted.document_setting_id " & Environment.NewLine
                sql += " values('" & DocumentName.Replace("'", "''") & "'," & IntervalYear & ", '" & ActiveStatus & "', getdate(), " & UpdateBy & ")"
            Else
                sql = " update law_document_setting " & Environment.NewLine
                sql += " set document_name='" & DocumentName.Replace("'", "''") & "' " & Environment.NewLine
                sql += ", interval=" & IntervalYear & Environment.NewLine
                sql += ", active_status='" & ActiveStatus & "'" & Environment.NewLine
                sql += ", Update_Time=getdate() " & Environment.NewLine
                sql += ", Update_By=" & UpdateBy & Environment.NewLine
                sql += " output inserted.document_setting_id " & Environment.NewLine
                sql += " where document_setting_id=" & DocumentProcessSettingID
            End If


            Dim dt As DataTable = BL.Execute_DataTable(sql)
            If dt.Rows.Count > 0 Then
                Dim NewID As Long = Convert.ToInt64(dt.Rows(0)("document_setting_id"))

                If PaperDT.Rows.Count > 0 Then
                    Dim re As String = ""
                    For Each pDr As DataRow In PaperDT.Rows
                        If pDr("document_setting_paper_id") > 0 Then
                            sql = " update LAW_Document_Setting_Paper "
                            sql += " Set document_setting_id=" & NewID
                            sql += ", paper_name='" & pDr("paper_name") & "' "
                            sql += ", percent_complete=" & pDr("percent_complete")
                            sql += ", law_organize_id=" & pDr("law_organize_id")
                            sql += ", Update_Time=getdate() "
                            sql += ", Update_By=" & UpdateBy
                            sql += " output inserted.document_setting_paper_id "
                            sql += " where document_setting_paper_id=" & pDr("document_setting_paper_id")
                        Else
                            sql = " insert into LAW_Document_Setting_Paper(document_setting_id, paper_name, law_organize_id, percent_complete, Update_Time, Update_By)"
                            sql += " output inserted.document_setting_paper_id " & Environment.NewLine
                            sql += " values(" & NewID & ",'" & pDr("paper_name") & "'," & pDr("law_organize_id") & "," & pDr("percent_complete") & ", getdate()," & UpdateBy & ")"
                        End If


                        re = BL.Execute_Command(sql)
                        If re.ToLower <> "true" Then
                            Exit For
                        End If
                    Next
                    If re.ToLower = "true" Then
                        ret = "true|" & NewID
                    Else
                        ret = re
                    End If
                Else
                    ret = "true|" & NewID
                End If
            End If
            dt.Dispose()
        Catch ex As Exception
            ret = "false|Exception " & ex.Message & vbNewLine & ex.StackTrace
        End Try

        Return ret
    End Function

    Public Function DeleteDocumentSetting(DocumentSettingID As Long) As String
        Dim ret As String = "false"
        Try
            Dim sql As String = "delete from LAW_Document_Setting_paper where document_setting_id=" & DocumentSettingID
            ret = BL.Execute_Command(sql)
            If ret.ToLower = "true" Then
                sql = "delete from LAW_Document_Setting where document_setting_id=" & DocumentSettingID
                ret = BL.Execute_Command(sql)
            End If
        Catch ex As Exception
            ret = "false|Exception " & ex.Message
        End Try
        Return ret
    End Function

    Public Function DeleteDocumentSettingPaper(DocumentSettingPaperID As Long) As String
        Dim ret As String = "false"
        Try
            Dim sql As String = "delete from LAW_Document_Setting_paper where document_setting_paper_id=" & DocumentSettingPaperID
            ret = BL.Execute_Command(sql)
        Catch ex As Exception
            ret = "false|Exception " & ex.Message
        End Try
        Return ret
    End Function

    Public Function DeleteLawOrganize(LawOrgID As Long) As String
        Dim ret As String = "false"
        Try
            Dim sql As String = "delete from LAW_Organize where law_organize_id=" & LawOrgID
            ret = BL.Execute_Command(sql)
        Catch ex As Exception
            ret = "false|Exception " & ex.Message
        End Try
        Return ret
    End Function


    Public Function GetListDocumentTemplate() As DataTable
        Dim SQL As String = "select document_setting_id, interval,document_name,  active_status "
        SQL += " from law_document_setting "
        SQL += " order by document_name "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception

        End Try
        Return DT
    End Function

    Public Function GetDataDocumentTemplate(DocumentSettingID As Long) As DataTable
        Dim DT As DataTable = GetListDocumentTemplate()
        DT.DefaultView.RowFilter = "document_setting_id='" & DocumentSettingID & "'"
        Return DT.DefaultView.ToTable
    End Function

    Public Function GetListDocumentSettingPaper(DocumentSettingID As Long) As DataTable
        Dim sql As String = "select p.document_setting_paper_id, p.document_setting_id, p.paper_name, " & Environment.NewLine
        sql += " p.law_organize_id, org.org_name, p.percent_complete " & Environment.NewLine
        sql += " from LAW_Document_Setting_paper p " & Environment.NewLine
        sql += " inner join LAW_Organize org on p.law_organize_id=org.law_organize_id "
        sql += " where document_setting_id=" & DocumentSettingID

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        Return dt
    End Function

    Public Function GetListDocumentPlanPaper(DocumentPlanID As Long) As DataTable
        Dim sql As String = "select pp.document_setting_paper_id, pp.plan_notice_date, pp.plan_critical_date, pp.upload_date, sp.paper_name, org.org_name, " & Environment.NewLine
        sql += " pp.percent_complete, pp.document_plan_paper_id, pp.document_plan_id,pp.next_notice_date " & Environment.NewLine
        sql += " from LAW_Document_Plan_paper pp " & Environment.NewLine
        sql += " inner join LAW_Document_Setting_paper sp on sp.document_setting_paper_id=pp.document_setting_paper_id"
        sql += " inner join LAW_Organize org on org.law_organize_id=sp.law_organize_id"
        sql += " where document_plan_id=" & DocumentPlanID

        Return BL.Execute_DataTable(sql)
    End Function

    Public Sub BindMasterDocument(DDL As DropDownList, Optional SelectedValue As Integer = -1)
        Dim sql As String = "select document_setting_id, document_name "
        sql += " from law_document_setting "
        sql += " where active_status='Y'"
        sql += " order by document_name "

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        If dt.Rows.Count > 0 Then
            DDL.Items.Clear()
            DDL.Items.Add(New ListItem("Choose Document", 0))

            For i As Integer = 0 To dt.Rows.Count - 1
                DDL.Items.Add(New ListItem(dt.Rows(i)("document_name"), dt.Rows(i)("document_setting_id")))
            Next

            If DDL.SelectedIndex < 1 And SelectedValue > 0 Then
                For i As Integer = 0 To DDL.Items.Count - 1
                    If DDL.Items(i).Value.ToString = SelectedValue.ToString Then
                        DDL.SelectedIndex = i
                        Exit For
                    End If
                Next
            End If
        End If
    End Sub

    Public Sub BindAllDocumentList(DDL As DropDownList, Optional SelectedValue As Integer = -1)
        Dim sql As String = ""
        sql += "     Select  DISTINCT document_name     " & vbLf
        sql += " From LAW_Document_Plan " & vbLf


        sql += " Order BY document_name " & vbLf


        Dim dt As DataTable = BL.Execute_DataTable(sql)
        If dt.Rows.Count > 0 Then
            DDL.Items.Clear()
            DDL.Items.Add(New ListItem("Choose Document", 0))

            For i As Integer = 0 To dt.Rows.Count - 1
                DDL.Items.Add(New ListItem(dt.Rows(i)("document_name")))
            Next

            If DDL.SelectedIndex < 1 And SelectedValue > 0 Then
                For i As Integer = 0 To DDL.Items.Count - 1
                    If DDL.Items(i).Value.ToString = SelectedValue.ToString Then
                        DDL.SelectedIndex = i
                        Exit For
                    End If
                Next
            End If
        End If
    End Sub


#End Region

#Region "Document Plan"
    Public Function SaveDocumentPlan(DocumentPlanID As Long, DocumentSettingID As Long, DocumentName As String, DocumentYear As Integer, PlantID As Integer, TagTableName As String, TagID As Integer, PaperNewDT As DataTable, UpdateBy As Integer) As String
        Dim ret As String = "False"
        Try
            Dim sql As String = ""
            If DocumentPlanID = 0 Then
                sql = " insert into LAW_Document_Plan (document_setting_id, document_name, plan_status, Update_Time, Update_By, document_year, plant_id, tag_table_name, tag_id)" & Environment.NewLine
                sql += " output inserted.document_plan_id " & Environment.NewLine
                sql += " values(" & DocumentSettingID & ",'" & DocumentName.Replace("'", "''") & "', 1, getdate(), " & UpdateBy & "," & DocumentYear & "," & PlantID & ",'" & TagTableName & "'," & TagID & ")" & Environment.NewLine
            Else
                sql = "update LAW_Document_Plan " & Environment.NewLine
                sql += " set document_setting_id=" & DocumentSettingID & Environment.NewLine
                sql += ", document_name='" & DocumentName.Replace("'", "''") & "'" & Environment.NewLine
                sql += ", update_time=getdate() " & Environment.NewLine
                sql += ", update_by= " & UpdateBy & Environment.NewLine
                sql += ", document_year=" & DocumentYear & Environment.NewLine
                sql += ", plant_id=" & PlantID & Environment.NewLine
                sql += ", tag_table_name='" & TagTableName & "' " & Environment.NewLine
                sql += ", tag_id= " & TagID & Environment.NewLine
                sql += " output inserted.document_plan_id " & Environment.NewLine
                sql += " where document_plan_id=" & DocumentPlanID
            End If

            Dim dt As DataTable = BL.Execute_DataTable(sql)
            If dt.Rows.Count > 0 Then
                DocumentPlanID = Convert.ToInt64(dt.Rows(0)("document_plan_id"))
                sql = " select * from LAW_Document_Plan_Paper where document_plan_id=" & DocumentPlanID

                Dim PaperDT As DataTable = BL.Execute_DataTable(sql)

                If PaperNewDT.Rows.Count > 0 Then
                    For Each pDr As DataRow In PaperNewDT.Rows
                        Dim DocumentPlanPaperID As Long = 0
                        PaperDT.DefaultView.RowFilter = "document_plan_id='" & DocumentPlanID & "' and document_setting_paper_id='" & pDr("document_setting_paper_id") & "'"
                        If PaperDT.DefaultView.Count > 0 Then
                            sql = " update LAW_Document_Plan_Paper " & Environment.NewLine
                            sql += " set document_plan_id= " & DocumentPlanID & Environment.NewLine
                            sql += ", document_setting_paper_id=" & pDr("document_setting_paper_id") & " " & Environment.NewLine
                            sql += ", percent_complete= " & pDr("percent_complete") & Environment.NewLine
                            sql += ", plan_notice_date=" & ToQueryDate(pDr("plan_notice_date")) & Environment.NewLine
                            sql += ", plan_critical_date=" & ToQueryDate(pDr("plan_critical_date")) & Environment.NewLine
                            sql += ", Update_Time=getdate() " & Environment.NewLine
                            sql += ", Update_By= " & UpdateBy & Environment.NewLine
                            sql += " output inserted.document_plan_paper_id " & Environment.NewLine
                            sql += " where document_plan_paper_id=" & DocumentPlanPaperID

                            DocumentPlanPaperID = PaperDT.DefaultView(0)("document_plan_paper_id")
                        Else
                            sql = "insert into LAW_Document_Plan_Paper(document_plan_id,document_setting_paper_id, percent_complete,plan_notice_date, plan_critical_date, Update_Time, Update_By)" & Environment.NewLine
                            sql += "output inserted.document_plan_paper_id " & Environment.NewLine
                            sql += " values (" & DocumentPlanID & ", " & pDr("document_setting_paper_id") & ", " & pDr("percent_complete") & "," & ToQueryDate(pDr("plan_notice_date")) & "," & ToQueryDate(pDr("plan_critical_date")) & ", getdate()," & UpdateBy & ")" & Environment.NewLine
                        End If

                        ret = BL.Execute_Command(sql)
                        If ret.ToLower <> "true" Then
                            Exit For
                        End If
                    Next
                End If

                If ret = "True" Then
                    ret = "True|" & DocumentPlanID
                End If
            End If
            dt.Dispose()
        Catch ex As Exception
            ret = "false|Exception " & ex.Message
        End Try
        Return ret
    End Function

    Public Function GetListDocumentPlan(DocumentPlanID As Long) As DataTable
        Dim SQL As String = "select p.document_plan_id, pn.plant_id, isnull(tag.tag_id,0) tag_id, isnull(pc.proc_id,0) proc_id,  "
        SQL += " isnull(aa.area_id,0) area_id, p.document_name, p.document_year, pn.plant_code,st.document_setting_id, " & Environment.NewLine
        SQL += " case when tag.tag_id is null then '' else aa.area_code + '-' + pc.proc_code + '-' + tag.tag_no  end tag_no, p.next_notice_date " & Environment.NewLine
        SQL += " From LAW_Document_Plan p " & Environment.NewLine
        SQL += " inner join MS_PLANT pn on pn.plant_id=p.plant_id " & Environment.NewLine
        SQL += " left join MS_ST_TAG tag on tag.tag_id=p.tag_id " & Environment.NewLine
        SQL += " left join MS_PROCESS pc on pc.proc_id=tag.proc_id " & Environment.NewLine
        SQL += " left join MS_AREA aa on aa.area_id=tag.area_id " & Environment.NewLine
        SQL += " inner join LAW_Document_Setting st on st.document_setting_id=p.document_setting_id " & Environment.NewLine
        If DocumentPlanID > 0 Then
            SQL += " where p.document_plan_id='" & DocumentPlanID & "'" & Environment.NewLine
        End If
        SQL += " order by p.document_name " & Environment.NewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception

        End Try
        Return DT
    End Function

    Public Function GetListDocumentAllPlan(DocTemplateName As String, DocumentYear As Integer, PlantID As Integer, TagID As Integer) As DataTable

        Dim SQL As String = "select p.document_plan_id, p.document_name, st.document_setting_id, " & Environment.NewLine
        SQL += " p.document_year, p.plant_id, pn.plant_code, p.percent_weight,"
        SQL += " case when tag.tag_id is null then '' else a.area_code + '-' + pc.proc_code + '-' + tag.tag_no  end tag_no"
        SQL += " From LAW_Document_Plan p " & Environment.NewLine
        SQL += " inner join MS_PLANT pn on pn.plant_id=p.plant_id " & Environment.NewLine
        SQL += " left join MS_ST_TAG tag on tag.tag_id=p.tag_id " & Environment.NewLine
        SQL += " left join MS_AREA a on a.area_id=tag.area_id " & Environment.NewLine
        SQL += " left join MS_PROCESS pc on pc.proc_id=tag.proc_id " & Environment.NewLine
        SQL += " inner join LAW_Document_Setting st on st.document_setting_id=p.document_setting_id " & Environment.NewLine
        SQL += " where 1=1 "

        If DocTemplateName <> "" Then
            SQL += " and p.document_name='" & DocTemplateName & "'" & Environment.NewLine
        End If
        If DocumentYear > 0 Then
            SQL += " and p.document_year=" & DocumentYear & Environment.NewLine
        End If
        If PlantID > 0 Then
            SQL += " and p.plant_id=" & PlantID & Environment.NewLine
        End If
        If TagID > 0 Then
            SQL += " and p.tag_id=" & TagID & Environment.NewLine
        End If

        SQL += " order by p.document_name " & Environment.NewLine

        Dim DT As DataTable = BL.Execute_DataTable(SQL)
        Return DT
    End Function
    Public Function GetDocumentNotificationList(NoticeYear As Integer, template_Name As String, Plant_Code As String, Tag_Code As String) As DataTable


        Dim Sql As String = " SELECT * FROM vw_Notification_List " & vbLf
        If NoticeYear = 0 Then
            Sql += "    where 1=1" & vbLf
        Else
            Sql += "    where Year(plan_notice_date)+543 ='" & NoticeYear.ToString() & "'  " & vbLf
        End If
        Sql += "    And template_Name Like '%" & template_Name.ToString.Replace("'", "''") & "%'  " & vbLf
        Sql += "    And Plant_Code Like '%" & Plant_Code.Replace("'", "''") & "%'  " & vbLf
        Sql += "    And Tag_Code Like '%" & Tag_Code.Replace("'", "''") & "%'  " & vbLf
        Sql += " ORDER BY  plan_notice_date , document_plan_id  " & vbLf

        Dim dt As DataTable = BL.Execute_DataTable(Sql)
        Return dt

    End Function

    Public Function DeleteDocumentPlant(DocumentPlanID As Long) As String
        Dim ret As String = "false"
        Dim sql As String = "select document_paper_upload_id, file_path, file_ext "
        sql += " from LAW_Document_Paper_Upload "
        sql += " where document_plan_paper_id in (select document_plan_paper_id from LAW_Document_Plan_Paper where document_plan_id=" & DocumentPlanID & ") "

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim FileName As String = dr("file_path") & dr("document_paper_upload_id") & dr("file_ext")
                If File.Exists(FileName) = True Then
                    Try
                        File.SetAttributes(FileName, FileAttributes.Normal)
                        File.Delete(FileName)
                    Catch ex As Exception

                    End Try
                End If
            Next
            sql = "delete from LAW_Document_Paper_Upload "
            sql += " where document_plan_paper_id in (select document_plan_paper_id from LAW_Document_Plan_Paper where document_plan_id=" & DocumentPlanID & " )"
            ret = BL.Execute_Command(sql)
        Else
            ret = "true"
        End If

        If ret.ToLower = "true" Then
            sql = " delete from LAW_Document_Plan_Paper "
            sql += " where document_plan_id= " & DocumentPlanID
            ret = BL.Execute_Command(sql)
            '--Header--
            If ret.ToLower = "true" Then
                sql = "delete from LAW_Document_Plan where document_plan_id=" & DocumentPlanID
                ret = BL.Execute_Command(sql)
            End If
        End If


        Return ret
    End Function

    Public Function GetDataDocumentPlan(DocumentPlanID As Long) As DataTable
        Return GetListDocumentPlan(DocumentPlanID)
    End Function

    Public Function GetDocumentPaperStatus(DocumentPlanPaperID As Long) As DocumentPlanStatusClass
        Dim sql As String = "Select pp.plan_notice_date, pp.plan_critical_date, pp.upload_date, pp.document_plan_paper_id, pp.percent_complete, count(pu.document_paper_upload_id) file_qty "
        sql += " from LAW_Document_Plan_Paper pp "
        sql += " left join LAW_Document_Paper_Upload pu on pu.document_plan_paper_id=pp.document_plan_paper_id "
        sql += " where pp.document_plan_paper_id= " & DocumentPlanPaperID
        sql += " Group by pp.plan_notice_date, pp.plan_critical_date, pp.upload_date, pp.document_plan_paper_id, pp.percent_complete"

        Dim ret As New DocumentPlanStatusClass
        Try
            Dim dt As DataTable = BL.Execute_DataTable(sql)
            ret = BuiltDocumentPlanStatus(dt)
            dt.Dispose()
        Catch ex As Exception
            ret = New DocumentPlanStatusClass
        End Try
        Return ret
    End Function

    Private Function BuiltDocumentPlanStatus(dt As DataTable) As DocumentPlanStatusClass
        Dim ret As New DocumentPlanStatusClass
        If dt.Rows.Count > 0 Then


            Dim LastStatus As String = ""
            Dim CurrStatus As String = ""
            For Each dr As DataRow In dt.Rows
                Dim FileQty As Integer = dr("file_qty")
                Dim vNoticeDate As DateTime = Convert.ToDateTime(dr("plan_notice_date"))
                Dim vCriticalDate As DateTime = Convert.ToDateTime(dr("plan_critical_date"))
                Dim vPercentComplete As Integer = Convert.ToInt16(dr("percent_complete"))
                Dim vUpdateDate As New DateTime(1, 1, 1)
                If Convert.IsDBNull(dr("upload_date")) = False Then
                    vUpdateDate = Convert.ToDateTime(dr("upload_date"))

                    If vUpdateDate.Date < ret.UploadDate.Date Then
                        ret.UploadDate = vUpdateDate
                    ElseIf ret.UploadDate.Year = 1 Then
                        ret.UploadDate = vUpdateDate
                    End If
                End If

                If vUpdateDate.Year = 1 And FileQty = 0 Then
                    'ยังไม่ Complete
                    If DateTime.Now.Date > vCriticalDate.Date And vUpdateDate.Year = 1 Then
                        'Status = Critical
                        CurrStatus = "LATE"
                        ret.PlanStatus = "CRITICAL"

                        If vCriticalDate.Date < ret.CriticalDate.Date Then
                            ret.CriticalDate = vCriticalDate
                        ElseIf ret.CriticalDate.Year = 1 Then
                            ret.CriticalDate = vCriticalDate
                        End If
                        ret.NoticeDate = vNoticeDate
                        ret.UploadDate = vUpdateDate

                        Exit For  'ถ้ามี Critical ให้จบ Loop เลย
                    End If
                    If DateTime.Now.Date > vNoticeDate.Date And vUpdateDate.Year = 1 Then
                        CurrStatus = "NOTICE"
                        ret.PlanStatus = "NOTICE"

                        If vNoticeDate.Date < ret.NoticeDate.Date Then
                            ret.NoticeDate = vNoticeDate
                        ElseIf ret.NoticeDate.Year = 1 Then
                            ret.NoticeDate = vNoticeDate
                        End If

                        ret.CriticalDate = vCriticalDate
                        ret.UploadDate = vUpdateDate
                    End If
                End If

                If CurrStatus = "" Then
                    If DateTime.Now.Date <= vNoticeDate.Date And vUpdateDate.Year = 1 Then
                        CurrStatus = "WAITING"
                        ret.PlanStatus = "WAITING"
                    End If

                    If vUpdateDate.Year <> 1 And FileQty > 0 Then
                        ret.PlanStatus = "COMPLETE"
                        ret.PercentComplete += vPercentComplete  'ต้อง Complete ก่อนถึงจะแสดง %

                        'ให้ขึ้นแจ้งว่า Late ในกรณีอัพเอกสารล่าช้า รวมถึงยังไม่อัพเอกสาร
                        If vUpdateDate.Date > vCriticalDate.Date Then
                            ret.LastStatus = "CRITICAL"
                        End If


                    End If

                    ret.CriticalDate = vCriticalDate
                    ret.NoticeDate = vNoticeDate
                    ret.UploadDate = vUpdateDate


                End If
            Next

            dt.DefaultView.RowFilter = String.Empty

            Select Case ret.PlanStatus
                Case "WAITING"
                    '--DateTime.Now.Date <= vNoticeDate.Date And vUpdateDate.Year = 1 Then
                    dt.DefaultView.RowFilter = "('" & DateTime.Now.ToString() & "'  <  plan_notice_date) AND upload_date IS NULL"
                Case "NOTICE"
                    '--DateTime.Now.Date > vNoticeDate.Date And vUpdateDate.Year = 1 
                    dt.DefaultView.RowFilter = "('" & DateTime.Now.ToString() & "'  > plan_notice_date ) AND  upload_date IS NULL"
                Case "CRITICAL"
                    dt.DefaultView.RowFilter = "upload_date IS NULL"
                Case "COMPLETE"
                    dt.DefaultView.RowFilter = "1=1"
            End Select

            Dim EmptyUpdateDate As New DateTime(1, 1, 1)
            If dt.DefaultView.Count > 0 Then
                dt.DefaultView.Sort = " plan_notice_date ASC"
                ret.NoticeDate = dt.DefaultView(0).Item("plan_notice_date")

                dt.DefaultView.Sort = " plan_critical_date ASC"
                ret.CriticalDate = dt.DefaultView(0).Item("plan_critical_date")

                dt.DefaultView.RowFilter = String.Empty
                dt.DefaultView.Sort = " upload_date DESC"
                If Not IsDBNull(dt.DefaultView(0).Item("upload_date")) Then
                    ret.UploadDate = dt.DefaultView(0).Item("upload_date")
                Else
                    ret.UploadDate = EmptyUpdateDate
                End If
            End If

            dt.DefaultView.RowFilter = String.Empty
            dt.DefaultView.RowFilter = "file_qty>0"
            If dt.DefaultView.Count > 0 Then
                Dim Per_Complete As Integer = dt.Compute("SUM(percent_complete)", " file_qty>0")
                ret.PercentComplete = Per_Complete
            Else
                ret.PercentComplete = 0
            End If


        End If

        Return ret
    End Function

    Public Function GetDocumentPlanStatus(DocumentPlanID As Long) As DocumentPlanStatusClass
        Dim sql As String = ""
        sql += " Select pp.plan_notice_date, pp.plan_critical_date, pp.upload_date, pp.document_plan_paper_id,pp.percent_complete, count(pu.document_paper_upload_id) file_qty " & Environment.NewLine
        sql += " from LAW_Document_Plan p  " & Environment.NewLine
        sql += " inner join LAW_Document_Plan_Paper pp on p.document_plan_id=pp.document_plan_id " & Environment.NewLine
        sql += " inner join LAW_Document_Setting_paper sp on sp.document_setting_paper_id=pp.document_setting_paper_id " & Environment.NewLine
        sql += " Left join LAW_Document_Paper_Upload pu on pu.document_plan_paper_id=pp.document_plan_paper_id " & Environment.NewLine
        sql += " where p.document_plan_id=" & DocumentPlanID & Environment.NewLine
        sql += " Group by pp.plan_notice_date, pp.plan_critical_date, pp.upload_date, pp.document_plan_paper_id,pp.percent_complete " & Environment.NewLine

        Dim ret As New DocumentPlanStatusClass
        Try
            Dim dt As DataTable = BL.Execute_DataTable(sql)
            ret = BuiltDocumentPlanStatus(dt)
            dt.Dispose()
        Catch ex As Exception
            ret = New DocumentPlanStatusClass
        End Try
        Return ret
    End Function

    Public Function SaveDocumentPaperUpload(DocumentPlanPaperID As Long, FileDesc As String, FilePath As String, OriginalFileName As String, FileExt As String, UpdateBy As Integer) As String
        Dim ret As String = "false"
        Try
            Dim sql As String = " insert into LAW_Document_Paper_Upload (document_plan_paper_id, file_desc, file_path,original_file_name, file_ext, Update_Time, Update_By) "
            sql += " output inserted.document_paper_upload_id "
            sql += " values (" & DocumentPlanPaperID & ",'" & FileDesc.Replace("'", "''") & "','" & FilePath.Replace("'", "''") & "','" & OriginalFileName.Replace("'", "''") & "','" & FileExt & "', getdate()," & UpdateBy & ") "

            Dim dt As DataTable = BL.Execute_DataTable(sql)
            If dt.Rows.Count > 0 Then
                sql = " update LAW_Document_Plan_Paper "
                sql += " set upload_date=getdate() "
                sql += " where document_plan_paper_id=" & DocumentPlanPaperID

                If BL.Execute_Command(sql).ToLower = "true" Then
                    ret = "true|" & dt.Rows(0)("document_paper_upload_id")
                End If
            End If
            dt.Dispose()
        Catch ex As Exception
            ret = "false|Exception " & ex.Message
        End Try

        Return ret
    End Function

    Public Function UpdateNextNoticeDate(DocumentPlanID As Long, NextNoticeDate As DateTime) As String
        Dim ret As String = "false"
        Try
            Dim sql As String = "update LAW_Document_Plan "
            sql += " set next_notice_date=" & ToQueryDate(NextNoticeDate)
            sql += " where document_plan_id=" & DocumentPlanID

            ret = BL.Execute_Command(sql).ToLower

            If ret.ToLower = "true" Then
                sql = "Update LAW_Document_Plan_Paper "
                sql += " set next_notice_date=" & ToQueryDate(NextNoticeDate)
                sql += " where document_plan_id= " & DocumentPlanID

                ret = BL.Execute_Command(sql).ToLower
            End If
        Catch ex As Exception
            ret = "false|Exception " & ex.Message
        End Try

        Return ret
    End Function

    Public Function GetListPlanPaperUpload(DocumentPlanPaperID As Long) As DataTable
        Dim sql As String = "select document_paper_upload_id, file_desc,file_path, original_file_name,file_ext  "
        sql += " From  LAW_Document_Paper_Upload"
        sql += " where document_plan_paper_id =" & DocumentPlanPaperID
        Dim dt As DataTable
        Try
            dt = BL.Execute_DataTable(sql)
        Catch ex As Exception
            dt = New DataTable
        End Try

        Return dt
    End Function

    Public Function GetDocumentPlanPercentComplete(DocumentPlanID As Long) As Integer
        Dim pStatus As DocumentPlanStatusClass = GetDocumentPlanStatus(DocumentPlanID)
        Return pStatus.PercentComplete
    End Function

    Public Sub BindDDLTagID(ddl As DropDownList, PlantID As Long, AreaID As Long, ProcessID As Long)

        Dim sql As String = "select tag.tag_id, a.area_code + '-' + pc.proc_code + '-' + tag.tag_no tag_no  "
        sql += " from MS_ST_TAG tag "
        sql += " inner join ms_area a on a.area_id=tag.area_id "
        sql += " inner join ms_plant p on p.plant_id=a.plant_id"
        sql += " inner join ms_process pc on pc.proc_id=tag.proc_id "
        sql += " where a.plant_id=" & PlantID

        If AreaID > 0 Then
            sql += " and tag.area_id=" & AreaID
        End If
        If ProcessID > 0 Then
            sql += " and tag.proc_id=" & ProcessID
        End If

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("tag_id") = 0
        dr("tag_no") = "Choose a Tag No"
        dt.Rows.InsertAt(dr, 0)

        ddl.DataTextField = "tag_no"
        ddl.DataValueField = "tag_id"
        ddl.DataSource = dt
        ddl.DataBind()
    End Sub

    Public Function GetDocumentPlanTable(DocumentPlanID As Long) As DataTable
        Dim sql As String = "select pp.plan_critical_date plan_date, pp.upload_date actual_date, sp.paper_name, "
        sql += " pp.percent_complete plan_percent, case when  pp.upload_date is null then 0 else pp.percent_complete end actual_percent "
        sql += " from  LAW_Document_Plan_Paper pp  "
        sql += " inner join LAW_Document_Setting_Paper sp on sp.document_setting_paper_id=pp.document_setting_paper_id "
        sql += " where pp.document_plan_id=" & DocumentPlanID
        sql += " order by plan_critical_date "
        Dim dt As DataTable = BL.Execute_DataTable(sql)

        Return dt
    End Function

    Public Function GetDocumentChartData(DocumentPlanID As Long) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("action_date", GetType(Date))
        dt.Columns.Add("paper_name")
        dt.Columns.Add("plan_percent")
        dt.Columns.Add("actual_percent")


        Dim sql As String = "select pp.plan_notice_date plan_date, pp.upload_date actual_date, sp.paper_name, "
        sql += " pp.percent_complete plan_percent, case when  pp.upload_date is null then 0 else pp.percent_complete end actual_percent "
        sql += " from  LAW_Document_Plan_Paper pp  "
        sql += " inner join LAW_Document_Setting_Paper sp on sp.document_setting_paper_id=pp.document_setting_paper_id "
        sql += " where pp.document_plan_id=" & DocumentPlanID
        sql += " order by plan_notice_date "

        Dim PlanDT As DataTable = BL.Execute_DataTable(sql)
        If PlanDT.Rows.Count > 0 Then
            For Each PlanDR As DataRow In PlanDT.Rows
                Dim dr As DataRow = dt.NewRow
                dr("action_date") = PlanDR("plan_date")
                dr("paper_name") = PlanDR("paper_name")
                dr("plan_percent") = PlanDR("plan_percent")
                If Convert.IsDBNull(PlanDR("actual_date")) = False Then
                    If Convert.ToDateTime(PlanDR("plan_date")).Date = Convert.ToDateTime(PlanDR("actual_date")).Date Then
                        dr("actual_percent") = PlanDR("actual_percent")
                    Else
                        dr("actual_percent") = 0
                    End If
                Else
                    dr("actual_percent") = 0
                End If
                dt.Rows.Add(dr)

                If Convert.IsDBNull(PlanDR("actual_date")) = False Then
                    dr = dt.NewRow
                    dr("action_date") = PlanDR("actual_date")
                    dr("paper_name") = PlanDR("paper_name")
                    dr("actual_percent") = PlanDR("actual_percent")

                    If Convert.ToDateTime(PlanDR("plan_date")).Date = Convert.ToDateTime(PlanDR("actual_date")).Date Then
                        dr("plan_percent") = PlanDR("plan_percent")
                    Else
                        dr("plan_percent") = 0
                    End If
                    dt.Rows.Add(dr)
                End If
            Next
            dt.DefaultView.Sort = "action_date"
        End If

        Return dt.DefaultView.ToTable
    End Function

    Public Function GetDocumentChartData_PlanActual(DocumentPlanID As Long) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("action_date", GetType(Date))
        dt.Columns.Add("paper_name")
        dt.Columns.Add("plan_percent")
        dt.Columns.Add("actual_percent")
        Dim sql As String = "select Convert(Date,pp.plan_critical_date) plan_date, Convert(Date,pp.upload_date) actual_date, sp.paper_name, "
        sql += " pp.percent_complete plan_percent, case when  pp.upload_date is null then 0 else pp.percent_complete end actual_percent "
        sql += " from  LAW_Document_Plan_Paper pp  "
        sql += " inner join LAW_Document_Setting_Paper sp on sp.document_setting_paper_id=pp.document_setting_paper_id "
        sql += " where pp.document_plan_id=" & DocumentPlanID
        sql += " order by plan_critical_date "

        Dim PlanDT As DataTable = BL.Execute_DataTable(sql)
        Return PlanDT
    End Function
    Public Function GetAllDate_PlanActual(DocumentPlanID As Long) As DataTable

        Dim sql As String = ""

        sql += " Select DISTINCT  Convert(Date, Action_date) Action_date  " & vbLf
        sql += " FROM( " & vbLf
        sql += " Select pp.plan_critical_date  Action_date  from  LAW_Document_Plan_Paper pp   inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id  where pp.document_plan_id=" & DocumentPlanID & "   " & vbLf
        sql += " UNION ALL " & vbLf
        sql += " Select pp.upload_date  Action_date  from  LAW_Document_Plan_Paper pp   inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id  where pp.document_plan_id=" & DocumentPlanID & "    " & vbLf
        '-----Today-----
        sql += " UNION ALL  " & Environment.NewLine
        sql += "  select CONVERT (date, GETDATE())  Action_date  " & Environment.NewLine

        sql += " )As TB  " & vbLf
        sql += " WHERE Action_date Is Not NULL " & vbLf
        sql += " Order BY Action_date asc " & vbLf

        Dim DA As New SqlDataAdapter(sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetStartDate_PlanActual(DocumentPlanID As Long) As DataTable

        Dim sql As String = ""
        sql += " Select DISTINCT Convert(Date, plan_notice_date) Action_date  " & vbLf
        sql += " FROM( " & vbLf
        sql += " Select pp.plan_critical_date  Action_date ,pp.plan_notice_date  from  LAW_Document_Plan_Paper pp   inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id  where pp.document_plan_id=" & DocumentPlanID & "   " & vbLf
        sql += " UNION ALL " & vbLf
        sql += " Select pp.upload_date  Action_date ,pp.plan_notice_date  from  LAW_Document_Plan_Paper pp   inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id  where pp.document_plan_id=" & DocumentPlanID & "    " & vbLf
        sql += " )As TB  " & vbLf
        sql += " WHERE Action_date Is Not NULL " & vbLf
        sql += " Order BY Action_date asc " & vbLf


        Dim DA As New SqlDataAdapter(sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    Public Function GetAllDate_YearData(DocYear As Integer) As DataTable
        Dim Year As String = ""
        If DocYear > 0 Then Year = DocYear

        Dim sql As String = ""
        sql += " Select DISTINCT Convert(Date, Action_date) Action_date FROM ( " & Environment.NewLine
        sql += " Select pp.plan_critical_date Action_date from  LAW_Document_Plan_Paper pp   inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id  inner join LAW_Document_Plan p On p.document_plan_id=pp.document_plan_id   AND percent_weight >0    where p.document_year LIKE '%" & Year & "%'  " & Environment.NewLine
        sql += " UNION ALL " & Environment.NewLine
        sql += " Select pp.upload_date Action_date from  LAW_Document_Plan_Paper pp   inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id  inner join LAW_Document_Plan p On p.document_plan_id=pp.document_plan_id  AND percent_weight >0      where p.document_year LIKE '%" & Year & "%'   " & Environment.NewLine

        '-----Today-----
        sql += " UNION ALL  " & Environment.NewLine
        sql += "  select CONVERT (date, GETDATE())  Action_date  " & Environment.NewLine


        sql += " )As TB  " & Environment.NewLine
        sql += " WHERE Action_date Is Not NULL  " & Environment.NewLine
        sql += " Order BY Action_date asc   " & Environment.NewLine

        Dim DA As New SqlDataAdapter(sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetStartDate_YearData(DocYear As Integer) As DataTable
        Dim Year As String = ""
        If DocYear > 0 Then
            Year = DocYear.ToString()
        End If
        Dim sql As String = ""
        sql += " Select DISTINCT Convert(Date, Action_date-5) Action_date FROM ( " & Environment.NewLine
        sql += " Select pp.plan_critical_date Action_date from  LAW_Document_Plan_Paper pp   inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id  inner join LAW_Document_Plan p On p.document_plan_id=pp.document_plan_id  AND percent_weight >0  where p.document_year LIKE '%" & Year & "%'  " & Environment.NewLine
        sql += " UNION ALL " & Environment.NewLine
        sql += " Select pp.upload_date Action_date from  LAW_Document_Plan_Paper pp   inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id  inner join LAW_Document_Plan p On p.document_plan_id=pp.document_plan_id  AND percent_weight >0   where p.document_year LIKE '%" & Year & "%'   " & Environment.NewLine
        sql += " )As TB  " & Environment.NewLine
        sql += " WHERE Action_date Is Not NULL  " & Environment.NewLine
        sql += " Order BY Action_date asc   " & Environment.NewLine

        Dim DA As New SqlDataAdapter(sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    Public Function GetDocumentChartYearData(DocYear As Integer) As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("action_date", GetType(Date))
        dt.Columns.Add("paper_name")
        dt.Columns.Add("plan_percent", GetType(Integer))
        dt.Columns.Add("actual_percent", GetType(Integer))


        Dim sql As String = "Select Convert(Date,pp.plan_critical_date) plan_date, Convert(Date,pp.upload_date) actual_date, sp.paper_name, "
        sql += "  (pp.percent_complete * p.percent_weight * 1.0)/100 plan_percent, "
        sql += " Case When  pp.upload_date Is null Then 0 Else (pp.percent_complete * p.percent_weight * 1.0)/100 End actual_percent "
        sql += " from  LAW_Document_Plan_Paper pp  "
        sql += " inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id "
        sql += " inner join LAW_Document_Plan p On p.document_plan_id=pp.document_plan_id "
        sql += " where p.document_year=" & DocYear
        sql += " order by plan_critical_date "

        Dim PlanDT As DataTable = BL.Execute_DataTable(sql)

        Return PlanDT
    End Function

    Public Function GetDocumentChartYearData_Plan(DocYear As Integer, DocTemplateName As String, plant_id As Integer, tag_id As Integer) As DataTable
        Dim dt As New DataTable
        Dim Year As String = ""
        If DocYear > 0 Then
            Year = DocYear.ToString()
        End If

        Dim sql As String = ""
        sql += " SELECT plan_date , paper_name ,SUM(plan_percent) plan_percent  "
        sql += " FROM ( "
        sql += " Select pp.plan_critical_date plan_date, sp.paper_name "
        sql += " ,   (pp.percent_complete * p.percent_weight * 1.0)/100 plan_percent "
        sql += " ,  Case When  pp.upload_date Is null Then 0 Else (pp.percent_complete * p.percent_weight * 1.0)/100 End actual_percent   "
        sql += " from  LAW_Document_Plan_Paper pp   "
        sql += " inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id   "


        sql += " inner join LAW_Document_Plan p On p.document_plan_id=pp.document_plan_id  AND percent_weight >0  "
        If DocTemplateName <> "" Then
            sql += "            AND  p.document_name = '" & DocTemplateName.Replace("'", "''") & "'"
        End If

        If plant_id > 0 Then
            sql += "            AND p.plant_id =" & plant_id & ""
        End If
        If tag_id > 0 Then
            sql += "            AND p.tag_id =" & tag_id & ""
        End If

        sql += " where p.document_year Like '%" & Year & "%'  "

        sql += " ) As TB GROUP BY plan_date,paper_name "


        Dim PlanDT As DataTable = BL.Execute_DataTable(sql)

        Return PlanDT
    End Function

    Public Function GetDocumentChartYearData_Actual(DocYear As Integer, DocTemplateName As String, plant_id As Integer, tag_id As Integer) As DataTable
        Dim dt As New DataTable
        Dim Year As String = ""
        If DocYear > 0 Then
            Year = DocYear.ToString()
        End If
        Dim sql As String = ""
        sql += " Select   Convert(Date,actual_date) actual_date , paper_name ,SUM(actual_percent) actual_percent   "
        sql += " FROM ( "
        sql += " Select  Convert(Date,pp.upload_date) actual_date, sp.paper_name "
        sql += " , Case When  pp.upload_date Is null Then 0 Else (pp.percent_complete * p.percent_weight * 1.0)/100 End actual_percent  "

        sql += " from  LAW_Document_Plan_Paper pp   "
        sql += " inner join LAW_Document_Setting_Paper sp On sp.document_setting_paper_id=pp.document_setting_paper_id   "


        sql += " inner join LAW_Document_Plan p On p.document_plan_id=pp.document_plan_id   AND percent_weight >0  "
        If DocTemplateName <> "" Then
            sql += "            AND  p.document_name = '" & DocTemplateName.Replace("'", "''") & "'"
        End If
        If plant_id > 0 Then
            sql += "            AND p.plant_id =" & plant_id & ""
        End If
        If tag_id > 0 Then
            sql += "            AND p.tag_id =" & tag_id & ""
        End If

        sql += " where p.document_year  LIKE '%" & Year & "%'  "

        sql += " ) As TB  "
        sql += " Where actual_date Is Not NULL "
        sql += " GROUP BY actual_date,paper_name "


        Dim ActualDT As DataTable = BL.Execute_DataTable(sql)

        Return ActualDT
    End Function

    Private Function GetSumDocumentPlanProgress(DocumentPlanID As Long) As DataTable
        Dim sql As String = "Select convert(varchar(6), pp.plan_notice_date,112) plan_notice_month , sum(pp.percent_complete) plan_value " & Environment.NewLine
        sql += " from LAW_Document_Plan_paper pp " & Environment.NewLine
        sql += " where pp.document_plan_id=" & DocumentPlanID & Environment.NewLine
        sql += " group by convert(varchar(6), pp.plan_notice_date,112)"

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        Return dt
    End Function

    Private Function GetSumDocumentActualProgress(DocumentPlanID As Long) As DataTable
        Dim sql As String = "Select convert(varchar(6), pp.upload_date,112) upload_month, sum(pp.percent_complete) actual_value " & Environment.NewLine
        sql += " from LAW_Document_Plan_paper pp " & Environment.NewLine
        sql += " where pp.document_plan_id=" & DocumentPlanID & Environment.NewLine
        sql += " group by convert(varchar(6), pp.upload_date,112)"

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        Return dt
    End Function

    Public Function UpdatePlanPercentWeight(LawDocumentPlanID As Long, PercentWeight As Integer) As String
        Dim ret As String = "False"
        Dim sql As String = "update LAW_Document_Plan " & Environment.NewLine
        sql += " Set percent_weight=" & PercentWeight & Environment.NewLine
        sql += " where document_plan_id=" & LawDocumentPlanID & Environment.NewLine

        ret = BL.Execute_Command(sql)

        Return ret
    End Function
#End Region

#Region "Law Organize"
    Public Function GetListLawOrg() As DataTable
        Dim sql As String = "Select law_organize_id, org_name, active_status, update_time "
        sql += " from LAW_Organize "
        sql += " order by org_name "

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        Return dt
    End Function
    Public Function UpdateLawORGStatus(LawOrgID As Long) As String
        Dim sql As String = "update LAW_Organize Set active_status=Case active_status When 1 Then 0 Else 1 End where law_organize_id=" & LawOrgID
        Return BL.Execute_Command(sql)
    End Function

    Public Function CheckDuplicateLawOrgName(LawOrgID As Long, OrgName As String) As String
        Dim ret As String = "False"
        Dim sql As String = "Select top 1 law_organize_id "
        sql += " from LAW_Organize "
        sql += " where org_name='" & OrgName.Replace("'", "''") & "' And law_organize_id<> " & LawOrgID

        Dim dt As DataTable = BL.Execute_DataTable(sql)
        If dt.Rows.Count > 0 Then
            ret = "Duplicate Organize Name " & OrgName
        Else
            ret = "False"
        End If
        Return ret
    End Function

    Public Function SaveLawOrganization(LawOrgID As Long, OrgName As String, ActiveStatus As Integer, UserName As Integer) As String
        Dim ret As String = ""
        Dim sql As String = ""

        If LawOrgID = 0 Then
            sql = "insert into LAW_Organize(org_name, active_status, update_time, update_by)"
            sql += " output inserted.law_organize_id "
            sql += " values('" & OrgName.Replace("'", "''") & "', " & ActiveStatus & ", getdate(), " & UserName & ")"


            Dim dt As DataTable = BL.Execute_DataTable(sql)
            If dt.Rows.Count > 0 Then
                ret = "True|" & dt.Rows(0)("law_organize_id")
            End If
        Else
            sql = " update LAW_Organize "
            sql += " Set org_name= '" & OrgName.Replace("'", "''") & "'"
            sql += ", active_status= " & ActiveStatus
            sql += ", update_time=getdate() "
            sql += ", update_by= " & UserName
            sql += " output inserted.law_organize_id"
            sql += " where law_organize_id=" & LawOrgID

            Dim dt As DataTable = BL.Execute_DataTable(sql)
            If dt.Rows.Count > 0 Then
                ret = "True|" & dt.Rows(0)("law_organize_id")
            End If
        End If

        Return ret
    End Function

    Public Function GetDataLawOrganize(LawOrgID As Long) As DataTable
        Dim sql As String = "Select law_organize_id, org_name, active_status "
        sql += " from LAW_Organize "
        sql += " where law_organize_id=" & LawOrgID
        Dim dt As DataTable = BL.Execute_DataTable(sql)
        Return dt
    End Function
#End Region

#Region "Search Function"
    Public Function SearchDocumentFile(wh As String) As DataTable
        Dim Sql As String = "Select d.folder_id, d.document_id, 0 file_id, d.document_name, d.document_description, d.file_ext, d.notice_date, d.critical_date, d.actual_date, '" & TreeviewNodeType.DocumentMenu.ToString & "' node_type " & Environment.NewLine
        Sql += " from law_document d " & Environment.NewLine
        Sql += " inner join law_folder f on f.folder_id=d.folder_id " & Environment.NewLine
        Sql += " where (d.document_name like '%" & wh.Replace("'", "''") & "%' " & Environment.NewLine
        Sql += " or d.document_description like '%" & wh.Replace("'", "''") & "%' " & Environment.NewLine
        Sql += " or d.original_file_name like '%" & wh.Replace("'", "''") & "%') " & Environment.NewLine

        Sql += " Union all " & Environment.NewLine
        Sql += " select f.folder_id, 0 document_id, 0 file_id, f.folder_name , f.folder_description, '' file_ext, null notice_date, null critical_date, null actual_date, '" & TreeviewNodeType.FolderMenu.ToString & "' node_type " & Environment.NewLine
        Sql += " from law_folder f " & Environment.NewLine
        Sql += " where (f.folder_name like '%" & wh.Replace("'", "''") & "%' or f.folder_description like '%" & wh.Replace("'", "''") & "%' )" & Environment.NewLine

        Dim dt As DataTable = BL.Execute_DataTable(Sql)
        If dt.Rows.Count > 0 Then
            dt.Columns.Add("folder_path")

            For i As Integer = 0 To dt.Rows.Count - 1
                Dim FolderPath As String = GetFolderPath(dt.Rows(i)("folder_id"))
                Dim vName As String = ""

                If FolderPath.EndsWith("/") = False Then
                    FolderPath += "/"
                End If

                If dt.Rows(i)("node_type") = TreeviewNodeType.DocumentMenu.ToString Then
                    Dim IconURL As String = ""
                    If Convert.IsDBNull(dt.Rows(i)("file_ext")) = False Then
                        IconURL = GetIconImageUrl(dt.Rows(i)("file_ext"))
                    Else
                        IconURL = "resources/images/icons/icon_blank_file.png"
                    End If

                    vName = "<img src='" & IconURL & "'  border='0' />" & dt.Rows(i)("document_name")
                ElseIf dt.Rows(i)("node_type") = TreeviewNodeType.FolderMenu.ToString Then
                    vName = "<img src='resources/images/icons/icon-Folder_close.png' border='0' />" & dt.Rows(i)("document_name")
                End If

                dt.Rows(i)("folder_path") = FolderPath
                dt.Rows(i)("document_name") = vName
            Next
        End If

        Return dt
    End Function
#End Region

    Private Function ToQueryDate(ByVal InputDate As DateTime) As String
        Dim C As New Converter
        If InputDate.Year = 1 Then
            Return "null"
        Else
            Return "'" & C.DateToString(InputDate, "yyyy-MM-dd") & "'"
        End If
    End Function

    Public Function GetNewOrganizeID() As Integer
        Dim SQL As String = "SELECT ISNULL(Max(LawOrgID),0)+1  FROM LAW_Organize"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

End Class




#Region "Enumulation"

Public Enum TreeviewNodeType
    RootMenu = 0
    FolderMenu = 1
    DocumentMenu = 2
    FileMenu = 3
End Enum



Public Enum LawDialogAction
    newFolder = 1
    duplicateFolder = 2
    editFolder = 3
    deleteFolder = 4
    newDocument = 5
    editDocument = 6
    deleteDocument = 7
    copyDocument = 8
    DownloadFolder = 9
End Enum
#End Region
