﻿Imports System
Imports System.Configuration
Imports System.Drawing
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports iTextSharp.text.pdf
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic

Public Class EIR_BL

    Public ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Public ServerMapPath As String = ConfigurationManager.AppSettings("ServerMapPath").ToString
    Public Picture_Path As String = ConfigurationManager.AppSettings("Picture_Path").ToString
    Public PostedReport_Path As String = ConfigurationManager.AppSettings("PostedReport_Path").ToString
    Public ReportPermissionManager As New ReportPermission
    Public PTT_LDAP As String = ConfigurationManager.AppSettings("PTT_LDAP").ToString
    Public PISConnection As String = ConfigurationManager.ConnectionStrings("PISConnectionString").ConnectionString

    Public Enum Report_Type
        Unknown = 0
        Stationary_Routine_Report = 1
        Stationary_Off_Routine_Report = 2
        Rotating_Routine_Report = 3
        Rotating_Off_Routine_Report = 4
        Spring_Hanger_Report = 5
        Lube_Oil_Report = 6
        PdMA_Report = 7
        Thermography_Report = 8
        MTap_Report = 9
        PdMA_MTap_Report = 10
        Turnaround_Inspection_Reports = 11

        Pipe_CUI_Reports = 20
        Pipe_ERO_Reports = 21
        Pipe_Routine_Reports = 22
        All = 99
    End Enum

    Public Enum ReportName_Problem
        NEW_PROBLEM = 1
        TOTAL_PROBLEM = 2
        TOTAL_PROBLEM_AREA = 3
    End Enum

    Public Enum Dashboard
        Current_Status = 1
        Annual_Progress = 2
        Problem_Improved_By_Plant = 3
        Problem_Improvement_By_Year = 4
        New_Problem_Occurred = 5
        Total_Problem_by_month = 6
    End Enum

    Public Enum Tag_Class
        Stationary = 1
        Rotating = 2
    End Enum

    Public Enum Warning
        Normal = 0
        Warning = 1
        Alert = 2
    End Enum

    Public Enum THM_Type
        INSTRUMENT = 1
        ELECTRICAL = 2
        PROCESS = 3
        POWER_TURBINE_EXHAUST = 4
        OTHER = 0
    End Enum

    Public Enum ISSUE
        Unknows = -1
        No_Problem = 0
        New_Problem = 1
        Not_Fix = 2
        Fixed_Completed = 3
        Fixed_Incomplete = 4
    End Enum

#Region "Formating"

    Public Function Is_Extentsion_Image(ByVal EXT_ID As FileAttachment.ExtensionType) As Boolean
        Select Case EXT_ID
            Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG,
                     FileAttachment.ExtensionType.PNG, FileAttachment.ExtensionType.TIFF,
                 FileAttachment.ExtensionType.SVG
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Function Get_FILE_EXTENSION_CONTENT_TYPE(ByVal EXTENSION As FileAttachment.ExtensionType) As String
        Dim DT As DataTable = Get_FILE_EXTENSION_INFO()
        DT.DefaultView.RowFilter = "EXT_ID=" & CInt(EXTENSION)
        If DT.DefaultView.Count > 0 AndAlso Not IsDBNull(DT.DefaultView(0).Item("Content_Type")) Then
            Return DT.DefaultView(0).Item("Content_Type").ToString
        Else
            Return ""
        End If
    End Function

    Public Function GetImageContentType(ByVal Image As System.Drawing.Image) As String
        Select Case Image.RawFormat.Guid
            Case Imaging.ImageFormat.Bmp.Guid
                Return "image/x-ms-bmp"
            Case Imaging.ImageFormat.Jpeg.Guid
                Return "image/jpeg"
            Case Imaging.ImageFormat.Gif.Guid
                Return "image/gif"
            Case Imaging.ImageFormat.Png.Guid
                Return "image/png"
            Case Else
                Return ""
        End Select
    End Function

    Public Enum Report_Step
        New_Step = 0
        Collecting_Step = 1
        Inspecting_Step = 2
        Approving_Step = 3
        Approved_Step = 4
    End Enum

    Public Enum User_Level
        PTT_Authenticated = -1
        Administrator = 0
        Collector = 1
        Inspector = 2
        Engineer = -2 ' -------------- Add ----------------
        Approver = 3
        Viewer = 4
    End Enum

    Public Function IsProgrammingDate(ByVal Input As String) As Boolean
        Try
            Dim Temp As Date = DateTime.Parse(Input)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function IsFormatFileName(ByVal FileName As String) As Boolean
        Dim ExceptChars As String = "/\:*?""<>|;"
        For i As Integer = 1 To Len(ExceptChars)
            If InStr(FileName, Mid(ExceptChars, i)) > 0 Then
                Return False
            End If
        Next
        Return True
    End Function

    Public Function ReportGridDescription(ByVal Input As String, ByVal Limit As Integer) As String
        If Len(Input) > Limit Then
            Return Input.Substring(0, Limit)
        Else
            Return Input
        End If
    End Function

    Public Function ReportGridTime(ByVal Input As DateTime) As String
        'Return Input.Day.ToString.PadLeft(2, "0") & "-" & Input.Month.ToString.PadLeft(2, "0") & "-" & Input.Year
        Return Input.ToString("dd-MMM-yyyy")
    End Function

    Public Function ReportProgrammingDate(ByVal Input As DateTime) As String
        Return Input.Year & "-" & Input.Month.ToString.PadLeft(2, "0") & "-" & Input.Day.ToString.PadLeft(2, "0")
    End Function

    Public Enum InspectionLevel
        All = 99
        Unknown = -1
        Normal = 0
        ClassC = 1
        ClassB = 2
        ClassA = 3
    End Enum

    Public Enum VibrationZone
        ZoneA = 0
        ZoneB = 1
        ZoneC = 2
        ZoneD = 3
    End Enum

    Public Function Get_Vibration_Zone(ByVal Unit As String, ByVal Position As String, ByVal Category As String, ByVal Value As Double) As String
        Select Case Unit
            Case "g's (Pk)"
                If Value < 0 Then
                    Return ""
                ElseIf Value <= 0.18 Then
                    Return "A1"
                ElseIf Value <= 0.28 Then
                    Return "A2"
                ElseIf Value <= 0.45 Then
                    Return "B1"
                ElseIf Value <= 0.72 Then
                    Return "B2"
                ElseIf Value <= 1.13 Then
                    Return "C1"
                ElseIf Value <= 1.79 Then
                    Return "C2"
                ElseIf Value <= 2.84 Then
                    Return "D1"
                ElseIf Value <= 4.5 Then
                    Return "D2"
                ElseIf Value <= 7.14 Then
                    Return "D3"
                ElseIf Value <= 11.31 Then
                    Return "D4"
                ElseIf Value > 11.31 Then
                    Return "D5"
                End If
            Case "mm/sec"
                Position = Trim(Position).ToUpper.Replace(" ", "")
                Category = Trim(Category).ToUpper.Replace(" ", "")
                If Position.Length < 3 Then Return ""

                '------------- Classify Equipe ----------
                Dim EquipeType As String = ""
                If Position.Substring(2, 1) = "P" Then '-------------- Pump ----------------
                    EquipeType = "Pump"
                ElseIf Position.Substring(1, 1) = "S" Or Position.Substring(2, 1) = "S" Then '-------------- Fan ----------------
                    EquipeType = "Fan"
                ElseIf Position.Substring(2, 1) = "M" Then '-------------- Motor ----------------
                    EquipeType = "Motor"
                Else
                    Return ""
                End If
                '------------- Classify Length ----------
                If Value < 0 Then
                    Return ""
                ElseIf Value <= 0.3 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "A1"
                        Case "Fan"
                            Return "A1"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "A1"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "A1"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "A1"
                            End If
                    End Select
                ElseIf Value <= 0.71 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "A2"
                        Case "Fan"
                            Return "A2"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "A2"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "A2"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "A2"
                            End If
                    End Select
                ElseIf Value <= 1.12 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "A3"
                        Case "Fan"
                            Return "A3"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "A3"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "A3"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "B1"
                            End If
                    End Select
                ElseIf Value <= 1.8 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "A4"
                        Case "Fan"
                            Return "A4"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "A4"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "B1"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "B2"
                            End If
                    End Select
                ElseIf Value <= 2.8 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "B1"
                        Case "Fan"
                            Return "B1"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "B1"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "B2"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "C1"
                            End If
                    End Select
                ElseIf Value <= 4.5 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "B2"
                        Case "Fan"
                            Return "B2"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "B2"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "C1"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "C2"
                            End If
                    End Select
                ElseIf Value <= 7.1 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "C1"
                        Case "Fan"
                            Return "B3"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "C1"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "C2"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "D1"
                            End If
                    End Select
                ElseIf Value <= 11.2 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "D1"
                        Case "Fan"
                            Return "C1"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "C2"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "D1"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "D2"
                            End If
                    End Select
                ElseIf Value <= 18 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "D2"
                        Case "Fan"
                            Return "D1"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "D1"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "D2"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "D3"
                            End If
                    End Select
                ElseIf Value <= 28 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "D3"
                        Case "Fan"
                            Return "D2"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "D2"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "D3"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "D4"
                            End If
                    End Select
                ElseIf Value > 28 Then
                    Select Case EquipeType
                        Case "Pump"
                            Return "D4"
                        Case "Fan"
                            Return "D3"
                        Case "Motor"
                            If Category.IndexOf("CLASSIII") > -1 Then
                                Return "D3"
                            ElseIf Category.IndexOf("CLASSII") > -1 Then
                                Return "D4"
                            ElseIf Category.IndexOf("CLASSI") > -1 Then
                                Return "D5"
                            End If
                    End Select
                End If
        End Select
        Return ""
    End Function

    Public Function GetAllDocumentByReport(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM RPT_OffRoutine_Doc WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " ORDER BY DOC_ID", ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_User_Level_Name(ByVal InputStep As User_Level) As String
        Select Case InputStep
            Case User_Level.Administrator
                Return "Administrator"
            Case User_Level.Collector
                Return "Collector"
            Case User_Level.Inspector
                Return "Inspector"
            Case User_Level.Approver
                Return "Approver"
            Case User_Level.Viewer
                Return "Viewer"
            Case Else
                Return "PTT Authenticated"
        End Select
    End Function

    Public Function Get_User_Level_Color(ByVal InputStep As User_Level) As Color
        Select Case InputStep
            Case User_Level.Administrator
                Return Color.Red
            Case User_Level.Collector
                Return Color.Orange
            Case User_Level.Inspector
                Return Color.SteelBlue
            Case User_Level.Approver
                Return Color.Violet
            Case User_Level.Viewer
                Return Color.Green
            Case Else
                Return Color.Black
        End Select
    End Function

    Public Function Get_Problem_Level_Name(ByVal LEVEL As InspectionLevel) As String
        Select Case LEVEL
            Case InspectionLevel.Normal
                Return "Normal"
            Case InspectionLevel.ClassC
                Return "ClassC"
            Case InspectionLevel.ClassB
                Return "ClassB"
            Case InspectionLevel.ClassA
                Return "ClassA"
            Case InspectionLevel.Unknown
                Return "Unknown"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_Inspection_Css_Box_By_Zone(ByVal Level As VibrationZone) As String
        Select Case Level
            Case VibrationZone.ZoneA
                Return "LevelZoneA"
            Case VibrationZone.ZoneB
                Return "LevelZoneB"
            Case VibrationZone.ZoneC
                Return "LevelZoneC"
            Case VibrationZone.ZoneD
                Return "LevelZoneD"
            Case Else
                Return "LevelUnknowZone"
        End Select
    End Function

    Public Function Get_Inspection_Zone_By_Box_Css(ByVal CSS As String) As VibrationZone
        Select Case CSS.ToUpper
            Case "LevelZoneA".ToUpper
                Return VibrationZone.ZoneA
            Case "LevelZoneB".ToUpper
                Return VibrationZone.ZoneB
            Case "LevelZoneC".ToUpper
                Return VibrationZone.ZoneC
            Case "LevelZoneD".ToUpper
                Return VibrationZone.ZoneD
            Case Else
                Return -1
        End Select
    End Function

    Public Function Get_Inspection_Css_Text_By_Zone(ByVal Level As VibrationZone) As String
        Select Case Level
            Case VibrationZone.ZoneA
                Return "TextZoneA"
            Case VibrationZone.ZoneB
                Return "TextZoneB"
            Case VibrationZone.ZoneC
                Return "TextZoneC"
            Case VibrationZone.ZoneD
                Return "TextZoneD"
            Case Else
                Return "TextDeselect"
        End Select
    End Function

    Public Function Get_Inspection_Zone_By_Text_Css(ByVal CSS As String) As VibrationZone
        Select Case CSS.ToUpper
            Case "TextZoneA".ToUpper
                Return VibrationZone.ZoneA
            Case "TextZoneB".ToUpper
                Return VibrationZone.ZoneB
            Case "TextZoneC".ToUpper
                Return VibrationZone.ZoneC
            Case "TextZoneD".ToUpper
                Return VibrationZone.ZoneD
            Case Else
                Return -1
        End Select
    End Function

    Public Function Get_Inspection_Css_Box_By_Level(ByVal Level As InspectionLevel) As String
        Select Case Level
            Case InspectionLevel.Normal
                Return "LevelNormal"
            Case InspectionLevel.ClassC
                Return "LevelClassC"
            Case InspectionLevel.ClassB
                Return "LevelClassB"
            Case InspectionLevel.ClassA
                Return "LevelClassA"
            Case Else
                Return "LevelDeselect"
        End Select
    End Function

    Public Function Get_Inspection_Level_By_Box_Css(ByVal CSS As String) As InspectionLevel
        Select Case CSS.ToUpper
            Case "LevelNormal".ToUpper
                Return InspectionLevel.Normal
            Case "LevelClassC".ToUpper
                Return InspectionLevel.ClassC
            Case "LevelClassB".ToUpper
                Return InspectionLevel.ClassB
            Case "LevelClassA".ToUpper
                Return InspectionLevel.ClassA
            Case Else
                Return -1
        End Select
    End Function

    Public Function Get_Inspection_Css_Text_By_Level(ByVal Level As InspectionLevel) As String
        Select Case Level
            Case InspectionLevel.Normal
                Return "TextNormal"
            Case InspectionLevel.ClassC
                Return "TextClassC"
            Case InspectionLevel.ClassB
                Return "TextClassB"
            Case InspectionLevel.ClassA
                Return "TextClassA"
            Case Else
                Return "TextDeselect"
        End Select
    End Function

    Public Function Get_Inspection_Level_By_Text_Css(ByVal CSS As String) As InspectionLevel
        Select Case CSS.ToUpper
            Case "TextClassC".ToUpper
                Return InspectionLevel.ClassC
            Case "TextClassB".ToUpper
                Return InspectionLevel.ClassB
            Case "TextClassA".ToUpper
                Return InspectionLevel.ClassA
            Case "TextNormal".ToUpper
                Return InspectionLevel.Normal
            Case Else
                Return -1
        End Select
    End Function



    Public Function Get_Lube_Oil_TAN_Css(ByVal TAN As Double) As String
        Select Case True
            Case TAN > 5
                Return "LO_Very_Hight"
            Case TAN > 3
                Return "LO_Hight"
            Case TAN >= 1.5
                Return "LO_Alarm"
            Case TAN >= 1
                Return "LO_Warning"
            Case Else
                Return "LO_Normal"
        End Select
    End Function

    Public Function Get_Lube_Oil_ANTI_OX_Css(ByVal OX_Value As Double) As String
        Select Case True
            Case OX_Value <= 10
                Return "LO_Very_Hight"
            Case OX_Value <= 30
                Return "LO_Hight"
            Case OX_Value <= 60
                Return "LO_Alarm"
            Case OX_Value <= 80
                Return "LO_Warning"
            Case Else
                Return "LO_Normal"
        End Select
    End Function

    Public Function Get_Lube_Oil_OX_Css(ByVal OX_Value As Double) As String
        Select Case True
            Case OX_Value < 20
                Return "LO_Normal"
            Case OX_Value < 30
                Return "LO_Alarm"
            Case Else
                Return "LO_Hight"
        End Select
    End Function

    Public Function Get_Lube_Oil_Varnish_Css(ByVal Varnish As Integer) As String
        Select Case True
            Case Varnish < 15
                Return "LO_Normal"
            Case Varnish <= 30
                Return "LO_Alarm"
            Case Varnish <= 40
                Return "LO_Abnormal"
            Case Varnish > 40
                Return "LO_Hight"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_Lube_Water_Mineral_Css(ByVal Water_Value As Double) As String
        Select Case True
            Case Water_Value < 300
                Return "LO_Normal"
            Case Water_Value < 750
                Return "LO_Alarm"
            Case Else
                Return "LO_Hight"
        End Select
    End Function

    Public Function Get_Lube_Water_Synthetic_Css(ByVal Water_Value As Double) As String
        Select Case True
            Case Water_Value < 1000
                Return "LO_Normal"
            Case Water_Value < 1500
                Return "LO_Alarm"
            Case Else
                Return "LO_Hight"
        End Select
    End Function

#End Region

#Region "Basic Binding"

    Public Sub BindCLB_Plant(ByRef clb As CheckBoxList, Optional ByVal PLANT As Integer() = Nothing)
        Dim SQL As String = "SELECT * FROM MS_PLANT WHERE active_status=1 ORDER BY PLANT_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        clb.Items.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PLANT_CODE"), DT.Rows(i).Item("PLANT_ID"))
            clb.Items.Add(Item)
            If Not IsNothing(PLANT) AndAlso Array.IndexOf(PLANT, DT.Rows(i).Item("PLANT_ID")) > -1 Then
                clb.Items(clb.Items.Count - 1).Selected = True
            End If
        Next
    End Sub

    Public Sub BindCLB_Skill(ByRef clb As CheckBoxList, Optional ByVal SKILL As String() = Nothing)
        clb.Items.Clear()
        '------------------ Stationary -----------
        clb.Items.Add(New ListItem("Stationary"))
        If Not IsNothing(SKILL) AndAlso Array.IndexOf(SKILL, "Stationary") > -1 Then
            clb.Items(clb.Items.Count - 1).Selected = True
        End If

        '------------------ Rotating -----------
        clb.Items.Add(New ListItem("Rotating"))
        If Not IsNothing(SKILL) AndAlso Array.IndexOf(SKILL, "Rotating") > -1 Then
            clb.Items(clb.Items.Count - 1).Selected = True
        End If
    End Sub

    Public Sub BindDDlPlant(ByRef ddl As DropDownList, Optional ByVal PLANT_ID As Integer = 0)
        Dim SQL As String = "SELECT * FROM MS_PLANT WHERE active_status=1 ORDER BY PLANT_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Plant...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PLANT_CODE"), DT.Rows(i).Item("PLANT_ID"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
        If PLANT_ID <> 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = PLANT_ID.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub


    Public Sub BindDDlReportStep(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT * FROM MS_Report_Step"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Status...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("STEP_Name"), DT.Rows(i).Item("RPT_STEP"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Function Get_Stationary_Default_Inspection() As DataTable
        Dim SQL As String = "SELECT INSP_ID,INSP_Name,REF_INSP_ID,REF_STATUS_ID," & vbLf
        SQL &= " INSP_Order FROM MS_ST_Default_Inspection ORDER BY INSP_Order" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Rotating_Default_Inspection() As DataTable
        Dim SQL As String = "SELECT INSP_ID,INSP_Name,REF_INSP_ID,REF_STATUS_ID," & vbLf
        SQL &= " INSP_Order FROM MS_RO_Default_Inspection ORDER BY INSP_Order" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Stationary_Default_Inspection_Status() As DataTable
        Dim SQL As String = "SELECT * " & vbNewLine
        SQL &= " FROM MS_ST_Default_Inspection_Status " & vbNewLine
        SQL &= " ORDER BY STATUS_Order" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Rotating_Default_Inspection_Status() As DataTable
        Dim SQL As String = "SELECT * " & vbNewLine
        SQL &= " FROM MS_RO_Default_Inspection_Status " & vbNewLine
        SQL &= " ORDER BY STATUS_Order" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Stationary_TagType_Inspection(ByVal TypeID As Integer) As DataTable
        Dim SQL As String = "SELECT * " & vbNewLine
        SQL &= " FROM MS_ST_TAG_Inspection " & vbNewLine
        SQL &= " WHERE Tag_Type_ID =" & TypeID & vbNewLine
        SQL &= " ORDER BY INSP_ID,STATUS_ID" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Rotating_TagType_Inspection(ByVal TypeID As Integer) As DataTable
        Dim SQL As String = "SELECT * " & vbNewLine
        SQL &= " FROM MS_RO_TAG_Inspection " & vbNewLine
        SQL &= " WHERE Tag_Type_ID =" & TypeID & vbNewLine
        SQL &= " ORDER BY INSP_ID,STATUS_ID" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Sub BindDDl_ST_Tag(ByRef ddl As DropDownList, Optional ByVal PLANT_ID As Integer = 0, Optional ByVal ROUTE_ID As Integer = 0, Optional ByVal AREA_ID As Integer = 0, Optional ByVal PROC_ID As Integer = 0, Optional ByVal TAG_TYPE_ID As Integer = 0, Optional ByVal SelectedValue As Integer = -1)
        Dim DT As DataTable = Get_Active_Stationary_Tag(PLANT_ID, ROUTE_ID, AREA_ID, PROC_ID, TAG_TYPE_ID)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Tag...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TAG_CODE"), DT.Rows(i).Item("TAG_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_RO_Tag(ByRef ddl As DropDownList, Optional ByVal PLANT_ID As Integer = 0, Optional ByVal ROUTE_ID As Integer = 0, Optional ByVal AREA_ID As Integer = 0, Optional ByVal PROC_ID As Integer = 0, Optional ByVal TAG_TYPE_ID As Integer = 0, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim DT As DataTable = Get_Active_Rotating_Tag(PLANT_ID, ROUTE_ID, AREA_ID, PROC_ID, TAG_TYPE_ID)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Tag...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TAG_CODE"), DT.Rows(i).Item("TAG_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_ST_INSPStatus(ByRef ddl As DropDownList, ByVal TAG_TYPE_ID As Integer, ByVal INSP_ID As Integer, Optional ByVal SelectedValue As Integer = -1)

        Dim SQL As String = "SELECT MST.INSP_ID,INSP_Name,MST_STATUS.STATUS_ID,STATUS_Name" & vbNewLine
        SQL &= " FROM MS_ST_TAG_TYPE " & vbNewLine
        SQL &= " INNER JOIN MS_ST_TAG_Inspection MST ON MS_ST_TAG_TYPE.TAG_TYPE_ID=MST.TAG_TYPE_ID" & vbNewLine
        SQL &= " INNER JOIN MS_ST_Default_Inspection INSP ON MST.INSP_ID=INSP.INSP_ID" & vbNewLine
        SQL &= " INNER JOIN MS_ST_Default_Inspection_Status MST_STATUS ON MST.STATUS_ID=MST_STATUS.STATUS_ID" & vbNewLine
        SQL &= " WHERE MS_ST_TAG_TYPE.TAG_TYPE_ID =" & TAG_TYPE_ID & " And MS_ST_TAG_TYPE.Active_Status = 1 " & vbNewLine
        SQL &= " AND MST.INSP_ID=" & INSP_ID & vbNewLine
        SQL &= " ORDER BY MST_STATUS.STATUS_Order" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("STATUS_Name"), DT.Rows(i).Item("STATUS_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > -1 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Sub BindDDlVibrationZone(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1, Optional ByVal DisplayDefaultCaption As Boolean = True)

        ddl.Items.Clear()
        ddl.Items.Clear()
        If DisplayDefaultCaption Then
            ddl.Items.Add(New ListItem("Level...", -1))
        Else
            ddl.Items.Add(New ListItem("", -1))
        End If

        ddl.Items.Add(New ListItem("Zone A", 0))
        ddl.Items.Add(New ListItem("Zone B", 1))
        ddl.Items.Add(New ListItem("Zone C", 2))
        ddl.Items.Add(New ListItem("Zone D", 3))

        If ddl.SelectedIndex < 1 And SelectedValue > -1 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Sub BindDDl_RO_INSPStatus(ByRef ddl As DropDownList, ByVal TAG_TYPE_ID As Integer, ByVal INSP_ID As Integer, Optional ByVal SelectedValue As Integer = -1)

        Dim SQL As String = "SELECT MST.INSP_ID,INSP_Name,MST_STATUS.STATUS_ID,STATUS_Name" & vbNewLine
        SQL &= " FROM MS_RO_TAG_TYPE " & vbNewLine
        SQL &= " INNER JOIN MS_RO_TAG_Inspection MST ON MS_RO_TAG_TYPE.TAG_TYPE_ID=MST.TAG_TYPE_ID" & vbNewLine
        SQL &= " INNER JOIN MS_RO_Default_Inspection INSP ON MST.INSP_ID=INSP.INSP_ID" & vbNewLine
        SQL &= " INNER JOIN MS_RO_Default_Inspection_Status MST_STATUS ON MST.STATUS_ID=MST_STATUS.STATUS_ID" & vbNewLine
        SQL &= " WHERE MS_RO_TAG_TYPE.TAG_TYPE_ID =" & TAG_TYPE_ID & " And MS_RO_TAG_TYPE.Active_Status = 1 " & vbNewLine
        SQL &= " AND MST.INSP_ID=" & INSP_ID & vbNewLine
        SQL &= " ORDER BY MST_STATUS.STATUS_Order" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("STATUS_Name"), DT.Rows(i).Item("STATUS_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > -1 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Sub BindDDlUserLevel(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_LEVEL --WHERE LEVEL_ID>0"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Level...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("LEVEL_NAME"), DT.Rows(i).Item("LEVEL_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > -1 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Sub BindDDlUserPosition(ByRef ddl As DropDownList, Optional ByVal POS_ID As Integer = -1)

        Dim SQL As String = "SELECT * FROM MS_User_Position ORDER BY POS_ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Position...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("POS_Name"), DT.Rows(i).Item("POS_ID"))
            ddl.Items.Add(Item)
        Next
        If POS_ID <> -1 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = POS_ID.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindCmbReportOfficerLubeOil(ByRef cmb As DropDownList,
                                    ByVal RPT_Year As Integer,
                                    ByVal RPT_No As Integer,
                                    ByVal Role As User_Level,
                                    Optional ByVal User_Full_Name As String = "")

        Dim SQL As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbLf
        SQL &= " DECLARE @RPT_No AS INT =" & RPT_No & vbLf
        SQL &= " SELECT DISTINCT * FROM" & vbLf
        SQL &= " (SELECT P.POS_ID,ISNULL(U.User_Prefix,'') + ISNULL(U.USER_Name,'') + ' ' + ISNULL(U.User_Surname,'') User_Full_Name" & vbLf
        SQL &= " FROM MS_USER U " & vbLf
        SQL &= " INNER JOIN MS_User_Coverage C ON U.USER_ID=C.USER_ID" & vbLf
        SQL &= " INNER JOIN MS_User_Position P ON U.POS_ID=P.POS_ID" & vbLf
        SQL &= " INNER JOIN MS_User_Skill S ON U.USER_ID=S.USER_ID" & vbLf

        Dim RoleName As String = ""
        Select Case Role
            Case User_Level.Collector '----------- Collected By -----------
                SQL &= " WHERE S.S_LubeOil=1 " & vbLf
                'SQL &= " AND U.LEVEL_ID IN (1,2) " & vbLf ' (Collector,Inspector)
                'SQL &= " AND U.POS_ID IN (0,1) " & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Collector,'') User_Full_Name FROM RPT_LO_Header " & vbLf

                RoleName = "Collector"

            Case User_Level.Inspector '----------- Inspected By -----------
                SQL &= " WHERE S.S_LubeOil=1 " & vbLf
                'SQL &= " AND U.LEVEL_ID IN (2) " & vbLf ' (Inspector)
                SQL &= " AND U.POS_ID IN (1,2,3)" & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Inspector,'') User_Full_Name FROM RPT_LO_Header " & vbLf

                RoleName = "Inspector"

            Case User_Level.Engineer '---------------- Engineer ---------
                SQL &= " WHERE S.S_LubeOil=1 " & vbLf
                SQL &= " AND U.POS_ID =2" & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Engineer,'') User_Full_Name FROM RPT_LO_Header " & vbLf

                RoleName = "Engineer"

            Case User_Level.Approver  '----------- Approved By -----------
                SQL &= " WHERE S.S_LubeOil=1 " & vbLf
                SQL &= " AND U.POS_ID IN (2,3)" & vbLf
                SQL &= " AND U.LEVEL_ID IN (3) " & vbLf ' (Approver)
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Analyst,'') User_Full_Name FROM RPT_LO_Header " & vbLf

                RoleName = "Approver"

        End Select
        SQL &= " WHERE RPT_Year=@RPT_Year AND RPT_No=@RPT_No" & vbLf
        SQL &= " ) A" & vbLf
        SQL &= " WHERE User_Full_Name<>''" & vbLf
        SQL &= " ORDER BY POS_ID DESC,User_Full_Name ASC" & vbLf

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim Col() As String = {"User_Full_Name"}
        DT = DT.DefaultView.ToTable(True, "User_Full_Name")

        cmb.Items.Clear()
        cmb.Items.Add(New ListItem("Choose " & RoleName & "...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("User_Full_Name"), DT.Rows(i).Item("User_Full_Name"))
            cmb.Items.Add(Item)
            If User_Full_Name = DT.Rows(i).Item("User_Full_Name") Then
                cmb.SelectedIndex = cmb.Items.Count - 1
            End If
        Next

    End Sub

    Public Sub BindCmbReportOfficer(ByRef cmb As DropDownList,
                                   ByVal RPT_Year As Integer,
                                   ByVal RPT_No As Integer,
                                   ByVal PLANT_ID As Integer,
                                   ByVal RPT_Type As Report_Type,
                                   ByVal Role As User_Level,
                                   Optional ByVal User_Full_Name As String = "")

        Dim Type_Filter As String = ""
        Dim HeaderTable As String = ""
        Select Case RPT_Type
            Case Report_Type.Stationary_Routine_Report, Report_Type.Stationary_Off_Routine_Report, Report_Type.Spring_Hanger_Report
                Type_Filter = "S.S_Stationary=1"
                HeaderTable = "RPT_ST_Header"
            Case Report_Type.Rotating_Routine_Report, Report_Type.Rotating_Off_Routine_Report
                Type_Filter = "S.S_Rotating=1"
                HeaderTable = "RPT_RO_Header"
            Case Report_Type.PdMA_Report
                Type_Filter = "S.S_PdMA=1"
                HeaderTable = "RPT_PdMA_Header"
            Case Report_Type.MTap_Report
                Type_Filter = "S.S_PdMA=1"
                HeaderTable = "RPT_MTAP_Header"
            Case Report_Type.Thermography_Report
                Type_Filter = "S.S_Thermography=1"
                HeaderTable = "RPT_THM_Header"
            Case Report_Type.Turnaround_Inspection_Reports
                Type_Filter = "S.S_Stationary=1"
                HeaderTable = "RPT_ST_TA_Header"
            Case Report_Type.Pipe_CUI_Reports
                Type_Filter = "S.S_Stationary=1"
                Type_Filter = "RPT_PIPE_CUI_Header"
            Case Report_Type.Pipe_ERO_Reports
                Type_Filter = "S.S_Stationary=1"
                Type_Filter = "RPT_PIPE_ERO_Header"
        End Select

        Dim SQL As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbLf
        SQL &= " DECLARE @RPT_No AS INT =" & RPT_No & vbLf
        SQL &= " DECLARE @PLANT_ID As INT =" & PLANT_ID & vbLf
        SQL &= " SELECT DISTINCT * FROM" & vbLf
        SQL &= " (SELECT P.POS_ID,ISNULL(U.User_Prefix,'') + ISNULL(U.USER_Name,'') + ' ' + ISNULL(U.User_Surname,'') User_Full_Name" & vbLf
        SQL &= " FROM MS_USER U " & vbLf
        SQL &= " INNER JOIN MS_User_Coverage C ON U.USER_ID=C.USER_ID" & vbLf
        SQL &= " INNER JOIN MS_User_Position P ON U.POS_ID=P.POS_ID" & vbLf
        SQL &= " INNER JOIN MS_User_Skill S ON U.USER_ID=S.USER_ID" & vbLf

        Dim RoleName As String = ""
        Select Case Role
            Case User_Level.Collector '----------- Collected By -----------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID AND " & Type_Filter & vbLf
                'SQL &= " AND U.LEVEL_ID IN (1,2) " & vbLf ' (Collector,Inspector)
                'SQL &= " AND U.POS_ID IN (0,1) " & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Collector,'') User_Full_Name FROM " & HeaderTable & vbLf

                RoleName = "Collector"

            Case User_Level.Inspector '----------- Inspected By -----------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID AND " & Type_Filter & vbLf
                'SQL &= " AND U.LEVEL_ID IN (2) " & vbLf ' (Inspector)
                SQL &= " AND U.POS_ID IN (1,2,3)" & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Inspector,'') User_Full_Name FROM " & HeaderTable & vbLf

                RoleName = "Inspector"

            Case User_Level.Engineer '---------------- Engineer ---------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID AND " & Type_Filter & vbLf
                SQL &= " AND U.POS_ID =2" & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Engineer,'') User_Full_Name FROM " & HeaderTable & vbLf

                RoleName = "Engineer"

            Case User_Level.Approver  '----------- Approved By -----------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID AND " & Type_Filter & vbLf
                SQL &= " AND U.POS_ID IN (2,3)" & vbLf
                SQL &= " AND U.LEVEL_ID IN (3) " & vbLf ' (Approver)
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Analyst,'') User_Full_Name FROM " & HeaderTable & vbLf

                RoleName = "Approver"

        End Select
        SQL &= " WHERE RPT_Year=@RPT_Year AND RPT_No=@RPT_No" & vbLf
        SQL &= " ) A" & vbLf
        SQL &= " WHERE User_Full_Name<>''" & vbLf
        SQL &= " ORDER BY POS_ID DESC,User_Full_Name ASC" & vbLf

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim Col() As String = {"User_Full_Name"}
        DT = DT.DefaultView.ToTable(True, "User_Full_Name")

        cmb.Items.Clear()
        cmb.Items.Add(New ListItem("Choose " & RoleName & "...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("User_Full_Name"), DT.Rows(i).Item("User_Full_Name"))
            cmb.Items.Add(Item)
            If User_Full_Name = DT.Rows(i).Item("User_Full_Name") Then
                cmb.SelectedIndex = cmb.Items.Count - 1
            End If
        Next
    End Sub




    Public Sub BindCmbST_TAReportOfficer(ByRef cmb As DropDownList,
                                   ByVal RPT_Year As Integer,
                                   ByVal RPT_No As Integer,
                                   ByVal PLANT_ID As Integer,
                                   ByVal Role As User_Level,
                                   Optional ByVal User_Full_Name As String = "")

        Dim SQL As String = "" & vbLf
        SQL &= " DECLARE @RPT_Year As INT =" & RPT_Year & vbLf
        SQL &= " DECLARE @RPT_No As INT =" & RPT_No & vbLf
        SQL &= " DECLARE @PLANT_ID As INT =" & PLANT_ID & vbLf
        SQL &= " SELECT DISTINCT * FROM" & vbLf
        SQL &= " (SELECT P.POS_ID,ISNULL(U.User_Prefix,'') + ISNULL(U.USER_Name,'') + ' ' + ISNULL(U.User_Surname,'') User_Full_Name" & vbLf
        SQL &= " FROM MS_USER U " & vbLf
        SQL &= " INNER JOIN MS_User_Coverage C ON U.USER_ID=C.USER_ID" & vbLf
        SQL &= " INNER JOIN MS_User_Position P ON U.POS_ID=P.POS_ID" & vbLf
        SQL &= " INNER JOIN MS_User_Skill S ON U.USER_ID=S.USER_ID" & vbLf

        Dim RoleName As String = ""
        Select Case Role
            Case User_Level.Collector '----------- Collected By -----------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID   " & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Collector,'') User_Full_Name FROM RPT_ST_TA_Header" & vbLf

                RoleName = "Collector"

            Case User_Level.Inspector '----------- Inspected By -----------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID   " & vbLf
                'SQL &= " AND U.LEVEL_ID IN (2) " & vbLf ' (Inspector)
                SQL &= " AND U.POS_ID IN (1,2,3)" & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Inspector,'') User_Full_Name FROM RPT_ST_TA_Header" & vbLf

                RoleName = "Inspector"

            Case User_Level.Engineer '---------------- Engineer ---------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID   " & vbLf
                SQL &= " AND U.POS_ID =2" & vbLf
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Engineer,'') User_Full_Name FROM RPT_ST_TA_Header" & vbLf

                RoleName = "Engineer"

            Case User_Level.Approver  '----------- Approved By -----------
                SQL &= " WHERE C.PLANT_ID=@PLANT_ID   " & vbLf
                SQL &= " AND U.POS_ID IN (2,3)" & vbLf
                SQL &= " AND U.LEVEL_ID IN (3) " & vbLf ' (Approver)
                SQL &= " Union ALL " & vbLf
                SQL &= " SELECT 0 POS_ID, ISNULL(Officer_Analyst,'') User_Full_Name FROM RPT_ST_TA_Header" & vbLf

                RoleName = "Approver"

        End Select
        SQL &= " WHERE  RPT_Year=@RPT_Year AND RPT_No=@RPT_No " & vbLf
        SQL &= " ) A" & vbLf
        SQL &= " WHERE User_Full_Name<>''" & vbLf
        SQL &= " ORDER BY POS_ID DESC,User_Full_Name ASC" & vbLf

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim Col() As String = {"User_Full_Name"}
        DT = DT.DefaultView.ToTable(True, "User_Full_Name")

        cmb.Items.Clear()
        cmb.Items.Add(New ListItem("Choose " & RoleName & "...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("User_Full_Name"), DT.Rows(i).Item("User_Full_Name"))
            cmb.Items.Add(Item)
            If User_Full_Name = DT.Rows(i).Item("User_Full_Name") Then
                cmb.SelectedIndex = cmb.Items.Count - 1
            End If
        Next
    End Sub

    Public Function GetUserDetail(ByVal USER_ID As Integer) As DataTable
        Dim SQL As String = "SELECT * FROM VW_USER_ALL_GROUP WHERE USER_ID=" & USER_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    Public Sub BindDDlLevel(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1, Optional ByVal DisplayDefaultCaption As Boolean = True)

        Dim SQL As String = "SELECT * FROM ISPT_Class ORDER BY ICLS_ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If DisplayDefaultCaption Then
            ddl.Items.Add(New ListItem("Level...", -1))
        Else
            ddl.Items.Add(New ListItem("", -1))
        End If

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("ICLS_Description"), DT.Rows(i).Item("ICLS_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > -1 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub

    Public Sub BindDDl_RPT_ST_TA_Tag(ByRef ddl As DropDownList, ByVal RPT_Year As Integer, ByVal rpt_No As Integer, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = " SELECT VW_ST_TA_TAG.TAG_ID , VW_ST_TA_TAG.TAG_CODE  , VW_ST_TA_TAG.tag_name " & vbLf
        SQL &= " From RPT_ST_TA_Detail  " & vbLf
        SQL &= " Left Join VW_ST_TA_TAG On VW_ST_TA_TAG.Tag_id = RPT_ST_TA_Detail.tag_id " & vbLf
        SQL &= " where RPT_Year = " & RPT_Year & " And rpt_No = " & rpt_No & " " & vbLf

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem(" ", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TAG_CODE") & " : " & DT.Rows(i).Item("tag_name"), DT.Rows(i).Item("TAG_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_RPT_ST_TA_INSP_ID(ByRef ddl As DropDownList, ByVal RPT_Year As Integer, ByVal rpt_No As Integer, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = " "
        SQL &= " Select   DISTINCT RPT_ST_TA_Detail_Step.INSP_ID , MS_ST_Default_Inspection.INSP_Name " & vbLf
        SQL &= " From RPT_ST_TA_Detail_Step    " & vbLf
        SQL &= " Left Join  MS_ST_Default_Inspection  On MS_ST_Default_Inspection.INSP_ID = RPT_ST_TA_Detail_Step.INSP_ID  " & vbLf

        SQL &= " where RPT_Year = " & RPT_Year & " And rpt_No = " & rpt_No & " " & vbLf

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem(" ", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("INSP_Name"), DT.Rows(i).Item("INSP_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_RPT_ST_TA_INSP_STATUS_ID(ByRef ddl As DropDownList, ByVal RPT_Year As Integer, ByVal rpt_No As Integer, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = " "
        SQL &= " Select   DISTINCT RPT_ST_TA_Detail_Step.INSP_STATUS_ID , MS_ST_Default_Inspection_Status.STATUS_Name " & vbLf
        SQL &= " From RPT_ST_TA_Detail_Step    " & vbLf
        SQL &= " Left Join  MS_ST_Default_Inspection_Status  On MS_ST_Default_Inspection_Status.STATUS_ID = RPT_ST_TA_Detail_Step.INSP_STATUS_ID  " & vbLf
        SQL &= " where RPT_Year = " & RPT_Year & " And rpt_No = " & rpt_No & " " & vbLf

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem(" ", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("STATUS_Name"), DT.Rows(i).Item("INSP_STATUS_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_RPT_ST_TA_Class(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)

        Dim SQL As String = "SELECT * FROM ISPT_Class"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Calss...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("ICLS_Description"), DT.Rows(i).Item("ICLS_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Function GetInspectionNotRequirePicture() As Integer()
        Dim Result() As Integer = {}
        Dim DA As New SqlDataAdapter("SELECT INSP_ID FROM VW_Not_Require_Inspection_Picture", ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Array.Resize(Result, Result.Length + 1)
            Result(Result.Length - 1) = DT.Rows(i).Item("INSP_ID")
        Next
        Return Result
    End Function

    Public Function IsInspectionRequirePicture(ByVal INSP_ID As Integer) As Boolean
        Dim INSP As Integer() = GetInspectionNotRequirePicture()
        Return Array.IndexOf(INSP, INSP_ID) = -1
    End Function

    Public Sub BindDDLLawOrganize(ddl As DropDownList, SelectedValue As Integer)
        Dim sql As String = "select law_organize_id, org_name ,active_status "
        sql += " from LAW_Organize "
        sql += " order by org_name "

        Dim dt As DataTable = Execute_DataTable(sql)
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Organize...", 0))

        'For i As Integer = 0 To dt.Rows.Count - 1
        '    Dim Item As New ListItem(dt.Rows(i).Item("org_name"), dt.Rows(i).Item("law_organize_id"))
        '    ddl.Items.Add(Item)
        'Next

        For i As Integer = 0 To dt.Rows.Count - 1
            '----------- เพิ่มการเชคค่า Disable แต่ Selected -----------
            If dt.Rows(i).Item("law_organize_id") = SelectedValue Or dt.Rows(i).Item("active_status") Then
                Dim Item As New ListItem(dt.Rows(i).Item("org_name"), dt.Rows(i).Item("law_organize_id"))
                ddl.Items.Add(Item)
            End If
            '------------- เพิ่มการเชคค่า Disable แต่ Selected ----------
        Next


        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub
#End Region

#Region "Data Privider"

    Public Function Calculate_Lube_Oil_Level(ByVal AntiOxidation As Double, ByVal TAN As Double) As Integer
        Dim SQL As String = "Select dbo.UDF_Calculate_Lube_Oil_Level(" & TAN & "," & AntiOxidation & ") LEVEL"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Return 0
        If IsDBNull(DT.Rows(0)(0)) Then Return -1
        Return DT.Rows(0)(0)
    End Function

    Public Function Get_Plant_List() As DataTable
        Dim SQL As String = "Select * FROM MS_PLANT WHERE active_status=1 ORDER BY PLANT_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Plant_Name(ByVal PLANT_ID As Integer) As String
        Dim SQL As String = "Select PLANT_NAME FROM MS_PLANT WHERE PLANT_ID=" & PLANT_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("PLANT_NAME")) Then
            Return DT.Rows(0).Item("PLANT_NAME")
        Else
            Return ""
        End If
    End Function

    Public Function Get_Tag_Code_Stationary(ByVal TAG_ID As Integer) As String
        Dim SQL As String = "Select TOP 1 TAG_Code FROM VW_ALL_ACTIVE_ST_TAG WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("TAG_Code")) Then
            Return DT.Rows(0).Item(0)
        Else
            Return ""
        End If
    End Function

    Public Function Get_Tag_Code_Rotating(ByVal TAG_ID As Integer) As String
        Dim SQL As String = "Select TOP 1 TAG_Code FROM VW_ALL_ACTIVE_RO_TAG WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("TAG_Code")) Then
            Return DT.Rows(0).Item(0)
        Else
            Return ""
        End If
    End Function

    Public Function Get_Active_Stationary_Tag(Optional ByVal PLANT_ID As Integer = 0, Optional ByVal ROUTE_ID As Integer = 0, Optional ByVal AREA_ID As Integer = 0, Optional ByVal PROC_ID As Integer = 0, Optional ByVal TAG_TYPE_ID As Integer = 0) As DataTable
        Dim SQL As String = "Select * FROM VW_ALL_ACTIVE_ST_TAG "
        Dim WHERE As String = ""
        If PLANT_ID <> 0 Then
            WHERE &= " PLANT_ID=" & PLANT_ID & " And "
        End If
        If ROUTE_ID <> 0 Then
            WHERE &= " ROUTE_ID=" & ROUTE_ID & " And "
        End If
        If AREA_ID <> 0 Then
            WHERE &= " AREA_ID=" & AREA_ID & " And "
        End If
        If PROC_ID <> 0 Then
            WHERE &= " PROC_ID=" & PROC_ID & " And "
        End If
        If TAG_TYPE_ID <> 0 Then
            WHERE &= " TAG_TYPE_ID=" & TAG_TYPE_ID & " And "
        End If

        If WHERE <> "" Then SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4)
        SQL &= "  ORDER BY TAG_CODE "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Active_Rotating_Tag(Optional ByVal PLANT_ID As Integer = 0, Optional ByVal ROUTE_ID As Integer = 0, Optional ByVal AREA_ID As Integer = 0, Optional ByVal PROC_ID As Integer = 0, Optional ByVal TAG_TYPE_ID As Integer = 0) As DataTable
        Dim SQL As String = "Select * FROM VW_ALL_ACTIVE_RO_TAG "
        Dim WHERE As String = ""
        If PLANT_ID <> 0 Then
            WHERE &= " PLANT_ID=" & PLANT_ID & " And "
        End If
        If ROUTE_ID <> 0 Then
            WHERE &= " ROUTE_ID=" & ROUTE_ID & " And "
        End If
        If AREA_ID <> 0 Then
            WHERE &= " AREA_ID=" & AREA_ID & " And "
        End If
        If PROC_ID <> 0 Then
            WHERE &= " PROC_ID=" & PROC_ID & " And "
        End If
        If TAG_TYPE_ID <> 0 Then
            WHERE &= " TAG_TYPE_ID=" & TAG_TYPE_ID & " And "
        End If

        If WHERE <> "" Then SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4)
        SQL &= "  ORDER BY TAG_CODE "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    Public Function Get_All_ST_Tag_By_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = "Select DISTINCT TAG_ID,TAG_CODE,TAG_Name FROM VW_REPORT_ST_DETAIL WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " ORDER BY TAG_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_All_RO_Tag_By_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = "Select DISTINCT TAG_ID,TAG_CODE FROM VW_REPORT_RO_DETAIL WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " ORDER BY TAG_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function GetNew_ST_DocumentID() As Integer
        Dim SQL As String = "Select IsNull(MAX(DOC_ID),0)+1 FROM SO_Doc "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Public Function GetNew_RO_DocumentID() As Integer
        Dim SQL As String = "Select IsNull(MAX(DOC_ID),0)+1 FROM RO_Doc "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

#End Region

#Region "Spring Hanger"

    Public Function GetSpringReportID(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As Integer
        Dim SQL As String = "Select RPT_ID FROM SPH_RPT_Header WHERE RPT_Year=" & RPT_Year & " And RPT_No= " & RPT_No & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return DT.Rows(0)(0)
        Else
            Return 0
        End If
    End Function

    Public Sub BindDDlSpringRoute(ByRef ddl As DropDownList, Optional ByVal PLANT_ID As Integer = 0, Optional ByVal ROUTE_ID As Integer = 0)
        Dim SQL As String = "Select ROUTE_ID,ROUTE_Code " & vbLf
        SQL &= " FROM SPH_MS_ROUTE" & vbLf
        Dim Filter As String = ""
        If PLANT_ID <> 0 Then
            Filter &= " PLANT_ID=" & PLANT_ID & " And "
        End If
        If Filter <> "" Then SQL &= " WHERE " & Filter.Substring(0, Filter.Length - 4) & vbLf
        SQL &= " ORDER BY ROUTE_Code"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Route...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("ROUTE_Code"), DT.Rows(i).Item("ROUTE_ID"))
            ddl.Items.Add(Item)
        Next
        If ROUTE_ID <> 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = ROUTE_ID.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlSpring(ByRef ddl As DropDownList, Optional ByVal PLANT_ID As Integer = 0, Optional ByVal ROUTE_ID As Integer = 0, Optional ByVal SPH_ID As Integer = 0)
        Dim SQL As String = "Select SPH_ID,SPH_No " & vbLf
        SQL &= " FROM SPH_MS_Spring " & vbLf
        SQL &= " INNER JOIN SPH_MS_Route On SPH_MS_Spring.ROUTE_ID=SPH_MS_Route.ROUTE_ID" & vbLf

        Dim Filter As String = ""
        If PLANT_ID <> 0 Then
            Filter &= " SPH_MS_Route.PLANT_ID=" & PLANT_ID & " And "
        End If
        If ROUTE_ID <> 0 Then
            Filter &= " SPH_MS_Spring.ROUTE_ID =" & ROUTE_ID & " And "
        End If
        If Filter <> "" Then SQL &= " WHERE " & Filter.Substring(0, Filter.Length - 4) & vbLf
        SQL &= " ORDER BY SPH_No"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Spring...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("SPH_ID"), DT.Rows(i).Item("SPH_No"))
            ddl.Items.Add(Item)
        Next
        If SPH_ID <> 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SPH_ID.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

#End Region

#Region "Create/Edit Report"

    Public Function Get_Report_Step_Color(ByVal InputStep As Report_Step) As Color
        Select Case InputStep
            Case Report_Step.New_Step
                Return Color.Red
            Case Report_Step.Collecting_Step
                Return Color.Orange
            Case Report_Step.Inspecting_Step
                Return Color.SteelBlue
            Case Report_Step.Approving_Step
                Return Color.Violet
            Case Report_Step.Approved_Step
                Return Color.Green
            Case Else
                Return Color.Black
        End Select
    End Function

    Public Function GetReportCode(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As String
        Dim SQL As String = "Select dbo.UDF_RPT_Code(" & RPT_Year & "," & RPT_No & ") As RPT_CODE" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Public Function GetNewReportNumber(ByVal RPT_Year As Integer) As Integer
        Dim SQL As String = "Select dbo.UDF_Get_New_Report_Number(" & RPT_Year & ")" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Public Function GetNew_SPH_ReportID() As Integer
        Dim SQL As String = "Select IsNull(MAX(RPT_ID),0)+1 FROM SPH_RPT_Header "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

    Public Sub Construct_Report_SPH(ByVal By As Integer, ByVal RPT_ID As Integer)

        Dim SQL As String = "Select DISTINCT SPH_ID" & vbLf
        SQL &= " FROM SPH_RPT_Header" & vbLf
        SQL &= " INNER JOIN SPH_MS_Route On SPH_RPT_Header.ROUTE_ID=SPH_MS_Route.ROUTE_ID And SPH_MS_Route.Active_Status=1" & vbLf
        SQL &= " INNER JOIN SPH_MS_Spring On SPH_MS_Route.ROUTE_ID=SPH_MS_Spring.ROUTE_ID And SPH_MS_Spring.Active_Status=1 " & vbLf
        SQL &= " WHERE RPT_ID =" & RPT_ID & vbLf
        Dim MA As New SqlDataAdapter(SQL, ConnStr)
        Dim MT As New DataTable
        MA.Fill(MT) ' Must Created

        SQL = "Select * FROM SPH_RPT_Detail WHERE RPT_ID=" & RPT_ID
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT) ' Current In Database

        '--------------- Compare --------------
        Dim ColSpring() As String = {"SPH_ID"}
        '---------- Find Point To Be Remove ------------------
        DT.DefaultView.RowFilter = ""
        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            MT.DefaultView.RowFilter = "SPH_ID=" & DT.Rows(i).Item("SPH_ID")
            If MT.DefaultView.Count = 0 Then '----------- Must Remove From DT --------------
                DT.DefaultView(0).Row.Delete()
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT)
                DT.AcceptChanges()
            End If
        Next

        '----------- Find Spring To Be Added-------------------
        For i As Integer = 0 To MT.Rows.Count - 1
            DT.DefaultView.RowFilter = "SPH_ID=" & MT.Rows(i).Item("SPH_ID")
            If DT.DefaultView.Count = 0 Then
                Dim DR As DataRow = DT.NewRow
                DR("DETAIL_ID") = Get_New_SPH_DetailID()
                DR("RPT_ID") = RPT_ID
                DR("SPH_ID") = MT.Rows(i).Item("SPH_ID")
                DR("INSP_Level") = ""
                DR("Corrosion") = ""
                DR("PossibleCause") = ""
                DR("Comment") = ""
                DR("PIC_Detail1") = DBNull.Value
                DR("PIC_Detail2") = DBNull.Value
                DR("Update_Time") = Now
                DR("Update_By") = By
                DT.Rows.Add(DR)
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT)
            End If
        Next


    End Sub

    Public Sub Construct_LO_Report_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal By As Integer)

        '-------------------- Get Report Type ----------------
        Dim SQL As String = "Select RPT_Period_Type FROM RPT_LO_Header WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        Dim Period_Type As EIR_BL.LO_Period_Type = DT.Rows(0).Item("RPT_Period_Type")

        '-------------------- Update Plant AND Lube Type----------------------
        SQL = "Select MS_LO_TAG.*,Oil_TYPE_Name,Oil_Cat FROM MS_LO_TAG " & vbLf
        SQL &= "LEFT JOIN MS_LO_Oil_Type OIL On MS_LO_TAG.Oil_TYPE_ID=OIL.Oil_TYPE_ID" & vbLf
        SQL &= "WHERE MS_LO_TAG.Active_Status=1 " & vbLf
        Select Case Period_Type
            Case 1 '-----------------Monthly
                SQL &= " And LO_TAG_TYPE=2" & vbLf
            Case 2 '-----------------Quaterly
                SQL &= " And LO_TAG_TYPE In (1,2)" & vbLf
        End Select
        SQL &= " ORDER BY PLANT_ID,LO_TAG_NO" & vbLf
        Dim Tag As New DataTable
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Tag)

        '-------------------- Remove Unused Tag --------------------
        DT = New DataTable
        DA = New SqlDataAdapter("Select * FROM RPT_LO_Detail WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No, ConnStr)
        DA.Fill(DT)
        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            Tag.DefaultView.RowFilter = "LO_TAG_ID=" & DT.Rows(i).Item("LO_TAG_ID")
            If Tag.DefaultView.Count = 0 Then
                DT.Rows(i).Delete()
            End If
        Next
        Try
            Dim CMD As New SqlCommandBuilder(DA)
            DA.Update(DT) : DT.AcceptChanges()
        Catch : End Try

        '--------------------- Add New Tag ----------------------
        For i As Integer = 0 To Tag.Rows.Count - 1
            DT.DefaultView.RowFilter = "LO_TAG_ID=" & Tag.Rows(i).Item("LO_TAG_ID")
            If DT.DefaultView.Count = 0 Then
                '------------------- Add Information ------------------
                Dim DR As DataRow = DT.NewRow
                DR("DETAIL_ID") = GetNew_LO_DetailID()
                DR("RPT_Year") = RPT_Year
                DR("RPT_No") = RPT_No
                DR("LO_TAG_ID") = Tag.Rows(i).Item("LO_TAG_ID")
                DR("PLANT_ID") = Tag.Rows(i).Item("PLANT_ID")
                DR("LO_TAG_NO") = Tag.Rows(i).Item("LO_TAG_NO")
                DR("LO_TAG_Name") = Tag.Rows(i).Item("LO_TAG_Name")
                DR("LO_TAG_TYPE") = Tag.Rows(i).Item("LO_TAG_TYPE")
                DR("OIL_TYPE_ID") = Tag.Rows(i).Item("OIL_TYPE_ID")
                DR("OIL_TYPE_NAME") = Tag.Rows(i).Item("OIL_TYPE_NAME").ToString
                DR("OIL_CAT") = Tag.Rows(i).Item("OIL_CAT")
                DR("Recomment") = ""
                DR("Update_Time") = Now
                DT.Rows.Add(DR)
            Else
                '--------------- Update Information -------------------
                Dim DR As DataRow = DT.DefaultView(0).Row
                DR("PLANT_ID") = Tag.Rows(i).Item("PLANT_ID")
                DR("LO_TAG_NO") = Tag.Rows(i).Item("LO_TAG_NO")
                DR("LO_TAG_Name") = Tag.Rows(i).Item("LO_TAG_Name")
                DR("LO_TAG_TYPE") = Tag.Rows(i).Item("LO_TAG_TYPE")
                DR("OIL_TYPE_ID") = Tag.Rows(i).Item("OIL_TYPE_ID")
                DR("OIL_TYPE_NAME") = Tag.Rows(i).Item("OIL_TYPE_NAME").ToString
                DR("OIL_CAT") = Tag.Rows(i).Item("OIL_CAT")
                '--------------- Check Condition Parameter -------------
                If Tag.Rows(i).Item("LO_TAG_TYPE") = LO_Tag_Type.Balance_Of_Plant Then
                    DR("TAN_Value") = DBNull.Value
                    DR("TAN_Date") = DBNull.Value

                    DR("OX_Value") = DBNull.Value
                    DR("OX_Date") = DBNull.Value

                    DR("WATER_Value") = DBNull.Value
                    DR("WATER_Date") = DBNull.Value
                End If

                If Period_Type = LO_Period_Type.Monthy Then
                    DR("VANISH_Value") = DBNull.Value
                    DR("VANISH_Date") = DBNull.Value

                    If Tag.Rows(i).Item("LO_TAG_TYPE") = LO_Tag_Type.Balance_Of_Plant Then
                        DR("PART_COUNT_Value") = DBNull.Value
                        DR("PART_COUNT_Date") = DBNull.Value
                    End If
                End If
                DR("Update_Time") = Now
            End If
            Try
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT) : DT.AcceptChanges()
            Catch : End Try
        Next
    End Sub

    Public Function GetNew_LO_DetailID() As Integer
        Dim DA As New SqlDataAdapter("Select ISNULL(MAX(DETAIL_ID),0)+1 FROM RPT_LO_Detail", ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function


    Public Function SPH_CalculateTravelStatus(ByVal Length_Cold As Object, ByVal Length_Hot As Object, ByVal INSP_Level As String) As String
        If INSP_Level = "เขียว" Or INSP_Level = "สีเขียว" Or INSP_Level.ToLower = "In-range" Then
            Return "Accepted"
        End If

        If INSP_Level = "สีฟ้า" Or INSP_Level = "ฟ้า" Or INSP_Level = "สีส้ม" Or INSP_Level = "ส้ม" Or INSP_Level.ToLower = "out-range" Then
            Return "Adjusted"
        End If

        If IsDBNull(Length_Cold) Or IsDBNull(Length_Hot) Or Not IsNumeric(INSP_Level) Or Not IsNumeric(Length_Cold) Or Not IsNumeric(Length_Hot) Then
            Return "Unknow"
        End If
        Dim INSP As Double = CDbl(INSP_Level)
        Dim UBound As Double
        Dim LBound As Double
        If Length_Cold > Length_Hot Then
            UBound = Length_Cold
            LBound = Length_Hot
        Else
            UBound = Length_Hot
            LBound = Length_Cold
        End If

        If INSP > UBound Or INSP < LBound Then
            Return "Adjusted"
        Else
            Return "Accepted"
        End If
    End Function

    Public Function Get_New_SPH_DetailID() As Integer
        Dim SQL As String = "Select IsNull(MAX(Detail_ID),0)+1 FROM SPH_RPT_Detail "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Public Function Get_SPH_Image(ByVal DETAIL_ID As Integer, ByVal IMG_POS As Integer) As Byte()
        Dim SQL As String = "Select PIC_Detail" & IMG_POS & " FROM SPH_RPT_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) Then
            Return DT.Rows(0).Item(0)
        Else
            Return Nothing
        End If
    End Function

    Public Sub Construct_ST_Report_Detail(ByVal By As Integer, ByVal RPT_Year As Integer, ByVal RPT_No As Integer, Optional ByVal TAG_ID As Integer = 0)
        Dim SQL As String = "EXEC dbo.SP_New_ST_Report_Structure " & RPT_Year & "," & RPT_No & ","
        If TAG_ID = 0 Then
            SQL &= "NULL"
        Else
            SQL &= TAG_ID
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.SelectCommand.CommandTimeout = 240
        Dim LastData As New DataTable ' ----------- New Data -----------
        DA.Fill(LastData)

        SQL = "Select * FROM RPT_ST_Detail WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Dim DT As New DataTable ' ----------- Current Data -----------
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)

        '--------------- Compare --------------
        Dim ColInsp() As String = {"TAG_ID", "INSP_ID"}

        '---------- Find Point To Be Remove ------------------
        DT.DefaultView.RowFilter = ""
        Dim OldKey As DataTable = DT.DefaultView.ToTable(True, ColInsp).Copy
        For i As Integer = 0 To OldKey.Rows.Count - 1
            LastData.DefaultView.RowFilter = "TAG_ID=" & OldKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & OldKey.Rows(i).Item("INSP_ID")
            If LastData.DefaultView.Count = 0 Then '----------- Must Remove From DT --------------
                DT.DefaultView.RowFilter = "TAG_ID=" & OldKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & OldKey.Rows(i).Item("INSP_ID")
                While DT.DefaultView.Count > 0
                    DT.DefaultView(0).Row.Delete()
                    Dim cmd As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                    DT.AcceptChanges()
                End While
            End If
        Next

        '---------- Find New Tag And Inspection Point --------
        LastData.DefaultView.RowFilter = ""
        Dim NewKey As DataTable = LastData.DefaultView.ToTable(True, ColInsp).Copy
        For i As Integer = 0 To NewKey.Rows.Count - 1
            DT.DefaultView.RowFilter = "TAG_ID=" & NewKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & NewKey.Rows(i).Item("INSP_ID")
            If DT.DefaultView.Count = 0 Then '----------- Add New Key From DT --------------
                LastData.DefaultView.RowFilter = "TAG_ID=" & NewKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & NewKey.Rows(i).Item("INSP_ID")
                Dim StrartDetailID As Integer = Get_New_ST_DetailID()
                For r As Integer = 0 To LastData.DefaultView.Count - 1
                    Dim DR As DataRow = DT.NewRow
                    DR("DETAIL_ID") = StrartDetailID + r
                    DR("RPT_Year") = RPT_Year
                    DR("RPT_No") = RPT_No
                    DR("TAG_ID") = LastData.DefaultView(r).Item("TAG_ID")
                    DR("TAG_TYPE_ID") = LastData.DefaultView(r).Item("TAG_TYPE_ID")
                    DR("INSP_ID") = LastData.DefaultView(r).Item("INSP_ID")
                    If Not IsDBNull(LastData.DefaultView(r).Item("STATUS_ID")) Then
                        DR("STATUS_ID") = LastData.DefaultView(r).Item("STATUS_ID")
                    End If
                    If Not IsDBNull(LastData.DefaultView(r).Item("COMP_NO")) Then
                        DR("COMP_NO") = LastData.DefaultView(r).Item("COMP_NO")
                    End If
                    DR("PROB_Detail") = ""
                    DR("PROB_Recomment") = ""
                    If Not IsDBNull(LastData.DefaultView(r).Item("LAST_DETAIL_ID")) Then
                        DR("LAST_DETAIL_ID") = LastData.DefaultView(r).Item("LAST_DETAIL_ID")
                    End If
                    If Not IsDBNull(LastData.DefaultView(r).Item("Responsible")) Then
                        DR("Responsible") = LastData.DefaultView(r).Item("Responsible")
                    End If
                    DR("ICLS_ID") = DBNull.Value
                    DR("Create_Flag") = "Main"
                    DR("Responsible") = ""
                    DR("PIC_Detail1") = False
                    DR("PIC_Detail2") = False
                    DR("Detail1") = ""
                    DR("Detail2") = ""
                    '----------------------------------------------                   
                    DT.Rows.Add(DR)
                Next
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT)
            End If
        Next

    End Sub

    Public Sub Construct_RO_Report_Detail(ByVal By As Integer, ByVal RPT_Year As Integer, ByVal RPT_No As Integer, Optional ByVal TAG_ID As Integer = 0)

        Dim SQL As String = "EXEC dbo.SP_New_RO_Report_Structure " & RPT_Year & "," & RPT_No & ","
        If TAG_ID = 0 Then
            SQL &= "NULL"
        Else
            SQL &= TAG_ID
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.SelectCommand.CommandTimeout = 240
        Dim LastData As New DataTable ' ----------- New Data -----------
        DA.Fill(LastData)

        SQL = "Select * FROM RPT_RO_Detail WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Dim DT As New DataTable ' ----------- Current Data -----------
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)

        '--------------- Compare --------------
        Dim ColInsp() As String = {"TAG_ID", "INSP_ID"}

        '---------- Find Point To Be Remove ------------------
        DT.DefaultView.RowFilter = ""
        Dim OldKey As DataTable = DT.DefaultView.ToTable(True, ColInsp).Copy
        For i As Integer = 0 To OldKey.Rows.Count - 1
            LastData.DefaultView.RowFilter = "TAG_ID=" & OldKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & OldKey.Rows(i).Item("INSP_ID")
            If LastData.DefaultView.Count = 0 Then '----------- Must Remove From DT --------------
                DT.DefaultView.RowFilter = "TAG_ID=" & OldKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & OldKey.Rows(i).Item("INSP_ID")
                While DT.DefaultView.Count > 0
                    DT.DefaultView(0).Row.Delete()
                    Dim cmd As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                    DT.AcceptChanges()
                End While
            End If
        Next

        '---------- Find New Tag And Inspection Point --------
        LastData.DefaultView.RowFilter = ""
        Dim NewKey As DataTable = LastData.DefaultView.ToTable(True, ColInsp).Copy
        For i As Integer = 0 To NewKey.Rows.Count - 1
            DT.DefaultView.RowFilter = "TAG_ID=" & NewKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & NewKey.Rows(i).Item("INSP_ID")
            If DT.DefaultView.Count = 0 Then '----------- Add New Key From DT --------------
                LastData.DefaultView.RowFilter = "TAG_ID=" & NewKey.Rows(i).Item("TAG_ID") & " And INSP_ID=" & NewKey.Rows(i).Item("INSP_ID")
                Dim StrartDetailID As Integer = Get_New_RO_DetailID()
                For r As Integer = 0 To LastData.DefaultView.Count - 1
                    Dim DR As DataRow = DT.NewRow
                    DR("DETAIL_ID") = StrartDetailID + r
                    DR("RPT_Year") = RPT_Year
                    DR("RPT_No") = RPT_No
                    DR("TAG_ID") = LastData.DefaultView(r).Item("TAG_ID")
                    DR("TAG_TYPE_ID") = LastData.DefaultView(r).Item("TAG_TYPE_ID")
                    DR("INSP_ID") = LastData.DefaultView(r).Item("INSP_ID")
                    If Not IsDBNull(LastData.DefaultView(r).Item("STATUS_ID")) Then
                        DR("STATUS_ID") = LastData.DefaultView(r).Item("STATUS_ID")
                    End If
                    If Not IsDBNull(LastData.DefaultView(r).Item("COMP_NO")) Then
                        DR("COMP_NO") = LastData.DefaultView(r).Item("COMP_NO")
                    End If
                    DR("PROB_Detail") = ""
                    DR("PROB_Recomment") = ""
                    If Not IsDBNull(LastData.DefaultView(r).Item("LAST_DETAIL_ID")) Then
                        DR("LAST_DETAIL_ID") = LastData.DefaultView(r).Item("LAST_DETAIL_ID")
                    End If
                    If Not IsDBNull(LastData.DefaultView(r).Item("Responsible")) Then
                        DR("Responsible") = LastData.DefaultView(r).Item("Responsible")
                    End If
                    DR("ICLS_ID") = DBNull.Value
                    DR("Create_Flag") = "Main"
                    DR("Responsible") = ""
                    DR("PIC_Detail1") = False
                    DR("PIC_Detail2") = False
                    DR("Detail1") = ""
                    DR("Detail2") = ""
                    '----------------------------------------------                   
                    DT.Rows.Add(DR)
                Next
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT)
            End If
        Next

    End Sub

    Public Function Get_New_ST_DetailID() As Integer
        Dim SQL As String = "Select IsNull(MAX(Detail_ID),0)+1 FROM RPT_ST_Detail "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Public Function Get_New_RO_DetailID() As Integer
        Dim SQL As String = "Select IsNull(MAX(Detail_ID),0)+1 FROM RPT_RO_Detail "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Public Function Get_ST_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DETAIL_ID As Integer, ByVal IMG_POS As Integer) As Byte()
        Dim SQL As String = "Select PIC_Detail" & IMG_POS & " FROM RPT_ST_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) AndAlso DT.Rows(0).Item(0) Then
            Dim ImgPath As String = Picture_Path & "\" & RPT_Year & "\" & RPT_No & "\" & DETAIL_ID & "_" & IMG_POS
            If File.Exists(ImgPath) Then
                Dim C As New Converter
                Dim F As FileStream = File.Open(ImgPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim S As Byte() = C.StreamToByte(F)
                F.Close()
                F.Dispose()
                Return S
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public Function Get_ST_Image(ByVal DETAIL_ID As Integer, ByVal IMG_POS As Integer) As Byte()
        Dim SQL As String = "Select PIC_Detail" & IMG_POS & ",RPT_Year,RPT_No FROM RPT_ST_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) AndAlso DT.Rows(0).Item(0) Then
            Dim ImgPath As String = Picture_Path & "\" & DT.Rows(0).Item("RPT_Year") & "\" & DT.Rows(0).Item("RPT_No") & "\" & DETAIL_ID & "_" & IMG_POS
            If File.Exists(ImgPath) Then
                Dim C As New Converter
                Dim F As FileStream = File.Open(ImgPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim S As Byte() = C.StreamToByte(F)
                F.Close()
                F.Dispose()
                Return S
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public Function Get_RO_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DETAIL_ID As Integer, ByVal IMG_POS As Integer) As Byte()
        Dim SQL As String = "Select PIC_Detail" & IMG_POS & ",RPT_Year,RPT_No FROM RPT_RO_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) AndAlso DT.Rows(0).Item(0) Then
            Dim ImgPath As String = Picture_Path & "\" & DT.Rows(0).Item("RPT_Year") & "\" & DT.Rows(0).Item("RPT_No") & "\" & DETAIL_ID & "_" & IMG_POS
            If File.Exists(ImgPath) Then
                Dim C As New Converter
                Dim F As FileStream = File.Open(ImgPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim S As Byte() = C.StreamToByte(F)
                F.Close()
                F.Dispose()
                Return S
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public Function Get_RO_Image(ByVal DETAIL_ID As Integer, ByVal IMG_POS As Integer) As Byte()
        Dim SQL As String = "Select PIC_Detail" & IMG_POS & ",RPT_Year,RPT_No FROM RPT_RO_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) AndAlso DT.Rows(0).Item(0) Then
            Dim ImgPath As String = Picture_Path & "\" & DT.Rows(0).Item("RPT_Year") & "\" & DT.Rows(0).Item("RPT_No") & "\" & DETAIL_ID & "_" & IMG_POS
            If File.Exists(ImgPath) Then
                Dim C As New Converter
                Dim F As FileStream = File.Open(ImgPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim S As Byte() = C.StreamToByte(F)
                F.Close()
                F.Dispose()
                Return S
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public Function Save_Picture_File(ByVal Pic_Detail As Byte(), ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DETAIL_ID As Integer, ByVal IMG_POS As Integer) As Boolean
        Dim Path As String = Picture_Path & "\" & RPT_Year
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If
        Path &= "\" & RPT_No
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If
        Path &= "\" & DETAIL_ID & "_" & IMG_POS
        Dim CountProcess As Integer = 0
        If File.Exists(Path) Then
            '-------------- Changed------------
            While File.Exists(Path)
                CountProcess += 1
                If CountProcess > 20 Then Return False
                Try
                    Kill(Path)
                Catch ex As Exception
                    Threading.Thread.Sleep(500)
                End Try
            End While
        End If
        If IsNothing(Pic_Detail) Then Return True

        CountProcess = 0
        Do While True
            Try
                CountProcess += 1
                If CountProcess > 20 Then Return False
                Dim F As FileStream = File.Open(Path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
                F.Write(Pic_Detail, 0, Pic_Detail.Length)
                F.Close()
                Return True
            Catch ex As Exception
                Threading.Thread.Sleep(500)
            End Try
        Loop
        Return False
    End Function

    Public Function Get_Rotating_OffRoutine_Vibration_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As Byte()
        Dim SQL As String = "Select PIC_Detail FROM RO_Vibration WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) Then
            Return DT.Rows(0).Item(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function Get_New_Stationary_Doc_ID() As Integer
        Dim SQL As String = "Select IsNull(MAX(DOC_ID),0)+1 FROM SO_Doc "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Public Function Get_New_Rotating_Doc_ID() As Integer
        Dim SQL As String = "Select IsNull(MAX(DOC_ID),0)+1 FROM RO_Doc "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Public Function Get_Stationary_Document_Image(ByVal DOC_ID As Integer) As Byte()
        Dim SQL As String = "Select DOC_Detail FROM SO_Doc WHERE DOC_ID=" & DOC_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) Then
            Return DT.Rows(0).Item(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function GetNew_RO_Vibration_ID() As Integer
        Dim SQL As String = "Select IsNull(MAX(DETAIL_ID),0)+1 FROM RO_Vibration "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Public Function Get_Rotating_Document_Image(ByVal DOC_ID As Integer) As Byte()
        Dim SQL As String = "Select DOC_Detail FROM RO_Doc WHERE DOC_ID=" & DOC_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) Then
            Return DT.Rows(0).Item(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function Get_Stationary_OffRoutine_Document(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = "Select DOC_ID,RPT_Year,RPT_No,DOC_Name,DOC_Recomment,Content_Type,DOC_Align " & vbLf
        SQL &= " FROM SO_Doc WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " ORDER BY DOC_ID" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_Rotating_OffRoutine_Document(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = "Select DOC_ID,RPT_Year,RPT_No,DOC_Detail,DOC_Name,DOC_Recomment,Content_Type,DOC_Align,ShowRecomment " & vbLf
        SQL &= " FROM RO_Doc WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " ORDER BY DOC_ID" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function




#End Region

#Region "Create Report"
    Public Structure CreateReportResult
        Public Success As Boolean
        Public Message As String
    End Structure

    Public Function GeneratePostedReport(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DestinationPath As String) As CreateReportResult
        Dim Result As New CreateReportResult
        Dim RC As New ReportClass

        '---------------กรณีเป็น Report ที่ Posted มาแล้ว ลบ File เดิม------------------
        Dim CountProcess As Integer = 0
        While File.Exists(DestinationPath) And CountProcess <= 20
            CountProcess += 1
            Try
                Kill(DestinationPath)
            Catch ex As Exception
                Threading.Thread.Sleep(500)
            End Try
        End While

        Dim TempFile As String = RC.GetReport(RPT_Year, RPT_No)
        If TempFile = "" Then
            Result.Success = False
            Return Result
        End If

        CountProcess = 0
        Dim _err As Boolean = True
        Dim _msg As String = ""

        While _err And CountProcess <= 20
            CountProcess += 1
            Try
                File.Copy(TempFile, DestinationPath, True)
                _err = False
            Catch ex As Exception
                _msg = ex.Message
                Threading.Thread.Sleep(500)
                _err = True
            End Try
        End While
        If _err And CountProcess > 20 Then
            Result.Success = False
            Result.Message = _msg.Replace("\", "\\")
            Return Result
        End If

        Try
            Kill(TempFile)
        Catch : End Try
        Result.Success = True
        Return Result
    End Function
#End Region

#Region "eMonitor"

    Public Function Get_Vibration_Tag_Stationary_By_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = "Select DISTINCT RPT_CODE,RPT_Year,RPT_No,TAG.TAG_ID,TAG.TAG_Code,TAG.TAG_Name,TAG.TAG_TYPE_ID,TAG.TAG_TYPE_Name," & vbNewLine
        SQL &= " AREA_Code,PROC_Code" & vbNewLine
        SQL &= " FROM VW_REPORT_ST_DETAIL VW " & vbNewLine
        SQL &= " INNER JOIN VW_ALL_ACTIVE_ST_TAG TAG On VW.TAG_ID=TAG.TAG_ID" & vbNewLine
        SQL &= " WHERE VW.RPT_Year=" & RPT_Year & " And VW.RPT_No=" & RPT_No & vbNewLine
        SQL &= " And INSP_ID=12 And CURRENT_LEVEL>1" & vbNewLine ' Problem
        SQL &= " ORDER BY TAG_Code"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT

    End Function

    Public Function Get_Vibration_Tag_Rotating_By_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = "Select DISTINCT RPT_CODE,RPT_Year,RPT_No,TAG.TAG_ID,TAG.TAG_Code,TAG.TAG_Name,TAG.TAG_TYPE_ID,TAG.TAG_TYPE_Name," & vbNewLine
        SQL &= " AREA_Code,PROC_Code" & vbNewLine
        SQL &= " FROM VW_REPORT_RO_DETAIL VW " & vbNewLine
        SQL &= " INNER JOIN VW_ALL_ACTIVE_RO_TAG TAG On VW.TAG_ID=TAG.TAG_ID" & vbNewLine
        SQL &= " WHERE VW.RPT_Year=" & RPT_Year & " And VW.RPT_No=" & RPT_No & vbNewLine
        SQL &= " And INSP_ID=12 And CURRENT_LEVEL>1" & vbNewLine ' Problem
        SQL &= " ORDER BY TAG_Code"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT

    End Function

    Public Function Get_eMonitor_Plant(ByVal PLANT_ID As Integer) As String
        Select Case PLANT_ID
            Case 1
                Return "GSP1"
            Case 2
                Return "GSP2"
            Case 3
                Return "GSP3"
            Case 4
                Return "GSP5"
            Case 5
                Return "GSP6"
            Case 8
                Return "RPLF"
            Case 9
                Return "ESP"
            Case Else
                Return "OTHERS"
        End Select
    End Function



#End Region

#Region "SPH"
    Public Sub Drop_SPH_RPT_Header(ByVal RPT_ID As Integer)
        Drop_SPH_RPT_Detail(RPT_ID)

        Dim SQL As String = "DELETE FROM SPH_RPT_Header WHERE RPT_ID=" & RPT_ID & vbNewLine
        Dim Conn As New SqlConnection(ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = SQL
            Try
                .ExecuteNonQuery()
            Catch ex As Exception
                Conn.Close()
                Err.Raise(0, , ex.Message)
                Exit Sub
            End Try
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()

    End Sub

    Public Sub Drop_SPH_RPT_Detail(ByVal RPT_ID As Integer)
        Dim SQL As String = "DELETE FROM SPH_RPT_Detail WHERE RPT_ID=" & RPT_ID & vbNewLine
        Dim Conn As New SqlConnection(ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = SQL
            Try
                .ExecuteNonQuery()
            Catch ex As Exception
                Conn.Close()
                Err.Raise(0, , ex.Message)
                Exit Sub
            End Try
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
#End Region

#Region "LO"
    Public Enum LO_Period_Type
        Monthy = 1
        Quaterly = 2
    End Enum

    Public Enum LO_Tag_Type
        Balance_Of_Plant = 1
        Critical_Machine = 2
    End Enum

    Public Function Get_LubeOil_HIST_OX_ID(ByVal DETAIL_ID As Integer) As Integer
        Dim SQL As String = "Select dbo.UDF_Get_LubeOil_HIST_OX_ID(" & DETAIL_ID & ")" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If Not IsDBNull(DT.Rows(0)(0)) Then
            Return DT.Rows(0)(0)
        Else
            Return 0
        End If
    End Function

    Public Function Get_LubeOil_HIST_TAN_ID(ByVal DETAIL_ID As Integer) As Integer
        Dim SQL As String = "Select dbo.UDF_Get_LubeOil_HIST_TAN_ID(" & DETAIL_ID & ")" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If Not IsDBNull(DT.Rows(0)(0)) Then
            Return DT.Rows(0)(0)
        Else
            Return 0
        End If
    End Function

    Public Function Get_LubeOil_HIST_VANISH_ID(ByVal DETAIL_ID As Integer) As Integer
        Dim SQL As String = "Select dbo.UDF_Get_LubeOil_HIST_VANISH_ID(" & DETAIL_ID & ")" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If Not IsDBNull(DT.Rows(0)(0)) Then
            Return DT.Rows(0)(0)
        Else
            Return 0
        End If
    End Function

    Public Function Get_LubeOil_HIST_PART_ID(ByVal DETAIL_ID As Integer) As Integer
        Dim SQL As String = "Select dbo.UDF_Get_LubeOil_HIST_PART_ID(" & DETAIL_ID & ")" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If Not IsDBNull(DT.Rows(0)(0)) Then
            Return DT.Rows(0)(0)
        Else
            Return 0
        End If
    End Function

    Public Function Get_LubeOil_HIST_WATER_ID(ByVal DETAIL_ID As Integer) As Integer
        Dim SQL As String = "Select dbo.UDF_Get_LubeOil_HIST_WATER_ID(" & DETAIL_ID & ")" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If Not IsDBNull(DT.Rows(0)(0)) Then
            Return DT.Rows(0)(0)
        Else
            Return 0
        End If
    End Function

    Public Function Get_LubeOil_LAST_VANISH_Value(ByVal DETAIL_ID As Integer) As Object
        Dim SQL As String = "Select VANISH_Value " & vbLf
        SQL &= " FROM RPT_LO_Detail" & vbLf
        SQL &= " WHERE DETAIL_ID=dbo.UDF_Get_LubeOil_HIST_VANISH_ID(" & DETAIL_ID & ")"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 OrElse IsDBNull(DT.Rows(0)(0)) Then
            Return Nothing
        Else
            Return DT.Rows(0)(0)
        End If
    End Function

    Public Function Get_LubeOil_LAST_VANISH_Date(ByVal DETAIL_ID As Integer) As Object
        Dim SQL As String = "Select VANISH_Date " & vbLf
        SQL &= " FROM RPT_LO_Detail" & vbLf
        SQL &= " WHERE DETAIL_ID=dbo.UDF_Get_LubeOil_HIST_VANISH_ID(" & DETAIL_ID & ")"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 OrElse IsDBNull(DT.Rows(0)(0)) Then
            Return Nothing
        Else
            Return DT.Rows(0)(0)
        End If
    End Function

#End Region

    Public Sub Drop_Stationary_OffRoutine_Document(ByVal RPT_Year As Integer, Optional ByVal RPT_No As Integer = 0, Optional ByVal DOC_ID As Integer = 0)
        Dim SQL As String = "DELETE FROM SO_Doc" & vbNewLine
        SQL &= " WHERE DOC_ID In (Select DOC_ID FROM SO_Doc WHERE RPT_Year=" & RPT_Year & ")" & vbNewLine
        If RPT_No <> 0 Then
            SQL &= " And DOC_ID In (Select DOC_ID FROM SO_Doc WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & ")" & vbNewLine
        End If
        If DOC_ID <> 0 Then
            SQL &= " And DOC_ID =" & DOC_ID & vbNewLine
        End If

        Dim Conn As New SqlConnection(ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = SQL
            Try
                .ExecuteNonQuery()
            Catch ex As Exception
                Conn.Close()
                Err.Raise(0, , ex.Message)
                Exit Sub
            End Try
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub Drop_Rotating_OffRoutine_Document(ByVal RPT_Year As Integer, Optional ByVal RPT_No As Integer = 0, Optional ByVal DOC_ID As Integer = 0)
        Dim SQL As String = "DELETE FROM RO_Doc" & vbNewLine
        SQL &= " WHERE DOC_ID In (Select DOC_ID FROM RO_Doc WHERE RPT_Year=" & RPT_Year & ")" & vbNewLine
        If RPT_No <> 0 Then
            SQL &= " And DOC_ID In (Select DOC_ID FROM RO_Doc WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & ")" & vbNewLine
        End If
        If DOC_ID <> 0 Then
            SQL &= " And DOC_ID =" & DOC_ID & vbNewLine
        End If

        Dim Conn As New SqlConnection(ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = SQL
            Try
                .ExecuteNonQuery()
            Catch ex As Exception
                Conn.Close()
                Err.Raise(0, , ex.Message)
                Exit Sub
            End Try
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub Drop_Rotating_Routine_Import(ByVal RPT_Year As Integer, Optional ByVal RPT_No As Integer = 0, Optional ByVal TAG_ID As Integer = 0)

        Dim SQL As String = "DELETE FROM RPT_Rotating_Routine_Import" & vbNewLine
        SQL &= " WHERE RPT_Year=" & RPT_Year
        If RPT_No <> 0 Then
            SQL &= " And RPT_No=" & RPT_No
        End If
        If TAG_ID <> 0 Then
            SQL &= " And TAG_ID=" & TAG_ID
        End If

        Dim Conn As New SqlConnection(ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = SQL
            Try
                .ExecuteNonQuery()
            Catch ex As Exception
                Conn.Close()
                Err.Raise(0, , ex.Message)
                Exit Sub
            End Try
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

#Region "ST"

    Public Function SP_Get_Previous_Incompleted_ST_Off_Routine_Report(ByVal TAG_ID As Integer) As DataTable
        Dim SQL As String = "EXEC dbo.SP_Get_Previous_Incompleted_ST_Off_Routine_Report " & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Check_Previous_Incomplete_ST_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, Optional ByVal TAG_ID As Integer = 0) As DataTable
        Dim SQL As String = "EXEC dbo.SP_Get_Previous_Incompleted_ST_Report " & RPT_Year & "," & RPT_No & ","
        If TAG_ID <> 0 Then
            SQL &= TAG_ID
        Else
            SQL &= "NULL"
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Sub Drop_RPT_ST_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_ST_Detail(RPT_Year, RPT_No)
        Drop_RPT_SO_DOC(RPT_Year, RPT_No)

        Dim SQL As String = "DELETE FROM RPT_ST_Header " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Execute_Command(SQL)

    End Sub

    Public Sub Drop_RPT_ST_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal TAG_ID As Integer)
        Dim SQL As String = "Select * FROM RPT_ST_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " And TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_ST_Picture(DT.Rows(i).Item("DETAIL_ID"))
        Next
        SQL = "DELETE FROM RPT_ST_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " And TAG_ID=" & TAG_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_ST_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim SQL As String = "Select * FROM RPT_ST_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_ST_Picture(DT.Rows(i).Item("DETAIL_ID"))
        Next
        SQL = "DELETE FROM RPT_ST_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_ST_Detail(ByVal DETAIL_ID As Integer)

        Drop_RPT_ST_Picture(DETAIL_ID)
        '--------- Drop Sector ------------
        Drop_Stationary_Template_Sector(Get_RPT_ST_TYPE_BY_DETAIL(DETAIL_ID), DETAIL_ID)

        Dim SQL As String = "DELETE FROM RPT_ST_Detail " & vbNewLine
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Execute_Command(SQL)

    End Sub

    Public Function Get_RPT_ST_TYPE_BY_DETAIL(ByVal DETAIL_ID As Integer) As Report_Type
        Dim SQL As String = "SELECT RPT_Type_ID FROM VW_REPORT_ST_DETAIL" & vbNewLine
        SQL &= "WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Return DT.Rows(0).Item("RPT_Type_ID")
        Else
            Return Report_Type.Unknown
        End If
    End Function


    Public Sub Drop_RPT_ST_Detail_CheckFlag(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "Select * FROM RPT_ST_Detail " & vbNewLine
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then

            Drop_RPT_ST_Picture(DETAIL_ID)
            '--------- Drop Sector ------------
            Drop_Stationary_Template_Sector(Get_RPT_ST_TYPE_BY_DETAIL(DETAIL_ID), DETAIL_ID)

            If DT.Rows(0).Item("Create_Flag") = "New" Then
                SQL = "DELETE FROM RPT_ST_Detail " & vbNewLine
                SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
                Execute_Command(SQL)
            Else ' Main
                DT.Rows(0).Item("PROB_Detail") = DBNull.Value
                DT.Rows(0).Item("PROB_Recomment") = DBNull.Value
                DT.Rows(0).Item("Fixed") = DBNull.Value
                DT.Rows(0).Item("ICLS_ID") = DBNull.Value
                DT.Rows(0).Item("Create_Flag") = "Main"
                DT.Rows(0).Item("Responsible") = DBNull.Value
                DT.Rows(0).Item("PIC_Detail1") = False
                DT.Rows(0).Item("PIC_Detail2") = False
                DT.Rows(0).Item("Detail1") = DBNull.Value
                DT.Rows(0).Item("Detail2") = DBNull.Value
                DT.Rows(0).Item("Update_Time") = Now

                Try
                    Dim cmd As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                    DT.AcceptChanges()
                Catch ex As Exception
                    Threading.Thread.Sleep(500)
                    Drop_RPT_ST_Detail_CheckFlag(DETAIL_ID)
                End Try

            End If
        End If
    End Sub

    Public Sub Drop_RPT_ST_Picture(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "Select * FROM RPT_ST_Detail " & vbNewLine
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            DT.Rows(0).Item("PIC_Detail1") = False
            DT.Rows(0).Item("PIC_Detail2") = False
            DT.Rows(0).Item("Detail1") = DBNull.Value
            DT.Rows(0).Item("Detail2") = DBNull.Value
            '-------------------- Delete File ---------------
            For i As Integer = 1 To 2
                Dim FileName As String = Picture_Path & "\" & DT.Rows(0).Item("RPT_Year") & "\" & DT.Rows(0).Item("RPT_No") & "\" & DETAIL_ID & "_" & i
                If File.Exists(FileName) Then
                    Try
                        Kill(FileName)
                    Catch
                    End Try
                End If
            Next
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
            DT.AcceptChanges()
        End If

    End Sub

    Public Sub Drop_RPT_SO_DOC(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim SQL As String = "DELETE FROM SO_DOC " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_SO_DOC(ByVal DOC_ID As Integer)
        Dim SQL As String = "DELETE FROM SO_DOC " & vbNewLine
        SQL &= "WHERE  DOC_ID=" & DOC_ID
        Execute_Command(SQL)
    End Sub
#End Region

    Public Function Get_ATTACHMENT_FILE_TYPE() As DataTable
        Dim SQL As String = "Select AFT_ID,AFT_Name_EN,AFT_Name_TH,UIColor,Active_Status " & vbLf
        SQL &= " FROM MS_ATTACHMENT_FILE_TYPE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_FILE_EXTENSION_INFO() As DataTable
        Dim SQL As String = "Select * " & vbLf
        SQL &= " FROM MS_FILE_EXTENSION"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_TM_Instrument_Info(Optional ByVal INSM_ID As Integer = 0) As DataTable
        Dim SQL As String = "Select MS_TM_Instrument.*,INSM_Type_Name FROM MS_TM_Instrument" & vbLf
        SQL &= "INNER JOIN MS_TM_Instrument_Type On MS_TM_Instrument.INSM_Type_ID=MS_TM_Instrument_Type.INSM_Type_ID" & vbLf
        If INSM_ID <> 0 Then
            SQL &= "WHERE INSM_ID=" & INSM_ID & vbLf
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

#Region "Pipe"

    '------------- เตรียมเอาออก เพราะไปใช้ FileAttachment แทน -----------
    Public Class PipeDrawingDetail
        Public File_ID As Integer = 0
        Public File_Type As String = ""
        Public File_Data As Byte() = {}

        Public Sub Dispose()
            Me.Dispose()
        End Sub
    End Class


#End Region

#Region "RO"

    Public Function Check_Previous_Incomplete_RO_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, Optional ByVal TAG_ID As Integer = 0) As DataTable
        Dim SQL As String = "EXEC dbo.SP_Get_Previous_Incompleted_RO_Report " & RPT_Year & "," & RPT_No & ","
        If TAG_ID <> 0 Then
            SQL &= TAG_ID
        Else
            SQL &= "NULL"
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Sub Drop_RPT_RO_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)

        Drop_RPT_RO_Detail(RPT_Year, RPT_No)
        Drop_RPT_RO_eMonitor(RPT_Year, RPT_No)
        Drop_RPT_RO_DOC(RPT_Year, RPT_No)

        Dim SQL As String = "DELETE FROM RPT_RO_Header " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_RO_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal TAG_ID As Integer)
        Dim SQL As String = "Select * FROM RPT_RO_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " And TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_RO_Picture(DT.Rows(i).Item("DETAIL_ID"))
        Next
        SQL = "DELETE FROM RPT_RO_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " And TAG_ID=" & TAG_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_RO_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim SQL As String = "Select * FROM RPT_RO_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_RO_Picture(DT.Rows(0).Item("DETAIL_ID"))
        Next
        SQL = "DELETE FROM RPT_RO_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_RO_Detail(ByVal DETAIL_ID As Integer)
        Drop_RPT_RO_Picture(DETAIL_ID)
        Dim SQL As String = "DELETE FROM RPT_RO_Detail " & vbNewLine
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_RO_Detail_CheckFlag(ByVal DETAIL_ID As Integer)

        Dim SQL As String = "Select * FROM RPT_RO_Detail " & vbNewLine
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("Create_Flag") = "New" Then
                Drop_RPT_ST_Picture(DETAIL_ID)
                SQL = "DELETE FROM RPT_RO_Detail " & vbNewLine
                SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
                Execute_Command(SQL)
            Else
                Dim DR As DataRow = DT.Rows(0)

                DR("PROB_Detail") = ""
                DR("PROB_Recomment") = ""
                DR("ICLS_ID") = DBNull.Value
                If IsDBNull(DR("LAST_DETAIL_ID")) OrElse DR("LAST_DETAIL_ID") = 0 Then '---------- ปัญหาใหม่ปรับ Status กลับ ----------
                    DR("STATUS_ID") = DBNull.Value
                End If
                DR("Create_Flag") = "Main"
                DR("Responsible") = ""
                Drop_RPT_ST_Picture(DETAIL_ID)
                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT)
                DT.AcceptChanges()
            End If
        End If
    End Sub

    Public Sub Drop_RPT_RO_Picture(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "Select * FROM RPT_RO_Detail " & vbNewLine
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            DT.Rows(0).Item("PIC_Detail1") = False
            DT.Rows(0).Item("PIC_Detail2") = False
            DT.Rows(0).Item("Detail1") = ""
            DT.Rows(0).Item("Detail2") = ""
            '-------------------- Delete File ---------------
            For i As Integer = 1 To 2
                Dim FileName As String = Picture_Path & "\" & DT.Rows(0).Item("RPT_Year") & "\" & DT.Rows(0).Item("RPT_No") & "\" & DETAIL_ID & "_" & i
                If File.Exists(FileName) Then
                    Try
                        Kill(FileName)
                    Catch
                    End Try
                End If
            Next
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
            DT.AcceptChanges()
        End If
    End Sub

    Public Sub Drop_RPT_RO_eMonitor(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim SQL As String = "DELETE FROM RPT_Rotating_Routine_Import " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_RO_DOC(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim SQL As String = "DELETE FROM RO_DOC " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_RO_DOC(ByVal DOC_ID As Integer)
        Dim SQL As String = "DELETE FROM RO_DOC " & vbNewLine
        SQL &= "WHERE  DOC_ID=" & DOC_ID
        Execute_Command(SQL)
    End Sub


    Public Sub Drop_RPT_LO_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_LO_Detail(RPT_Year, RPT_No)
        Dim SQL As String = "DELETE FROM RPT_LO_Header " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub


    Public Sub Drop_RPT_LO_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal TAG_ID As Integer)
        Dim SQL As String = "DELETE FROM RPT_LO_Detail " & vbNewLine
        SQL &= "WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " And LO_TAG_ID=" & TAG_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_LO_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim SQL As String = "DELETE FROM RPT_LO_Detail " & vbNewLine
        SQL &= "WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_LO_Detail(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "DELETE FROM RPT_LO_Detail " & vbNewLine
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Execute_Command(SQL)
    End Sub

#End Region

#Region "PdMA"
    Public Sub Construct_PDMA_Report_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RouteID As Integer)
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        SQL = "Select * FROM MS_PDMA_TAG" & vbLf
        SQL &= "WHERE Active_Status=1 And ROUTE_ID = " & RouteID & vbLf
        SQL &= " ORDER BY TAG_CODE" & vbLf
        Dim Tag As New DataTable
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Tag)

        '-------------------- Remove Unused Tag --------------------
        DT = New DataTable
        DA = New SqlDataAdapter("Select * FROM RPT_PDMA_Detail WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No, ConnStr)
        DA.Fill(DT)

        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            Tag.DefaultView.RowFilter = "TAG_ID=" & DT.Rows(i).Item("TAG_ID")
            If Tag.DefaultView.Count = 0 Then
                DT.Rows(i).Delete()
            End If
        Next
        Try
            Dim CMD As New SqlCommandBuilder(DA)
            DA.Update(DT) : DT.AcceptChanges()
        Catch : End Try

        '--------------------- Add New Tag ----------------------
        For i As Integer = 0 To Tag.Rows.Count - 1
            DT.DefaultView.RowFilter = "TAG_ID=" & Tag.Rows(i).Item("TAG_ID")

            If DT.DefaultView.Count = 0 Then
                '------------------- Add Information ------------------
                Dim DR As DataRow = DT.NewRow
                DR("DETAIL_ID") = GetNew_PDMA_DetailID()
                DR("RPT_Year") = RPT_Year
                DR("RPT_No") = RPT_No
                DR("ROUTE_ID") = Tag.Rows(i).Item("ROUTE_ID")
                DR("TAG_ID") = Tag.Rows(i).Item("TAG_ID")
                DR("TAG_CODE") = Tag.Rows(i).Item("TAG_CODE")
                DR("TAG_Name") = Tag.Rows(i).Item("TAG_Name")
                DR("TAG_Mode") = 0
                DR("Update_Time") = Now
                DT.Rows.Add(DR)
            Else
                '--------------- Update Information -------------------
                Dim DR As DataRow = DT.DefaultView(0).Row
                DR("ROUTE_ID") = Tag.Rows(i).Item("ROUTE_ID")
                DR("TAG_ID") = Tag.Rows(i).Item("TAG_ID")
                DR("TAG_CODE") = Tag.Rows(i).Item("TAG_CODE")
                DR("TAG_Name") = Tag.Rows(i).Item("TAG_Name")
                DR("Update_Time") = Now
            End If
            Try
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT) : DT.AcceptChanges()
            Catch : End Try
        Next
    End Sub

    Public Function GetNew_PDMA_DetailID() As Integer
        Dim DA As New SqlDataAdapter("Select ISNULL(MAX(DETAIL_ID),0)+1 FROM RPT_PDMA_Detail", ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

    Public Sub BindDDl_PDMA_Route(ByVal PLANT_ID As Integer, ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "Select * FROM MS_PDMA_ROUTE WHERE active_status=1 And PLANT_ID=" & PLANT_ID & " ORDER BY ROUTE_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Route...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("ROUTE_CODE"), DT.Rows(i).Item("ROUTE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex <1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Function Get_PDMA_AnalyzingCS_Text(ByVal Value As Double) As String
        Select Case True
            Case Value <0
                           Return ""
            Case Value < 30
                Return "Severe"
            Case Value <= 36
                Return "Severe"
            Case Value <= 42
                Return "Caution"
            Case Value <= 48
                Return "Observe"
            Case Value <= 54
                Return "Moderate"
            Case Value <= 60
                Return "Good"
            Case Value > 60
                Return "Excellent"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_PDMA_AnalyzingCS_CSS(ByVal Value As Double) As String
        Select Case True
            Case Value < 0
                Return "PDMA_CS_0"
            Case Value < 30
                Return "PDMA_CS_7"
            Case Value <= 36
                Return "PDMA_CS_6"
            Case Value <= 42
                Return "PDMA_CS_5"
            Case Value <= 48
                Return "PDMA_CS_4"
            Case Value <= 54
                Return "PDMA_CS_3"
            Case Value <= 60
                Return "PDMA_CS_2"
            Case Value > 60
                Return "PDMA_CS_1"
            Case Else
                Return "PDMA_CS_0"
        End Select
    End Function

    Public Function Get_PDMA_AnalyzingEccentricity_Text(ByVal Value As Double) As String
        Select Case True
            Case Value < 0
                Return ""
            Case Value <= 10
                Return "Good"
            Case Value <= 20
                Return "Moderate"
            Case Value > 20
                Return "Severe"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_PDMA_AnalyzingEccentricity_CSS(ByVal Value As Double) As String
        Select Case True
            Case Value < 0
                Return ""
            Case Value <= 10
                Return "PDMA_Eccentricity_Good"
            Case Value <= 20
                Return "PDMA_Eccentricity_Moderate"
            Case Value > 20
                Return "PDMA_Eccentricity_Severe"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_PDMA_AnalyzingPI_Text(ByVal Value As Double) As String
        Select Case True
            Case Value < 0
                Return ""
            Case Value < 1
                Return "Severe"
            Case Value < 1.5
                Return "Caution"
            Case Value < 2
                Return "Observe"
            Case Value < 5
                Return "Good"
            Case Value <= 7
                Return "*Caution"
            Case Value > 7
                Return "*Severe"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_PDMA_AnalyzingPI_CSS(ByVal Value As Double) As String
        Select Case True
            Case Value < 0
                Return "PDMA_PI"
            Case Value < 1
                Return "PDMA_PI_Severe"
            Case Value < 1.5
                Return "PDMA_PI_Caution"
            Case Value < 2
                Return "PDMA_PI_Observe"
            Case Value < 5
                Return "PDMA_PI_Good"
            Case Value <= 7
                Return "PDMA_PI_Caution"
            Case Value > 7
                Return "PDMA_PI_Severe"
            Case Else
                Return "PDMA_PI"
        End Select
    End Function

    Public Function Get_PDMA_Color_CSS(ByVal Value As String) As String
        Select Case True
            Case Value.ToUpper = ""
                Return "PDMA_PI"
            Case Value.ToUpper = "RED"
                Return "PDMA_PI_Severe"
            Case Value.ToUpper = "ORANGE"
                Return "PDMA_PI_Caution"
            Case Value.ToUpper = "YELLOW"
                Return "PDMA_PI_Observe"
            Case Value.ToUpper = "GREEN"
                Return "PDMA_PI_Good"
            Case Else
                Return "PDMA_PI"
        End Select
    End Function

    Public Function Get_PDMA_Color_Text(ByVal Value As String) As String
        Select Case True
            Case Value.ToUpper = ""
                Return ""
            Case Value.ToUpper = "RED"
                Return "Severe"
            Case Value.ToUpper = "ORANGE"
                Return "Caution"
            Case Value.ToUpper = "YELLOW"
                Return "Observe"
            Case Value.ToUpper = "GREEN"
                Return "Good"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_PDMA_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DETAIL_ID As Integer, ByVal ImgName As String) As Byte()
        Dim SQL As String = "Select " & ImgName & ",RPT_Year,RPT_No FROM RPT_PDMA_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso DT.Rows(0).Item(ImgName).ToString <> "" Then
            Dim ImgPath As String = DT.Rows(0).Item(ImgName).ToString
            If File.Exists(ImgPath) Then
                Dim C As New Converter
                Dim F As FileStream = File.Open(ImgPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim S As Byte() = C.StreamToByte(F)
                F.Close()
                F.Dispose()
                Return S
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public Function Get_PDMA_Problem_Recomment(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = "Select DETAIL_ID,TAG_CODE"
        SQL &= " FROM RPT_PDMA_Detail "
        SQL &= " WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        SQL &= " ORDER BY TAG_CODE"
        Dim TAG As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(TAG)

        Dim Result As New DataTable
        SQL = "EXEC dbo.SP_PDMA_PROB_Comment 0"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Result)
        '------------- Get Default Structure ---------
        For i As Integer = 0 To TAG.Rows.Count - 1
            Dim DT As DataTable = Get_PDMA_Problem_Recomment(TAG.Rows(i).Item("DETAIL_ID"))
            Result.Merge(DT)
        Next
        Return Result
    End Function

    Public Function Get_PDMA_Problem_Recomment(ByVal Detail_ID As Integer) As DataTable
        Dim SQL As String = "EXEC dbo.SP_PDMA_PROB_Comment " & Detail_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Sub Drop_RPT_PDMA_Comment(ByVal DETAIL_ID As Integer, ByVal PROB_Code As String)
        Dim SQL As String = "DELETE FROM RPT_PDMA_PROB_Comment " & vbLf
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID & " And PROB_Code='" & PROB_Code.Replace("'", "''") & "'"
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_PDMA_Comment(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "DELETE FROM RPT_PDMA_PROB_Comment " & vbLf
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_PDMA_Detail(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "SELECT RPT_Year,RPT_No FROM RPT_PDMA_Detail " & vbLf
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Exit Sub

        '------------- Delete Picture -----------
        Dim RPT_Year As Integer = DT.Rows(0).Item("RPT_Year")
        Dim RPT_No As Integer = DT.Rows(0).Item("RPT_Year")
        Dim Path As String = Picture_Path & "\" & RPT_Year & "\" & RPT_No & "\"
        If Directory.Exists(Path) Then
            For i As Integer = 1 To 5
                Try
                    File.Delete(Path & DETAIL_ID & "_" & i)
                Catch : End Try
            Next
        End If

        Drop_RPT_PDMA_Comment(DETAIL_ID)
        SQL = "DELETE FROM RPT_PDMA_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_PDMA_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim SQL As String = "SELECT DETAIL_ID FROM RPT_PDMA_Detail " & vbLf
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_PDMA_Detail(DT.Rows(i).Item("DETAIL_ID"))
        Next
        SQL = "DELETE FROM RPT_PDMA_Detail "
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_PDMA_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_PDMA_Detail(RPT_Year, RPT_No)
        Dim SQL As String = "DELETE FROM RPT_PDMA_Header " & vbLf
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub
#End Region

#Region "MTAP"
    Public Sub Construct_MTAP_Report_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal PlantID As Integer)
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        SQL = "SELECT * FROM MS_MTAP_TAG" & vbLf
        SQL &= "WHERE Active_Status=1 and Plant_ID = " & PlantID & vbLf
        SQL &= " ORDER BY TAG_CODE" & vbLf
        Dim Tag As New DataTable
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Tag)

        '-------------------- Remove Unused Tag --------------------
        DT = New DataTable
        DA = New SqlDataAdapter("SELECT * FROM RPT_MTAP_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, ConnStr)
        DA.Fill(DT)

        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            Tag.DefaultView.RowFilter = "TAG_ID=" & DT.Rows(i).Item("TAG_ID")
            If Tag.DefaultView.Count = 0 Then
                DT.Rows(i).Delete()
            End If
        Next
        Try
            Dim CMD As New SqlCommandBuilder(DA)
            DA.Update(DT) : DT.AcceptChanges()
        Catch : End Try

        '--------------------- Add New Tag ----------------------
        For i As Integer = 0 To Tag.Rows.Count - 1
            DT.DefaultView.RowFilter = "TAG_ID=" & Tag.Rows(i).Item("TAG_ID")

            If DT.DefaultView.Count = 0 Then
                '------------------- Add Information ------------------
                Dim DR As DataRow = DT.NewRow
                DR("DETAIL_ID") = GetNew_MTAP_DetailID()
                DR("RPT_Year") = RPT_Year
                DR("RPT_No") = RPT_No
                DR("PLANT_ID") = Tag.Rows(i).Item("PLANT_ID")
                DR("TAG_ID") = Tag.Rows(i).Item("TAG_ID")
                DR("TAG_CODE") = Tag.Rows(i).Item("TAG_CODE")
                DR("TAG_Name") = Tag.Rows(i).Item("TAG_Name")
                DR("TAG_Mode") = 0
                DR("Update_Time") = Now
                DT.Rows.Add(DR)
            Else
                '--------------- Update Information -------------------
                Dim DR As DataRow = DT.DefaultView(0).Row
                DR("PLANT_ID") = Tag.Rows(i).Item("PLANT_ID")
                DR("TAG_ID") = Tag.Rows(i).Item("TAG_ID")
                DR("TAG_CODE") = Tag.Rows(i).Item("TAG_CODE")
                DR("TAG_Name") = Tag.Rows(i).Item("TAG_Name")
                DR("Update_Time") = Now
            End If
            Try
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT) : DT.AcceptChanges()
            Catch : End Try
        Next
    End Sub

    Public Function GetNew_MTAP_DetailID() As Integer
        Dim DA As New SqlDataAdapter("SELECT ISNULL(MAX(DETAIL_ID),0)+1 FROM RPT_MTAP_Detail", ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

    Public Function Get_MTAP_Image(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DETAIL_ID As Integer, ByVal ImgName As String) As Byte()
        Dim SQL As String = "SELECT " & ImgName & ",RPT_Year,RPT_No FROM RPT_MTAP_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso DT.Rows(0).Item(ImgName).ToString <> "" Then
            Dim ImgPath As String = DT.Rows(0).Item(ImgName).ToString
            If File.Exists(ImgPath) Then
                Dim C As New Converter
                Dim F As FileStream = File.Open(ImgPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim S As Byte() = C.StreamToByte(F)
                F.Close()
                F.Dispose()
                Return S
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public Function Get_MTAP_Problem_Recomment(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = "SELECT DETAIL_ID,TAG_CODE"
        SQL &= " FROM RPT_MTAP_Detail "
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        SQL &= " ORDER BY TAG_CODE"
        Dim TAG As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(TAG)

        Dim Result As New DataTable
        SQL = "EXEC dbo.SP_MTAP_PROB_Comment 0"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Result)
        '------------- Get Default Structure ---------
        For i As Integer = 0 To TAG.Rows.Count - 1
            Dim DT As DataTable = Get_MTAP_Problem_Recomment(TAG.Rows(i).Item("DETAIL_ID"))
            Result.Merge(DT)
        Next
        Return Result
    End Function

    Public Function Get_MTAP_Problem_Recomment(ByVal Detail_ID As Integer) As DataTable
        Dim SQL As String = "EXEC dbo.SP_MTAP_PROB_Comment " & Detail_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Sub Drop_RPT_MTAP_Comment(ByVal DETAIL_ID As Integer, ByVal PROB_Code As String)
        Dim SQL As String = "DELETE FROM RPT_MTAP_PROB_Comment " & vbLf
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID & " AND PROB_Code='" & PROB_Code.Replace("'", "''") & "'"
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_MTAP_Comment(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "DELETE FROM RPT_MTAP_PROB_Comment " & vbLf
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_MTAP_Detail(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "SELECT RPT_Year,RPT_No FROM RPT_MTAP_Detail " & vbLf
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Exit Sub

        '------------- Delete Picture -----------
        Dim RPT_Year As Integer = DT.Rows(0).Item("RPT_Year")
        Dim RPT_No As Integer = DT.Rows(0).Item("RPT_Year")
        Dim Path As String = Picture_Path & "\" & RPT_Year & "\" & RPT_No & "\"
        If Directory.Exists(Path) Then
            For i As Integer = 1 To 5
                Try
                    File.Delete(Path & DETAIL_ID & "_" & i)
                Catch : End Try
            Next
        End If

        Drop_RPT_MTAP_Comment(DETAIL_ID)
        SQL = "DELETE FROM RPT_MTAP_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_MTAP_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim SQL As String = "SELECT DETAIL_ID FROM RPT_MTAP_Detail " & vbLf
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_MTAP_Detail(DT.Rows(i).Item("DETAIL_ID"))
        Next
        SQL = "DELETE FROM RPT_MTAP_Detail "
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_MTAP_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_MTAP_Detail(RPT_Year, RPT_No)
        Dim SQL As String = "DELETE FROM RPT_MTAP_Header " & vbLf
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub
#End Region

#Region "Thermography"
    Public Sub Construct_THM_Report_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal RouteID As Integer)
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        SQL = "SELECT * FROM MS_THM_TAG" & vbLf
        SQL &= "WHERE Active_Status=1 and ROUTE_ID = " & RouteID & vbLf
        SQL &= " ORDER BY TAG_CODE" & vbLf
        Dim Tag As New DataTable
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Tag)

        '-------------------- Remove Unused Tag --------------------
        DT = New DataTable
        DA = New SqlDataAdapter("SELECT * FROM RPT_THM_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, ConnStr)
        DA.Fill(DT)

        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            Tag.DefaultView.RowFilter = "TAG_ID=" & DT.Rows(i).Item("TAG_ID")
            If Tag.DefaultView.Count = 0 Then
                DT.Rows(i).Delete()
            End If
        Next
        Try
            Dim CMD As New SqlCommandBuilder(DA)
            DA.Update(DT) : DT.AcceptChanges()
        Catch : End Try

        '--------------------- Add New Tag ----------------------
        For i As Integer = 0 To Tag.Rows.Count - 1
            DT.DefaultView.RowFilter = "TAG_ID=" & Tag.Rows(i).Item("TAG_ID")

            If DT.DefaultView.Count = 0 Then
                '------------------- Add Information ------------------
                Dim DR As DataRow = DT.NewRow
                DR("DETAIL_ID") = GetNew_THM_DetailID()
                DR("RPT_Year") = RPT_Year
                DR("RPT_No") = RPT_No
                DR("ROUTE_ID") = Tag.Rows(i).Item("ROUTE_ID")
                DR("TAG_ID") = Tag.Rows(i).Item("TAG_ID")
                DR("TAG_CODE") = Tag.Rows(i).Item("TAG_CODE")
                DR("TAG_Name") = Tag.Rows(i).Item("TAG_Name")
                DR("Update_Time") = Now
                DT.Rows.Add(DR)
            Else
                '--------------- Update Information -------------------
                Dim DR As DataRow = DT.DefaultView(0).Row
                DR("ROUTE_ID") = Tag.Rows(i).Item("ROUTE_ID")
                DR("TAG_ID") = Tag.Rows(i).Item("TAG_ID")
                DR("TAG_CODE") = Tag.Rows(i).Item("TAG_CODE")
                DR("TAG_Name") = Tag.Rows(i).Item("TAG_Name")
                DR("Update_Time") = Now
            End If
            Try
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT) : DT.AcceptChanges()
            Catch : End Try
        Next
    End Sub

    Public Function GetNew_THM_DetailID() As Integer
        Dim DA As New SqlDataAdapter("SELECT ISNULL(MAX(DETAIL_ID),0)+1 FROM RPT_THM_Detail", ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

    Public Sub BindDDl_THM_Route(ByVal THM_TYPE_ID As Integer, ByVal PLANT_ID As Integer, ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT * FROM MS_THM_ROUTE WHERE active_status=1 AND THM_TYPE_ID = " & THM_TYPE_ID & " AND PLANT_ID=" & PLANT_ID & " ORDER BY ROUTE_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Route...", 0))

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("ROUTE_CODE"), DT.Rows(i).Item("ROUTE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_THM_Route_Report(ByVal THM_TYPE_ID As Integer, ByVal PLANT_ID As Integer, ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT * FROM MS_THM_ROUTE WHERE active_status=1 AND THM_TYPE_ID = " & THM_TYPE_ID & " AND PLANT_ID=" & PLANT_ID & " ORDER BY ROUTE_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If DT.Rows.Count > 1 Then
            ddl.Items.Add(New ListItem("Choose a Route...", 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("ROUTE_CODE"), DT.Rows(i).Item("ROUTE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDl_THM_Type(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT * FROM MS_THM_TYPE WHERE active_status=1 ORDER BY THM_TYPE_Name"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Equipement Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("THM_TYPE_Name"), DT.Rows(i).Item("THM_TYPE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub Save_THM_File(ByVal ReportCode As String, ByVal FileIndex As Integer, ByVal DETAIL_ID As Integer, ByVal FileBinary As Byte())

        '------------- Save As File-------------


        Dim Folder As String = PostedReport_Path & "\THM\" & ReportCode
        If Not Directory.Exists(Folder) Then
            Directory.CreateDirectory(Folder)
        End If

        Dim FileName As String = ReportCode & "-" & FileIndex.ToString.PadLeft(3, "0")
        Dim FilePath As String = Folder & "\" & FileName
        If File.Exists(FilePath) Then
            File.Delete(FilePath)
        End If
        Dim F As FileStream = File.OpenWrite(FilePath)
        F.Write(FileBinary, 0, FileBinary.Length)
        F.Close()

        '----------- Save To Database-----------
        Dim Sql As String = "UPDATE RPT_THM_Detail SET Result_FileName='" & FileName & "' WHERE Detail_ID = " & DETAIL_ID
        Dim Conn As New SqlConnection(ConnStr)
        Conn.Open()
        Dim Com As New SqlCommand
        Com.Connection = Conn
        Com.CommandType = CommandType.Text
        Com.CommandText = Sql
        Com.ExecuteNonQuery()
        Com.Clone()
        Com.Dispose()
        Conn.Close()
        Conn.Dispose()

    End Sub

    Public Sub Drop_THM_File(ByVal ReportCode As String, ByVal FileIndex As Integer, ByVal DETAIL_ID As Integer)

        '------------- Drop Physical File-------------
        Dim Folder As String = PostedReport_Path & "\THM"
        Dim FileName As String = ReportCode & "-" & FileIndex.ToString.PadLeft(3, "0")
        Dim FilePath As String = Folder & "\" & ReportCode & "\" & FileName
        If File.Exists(FilePath) Then
            File.Delete(FilePath)
        End If

        '----------- Save To Database-----------
        Dim Sql As String = "UPDATE RPT_THM_Detail SET Result_FileName=NULL WHERE Detail_ID = " & DETAIL_ID
        Dim Conn As New SqlConnection(ConnStr)
        Conn.Open()
        Dim Com As New SqlCommand
        Com.Connection = Conn
        Com.CommandType = CommandType.Text
        Com.CommandText = Sql
        Com.ExecuteNonQuery()
        Com.Clone()
        Com.Dispose()
        Conn.Close()
        Conn.Dispose()

    End Sub

    Public Sub Drop_RPT_THM_Comment(ByVal DETAIL_ID As Integer, ByVal PROB_Code As String)
        Dim SQL As String = "DELETE FROM RPT_THM_PROB_Comment " & vbLf
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID & " AND PROB_Code='" & PROB_Code.Replace("'", "''") & "'"
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_THM_Comment(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "DELETE FROM RPT_THM_PROB_Comment " & vbLf
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_THM_Detail(ByVal DETAIL_ID As Integer)
        Dim SQL As String = "SELECT RPT_Year,RPT_No FROM RPT_THM_Detail " & vbLf
        SQL &= "WHERE  DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Exit Sub

        '------------- Delete Picture -----------
        Dim RPT_Year As Integer = DT.Rows(0).Item("RPT_Year")
        Dim RPT_No As Integer = DT.Rows(0).Item("RPT_Year")
        Dim Path As String = Picture_Path & "\" & RPT_Year & "\" & RPT_No & "\"
        If Directory.Exists(Path) Then
            For i As Integer = 1 To 5
                Try
                    File.Delete(Path & DETAIL_ID & "_" & i)
                Catch : End Try
            Next
        End If

        Drop_RPT_THM_Comment(DETAIL_ID)
        SQL = "DELETE FROM RPT_THM_Detail WHERE DETAIL_ID=" & DETAIL_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_THM_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Dim SQL As String = "SELECT DETAIL_ID FROM RPT_THM_Detail " & vbLf
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_RPT_THM_Detail(DT.Rows(i).Item("DETAIL_ID"))
        Next
        SQL = "DELETE FROM RPT_THM_Detail "
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_RPT_THM_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
        Drop_RPT_THM_Detail(RPT_Year, RPT_No)
        Dim SQL As String = "DELETE FROM RPT_THM_Header " & vbLf
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Execute_Command(SQL)
    End Sub

    Public Function Get_THM_Problem_Recomment(ByVal RPT_Year As Integer, ByVal RPT_No As Integer) As DataTable
        Dim SQL As String = "SELECT DETAIL_ID,TAG_CODE"
        SQL &= " FROM RPT_THM_Detail "
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        SQL &= " ORDER BY TAG_CODE"
        Dim TAG As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(TAG)

        Dim Result As New DataTable
        SQL = "EXEC dbo.SP_THM_PROB_Comment 0"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Result)
        '------------- Get Default Structure ---------
        For i As Integer = 0 To TAG.Rows.Count - 1
            Dim DT As DataTable = Get_THM_Problem_Recomment(TAG.Rows(i).Item("DETAIL_ID"))
            Result.Merge(DT)
        Next
        Return Result
    End Function

    Public Function Get_THM_Problem_Recomment(ByVal Detail_ID As Integer) As DataTable
        Dim SQL As String = "EXEC dbo.SP_THM_PROB_Comment " & Detail_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function
#End Region

#Region "PDF"
    Public Sub PushString(ByRef Arr As String(), ByVal NextString As String)
        Array.Resize(Arr, Arr.Length + 1)
        Arr(Arr.Length - 1) = NextString
    End Sub

    Public Sub MergePDF(ByVal PDFList As String(), ByVal OutputPath As String)

        iTextSharp.text.Document.Compress = True

        Dim PDFFileName As String = PDFList(0)
        Dim reader As New PdfReader(PDFFileName)
        Dim document As New iTextSharp.text.Document(reader.GetPageSizeWithRotation(1))

        Dim fs As New FileStream(OutputPath, FileMode.Create)
        Dim copy As New PdfCopy(document, fs)
        copy.SetPdfVersion(PdfWriter.PDF_VERSION_1_5)
        'copy.CompressionLevel = PdfStream.BEST_SPEED

        Dim stamper As New PdfStamper(reader, fs)
        stamper.SetFullCompression()

        document.Open()

        '----------------- Read First File -------------
        For p As Integer = 1 To reader.NumberOfPages
            copy.AddPage(copy.GetImportedPage(reader, p))
            copy.SetPdfVersion(PdfWriter.PDF_VERSION_1_5)
        Next
        reader.Close()

        '-------------------Read Other File-------------
        For i As Integer = 1 To PDFList.Length - 1

            If Not File.Exists(PDFList(i)) Then Continue For

            reader = New PdfReader(PDFList(i))
            For p As Integer = 1 To reader.NumberOfPages
                copy.AddPage(copy.GetImportedPage(reader, p))
                copy.SetPdfVersion(PdfWriter.PDF_VERSION_1_5)
                'copy.CompressionLevel = PdfStream.BEST_SPEED
            Next
            reader.Close()
        Next

        document.Close()
        copy.Close()
        fs.Close()
        fs.Dispose()

        '------------------ Delete Temp File ------------
        For i As Integer = 0 To PDFList.Length - 1
            Try
                File.Delete(PDFList(i))
            Catch ex As Exception
            End Try
        Next

    End Sub

    Public Class Document_Detail
        Public DOC_Type As String
        Public DOC_BIN As Byte()
    End Class

    'Public Function GetDocumentDetail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DOC_ID As Integer) As Document_Detail
    '    Dim DT As New DataTable
    '    Dim SQL As String = "SELECT DOC_BIN,DOC_Type FROM RPT_OffRoutine_Doc WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND DOC_ID=" & DOC_ID
    '    Dim DA As New SqlDataAdapter(SQL, ConnStr)
    '    DA.Fill(DT)
    '    If DT.Rows.Count = 0 Then
    '        Return Nothing
    '    Else
    '        Dim Result As New Document_Detail
    '        With Result
    '            .DOC_Type = DT.Rows(0).Item("DOC_Type")
    '            .DOC_BIN = DT.Rows(0).Item("DOC_BIN")
    '        End With
    '        Return Result
    '    End If
    'End Function

#End Region

#Region "Binding"

    Public Sub BindDDlLOType(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)

        Dim SQL As String = "SELECT * FROM MS_LO_Oil_Type WHERE active_status=1 ORDER BY Oil_TYPE_Name"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Oil Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Oil_TYPE_Name"), DT.Rows(i).Item("Oil_TYPE_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Public Sub BindDDlTagType(ByVal ST_or_RO As String, ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)

        Dim SQL As String = ""
        Select Case ST_or_RO.ToUpper
            Case "ST"
                SQL = "SELECT * FROM MS_ST_TAG_TYPE WHERE active_status=1 ORDER BY TAG_TYPE_Name"
            Case "RO"
                SQL = "SELECT * FROM MS_RO_TAG_TYPE WHERE active_status=1 ORDER BY TAG_TYPE_Name"
        End Select

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Equipement-Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TAG_TYPE_Name"), DT.Rows(i).Item("TAG_TYPE_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Public Sub BindDDlProcess(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)

        Dim SQL As String = "SELECT * FROM MS_PROCESS WHERE active_status=1 ORDER BY PROC_Code"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Process...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PROC_Code"), DT.Rows(i).Item("PROC_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next

    End Sub

    Public Sub BindDDlArea(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_AREA WHERE active_status=1 ORDER BY AREA_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose an Area...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("AREA_CODE"), DT.Rows(i).Item("AREA_ID"))
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Public Sub BindDDlArea(ByVal PLANT_ID As Integer, ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_AREA WHERE active_status=1 AND PLANT_ID=" & PLANT_ID & " ORDER BY AREA_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose an Area...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("AREA_CODE"), DT.Rows(i).Item("AREA_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Public Sub BindDDl_ST_Route(ByVal PLANT_ID As Integer, ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)

        Dim SQL As String = "SELECT * FROM MS_ST_ROUTE WHERE active_status=1 AND PLANT_ID=" & PLANT_ID & " ORDER BY ROUTE_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Route...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("ROUTE_CODE"), DT.Rows(i).Item("ROUTE_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Public Sub BindDDl_RO_Route(ByVal PLANT_ID As Integer, ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_RO_ROUTE WHERE active_status=1 AND PLANT_ID=" & PLANT_ID & " ORDER BY ROUTE_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose a Route...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("ROUTE_CODE"), DT.Rows(i).Item("ROUTE_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Public Sub BindDDl_AttachmentType(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_ATTACHMENT_FILE_TYPE WHERE active_status=1 "
        SQL &= "ORDER BY AFT_ID "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Attachment Type...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("AFT_Name_EN"), DT.Rows(i).Item("AFT_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

#End Region

#Region "SQL Execution"
    Public Function Execute_DataTable(sql As String) As DataTable
        Dim DT As New DataTable
        Try
            Dim DA As New SqlDataAdapter(sql, ConnStr)
            DA.Fill(DT)
        Catch ex As Exception
            DT = New DataTable
        End Try

        Return DT
    End Function

    Public Function Execute_DataTable(sql As String, cmdParms() As SqlParameter) As DataTable
        Dim DT As New DataTable
        Try
            Dim DA As New SqlDataAdapter(sql, ConnStr)
            If cmdParms IsNot Nothing Then
                For Each parm As SqlParameter In cmdParms
                    Try
                        If parm IsNot Nothing Then
                            DA.SelectCommand.Parameters.Add(parm)
                        End If
                    Catch ex As ArgumentNullException
                        'Throw New ApplicationException(ErrorNullParameter, ex)
                    Catch ex As ArgumentException
                        'Throw New ApplicationException(ErrorDuplicateParameter, ex)
                    End Try
                Next
            End If
            DA.Fill(DT)
            DA.Dispose()
        Catch ex As Exception
            DT = New DataTable
        End Try

        Return DT
    End Function

    Public Function Execute_Command(ByVal Command As String, cmdParms() As SqlParameter) As String
        Dim ret As String = "False"
        Try
            Dim Conn As New SqlConnection(ConnStr)
            Dim Comm As New SqlCommand
            Conn.Open()
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = Command

                If cmdParms IsNot Nothing Then
                    For Each parm As SqlParameter In cmdParms
                        Try
                            If parm IsNot Nothing Then
                                .Parameters.Add(parm)
                            End If
                        Catch ex As ArgumentNullException
                            'Throw New ApplicationException(ErrorNullParameter, ex)
                        Catch ex As ArgumentException
                            'Throw New ApplicationException(ErrorDuplicateParameter, ex)
                        End Try
                    Next
                End If

                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()

            ret = "True"
        Catch ex As Exception
            ret = "False|Exception " & ex.Message
        End Try
        Return ret
    End Function

    Public Function Execute_Command(ByVal Command As String) As String
        Dim ret As String = "False"
        Try
            Dim Conn As New SqlConnection(ConnStr)
            Dim Comm As New SqlCommand
            Conn.Open()
            With Comm
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = Command
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
            ret = "True"
        Catch ex As Exception
            ret = "False|Exception " & ex.Message
        End Try
        Return ret
    End Function
#End Region


#Region "Turnaround"

    Public Class ST_TA_DrawingDetail
        Public File_ID As Integer = 0
        Public File_Type As String = ""
        Public File_Data As Byte() = {}
    End Class

    Public Enum ST_TAG_TYPE
        Drum = 2
        General_Stationary = 3
        Absorber = 4
        Column = 5
        Filter = 6
        Heat_Exchnager = 7
        Strainer = 8

    End Enum

    Public Function TAG_TYPE_For_TA() As String
        Dim ST_TA_TAG_TYPE As String = ST_TAG_TYPE.Drum & "," & ST_TAG_TYPE.Absorber & "," & ST_TAG_TYPE.Column & "," & ST_TAG_TYPE.Filter & "," & ST_TAG_TYPE.Heat_Exchnager & "," & ST_TAG_TYPE.Strainer & "," & ST_TAG_TYPE.General_Stationary

        Return ST_TA_TAG_TYPE
    End Function

    Public Sub BindDDlST_TA_TagType(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = ""
        Dim ST_TA_TAG_TYPE As String = ST_TAG_TYPE.Drum & ", " & ST_TAG_TYPE.Absorber & ", " & ST_TAG_TYPE.Column & ", " & ST_TAG_TYPE.Filter & ", " & ST_TAG_TYPE.Heat_Exchnager & ", " & ST_TAG_TYPE.Strainer
        SQL = "SELECT * FROM MS_ST_TAG_TYPE WHERE active_status=1 And TAG_TYPE_ID IN (" & ST_TA_TAG_TYPE & ") ORDER BY TAG_TYPE_Name"


        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Equipement-Type...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TAG_TYPE_Name"), DT.Rows(i).Item("TAG_TYPE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub


    Public Function Get_TA_Default_Inspection() As DataTable

        Dim SQL As String = ""
        SQL &= " Select 'A' Group_Order, INSP_ID,INSP_Name,REF_INSP_ID,REF_STATUS_ID," & vbNewLine
        SQL &= " INSP_Order , 'MS_ST_Default_Inspection' To_Table  FROM MS_ST_Default_Inspection  WHERE INSP_ID NOT IN (9)" & vbNewLine
        SQL &= " UNION" & vbNewLine
        SQL &= " SELECT 'B' Group_Order, INSP_ID,INSP_Name,REF_INSP_ID,REF_STATUS_ID," & vbNewLine
        SQL &= " INSP_Order , 'MS_ST_TA_Default_Inspection' To_Table FROM MS_ST_TA_Default_Inspection " & vbNewLine
        SQL &= " UNION" & vbNewLine
        SQL &= " SELECT 'C'  Group_Order, INSP_ID,INSP_Name,REF_INSP_ID,REF_STATUS_ID," & vbNewLine
        SQL &= " INSP_Order , 'MS_ST_Default_Inspection' To_Table FROM MS_ST_Default_Inspection  WHERE INSP_ID IN (9)" & vbNewLine
        SQL &= " ORDER BY Group_Order,INSP_Order" & vbNewLine


        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_TA_Default_Inspection_Status() As DataTable

        Dim SQL As String = ""
        SQL &= " Select 'A' Group_Order, * , 'MS_ST_Default_Inspection_Status' To_Table " & vbNewLine
        SQL &= " From MS_ST_Default_Inspection_Status" & vbNewLine
        SQL &= " Where Status_ID Not In (7)" & vbNewLine
        SQL &= " UNION ALL" & vbNewLine
        SQL &= " Select 'B' Group_Order, * , 'MS_ST_TA_Default_Inspection_Status' To_Table " & vbNewLine
        SQL &= " From MS_ST_TA_Default_Inspection_Status" & vbNewLine
        SQL &= " UNION ALL" & vbNewLine
        SQL &= " Select 'C' Group_Order, * , 'MS_ST_Default_Inspection_Status' To_Table " & vbNewLine
        SQL &= " From MS_ST_Default_Inspection_Status" & vbNewLine
        SQL &= " Where Status_ID In (7)" & vbNewLine
        SQL &= " ORDER BY Group_Order, STATUS_Order" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ' list inspect สำหรับ แต่ละประเภทอุปกรณ์
    Public Sub BindDDl_Type_INSP_Name(ByRef ddl As DropDownList, Optional ByVal TAG_TYPE_ID As Integer = 0, Optional ByVal SelectedValue As Integer = -1)
        Dim DT As DataTable = Get_Type_INSP_Name(TAG_TYPE_ID)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Parts / Components...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("INSP_Name"), DT.Rows(i).Item("Insp_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub


    '---Datatable inspect สำหรับ แต่ละประเภทอุปกรณ์
    Public Function Get_Type_INSP_Name(ByVal TypeID As Integer) As DataTable
        Dim SQL As String = ""
        SQL &= "      SELECT DISTINCT TAG_TYPE_ID,Insp_ID,To_Table,INSP_Name FROM (  Select MS_ST_TAG_Inspection.* , 'MS_ST_TAG_Inspection' To_Table ,MS_ST_Default_Inspection.INSP_Name      " & vbNewLine
        SQL &= "           	    FROM MS_ST_TAG_Inspection                                                                                        											  " & vbNewLine
        SQL &= "           	    LEFT JOIN MS_ST_Default_Inspection On MS_ST_Default_Inspection.INSP_ID = MS_ST_TAG_Inspection.INSP_ID            											  " & vbNewLine
        SQL &= "           	    WHERE Tag_Type_ID =" & TypeID & "																						  												  " & vbNewLine
        'SQL &= "           	    UNION ALL                                                                                                        											  " & vbNewLine
        'SQL &= "                  Select  MS_ST_TA_TAG_Inspection.* ,'MS_ST_TA_TAG_Inspection' To_Table ,MS_ST_TA_Default_Inspection.INSP_Name 												  " & vbNewLine
        'SQL &= "           	    FROM MS_ST_TA_TAG_Inspection                                                                                     											  " & vbNewLine
        'SQL &= "           	    LEFT JOIN MS_ST_TA_Default_Inspection On MS_ST_TA_Default_Inspection.INSP_ID = MS_ST_TA_TAG_Inspection.INSP_ID   											  " & vbNewLine
        'SQL &= "           	    WHERE Tag_Type_ID =" & TypeID & "																					  													  " & vbNewLine
        SQL &= "       ) AS TB      																																						  " & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    '---Datatable ST_TA_MS_TagType
    Public Function Get_TA_TagType_Inspection(ByVal TypeID As Integer) As DataTable
        Dim SQL As String = "SELECT * , 'MS_ST_TAG_Inspection' To_Table " & vbNewLine
        SQL &= " FROM MS_ST_TAG_Inspection " & vbNewLine
        SQL &= " WHERE Tag_Type_ID =" & TypeID & vbNewLine
        'SQL &= " UNION ALL" & vbNewLine
        'SQL &= " SELECT * ,'MS_ST_TA_TAG_Inspection' To_Table " & vbNewLine
        'SQL &= " FROM MS_ST_TA_TAG_Inspection " & vbNewLine
        'SQL &= " WHERE Tag_Type_ID =" & TypeID & vbNewLine
        SQL &= " ORDER BY INSP_ID,STATUS_ID" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    '----Datatable หา Insp. ขอลแต่ละ Equipment 
    Public Function Get_TA_Inspection_For_Equipment(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal TypeID As Integer, ByVal DETAIL_ID As Integer, ByVal To_Table As String) As DataTable
        Dim SQL As String = "" & vbNewLine


        SQL &= "     			Select DISTINCT Tag_Type_ID,INSP.INSP_ID,To_Table,INSP.INSP_Name, Detail.STATUS_ID															" & vbNewLine
        SQL &= "     			,Detail.Group1_Task_Name	,Detail.RPT_Absorber_Step_ID																								" & vbNewLine
        SQL &= "     			,Detail.RPT_Period_Start,Detail.RPT_Period_End,Detail.Created_Time															" & vbNewLine
        SQL &= "     			,Detail.Group1_Component,Detail.Group2_Component,Detail.Group3_Component,Detail.Group4_Component							" & vbNewLine
        SQL &= "     			,Detail.DETAIL_ID	,Detail.RPT_DATE,Detail.TIME_Start,	Detail.TIME_End	,case when Detail.RPT_Status is null or Detail.RPT_Status=0	then 'Inspecting' ELSE 'Finished'	 END  STATUS_NAME																											" & vbNewLine
        SQL &= "     			FROM (                                                      																" & vbNewLine
        SQL &= "              	    Select MS_ST_TAG_Inspection.* , 'MS_ST_TAG_Inspection' To_Table ,MS_ST_Default_Inspection.INSP_Name              		" & vbNewLine
        SQL &= "              	    FROM MS_ST_TAG_Inspection                                                                                        		" & vbNewLine
        SQL &= "              	    LEFT JOIN MS_ST_Default_Inspection On MS_ST_Default_Inspection.INSP_ID = MS_ST_TAG_Inspection.INSP_ID            		" & vbNewLine
        SQL &= "              	    WHERE Tag_Type_ID =" & TypeID & "																									" & vbNewLine
        'SQL &= "              	    UNION ALL                                                                                                        		" & vbNewLine
        'SQL &= "                    Select  MS_ST_TA_TAG_Inspection.* ,'MS_ST_TA_TAG_Inspection' To_Table ,MS_ST_TA_Default_Inspection.INSP_Name 			" & vbNewLine
        'SQL &= "              	    FROM MS_ST_TA_TAG_Inspection                                                                                     		" & vbNewLine
        'SQL &= "              	    LEFT JOIN MS_ST_TA_Default_Inspection On MS_ST_TA_Default_Inspection.INSP_ID = MS_ST_TA_TAG_Inspection.INSP_ID   		" & vbNewLine
        'SQL &= "              	    WHERE Tag_Type_ID =" & TypeID & "																									" & vbNewLine
        SQL &= "                  ) AS INSP   																												" & vbNewLine
        SQL &= "     																																		" & vbNewLine
        SQL &= "     			 INNER JOIN  " & To_Table & "  AS Detail  ON  INSP.INSP_ID = detail.INSP_ID  											" & vbNewLine
        SQL &= "     						AND  Detail.RPT_Year =" & RPT_Year & " AND Detail.RPT_No =" & RPT_No & "  AND  Detail .DETAIL_ID =" & DETAIL_ID & "									" & vbNewLine
        SQL &= "     ORDER BY INSP_ID, RPT_DATE desc" & vbNewLine



        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    '----Datatable หา Insp. ขอลแต่ละ Equipment 
    'Public Function Get_ST_TA_Inspection_For_Equipment(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal TypeID As Integer, ByVal DETAIL_ID As Integer, ByVal To_Table As String) As DataTable
    Public Function Get_ST_TA_Inspection_For_Equipment(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, Optional DETAIL_STEP_ID As Integer = 0) As DataTable

        Dim SQL As String = "" & vbNewLine


        SQL &= "          			Select DISTINCT																																									" & vbNewLine
        SQL &= "     				Detail_Step.DETAIL_ID																																							" & vbNewLine
        SQL &= "     				,Detail_Step.DETAIL_STEP_ID																																						" & vbNewLine
        SQL &= "     				,Detail_Step.TAG_ID																																								" & vbNewLine
        SQL &= "     				,Detail_Step.INSP_ID																																							" & vbNewLine
        SQL &= "     				,INSP.INSP_Name																																									" & vbNewLine
        SQL &= "     				,Detail_Step.INSP_STATUS_ID																																						" & vbNewLine
        SQL &= "     				,INSP.STATUS_Name																																								" & vbNewLine
        SQL &= "     				,Detail_Step.RPT_DATE																																							" & vbNewLine
        SQL &= "     				,Detail.Tag_Type_ID																																								" & vbNewLine
        SQL &= "     				,MS_ST_TAG_TYPE.TAG_TYPE_Name																																					" & vbNewLine
        SQL &= "     				,MS_ST_TA_Step.STEP_ID		,Detail_Step.FIXED																																						" & vbNewLine
        SQL &= "     				,MS_ST_TA_Step.Step_Name 	, Detail_Step.STATUS_ID																																					" & vbNewLine
        SQL &= "     				,VW_ST_TA_TAG.TAG_CODE ,VW_ST_TA_TAG.TAG_Name																																					" & vbNewLine
        SQL &= "          			,Detail_Step.Condition_Problem																																					" & vbNewLine
        SQL &= "          			,Detail_Step.Created_Time																																						" & vbNewLine
        SQL &= "     				,Detail_Step.TIME_Start,	Detail_Step.TIME_End																																" & vbNewLine
        SQL &= "     				,Detail_Step.Employees,Detail_Step.STATUS_ID,Detail_Step.Condition_Problem,Detail_Step.Possible_Cause,Detail_Step.Recommendation		" & vbNewLine

        SQL &= "     				,case when Detail_Step.RPT_Status is null or Detail_Step.RPT_Status=0	then 'Inspecting' ELSE 'Finished'	 END RPT_Status  --Progress_NAME									" & vbNewLine
        SQL &= "          			FROM																																											" & vbNewLine
        SQL &= "     				RPT_ST_TA_Detail_Step  AS Detail_Step  																																			" & vbNewLine
        SQL &= "     				LEFT JOIN 																																										" & vbNewLine
        SQL &= "     				 (                                                      																														" & vbNewLine
        SQL &= "                   	    Select MS_ST_TAG_Inspection.* , 'MS_ST_TAG_Inspection' To_Table ,MS_ST_Default_Inspection.INSP_Name       ,	MS_ST_Default_Inspection_Status.STATUS_Name       				" & vbNewLine
        SQL &= "                   	    FROM MS_ST_TAG_Inspection                                                                                        															" & vbNewLine
        SQL &= "                   	    LEFT JOIN MS_ST_Default_Inspection On MS_ST_Default_Inspection.INSP_ID = MS_ST_TAG_Inspection.INSP_ID            															" & vbNewLine
        SQL &= "                   	    LEFT JOIN MS_ST_Default_Inspection_Status On MS_ST_Default_Inspection_Status.STATUS_ID = MS_ST_TAG_Inspection.STATUS_ID          											" & vbNewLine
        SQL &= "                   	   																																												" & vbNewLine
        'SQL &= "     				    UNION ALL                                                                                                        															" & vbNewLine
        'SQL &= "                         Select  MS_ST_TA_TAG_Inspection.* ,'MS_ST_TA_TAG_Inspection' To_Table ,MS_ST_TA_Default_Inspection.INSP_Name ,	MS_ST_TA_Default_Inspection_Status.STATUS_Name				" & vbNewLine
        'SQL &= "                   	    FROM MS_ST_TA_TAG_Inspection                                                                                     															" & vbNewLine
        'SQL &= "                   	    LEFT JOIN MS_ST_TA_Default_Inspection On MS_ST_TA_Default_Inspection.INSP_ID = MS_ST_TA_TAG_Inspection.INSP_ID   															" & vbNewLine
        'SQL &= "                   	    LEFT JOIN MS_ST_TA_Default_Inspection_Status On MS_ST_TA_Default_Inspection_Status.STATUS_ID = MS_ST_TA_TAG_Inspection.STATUS_ID          									" & vbNewLine
        SQL &= "                       ) AS INSP  																																									" & vbNewLine
        SQL &= "          			 ON  Detail_Step.INSP_ID = INSP.INSP_ID AND  Detail_Step.INSP_STATUS_ID = INSP.STATUS_ID 																						" & vbNewLine
        SQL &= "     				 LEFT JOIN RPT_ST_TA_DETAIL Detail ON  Detail.DETAIL_ID = Detail_Step.DETAIL_ID																									" & vbNewLine
        SQL &= "     				 LEFT JOIN MS_ST_TA_Step ON  MS_ST_TA_Step.STEP_ID = Detail_Step.STEP_ID																										" & vbNewLine
        SQL &= "     				 LEFT JOIN MS_ST_TAG_TYPE ON  MS_ST_TAG_TYPE.TAG_TYPE_ID = Detail.TAG_TYPE_ID																									" & vbNewLine
        SQL &= "     				 LEFT JOIN VW_ST_TA_TAG On VW_ST_TA_TAG.TAG_ID=Detail.TAG_ID And VW_ST_TA_TAG.TAG_TYPE_ID=Detail.TAG_TYPE_ID																									" & vbNewLine

        SQL &= "     	WHERE 	Detail_Step.RPT_Year =" & RPT_Year & " AND Detail_Step.RPT_No =" & RPT_No & " 																																" & vbNewLine

        If (DETAIL_STEP_ID > 0) Then
            SQL &= "        AND Detail_Step	.DETAIL_STEP_ID =" & DETAIL_STEP_ID & "																																							" & vbNewLine
        End If

        SQL &= "          ORDER BY   VW_ST_TA_TAG.TAG_CODE,  MS_ST_TA_Step.STEP_ID,INSP.INSP_Name,INSP.STATUS_Name, RPT_DATE desc , INSP_ID																																							" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function




    Public Function Check_Previous_Incomplete_ST_TA_Report(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, Optional ByVal TAG_ID As Integer = 0) As DataTable
        Dim SQL As String = "EXEC dbo.SP_Get_Previous_Incompleted_ST_TA_Report " & RPT_Year & "," & RPT_No & ","
        If TAG_ID <> 0 Then
            SQL &= TAG_ID
        Else
            SQL &= "NULL"
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Function Get_ST_TA_As_Found_Image(ByVal DETAIL_ID As Integer, ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal TAG_ID As Integer, ByVal INSP_ID As Integer, ByVal IMG_POS As Integer) As Byte()
        Dim SQL As String = "Select PIC_Detail" & IMG_POS & ",RPT_Year,RPT_No FROM RPT_Absorber_As_Found "
        SQL &= "    WHERE DETAIL_ID=" & DETAIL_ID & " And RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & "  And TAG_ID=" & TAG_ID & " And INSP_ID=" & INSP_ID & " "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) AndAlso DT.Rows(0).Item(0) Then
            Dim ImgPath As String = Picture_Path & "\" & "ST_TA" & "\" & DT.Rows(0).Item("RPT_Year") & "\" & DT.Rows(0).Item("RPT_No") & "\" & DETAIL_ID & "\" & INSP_ID & "_" & IMG_POS
            If File.Exists(ImgPath) Then
                Dim C As New Converter
                Dim F As FileStream = File.Open(ImgPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim S As Byte() = C.StreamToByte(F)
                F.Close()
                F.Dispose()
                Return S
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public Function Save_Picture_File_ST_TA(ByVal Pic_Detail As Byte(), ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DETAIL_ID As Integer, ByVal INSP_ID As Integer, ByVal IMG_POS As Integer, ByVal Optional RPT_DATE As String = "") As Boolean
        Dim Path As String = Picture_Path & "\" & "ST_TA"
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If
        Path &= "\" & RPT_Year
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If

        Path &= "\" & RPT_No
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If

        If (RPT_DATE <> "") Then
            Path &= "\" & DETAIL_ID & "_" & INSP_ID & "_" & IMG_POS & "_" & RPT_DATE
        Else
            Path &= "\" & DETAIL_ID & "_" & INSP_ID & "_" & IMG_POS

        End If
        Dim CountProcess As Integer = 0
        If File.Exists(Path) Then
            '-------------- Changed------------
            While File.Exists(Path)
                CountProcess += 1
                If CountProcess > 20 Then Return False
                Try
                    Kill(Path)
                Catch ex As Exception
                    Threading.Thread.Sleep(500)
                End Try
            End While
        End If
        If IsNothing(Pic_Detail) Then Return True

        CountProcess = 0
        Do While True
            Try
                CountProcess += 1
                If CountProcess > 20 Then Return False
                File.WriteAllBytes(Path, Pic_Detail)
                Return True
            Catch ex As Exception
                Threading.Thread.Sleep(500)
            End Try
        Loop
        Return False
    End Function


    Public Enum ST_TA_STEP
        As_Found = 1
        After_Clean = 2
        NDE = 3
        Repair = 4
        After_Repair = 5
        Final = 6
    End Enum

    Public Enum ST_TA_STATUS
        Open_Task = -1
        In_Progress = 0
        Finish = 1
    End Enum

    'Public Sub Drop_RPT_ST_TA(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal TAG_ID As Integer, ByVal DETAIL_STEP_ID As Integer, ByVal INSP_ID As Integer, ByVal To_Table As String, ByVal Optional RPT_DATE As String = "")
    '    Dim SQL As String = "Select * FROM " & To_Table & " " & vbNewLine
    '    SQL &= " WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " And TAG_ID=" & TAG_ID & " And DETAIL_STEP_ID=" & DETAIL_STEP_ID & " And INSP_ID=" & INSP_ID
    '    If (RPT_DATE <> "") Then
    '        SQL &= " And RPT_DATE='" & RPT_DATE & "'"
    '    Else
    '        SQL &= " And RPT_DATE Is null "
    '    End If

    '    Dim DA As New SqlDataAdapter(SQL, ConnStr)
    '    Dim DT As New DataTable
    '    DA.Fill(DT)





    '    'RPT_ST_TA_Header
    '    'RPT_ST_TA_Detail
    '    'RPT_ST_TA_Detail_Step
    '    'RPT_ST_TA_Detail_Sector
    '    'MS_Template_Sector








    '    For i As Integer = 0 To DT.Rows.Count - 1

    '        '-- Del  RPT_ST_TA_Detail_Sector --

    '        Drop_RPT_ST_TA_Picture(RPT_Year, RPT_No, TAG_ID, DETAIL_STEP_ID, INSP_ID, To_Table, RPT_DATE)

    '    Next
    '    SQL = " DELETE FROM " & To_Table & " " & vbNewLine
    '    SQL &= " WHERE  RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & " AND DETAIL_STEP_ID=" & DETAIL_STEP_ID & " AND INSP_ID=" & INSP_ID
    '    If (RPT_DATE <> "") Then
    '        SQL &= " AND RPT_DATE='" & RPT_DATE & "'"
    '    Else
    '        SQL &= " And RPT_DATE Is null "
    '    End If
    '    Execute_Command(SQL)
    'End Sub


    'RPT_ST_TA_Header
    'RPT_ST_TA_Detail
    'RPT_ST_TA_Detail_Step
    'RPT_ST_TA_Detail_Sector
    '==== 1 MS_Template_Sector====
    Public Sub Drop_RPT_ST_TA(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal TAG_ID As Integer, ByVal DETAIL_STEP_ID As Integer, ByVal INSP_ID As Integer, ByVal To_Table As String, ByVal Optional RPT_DATE As String = "")
        Dim SQL As String = "Select * FROM RPT_ST_TA_Detail_Sector " & vbNewLine
        SQL &= " WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Data As New DataTable
        DA.Fill(DT_Data)
        If DT_Data.Rows.Count > 0 Then
            For i As Integer = 0 To DT_Data.Rows.Count - 1
                '--ลบ path รูปภาพ--
                Drop_RPT_ST_TA_Picture(RPT_Year, RPT_No, DT_Data.Rows(i).Item("Sector_id"))
                '--ลบข้อมูลในตาราง sector--
                SQL = " DELETE FROM MS_Template_Sector " & vbNewLine
                SQL &= " WHERE  Sector_ID=" & DT_Data.Rows(i).Item("Sector_id") & vbNewLine
                Execute_Command(SQL)
            Next
        End If

    End Sub

    '==== 1 MS_Template_Sector====
    Public Sub Drop_MS_Template_Sector(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DETAIL_STEP_ID As Integer)
        Dim SQL As String = "Select * FROM RPT_ST_TA_Detail_Sector " & vbNewLine
        SQL &= " WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Data As New DataTable
        DA.Fill(DT_Data)
        If DT_Data.Rows.Count > 0 Then
            For i As Integer = 0 To DT_Data.Rows.Count - 1
                '--ลบ path รูปภาพ--
                Drop_RPT_ST_TA_Picture(RPT_Year, RPT_No, DT_Data.Rows(i).Item("Sector_id"))
                '--ลบข้อมูลในตาราง sector--
                SQL = " DELETE FROM MS_Template_Sector " & vbNewLine
                SQL &= " WHERE  Sector_ID=" & DT_Data.Rows(i).Item("Sector_id") & vbNewLine
                Execute_Command(SQL)
            Next
        End If

    End Sub

    '===2 ลบไฟล์รูปภาพ===
    Public Sub Drop_RPT_ST_TA_Picture(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal Sector_ID As Integer)
        Dim FileName As String = ""
        For j As Integer = 1 To 4
            FileName = Picture_Path & "\ST_TA_Template\" & RPT_Year & "\" & RPT_No & "\" & Sector_ID
            If j > 1 Then
                FileName &= "_" & j
            End If
            If File.Exists(FileName) Then
                Try
                    Kill(FileName)
                Catch
                End Try
            End If
        Next

    End Sub

    'RPT_ST_TA_Header
    'RPT_ST_TA_Detail 

    '=====3 RPT_ST_TA_Detail_Sector====
    Public Sub Drop_RPT_ST_TA_Detail_Sector(ByVal DETAIL_STEP_ID As Integer)
        Dim SQL As String = "Select * FROM RPT_ST_TA_Detail_Sector " & vbNewLine
        SQL &= " WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Data As New DataTable
        DA.Fill(DT_Data)
        If DT_Data.Rows.Count > 0 Then
            SQL = " DELETE FROM RPT_ST_TA_Detail_Sector " & vbNewLine
            SQL &= " WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
            Execute_Command(SQL)
        End If

    End Sub

    '=====4 RPT_ST_TA_Detail_Step====
    Public Sub Drop_RPT_ST_TA_Detail_Step(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal DETAIL_STEP_ID As Integer)

        Drop_MS_Template_Sector(RPT_Year, RPT_No, DETAIL_STEP_ID)
        Drop_RPT_ST_TA_Detail_Sector(DETAIL_STEP_ID)

        Dim SQL As String = "Select * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        SQL &= " WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Data As New DataTable
        DA.Fill(DT_Data)
        If DT_Data.Rows.Count > 0 Then
            SQL = " DELETE FROM RPT_ST_TA_Detail_Step " & vbNewLine
            SQL &= " WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
            Execute_Command(SQL)
        End If

    End Sub

    '===RPT_ST_TA_Detail===
    Public Sub Drop_RPT_ST_TA_Detail(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal Optional STEP_ID As Integer = 0)


        Dim SQL As String = "Select * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        If STEP_ID > 0 Then
            SQL &= " AND STEP_ID=" & STEP_ID
        End If

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Data As New DataTable
        DA.Fill(DT_Data)


        If DT_Data.Rows.Count > 0 Then

            For i As Integer = 0 To DT_Data.Rows.Count - 1
                Drop_RPT_ST_TA_Detail_Step(RPT_Year, RPT_No, DT_Data.Rows(i).Item("DETAIL_STEP_ID"))
            Next
        End If

        'If STEP_ID = 0 Then
        '    '---RPT_ST_TA_Detail---
        '    SQL = " DELETE FROM RPT_ST_TA_Detail " & vbNewLine
        '    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        '    Execute_Command(SQL)

        'End If

    End Sub

    '===RPT_ST_TA_Header===
    Public Sub Drop_RPT_ST_TA_Header(ByVal RPT_Year As Integer, ByVal RPT_No As Integer)

        Drop_RPT_ST_TA_Detail(RPT_Year, RPT_No)

        '---RPT_ST_TA_Detail---
        Dim SQL_Detail As String = "  DELETE FROM RPT_ST_TA_Detail " & vbNewLine
        SQL_Detail &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Execute_Command(SQL_Detail)


        '---RPT_ST_TA_Header---
        Dim SQL As String = " DELETE FROM RPT_ST_TA_Header " & vbNewLine
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Execute_Command(SQL)

    End Sub







    Public Sub Drop_RPT_ST_TA_Repare(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal TAG_ID As Integer, ByVal DETAIL_ID As Integer, ByVal INSP_ID As Integer, ByVal To_Table As String, ByVal Optional RPT_DATE As String = "")
        Dim SQL As String = "Select * FROM " & To_Table & " " & vbNewLine
        SQL &= " WHERE  RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No & " And TAG_ID=" & TAG_ID & " And DETAIL_ID=" & DETAIL_ID & " And INSP_ID=" & INSP_ID
        If (RPT_DATE <> "") Then
            SQL &= " And RPT_DATE Is null"
        End If

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        'For i As Integer = 0 To DT.Rows.Count - 1
        '    Drop_RPT_ST_TA_Picture(RPT_Year, RPT_No, TAG_ID, DETAIL_ID, INSP_ID, To_Table, RPT_DATE)
        'Next
        SQL = " DELETE FROM " & To_Table & " " & vbNewLine
        SQL &= " WHERE  RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & " AND DETAIL_ID=" & DETAIL_ID & " AND INSP_ID=" & INSP_ID

        SQL &= " AND RPT_DATE is null"

            Execute_Command(SQL)
    End Sub




    Public Function Get_ST_TA_Status_Color(ByVal ReportStatus As ST_TA_STATUS) As Color
        Select Case ReportStatus
            Case ST_TA_STATUS.Open_Task
                Return Color.Gray
            Case ST_TA_STATUS.In_Progress
                Return Color.SteelBlue
            Case ST_TA_STATUS.Finish
                Return Color.Green
        End Select
    End Function


    Public Sub Drop_ST_TA_Tag_Drawing(ByVal TAG_ID As Integer, ByVal To_Table As String)
        '-------- Delete Physical File ---------
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM " & To_Table & " WHERE TAG_ID=" & TAG_ID, ConnStr)
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_ST_TA_Tag_Drawing(TAG_ID, To_Table, DT.Rows(i).Item("File_ID"))
        Next
    End Sub

    Public Sub Drop_ST_TA_Tag_Drawing(ByVal TAG_ID As Integer, ByVal To_Table As String, ByVal File_ID As Integer)
        '-------- Delete Physical File ---------
        Dim To_Folder As String = ""
        Select Case To_Table
            Case "MS_ST_ABSORBER_Drawing"
                To_Folder = "Absorber"
            Case "MS_ST_DRUM_Drawing"
                To_Folder = "Drum"
            Case "MS_ST_FILTER_Drawing"
                To_Folder = "Filter"
            Case "MS_ST_COLUMN_Drawing"
                To_Folder = "Column"
            Case "MS_ST_HEAT_EXCHNAGER_Drawing"
                To_Folder = "Heat_Exchnager"

            Case ""
        End Select

        Dim Path As String = Picture_Path & "\" & "ST_TA" & "\Drawing\" & To_Folder & "\" & TAG_ID & "\" & File_ID
        If File.Exists(Path) Then
            Try
                File.Delete(Path)
            Catch : End Try
        End If
        Path = Picture_Path & "\" & "ST_TA" & "\Drawing\" & To_Folder & "\" & TAG_ID
        Dim F As String() = Directory.GetFiles(Path)
        If F.Count = 0 Then Directory.Delete(Path)
        '-------- Delete Database ---------
        Dim Comm As New SqlCommand()
        Dim Conn As New SqlConnection(ConnStr)
        Conn.Open()
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM " & To_Table & " WHERE TAG_ID=" & TAG_ID & " AND File_ID=" & File_ID
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    Public Sub BindDDlST_TA_Step(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim OldSelectedText As String = ddl.Text
        Dim SQL As String = "SELECT * FROM MS_ST_TA_Step Order by Step_Seq"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("Choose Step...", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Step_Name"), DT.Rows(i).Item("Step_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

    End Sub


    Function DT_Daily_Date(ByRef _RPT_Year As Integer, ByRef _RPT_No As Integer, ByRef _TAG_ID As Integer, ByRef _INSP_ID As Integer, ByRef _INSP_STATUS_ID As Integer) As DataTable
        Dim Sql As String = ""
        Sql &= "   DECLARE @RPT_Year As integer = " & _RPT_Year & "									" & vbNewLine
        Sql &= "   DECLARE @RPT_No As integer = " & _RPT_No & "										" & vbNewLine
        Sql &= "   DECLARE @TAG_ID As integer = " & _TAG_ID & "										" & vbNewLine
        Sql &= "   DECLARE @INSP_ID As integer = " & _INSP_ID & "										" & vbNewLine
        Sql &= "   DECLARE @INSP_STATUS_ID As integer = " & _INSP_STATUS_ID & "								" & vbNewLine
        Sql &= "   SELECT DISTINCT  													" & vbNewLine
        Sql &= "          STEP_ID														" & vbNewLine
        Sql &= "         ,RPT_DATE														" & vbNewLine
        Sql &= "         ,STATUS_ID														" & vbNewLine
        Sql &= "         ,MAX(STATUS_ID) MAX_CLASS										" & vbNewLine
        Sql &= "     FROM RPT_ST_TA_Detail_Step											" & vbNewLine
        Sql &= "     WHERE RPT_Year = @RPT_Year AND RPT_No = @RPT_No					" & vbNewLine
        Sql &= "     AND TAG_ID=@TAG_ID													" & vbNewLine
        Sql &= "     AND INSP_ID =@INSP_ID												" & vbNewLine
        Sql &= "     AND INSP_STATUS_ID= @INSP_STATUS_ID								" & vbNewLine
        Sql &= "     GROUP BY STEP_ID  ,RPT_DATE ,STATUS_ID								" & vbNewLine
        Sql &= "     ORDER BY RPT_DATE 	DESC						" & vbNewLine

        Dim DA As New SqlDataAdapter(Sql, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)


        Return DT
    End Function


#End Region


#Region "Template"

    Public Function GetNew_Table_ID(ByVal Table_Name As String, ByVal PrimaryID As String) As Integer
        Dim DA As New SqlDataAdapter("SELECT ISNULL(MAX(" & PrimaryID & "),0)+1 FROM " & Table_Name & "", ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

#Region "Stationary Routine And OffRoutine"

    Public Sub Drop_Stationary_Template_Sector(ByVal Sector_ID As Integer)
        '----------Drop File ---------
        Dim SQL As String = "SELECT UNIQUE_ID FROM RPT_ST_Template_File WHERE Sector_ID=" & Sector_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_Stationary_Template_File(DT.Rows(0).Item("UNIQUE_ID"))
        Next

        '--------- Drop Sector -------
        SQL = "DELETE FROM RPT_Template_Sector WHERE Sector_ID=" & Sector_ID
        Execute_Command(SQL)
    End Sub

    Public Sub Drop_Stationary_Template_Sector(ByVal RPT_Type_ID As Report_Type, ByVal DETAIL_ID As Integer)

        Dim SQL As String = "SELECT Sector_ID FROM RPT_Template_Sector WHERE RPT_Type_ID=" & RPT_Type_ID & " AND DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Drop_Stationary_Template_Sector(DT.Rows(0).Item("Sector_ID"))
        Next
    End Sub

    Public Function Get_Stationary_Template_File(ByVal UNIQUE_ID As String) As FileAttachment
        Dim DT As New DataTable
        Dim SQL As String = "Select * FROM RPT_ST_Template_File WHERE UNIQUE_ID='" & UNIQUE_ID.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Return Nothing

        '-------------- Check Path----------------
        Dim SVGPath As String = Picture_Path & "\Template\Stationary\SVG"
        If Not Directory.Exists(SVGPath) Then
            Directory.CreateDirectory(SVGPath)
        End If
        Dim ImagePath As String = Picture_Path & "\Template\Stationary\Image"
        If Not Directory.Exists(ImagePath) Then
            Directory.CreateDirectory(ImagePath)
        End If
        Dim DocPath As String = Picture_Path & "\Template\Stationary\Doc"
        If Not Directory.Exists(DocPath) Then
            Directory.CreateDirectory(DocPath)
        End If

        Dim C As New Converter
        Dim Attachment As New FileAttachment
        Dim DR As DataRow = DT.Rows(0)

        Dim SVG As New SVG_API
        With Attachment
            .UNIQUE_ID = DR("UNIQUE_ID").ToString
            .Title = DR("File_Title").ToString
            .Description = DR("File_Desc").ToString
            .Tag = DR("File_TAG").ToString
            .Extension = DR("EXT_ID")
            .DocType = DR("AFT_ID")

            '-------------- Get .Content
            Select Case .Extension
                Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG, FileAttachment.ExtensionType.PNG,
                             FileAttachment.ExtensionType.SVG, FileAttachment.ExtensionType.TIFF

                    Dim Path As String = SVGPath & "\" & DR("UNIQUE_ID").ToString
                    If File.Exists(Path) Then
                        .Content = C.StringToByte(SVG.GetSVGContent(Path), Converter.EncodeType._UTF8)
                    ElseIf File.Exists(ImagePath & "\" & DR("UNIQUE_ID").ToString) Then
                        Path = ImagePath & "\" & DR("UNIQUE_ID").ToString
                        Dim F As Stream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                        Dim IMG As Drawing.Image = Drawing.Image.FromStream(F)
                        .Content = C.StringToByte(SVG.ImageToSVG(IMG), Converter.EncodeType._UTF8)
                        F.Dispose()
                        IMG.Dispose()
                    End If

                Case FileAttachment.ExtensionType.DOC, FileAttachment.ExtensionType.PDF,
                     FileAttachment.ExtensionType.PPT, FileAttachment.ExtensionType.XLS

                    Dim Path As String = DocPath & "\" & DR("UNIQUE_ID").ToString
                    If Not File.Exists(Path) Then Return Nothing
                    Dim st As FileStream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                    .Content = C.StreamToByte(st)
                    st.Close()

            End Select

            .LastEditTime = DR("Update_Time")
            .LastEditBy = DR("Update_By")
        End With
        Return Attachment
    End Function

    Public Function Get_Stationary_Template_File(ByVal Sector_ID As Integer, ByVal File_ID As Integer) As FileAttachment
        Dim DT As New DataTable
        Dim SQL As String = "Select UNIQUE_ID FROM RPT_ST_Template_File WHERE Sector_ID=" & Sector_ID & " AND File_ID=" & File_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Return Nothing

        Return Get_Stationary_Template_File(DT.Rows(0).Item("UNIQUE_ID"))
    End Function

    Public Sub Save_Stationary_Template_File(ByVal Sector_ID As Integer, ByVal File_ID As Integer, ByVal Attachment As FileAttachment)
        Dim SQL As String = "SELECT * FROM RPT_ST_Template_File" & vbLf
        SQL &= " WHERE Sector_ID = " & Sector_ID & " And File_ID = " & File_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
            DR("Sector_ID") = Sector_ID
            DR("File_ID") = File_ID
            DR("UNIQUE_ID") = Attachment.UNIQUE_ID & "_" & File_ID
        Else
            DR = DT.Rows(0)
        End If
        DR("AFT_ID") = Attachment.DocType
        DR("File_Title") = Attachment.Title
        DR("File_Desc") = Attachment.Description
        DR("File_TAG") = Attachment.Tag
        '--------------- Get Dimension --------
        Dim C As New Converter
        Dim SVG As New SVG_API
        Dim IMG As Drawing.Image = Nothing
        Dim SVGContent As Byte() = {}
        Dim DocContent As Byte() = {}
        Select Case Attachment.Extension
            Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG,
                 FileAttachment.ExtensionType.PNG, FileAttachment.ExtensionType.TIFF
                IMG = Drawing.Image.FromStream(C.ByteToStream(Attachment.Content)) '---------------- Prepare Image Content---------------
                SVGContent = C.StringToByte(SVG.ImageToSVG(IMG), Converter.EncodeType._UTF8) '---------------- Prepare SVG Content----------------------------
                DR("File_Width") = IMG.Width
                DR("File_Height") = IMG.Height

            Case FileAttachment.ExtensionType.SVG
                IMG = SVG.SVGToImage(C.ByteToString(Attachment.Content, Converter.EncodeType._UTF8)) '---------------- Prepare Image Content---------------
                SVGContent = Attachment.Content '---------------- Prepare SVG Content------------------------------------------
                DR("File_Width") = IMG.Width
                DR("File_Height") = IMG.Height

            Case FileAttachment.ExtensionType.DOC, FileAttachment.ExtensionType.PDF,
                  FileAttachment.ExtensionType.PPT, FileAttachment.ExtensionType.XLS
                DocContent = Attachment.Content
            Case Else
                'Exit Sub
        End Select

        DR("EXT_ID") = Attachment.Extension
        DR("Update_By") = Attachment.LastEditBy
        DR("Update_Time") = Attachment.LastEditTime

        '------------------ Save Physical File--------------
        Dim SVGPath As String = Picture_Path & "\Template\Stationary\SVG\"
        Dim ImagePath As String = Picture_Path & "\Template\Stationary\Image\"
        Dim DocPath As String = Picture_Path & "\Template\Stationary\Doc\"
        If Not Directory.Exists(SVGPath) Then Directory.CreateDirectory(SVGPath)
        If Not Directory.Exists(ImagePath) Then Directory.CreateDirectory(ImagePath)
        If Not Directory.Exists(DocPath) Then Directory.CreateDirectory(DocPath)

        SVGPath &= Attachment.UNIQUE_ID & "_" & File_ID
        ImagePath &= Attachment.UNIQUE_ID & "_" & File_ID
        DocPath &= Attachment.UNIQUE_ID & "_" & File_ID
        '---------------- Delete Destination -------------
        If File.Exists(SVGPath) Then
            Try
                DeleteFile(SVGPath)
            Catch : End Try
        End If
        If File.Exists(ImagePath) Then
            Try
                DeleteFile(ImagePath)
            Catch : End Try
        End If
        If File.Exists(DocPath) Then
            Try
                DeleteFile(DocPath)
            Catch : End Try
        End If
        '---------------- Save SVG -------------------------
        Select Case Attachment.Extension
            Case FileAttachment.ExtensionType.GIF, FileAttachment.ExtensionType.JPEG,
            FileAttachment.ExtensionType.PNG, FileAttachment.ExtensionType.TIFF, FileAttachment.ExtensionType.SVG

                Try
                    IMG.Save(ImagePath)
                    Dim F As FileStream = File.Open(SVGPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
                    F.Write(SVGContent, 0, SVGContent.Length)
                    F.Close()
                    F.Dispose()
                Catch ex As Exception
                    'Exit Sub
                End Try

            Case FileAttachment.ExtensionType.DOC, FileAttachment.ExtensionType.PDF,
                 FileAttachment.ExtensionType.PPT, FileAttachment.ExtensionType.XLS

                Try
                    Dim st As FileStream = File.Open(DocPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
                    st.Write(DocContent, 0, DocContent.Length)
                    st.Close()
                    st.Dispose()
                Catch ex As Exception
                    'Exit Sub
                End Try

        End Select
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
    End Sub

    Public Sub Drop_Stationary_Template_File(ByVal Sector_ID As Integer, ByVal File_ID As Integer)
        Dim DT As New DataTable
        Dim SQL As String = "Select UNIQUE_ID FROM RPT_ST_Template_File WHERE Sector_ID=" & Sector_ID & " AND File_ID=" & File_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        Drop_Stationary_Template_File(DT.Rows(0).Item("UNIQUE_ID"))
    End Sub

    Public Sub Drop_Stationary_Template_File(ByVal UNIQUE_ID As String)
        Dim SQL As String = "DELETE FROM RPT_ST_Template_File WHERE UNIQUE_ID='" & UNIQUE_ID.Replace("'", "''") & "'"
        Execute_Command(SQL)

        '-------- Delete File -------------
        Dim SVGPath As String = Picture_Path & "\Template\Stationary\SVG\" & UNIQUE_ID
        Dim ImagePath As String = Picture_Path & "\Template\Stationary\Image\" & UNIQUE_ID
        Dim DocPath As String = Picture_Path & "\Template\Stationary\Doc\" & UNIQUE_ID
        Try
            If File.Exists(SVGPath) Then DeleteFile(SVGPath)
        Catch : End Try
        Try
            If File.Exists(ImagePath) Then DeleteFile(ImagePath)
        Catch : End Try
        Try
            If File.Exists(DocPath) Then DeleteFile(DocPath)
        Catch : End Try

    End Sub

#End Region

    Public Function Save_Picture_File_ST_TA_Template(ByVal Pic_Detail As Byte(), ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal Sector_ID As Integer, Optional ByVal ImageNo As Integer = Nothing) As Boolean
        Dim Path As String = Picture_Path & "\" & "ST_TA_Template"
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If
        Path &= "\" & RPT_Year
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If

        Path &= "\" & RPT_No
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If

        If Not IsDBNull(Sector_ID) Then
            Path &= "\" & Sector_ID
        Else

        End If

        If Not IsDBNull(ImageNo) Then
            If (ImageNo > 0) Then
                Path &= "_" & ImageNo
            End If
        End If

        Dim CountProcess As Integer = 0
        If File.Exists(Path) Then
            '-------------- Changed------------
            While File.Exists(Path)
                CountProcess += 1
                If CountProcess > 20 Then Return False
                Try
                    Kill(Path)
                Catch ex As Exception
                    Threading.Thread.Sleep(500)
                End Try
            End While
        End If
        If IsNothing(Pic_Detail) Then Return True

        CountProcess = 0
        Do While True
            Try
                CountProcess += 1
                If CountProcess > 20 Then Return False
                File.WriteAllBytes(Path, Pic_Detail)
                Return True
            Catch ex As Exception
                Threading.Thread.Sleep(500)
            End Try
        Loop
        Return False
    End Function

    Public Function Get_ST_TA_As_Found_Image_Template(ByVal RPT_Year As Integer, ByVal RPT_No As Integer, ByVal Sector_ID As Integer, ByVal IMG_POS As Integer) As Byte()
        Dim SQL As String = "Select PIC_Detail" & IMG_POS & ",Sector_ID FROM MS_Template_Sector "
        SQL &= "    WHERE Sector_ID=" & Sector_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item(0)) AndAlso DT.Rows(0).Item(0) Then
            Dim ImgPath As String = Picture_Path & "\" & "ST_TA_Template" & "\" & RPT_Year & "\" & RPT_No & "\" & Sector_ID
            If (Convert.ToInt16(IMG_POS) > 1) Then
                ImgPath += "_" & IMG_POS
            End If

            If File.Exists(ImgPath) Then
                Dim C As New Converter
                Dim F As FileStream = File.Open(ImgPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim S As Byte() = C.StreamToByte(F)
                F.Close()
                F.Dispose()
                Return S
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function



#End Region

    '-----CUI----


    Public Sub BindDDl_CorrosionAllowance(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM MS_ST_CA ORDER BY CA_ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("...", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As ListItem = Nothing
            If DT.Rows(i).Item("CA_ID") = 1 Then
                Item = New ListItem(DT.Rows(i).Item("CA_Code") & " (none)", DT.Rows(i).Item("CA_ID"))
            Else
                Item = New ListItem(DT.Rows(i).Item("CA_Code") & " (" & CDbl(DT.Rows(i).Item("CA_DEPTH_MM")) & " mm)", DT.Rows(i).Item("CA_ID"))
            End If
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub



End Class

