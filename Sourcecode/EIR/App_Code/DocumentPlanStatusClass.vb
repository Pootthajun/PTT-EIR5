﻿Public Class DocumentPlanStatusClass
    Dim _PlanStatus As String
    Dim _LastStatus As String
    Dim _NoticeDate As New DateTime(1, 1, 1)
    Dim _CriticalDate As New DateTime(1, 1, 1)
    Dim _UploadDate As New DateTime(1, 1, 1)
    Dim _PercentComplete As Integer = 0

    Public Property LastStatus As String
        Get
            Return _LastStatus
        End Get
        Set(value As String)
            _LastStatus = value
        End Set
    End Property

    Public Property PlanStatus As String
        Get
            Return _PlanStatus
        End Get
        Set(value As String)
            _PlanStatus = value
        End Set
    End Property

    Public Property NoticeDate As DateTime
        Get
            Return _NoticeDate
        End Get
        Set(value As DateTime)
            _NoticeDate = value
        End Set
    End Property

    Public Property CriticalDate As DateTime
        Get
            Return _CriticalDate
        End Get
        Set(value As DateTime)
            _CriticalDate = value
        End Set
    End Property

    Public Property UploadDate As DateTime
        Get
            Return _UploadDate
        End Get
        Set(value As DateTime)
            _UploadDate = value
        End Set
    End Property

    Public Property PercentComplete As Integer
        Get
            Return _PercentComplete
        End Get
        Set(value As Integer)
            _PercentComplete = value
        End Set
    End Property
End Class