﻿Public Class ReportPermission

    Public RPT_STEP As EIR_BL.Report_Step
    Public USER_LEVEL As EIR_BL.User_Level
    Public USER_ID As Integer = -1
    Public RPT_LOCK_BY As Integer = -1

    Public ReadOnly Property CanEdit() As Boolean
        Get
            Select Case RPT_STEP
                Case EIR_BL.Report_Step.New_Step
                    Return USER_LEVEL = EIR_BL.User_Level.Collector _
                                      Or USER_LEVEL = EIR_BL.User_Level.Administrator
                Case EIR_BL.Report_Step.Collecting_Step
                    Return USER_LEVEL = EIR_BL.User_Level.Administrator _
                                      Or (
                                           USER_LEVEL = EIR_BL.User_Level.Collector And (RPT_LOCK_BY = USER_ID Or RPT_LOCK_BY = -1)
                                         )
                Case EIR_BL.Report_Step.Inspecting_Step
                    Return USER_LEVEL = EIR_BL.User_Level.Administrator _
                                      Or (
                                           USER_LEVEL = EIR_BL.User_Level.Inspector And (RPT_LOCK_BY = USER_ID Or RPT_LOCK_BY = -1)
                                         )
                Case EIR_BL.Report_Step.Approving_Step
                    Return USER_LEVEL = EIR_BL.User_Level.Administrator _
                                     Or (
                                           USER_LEVEL = EIR_BL.User_Level.Approver And (RPT_LOCK_BY = USER_ID Or RPT_LOCK_BY = -1)
                                         )
                Case EIR_BL.Report_Step.Approved_Step
                    Return USER_LEVEL = EIR_BL.User_Level.Administrator
            End Select
            Return EIR_BL.Report_Step.New_Step
        End Get
    End Property

    Public ReadOnly Property CanPreview() As Boolean
        Get
            Return RPT_STEP >= EIR_BL.Report_Step.Collecting_Step
        End Get
    End Property

End Class

Public Class Report_CUI_ERO_Permission
    Public USER_LEVEL As EIR_BL.User_Level
    Public USER_ID As Integer = -1
    Public RPT_LOCK_BY As Integer = -1
    Public IsFinished As Boolean = False

    Public ReadOnly Property CanEdit() As Boolean
        Get
            If IsFinished Then
                Return (USER_LEVEL = EIR_BL.User_Level.Administrator) Or (RPT_LOCK_BY = USER_ID)
            Else
                Return USER_LEVEL <> EIR_BL.User_Level.PTT_Authenticated And USER_LEVEL <> EIR_BL.User_Level.Viewer
            End If
        End Get
    End Property

End Class
