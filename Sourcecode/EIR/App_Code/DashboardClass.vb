﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Public Class DashboardClass

    Dim BL As New EIR_BL

    Public Sub BindDDlEquipment(ByRef ddl As DropDownList, ByVal Equipment As EIR_BL.Report_Type)
        Dim Item As New ListItem()
        Item = New ListItem("Stationary", EIR_BL.Report_Type.Stationary_Routine_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("Rotating", EIR_BL.Report_Type.Rotating_Routine_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("Lube Oil", EIR_BL.Report_Type.Lube_Oil_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("PdMA & MTap", EIR_BL.Report_Type.PdMA_MTap_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("Thermography", EIR_BL.Report_Type.Thermography_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("All Equipment", 99)
        ddl.Items.Add(Item)
        ddl.SelectedValue = Equipment
    End Sub

    Public Sub BindDDlEquipmentAnnualProgess(ByRef ddl As DropDownList, ByVal Equipment As EIR_BL.Report_Type)
        Dim Item As New ListItem()
        Item = New ListItem("Stationary", EIR_BL.Report_Type.Stationary_Routine_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("Rotating", EIR_BL.Report_Type.Rotating_Routine_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("Lube Oil", EIR_BL.Report_Type.Lube_Oil_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("PdMA", EIR_BL.Report_Type.PdMA_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("MTap", EIR_BL.Report_Type.MTap_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("Thermography", EIR_BL.Report_Type.Thermography_Report)
        ddl.Items.Add(Item)
        Item = New ListItem("All Equipment", 99)
        ddl.Items.Add(Item)
        ddl.SelectedValue = Equipment
    End Sub

    Public Sub BindDDlMonthThai(ByRef ddl As DropDownList, ByVal SelectedMonth As Integer)
        Dim Item As New ListItem()
        Item = New ListItem("มกราคม", 1)
        ddl.Items.Add(Item)
        Item = New ListItem("กุมภาพันธ์", 2)
        ddl.Items.Add(Item)
        Item = New ListItem("มีนาคม", 3)
        ddl.Items.Add(Item)
        Item = New ListItem("เมษายน", 4)
        ddl.Items.Add(Item)
        Item = New ListItem("พฤษภาคม", 5)
        ddl.Items.Add(Item)
        Item = New ListItem("มิถุนายน", 6)
        ddl.Items.Add(Item)
        Item = New ListItem("กรกฎาคม", 7)
        ddl.Items.Add(Item)
        Item = New ListItem("สิงหาคม", 8)
        ddl.Items.Add(Item)
        Item = New ListItem("กันยายน", 9)
        ddl.Items.Add(Item)
        Item = New ListItem("ตุลาคม", 10)
        ddl.Items.Add(Item)
        Item = New ListItem("พฤศจิกายน", 11)
        ddl.Items.Add(Item)
        Item = New ListItem("ธันวาคม", 12)
        ddl.Items.Add(Item)
        ddl.SelectedValue = SelectedMonth
    End Sub

    Public Sub BindDDlMonthEng(ByRef ddl As DropDownList, ByVal SelectedMonth As Integer)
        Dim Item As New ListItem()
        Item = New ListItem("January", 1)
        ddl.Items.Add(Item)
        Item = New ListItem("February", 2)
        ddl.Items.Add(Item)
        Item = New ListItem("March", 3)
        ddl.Items.Add(Item)
        Item = New ListItem("April", 4)
        ddl.Items.Add(Item)
        Item = New ListItem("May", 5)
        ddl.Items.Add(Item)
        Item = New ListItem("June", 6)
        ddl.Items.Add(Item)
        Item = New ListItem("July", 7)
        ddl.Items.Add(Item)
        Item = New ListItem("August", 8)
        ddl.Items.Add(Item)
        Item = New ListItem("September", 9)
        ddl.Items.Add(Item)
        Item = New ListItem("October", 10)
        ddl.Items.Add(Item)
        Item = New ListItem("November", 11)
        ddl.Items.Add(Item)
        Item = New ListItem("December", 12)
        ddl.Items.Add(Item)
        ddl.SelectedValue = SelectedMonth
    End Sub

    Public Sub BindDDlYear(ByRef ddl As DropDownList, ByVal SelectedYear As Integer)
        Dim SQL As String = ""
        SQL &= " select MIN(RPT_Year) as START_RPT_Year from (" & vbNewLine
        SQL &= " select MIN(RPT_Year) as RPT_Year from RPT_ST_Header" & vbNewLine
        SQL &= " union all" & vbNewLine
        SQL &= " select MIN(RPT_Year) as RPT_Year from RPT_RO_Header" & vbNewLine
        SQL &= " ) as StartYear" & vbNewLine

        Dim DT As New DataTable ' ----------- Current Data -----------
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        Dim StartYear As Integer = 0
        Dim EndYear As Integer = 0
        StartYear = DT.Rows(0).Item("START_RPT_Year")
        EndYear = Date.Now.Year
        If EndYear < 2500 Then
            EndYear = EndYear + 543
        End If
        If EndYear - StartYear = 0 Then
            ddl.Items.Add(DT.Rows(0).Item("START_RPT_Year").ToString)
        Else
            For i As Integer = 0 To (EndYear - StartYear)
                ddl.Items.Add(DT.Rows(0).Item("START_RPT_Year").ToString + i)
            Next
        End If
        If SelectedYear < 2500 Then
            SelectedYear = SelectedYear + 543
        End If
        ddl.SelectedValue = SelectedYear
    End Sub

    Function FindMonthNameEng(ByVal Month As Integer) As String
        Select Case Month
            Case 1
                Return "January"
            Case 2
                Return "February"
            Case 3
                Return "March"
            Case 4
                Return "April"
            Case 5
                Return "May"
            Case 6
                Return "June"
            Case 7
                Return "July"
            Case 8
                Return "August"
            Case 9
                Return "September"
            Case 10
                Return "October"
            Case 11
                Return "November"
            Case 12
                Return "December"
            Case Else
                Return ""
        End Select

    End Function

    Function FindMonthNameEngShot(ByVal Month As Integer) As String
        Select Case Month
            Case 1
                Return "Jan"
            Case 2
                Return "Feb"
            Case 3
                Return "Mar"
            Case 4
                Return "Apr"
            Case 5
                Return "May"
            Case 6
                Return "Jun"
            Case 7
                Return "Jul"
            Case 8
                Return "Aug"
            Case 9
                Return "Sep"
            Case 10
                Return "Oct"
            Case 11
                Return "Nov"
            Case 12
                Return "Dec"
            Case Else
                Return ""
        End Select

    End Function

    Function FindEquipmentName(ByVal Equipment As EIR_BL.Report_Type) As String
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                Return "Stationary"
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                Return "Rotating"
            Case EIR_BL.Report_Type.Lube_Oil_Report
                Return "Lube Oil"
            Case EIR_BL.Report_Type.PdMA_Report
                Return "PdMA"
            Case EIR_BL.Report_Type.MTap_Report
                Return "MTap"
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                Return "PdMA & MTap"
            Case EIR_BL.Report_Type.Thermography_Report
                Return "Thermography"
            Case 99
                Return "All Equipment"
            Case Else
                Return ""
        End Select
    End Function

    Function FindMaxValue(ByVal Val As Integer) As Integer
        Select Case CStr(Val).Length
            Case 1
                Return 10
            Case 2
                Return FindNumber(Left(CStr(Val), 2))
            Case 3
                Return FindNumber(Left(CStr(Val), 2)) + "0"
            Case 4
                Return FindNumber(Left(CStr(Val), 2)) + "00"
            Case 5
                Return FindNumber(Left(CStr(Val), 2)) + "000"
            Case 6
                Return FindNumber(Left(CStr(Val), 2)) + "0000"
            Case Else
                Return Val
        End Select
    End Function

    Function FindNumber(ByVal Val As String) As String
        Dim Ret_Val As String = ""
        If CInt(Val) < 15 Then
            Ret_Val = "15"
        ElseIf CInt(Val) < 20 Then
            Ret_Val = "20"
        ElseIf CInt(Val) < 25 Then
            Ret_Val = "25"
        ElseIf CInt(Val) < 30 Then
            Ret_Val = "30"
        ElseIf CInt(Val) < 35 Then
            Ret_Val = "35"
        ElseIf CInt(Val) < 40 Then
            Ret_Val = "40"
        ElseIf CInt(Val) < 45 Then
            Ret_Val = "45"
        ElseIf CInt(Val) < 50 Then
            Ret_Val = "50"
        ElseIf CInt(Val) < 55 Then
            Ret_Val = "55"
        ElseIf CInt(Val) < 60 Then
            Ret_Val = "60"
        ElseIf CInt(Val) < 65 Then
            Ret_Val = "65"
        ElseIf CInt(Val) < 70 Then
            Ret_Val = "70"
        ElseIf CInt(Val) < 75 Then
            Ret_Val = "75"
        ElseIf CInt(Val) < 80 Then
            Ret_Val = "80"
        ElseIf CInt(Val) < 85 Then
            Ret_Val = "85"
        ElseIf CInt(Val) < 90 Then
            Ret_Val = "90"
        ElseIf CInt(Val) < 95 Then
            Ret_Val = "95"
        Else
            Ret_Val = "100"
        End If
        Return Ret_Val
    End Function

    Function GetDayOfYear(ByVal Year As Integer) As DataTable
        If Year > 2500 Then
            Year = Year - 543
        End If
        '********** หาจำนวนวัน ของปี **********

        Dim DT_Days As New DataTable
        DT_Days.Columns.Add("Date", GetType(DateTime))
        DT_Days.Columns.Add("Days")
        DT_Days.Columns.Add("Months")
        DT_Days.Columns.Add("Plan", GetType(Double))
        DT_Days.Columns.Add("Progess", GetType(Double))
        Dim DR As DataRow

        Dim culture As New System.Globalization.CultureInfo("en-US")

        For i As Integer = 1 To 12
            For j As Integer = 1 To DateTime.DaysInMonth(Year, i)
                DR = DT_Days.NewRow
                Dim tmp As String = Year & "-" & i.ToString.PadLeft(2, "0") & "-" & j.ToString.PadLeft(2, "0")
                DR("Days") = Year.ToString + i.ToString.PadLeft(2, "0") + j.ToString.PadLeft(2, "0")
                DR("Date") = DateTime.ParseExact(tmp, "yyyy-MM-dd", culture)
                Dim C As New Converter
                DR("Months") = C.ToMonthNameEN(i).Substring(0, 3)
                DR("Plan") = 0
                DR("Progess") = 0
                DT_Days.Rows.Add(DR)
            Next
        Next
        Return DT_Days
    End Function

#Region "Bind Data"

#Region "Annual Progress"

    Function AnnualProgressTotal(ByVal Year As Integer, ByVal Equipment As EIR_BL.Report_Type, Optional ByVal PlantId As Integer = 0) As DataTable

        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        SQL &= " declare @Year as int = " & Year & ";" & vbNewLine
        SQL &= " declare @Equipment as int = " & Equipment & ";" & vbNewLine
        SQL &= " declare @PlantId as int = " & PlantId & ";" & vbNewLine

        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select COUNT(1) as Total_Progess from RPT_ST_Header" & vbNewLine
                SQL &= " where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select COUNT(1) as Total_Progess from RPT_RO_Header" & vbNewLine
                SQL &= " where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
            Case 3
                SQL &= "Select Sum(All_Progess) as Total_Progess" & vbNewLine
                SQL &= "from (" & vbNewLine
                SQL &= "select COUNT(1) as All_Progess from RPT_ST_Header" & vbNewLine
                SQL &= "where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= "Union All" & vbNewLine
                SQL &= "select COUNT(1) as All_Progess from RPT_RO_Header" & vbNewLine
                SQL &= "where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= ") as ALL_Equipment" & vbNewLine

            Case EIR_BL.Report_Type.Lube_Oil_Report

                SQL &= " SELECT COUNT(1) Total_Progess  FROM"
                SQL &= " ("
                SQL &= " SELECT DISTINCT dbo.UDF_RPT_Code(RPT_LO_Header.RPT_Year,RPT_LO_Header.RPT_No) RPT_Code," & vbLf
                SQL &= "       RPT_LO_Header.RPT_Year,RPT_Period_Start," & vbLf
                SQL &= "       RPT_Month,CASE RPT_Period_Type WHEN 1 THEN 'Monthly' WHEN 2 THEN 'Quaterly' Else '' END RPT_Period_Type_Name," & vbLf
                SQL &= "       ---------------- Calculate Total TAG -----------" & vbLf
                SQL &= "       CASE WHEN RPT_LO_Header.RPT_STEP>0 THEN RPT_LO_Detail.LO_TAG_ID" & vbLf
                SQL &= "       ELSE TAG.LO_TAG_ID END LO_TAG_ID," & vbLf
                SQL &= "       CASE WHEN RPT_LO_Header.RPT_STEP>0 THEN RPT_LO_Detail.PLANT_ID" & vbLf
                SQL &= "       ELSE TAG.PLANT_ID END PLANT_ID,Plant_Code," & vbLf
                SQL &= "       CASE WHEN RPT_Step=4 THEN 1 ELSE 0 END Is_Completed," & vbLf
                SQL &= "       CASE WHEN RPT_Step=4 THEN RPT_LO_Header.Update_Time ELSE Null END Completed_Date" & vbLf
                SQL &= " 					  " & vbLf
                SQL &= " FROM dbo.RPT_LO_Header " & vbLf
                SQL &= " -----------------แสดงจำนวนใน Detail เมื่อ Step > Collecting --------------------" & vbLf
                SQL &= " LEFT OUTER JOIN RPT_LO_Detail ON ISNULL(RPT_LO_Header.RPT_STEP,0)>0" & vbLf
                SQL &= " 				AND RPT_LO_Header.RPT_Year=RPT_LO_Detail.RPT_Year" & vbLf
                SQL &= " 				AND RPT_LO_Header.RPT_No=RPT_LO_Detail.RPT_No" & vbLf
                SQL &= " 				" & vbLf
                SQL &= " -----------------แสดงจำนวนใน LO_Tag เมื่อ Step < Inspecting --------------------" & vbLf
                SQL &= " LEFT JOIN MS_LO_TAG TAG ON ISNULL(RPT_LO_Header.RPT_STEP,0)=0" & vbLf
                SQL &= " 						AND " & vbLf
                SQL &= " 						(RPT_LO_Header.RPT_Period_Type=2 OR " & vbLf
                SQL &= " 							(RPT_LO_Header.RPT_Period_Type=1 AND TAG.LO_TAG_TYPE=2)" & vbLf
                SQL &= " 						)" & vbLf
                SQL &= " 						AND TAG.Active_Status=1" & vbLf
                SQL &= " 						" & vbLf
                SQL &= " LEFT JOIN MS_PLANT PLANT ON "
                SQL &= " 					  CASE WHEN RPT_LO_Header.RPT_STEP>0 THEN RPT_LO_Detail.PLANT_ID" & vbLf
                SQL &= "                       ELSE TAG.PLANT_ID END = PLANT.PLANT_ID" & vbLf
                SQL &= " WHERE RPT_LO_Header.RPT_Year-543=@Year" & vbLf
                SQL &= ") as ALL_Equipment" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " WHERE PLANT_ID = @PlantId" & vbNewLine
                End If

            Case EIR_BL.Report_Type.PdMA_Report
                SQL &= " select COUNT(1) as Total_Progess from RPT_PdMA_Header" & vbNewLine
                SQL &= " where YEAR(RPT_Period_Start) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If

            Case EIR_BL.Report_Type.Thermography_Report

        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function AnnualProgressDetail(ByVal Year As Integer, ByVal Equipment As EIR_BL.Report_Type, Optional ByVal PlantId As Integer = 0) As DataTable

        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        SQL &= " declare @Year as int= " & Year & ";" & vbNewLine
        SQL &= " declare @Equipment as int = " & Equipment & ";" & vbNewLine
        SQL &= " declare @PlantId as int= " & PlantId & ";" & vbNewLine

        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= "select convert(varchar(8),RPT_Period_End,112) as RPT_Period_End" & vbNewLine
                SQL &= ",convert(varchar(8),RPT_ANL_Date,112) as RPT_ANL_Date" & vbNewLine
                '------------ หา Status ที่ Step >= Collecting และ เลือกเวลาที่น้อยที่สุดมา Collect -> Inspect -> Analyse --------------------------
                'SQL &= ",CASE WHEN RPT_STEP>" & EIR_BL.Report_Step.Collecting_Step & " THEN convert(varchar(8),ISNULL(ISNULL(RPT_COL_Date,RPT_INSP_Date),RPT_ANL_Date),112) ELSE 0 END as RPT_ANL_Date" & vbNewLine

                SQL &= ",'ST' as Equipment" & vbNewLine
                SQL &= "from RPT_ST_Header" & vbNewLine
                'SQL &= "select convert(varchar(8),DATEADD(dd,RD.D,RPT_Period_Start),112) RPT_Period_End,convert(varchar(8),RPT_COL_Date,112) RPT_ANL_Date,'ST' as Equipment" & vbNewLine
                'SQL &= "from RPT_ST_Header" & vbNewLine
                'SQL &= "Left Join" & vbNewLine
                'SQL &= "(SELECT ROUTE_ID,AVG(DATEDIFF(dd,RPT_Period_Start,RPT_COL_Date)) D FROM RPT_ST_Header" & vbNewLine
                'SQL &= "GROUP BY ROUTE_ID) RD ON RPT_ST_Header.ROUTE_ID=RD.ROUTE_ID" & vbNewLine
                SQL &= "where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= "order by RPT_Period_End,RPT_ANL_Date" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= "select convert(varchar(8),RPT_Period_End,112) as RPT_Period_End" & vbNewLine
                SQL &= ",convert(varchar(8),RPT_ANL_Date,112) as RPT_ANL_Date" & vbNewLine

                ''------------ หา Status ที่ Step >= Collecting และ เลือกเวลาที่น้อยที่สุดมา Collect -> Inspect -> Analyse --------------------------
                'SQL &= ",CASE WHEN RPT_STEP>" & EIR_BL.Report_Step.Collecting_Step & " THEN convert(varchar(8),ISNULL(ISNULL(RPT_COL_Date,RPT_INSP_Date),RPT_ANL_Date),112) ELSE NULL END as RPT_ANL_Date" & vbNewLine

                SQL &= ",'RO' as Equipment" & vbNewLine
                SQL &= "from RPT_RO_Header" & vbNewLine
                SQL &= "where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= "order by RPT_Period_End,RPT_ANL_Date" & vbNewLine
                'Case EIR_BL.Report_Type.Lube_Oil_Report
                '    SQL &= " select convert(varchar(8),RPT_Period_End,112) as RPT_Period_End" & vbNewLine
                '    SQL &= " ,convert(varchar(8),RPT_ANL_Date,112) as RPT_ANL_Date" & vbNewLine
                '    SQL &= " ,'ST' as Equipment" & vbNewLine
                '    SQL &= " from RPT_ST_Header" & vbNewLine
                '    SQL &= " where YEAR(RPT_Period_End) = @Year" & vbNewLine
                '    If PlantId > 0 Then
                '        SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                '    End If
                '    SQL &= " Union All" & vbNewLine
                '    SQL &= " select convert(varchar(8),RPT_Period_End,112) as RPT_Period_End" & vbNewLine
                '    SQL &= " ,convert(varchar(8),RPT_ANL_Date,112) as RPT_ANL_Date" & vbNewLine
                '    SQL &= " ,'RO' as Equipment" & vbNewLine
                '    SQL &= " from RPT_RO_Header" & vbNewLine
                '    SQL &= " where YEAR(RPT_Period_End) = @Year" & vbNewLine
                '    If PlantId > 0 Then
                '        SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                '    End If
                '    SQL &= " order by RPT_Period_End,RPT_ANL_Date" & vbNewLine
            Case EIR_BL.Report_Type.Lube_Oil_Report
                SQL &= " SELECT   convert(varchar(8),RPT_Period_End,112) as RPT_Period_End" & vbNewLine
                SQL &= " ,CASE RPT_Step WHEN 4 THEN convert(varchar(8),RPT_LO_Header.RPT_ANL_Date,112) ELSE NULL END RPT_ANL_Date" & vbNewLine

                ''------------ หา Status ที่ Step >= Collecting และ เลือกเวลาที่น้อยที่สุดมา Collect -> Inspect -> Analyse --------------------------
                'SQL &= ",CASE WHEN RPT_STEP>" & EIR_BL.Report_Step.Collecting_Step & " THEN convert(varchar(8),ISNULL(ISNULL(RPT_COL_Date,RPT_INSP_Date),RPT_ANL_Date),112) ELSE 0 END as RPT_ANL_Date" & vbNewLine

                SQL &= " ,'LO' Equipment" & vbNewLine
                SQL &= " 					  " & vbNewLine
                SQL &= " FROM dbo.RPT_LO_Header " & vbNewLine
                SQL &= " -----------------แสดงจำนวนใน Detail เมื่อ Step > Collecting --------------------" & vbNewLine
                SQL &= " LEFT OUTER JOIN RPT_LO_Detail ON ISNULL(RPT_LO_Header.RPT_STEP,0)>0" & vbNewLine
                SQL &= " 				AND RPT_LO_Header.RPT_Year=RPT_LO_Detail.RPT_Year" & vbNewLine
                SQL &= " 				AND RPT_LO_Header.RPT_No=RPT_LO_Detail.RPT_No" & vbNewLine
                SQL &= " 				" & vbNewLine
                SQL &= " -----------------แสดงจำนวนใน LO_Tag เมื่อ Step < Inspecting --------------------" & vbNewLine
                SQL &= " LEFT JOIN MS_LO_TAG TAG ON ISNULL(RPT_LO_Header.RPT_STEP,0)=0" & vbNewLine
                SQL &= " 						AND " & vbNewLine
                SQL &= " 						(RPT_LO_Header.RPT_Period_Type=2 OR " & vbNewLine
                SQL &= " 							(RPT_LO_Header.RPT_Period_Type=1 AND TAG.LO_TAG_TYPE=2)" & vbNewLine
                SQL &= " 						)" & vbNewLine
                SQL &= " 						AND TAG.Active_Status=1" & vbNewLine
                SQL &= " 						" & vbNewLine
                SQL &= " LEFT JOIN MS_PLANT PLANT ON " & vbNewLine
                SQL &= " 					  CASE WHEN RPT_LO_Header.RPT_STEP>0 THEN RPT_LO_Detail.PLANT_ID" & vbNewLine
                SQL &= "                       ELSE TAG.PLANT_ID END = PLANT.PLANT_ID" & vbNewLine
                SQL &= " WHERE RPT_LO_Header.RPT_Year-543=@Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and CASE WHEN RPT_LO_Header.RPT_STEP>0 THEN RPT_LO_Detail.PLANT_ID ELSE TAG.PLANT_ID END = @PlantId" & vbNewLine
                End If
                SQL &= " order by RPT_Period_End,RPT_ANL_Date" & vbNewLine
            Case EIR_BL.Report_Type.PdMA_Report
                SQL &= "select convert(varchar(8),RPT_Period_Start,112) as RPT_Period_End" & vbNewLine
                SQL &= ",convert(varchar(8),RPT_ANL_Date,112) as RPT_ANL_Date" & vbNewLine

                '------------ หา Status ที่ Step >= Collecting และ เลือกเวลาที่น้อยที่สุดมา Collect -> Inspect -> Analyse --------------------------
                'SQL &= ",CASE WHEN RPT_STEP>" & EIR_BL.Report_Step.Collecting_Step & " THEN convert(varchar(8),ISNULL(ISNULL(RPT_COL_Date,RPT_INSP_Date),RPT_ANL_Date),112) ELSE 0 END as RPT_ANL_Date" & vbNewLine

                SQL &= ",'PdMA' as Equipment" & vbNewLine
                SQL &= "from RPT_PdMA_Header" & vbNewLine
                SQL &= "where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= "order by RPT_Period_End,RPT_ANL_Date" & vbNewLine
            Case EIR_BL.Report_Type.MTap_Report
                SQL &= "select convert(varchar(8),RPT_Period_End,112) as RPT_Period_End" & vbNewLine
                SQL &= ",convert(varchar(8),RPT_ANL_Date,112) as RPT_ANL_Date" & vbNewLine
                SQL &= ",'MTAP' as Equipment" & vbNewLine
                SQL &= "from RPT_MTAP_Header" & vbNewLine
                SQL &= "where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= "order by RPT_Period_End,RPT_ANL_Date" & vbNewLine
            Case EIR_BL.Report_Type.Thermography_Report
                SQL &= "select convert(varchar(8),RPT_Period_End,112) as RPT_Period_End" & vbNewLine
                SQL &= ",convert(varchar(8),RPT_ANL_Date,112) as RPT_ANL_Date" & vbNewLine
                SQL &= ",'THM' as Equipment" & vbNewLine
                SQL &= "from RPT_THM_Header" & vbNewLine
                SQL &= "where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= "order by RPT_Period_End,RPT_ANL_Date" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function AnnualProgressPlan(ByVal Year As Integer, ByVal Equipment As EIR_BL.Report_Type, Optional ByVal PlantId As Integer = 0) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        SQL &= " declare @Year as int= " & Year & ";" & vbNewLine
        SQL &= " declare @Equipment as int= " & Equipment & ";" & vbNewLine
        SQL &= " declare @PlantId as int= " & PlantId & ";" & vbNewLine

        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select MONTH(RPT_Period_End) as M,COUNT(1) as M_Period_End" & vbNewLine
                SQL &= " from RPT_ST_Header" & vbNewLine
                SQL &= " where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= " group by MONTH(RPT_Period_End)" & vbNewLine
                SQL &= " order by MONTH(RPT_Period_End)" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select MONTH(RPT_Period_End) as M,COUNT(1) as M_Period_End" & vbNewLine
                SQL &= " from RPT_RO_Header" & vbNewLine
                SQL &= " where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= " group by MONTH(RPT_Period_End)" & vbNewLine
                SQL &= " order by MONTH(RPT_Period_End)" & vbNewLine
                'Case 3
                '    SQL &= " select M,SUM(M_Period_End) as M_Period_End" & vbNewLine
                '    SQL &= " from (" & vbNewLine
                '    SQL &= " 	select MONTH(RPT_Period_End) as M,COUNT(1) as M_Period_End" & vbNewLine
                '    SQL &= " 	from RPT_ST_Header" & vbNewLine
                '    SQL &= " 	where YEAR(RPT_Period_End) = @Year" & vbNewLine
                '    If PlantId > 0 Then
                '        SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                '    End If
                '    SQL &= " 	group by MONTH(RPT_Period_End)" & vbNewLine
                '    SQL &= " 	Union All" & vbNewLine
                '    SQL &= " 	select MONTH(RPT_Period_End) as M,COUNT(1) as M_Period_End" & vbNewLine
                '    SQL &= " 	from RPT_RO_Header" & vbNewLine
                '    SQL &= " 	where YEAR(RPT_Period_End) = @Year" & vbNewLine
                '    If PlantId > 0 Then
                '        SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                '    End If
                '    SQL &= " 	group by MONTH(RPT_Period_End)" & vbNewLine
                '    SQL &= "       ) as ALL_Equipment" & vbNewLine
                '    SQL &= " group by M" & vbNewLine
                '    SQL &= " order by M" & vbNewLine
            Case EIR_BL.Report_Type.Lube_Oil_Report
                SQL &= " SELECT MONTH(RPT_Period_End) M,COUNT(1) M_Period_End --," & vbNewLine
                SQL &= " 	  --CASE RPT_Step WHEN 4 THEN 1 ELSE 0 END RPT_ANL_Date" & vbNewLine
                SQL &= " 					  " & vbNewLine
                SQL &= " FROM dbo.RPT_LO_Header " & vbNewLine
                SQL &= " -----------------แสดงจำนวนใน Detail เมื่อ Step > Collecting --------------------" & vbNewLine
                SQL &= " LEFT OUTER JOIN RPT_LO_Detail ON ISNULL(RPT_LO_Header.RPT_STEP,0)>0" & vbNewLine
                SQL &= " 				AND RPT_LO_Header.RPT_Year=RPT_LO_Detail.RPT_Year  						" & vbNewLine
                SQL &= " 				AND RPT_LO_Header.RPT_No=RPT_LO_Detail.RPT_No" & vbNewLine
                SQL &= " 				" & vbNewLine
                SQL &= " -----------------แสดงจำนวนใน LO_Tag เมื่อ Step < Inspecting --------------------" & vbNewLine
                SQL &= " LEFT JOIN MS_LO_TAG TAG ON ISNULL(RPT_LO_Header.RPT_STEP,0)=0" & vbNewLine
                SQL &= " 						AND " & vbNewLine
                SQL &= " 						(RPT_LO_Header.RPT_Period_Type=2 OR " & vbNewLine
                SQL &= " 							(RPT_LO_Header.RPT_Period_Type=1 AND TAG.LO_TAG_TYPE=2)" & vbNewLine
                SQL &= " 						)" & vbNewLine
                SQL &= " 						AND TAG.Active_Status=1" & vbNewLine
                SQL &= " 						" & vbNewLine
                SQL &= " LEFT JOIN MS_PLANT PLANT ON " & vbNewLine
                SQL &= " 					  CASE WHEN RPT_LO_Header.RPT_STEP>0 THEN RPT_LO_Detail.PLANT_ID" & vbNewLine
                SQL &= "                       ELSE TAG.PLANT_ID END = PLANT.PLANT_ID" & vbNewLine
                SQL &= " WHERE RPT_LO_Header.RPT_Year-543 = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and CASE WHEN RPT_LO_Header.RPT_STEP>0 THEN RPT_LO_Detail.PLANT_ID ELSE TAG.PLANT_ID END = @PlantId" & vbNewLine
                End If
                SQL &= " " & vbNewLine
                SQL &= " GROUP BY MONTH(RPT_Period_End)" & vbNewLine
                SQL &= " order by M" & vbNewLine
            Case EIR_BL.Report_Type.PdMA_Report
                SQL &= " select MONTH(RPT_Period_Start) as M,COUNT(1) as M_Period_End" & vbNewLine
                SQL &= " from RPT_PdMA_Header" & vbNewLine
                SQL &= " where YEAR(RPT_Period_End) = @Year" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= " group by MONTH(RPT_Period_Start)" & vbNewLine
                SQL &= " order by MONTH(RPT_Period_Start)" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function AnnualProgressComplete(ByVal Year As Integer, ByVal Equipment As EIR_BL.Report_Type, Optional ByVal PlantId As Integer = 0) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        SQL &= " declare @Year as int; select @Year = " & Year & ";" & vbNewLine
        SQL &= " declare @Equipment as int; select @Equipment = " & Equipment & ";" & vbNewLine
        SQL &= " declare @PlantId as int; select @PlantId = " & PlantId & ";" & vbNewLine

        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select MONTH(RPT_ANL_Date) as M,COUNT(1) as M_ANL_Date" & vbNewLine
                SQL &= " from RPT_ST_Header" & vbNewLine
                SQL &= " where YEAR(RPT_Period_End) = @Year and RPT_ANL_Date is not null" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= " group by MONTH(RPT_ANL_Date)" & vbNewLine
                SQL &= " order by MONTH(RPT_ANL_Date)" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select MONTH(RPT_ANL_Date) as M,COUNT(1) as M_ANL_Date" & vbNewLine
                SQL &= " from RPT_RO_Header" & vbNewLine
                SQL &= " where YEAR(RPT_Period_End) = @Year and RPT_ANL_Date is not null" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= " group by MONTH(RPT_ANL_Date)" & vbNewLine
                SQL &= " order by MONTH(RPT_ANL_Date)" & vbNewLine
                'Case 3
                '    SQL &= " select M,SUM(M_ANL_Date) as M_ANL_Date"
                '    SQL &= " from (" & vbNewLine
                '    SQL &= " 	select MONTH(RPT_ANL_Date) as M,COUNT(1) as M_ANL_Date" & vbNewLine
                '    SQL &= " 	from RPT_ST_Header" & vbNewLine
                '    SQL &= " 	where YEAR(RPT_Period_End) = @Year and RPT_ANL_Date is not null" & vbNewLine
                '    If PlantId > 0 Then
                '        SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                '    End If
                '    SQL &= " 	group by MONTH(RPT_ANL_Date)" & vbNewLine
                '    SQL &= " 	Union All" & vbNewLine
                '    SQL &= " 	select MONTH(RPT_ANL_Date) as M,COUNT(1) as M_ANL_Date" & vbNewLine
                '    SQL &= " 	from RPT_RO_Header" & vbNewLine
                '    SQL &= " 	where YEAR(RPT_Period_End) = @Year and RPT_ANL_Date is not null" & vbNewLine
                '    If PlantId > 0 Then
                '        SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                '    End If
                '    SQL &= " 	group by MONTH(RPT_ANL_Date)" & vbNewLine
                '    SQL &= "      ) as ALL_Equipment" & vbNewLine
                '    SQL &= " group by M" & vbNewLine
                '    SQL &= " order by M" & vbNewLine
            Case EIR_BL.Report_Type.Lube_Oil_Report

                SQL &= " SELECT MONTH(ISNULL(RPT_ANL_Date,RPT_LO_Header.Update_Time)) M,COUNT(1) M_ANL_Date " & vbNewLine
                SQL &= " 					  " & vbNewLine
                SQL &= " FROM dbo.RPT_LO_Header " & vbNewLine
                SQL &= " -----------------แสดงจำนวนใน Detail เมื่อ Step > Collecting --------------------" & vbNewLine
                SQL &= " LEFT OUTER JOIN RPT_LO_Detail ON ISNULL(RPT_LO_Header.RPT_STEP,0)>0" & vbNewLine
                SQL &= " 				AND RPT_LO_Header.RPT_Year=RPT_LO_Detail.RPT_Year  						" & vbNewLine
                SQL &= " 				AND RPT_LO_Header.RPT_No=RPT_LO_Detail.RPT_No" & vbNewLine
                SQL &= " 				" & vbNewLine
                SQL &= " -----------------แสดงจำนวนใน LO_Tag เมื่อ Step < Inspecting --------------------" & vbNewLine
                SQL &= " LEFT JOIN MS_LO_TAG TAG ON ISNULL(RPT_LO_Header.RPT_STEP,0)=0" & vbNewLine
                SQL &= " 						AND " & vbNewLine
                SQL &= " 						(RPT_LO_Header.RPT_Period_Type=2 OR " & vbNewLine
                SQL &= " 							(RPT_LO_Header.RPT_Period_Type=1 AND TAG.LO_TAG_TYPE=2)" & vbNewLine
                SQL &= " 						)" & vbNewLine
                SQL &= " 						AND TAG.Active_Status=1" & vbNewLine
                SQL &= " 						" & vbNewLine
                SQL &= " LEFT JOIN MS_PLANT PLANT ON " & vbNewLine
                SQL &= " 					  CASE WHEN RPT_LO_Header.RPT_STEP>0 THEN RPT_LO_Detail.PLANT_ID" & vbNewLine
                SQL &= "                       ELSE TAG.PLANT_ID END = PLANT.PLANT_ID" & vbNewLine
                SQL &= " WHERE RPT_LO_Header.RPT_Year-543 = @Year AND RPT_LO_Header.RPT_STEP=4" & vbNewLine
                SQL &= " " & vbNewLine
                SQL &= " and CASE WHEN RPT_LO_Header.RPT_STEP>0 THEN RPT_LO_Detail.PLANT_ID ELSE TAG.PLANT_ID END = @PlantId" & vbNewLine
                SQL &= " " & vbNewLine
                SQL &= " GROUP BY MONTH(ISNULL(RPT_ANL_Date,RPT_LO_Header.Update_Time))" & vbNewLine
                SQL &= " order by M" & vbNewLine
            Case EIR_BL.Report_Type.PdMA_Report
                SQL &= " select MONTH(RPT_ANL_Date) as M,COUNT(1) as M_ANL_Date" & vbNewLine
                SQL &= " from RPT_PdMA_Header" & vbNewLine
                SQL &= " where YEAR(RPT_Period_End) = @Year and RPT_ANL_Date is not null" & vbNewLine
                If PlantId > 0 Then
                    SQL &= " and PLANT_ID = @PlantId" & vbNewLine
                End If
                SQL &= " group by MONTH(RPT_ANL_Date)" & vbNewLine
                SQL &= " order by MONTH(RPT_ANL_Date)" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function
#End Region

#Region "Improvement"
    Function Improvement(ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        If Year_F > 2500 Then
            Year_F = Year_F - 543
        End If
        If Year_T > 2500 Then
            Year_T = Year_T - 543
        End If

        SQL &= " Declare @Date_F as varchar(8); " & vbNewLine
        SQL &= " Select @Date_F = Convert(varchar(4)," & Year_F & ") + Right('0' +  Convert(varchar(2)," & Month_F & "),2) + '01';" & vbNewLine
        SQL &= " Declare @Date_T as varchar(8); " & vbNewLine
        SQL &= " Select @Date_T = Convert(varchar(4)," & Year_T & ") + Right('0' +  Convert(varchar(2)," & Month_T & "),2) + '32';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                SQL &= " isnull(Improvement,0) Improvement" & vbNewLine
                SQL &= " from MS_Plant" & vbNewLine
                SQL &= " left join" & vbNewLine
                SQL &= " (" & vbNewLine
                SQL &= " 	select PLANT_ID,COUNT(1) as Improvement" & vbNewLine
                SQL &= " 	from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " 	ISSUE = " & REPORT_ISSUE.Fixed_Completed & vbNewLine
                SQL &= " 	group by PLANT_ID" & vbNewLine
                SQL &= " ) ST on MS_Plant.PLANT_ID = ST.PLANT_ID" & vbNewLine
                SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                SQL &= " isnull(Improvement,0) Improvement" & vbNewLine
                SQL &= " from MS_Plant" & vbNewLine
                SQL &= " left join" & vbNewLine
                SQL &= " (" & vbNewLine
                SQL &= " 	select PLANT_ID,COUNT(1) as Improvement" & vbNewLine
                SQL &= " 	from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " 	ISSUE = " & REPORT_ISSUE.Fixed_Completed & vbNewLine
                SQL &= " 	group by PLANT_ID" & vbNewLine
                SQL &= " ) RO on MS_Plant.PLANT_ID = RO.PLANT_ID" & vbNewLine
                SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
            Case EIR_BL.Report_Type.Lube_Oil_Report
                SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                SQL &= " isnull(Improvement,0) Improvement" & vbNewLine
                SQL &= " from MS_Plant" & vbNewLine
                SQL &= " left join" & vbNewLine
                SQL &= " (" & vbNewLine
                SQL &= "	SELECT PLANT_ID," & vbNewLine
                SQL &= "	SUM(CASE WHEN TAN_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN OX_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN WATER_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN VANISH_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END) Improvement" & vbNewLine
                SQL &= "	FROM VW_DASHBOARD_LO " & vbNewLine
                SQL &= "	WHERE (TAN_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR OX_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR WATER_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR VANISH_ISSUE = " & REPORT_ISSUE.Fixed_Completed & ") AND" & vbNewLine
                SQL &= "	CONVERT(varchar(8),INSPECTED_DATE,112) >= @Date_F AND" & vbNewLine
                SQL &= " 	CONVERT(varchar(8),INSPECTED_DATE,112) <= @Date_T" & vbNewLine
                SQL &= "	GROUP BY PLANT_ID" & vbNewLine
                SQL &= " ) LO on MS_Plant.PLANT_ID = LO.PLANT_ID" & vbNewLine
                SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                SQL &= " isnull(Improvement,0) Improvement" & vbNewLine
                SQL &= " from MS_Plant" & vbNewLine
                SQL &= " left join" & vbNewLine
                SQL &= " (" & vbNewLine
                SQL &= "	SELECT PLANT_ID," & vbNewLine
                SQL &= "	SUM(CASE WHEN PWQ_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN INS_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN PWC_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END +" & vbNewLine
                SQL &= "	CASE WHEN STA_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN ROT_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN AIR_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END) Improvement" & vbNewLine
                SQL &= "	FROM VW_DASHBOARD_PDMA " & vbNewLine
                SQL &= "	WHERE (PWQ_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR INS_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR PWC_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR STA_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR ROT_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR AIR_ISSUE = " & REPORT_ISSUE.Fixed_Completed & ") AND" & vbNewLine
                SQL &= "	CONVERT(varchar(8),INSPEC_DATE,112) >= @Date_F AND" & vbNewLine
                SQL &= " 	CONVERT(varchar(8),INSPEC_DATE,112) <= @Date_T" & vbNewLine
                SQL &= "	GROUP BY PLANT_ID" & vbNewLine
                SQL &= " ) PDMA on MS_Plant.PLANT_ID = PDMA.PLANT_ID" & vbNewLine

                SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
            Case EIR_BL.Report_Type.Thermography_Report
                SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                SQL &= " isnull(Improvement,0) Improvement" & vbNewLine
                SQL &= " from MS_Plant" & vbNewLine
                SQL &= " left join" & vbNewLine
                SQL &= " (" & vbNewLine
                SQL &= "	SELECT PLANT_ID,COUNT(1)Improvement" & vbNewLine
                SQL &= "	FROM VW_DASHBOARD_THM" & vbNewLine
                SQL &= "	WHERE ISSUE = " & REPORT_ISSUE.Fixed_Completed & " AND" & vbNewLine
                SQL &= "	CONVERT(varchar(8),Inspected_Date,112) >= @Date_F AND" & vbNewLine
                SQL &= " 	CONVERT(varchar(8),Inspected_Date,112) <= @Date_T" & vbNewLine
                SQL &= "	GROUP BY PLANT_ID" & vbNewLine
                SQL &= " ) THM on MS_Plant.PLANT_ID = THM.PLANT_ID" & vbNewLine
                SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function ImprovementPlant(ByVal PlantID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL &= " Declare @Plant_ID as int;select @Plant_ID = " & PlantID & ";" & vbNewLine
        SQL &= " Declare @Date_F as varchar(8); " & vbNewLine
        SQL &= " Select @Date_F = Convert(varchar(4)," & Year_F & ") + Right('0' +  Convert(varchar(2)," & Month_F & "),2) + '01';" & vbNewLine
        SQL &= " Declare @Date_T as varchar(8); " & vbNewLine
        SQL &= " Select @Date_T = Convert(varchar(4)," & Year_T & ") + Right('0' +  Convert(varchar(2)," & Month_T & "),2) + '32';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select MONTH(RPT_Period_Start) as MM,YEAR(RPT_Period_Start) as YY,COUNT(1) as Improvement" & vbNewLine
                SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " ISSUE = " & REPORT_ISSUE.Fixed_Completed & " and PLANT_ID = @Plant_ID" & vbNewLine
                SQL &= " group by PLANT_ID,MONTH(RPT_Period_Start),YEAR(RPT_Period_Start)" & vbNewLine
                SQL &= " order by YY,MM" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select MONTH(RPT_Period_Start) as MM,YEAR(RPT_Period_Start) as YY,COUNT(1) as Improvement" & vbNewLine
                SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " ISSUE = " & REPORT_ISSUE.Fixed_Completed & " and PLANT_ID = @Plant_ID" & vbNewLine
                SQL &= " group by PLANT_ID,MONTH(RPT_Period_Start),YEAR(RPT_Period_Start)" & vbNewLine
                SQL &= " order by YY,MM" & vbNewLine
            Case EIR_BL.Report_Type.Lube_Oil_Report
                SQL &= "select MONTH(Inspected_Date) as MM,YEAR(Inspected_Date) as YY," & vbNewLine
                SQL &= "	SUM(CASE WHEN TAN_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN OX_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN WATER_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN VANISH_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END) Improvement" & vbNewLine
                SQL &= "from VW_DASHBOARD_LO" & vbNewLine
                SQL &= "where (TAN_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR OX_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR WATER_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR VANISH_ISSUE = " & REPORT_ISSUE.Fixed_Completed & ") AND" & vbNewLine
                SQL &= "CONVERT(varchar(8),Inspected_Date,112) >= @Date_F and" & vbNewLine
                SQL &= "CONVERT(varchar(8),Inspected_Date,112) <= @Date_T and" & vbNewLine
                SQL &= "PLANT_ID = @Plant_ID" & vbNewLine
                SQL &= "group by PLANT_ID,MONTH(Inspected_Date),YEAR(Inspected_Date)" & vbNewLine
                SQL &= "order by YY,MM" & vbNewLine
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                SQL &= "select MONTH(Inspec_Date) as MM,YEAR(Inspec_Date) as YY," & vbNewLine
                SQL &= "	SUM(CASE WHEN PWQ_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN INS_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN PWC_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END +" & vbNewLine
                SQL &= "	CASE WHEN STA_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN ROT_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN AIR_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END) Improvement" & vbNewLine
                SQL &= "from VW_DASHBOARD_PDMA" & vbNewLine
                SQL &= "where (PWQ_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR INS_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR PWC_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR STA_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR ROT_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR AIR_ISSUE = " & REPORT_ISSUE.Fixed_Completed & ") AND" & vbNewLine
                SQL &= "CONVERT(varchar(8),Inspec_Date,112) >= @Date_F and" & vbNewLine
                SQL &= "CONVERT(varchar(8),Inspec_Date,112) <= @Date_T and" & vbNewLine
                SQL &= "PLANT_ID = @Plant_ID" & vbNewLine
                SQL &= "group by PLANT_ID,MONTH(Inspec_Date),YEAR(Inspec_Date)" & vbNewLine
                SQL &= "order by YY,MM" & vbNewLine
            Case EIR_BL.Report_Type.Thermography_Report
                SQL &= " select MONTH(Inspected_Date) as MM,YEAR(Inspected_Date) as YY,COUNT(1) as Improvement" & vbNewLine
                SQL &= " from VW_DASHBOARD_THM" & vbNewLine
                SQL &= " where CONVERT(varchar(8),Inspected_Date,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),Inspected_Date,112) <= @Date_T and" & vbNewLine
                SQL &= " ISSUE = " & REPORT_ISSUE.Fixed_Completed & " and PLANT_ID = @Plant_ID" & vbNewLine
                SQL &= " group by PLANT_ID,MONTH(Inspected_Date),YEAR(Inspected_Date)" & vbNewLine
                SQL &= " order by YY,MM" & vbNewLine

        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function
#End Region

#Region "Improvement Sheet"
    Function ImprovementSheetYear(ByVal Year As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        SQL &= " Declare @Year as int; " & vbNewLine
        SQL &= " Select @Year = " & Year & ";" & vbNewLine

        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select Convert(varchar(4),Year(RPT_Period_Start)) as YY,Count(1) as Improvement" & vbNewLine
                SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " where Year(RPT_Period_Start) < @Year and ISSUE = " & REPORT_ISSUE.Fixed_Completed & vbNewLine
                SQL &= " group by Year(RPT_Period_Start)" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select Convert(varchar(4),Year(RPT_Period_Start)) as YY,Count(1) as Improvement" & vbNewLine
                SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " where Year(RPT_Period_Start) < @Year and ISSUE = " & REPORT_ISSUE.Fixed_Completed & vbNewLine
                SQL &= " group by Year(RPT_Period_Start)" & vbNewLine
            Case EIR_BL.Report_Type.Lube_Oil_Report
                SQL &= " select Convert(varchar(4),Year(Inspected_Date)) as YY," & vbNewLine
                SQL &= "	SUM(CASE WHEN TAN_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN OX_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN WATER_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN VANISH_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END) Improvement" & vbNewLine
                SQL &= "from VW_DASHBOARD_LO" & vbNewLine
                SQL &= "where (TAN_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR OX_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR WATER_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR VANISH_ISSUE = " & REPORT_ISSUE.Fixed_Completed & ") AND" & vbNewLine
                SQL &= "Year(Inspected_Date) < @Year" & vbNewLine
                SQL &= "group by Year(Inspected_Date)" & vbNewLine
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                SQL &= " select Convert(varchar(4),Year(Inspec_Date)) as YY," & vbNewLine
                SQL &= "	SUM(CASE WHEN PWQ_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN INS_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN PWC_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END +" & vbNewLine
                SQL &= "	CASE WHEN STA_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN ROT_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "	CASE WHEN AIR_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END) Improvement" & vbNewLine
                SQL &= "from VW_DASHBOARD_PDMA" & vbNewLine
                SQL &= "where (PWQ_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR INS_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR PWC_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR STA_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR ROT_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR AIR_ISSUE = " & REPORT_ISSUE.Fixed_Completed & ") AND" & vbNewLine
                SQL &= "Year(Inspec_Date) < @Year" & vbNewLine
                SQL &= "group by Year(Inspec_Date)" & vbNewLine
            Case EIR_BL.Report_Type.Thermography_Report
                SQL &= " select Convert(varchar(4),Year(Inspected_Date)) as YY,Count(1) as Improvement" & vbNewLine
                SQL &= " from VW_DASHBOARD_THM" & vbNewLine
                SQL &= " where Year(Inspected_Date) < @Year and ISSUE = " & REPORT_ISSUE.Fixed_Completed & vbNewLine
                SQL &= " group by Year(Inspected_Date)" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function ImprovementSheetMonth(ByVal Year As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL &= " Declare @Year as int; " & vbNewLine
        SQL &= " Select @Year = " & Year & ";" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select convert(varchar(3),Datename(month,RPT_Period_Start)) as MM,Count(1) as Improvement" & vbNewLine
                SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " where Year(RPT_Period_Start) = @Year and ISSUE = " & REPORT_ISSUE.Fixed_Completed & vbNewLine
                SQL &= " group by Datename(month,RPT_Period_Start)" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select convert(varchar(3),Datename(month,RPT_Period_Start)) as MM,Count(1) as Improvement" & vbNewLine
                SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " where Year(RPT_Period_Start) = @Year and ISSUE = " & REPORT_ISSUE.Fixed_Completed & vbNewLine
                SQL &= " group by Datename(month,RPT_Period_Start)" & vbNewLine
            Case EIR_BL.Report_Type.Lube_Oil_Report
                SQL &= "select convert(varchar(3),Datename(month,INSPECTED_DATE)) as MM," & vbNewLine
                SQL &= "SUM(CASE WHEN TAN_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "CASE WHEN OX_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "CASE WHEN WATER_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END +" & vbNewLine
                SQL &= "CASE WHEN VANISH_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END) Improvement" & vbNewLine
                SQL &= "from VW_DASHBOARD_LO" & vbNewLine
                SQL &= "where Year(INSPECTED_DATE) = @Year and " & vbNewLine
                SQL &= "(TAN_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR OX_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR WATER_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR VANISH_ISSUE = " & REPORT_ISSUE.Fixed_Completed & ")" & vbNewLine
                SQL &= "group by Datename(month,INSPECTED_DATE)" & vbNewLine
            Case EIR_BL.Report_Type.PdMA_MTap_Report
                SQL &= "select convert(varchar(3),Datename(month,Inspec_Date)) as MM," & vbNewLine
                SQL &= "SUM(CASE WHEN PWQ_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END +" & vbNewLine
                SQL &= "CASE WHEN INS_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "CASE WHEN PWC_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END +" & vbNewLine
                SQL &= "CASE WHEN STA_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END +" & vbNewLine
                SQL &= "CASE WHEN ROT_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END + " & vbNewLine
                SQL &= "CASE WHEN AIR_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " THEN 1 ELSE 0 END) Improvement" & vbNewLine
                SQL &= "from VW_DASHBOARD_PDMA" & vbNewLine
                SQL &= "where Year(Inspec_Date) = @Year and " & vbNewLine
                SQL &= "(PWQ_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR INS_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR PWC_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR STA_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR ROT_ISSUE = " & REPORT_ISSUE.Fixed_Completed & " OR AIR_ISSUE = " & REPORT_ISSUE.Fixed_Completed & ")" & vbNewLine
                SQL &= "group by Datename(month,Inspec_Date)" & vbNewLine
            Case EIR_BL.Report_Type.Thermography_Report
                SQL &= " select convert(varchar(3),Datename(month,Inspected_Date)) as MM,Count(1) as Improvement" & vbNewLine
                SQL &= " from VW_DASHBOARD_THM" & vbNewLine
                SQL &= " where Year(Inspected_Date) = @Year and ISSUE = " & REPORT_ISSUE.Fixed_Completed & vbNewLine
                SQL &= " group by Datename(month,Inspected_Date)" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function
#End Region

#Region "Problem"
    Function Problem(ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        SQL &= " Declare @Date_F as varchar(8); " & vbNewLine
        SQL &= " Select @Date_F = Convert(varchar(4)," & Year_F & ") + Right('0' +  Convert(varchar(2)," & Month_F & "),2) + '01';" & vbNewLine
        SQL &= " Declare @Date_T as varchar(8); " & vbNewLine
        SQL &= " Select @Date_T = Convert(varchar(4)," & Year_T & ") + Right('0' +  Convert(varchar(2)," & Month_T & "),2) + '32';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                SQL &= " isnull(ClassC,0) ClassC," & vbNewLine
                SQL &= " isnull(ClassB,0) ClassB," & vbNewLine
                SQL &= " isnull(ClassA,0) ClassA" & vbNewLine
                SQL &= " from MS_Plant" & vbNewLine
                SQL &= " left join" & vbNewLine
                SQL &= " (" & vbNewLine
                SQL &= " 	select PLANT_ID," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " 	from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " 	ISSUE = " & REPORT_ISSUE.New_Problem & vbNewLine
                SQL &= " 	group by PLANT_ID" & vbNewLine
                SQL &= " ) ST on MS_Plant.PLANT_ID = ST.PLANT_ID" & vbNewLine
                SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                SQL &= " isnull(ClassC,0) ClassC," & vbNewLine
                SQL &= " isnull(ClassB,0) ClassB," & vbNewLine
                SQL &= " isnull(ClassA,0) ClassA" & vbNewLine
                SQL &= " from MS_Plant" & vbNewLine
                SQL &= " left join" & vbNewLine
                SQL &= " (" & vbNewLine
                SQL &= " 	select PLANT_ID," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " 	from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " 	ISSUE = " & REPORT_ISSUE.New_Problem & vbNewLine
                SQL &= " 	group by PLANT_ID" & vbNewLine
                SQL &= " ) RO on MS_Plant.PLANT_ID = RO.PLANT_ID" & vbNewLine
                SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
                'Case 3
                '    SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                '    SQL &= " isnull(ClassC,0) ClassC," & vbNewLine
                '    SQL &= " isnull(ClassB,0) ClassB," & vbNewLine
                '    SQL &= " isnull(ClassA,0) ClassA" & vbNewLine
                '    SQL &= " from MS_Plant" & vbNewLine
                '    SQL &= " left join" & vbNewLine
                '    SQL &= " ("
                '    SQL &= " 	select PLANT_ID,SUM(ClassC) ClassC,SUM(ClassB) ClassB,SUM(ClassA) ClassA" & vbNewLine
                '    SQL &= " 	from (" & vbNewLine
                '    SQL &= " 		select PLANT_ID," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 		from VW_DASHBOARD_ST" & vbNewLine
                '    SQL &= " 		where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                '    SQL &= " 		CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                '    SQL &= " 		ISSUE = 1" & vbNewLine
                '    SQL &= " 		group by PLANT_ID" & vbNewLine
                '    SQL &= " 		union all" & vbNewLine
                '    SQL &= " 		select PLANT_ID," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 		from VW_DASHBOARD_RO" & vbNewLine
                '    SQL &= " 		where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                '    SQL &= " 		CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                '    SQL &= " 		ISSUE = 1" & vbNewLine
                '    SQL &= " 		group by PLANT_ID" & vbNewLine
                '    SQL &= " 	) as ALL_Equipment" & vbNewLine
                '    SQL &= " 	group by PLANT_ID" & vbNewLine
                '    SQL &= " ) ALL_EQ on MS_Plant.PLANT_ID = ALL_EQ.PLANT_ID" & vbNewLine
                '    SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function ProblemPlant(ByVal PlantID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL &= " Declare @Plant_ID as int;select @Plant_ID = " & PlantID & ";" & vbNewLine
        SQL &= " Declare @Date_F as varchar(8); " & vbNewLine
        SQL &= " Select @Date_F = Convert(varchar(4)," & Year_F & ") + Right('0' +  Convert(varchar(2)," & Month_F & "),2) + '01';" & vbNewLine
        SQL &= " Declare @Date_T as varchar(8); " & vbNewLine
        SQL &= " Select @Date_T = Convert(varchar(4)," & Year_T & ") + Right('0' +  Convert(varchar(2)," & Month_T & "),2) + '32';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select MONTH(RPT_Period_Start) as MM,YEAR(RPT_Period_Start) as YY," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " ISSUE = " & REPORT_ISSUE.New_Problem & " and PLANT_ID = @Plant_ID" & vbNewLine
                SQL &= " group by PLANT_ID,MONTH(RPT_Period_Start),YEAR(RPT_Period_Start)" & vbNewLine
                SQL &= " order by YY,MM" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select MONTH(RPT_Period_Start) as MM,YEAR(RPT_Period_Start) as YY," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " ISSUE = " & REPORT_ISSUE.New_Problem & " and PLANT_ID = @Plant_ID" & vbNewLine
                SQL &= " group by PLANT_ID,MONTH(RPT_Period_Start),YEAR(RPT_Period_Start)" & vbNewLine
                SQL &= " order by YY,MM" & vbNewLine
                'Case 3
                '    SQL &= " select MM,YY,SUM(ClassC) ClassC,SUM(ClassB) ClassB,SUM(ClassA) ClassA" & vbNewLine
                '    SQL &= " from (" & vbNewLine
                '    SQL &= " 	select MONTH(RPT_Period_Start) as MM,YEAR(RPT_Period_Start) as YY," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 	from VW_DASHBOARD_ST" & vbNewLine
                '    SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                '    SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                '    SQL &= " 	ISSUE = 1 and PLANT_ID = @Plant_ID" & vbNewLine
                '    SQL &= " 	group by MONTH(RPT_Period_Start),YEAR(RPT_Period_Start)" & vbNewLine
                '    SQL &= " 	union all" & vbNewLine
                '    SQL &= " 	select MONTH(RPT_Period_Start) as MM,YEAR(RPT_Period_Start) as YY," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 	from VW_DASHBOARD_RO" & vbNewLine
                '    SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                '    SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                '    SQL &= " 	ISSUE = 1 and PLANT_ID = @Plant_ID" & vbNewLine
                '    SQL &= " 	group by MONTH(RPT_Period_Start),YEAR(RPT_Period_Start)" & vbNewLine
                '    SQL &= " ) as ALL_Equipment" & vbNewLine
                '    SQL &= " group by MM,YY" & vbNewLine
                '    SQL &= " order by YY,MM" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function
#End Region

#Region "Search Tag Class"
    Function SearchTagClassListTag(ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        SQL &= " Declare @Date_F as varchar(8); " & vbNewLine
        SQL &= " Select @Date_F = Convert(varchar(4)," & Year_F & ") + Right('0' +  Convert(varchar(2)," & Month_F & "),2) + '01';" & vbNewLine
        SQL &= " Declare @Date_T as varchar(8); " & vbNewLine
        SQL &= " Select @Date_T = Convert(varchar(4)," & Year_T & ") + Right('0' +  Convert(varchar(2)," & Month_T & "),2) + '32';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select 0 as Row,'Stationary'as EQ,RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID," & vbNewLine
                SQL &= " ROUTE_NAME,RPT_Round,PLANT_Order," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) as ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) as ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) as ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and ISSUE = " & REPORT_ISSUE.New_Problem & vbNewLine
                SQL &= " group by RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID,ROUTE_NAME,RPT_Round,PLANT_Order" & vbNewLine
                SQL &= " order by EQ desc,PLANT_Order,RPT_Year,ROUTE_ID,RPT_Round,RPT_CODE" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select 0 as Row,'Rotating'as EQ,RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID," & vbNewLine
                SQL &= " ROUTE_NAME,RPT_Round,PLANT_Order," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) as ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) as ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) as ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and ISSUE = " & REPORT_ISSUE.New_Problem & vbNewLine
                SQL &= " group by RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID,ROUTE_NAME,RPT_Round,PLANT_Order" & vbNewLine
                SQL &= " order by EQ desc,PLANT_Order,RPT_Year,ROUTE_ID,RPT_Round,RPT_CODE" & vbNewLine
            Case EIR_BL.Report_Type.Lube_Oil_Report
                SQL &= "select 0 as Row,'Lube Oil'as EQ,HD.RPT_Year,dbo.UDF_RPT_Code(DT.RPT_Year, DT.RPT_No) AS RPT_CODE," & vbNewLine
                SQL &= "DT.PLANT_ID,PLANT_Name," & vbNewLine
                SQL &= "0 ROUTE_ID,'' ROUTE_NAME,0 RPT_Round,PLANT_Order,0 ClassC,0 ClassB," & vbNewLine
                SQL &= "SUM(CASE WHEN TAN_Value>=1.5 OR " & vbNewLine
                SQL &= "(DT.Oil_Cat=1 AND Ox_Value>=30) OR" & vbNewLine
                SQL &= "(DT.Oil_Cat=2 AND Ox_Value<=30) OR " & vbNewLine
                SQL &= "VANISH_Value>30 OR" & vbNewLine
                SQL &= "(DT.Oil_Cat=1 AND WATER_Value>=750) OR" & vbNewLine
                SQL &= "(DT.Oil_Cat=2 AND WATER_Value>=1500) " & vbNewLine
                SQL &= "THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= "FROM RPT_LO_Header HD LEFT JOIN RPT_LO_Detail DT " & vbNewLine
                SQL &= "ON HD.RPT_Year = DT.RPT_Year AND HD.RPT_No = DT.RPT_No " & vbNewLine
                SQL &= "LEFT JOIN MS_Plant ON DT.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
                SQL &= "WHERE HD.RPT_No > 0 AND CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= "CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T" & vbNewLine
                SQL &= "group by HD.RPT_Year,dbo.UDF_RPT_Code(DT.RPT_Year, DT.RPT_No),DT.PLANT_ID,PLANT_NAME,PLANT_Order" & vbNewLine
                SQL &= "order by EQ desc,PLANT_Order,RPT_Year,RPT_CODE" & vbNewLine

        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function SearchTagClassPlant(ByVal PlantID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL &= " Declare @PlantID as int;select @PlantID = " & PlantID & ";" & vbNewLine
        SQL &= " Declare @Date_F as varchar(8); " & vbNewLine
        SQL &= " Select @Date_F = Convert(varchar(4)," & Year_F & ") + Right('0' +  Convert(varchar(2)," & Month_F & "),2) + '01';" & vbNewLine
        SQL &= " Declare @Date_T as varchar(8); " & vbNewLine
        SQL &= " Select @Date_T = Convert(varchar(4)," & Year_T & ") + Right('0' +  Convert(varchar(2)," & Month_T & "),2) + '32';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select ROUTE_ID,ROUTE_NAME," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " ISSUE = " & REPORT_ISSUE.New_Problem & " and PLANT_ID = @PlantID" & vbNewLine
                SQL &= " group by ROUTE_ID,ROUTE_NAME" & vbNewLine
                SQL &= " order by ROUTE_ID" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select ROUTE_ID,ROUTE_NAME," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " ISSUE = " & REPORT_ISSUE.New_Problem & " and PLANT_ID = @PlantID" & vbNewLine
                SQL &= " group by ROUTE_ID,ROUTE_NAME" & vbNewLine
                SQL &= " order by ROUTE_ID" & vbNewLine
                'Case 3
                'SQL &= " select ROUTE_ID,ROUTE_NAME,SUM(ClassC) ClassC,SUM(ClassB) ClassB,SUM(ClassA) ClassA" & vbNewLine
                'SQL &= " from (" & vbNewLine
                'SQL &= " 	select ROUTE_ID,ROUTE_NAME," & vbNewLine
                'SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                'SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                'SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                'SQL &= " 	from VW_DASHBOARD_ST" & vbNewLine
                'SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                'SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                'SQL &= " 	ISSUE = 1 and PLANT_ID = @PlantID" & vbNewLine
                'SQL &= " 	group by ROUTE_ID,ROUTE_NAME" & vbNewLine
                'SQL &= " 	union all" & vbNewLine
                'SQL &= " 	select ROUTE_ID,ROUTE_NAME," & vbNewLine
                'SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                'SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                'SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                'SQL &= " 	from VW_DASHBOARD_RO" & vbNewLine
                'SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                'SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                'SQL &= " 	ISSUE = 1 and PLANT_ID = @PlantID" & vbNewLine
                'SQL &= " 	group by ROUTE_ID,ROUTE_NAME" & vbNewLine
                'SQL &= " ) as ALL_Equipment" & vbNewLine
                'SQL &= " group by ROUTE_ID,ROUTE_NAME" & vbNewLine
                'SQL &= " order by ROUTE_ID" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function SearchTagClassListTagPlant(ByVal PlantID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        SQL &= " Declare @PlantID as int;select @PlantID = " & PlantID & ";" & vbNewLine
        SQL &= " Declare @Date_F as varchar(8); " & vbNewLine
        SQL &= " Select @Date_F = Convert(varchar(4)," & Year_F & ") + Right('0' +  Convert(varchar(2)," & Month_F & "),2) + '01';" & vbNewLine
        SQL &= " Declare @Date_T as varchar(8); " & vbNewLine
        SQL &= " Select @Date_T = Convert(varchar(4)," & Year_T & ") + Right('0' +  Convert(varchar(2)," & Month_T & "),2) + '32';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select 0 as Row,'Stationary'as EQ,RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID," & vbNewLine
                SQL &= " ROUTE_NAME,RPT_Round,PLANT_Order," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) as ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) as ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) as ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and ISSUE = " & REPORT_ISSUE.New_Problem & vbNewLine
                SQL &= " AND PLANT_ID = @PlantID" & vbNewLine
                SQL &= " group by RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID,ROUTE_NAME,RPT_Round,PLANT_Order" & vbNewLine
                SQL &= " order by EQ desc,PLANT_Order,RPT_Year,ROUTE_ID,RPT_Round,RPT_CODE" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select 0 as Row,'Rotating'as EQ,RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID," & vbNewLine
                SQL &= " ROUTE_NAME,RPT_Round,PLANT_Order," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) as ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) as ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) as ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and ISSUE = " & REPORT_ISSUE.New_Problem & vbNewLine
                SQL &= " AND PLANT_ID = @PlantID" & vbNewLine
                SQL &= " group by RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID,ROUTE_NAME,RPT_Round,PLANT_Order" & vbNewLine
                SQL &= " order by EQ desc,PLANT_Order,RPT_Year,ROUTE_ID,RPT_Round,RPT_CODE" & vbNewLine
                'Case 3
                'SQL &= " select 0 as Row,'Stationary'as EQ,RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID," & vbNewLine
                'SQL &= " ROUTE_NAME,RPT_Round,PLANT_Order," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) as ClassC," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) as ClassB," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) as ClassA" & vbNewLine
                'SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                'SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                'SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and ISSUE = 1" & vbNewLine
                'SQL &= " AND PLANT_ID = @PlantID" & vbNewLine
                'SQL &= " group by RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID,ROUTE_NAME,RPT_Round,PLANT_Order" & vbNewLine
                'SQL &= " Union All" & vbNewLine
                'SQL &= " select 0 as Row,'Rotating'as EQ,RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID," & vbNewLine
                'SQL &= " ROUTE_NAME,RPT_Round,PLANT_Order," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) as ClassC," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) as ClassB," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) as ClassA" & vbNewLine
                'SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                'SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                'SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and ISSUE = 1" & vbNewLine
                'SQL &= " AND PLANT_ID = @PlantID" & vbNewLine
                'SQL &= " group by RPT_Year,RPT_CODE,PLANT_ID,PLANT_NAME,ROUTE_ID,ROUTE_NAME,RPT_Round,PLANT_Order" & vbNewLine
                'SQL &= " order by EQ desc,PLANT_Order,RPT_Year,ROUTE_ID,RPT_Round,RPT_CODE" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function NewProblemPlantAllYear(ByVal PlantID As Integer, ByVal YY As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        SQL &= " Declare @PlantID as int;select @PlantID = " & PlantID & ";" & vbNewLine
        SQL &= " Declare @SDATE as datetime= '" & YY & "-01-01';" & vbNewLine
        SQL &= " Declare @EDATE as datetime= '" & (YY + 1) & "-01-01';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select DATEPART(MM,RPT_Period_Start) MM," & vbLf
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) as ClassC," & vbLf
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) as ClassB," & vbLf
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) as ClassA" & vbLf
                SQL &= " from VW_DASHBOARD_ST" & vbLf
                SQL &= " where RPT_Period_Start BETWEEN @SDATE AND @EDATE and ISSUE = " & REPORT_ISSUE.New_Problem & vbLf
                SQL &= " AND PLANT_ID = @PlantID" & vbLf
                SQL &= " group by DATEPART(MM,RPT_Period_Start)" & vbLf
                SQL &= " order by DATEPART(MM,RPT_Period_Start)" & vbLf
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select DATEPART(MM,RPT_Period_Start) MM," & vbLf
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) as ClassC," & vbLf
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) as ClassB," & vbLf
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) as ClassA" & vbLf
                SQL &= " from VW_DASHBOARD_RO" & vbLf
                SQL &= " where RPT_Period_Start BETWEEN @SDATE AND @EDATE and ISSUE = " & REPORT_ISSUE.New_Problem & vbLf
                SQL &= " AND PLANT_ID = @PlantID" & vbLf
                SQL &= " group by DATEPART(MM,RPT_Period_Start)" & vbLf
                SQL &= " order by DATEPART(MM,RPT_Period_Start)" & vbLf
                'Case 3
                'SQL &= " select DATEPART(MM,RPT_Period_Start) MM," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) as ClassC," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) as ClassB," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) as ClassA" & vbNewLine
                'SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                'SQL &= " where RPT_Period_Start BETWEEN @SDATE AND @EDATE and ISSUE = 1" & vbNewLine
                'SQL &= " AND PLANT_ID = @PlantID" & vbNewLine
                'SQL &= " group by DATEPART(MM,RPT_Period_Start)" & vbNewLine
                'SQL &= " Union All" & vbNewLine
                'SQL &= " select DATEPART(MM,RPT_Period_Start) MM," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) as ClassC," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) as ClassB," & vbNewLine
                'SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) as ClassA" & vbNewLine
                'SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                'SQL &= " where RPT_Period_Start BETWEEN @SDATE AND @EDATE and ISSUE = 1" & vbNewLine
                'SQL &= " AND PLANT_ID = @PlantID" & vbNewLine
                'SQL &= " group by DATEPART(MM,RPT_Period_Start)" & vbNewLine
                'SQL &= " order by DATEPART(MM,RPT_Period_Start)" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function
#End Region

#Region "Summary Report"
    Function SummaryReport(ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        SQL &= " Declare @Date_F as varchar(8); " & vbNewLine
        SQL &= " Select @Date_F = Convert(varchar(4)," & Year_F & ") + Right('0' +  Convert(varchar(2)," & Month_F & "),2) + '01';" & vbNewLine
        SQL &= " Declare @Date_T as varchar(8); " & vbNewLine
        SQL &= " Select @Date_T = Convert(varchar(4)," & Year_T & ") + Right('0' +  Convert(varchar(2)," & Month_T & "),2) + '32';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                SQL &= " isnull(ClassC,0) ClassC," & vbNewLine
                SQL &= " isnull(ClassB,0) ClassB," & vbNewLine
                SQL &= " isnull(ClassA,0) ClassA" & vbNewLine
                SQL &= " from MS_Plant" & vbNewLine
                SQL &= " left join" & vbNewLine
                SQL &= " (" & vbNewLine
                SQL &= " 	select PLANT_ID," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " 	from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T" & vbNewLine
                SQL &= " 	group by PLANT_ID" & vbNewLine
                SQL &= " ) ST on MS_Plant.PLANT_ID = ST.PLANT_ID" & vbNewLine
                SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                SQL &= " isnull(ClassC,0) ClassC," & vbNewLine
                SQL &= " isnull(ClassB,0) ClassB," & vbNewLine
                SQL &= " isnull(ClassA,0) ClassA" & vbNewLine
                SQL &= " from MS_Plant" & vbNewLine
                SQL &= " left join" & vbNewLine
                SQL &= " (" & vbNewLine
                SQL &= " 	select PLANT_ID," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " 	from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T" & vbNewLine
                SQL &= " 	group by PLANT_ID" & vbNewLine
                SQL &= " ) RO on MS_Plant.PLANT_ID = RO.PLANT_ID" & vbNewLine
                SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
                'Case 3
                '    SQL &= " Select MS_Plant.PLANT_ID,PLANT_Name," & vbNewLine
                '    SQL &= " isnull(ClassC,0) ClassC," & vbNewLine
                '    SQL &= " isnull(ClassB,0) ClassB," & vbNewLine
                '    SQL &= " isnull(ClassA,0) ClassA" & vbNewLine
                '    SQL &= " from MS_Plant" & vbNewLine
                '    SQL &= " left join" & vbNewLine
                '    SQL &= " ("
                '    SQL &= " 	select PLANT_ID,SUM(ClassC) ClassC,SUM(ClassB) ClassB,SUM(ClassA) ClassA" & vbNewLine
                '    SQL &= " 	from (" & vbNewLine
                '    SQL &= " 		select PLANT_ID," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 		from VW_DASHBOARD_ST" & vbNewLine
                '    SQL &= " 		where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                '    SQL &= " 		CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T" & vbNewLine
                '    SQL &= " 		group by PLANT_ID" & vbNewLine
                '    SQL &= " 		union all" & vbNewLine
                '    SQL &= " 		select PLANT_ID," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 		SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 		from VW_DASHBOARD_RO" & vbNewLine
                '    SQL &= " 		where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                '    SQL &= " 		CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T" & vbNewLine
                '    SQL &= " 		group by PLANT_ID" & vbNewLine
                '    SQL &= " 	) as ALL_Equipment" & vbNewLine
                '    SQL &= " 	group by PLANT_ID" & vbNewLine
                '    SQL &= " ) ALL_EQ on MS_Plant.PLANT_ID = ALL_EQ.PLANT_ID" & vbNewLine
                '    SQL &= " order by MS_Plant.PLANT_Order" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function SummaryReportPlant(ByVal PlantID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL &= " Declare @PlantID as int;select @PlantID = " & PlantID & ";" & vbNewLine
        SQL &= " Declare @Date_F as varchar(8); " & vbNewLine
        SQL &= " Select @Date_F = Convert(varchar(4)," & Year_F & ") + Right('0' +  Convert(varchar(2)," & Month_F & "),2) + '01';" & vbNewLine
        SQL &= " Declare @Date_T as varchar(8); " & vbNewLine
        SQL &= " Select @Date_T = Convert(varchar(4)," & Year_T & ") + Right('0' +  Convert(varchar(2)," & Month_T & "),2) + '32';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select ROUTE_ID,ROUTE_NAME," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " PLANT_ID = @PlantID" & vbNewLine
                SQL &= " group by ROUTE_ID,ROUTE_NAME" & vbNewLine
                SQL &= " order by ROUTE_ID" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select ROUTE_ID,ROUTE_NAME," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " PLANT_ID = @PlantID" & vbNewLine
                SQL &= " group by ROUTE_ID,ROUTE_NAME" & vbNewLine
                SQL &= " order by ROUTE_ID" & vbNewLine
                'Case 3
                '    SQL &= " select ROUTE_ID,ROUTE_NAME,SUM(ClassC) ClassC,SUM(ClassB) ClassB,SUM(ClassA) ClassA" & vbNewLine
                '    SQL &= " from (" & vbNewLine
                '    SQL &= " 	select ROUTE_ID,ROUTE_NAME," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 	from VW_DASHBOARD_ST" & vbNewLine
                '    SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                '    SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                '    SQL &= " 	PLANT_ID = @PlantID" & vbNewLine
                '    SQL &= " 	group by ROUTE_ID,ROUTE_NAME" & vbNewLine
                '    SQL &= " 	union all" & vbNewLine
                '    SQL &= " 	select ROUTE_ID,ROUTE_NAME," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 	from VW_DASHBOARD_RO" & vbNewLine
                '    SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                '    SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                '    SQL &= " 	PLANT_ID = @PlantID" & vbNewLine
                '    SQL &= " 	group by ROUTE_ID,ROUTE_NAME" & vbNewLine
                '    SQL &= " ) as ALL_Equipment" & vbNewLine
                '    SQL &= " group by ROUTE_ID,ROUTE_NAME" & vbNewLine
                '    SQL &= " order by ROUTE_ID" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function SummaryReportYear(ByVal PlantID As Integer, ByVal YY As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL &= " Declare @PlantID as int = " & PlantID & ";" & vbNewLine
        SQL &= " Declare @SDATE as datetime= '" & YY & "-01-01';" & vbNewLine
        SQL &= " Declare @EDATE as datetime= '" & (YY + 1) & "-01-01';" & vbNewLine

        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select ROUTE_ID,ROUTE_NAME,DATEPART(MM,RPT_Period_Start) MM," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " where RPT_Period_Start BETWEEN @SDATE AND @EDATE and" & vbNewLine
                SQL &= " PLANT_ID = @PlantID" & vbNewLine
                SQL &= " group by ROUTE_ID,ROUTE_NAME,DATEPART(MM,RPT_Period_Start)" & vbNewLine
                SQL &= " order by ROUTE_ID" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select ROUTE_ID,ROUTE_NAME,DATEPART(MM,RPT_Period_Start) MM," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " where RPT_Period_Start BETWEEN @SDATE AND @EDATE and" & vbNewLine
                SQL &= " PLANT_ID = @PlantID" & vbNewLine
                SQL &= " group by ROUTE_ID,ROUTE_NAME,DATEPART(MM,RPT_Period_Start)" & vbNewLine
                SQL &= " order by ROUTE_ID" & vbNewLine
                'Case 3
                '    SQL &= " select ROUTE_ID,ROUTE_NAME,MM,SUM(ClassC) ClassC,SUM(ClassB) ClassB,SUM(ClassA) ClassA" & vbNewLine
                '    SQL &= " from (" & vbNewLine
                '    SQL &= " 	select ROUTE_ID,ROUTE_NAME,DATEPART(MM,RPT_Period_Start) MM," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 	from VW_DASHBOARD_ST" & vbNewLine
                '    SQL &= "    where RPT_Period_Start BETWEEN @SDATE AND @EDATE and" & vbNewLine
                '    SQL &= " 	PLANT_ID = @PlantID" & vbNewLine
                '    SQL &= " 	group by ROUTE_ID,ROUTE_NAME,DATEPART(MM,RPT_Period_Start)" & vbNewLine
                '    SQL &= " 	union all" & vbNewLine
                '    SQL &= " 	select ROUTE_ID,ROUTE_NAME,DATEPART(MM,RPT_Period_Start) MM," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 	from VW_DASHBOARD_RO" & vbNewLine
                '    SQL &= "    where RPT_Period_Start BETWEEN @SDATE AND @EDATE and" & vbNewLine
                '    SQL &= " 	PLANT_ID = @PlantID" & vbNewLine
                '    SQL &= " 	group by ROUTE_ID,ROUTE_NAME,DATEPART(MM,RPT_Period_Start)" & vbNewLine
                '    SQL &= " ) as ALL_Equipment" & vbNewLine
                '    SQL &= " group by ROUTE_ID,ROUTE_NAME,MM" & vbNewLine
                '    SQL &= " order by ROUTE_ID" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function

    Function SummaryReportRoute(ByVal PlantID As Integer, ByVal RouteID As Integer, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As EIR_BL.Report_Type) As DataTable
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL &= " Declare @PlantID as int;select @PlantID = " & PlantID & ";" & vbNewLine
        SQL &= " Declare @RouteID as int;select @RouteID = " & RouteID & ";" & vbNewLine
        SQL &= " Declare @Date_F as varchar(8); " & vbNewLine
        SQL &= " Select @Date_F = Convert(varchar(4)," & Year_F & ") + Right('0' +  Convert(varchar(2)," & Month_F & "),2) + '01';" & vbNewLine
        SQL &= " Declare @Date_T as varchar(8); " & vbNewLine
        SQL &= " Select @Date_T = Convert(varchar(4)," & Year_T & ") + Right('0' +  Convert(varchar(2)," & Month_T & "),2) + '32';" & vbNewLine
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                SQL &= " select RPT_Round," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_ST" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " PLANT_ID = @PlantID and ROUTE_ID = @RouteID" & vbNewLine
                SQL &= " group by RPT_Round" & vbNewLine
                SQL &= " order by RPT_Round" & vbNewLine
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                SQL &= " select RPT_Round," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                SQL &= " SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                SQL &= " from VW_DASHBOARD_RO" & vbNewLine
                SQL &= " where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                SQL &= " CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                SQL &= " PLANT_ID = @PlantID and ROUTE_ID = @RouteID" & vbNewLine
                SQL &= " group by RPT_Round" & vbNewLine
                SQL &= " order by RPT_Round" & vbNewLine
                'Case 3
                '    SQL &= " select RPT_Round,SUM(ClassC) ClassC,SUM(ClassB) ClassB,SUM(ClassA) ClassA" & vbNewLine
                '    SQL &= " from (" & vbNewLine
                '    SQL &= " 	select RPT_Round," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 	from VW_DASHBOARD_ST" & vbNewLine
                '    SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                '    SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                '    SQL &= " 	PLANT_ID = @PlantID and ROUTE_ID = @RouteID" & vbNewLine
                '    SQL &= " 	group by RPT_Round" & vbNewLine
                '    SQL &= " 	union all" & vbNewLine
                '    SQL &= " 	select RPT_Round," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 1 THEN 1 ELSE 0 END) ClassC," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 2 THEN 1 ELSE 0 END) ClassB," & vbNewLine
                '    SQL &= " 	SUM(CASE WHEN ICLS_ID = 3 THEN 1 ELSE 0 END) ClassA" & vbNewLine
                '    SQL &= " 	from VW_DASHBOARD_RO" & vbNewLine
                '    SQL &= " 	where CONVERT(varchar(8),RPT_Period_Start,112) >= @Date_F and" & vbNewLine
                '    SQL &= " 	CONVERT(varchar(8),RPT_Period_Start,112) <= @Date_T and" & vbNewLine
                '    SQL &= " 	PLANT_ID = @PlantID and ROUTE_ID = @RouteID" & vbNewLine
                '    SQL &= " 	group by RPT_Round" & vbNewLine
                '    SQL &= " ) as ALL_Equipment" & vbNewLine
                '    SQL &= " group by RPT_Round" & vbNewLine
                '    SQL &= " order by RPT_Round" & vbNewLine
        End Select
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Return DT
    End Function
#End Region


    Function FixDate(ByVal Input As DateTime) As String
        Dim Year As Int32 = Input.Year
        If Input.Year > 2500 Then
            Year = Input.Year - 543
        End If
        Return Year.ToString & Input.Month.ToString.PadLeft(2, "0") & Input.Day.ToString.PadLeft(2, "0")
    End Function

    Public Enum REPORT_ISSUE
        All = -1
        Normal = 0  'ไม่มีปัญหา
        New_Problem = 1  'ปัญหาที่เกิดขึ้นใหม่
        Not_Fixed = 2  'ปัญหาที่ยังไม่ได้รับการแก้ไข
        Fixed_Completed = 3  'ปัญหาที่แก้ไขสมบูรณ์
        Fixed_Incompleted = 4  'ปัญหาที่แก้ไขไม่สมบูรณ์
    End Enum

    Public Const ZeroDate As DateTime = #1/1/1900#

    ''' <param name="Equipment">ประเภทอุปกรณ์</param>
    ''' <param name="PLANT_ID">รหัส Plant</param>
    ''' <param name="TAG_ID">รหัส TAG</param>
    ''' <param name="Plan_Date_From">วันที่แผนการตรวจ</param>
    ''' <param name="Plan_Date_To">วันที่แผนการตรวจ</param>
    ''' <param name="Inspected_Date">วันที่ตรวจเสร็จ</param>
    ''' <param name="ISSUE">สถานะการแก้ไขปัญหา</param>
    ''' <param name="ICLS_Level">ความรุนแรง Class A,B,C,D...</param>
    ''' <returns>ตารางที่ดึงค่าไปแสดงผลที่ Dashboard โดยต้องส่งค่าสอดคล้องกับ Parameter ที่ส่งมาให้กรองทั้งหมด</returns>
    Function GetDashboardData(ByVal Equipment As EIR_BL.Report_Type, Optional ByVal PLANT_ID As Integer = 0, Optional ByVal TAG_ID As Integer = 0, Optional ByVal Plan_Date_From As DateTime = ZeroDate, Optional ByVal Plan_Date_To As DateTime = ZeroDate, Optional ByVal Inspected_Date As DateTime = ZeroDate, Optional ByVal ISSUE As REPORT_ISSUE = REPORT_ISSUE.All, Optional ByVal ICLS_Level As EIR_BL.InspectionLevel = EIR_BL.InspectionLevel.All, Optional ByVal Equipment_ST_And_RO As Boolean = False, Optional ByVal AREA_ID As Integer = 0) As DataTable
        '--------------------- Function มันต้องรับ Parameter ได้ตามข้างบน ไม่งั้นมันก็ดึงมาทั้งฐานข้อมูล และฝั่ง UI ต้องมา WHERE เองทั้งหมด ------------------

        Dim ST As String = ""
        Dim RO As String = ""
        Dim LO As String = ""
        Dim PDMA As String = ""
        Dim THM As String = ""
        Dim ALL As String = ""
        Dim ST_And_RO As String = ""

        Dim Filter As String = " AND "
        If PLANT_ID > 0 Then
            Filter = Filter & " PLANT_ID = " & PLANT_ID & " AND"
        End If
        If TAG_ID > 0 Then
            Filter = Filter & " TAG_ID = " & TAG_ID & " AND"
        End If
        If AREA_ID > 0 Then
            Filter = Filter & " AREA_ID = " & AREA_ID & " AND"
        End If
        If Plan_Date_From <> ZeroDate Then
            Filter = Filter & " CONVERT(VARCHAR(8),RPT_Period_Start,112) >= " & FixDate(Plan_Date_From) & " AND"
        End If
        If Plan_Date_To <> ZeroDate Then
            Filter = Filter & " CONVERT(VARCHAR(8),RPT_Period_Start,112) <= " & FixDate(Plan_Date_To) & " AND"
        End If
        If Inspected_Date <> ZeroDate Then
            Filter = Filter & " CONVERT(VARCHAR(8),Inspected_Date,112) = " & FixDate(Inspected_Date) & " AND"
        End If
        Select Case ISSUE
            Case REPORT_ISSUE.Normal
                Filter = Filter & " ISSUE = " & REPORT_ISSUE.Normal & " AND"
            Case REPORT_ISSUE.New_Problem
                Filter = Filter & " ISSUE = " & REPORT_ISSUE.New_Problem & " AND"
            Case REPORT_ISSUE.Not_Fixed
                Filter = Filter & " ISSUE = " & REPORT_ISSUE.Not_Fixed & " AND"
            Case REPORT_ISSUE.Fixed_Completed
                Filter = Filter & " ISSUE = " & REPORT_ISSUE.Fixed_Completed & " AND"
            Case REPORT_ISSUE.Fixed_Incompleted
                Filter = Filter & " ISSUE = " & REPORT_ISSUE.Fixed_Incompleted & " AND"
        End Select
        Select Case ICLS_Level
            Case EIR_BL.InspectionLevel.Normal
                Filter = Filter & " ICLS_ID = " & EIR_BL.InspectionLevel.Normal & " AND"
            Case EIR_BL.InspectionLevel.ClassC
                Filter = Filter & " ICLS_ID = " & EIR_BL.InspectionLevel.ClassC & " AND"
            Case EIR_BL.InspectionLevel.ClassB
                Filter = Filter & " ICLS_ID = " & EIR_BL.InspectionLevel.ClassB & " AND"
            Case EIR_BL.InspectionLevel.ClassA
                Filter = Filter & " ICLS_ID = " & EIR_BL.InspectionLevel.ClassA & " AND"
        End Select
        If Filter <> " AND " Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        Else
            Filter = ""
        End If

        'Select Case Equipment 'ถ้าส่ง Equipment ก็เชคตามเงื่อนไขที่ส่งมา

        '    Case EIR_BL.Report_Type.Stationary_Routine_Report
        '        '--------- ดึงอะไรจาก Stationary_Routine_Report ก็เขียนในนี้ + Where ตามเงื่อนไข Parameter ที่ส่งมาด้วย -------------

        '    Case EIR_BL.Report_Type.Rotating_Routine_Report
        '        '--------- ดึงอะไรจาก Rotating_Routine_Report ก็เขียนในนี้ + Where ตามเงื่อนไข Parameter ที่ส่งมาด้วย-------------

        '    Case EIR_BL.Report_Type.Stationary_Off_Routine_Report
        '        '--------- ดึงอะไรจาก Stationary_Off_Routine_Report ก็เขียนในนี้ + Where ตามเงื่อนไข Parameter ที่ส่งมาด้วย-------------

        '    Case EIR_BL.Report_Type.Rotating_Off_Routine_Report
        '        '--------- ดึงอะไรจาก Rotating_Off_Routine_Report ก็เขียนในนี้ + Where ตามเงื่อนไข Parameter ที่ส่งมาด้วย-------------

        '    Case EIR_BL.Report_Type.Spring_Hanger_Report
        '        '--------- ดึงอะไรจาก Spring_Hanger_Report ก็เขียนในนี้ + Where ตามเงื่อนไข Parameter ที่ส่งมาด้วย-------------

        '    Case EIR_BL.Report_Type.Lube_Oil_Report
        '        '--------- ดึงอะไรจาก Lube_Oil_Report ก็เขียนในนี้ + Where ตามเงื่อนไข Parameter ที่ส่งมาด้วย-------------

        '    Case EIR_BL.Report_Type.PdMA_Report
        '        '--------- ดึงอะไรจาก PdMA_Report ก็เขียนในนี้ + Where ตามเงื่อนไข Parameter ที่ส่งมาด้วย-------------

        '    Case EIR_BL.Report_Type.Thermography_Report
        '        '--------- ดึงอะไรจาก Thermography_Report ก็เขียนในนี้ + Where ตามเงื่อนไข Parameter ที่ส่งมาด้วย-------------

        '    Case EIR_BL.Report_Type.All
        '        '---------- ถ้าต้องการทุกอุปกรณ์ ก็เขียนในนี้ -------------- + Where ตามเงื่อนไข Parameter ที่ส่งมาด้วย

        'End Select

        ST &= "SELECT " & EIR_BL.Report_Type.Stationary_Routine_Report & " AS RPT_TYPE_ID,'' AS RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,AREA_ID,AREA_Name," & vbNewLine
        ST &= "INSPECTED_DATE,RPT_Period_Start,INSP_Name INSPECTION,ISSUE,ICLS_ID,ICLS_Description,STATUS_Name,Detail" & vbNewLine
        ST &= "FROM VW_DASHBOARD_ST WHERE 1=1 " & Filter & vbNewLine

        RO &= "SELECT " & EIR_BL.Report_Type.Rotating_Routine_Report & " AS RPT_TYPE_ID,'' AS RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,AREA_ID,AREA_Name," & vbNewLine
        RO &= "INSPECTED_DATE,RPT_Period_Start,INSP_Name INSPECTION,ISSUE,ICLS_ID,ICLS_Description,STATUS_Name,Detail" & vbNewLine
        RO &= "FROM VW_DASHBOARD_RO WHERE 1=1 " & Filter & vbNewLine

        LO &= "SELECT * FROM (" & vbNewLine
        LO &= "SELECT " & EIR_BL.Report_Type.Lube_Oil_Report & " AS RPT_TYPE_ID,'' AS RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,LO_TAG_ID TAG_ID,LO_TAG_NO TAG_Code,PLANT_ID,PLANT_CODE PLANT_Name,0 AREA_ID,'' AREA_Name,INSPECTED_DATE" & vbNewLine
        LO &= ",RPT_Period_Start,'TAN' INSPECTION,TAN_ISSUE ISSUE,CASE WHEN TAN_Abnormal = 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        LO &= "CASE WHEN CASE WHEN TAN_Abnormal = 1 THEN 3 ELSE 0 END = 0 THEN 'Ok' ELSE 'Abnormal' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        LO &= "FROM VW_DASHBOARD_LO" & vbNewLine
        LO &= "UNION ALL" & vbNewLine
        LO &= "SELECT " & EIR_BL.Report_Type.Lube_Oil_Report & " AS RPT_TYPE_ID,'' AS RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,LO_TAG_ID TAG_ID,LO_TAG_NO TAG_Code,PLANT_ID,PLANT_CODE PLANT_Name,0 AREA_ID,'' AREA_Name,INSPECTED_DATE" & vbNewLine
        LO &= ",RPT_Period_Start,'OX' INSPECTION,OX_ISSUE ISSUE,CASE WHEN OX_Abnormal = 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        LO &= "CASE WHEN CASE WHEN OX_Abnormal = 1 THEN 3 ELSE 0 END = 0 THEN 'Ok' ELSE 'Abnormal' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        LO &= "FROM VW_DASHBOARD_LO" & vbNewLine
        LO &= "UNION ALL" & vbNewLine
        LO &= "SELECT " & EIR_BL.Report_Type.Lube_Oil_Report & " AS RPT_TYPE_ID,'' AS RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,LO_TAG_ID TAG_ID,LO_TAG_NO TAG_Code,PLANT_ID,PLANT_CODE PLANT_Name,0 AREA_ID,'' AREA_Name,INSPECTED_DATE" & vbNewLine
        LO &= ",RPT_Period_Start,'WATER' INSPECTION,WATER_ISSUE ISSUE,CASE WHEN WATER_Abnormal = 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        LO &= "CASE WHEN CASE WHEN WATER_Abnormal = 1 THEN 3 ELSE 0 END = 0 THEN 'Ok' ELSE 'Abnormal' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        LO &= "FROM VW_DASHBOARD_LO" & vbNewLine
        LO &= "UNION ALL" & vbNewLine
        LO &= "SELECT " & EIR_BL.Report_Type.Lube_Oil_Report & " AS RPT_TYPE_ID,'' AS RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,LO_TAG_ID TAG_ID,LO_TAG_NO TAG_Code,PLANT_ID,PLANT_CODE PLANT_Name,0 AREA_ID,'' AREA_Name,INSPECTED_DATE" & vbNewLine
        LO &= ",RPT_Period_Start,'VARNISH' INSPECTION,VANISH_ISSUE ISSUE,CASE WHEN VANISH_Abnormal = 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        LO &= "CASE WHEN CASE WHEN VANISH_Abnormal = 1 THEN 3 ELSE 0 END = 0 THEN 'Ok' ELSE 'Abnormal' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        LO &= "FROM VW_DASHBOARD_LO" & vbNewLine
        LO &= ") LO WHERE 1=1 " & Filter & vbNewLine

        PDMA &= "SELECT * FROM (SELECT " & EIR_BL.Report_Type.PdMA_Report & " AS RPT_TYPE_ID,RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,0 AREA_ID,'' AREA_Name,INSPEC_DATE INSPECTED_DATE" & vbNewLine
        PDMA &= ",RPT_Period_Start,'PWQ' INSPECTION,PWQ_ISSUE ISSUE,CASE WHEN PWQ_Abnormal = 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        PDMA &= "CASE WHEN CASE WHEN PWQ_Abnormal = 1 THEN 3 ELSE 0 END = 0 THEN 'Ok' ELSE 'Abnormal' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        PDMA &= "FROM VW_DASHBOARD_PDMA ) TB WHERE 1=1 " & Filter & vbNewLine
        PDMA &= "UNION ALL" & vbNewLine
        PDMA &= "SELECT * FROM (SELECT " & EIR_BL.Report_Type.PdMA_Report & " AS RPT_TYPE_ID,RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,0 AREA_ID,'' AREA_Name,INSPEC_DATE INSPECTED_DATE" & vbNewLine
        PDMA &= ",RPT_Period_Start,'INS' INSPECTION,INS_ISSUE ISSUE,CASE WHEN INS_Abnormal = 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        PDMA &= "CASE WHEN CASE WHEN INS_Abnormal = 1 THEN 3 ELSE 0 END = 0 THEN 'Ok' ELSE 'Abnormal' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        PDMA &= "FROM VW_DASHBOARD_PDMA ) TB WHERE 1=1 " & Filter & vbNewLine
        PDMA &= "UNION ALL" & vbNewLine
        PDMA &= "SELECT * FROM (SELECT " & EIR_BL.Report_Type.PdMA_Report & " AS RPT_TYPE_ID,RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,0 AREA_ID,'' AREA_Name,INSPEC_DATE INSPECTED_DATE" & vbNewLine
        PDMA &= ",RPT_Period_Start,'PWC' INSPECTION,PWC_ISSUE ISSUE,CASE WHEN PWC_Abnormal = 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        PDMA &= "CASE WHEN CASE WHEN PWC_Abnormal = 1 THEN 3 ELSE 0 END = 0 THEN 'Ok' ELSE 'Abnormal' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        PDMA &= "FROM VW_DASHBOARD_PDMA ) TB WHERE 1=1 " & Filter & vbNewLine
        PDMA &= "UNION ALL" & vbNewLine
        PDMA &= "SELECT * FROM (SELECT " & EIR_BL.Report_Type.PdMA_Report & " AS RPT_TYPE_ID,RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,0 AREA_ID,'' AREA_Name,INSPEC_DATE INSPECTED_DATE" & vbNewLine
        PDMA &= ",RPT_Period_Start,'STA' INSPECTION,STA_ISSUE ISSUE,CASE WHEN STA_Abnormal = 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        PDMA &= "CASE WHEN CASE WHEN STA_Abnormal = 1 THEN 3 ELSE 0 END = 0 THEN 'Ok' ELSE 'Abnormal' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        PDMA &= "FROM VW_DASHBOARD_PDMA ) TB WHERE 1=1 " & Filter & vbNewLine
        PDMA &= "UNION ALL" & vbNewLine
        PDMA &= "SELECT * FROM (SELECT " & EIR_BL.Report_Type.PdMA_Report & " AS RPT_TYPE_ID,RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,0 AREA_ID,'' AREA_Name,INSPEC_DATE INSPECTED_DATE" & vbNewLine
        PDMA &= ",RPT_Period_Start,'ROT' INSPECTION,ROT_ISSUE ISSUE,CASE WHEN ROT_Abnormal = 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        PDMA &= "CASE WHEN CASE WHEN ROT_Abnormal = 1 THEN 3 ELSE 0 END = 0 THEN 'Ok' ELSE 'Abnormal' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        PDMA &= "FROM VW_DASHBOARD_PDMA ) TB WHERE 1=1 " & Filter & vbNewLine
        PDMA &= "UNION ALL" & vbNewLine
        PDMA &= "SELECT * FROM (SELECT " & EIR_BL.Report_Type.PdMA_Report & " AS RPT_TYPE_ID,RPT_Type,DETAIL_ID,RPT_Year,Month(RPT_Period_Start) RPT_Month,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,0 AREA_ID,'' AREA_Name,INSPEC_DATE INSPECTED_DATE" & vbNewLine
        PDMA &= ",RPT_Period_Start,'AIR' INSPECTION,AIR_ISSUE ISSUE,CASE WHEN AIR_Abnormal = 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        PDMA &= "CASE WHEN CASE WHEN AIR_Abnormal = 1 THEN 3 ELSE 0 END = 0 THEN 'Ok' ELSE 'Abnormal' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        PDMA &= "FROM VW_DASHBOARD_PDMA ) TB WHERE 1=1 " & Filter & vbNewLine

        THM &= "SELECT * FROM (SELECT " & EIR_BL.Report_Type.Thermography_Report & " AS RPT_TYPE_ID,'' AS RPT_Type,DETAIL_ID,RPT_Year,1 RPT_Month,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,0 AREA_ID,'' AREA_Name,RPT_INSP_Date INSPECTED_DATE" & vbNewLine
        THM &= ",CONVERT(DATETIME,CONVERT(VARCHAR(4),RPT_YEAR - 543) + '-01-01') RPT_Period_Start,THM_TYPE_Name INSPECTION,ISSUE,CASE TAG_STATUS WHEN 1 THEN 3 ELSE 0 END ICLS_ID," & vbNewLine
        THM &= "CASE TAG_STATUS WHEN 0 THEN 'Ok' WHEN 1 THEN 'Abnormal' ELSE  'Ok' END ICLS_Description,'' as STATUS_Name,'' as Detail" & vbNewLine
        THM &= "FROM VW_DASHBOARD_THM ) TB WHERE 1=1 " & Filter & vbNewLine

        ALL &= ST & vbNewLine
        ALL &= "UNION ALL" & vbNewLine
        ALL &= RO & vbNewLine
        ALL &= "UNION ALL" & vbNewLine
        ALL &= LO & vbNewLine
        ALL &= "UNION ALL" & vbNewLine
        ALL &= PDMA & vbNewLine
        ALL &= "UNION ALL" & vbNewLine
        ALL &= THM & vbNewLine

        ST_And_RO &= ST & vbNewLine
        ST_And_RO &= "UNION ALL" & vbNewLine
        ST_And_RO &= RO & vbNewLine

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter

        If Equipment_ST_And_RO Then
            DA = New SqlDataAdapter(ST_And_RO, BL.ConnStr)
            DA.Fill(DT)
        Else
            Select Case Equipment
                Case EIR_BL.Report_Type.Stationary_Routine_Report
                    DA = New SqlDataAdapter(ST, BL.ConnStr)
                    DA.Fill(DT)
                Case EIR_BL.Report_Type.Rotating_Routine_Report
                    DA = New SqlDataAdapter(RO, BL.ConnStr)
                    DA.Fill(DT)
                Case EIR_BL.Report_Type.Lube_Oil_Report
                    DA = New SqlDataAdapter(LO, BL.ConnStr)
                    DA.Fill(DT)
                Case EIR_BL.Report_Type.PdMA_MTap_Report, EIR_BL.Report_Type.PdMA_Report
                    DA = New SqlDataAdapter(PDMA, BL.ConnStr)
                    DA.Fill(DT)
                Case EIR_BL.Report_Type.Thermography_Report
                    DA = New SqlDataAdapter(THM, BL.ConnStr)
                    DA.Fill(DT)
                Case EIR_BL.Report_Type.All
                    DA = New SqlDataAdapter(ALL, BL.ConnStr)
                    DA.Fill(DT)
            End Select
        End If

        Return DT
    End Function

#End Region

End Class
