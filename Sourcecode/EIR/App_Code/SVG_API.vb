﻿Imports System.IO
Imports System.Drawing
Imports System.Data
Imports System.Xml

Public Class SVG_API

    Dim C As New Converter

    Public Function GetSVGContent(ByVal Path As String) As String
        Return C.ByteToString(C.StreamToByte(IO.File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)), Converter.EncodeType._UTF8)
    End Function

    Public Function GetSVGDataset(ByVal SVGContent As String) As DataSet

        Dim XMLDoc As New XmlDocument()
        XMLDoc.LoadXml(SVGContent)

        Dim DS As New DataSet
        Dim Canvas As New DataTable
        '--------- Get Canvas Size-----------
        Canvas.TableName = "Canvas"
        Canvas.Columns.Add("width")
        Canvas.Columns.Add("height")
        For Each Node As XmlNode In XMLDoc.ChildNodes()
            If Node.NodeType <> XmlNodeType.Element Then Continue For
            BindCanvasDataTable(Node, Canvas)
        Next

        Dim Filter As New DataTable
        '------------ Get All Configuration Filter--------------
        Filter.TableName = "Filter"
        Filter.Columns.Add("id")
        Filter.Columns.Add("x")
        Filter.Columns.Add("y")
        Filter.Columns.Add("width")
        Filter.Columns.Add("height")
        '----------
        Filter.Columns.Add("feGaussianBlur_stdDeviation")
        Filter.Columns.Add("feGaussianBlur_in")
        For Each Node As XmlNode In XMLDoc.ChildNodes()
            If Node.NodeType <> XmlNodeType.Element Then Continue For
            BindFilterDataTable(Node, Filter)
        Next

        Dim Element As New DataTable
        Element.TableName = "Element"
        Element.Columns.Add("id")
        Element.Columns.Add("type")
        For Each Node As XmlNode In XMLDoc.ChildNodes()
            If Node.NodeType <> XmlNodeType.Element Then Continue For
            BindElementDataTable(Node, Element)
        Next

        DS.Tables.Add(Canvas)
        DS.Tables.Add(Filter)
        DS.Tables.Add(Element)

        Return DS
    End Function

    Private Sub BindCanvasDataTable(ByVal Node As XmlNode, ByRef DT As DataTable)
        If Node.Name.ToLower <> "svg" Then Exit Sub
        If IsNothing(Node.Attributes.ItemOf("width")) Or IsNothing(Node.Attributes.ItemOf("height")) Then Exit Sub
        Dim DR As DataRow = DT.NewRow
        DR("width") = Node.Attributes("width").Value
        DR("height") = Node.Attributes("height").Value
        DT.Rows.Add(DR)
    End Sub

    Private Sub BindFilterDataTable(ByVal Node As XmlNode, ByRef DT As DataTable)
        Select Case Node.Name.ToLower
            Case "svg", "defs"
                For i As Integer = 0 To Node.ChildNodes.Count - 1
                    If Node.ChildNodes(i).NodeType <> XmlNodeType.Element Then Continue For
                    BindFilterDataTable(Node.ChildNodes(i), DT)
                Next
            Case "filter"
                Dim DR As DataRow = DT.NewRow
                Dim NeedToBind As Boolean = False
                For i As Integer = 0 To DT.Columns.Count - 1
                    Dim AttName As String = DT.Columns(i).ColumnName
                    If Not IsNothing(Node.Attributes.ItemOf(AttName)) Then
                        DR(AttName) = Node.Attributes(AttName).Value
                        NeedToBind = True
                    End If
                Next
                If NeedToBind Then
                    DT.Rows.Add(DR)
                    '----------------Get GaussianBlur--------------
                    For Each ch As XmlNode In Node.ChildNodes
                        If ch.NodeType <> XmlNodeType.Element Then Continue For
                        Select Case ch.Name.ToLower
                            Case "feGaussianBlur".ToLower
                                If Not IsNothing(ch.Attributes("stdDeviation")) Then
                                    DR("feGaussianBlur_stdDeviation") = ch.Attributes("stdDeviation").Value
                                End If
                                If Not IsNothing(ch.Attributes("in")) Then
                                    DR("feGaussianBlur_in") = ch.Attributes("in").Value
                                End If
                        End Select
                        'feGaussianBlur
                    Next
                End If
        End Select


    End Sub

    Private Sub BindElementDataTable(ByVal Node As XmlNode, ByRef DT As DataTable)
        Select Case Node.Name.ToLower
            Case "svg", "g"
                For i As Integer = 0 To Node.ChildNodes.Count - 1
                    If Node.ChildNodes(i).NodeType <> XmlNodeType.Element Then Continue For
                    BindElementDataTable(Node.ChildNodes(i), DT)
                Next
            Case "rect", "image", "line", "text", "ellipse", "path", "circle"
                '------------ Add All Attributes For Current Node-------------

                If Node.Name.ToLower = "text" Then
                    Dim GG As String = ""
                End If

                For c As Integer = 0 To Node.Attributes.Count - 1
                    Dim X As New XmlNodeReader(Node)
                    'X.ReadContentAs(GetType(String),)

                    If DT.Columns.IndexOf(Node.Attributes(c).Name) = -1 Then
                        DT.Columns.Add(Node.Attributes(c).Name)
                    End If
                Next


                If IsNothing(Node.Attributes("id")) Then Exit Sub
                Dim DR As DataRow = DT.NewRow
                DR("type") = Node.Name '-------- Get Type Of Element---------
                DT.Rows.Add(DR)
                For a As Integer = 0 To DT.Columns.Count - 1
                    Dim AttName As String = DT.Columns(a).ColumnName
                    If Not IsNothing(Node.Attributes.ItemOf(AttName)) Then
                        DR(AttName) = Node.Attributes(AttName).Value
                    End If
                Next
                '----------- Add Custom InnerXML Value-----------
                If Node.Name.ToLower = "text" Then
                    If DT.Columns.IndexOf("text") = -1 Then
                        DT.Columns.Add("text")
                    End If
                    DR("text") = Node.InnerXml
                End If
                If Node.Name.ToLower = "image" Then
                    If DT.Columns.IndexOf("image") = -1 Then
                        DT.Columns.Add("image")
                    End If
                    DR("image") = Node.InnerXml
                End If

            Case "filter", "defs", "title"
                Exit Sub
        End Select
    End Sub

    Private Function ExistsAttribute(ByVal DR_Element As DataRow, ByVal Attribute As String) As Boolean
        If DR_Element.Table.Columns.IndexOf(Attribute) = -1 OrElse
            IsDBNull(DR_Element(Attribute)) OrElse
            Trim(DR_Element(Attribute).ToString) = "" OrElse
            DR_Element(Attribute).ToString.ToLower = "none" OrElse
            DR_Element(Attribute).ToString.ToLower = "null" OrElse
            DR_Element(Attribute).ToString.ToLower = "undefined" OrElse
            (IsNumeric(DR_Element(Attribute).ToString) AndAlso CSng(DR_Element(Attribute)) = 0) Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function SVGToImage(ByVal SVGContent As String) As Image
        Dim DS As DataSet

        Try
            DS = GetSVGDataset(SVGContent)
        Catch ex As Exception
            Return Nothing
        End Try

        Dim _canvas As DataTable = DS.Tables("Canvas")
        Dim Filter As DataTable = DS.Tables("Filter")
        Dim Element As DataTable = DS.Tables("Element")
        If _canvas.Rows.Count = 0 OrElse
            Not ExistsAttribute(_canvas.Rows(0), "width") OrElse
            Not IsNumeric(_canvas.Rows(0)("width")) OrElse
            Not ExistsAttribute(_canvas.Rows(0), "height") OrElse
            Not IsNumeric(_canvas.Rows(0)("height")) Then
            Throw New Exception("Invalid SVG canvas dimension")
            Return Nothing
        End If
        Dim Canvas As New Bitmap(CInt(_canvas.Rows(0)("width")), CInt(_canvas.Rows(0)("height")))

        For i As Integer = 0 To Element.Rows.Count - 1
            Try
                Select Case Element.Rows(i).Item("type").ToString.ToLower
                    Case "line"
                        DrawLine(Canvas, Element.Rows(i), Filter)
                    Case "rect"
                        DrawRect(Canvas, Element.Rows(i), Filter)
                    Case "ellipse"
                        DrawEllipse(Canvas, Element.Rows(i), Filter)
                    Case "circle"
                        DrawCircle(Canvas, Element.Rows(i), Filter)
                    Case "path"
                        DrawPath(Canvas, Element.Rows(i), Filter)
                    Case "text"
                        DrawText(Canvas, Element.Rows(i), Filter)
                    Case "image"
                        DrawImage(Canvas, Element.Rows(i), Filter)
                End Select
            Catch ex As Exception

            End Try
        Next

        Return Canvas
    End Function

    Private Sub DrawLine(ByVal Canvas As Bitmap, ByVal DR_Element As DataRow, ByVal Filter As DataTable)

        If Not ExistsAttribute(DR_Element, "stroke") Or Not ExistsAttribute(DR_Element, "stroke-width") Then Exit Sub

        Dim _stokecolor As Color = ColorTranslator.FromHtml(DR_Element("stroke"))
        Dim _size As Integer = DR_Element("stroke-width")
        Dim _P1 As New Point(DR_Element("x1"), DR_Element("y1"))
        Dim _P2 As New Point(DR_Element("x2"), DR_Element("y2"))
        Dim _pen As Pen = GetStrokePen(DR_Element)
        Dim G As Graphics = Graphics.FromImage(Canvas)
        Dim GP As New Drawing2D.GraphicsPath

        G.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        G.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
        GP.AddLine(_P1, _P2)
        '------------ Set Transform ----------------
        If ExistsAttribute(DR_Element, "transform") Then
            SetTransformGraphic(GP, DR_Element("transform").ToString)
        End If
        If ExistsAttribute(DR_Element, "fill") Then
            Try
                Dim _brush As New SolidBrush(ColorTranslator.FromHtml(DR_Element("fill")))
                G.FillPath(_brush, GP)
            Catch ex As Exception
            End Try
        End If
        '--------------- Set Draw Outline Text------------
        If ExistsAttribute(DR_Element, "stroke") And ExistsAttribute(DR_Element, "stroke-width") Then
            Try
                Dim P As Pen = GetStrokePen(DR_Element)
                G.DrawPath(P, GP)
            Catch ex As Exception
            End Try
        End If
        '---------- Opacity----------
        '---------- Transform ----------
        '-----------Apply Filter ----------

        G.Dispose()
        GP.Dispose()
    End Sub
    Private Sub DrawRect(ByVal Canvas As Bitmap, ByVal DR_Element As DataRow, ByVal Filter As DataTable)

        If (Not ExistsAttribute(DR_Element, "fill") And (Not ExistsAttribute(DR_Element, "stroke") Or Not ExistsAttribute(DR_Element, "stroke-width"))) Then Exit Sub

        Dim _X As Integer = DR_Element("X")
        Dim _Y As Integer = DR_Element("Y")
        Dim _Width As String = Trim(DR_Element("width").ToString)
        If _Width.IndexOf("%") > -1 Then
            _Width = Canvas.Width * Val(_Width.Replace("%", "") / 100)
        End If
        Dim _Height As String = Trim(DR_Element("height").ToString)
        If _Height.IndexOf("%") > -1 Then
            _Height = Canvas.Height * Val(_Height.Replace("%", "") / 100)
        End If
        Dim _pen As Pen = GetStrokePen(DR_Element)
        Dim G As Graphics = Graphics.FromImage(Canvas)
        G.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        G.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
        Dim GP As New Drawing2D.GraphicsPath
        Dim Rect As New Rectangle(_X, _Y, _Width, _Height)

        '------------ Set Round Corner-------------
        If ExistsAttribute(DR_Element, "rx") Then
            GP.AddPath(RoundedRect(Rect, DR_Element("rx")), True)
        Else
            GP.AddRectangle(Rect)
        End If
        '------------ Set Transform ----------------
        If ExistsAttribute(DR_Element, "transform") Then
            SetTransformGraphic(GP, DR_Element("transform").ToString)
        End If
        If ExistsAttribute(DR_Element, "fill") Then
            Try
                Dim _brush As New SolidBrush(ColorTranslator.FromHtml(DR_Element("fill")))
                G.FillPath(_brush, GP)
            Catch ex As Exception
            End Try
        End If
        '--------------- Set Draw Outline------------
        If ExistsAttribute(DR_Element, "stroke") And ExistsAttribute(DR_Element, "stroke-width") Then
            Try
                Dim P As Pen = GetStrokePen(DR_Element)
                G.DrawPath(P, GP)
            Catch ex As Exception
            End Try
        End If

        '---------- Round Corner (Attr=rx)----------
        '---------- Opacity----------
        '---------- Transform ----------
        '-----------Apply Filter ----------

        G.Dispose()
        GP.Dispose()
    End Sub
    Private Sub DrawEllipse(ByVal Canvas As Bitmap, ByVal DR_Element As DataRow, ByVal Filter As DataTable)

        If (Not ExistsAttribute(DR_Element, "fill") And (Not ExistsAttribute(DR_Element, "stroke") Or Not ExistsAttribute(DR_Element, "stroke-width"))) Then Exit Sub

        Dim _CX As Integer = DR_Element("cx")
        Dim _CY As Integer = DR_Element("cy")
        Dim _RX As Integer = DR_Element("rx")
        Dim _RY As Integer = DR_Element("ry")
        Dim _pen As Pen = GetStrokePen(DR_Element)
        Dim Rect As New Rectangle(_CX - _RX, _CY - _RY, _RX * 2, _RY * 2)
        Dim G As Graphics = Graphics.FromImage(Canvas)
        G.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        G.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
        Dim GP As New Drawing2D.GraphicsPath
        GP.AddEllipse(Rect)
        '------------ Set Transform ----------------
        If ExistsAttribute(DR_Element, "transform") Then
            SetTransformGraphic(GP, DR_Element("transform").ToString)
        End If

        If ExistsAttribute(DR_Element, "fill") Then
            Try
                Dim _brush As New SolidBrush(ColorTranslator.FromHtml(DR_Element("fill")))
                G.FillPath(_brush, GP)
            Catch ex As Exception
            End Try
        End If
        '--------------- Set Draw Outline------------
        If ExistsAttribute(DR_Element, "stroke") And ExistsAttribute(DR_Element, "stroke-width") Then
            Try
                Dim P As Pen = GetStrokePen(DR_Element)
                G.DrawPath(P, GP)
            Catch ex As Exception
            End Try
        End If
        '---------- Opacity----------
        '---------- Transform ----------
        '-----------Apply Filter ----------

        G.Dispose()
        GP.Dispose()
    End Sub
    Private Sub DrawCircle(ByVal Canvas As Bitmap, ByVal DR_Element As DataRow, ByVal Filter As DataTable)

        If (Not ExistsAttribute(DR_Element, "fill") And (Not ExistsAttribute(DR_Element, "stroke") Or Not ExistsAttribute(DR_Element, "stroke-width"))) Then Exit Sub

        Dim _CX As Integer = DR_Element("cx")
        Dim _CY As Integer = DR_Element("cy")
        Dim _R As Integer = DR_Element("r")
        Dim _pen As Pen = GetStrokePen(DR_Element)
        Dim Rect As New Rectangle(_CX - _R, _CY - _R, _R * 2, _R * 2)
        Dim G As Graphics = Graphics.FromImage(Canvas)
        G.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        G.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
        Dim GP As New Drawing2D.GraphicsPath
        GP.AddEllipse(Rect)
        '---------- Opacity----------
        '------------ Set Transform ----------------
        If ExistsAttribute(DR_Element, "transform") Then
            SetTransformGraphic(GP, DR_Element("transform").ToString)
        End If
        '-----------Apply Filter ----------

        If ExistsAttribute(DR_Element, "fill") Then
            Try
                Dim _brush As New SolidBrush(ColorTranslator.FromHtml(DR_Element("fill")))
                G.FillPath(_brush, GP)
            Catch ex As Exception
            End Try
        End If
        '--------------- Set Draw Outline------------
        If ExistsAttribute(DR_Element, "stroke") And ExistsAttribute(DR_Element, "stroke-width") Then
            Try
                Dim P As Pen = GetStrokePen(DR_Element)
                G.DrawPath(P, GP)
            Catch ex As Exception
            End Try
        End If

        G.Dispose()
        GP.Dispose()
    End Sub
    Private Sub DrawPath(ByVal Canvas As Bitmap, ByVal DR_Element As DataRow, ByVal Filter As DataTable)

        If (Not ExistsAttribute(DR_Element, "fill") And (Not ExistsAttribute(DR_Element, "stroke") Or Not ExistsAttribute(DR_Element, "stroke-width"))) Or
           Not ExistsAttribute(DR_Element, "d") Then Exit Sub
        Dim DT As DataTable = PathToCommand(DR_Element("d"))
        If DT.Rows.Count = 0 Then Exit Sub

        Dim G As Graphics = Graphics.FromImage(Canvas)
        Dim GP As New Drawing2D.GraphicsPath
        G.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
        G.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality

        Dim LastPoint As New Point(0, 0)
        Dim FirstPoint As New Point(0, 0)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Command As String = DT.Rows(i).Item("Command")
            Dim Splitter As Char() = {","}
            Dim Param As Single() = ConvertStringArrayToSingle(DT.Rows(i).Item("Param").ToString.Split(Splitter, StringSplitOptions.RemoveEmptyEntries), 1)

            'H = horizontal lineto
            'V = vertical lineto
            'C = curveto
            'S = smooth curveto
            'Q = quadratic Bézier curve
            'T = smooth quadratic Bézier curveto
            'A = elliptical Arc
            'Z = closepath

            Select Case Command
                Case "M" 'M = moveto
                    If i = 0 Then
                        FirstPoint.X = Param(0)
                        FirstPoint.Y = Param(1)
                    End If
                    LastPoint.X = Param(0)
                    LastPoint.Y = Param(1)
                Case "L" 'L = lineto
                    If i = 0 Or Param.Length < 2 Then Continue For '-------Skip-------
                    Dim Target As New Point(LastPoint.X + Param(0), LastPoint.Y + Param(1))
                    GP.AddLine(LastPoint, Target)
                    LastPoint = Target
                Case "H" '--------- Horizontal Line --------------
                    If i = 0 Or Param.Length = 0 Then Continue For '-------Skip-------
                    Dim Target As New Point(LastPoint.X + Param(0), LastPoint.Y)
                    GP.AddLine(LastPoint, Target)
                    LastPoint = Target
                Case "V" '--------- Vertical Line ----------------
                    If i = 0 Or Param.Length = 0 Then Continue For '-------Skip-------
                    Dim Target As New Point(LastPoint.X, LastPoint.Y + Param(1))
                    GP.AddLine(LastPoint, Target)
                    LastPoint = Target
                Case "C", "S" '-------------- Curve To = Bezier Curves = Smooth Curve
                    If i = 0 Or Param.Length < 6 Then Continue For '-------Skip-------
                    '--------- In Order 6 Point = C1 C2 T
                    Dim CP1 As New Point(LastPoint.X + Param(0), LastPoint.Y + Param(1))
                    Dim CP2 As New Point(LastPoint.X + Param(2), LastPoint.Y + Param(3))
                    Dim Target As New Point(LastPoint.X + Param(4), LastPoint.Y + Param(5))
                    GP.AddBezier(LastPoint, CP1, CP2, Target)
                    LastPoint = Target
                Case "Q", "T" '---------------quadratic Bézier curve=smooth quadratic Bézier curveto -----------
                    If i = 0 Or Param.Length < 4 Then Continue For '-------Skip-------
                    '--------- In Order 2 Point = C T
                    Dim CP As New Point(LastPoint.X + Param(0), LastPoint.Y + Param(1))
                    Dim Target As New Point(LastPoint.X + Param(2), LastPoint.Y + Param(3))
                    GP.AddBezier(LastPoint, CP, CP, Target)
                    LastPoint = Target
                Case "A"'----------------elliptical Arc-----------
                    '-------------- Remain---------------
                Case "Z"
                    If i = DT.Rows.Count - 1 Then
                        GP.AddLine(LastPoint, FirstPoint)
                    End If
                    Exit For
            End Select
        Next
        '------------ Set Transform ----------------
        If ExistsAttribute(DR_Element, "transform") Then
            SetTransformGraphic(GP, DR_Element("transform").ToString)
        End If
        If ExistsAttribute(DR_Element, "fill") Then
            Try
                Dim _brush As New SolidBrush(ColorTranslator.FromHtml(DR_Element("fill")))
                G.FillPath(_brush, GP)
            Catch ex As Exception
            End Try
        End If
        '--------------- Set Draw Outline Text------------
        If ExistsAttribute(DR_Element, "stroke") And ExistsAttribute(DR_Element, "stroke-width") Then
            Try
                Dim P As Pen = GetStrokePen(DR_Element)
                G.DrawPath(P, GP)
            Catch ex As Exception
            End Try
        End If
        '---------- Opacity----------
        '---------- Transform ----------
        '-----------Apply Filter ----------

        G.Dispose()
        GP.Dispose()
    End Sub
    Private Sub DrawText(ByVal Canvas As Bitmap, ByVal DR_Element As DataRow, ByVal Filter As DataTable)

        If (Not ExistsAttribute(DR_Element, "fill") And (Not ExistsAttribute(DR_Element, "stroke") Or Not ExistsAttribute(DR_Element, "stroke-width"))) Or
            Not ExistsAttribute(DR_Element, "font-size") Or
            Not ExistsAttribute(DR_Element, "font-family") Or
            Not ExistsAttribute(DR_Element, "Text") Then Exit Sub
        If Not DR_Element.Table.Columns.Contains("X") Or Not DR_Element.Table.Columns.Contains("Y") Then Exit Sub
        If Not IsNumeric(DR_Element("X")) Or Not IsNumeric(DR_Element("Y")) Then Exit Sub

        Dim _Font_Size As Single = CSng(DR_Element("font-size")) / 1.35


        Dim _Font_Family As String = DR_Element("font-family")
        Dim _Text As String = DR_Element("text")
        Dim _X As Single = DR_Element("X")
        Dim _Y As Single = DR_Element("Y")

        Dim Position As New Point(_X, _Y)
        ''------------ Check Transform ------------
        'If ExistsAttribute(DR_Element, "transform") Then
        '    Position = GetNewTransformPoint(Position, DR_Element("transform"))
        'End If

        '------------ Set Font And Style----------
        'Dim _Italic As Boolean = Not ExistsAttribute(DR_Element, "font-style") AndAlso DR_Element("font-style").ToString.ToLower = "italic"
        'Dim _Bold As Boolean = Not ExistsAttribute(DR_Element, "font-weight") AndAlso DR_Element("font-weight").ToString.ToLower = "bold"
        'Dim F As Font
        'If _Bold And _Italic Then
        '    F = New Font(_Font_Family, _Font_Size, FontStyle.Bold Or FontStyle.Italic, GraphicsUnit.Pixel)
        'ElseIf _Bold Then
        '    F = New Font(_Font_Family, _Font_Size, FontStyle.Bold, GraphicsUnit.Pixel)
        'ElseIf _Italic Then
        '    F = New Font(_Font_Family, _Font_Size, FontStyle.Italic, GraphicsUnit.Pixel)
        'Else
        'F = New Font(_Font_Family, _Font_Size, FontStyle.Regular, GraphicsUnit.Pixel)
        'End If

        Dim F As New Font(GetBestFontName(_Font_Family).FontFamily, _Font_Size, FontStyle.Regular, GraphicsUnit.Pixel)
        '-------------- Set Text Alignment---------- 
        Dim SF As New StringFormat(StringFormatFlags.NoClip)
        If Not ExistsAttribute(DR_Element, "text-anchor") Then
            Select Case DR_Element("text-anchor").ToString.ToLower
                Case "start"
                    SF.Alignment = StringAlignment.Near
                Case "middle"
                    SF.Alignment = StringAlignment.Center
                Case "end"
                    SF.Alignment = StringAlignment.Far
                Case Else
                    SF.Alignment = StringAlignment.Near
            End Select
        Else
            SF.Alignment = StringAlignment.Near
        End If
        SF.LineAlignment = StringAlignment.Near
        '--------------------------------------------

        Dim G As Graphics = Graphics.FromImage(Canvas)
        'Dim RECT As New Rectangle(0, 0, Canvas.Width, Canvas.Height)
        Dim GP As New Drawing2D.GraphicsPath
        G.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
        G.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality

        Dim Info As New FontInfo(G, F)
        Position.Y -= Info.AscentPixels
        GP.AddString(_Text, F.FontFamily, F.Style, _Font_Size, Position, SF)
        '------------ Set Transform ----------------
        If ExistsAttribute(DR_Element, "transform") Then
            SetTransformGraphic(GP, DR_Element("transform").ToString)
        End If
        '--------------- Set Fill Text------------
        If ExistsAttribute(DR_Element, "fill") Then
            Try
                Dim _brush As New SolidBrush(ColorTranslator.FromHtml(DR_Element("fill")))
                G.FillPath(_brush, GP)
            Catch ex As Exception
            End Try
        End If
        '--------------- Set Draw Outline Text------------
        If ExistsAttribute(DR_Element, "stroke") And ExistsAttribute(DR_Element, "stroke-width") Then
            Try
                Dim P As Pen = GetStrokePen(DR_Element)
                G.DrawPath(P, GP)
            Catch ex As Exception
            End Try
        End If

        '---------- Font Style Cannot Get Attributes---------
        '---------- Opacity----------
        '-----------Apply Filter ----------
        G.Dispose()
        GP.Dispose()
    End Sub
    Private Sub DrawImage(ByVal Canvas As Bitmap, ByVal DR_Element As DataRow, ByVal Filter As DataTable)

        If Not ExistsAttribute(DR_Element, "xlink:href") Or
           Not ExistsAttribute(DR_Element, "width") Or Not ExistsAttribute(DR_Element, "height") Then Exit Sub
        If Not DR_Element.Table.Columns.Contains("X") Or Not DR_Element.Table.Columns.Contains("Y") Then Exit Sub
        If Not IsNumeric(DR_Element("X")) Or Not IsNumeric(DR_Element("Y")) Then Exit Sub


        Dim _X As Single = DR_Element("X")
        Dim _Y As Single = DR_Element("Y")
        Dim _W As Single = DR_Element("width")
        Dim _H As Single = DR_Element("height")
        Dim SRC As String = DR_Element("xlink:href").ToString

        '------------ Get Image Src ------------
        Dim Blob As String = ""
        Dim ContentType As Imaging.ImageFormat = Imaging.ImageFormat.Bmp
        Dim IMG As Image

        Try
            If SRC.IndexOf("data:image/jpeg;base64,") = 0 Then
                ContentType = Imaging.ImageFormat.Jpeg
                Blob = SRC.Substring("data:image/jpeg;base64,".Length)
                IMG = C.BlobToImage(Blob)
            ElseIf SRC.IndexOf("data:image/png;base64,") = 0 Then
                ContentType = Imaging.ImageFormat.Png
                Blob = SRC.Substring("data:image/png;base64,".Length)
                IMG = C.BlobToImage(Blob)
            ElseIf SRC.IndexOf("data:image/gif;base64,") = 0 Then
                ContentType = Imaging.ImageFormat.Gif
                Blob = SRC.Substring("data:image/gif;base64,".Length)
                IMG = C.BlobToImage(Blob)
            Else
                Dim wc As New System.Net.WebClient
                Dim B As Byte() = wc.DownloadData(SRC)
                IMG = Image.FromStream(C.ByteToStream(B))
                ContentType = IMG.RawFormat
                wc.Dispose()
            End If
        Catch ex As Exception
            Exit Sub ' Unable To Read Expected URI
        End Try

        Dim G As Graphics = Graphics.FromImage(Canvas)
        G.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
        G.PixelOffsetMode = Drawing2D.PixelOffsetMode.HighQuality
        '---------- Set Contain Rescpect Ratio To Image---------
        Dim Resp As Single = IMG.Width / IMG.Height
        Dim Spec As Single = _W / _H
        If Spec > Resp Then
            '------- More Wider-------------
            Dim NewWidth As Single = Resp * _H
            '-------- Move To Center Area---
            _X += (_W - NewWidth) / 2
            _W = NewWidth
        Else
            '------- More Thinner-----------
            Dim NewHeight As Single = _W / Resp
            '------- Move To Middle Area----
            _Y += (_H - NewHeight) / 2
            _H = NewHeight
        End If
        Dim Rect As New Rectangle(_X, _Y, _W, _H)
        G.DrawImage(IMG, Rect)

    End Sub
    Public Function ImageToSVG(ByVal Image As Image) As String
        Dim Blob As String = C.ImageToBlob(Image)

        Select Case True
            Case Equals(Image.RawFormat, Imaging.ImageFormat.Bmp)
                Blob = "data:image/bmp;base64," & Blob
            Case Equals(Image.RawFormat, Imaging.ImageFormat.Jpeg)
                Blob = "data:image/jpeg;base64," & Blob
            Case Equals(Image.RawFormat, Imaging.ImageFormat.Png)
                Blob = "data:image/png;base64," & Blob
            Case Equals(Image.RawFormat, Imaging.ImageFormat.Gif)
                Blob = "data:image/gif;base64," & Blob
            Case Equals(Image.RawFormat, Imaging.ImageFormat.Tiff)
                Blob = "data:image/tiff;base64," & Blob
        End Select

        Dim Result As String = "<svg width=""" & Image.Width & """ height=""" & Image.Height & """ xmlns=""http://www.w3.org/2000/svg"" xmlns:xlink=""http://www.w3.org/1999/xlink"">" & vbNewLine
        Result &= " <g>" & vbNewLine
        Result &= "     <title>Background</title>" & vbNewLine
        Result &= "     <image xlink:href=""" & Blob & """ id=""imgBackground"" height=""" & Image.Height & """ width=""" & Image.Width & """ y=""0"" x=""0""/>" & vbNewLine
        Result &= " </g>" & vbNewLine
        Result &= "</svg>" & vbNewLine
        Return Result
    End Function

    Public Function ImageToSVG(ByVal Path As String) As String
        Dim ST As Stream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
        Dim IMG As Image = Image.FromStream(ST)
        Dim Result As String = ImageToSVG(IMG)
        IMG.Dispose()
        ST.Close()
        ST.Dispose()
        Return Result
    End Function

    Private Sub ApplyFilter()

    End Sub

    Private Function ConvertStringArrayToSingle(ByVal Arr As String(), ByVal Multiple As Single) As Single()
        Dim Result As Single() = {}
        For i As Integer = 0 To Arr.Count - 1
            Array.Resize(Result, Result.Length + 1)
            Result(Result.Length - 1) = Val(Arr(i)) * Multiple '-------  Try to devided with 9 it's OK ----------
        Next
        Return Result
    End Function

    Private Function GetBestFontName(ByVal FontHTML As String) As Font
        Dim Result As String() = FontHTML.Split(", ")
        For i As Integer = 0 To Result.Length - 1
            Result(i) = Trim(Result(i).Replace("'", ""))
            Dim F As New Font(Result(i), 10, FontStyle.Regular)
            If F.FontFamily.Name.ToUpper = Result(i).ToUpper Then
                Return F
            End If
        Next
        If Result.Length > 0 Then
            Return New Font(Result(0), 10, FontStyle.Regular)
        Else
            Return New Font("System", 10, FontStyle.Regular)
        End If
    End Function

    'Private Sub SetTransformGraphic(ByRef G As Graphics, ByVal TransformText As String)
    '    Dim TMPString As String = TransformText.Replace(" ", "").Replace(vbCr, "").Replace(vbLf, "").Replace(vbNewLine, "").ToLower

    '    Dim Param() As Single = {}
    '    '---------- Matrix-------------
    '    Param = ConvertStringArrayToSingle(GetTransformParameter(TMPString, "matrix"), 1)
    '    If Param.Length = 6 Then
    '        TMPString = TMPString.Replace("transform(" & String.Join(",", Param) & ")", "")
    '        G.Transform = New Drawing2D.Matrix(Param(0), Param(1), Param(2), Param(3), Param(4), Param(5))
    '    End If
    '    '---------- Rotate ---------------
    '    Param = ConvertStringArrayToSingle(GetTransformParameter(TMPString, "rotate"), 1)
    '    Select Case Param.Length
    '        Case 1
    '            TMPString = TMPString.Replace("rotate(" & String.Join(",", Param) & ")", "")
    '            G.RotateTransform(Param(0))
    '    End Select
    '    '---------- Translate ---------------
    '    Param = ConvertStringArrayToSingle(GetTransformParameter(TMPString, "translate"), 1)
    '    If Param.Length = 2 Then
    '        TMPString = TMPString.Replace("translate(" & String.Join(",", Param) & ")", "")
    '        G.TranslateTransform(Param(0), Param(1))
    '    End If
    '    '---------- Scale ---------------
    '    Param = ConvertStringArrayToSingle(GetTransformParameter(TMPString, "scale"), 1)
    '    Select Case Param.Length
    '        Case 1
    '            TMPString = TMPString.Replace("scale(" & String.Join(",", Param) & ")", "")
    '            G.ScaleTransform(Param(0), Param(0))
    '        Case 2
    '            TMPString = TMPString.Replace("scale(" & String.Join(",", Param) & ")", "")
    '            G.ScaleTransform(Param(0), Param(1))
    '    End Select

    'End Sub

    Private Sub SetTransformGraphic(ByRef GP As Drawing2D.GraphicsPath, ByVal TransformText As String)
        Dim TMPString As String = TransformText.Replace(" ", "").Replace(vbCr, "").Replace(vbLf, "").Replace(vbNewLine, "").ToLower

        Dim Param() As Single = {}
        '---------- Matrix-------------
        Param = ConvertStringArrayToSingle(GetTransformParameter(TMPString, "matrix"), 1)
        If Param.Length = 6 Then
            TMPString = TMPString.Replace(GetTransformCommand(TMPString, "matrix"), "")
            GP.Transform(New Drawing2D.Matrix(Param(0), Param(1), Param(2), Param(3), Param(4), Param(5)))
        End If

    End Sub

    Private Function GetStrokePen(ByVal DR_Element As DataRow) As Pen
        If Not ExistsAttribute(DR_Element, "stroke") Or Not ExistsAttribute(DR_Element, "stroke-width") Then Return Nothing
        Dim _stokecolor As Color = ColorTranslator.FromHtml(DR_Element("stroke"))
        Dim _size As Integer = DR_Element("stroke-width")
        Dim _pen As New Pen(_stokecolor, _size)
        If ExistsAttribute(DR_Element, "stroke-dasharray") Then
            _pen.DashPattern = ConvertStringArrayToSingle(DR_Element("stroke-dasharray").Split(","), 1 / 9)
        End If
        _pen.LineJoin = Drawing2D.LineJoin.Miter
        Return _pen
    End Function

    Private Function GetTransformParameter(ByVal Input As String, ByVal TransformFunctional As String) As String()
        Dim Result As String() = {}
        Dim StartIndex As Integer = Input.ToLower.IndexOf(TransformFunctional & "(")
        If StartIndex = -1 Then Return Result
        StartIndex += (TransformFunctional & "(").Length
        Dim CountComma As Integer = 0
        Dim CloseIndex As Integer = 0
        For i As Integer = StartIndex To Input.Length - 1
            If Input.Substring(i, "1") = "," Then
                CountComma += 1
            End If
            If Input.Substring(i, "1") = ")" Then
                CloseIndex = i
                Exit For
            End If
        Next
        Return Input.Substring(StartIndex, CloseIndex - StartIndex).Split(",")
    End Function

    Private Function GetTransformCommand(ByVal Input As String, ByVal TransformFunctional As String) As String
        Dim Result As String = ""
        Dim StartIndex As Integer = Input.ToLower.IndexOf(TransformFunctional & "(")
        If StartIndex = -1 Then Return Result
        StartIndex += (TransformFunctional & "(").Length
        Dim CountComma As Integer = 0
        Dim CloseIndex As Integer = 0
        For i As Integer = StartIndex To Input.Length - 1
            If Input.Substring(i, "1") = "," Then
                CountComma += 1
            End If
            If Input.Substring(i, "1") = ")" Then
                CloseIndex = i
                Exit For
            End If
        Next
        Return Input.Substring(StartIndex, CloseIndex - StartIndex)
    End Function

    Function GetNewTransformPoint(ByVal PrevPoint As Point, ByVal TransformMatrix As String) As Point
        'matrix(0.7665926492070518,0,0,0.6662945584703266,88.23562457264002,101.59892925223667)
        Dim Matrix As String() = Split(TransformMatrix.ToLower.Replace("matrix(", "").Replace(")", "").Replace(" ", ""), ",")
        Dim TX As Single = CDbl(Matrix(4))
        Dim TY As Single = CDbl(Matrix(5))
        Dim Result As New Point
        Result.X = PrevPoint.X + TX
        Result.Y = PrevPoint.Y + TY
        Return Result
    End Function

    Public Class FontInfo
        ' Heights And positions in pixels.
        Public EmHeightPixels As Single
        Public AscentPixels As Single
        Public DescentPixels As Single
        Public CellHeightPixels As Single
        Public InternalLeadingPixels As Single
        Public LineSpacingPixels As Single
        Public ExternalLeadingPixels As Single

        'Distances from the top of the cell in pixels.
        Public RelTop As Single
        Public RelBaseline As Single
        Public RelBottom As Single

        'Initialize the properties.
        Public Sub New(ByVal G As Graphics, ByVal the_font As Font)
            Dim em_height As Single = the_font.FontFamily.GetEmHeight(the_font.Style)
            EmHeightPixels = ConvertUnits(G, the_font.Size, the_font.Unit, GraphicsUnit.Pixel)
            Dim design_to_pixels As Single = EmHeightPixels / em_height
            AscentPixels = design_to_pixels * the_font.FontFamily.GetCellAscent(the_font.Style)
            DescentPixels = design_to_pixels * the_font.FontFamily.GetCellDescent(the_font.Style)
            CellHeightPixels = AscentPixels + DescentPixels
            InternalLeadingPixels = CellHeightPixels - EmHeightPixels
            LineSpacingPixels = design_to_pixels * the_font.FontFamily.GetLineSpacing(the_font.Style)
            ExternalLeadingPixels = LineSpacingPixels - CellHeightPixels
            RelTop = InternalLeadingPixels
            RelBaseline = AscentPixels
            RelBottom = CellHeightPixels
        End Sub

        Private Function ConvertUnits(ByVal G As Graphics, ByVal value As Single, ByVal from_unit As GraphicsUnit, ByVal to_unit As GraphicsUnit) As Single
            If from_unit = to_unit Then Return value
            ' Convert to pixels. 
            Select Case from_unit
                Case GraphicsUnit.Document
                    value *= G.DpiX / 300
                Case GraphicsUnit.Inch
                    value *= G.DpiX
                Case GraphicsUnit.Millimeter
                    value *= G.DpiX / 25.4F
                Case GraphicsUnit.Pixel
                Case GraphicsUnit.Point
                    value *= G.DpiX / 72
                Case Else
                    Throw New Exception("Unknown input unit " & from_unit.ToString() & " in FontInfo.ConvertUnits")
            End Select

            ' Convert from pixels to the New units. 
            Select Case to_unit
                Case GraphicsUnit.Document
                    value /= G.DpiX / 300
                Case GraphicsUnit.Inch
                    value /= G.DpiX
                Case GraphicsUnit.Millimeter
                    value /= G.DpiX / 25.4F
                Case GraphicsUnit.Pixel
            'Do nothing.
                Case GraphicsUnit.Point
                    value /= G.DpiX / 72
                Case Else
                    Throw New Exception("Unknown output unit " & to_unit.ToString() & " in FontInfo.ConvertUnits")
            End Select
            Return value
        End Function
    End Class

    Private Function PathToCommand(ByVal Path As String) As DataTable

        Dim DT As New DataTable
        DT.TableName = "Path"
        DT.Columns.Add("Command")
        DT.Columns.Add("Param")

        'M = moveto
        'L = lineto
        'H = horizontal lineto
        'V = vertical lineto
        'C = curveto
        'S = smooth curveto
        'Q = quadratic Bézier curve
        'T = smooth quadratic Bézier curveto
        'A = elliptical Arc
        'Z = closepath

        Path = Path.ToUpper
        Dim LastChar As Char = ""
        For i As Integer = 0 To Path.Length - 1
            Dim ThisChar As String = Path.Substring(i, 1)
            Select Case ThisChar
                Case "M", "L", "H", "V", "C", "S", "Q", "T", "A"
                    DT.Rows.Add(ThisChar, "") '----------------- New Command----------------
                Case vbNewLine, vbCr, vbLf, " ", ",", vbTab '------------- Punctuaion ----------
                    If DT.Rows.Count = 0 Then Continue For
                    DT.Rows(DT.Rows.Count - 1)("Param") &= ","
                Case "-", ".", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" '-------------------- Parameter Value--------------
                    If DT.Rows.Count = 0 Then Continue For
                    DT.Rows(DT.Rows.Count - 1)("Param") &= ThisChar
                Case "Z"
                    DT.Rows.Add(ThisChar, "")
                    Exit For
            End Select
        Next

        '----------------- Clean And Optimized Parameter-----------------
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Param As String = CleanPathParam(DT.Rows(i)("Param").ToString)
            DT.Rows(i).Item("Param") = Param
        Next

        Return DT
    End Function

    Private Function CleanPathParam(ByVal Param As String) As String
        While Param.IndexOf(",,") > -1
            Param = Param.Replace(",,", ",")
        End While
        While Param.IndexOf("-,") > -1
            Param = Param.Replace("-,", ",")
        End While
        While Param.IndexOf("..") > -1
            Param = Param.Replace("..", ".")
        End While
        While Left(Param, 1) = ","
            Param = Param.Substring(1)
        End While
        While Right(Param, 1) = ","
            Param = Param.Substring(0, Param.Length - 1)
        End While
        Return Param
    End Function

    Private Function RoundedRect(ByVal Rect As Rectangle, ByVal Radius As Integer) As Drawing2D.GraphicsPath

        Dim diameter As Integer = Radius * 2
        Dim size As New Size(diameter, diameter)
        Dim arc As Rectangle = New Rectangle(Rect.Location, size)
        Dim Path As New Drawing2D.GraphicsPath
        ' top left arc  
        Path.AddArc(Rect.X, Rect.Y, Radius, Radius, 180, 90)
        Path.AddArc(Rect.X + Rect.Width - Radius, Rect.Y, Radius, Radius, 270, 90)
        Path.AddArc(Rect.X + Rect.Width - Radius, Rect.Y + Rect.Height - Radius, Radius, Radius, 0, 90)
        Path.AddArc(Rect.X, Rect.Y + Rect.Height - Radius, Radius, Radius, 90, 90)

        Path.AddLine(Rect.X, Rect.Y + Rect.Height - Radius, Rect.X, Rect.Y + CInt(Radius / 2))
        Path.CloseFigure()
        Return Path
    End Function

    'Private Sub SetOpacity()

    'End Sub

End Class
