﻿Imports System
Imports System.IO
Imports AjaxControlToolkit
Imports System.Data.SqlClient

Public Class UC_DocumentTreePlanPaper
    Inherits System.Web.UI.UserControl
    Dim CL As New LawClass
    Dim BL As New EIR_BL

    Public Event SetPercentComplete(sender As UC_DocumentTreePlanPaper, PercentComplete As Integer)

    Public Sub SetPlanPaperData(dt As DataTable)
        If dt.Rows.Count > 0 Then
            lblDocumentPlanID.Text = dt.Rows(0)("document_plan_id")

            rptShowProcess.DataSource = dt
            rptShowProcess.DataBind()
        Else
            rptShowProcess.DataSource = Nothing
            rptShowProcess.DataBind()
        End If

    End Sub

    Private Sub ResetStatus(DocumentPlanPeperID As Long, lblPlanStatus As Label, lblLastStatus As Label)

        Dim pStatus As DocumentPlanStatusClass = CL.GetDocumentPaperStatus(DocumentPlanPeperID)
        Dim imgUrl As String = ""
        Select Case pStatus.PlanStatus
            Case "WAITING"
                lblPlanStatus.Text = "<span style='color:blue'><b>WAITING</b></span>"
            Case "NOTICE"
                lblPlanStatus.Text = "<span style='color:orange'><b>NOTICE</b></span>"
            Case "CRITICAL"
                lblPlanStatus.Text = "<span style='color:red'><b>LATE</b></span> <img src='resources/images/icons/alert.gif' />"
            Case "COMPLETE"
                lblPlanStatus.Text = "<span style='color:green'><b>COMPLETE</b></span>"
        End Select

        Select Case pStatus.LastStatus
            Case "CRITICAL"
                lblLastStatus.Text = "<span style='color:#dc8132;font-size: 10px;'><b>LATED</b></span>"
            Case Else
                lblLastStatus.Text = ""
        End Select


        'lblImageProcess.Text = "<td rowspan = ""2"" style=""background-image:url('" & imgUrl & "');background-size:70%;background-repeat:no-repeat;vertical-align:middle;text-align:center;"" class=""processBGImage"" >"
        'lblImageProcess.Text += "   <b>" & pStatus.PlanStatus & "</b>"
        'lblImageProcess.Text += "</td>"

    End Sub

    Private Sub rptShowProcess_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptShowProcess.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblImageProcess As Label = e.Item.FindControl("lblImageProcess")
        Dim lblPaperName As Label = e.Item.FindControl("lblPaperName")
        Dim lblNoticeDate As Label = e.Item.FindControl("lblNoticeDate")
        Dim lblCriticalDate As Label = e.Item.FindControl("lblCriticalDate")
        Dim lblUploadDate As Label = e.Item.FindControl("lblUploadDate")
        Dim lblFileIconList As Label = e.Item.FindControl("lblFileIconList")
        Dim lblDocumentPlanPaperID As Label = e.Item.FindControl("lblDocumentPlanPaperID")
        Dim lblPlanStatus As Label = e.Item.FindControl("lblPlanStatus")
        Dim lblLastStatus As Label = e.Item.FindControl("lblLastStatus")

        ResetStatus(e.Item.DataItem("document_plan_paper_id"), lblPlanStatus, lblLastStatus)

        lblPaperName.Text = e.Item.ItemIndex + 1 & "." & e.Item.DataItem("paper_name")
        'lblNoticeDate.Text = Convert.ToDateTime(e.Item.DataItem("plan_notice_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))
        'lblCriticalDate.Text = Convert.ToDateTime(e.Item.DataItem("plan_critical_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))

        lblNoticeDate.Text = BL.ReportGridTime(e.Item.DataItem("plan_notice_date"))
        lblCriticalDate.Text = BL.ReportGridTime(e.Item.DataItem("plan_critical_date"))

        If Convert.IsDBNull(e.Item.DataItem("upload_date")) = False Then
            'lblUploadDate.Text = Convert.ToDateTime(e.Item.DataItem("upload_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))
            lblUploadDate.Text = BL.ReportGridTime(e.Item.DataItem("upload_date"))
        End If

        lblDocumentPlanPaperID.Text = e.Item.DataItem("document_plan_paper_id")

        BuiltIconPlanPaperList(lblDocumentPlanPaperID.Text, lblPaperName.Text, lblFileIconList)


        '--------
        Dim rptPaper As Repeater = e.Item.FindControl("rptPaper")
        AddHandler rptPaper.ItemDataBound, AddressOf rptPaper_ItemDataBound
        Dim dtFile As DataTable = CL.GetListPlanPaperUpload(lblDocumentPlanPaperID.Text)
        rptPaper.DataSource = dtFile
        rptPaper.DataBind()





        'Upload File Handle
        Dim txtFileName As TextBox = e.Item.FindControl("txtFileName")
        Dim txtTempFilePath As TextBox = e.Item.FindControl("txtTempFilePath")
        Dim btnBrowse As Button = e.Item.FindControl("btnBrowse")
        Dim ful As FileUpload = e.Item.FindControl("ful")
        btnBrowse.Attributes.Add("onClick", "UploadFile('" & ful.ClientID & "'); return false;")
        ful.Attributes.Add("onchange", "asyncUploadFile('" & txtFileName.ClientID & "','" & txtTempFilePath.ClientID & "','" & ful.ClientID & "');")
    End Sub

    Private Sub BuiltIconPlanPaperList(DocumentPlanPaperID As Long, PaperName As String, lblFileIconList As Label)
        Dim dtFile As DataTable = CL.GetListPlanPaperUpload(DocumentPlanPaperID)
        If dtFile.Rows.Count > 0 Then
            Dim txt As New StringBuilder
            For Each dr As DataRow In dtFile.Rows
                Dim IconURL As String = CL.GetIconImageUrl(dr("file_ext"))
                txt.AppendLine("<a href='" & CL.LawDocumentDownloadURL() & "/" & dr("document_paper_upload_id") & dr("file_ext") & "' target='_Blank' title='" & PaperName & "'  >")
                txt.AppendLine("    <img src='" & IconURL & "' border='0' />")
                txt.AppendLine("</a>")

                'txt.AppendLine("<br />")
                'txt.AppendLine("<asp:ImageButton ID='btnDel'  runat='server' DOC_ID='" & dr("document_paper_upload_id") & "' ImageUrl='resources/images/icons/cross.png'   OnClick ='btnDelete_Click'   />")

                'Dim F As FileStream = File.Open(CL.LawDocumentPath() & dr("document_paper_upload_id") & dr("file_ext"), FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                'Dim C As New Converter
                'Dim B As Byte() = C.StreamToByte(F)
                'F.Close()
                'Response.Clear()
                'Response.BinaryWrite(B)




            Next

            lblFileIconList.Text = txt.ToString
        End If
        dtFile.Dispose()
    End Sub

    Protected Sub btnUpload_Click(sender As Object, e As EventArgs)
        Dim btnUpload As Button = DirectCast(sender, Button)
        Dim RowItem As RepeaterItem = btnUpload.Parent

        Dim lblPaperName As Label = RowItem.FindControl("lblPaperName")
        Dim lblUploadDate As Label = RowItem.FindControl("lblUploadDate")
        Dim btnBrowse As Button = RowItem.FindControl("btnBrowse")
        Dim txtFileName As TextBox = RowItem.FindControl("txtFileName")
        Dim txtTempFilePath As TextBox = RowItem.FindControl("txtTempFilePath")
        Dim lblDocumentPlanPaperID As Label = RowItem.FindControl("lblDocumentPlanPaperID")
        Dim lblImageProcess As Label = RowItem.FindControl("lblImageProcess")
        Dim lblFileIconList As Label = RowItem.FindControl("lblFileIconList")
        Dim lblPlanStatus As Label = RowItem.FindControl("lblPlanStatus")
        Dim lblLastStatus As Label = RowItem.FindControl("lblLastStatus")

        Dim ret As String = ""
        If File.Exists(txtTempFilePath.Text) = True Then
            Dim fInfo As New FileInfo(txtTempFilePath.Text)
            Dim FileExt As String = fInfo.Extension

            'Path จริง
            Dim LawDocumentPath As String = CL.LawDocumentPath()
            If Directory.Exists(LawDocumentPath) = False Then
                Directory.CreateDirectory(LawDocumentPath)
            End If

            'Save ข้อมูลลง DB
            ret = CL.SaveDocumentPaperUpload(lblDocumentPlanPaperID.Text, "", LawDocumentPath, fInfo.Name, FileExt, Session("USER_ID"))
            Dim tmp() As String = ret.Split("|")
            If tmp.Length = 2 Then
                If tmp(0).ToLower = "true" Then
                    Dim FilePath As String = LawDocumentPath & tmp(1) & FileExt
                    If fInfo.FullName <> FilePath Then
                        Try
                            If File.Exists(FilePath) = True Then
                                File.Delete(FilePath)
                            End If

                            File.Move(fInfo.FullName, FilePath)

                            ResetStatus(lblDocumentPlanPaperID.Text, lblPlanStatus, lblLastStatus)
                            BuiltIconPlanPaperList(lblDocumentPlanPaperID.Text, lblPaperName.Text, lblFileIconList)
                            lblUploadDate.Text = DateTime.Now.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("th-TH"))

                            Dim PlanPercentComplete As Integer = CL.GetDocumentPlanPercentComplete(lblDocumentPlanID.Text)
                            RaiseEvent SetPercentComplete(Me, PlanPercentComplete)
                        Catch ex As Exception

                        End Try
                    End If
                End If
            End If
            'End If
        End If
    End Sub



    Protected Sub rptPaper_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        '------Detail------ 
        Dim lblFileIconList As Label = e.Item.FindControl("lblFileIconList")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim LinkDoc As HtmlAnchor = e.Item.FindControl("LinkDoc")
        Dim imgDoc As HtmlImage = e.Item.FindControl("imgDoc")

        Dim txt As New StringBuilder
        Dim IconURL As String = CL.GetIconImageUrl(e.Item.DataItem("file_ext"))
        txt.AppendLine("<a href='" & CL.LawDocumentDownloadURL() & "/" & e.Item.DataItem("document_paper_upload_id") & e.Item.DataItem("file_ext") & "' target='_Blank' title='" & e.Item.DataItem("original_file_name").ToString() & "'  >")
        txt.AppendLine("    <img src='" & IconURL & "' border='0' />")
        txt.AppendLine("</a>")
        lblFileIconList.Text = txt.ToString

        LinkDoc.HRef = CL.LawDocumentDownloadURL() & "/" & e.Item.DataItem("document_paper_upload_id") & e.Item.DataItem("file_ext")
        LinkDoc.Title = e.Item.DataItem("original_file_name").ToString()
        imgDoc.Src = IconURL
        btnDelete.CommandArgument = e.Item.DataItem("document_paper_upload_id")

    End Sub

    Protected Sub rptPaper_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Try

            Dim sql As String = "select * "
            sql += " from LAW_Document_Paper_Upload "
            sql += " where document_paper_upload_id=" & btnDelete.CommandArgument
            Dim DA As New SqlDataAdapter(sql, BL.ConnStr)
            Dim DT_File As New DataTable
            DA.Fill(DT_File)

            If DT_File.Rows.Count > 0 Then
                For Each dr As DataRow In DT_File.Rows
                    Dim FileName As String = CL.LawDocumentPath() & dr("document_paper_upload_id") & dr("file_ext")
                    If File.Exists(FileName) = True Then
                        Try
                            File.SetAttributes(FileName, FileAttributes.Normal)
                            File.Delete(FileName)
                        Catch ex As Exception

                        End Try
                    End If

                    '----ตรวจสอบว่าลบไฟล์หมกแล้ว ให้เคลียร์ วันที่อัพโหลด
                    sql = "select * "
                    sql += " from LAW_Document_Paper_Upload "
                    sql += " where document_plan_paper_id=" & dr("document_plan_paper_id")
                    DA = New SqlDataAdapter(sql, BL.ConnStr)
                    Dim DT_ck As New DataTable
                    DA.Fill(DT_ck)
                    Dim DT As New DataTable
                    If DT_ck.Rows.Count = 1 Then
                        sql = "Update LAW_Document_Plan_Paper SET "
                        sql += " upload_date=NULL "
                        sql += " where document_plan_paper_id=" & dr("document_plan_paper_id")
                        DA = New SqlDataAdapter(sql, BL.ConnStr)
                        DT = New DataTable
                        DA.Fill(DT)

                    End If

                    sql = "delete from LAW_Document_Paper_Upload "
                    sql += " where document_paper_upload_id=" & btnDelete.CommandArgument
                    DA = New SqlDataAdapter(sql, BL.ConnStr)
                    DT = New DataTable
                    DA.Fill(DT)

                Next

            End If

        Catch ex As Exception
            '----แสดง Error
        End Try

        'Dim pDt As DataTable = CL.GetListDocumentPlanPaper(Request("id"))
        'SetPlanPaperData(pDt)

        Response.Redirect("LAW_Document_Summary_Detail.aspx?id=" & Request("id") & "")

    End Sub

End Class