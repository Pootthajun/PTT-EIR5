﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GL_Dialog_ST_Tag.aspx.vb" Inherits="EIR.GL_Dialog_ST_Tag" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="UpdateProgress.ascx" TagName="UpdateProgress" TagPrefix="uc1" %>
<%@ Register Src="~/GL_DialogUploadImage.ascx" TagPrefix="uc1" TagName="GL_DialogUploadImage" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" >
<title>PTT-EIR :: Electronic-Inspection-Report</title>
	  	
		<link rel="icon" type="image/ico" href="resources/images/icons/Logo.ico" />
        <link rel="shortcut icon" href="resources/images/icons/Logo.ico" />
        <!-- Main Stylesheet -->
		<link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />

    <style type="text/css">
        .eventTR {
        background-color:white;
        cursor:pointer;
        border-bottom:solid 1px #eeeeee;
        }

        .eventTR:hover td {
        background-color:#DAE7FC;
        }

    .Dialog_Picture {
        width:880px;
        left:150px;
    }

</style> 

</head>

<!-- Custom Script -->
<script type="text/javascript" src="js/PTTScript.js"></script>
<body style="background-image:None; background-color:#f5f5f5; font-size: 12px; font-family:Tahoma;">
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="scrip1" runat="server" />
    <h2 style="padding: 0px;">History for : <asp:Label ID="lblTagCode" runat="server" Font-Bold="true"></asp:Label> (<asp:Label ID="lblCurrent" runat="server" Font-Bold="true"></asp:Label>)</h2>
    <table width="95%" border="0" cellpadding="0" cellspacing="0" class="panel" style="padding: 0px;">
  <tr>
    <td height="20">Tag Name :</td>
    <td height="20" style="border-bottom:solid 1px #CCCCCC;"><asp:Label ID="lblTagName" runat="server" Font-Bold="true"></asp:Label></td>
    <td >&nbsp;</td>
    <td height="20">Route : </td>
    <td height="20" style="border-bottom:solid 1px #CCCCCC;"><asp:Label ID="lblRoute" runat="server" Font-Bold="true"></asp:Label></td>

  </tr>
  <tr>
    <td height="20">Type : </td>
    <td height="20" style="border-bottom:solid 1px #CCCCCC;"><asp:Label ID="lblType" runat="server" Font-Bold="true"></asp:Label></td>
    <td>&nbsp;</td>
    <td height="20">Process : </td>
    <td height="20" style="border-bottom:solid 1px #CCCCCC;"><asp:Label ID="lblProcess" runat="server" Font-Bold="true"></asp:Label></td>

  </tr>
  <tr>
    <td height="20">Location : </td>
    <td colspan="4" style="border-bottom:solid 1px #CCCCCC;"><asp:Label ID="lblLocation" runat="server" Font-Bold="true"></asp:Label></td>
  </tr>
</table>
<asp:UpdatePanel ID="udp" runat="server">
    <ContentTemplate>


 <h3 style="padding: 0 0 0px 0;">Traceable Problem History
             <asp:TextBox runat="server" ID="txt_Prob_Start"
            style="position:relative; left: 0px; top: 0px;"  Font-Bold="true"
            CssClass="text-input small-input " Width="75px" MaxLength="15" 
            AutoPostBack="True"></asp:TextBox>
            <cc1:CalendarExtender ID="txt_Prob_Start_CalendarExtender" runat="server" 
            Format="dd MMM yyyy" TargetControlID="txt_Prob_Start">
            </cc1:CalendarExtender>
                                            
            &nbsp; to &nbsp; 
            <asp:TextBox runat="server" ID="txt_Prob_End"
            style="position:relative; left: 0px;"  Font-Bold="true"
            CssClass="text-input small-input " Width="75px" MaxLength="15" 
            AutoPostBack="True"></asp:TextBox>
            <cc1:CalendarExtender ID="txt_Prob_End_CalendarExtender" runat="server" 
            Format="dd MMM yyyy" TargetControlID="txt_Prob_End">
            </cc1:CalendarExtender>
        <asp:LinkButton ID="lnkDisplay" Font-Size="12px" runat="server" Visible="false">Toggle Display</asp:LinkButton>
    </h3> 

<table width="100%" border="0" cellspacing="2" cellpadding="4" style="text-align:center;">
   <tr>
     <td style="background-color:#336699;"><span style="color:White; font-weight:bold;"><b>Date</b></span></td>
     <td style="background-color:#336699;"><span style="color:White; font-weight:bold;"><b>Report</b></span></td>
     <td style="background-color:#336699;"><span style="color:White; font-weight:bold;"><b>Problem</b></span></td>
     <td style="background-color:#336699;"><span style="color:White; font-weight:bold;"><b>Detail</b></span></td>
     <td style="background-color:#336699;"><span style="color:White; font-weight:bold;"><b>Raise By</b></span></td>
   </tr>
   <asp:Repeater ID="rptHistory" runat="server">
   <ItemTemplate>
       <tr class="eventTR">
         <td align="center" title="Click to preview report" style="" id="tdDate" runat="server"><a ID="lblDate" runat="server" Font-Bold="True" href="javascript:;"></a></td>
         <td id="tdReport" title="Click to preview report" runat="server"><a ID="lblReport" Font-Bold="True" runat="server" href="javascript:;"></a></td>
         <td id="tdLevel" runat="server"><asp:Label ID="lblInspection" runat="server" Height="20px"></asp:Label></td>
         <td id="tdDetail" runat="server" style="text-align:left;"><asp:Label ID="lblDetail" runat="server"></asp:Label></td>
         <td id="tdBy" runat="server"><asp:Label ID="lblBy" runat="server"></asp:Label><asp:Button ID="btnView" runat="server" style="display:none;" CommandName="View" /></td>
       </tr>
   </ItemTemplate>
   </asp:Repeater>
   
 </table>
 
 <uc1:GL_DialogUploadImage runat="server" ID="DialogDetail" />
 <uc1:UpdateProgress ID="UpdateProgress1" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
   </form>
</body>
</html>
