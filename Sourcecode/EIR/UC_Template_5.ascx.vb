﻿
Public Class UC_Template_5
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Dim C As New Converter

    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return lblProperty.Attributes("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            lblProperty.Attributes("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Public Property Sector_ID() As String
        Get
            Return lblProperty.Attributes("Sector_ID")
        End Get
        Set(ByVal value As String)

            lblProperty.Attributes("Sector_ID") = value
        End Set
    End Property

    Public Property Detail1 As String
        Get
            Return TextEditor1.HTML
        End Get
        Set(value As String)
            TextEditor1.HTML = value
        End Set
    End Property

#Region "Image Functional"
    Private Function BlankImage() As Byte()
        Dim F As IO.FileStream = IO.File.Open(BL.ServerMapPath & "\resources\images\transparent.png", IO.FileMode.Open, IO.FileAccess.Read, IO.FileShare.ReadWrite)
        Dim C As New Converter
        Dim B As Byte() = C.StreamToByte(F)
        F.Close()
        Return B
    End Function


    Public Property IMAGE_1() As FileAttachment
        Get
            Try

                Dim Obj As New FileAttachment
                With Obj
                    .UNIQUE_ID = UNIQUE_POPUP_ID
                    .DocType = FileAttachment.AttachmentType.Cad_Drawing
                    .Extension = FileAttachment.ExtensionType.PNG
                    '-------------- Load Blank Image-------------
                    .Content = BlankImage()
                    '-------------- Close Opened File-------------
                    .Description = Detail1
                End With

                Return Obj
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(ByVal value As FileAttachment)
            If IsNothing(value) Then
                Detail1 = ""
            Else
                Detail1 = value.Description
            End If
        End Set
    End Property

    Public Property Disabled() As Boolean
        Get
            Return Not TextEditor1.Enabled
        End Get
        Set(ByVal value As Boolean)
            TextEditor1.Enabled = Not value
        End Set
    End Property

#End Region


End Class