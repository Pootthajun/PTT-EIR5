﻿Imports System.Data
Imports System.Data.SqlClient

Public Class GL_Dialog_SearchTagClass
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Public Property REPORT_CODE() As String
        Get
            If Not IsNothing(ViewState("REPORT_CODE")) Then
                Return ViewState("REPORT_CODE")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("REPORT_CODE") = value
        End Set
    End Property

    Public Property EQUIPMENT() As String
        Get
            If Not IsNothing(ViewState("EQUIPMENT")) Then
                Return ViewState("EQUIPMENT")
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            ViewState("EQUIPMENT") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Close", "alert('Please login and try again'); window.close();", True)
            Exit Sub
        End If

        If Not IsPostBack Then
            REPORT_CODE = Request.QueryString("REPORT_CODE")
            EQUIPMENT = Request.QueryString("EQUIPMENT")
            lblReportCode.Text = REPORT_CODE
            BindTag()
        End If
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Focus", "window.focus();", True)
    End Sub

    Private Sub BindTag()
        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        If EQUIPMENT = "ST" Then
            SQL &= "SELECT VW_REPORT_ST_DETAIL.RPT_Year,VW_REPORT_ST_DETAIL.RPT_No, VW_REPORT_ST_DETAIL.RPT_CODE" & vbNewLine
            SQL &= ",INSP_ID,INSP_Name" & vbNewLine
            SQL &= ",CURRENT_STATUS_ID" & vbNewLine
            SQL &= ",CURRENT_STATUS_Name" & vbNewLine
            SQL &= ",CURRENT_LEVEL" & vbNewLine
            SQL &= ",CURRENT_LEVEL_Desc" & vbNewLine
            SQL &= ",CURRENT_PROB_Detail" & vbNewLine
            SQL &= ",LAST_STATUS_Name" & vbNewLine
            SQL &= ",LAST_LEVEL" & vbNewLine
            SQL &= ",LAST_LEVEL_Desc" & vbNewLine
            SQL &= ",TAG_CODE" & vbNewLine
            SQL &= ",ISNULL(MS_User.User_Prefix,'') + ISNULL(MS_User.USER_Name,'') + ' ' + ISNULL(MS_User.User_Surname,'') USER_Full_Name" & vbNewLine
            SQL &= "FROM VW_REPORT_ST_DETAIL LEFT JOIN VW_REPORT_ST_HEADER ON VW_REPORT_ST_DETAIL.RPT_CODE=VW_REPORT_ST_HEADER.RPT_CODE" & vbNewLine
            SQL &= "LEFT JOIN MS_User ON VW_REPORT_ST_HEADER.Created_By=MS_User.USER_ID " & vbNewLine
            SQL &= "WHERE VW_REPORT_ST_DETAIL.RPT_CODE ='" & REPORT_CODE & "'  AND ISSUE = 1" & vbNewLine
            SQL &= "AND Detail_ID IS NOT NULL AND (VW_REPORT_ST_DETAIL.CURRENT_LEVEL IS NOT NULL OR VW_REPORT_ST_DETAIL.LAST_LEVEL IS NOT NULL)" & vbNewLine
            SQL &= "ORDER BY VW_REPORT_ST_DETAIL.RPT_CODE DESC,INSP_ID" & vbNewLine
        Else
            SQL &= "SELECT VW_REPORT_RO_DETAIL.RPT_Year,VW_REPORT_RO_DETAIL.RPT_No, VW_REPORT_RO_DETAIL.RPT_CODE" & vbNewLine
            SQL &= ",INSP_ID,INSP_Name" & vbNewLine
            SQL &= ",CURRENT_STATUS_ID" & vbNewLine
            SQL &= ",CURRENT_STATUS_Name" & vbNewLine
            SQL &= ",CURRENT_LEVEL" & vbNewLine
            SQL &= ",CURRENT_LEVEL_Desc" & vbNewLine
            SQL &= ",CURRENT_PROB_Detail" & vbNewLine
            SQL &= ",LAST_STATUS_Name" & vbNewLine
            SQL &= ",LAST_LEVEL" & vbNewLine
            SQL &= ",LAST_LEVEL_Desc" & vbNewLine
            SQL &= ",TAG_CODE" & vbNewLine
            SQL &= ",ISNULL(MS_User.User_Prefix,'') + ISNULL(MS_User.USER_Name,'') + ' ' + ISNULL(MS_User.User_Surname,'') USER_Full_Name" & vbNewLine
            SQL &= "FROM VW_REPORT_RO_DETAIL LEFT JOIN VW_REPORT_RO_HEADER ON VW_REPORT_RO_DETAIL.RPT_CODE=VW_REPORT_RO_HEADER.RPT_CODE" & vbNewLine
            SQL &= "LEFT JOIN MS_User ON VW_REPORT_RO_HEADER.Created_By=MS_User.USER_ID " & vbNewLine
            SQL &= "WHERE VW_REPORT_RO_DETAIL.RPT_CODE ='" & REPORT_CODE & "' AND ISSUE = 1" & vbNewLine
            SQL &= "AND Detail_ID IS NOT NULL AND (VW_REPORT_RO_DETAIL.CURRENT_LEVEL IS NOT NULL OR VW_REPORT_RO_DETAIL.LAST_LEVEL IS NOT NULL)" & vbNewLine
            SQL &= "ORDER BY VW_REPORT_RO_DETAIL.RPT_CODE DESC,INSP_ID" & vbNewLine
        End If

        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)

        'DT.DefaultView.Sort = "RPT_Year DESC,RPT_No DESC DESC"
        'DT = DT.DefaultView.ToTable
        rptHistory.DataSource = DT
        rptHistory.DataBind()

        '--------------------- Get Current Status ------------------
        If DT.Rows.Count > 0 Then
            lblRaiseBy.Text = DT.Rows(0).Item("USER_Full_Name").ToString
            DT.DefaultView.Sort = "RPT_Year DESC,RPT_No DESC"
            Dim RPT_Code As String = ""
            If Not IsDBNull(DT.Rows(0).Item("RPT_CODE")) Then
                RPT_Code = DT.Rows(0).Item("RPT_CODE")
            End If
            DT.Columns.Add("L", GetType(Integer), "IIF(CURRENT_LEVEL IS NOT NULL,CURRENT_LEVEL,LAST_LEVEL)")
            Dim LEVEL As Object = DT.Compute("MAX(L)", "RPT_Code='" & RPT_Code & "'")
        End If

    End Sub

    Dim LastReport As String = ""
    Dim LastDate As String = ""
    Dim LastUser As String = ""

    Protected Sub rptHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptHistory.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tdTag As HtmlTableRow = e.Item.FindControl("tdTag")
        Dim tdLevel As HtmlTableCell = e.Item.FindControl("tdLevel")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblDetail As Label = e.Item.FindControl("lblDetail")

        If e.Item.DataItem("RPT_CODE") <> LastReport Then
            lblTag.Text = e.Item.DataItem("TAG_CODE")
        End If

        Dim lblInspection As Label = e.Item.FindControl("lblInspection")
        If Not IsDBNull(e.Item.DataItem("INSP_Name")) Then
            lblInspection.Text = e.Item.DataItem("INSP_Name")
        End If
        If Not IsDBNull(e.Item.DataItem("CURRENT_STATUS_Name")) Then
            lblInspection.Text &= " " & e.Item.DataItem("CURRENT_STATUS_Name")
        ElseIf Not IsDBNull(e.Item.DataItem("LAST_STATUS_Name")) Then
            lblInspection.Text &= " " & e.Item.DataItem("LAST_STATUS_Name")
        End If

        Dim LAST_LEVEL As Integer = -1
        Dim CURRENT_LEVEL As Integer = -1
        Dim LAST_LEVEL_DESC As String = ""
        Dim CURRENT_LEVEL_DESC As String = ""
        If Not IsDBNull(e.Item.DataItem("LAST_LEVEL")) Then
            LAST_LEVEL = e.Item.DataItem("LAST_LEVEL")
            LAST_LEVEL_DESC = e.Item.DataItem("LAST_LEVEL_DESC")
        End If
        If Not IsDBNull(e.Item.DataItem("CURRENT_LEVEL")) Then
            CURRENT_LEVEL = e.Item.DataItem("CURRENT_LEVEL")
            CURRENT_LEVEL_DESC = e.Item.DataItem("CURRENT_LEVEL_DESC")
        End If

        Dim Tooltip As String = ""
        If Not IsDBNull(e.Item.DataItem("INSP_ID")) Then
            If CURRENT_LEVEL = -1 Then
                tdLevel.Attributes("Class") = "Text" & LAST_LEVEL_DESC
                Tooltip = LAST_LEVEL_DESC
            Else
                tdLevel.Attributes("Class") = "Text" & CURRENT_LEVEL_DESC
                Tooltip = CURRENT_LEVEL_DESC
            End If
        End If


        If Not IsDBNull(e.Item.DataItem("CURRENT_PROB_Detail")) Then lblDetail.Text = e.Item.DataItem("CURRENT_PROB_Detail")

        tdTag.Attributes("Title") = Tooltip
        lblTag.ToolTip = Tooltip
        lblDetail.ToolTip = Tooltip
        lblInspection.ToolTip = Tooltip

        tdTag.Attributes("onClick") = "ShowPreviewReport(" & e.Item.DataItem("RPT_Year") & "," & e.Item.DataItem("RPT_No") & ");"
    End Sub

End Class