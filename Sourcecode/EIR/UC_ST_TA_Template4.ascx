﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_ST_TA_Template4.ascx.vb" Inherits="EIR.UC_ST_TA_Template4" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="UC_TextEditor.ascx" tagname="UC_TextEditor" tagprefix="uc1" %>

<asp:Label ID="lblProperty" runat="server" Text="" Style="display: none;"></asp:Label>

<%--------------------------------------------------------------------------------------%>
  <style>
        /*form textarea,*/
        form .wysiwyg {
            padding: 6px;
            font-size: 13px;
            /*border: 1px solid #d5d5d5;*/
            color: #333;
            border-style: none;
            background: none;
        }
    </style>
<tr style="text-align: center">
    <td style="width: 50%; text-align: center;" colspan="2">
         <table style="background-color: white;">
            <tr style="border-style: solid; border-width: thin; border-color: #C0C0C0;">
                <td style="vertical-align: middle;text-align :center ;">
                    <asp:Image ImageUrl="RenderImage_ST_TA_Sector.aspx" ID="ImgPreview1" runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer; cursor: pointer; text-align: center; max-width: 100%; min-width: 50%;" />

                    <asp:Button ID="btnRefreshImage" runat="server" Text="Update/Refresh" Style="visibility: hidden; width: 0px;" />
                </td>
            </tr>
        </table>
    </td>

</tr>




<tr id="trimage_1" runat="server" visible="false" style="text-align: center">
    <td style="width: 50%; background-color: black;" colspan="2" >
        <div class="thumbnail">


            <a id="lnk_File_Dialog1" runat="server" target="_blank" title="Figure No.1">
                <asp:Image ID="img_File1" Width="100%" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
            </a>
        </div>
    </td>

</tr>
<%--------------------------------------------------------------------------------------%>
<tr id="trbtn_1" runat="server" visible="false">
    <td style="width: 50%;" class="toolbar" colspan="2">
        <asp:Button ID="btnUpload1" runat="server" Text="" Style="display: none;" />
        <asp:ImageButton ID="btnEdit1" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" Style="margin-right: 10px;" />
        <asp:ImageButton ID="btnDelete1" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
        <cc1:ConfirmButtonExtender ID="btnDelete1_Confirm" runat="server" ConfirmText="Confirm to delete Photographic location?" TargetControlID="btnDelete1"></cc1:ConfirmButtonExtender>
    </td>
</tr>
<!-- End Table Images -->

<%--------------------------------------------------------------------------------------%>



