﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_PipeRoutineInspection.ascx.vb" Inherits="EIR.GL_PipeRoutineInspection" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="GL_DialogPipeRoutineDetail.ascx" tagname="GL_DialogPipeRoutineDetail" tagprefix="uc2" %>

<asp:Panel ID="pnl" runat="server" DefaultButton="btn_Autosave">                                      
    <table width="100%"  style="height:24px;" align="right" cellpadding="0" cellspacing="0">
    <tr>
        <td style="width:100px;" id="TDLevel" runat="server" ><asp:Label ID="lbl_Inspection" runat="server"></asp:Label>&nbsp;</td>
        <td style="text-align:center; width:100px; background-color:#f9fefe;">
            <asp:Label ID="lblPart" runat="server" Visible="false" onmouseover="this.title=this.value;"></asp:Label>
            <asp:TextBox ID="txtPart" runat="server" Width="95px" BorderStyle="none" onmouseover="this.title=this.value;" BackColor="Transparent" Font-Size="11px" MaxLength="100"></asp:TextBox>
        </td>
        <td style="text-align:center; width:80px; background-color:#fefef7;"><a ID="lbl_LastStatus" runat="server" href="javascript:;"></a></td>
        <td style="text-align:center; width:70px; background-color:#fefef7;">
            <asp:DropDownList ID="ddl_Fixed" runat="server" BackColor="#f9fefe" Width="100%" BorderStyle="none" Font-Size="11px">
                <asp:ListItem Text=""></asp:ListItem>
                <asp:ListItem Text="Fixed"></asp:ListItem>
                <asp:ListItem Text="Not fix"></asp:ListItem>
            </asp:DropDownList></td>
        <td style="text-align:center; width:100px; background-color:#f9fefe;"><asp:DropDownList ID="ddl_Status" runat="server" BackColor="#f9fefe" Width="100%" BorderStyle="none" Font-Size="11px"></asp:DropDownList></td>
        <td style="text-align:center; width:100px; background-color:#f9fefe;"><asp:DropDownList BackColor="#f9fefe" ID="ddl_Level" runat="server" Width="100%" BorderStyle="none" Font-Size="11px"></asp:DropDownList></td>
                                                                                      
        <td style="width:100px;"><asp:Label ID="lbl_Trace" runat="server" Font-Italic="true" ></asp:Label></td>
        <td style="text-align:center; " ><asp:TextBox ID="txt_Detail" runat="server" Width="100%" BorderStyle="none" Font-Size="11px" MaxLength="1000"></asp:TextBox></td>
        <td style="text-align:left; width:70px;">
                                                    <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="resources/images/icons/pencil.png" ToolTip="Upload/edit more detail" />
                                                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="resources/images/icons/cross.png" ToolTip="Clear detail for this inspection point" />
                                                    <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this inspection permanently?" />
                                                    <asp:Image ID="imgWarning" runat="server" style="cursor:help;" ImageUrl="resources/images/icons/alert.gif" ToolTip="Unable to autosave Please completed all require detail !!" />
                <asp:Button ID="btn_LastStatus" runat="server" Style="display:none;" />
                <asp:Button ID="btn_Autosave" runat="server" Style="display:none;" />
        </td>
    </tr>
    </table>
</asp:Panel>
<uc2:GL_DialogPipeRoutineDetail ID="DialogDetail" runat="server" />