﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Public Class Dashboard_Summary_Route
    Inherits System.Web.UI.Page

    Dim Dashboard As New DashboardClass
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dim Plant_ID As Integer = Request.QueryString("PLANT_ID")
            Dim Route_ID As Integer = Request.QueryString("ROUTE_ID")
            Dim Route_Name As String = Request.QueryString("ROUTE_NAME")
            Dim Month_F As Integer = Request.QueryString("MONTH_F")
            Dim Month_T As Integer = Request.QueryString("MONTH_T")
            Dim Year_F As Integer = Request.QueryString("YEAR_F")
            Dim Year_T As Integer = Request.QueryString("YEAR_T")
            Dim Equipment As Integer = Request.QueryString("EQUIPMENT")

            Dashboard.BindDDlMonthEng(ddl_Month_F, Month_F)
            Dashboard.BindDDlMonthEng(ddl_Month_T, Month_T)
            Dashboard.BindDDlYear(ddl_Year_F, Year_F)
            Dashboard.BindDDlYear(ddl_Year_T, Year_T)
            Dashboard.BindDDlEquipment(ddl_Equipment, Equipment)


            lblBack.PostBackUrl = "Dashboard_Summary_Plant.aspx?PLANT_ID=" & Plant_ID & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & Equipment

            lblRouteName.Text = Route_Name
            lblRouteID.Text = Route_ID
            lblPlantID.Text = Plant_ID
            BindData()
        End If
    End Sub

    Private Sub BindData()
        UC_Dashboard_Summary_Route.BindData(lblPlantID.Text, lblRouteID.Text, lblRouteName.Text, ddl_Month_F.SelectedValue, ddl_Month_T.SelectedValue, ddl_Year_F.SelectedValue, ddl_Year_T.SelectedValue, ddl_Equipment.SelectedValue)
    End Sub

    Protected Sub ALL_ddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Month_F.SelectedIndexChanged, ddl_Month_T.SelectedIndexChanged, ddl_Year_F.SelectedIndexChanged, ddl_Year_T.SelectedIndexChanged, ddl_Equipment.SelectedIndexChanged

        BindData()
        lblBack.PostBackUrl = "Dashboard_Summary_Plant.aspx?PLANT_ID=" & lblPlantID.Text & "&MONTH_F=" & ddl_Month_F.SelectedValue & "&MONTH_T=" & ddl_Month_T.SelectedValue & "&YEAR_F=" & ddl_Year_F.SelectedValue & "&YEAR_T=" & ddl_Year_T.SelectedValue & "&EQUIPMENT=" & ddl_Equipment.SelectedValue

    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Export", "window.open('Dashboard_Summary_Route_Export.aspx?Plant_ID=" & lblPlantID.Text & "&Route_ID=" & lblRouteID.Text & "&Route_Name=" & lblRouteName.Text & "&Month_F=" & ddl_Month_F.SelectedValue & "&Month_T=" & ddl_Month_T.SelectedValue & "&YEAR_F=" & ddl_Year_F.SelectedValue & "&YEAR_T=" & ddl_Year_T.SelectedValue & "&EQUIPMENT=" & ddl_Equipment.SelectedValue & "');", True)
        BindData()
    End Sub

End Class