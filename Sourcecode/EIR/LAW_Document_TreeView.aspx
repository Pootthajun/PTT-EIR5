﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="LAW_Document_TreeView.aspx.vb" Inherits="EIR.LAW_Document_TreeView" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="UC_DocumentTreeList.ascx" TagPrefix="UC" TagName="UC_DocumentTreeList" %>
<%@ Register src="GL_DialogLawJob.ascx" tagname="GL_DialogLawJob" tagprefix="uc1" %>
<%@ Register src="GL_DialogLawUploadDocument.ascx" tagname="GL_DialogLawUploadDocument" tagprefix="uc2" %>

<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <link href="<%=ResolveUrl("~/resources/astreeview/astreeview.css")%>" type="text/css" rel="stylesheet" />
	<link href="<%=ResolveUrl("~/resources/astreeview/contextmenu.css")%>" type="text/css" rel="stylesheet" />
	
	<script src="<%=ResolveUrl("~/resources/astreeview/astreeview.min.js")%>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/resources/astreeview/contextmenu.min.js")%>" type="text/javascript"></script>
</asp:Content>
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

            <style type="text/css">
                .treeNode {
                    font: 14px Arial, Sans-Serif;
                    width: 0px;
                    text-align: left; 
                }
            </style>

            <script src="resources/ContextMenu/jquery.contextMenu.js" type="text/javascript"></script>
	        <link href="resources/ContextMenu/jquery.contextMenu.css" rel="stylesheet" type="text/css" />

            <!-- Page Head -->
	        <h2>Law Document Summary</h2>
            <div class="content-box"><!-- Start Content Box -->
                <!-- End .content-box-header -->
                <div class="content-box-header">
                <h3>Filter</h3>
				    <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true"
                        style="position:relative; top:5px; left: 0px;" 
                        CssClass="text-input small-input " Width="400px"  ></asp:TextBox>
                    <div class="clear"></div>
                </div>
                <div class="content-box-content">

                    <asp:Panel ID="pnlTreeView" runat="server" CssClass="tab-content default-tab">
                        <!-- This is the target div. id must match the href of this div's tab -->
                        <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                            <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                            <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                        </asp:Panel>

                        <UC:UC_DocumentTreeList runat="server" ID="tvLawDocument" />
                        

                        <asp:TextBox ID="txtNodeID" runat="server" style="display:none" ></asp:TextBox>
                        <asp:TextBox ID="txtAction" runat="server" style="display:none" ></asp:TextBox>
                        <asp:LinkButton ID="btnContextMenuAction" runat="server"  Style="display: none" />



                        <ul id="FolderMenuList" class="contextMenu"  >
                            <li ><a href="#duplicateFolder">Duplicate</a></li>
                            <li class="separator"><a href="#newFolder">New Job</a></li>
			                <li ><a href="#editFolder">Edit Job</a></li>	
                            <li ><a href="#newDocument">New Document</a></li>
                            <li ><a href="#DownloadFolder">Download</a></li>
                                
			                <li class="separator"><a href="#deleteFolder">Delete</a></li>
		                </ul>

                        <ul id="DocumentMenuList" class="contextMenu" >
			                <li ><a href="#editDocument">Edit</a></li>
			                <li class="separator"><a href="#deleteDocument">Delete</a></li>
		                </ul>

                        <asp:Button ID="btnAddRootJob" runat="server" Class="button" Text="Add Job" />

                        <uc1:GL_DialogLawJob ID="GL_DialogLawFolder1" runat="server" />
                        <uc2:GL_DialogLawUploadDocument ID="GL_DialogLawUploadDocument1" runat="server" />

                    </asp:Panel>

                    <asp:Panel ID="pnlSearchTableView" runat="server">
                        <table>
                            <asp:Repeater ID="rptSearchResult" runat="server">
                                <HeaderTemplate>
                                    <thead>
                                        <tr>
                                            <th ><a href="#">Name</a> </th>
                                            <th ><a href="#">Path</a></th>
                                            <th ><a href="#">Upload Date</a></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><asp:Label ID="lblName" runat="server"></asp:Label></td>
                                        <td><asp:Label ID="lblPath" runat="server"></asp:Label></td>
                                        <td><asp:Label ID="lblUpdateDate" runat="server"></asp:Label></td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
                    </asp:Panel>
                </div>
            </div>

            <script type="text/javascript">
 
                $(document).ready(InitContextMenu());
                //end document Ready

                function InitContextMenu() {
                    $(".RootMenu").contextMenu({
		            menu: 'RootMenuList'
		            }, function(action, el, pos) {
		                //alert(
			            //'Action: ' + action + '\n\n' +
			            //'Element text: ' + $(el).text() + '\n\n' +
			            //'GUID: ' + getGUID($(el).attr("href")) + '\n\n' +
			            //'X: ' + pos.x + '  Y: ' + pos.y + ' (relative to element)\n\n' +
			            //'X: ' + pos.docX + '  Y: ' + pos.docY + ' (relative to document)'
		                //);
		                //alert(action + " Node:" + $(el).attr("id"));

                        document.getElementById('<%=txtNodeID.ClientID %>').value = $(el).attr("id");
		                document.getElementById('<%=txtAction.ClientID %>').value = action;
                        document.getElementById('<%=btnContextMenuAction.ClientID %>').click();
		            });



		            $(".FolderMenu").contextMenu({
		            menu: 'FolderMenuList'
		            }, function(action, el, pos) {
		                //alert(
			            //'Action: ' + action + '\n\n' +
			            //'Element text: ' + $(el).text() + '\n\n' +
			            //'GUID: ' + getGUID($(el).attr("href")) + '\n\n' +
			            //'X: ' + pos.x + '  Y: ' + pos.y + ' (relative to element)\n\n' +
			            //'X: ' + pos.docX + '  Y: ' + pos.docY + ' (relative to document)'
		                //);
		                //alert(action + " Node:" + $(el).attr("id"));

                        document.getElementById('<%=txtNodeID.ClientID %>').value = $(el).attr("id");
		                document.getElementById('<%=txtAction.ClientID %>').value = action;

		                if (action != "deleteFolder") {
		                    document.getElementById('<%=btnContextMenuAction.ClientID %>').click();
		                } else {
		                    var c = confirm("Are you sure to delete this folder permanently?");
		                    if (c == true) {
                                document.getElementById('<%=btnContextMenuAction.ClientID %>').click();
		                    }
		                }
		            });
					
					
		            $(".DocumentMenu").contextMenu({
			            menu: 'DocumentMenuList'
		            }, function(action, el, pos) {
		                //alert(
			            //'Action: ' + action + '\n\n' +
			            //'Element text: ' + $(el).text() + '\n\n' + 
			            //'GUID: ' + getGUID($(el).attr("href")) + '\n\n' + 
			            //'X: ' + pos.x + '  Y: ' + pos.y + ' (relative to element)\n\n' + 
			            //'X: ' + pos.docX + '  Y: ' + pos.docY+ ' (relative to document)'
			            //);

                        document.getElementById('<%=txtNodeID.ClientID %>').value = $(el).attr("id");
		                document.getElementById('<%=txtAction.ClientID %>').value = action;

		                if (action != "deleteDocument") {
		                    document.getElementById('<%=btnContextMenuAction.ClientID %>').click();
		                } else {
		                    var c = confirm("Are you sure to delete this Document permanently?");
		                    if (c == true) {
                                document.getElementById('<%=btnContextMenuAction.ClientID %>').click();
		                    }
		                }
		            });
	            }
				    

	            //GUID s\\48e8d94d-e6eb-4b4d-a70f-4c82c3e42630
	            // s\\81694dbe-548d-4921-87eb-f6be61ab7dfb\\778c071f-b419-428b-aecb-68b561c25164
	            function getGUID(mystr) {
		            var reGUID = /\w{8}[-]\w{4}[-]\w{4}[-]\w{4}[-]\w{12}/g //regular expression defining GUID
		            var retArr = [];
		            var retval = '';
		            retArr = mystr.match(reGUID);
		            if(retArr != null)
		            {
			            retval = retArr[retArr.length - 1];
		            }
		            return retval;
	            }
            </script>

</asp:Content>



