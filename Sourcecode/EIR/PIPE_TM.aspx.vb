﻿Imports System.Data.SqlClient
Public Class PIPE_TM
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            ResetTM(Nothing, Nothing)
            ClearPanelSearch()
        End If
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindTM()

        Dim SQL As String = "SELECT INSM_ID,INSM_NAME,INSM_SERIAL,MS_TM_Instrument_Type.INSM_Type_ID,INSM_Type_Name,Active_Status,Update_By,Update_Time" & vbLf
        SQL &= " FROM MS_TM_Instrument" & vbLf
        SQL &= " INNER JOIN MS_TM_Instrument_Type ON MS_TM_Instrument.INSM_Type_ID=MS_TM_Instrument_Type.INSM_Type_ID" & vbLf
        Dim WHERE As String = ""
        If txt_Search_Name.Text <> "" Then
            WHERE &= " INSM_NAME Like '%" & txt_Search_Name.Text.Replace("'", "''") & "%' AND "
        End If
        If txt_Search_Serial.Text <> "" Then
            WHERE &= " INSM_SERIAL Like '%" & txt_Search_Serial.Text.Replace("'", "''") & "%' AND "
        End If
        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If
        SQL &= " ORDER BY INSM_NAME" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("PIPE_TM") = DT

        Navigation.SesssionSourceName = "PIPE_TM"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTM
    End Sub

    Protected Sub rptTM_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTM.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblSerial As Label = e.Item.FindControl("lblSerial")
        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        'Dim cfbDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfbDelete")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblName.Text = e.Item.DataItem("INSM_NAME").ToString
        lblSerial.Text = e.Item.DataItem("INSM_SERIAL").ToString
        lblType.Text = e.Item.DataItem("INSM_Type_Name").ToString

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        If Not IsDBNull(e.Item.DataItem("Update_Time")) Then
            lblUpdateTime.Text = C.DateToString(e.Item.DataItem("Update_Time"), "dd MMM yyyy")
        Else
            lblUpdateTime.Text = "-"
        End If
        btnEdit.Attributes("INSM_ID") = e.Item.DataItem("INSM_ID")

    End Sub

    Protected Sub rptTM_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTM.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim INSM_ID As Integer = btnEdit.Attributes("INSM_ID")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                lblUpdateMode.Text = "Update"
                lblUpdateMode.Attributes("INSM_ID") = INSM_ID
                '------------------------------------
                pnlListTM.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT INSM_ID,INSM_NAME,INSM_SERIAL,INSM_Type_ID,Active_Status,Update_By,Update_Time" & vbLf
                SQL &= " FROM MS_TM_Instrument" & vbLf
                SQL &= " WHERE INSM_ID = " & INSM_ID & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Instrument is Not Found"
                    pnlBindingError.Visible = True
                    BindTM()
                    Exit Sub
                End If

                txtName.Text = DT.Rows(0).Item("INSM_NAME").ToString
                txtSerial.Text = DT.Rows(0).Item("INSM_SERIAL").ToString
                PIPE.BindDDl_TM_Instrument_Type(ddl_INSM_Type, DT.Rows(0).Item("INSM_Type_ID"))
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")
                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_TM_Instrument Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE INSM_ID=" & INSM_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindTM()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try

            Case "Delete"
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = "DELETE FROM MS_TM_Instrument WHERE INSM_ID=" & INSM_ID
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindTM()

                lblBindingSuccess.Text = "Delete successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try
        End Select
    End Sub

    Protected Sub ResetTM(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindTM()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListTM.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()

        pnlEdit.Visible = False
        lblUpdateMode.Text = ""

        txtName.Text = ""
        txtSerial.Text = ""
        PIPE.BindDDl_TM_Instrument_Type(ddl_INSM_Type)
        chkAvailable.Checked = True
        btnCreate.Visible = True

    End Sub


#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        txt_Search_Name.Text = ""
        txt_Search_Serial.Text = ""
        BindTM()
    End Sub

    Private Sub txt_Search_TextChanged(sender As Object, e As EventArgs) Handles txt_Search_Name.TextChanged, txt_Search_Serial.TextChanged
        BindTM()
    End Sub
#End Region

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        lblUpdateMode.Text = "Create"
        lblUpdateMode.Attributes("INSM_ID") = 0

        '-----------------------------------
        pnlListTM.Enabled = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        If txtName.Text = "" Then
            lblValidation.Text = "Please insert instrument name "
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtSerial.Text = "" Then
            lblValidation.Text = "Please insert serial number "
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_INSM_Type.SelectedIndex = 0 Then
            lblValidation.Text = "Please select instrument type "
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim INSM_ID As Integer = lblUpdateMode.Attributes("INSM_ID")

        Dim SQL As String = "SELECT * FROM MS_TM_Instrument WHERE INSM_NAME='" & txtName.Text.Replace("'", "''") & "' AND INSM_SERIAL='" & txtSerial.Text.Replace("'", "''") & "' AND INSM_ID<>" & INSM_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This instrument is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_TM_Instrument WHERE INSM_ID=" & INSM_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            INSM_ID = GetNewISNMID()
            DR("INSM_ID") = INSM_ID
        Else
            DR = DT.Rows(0)
        End If

        DR("INSM_NAME") = txtName.Text
        DR("INSM_SERIAL") = txtSerial.Text
        DR("INSM_Type_ID") = ddl_INSM_Type.Items(ddl_INSM_Type.SelectedIndex).Value
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetTM(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

        '------------ Focus Last Edit -----------
        'DT = Session("PIPE_TM")
        'DT.DefaultView.RowFilter = "INSM_ID=" & INSM_ID

    End Sub

    Private Function GetNewISNMID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(INSM_ID),0)+1 FROM MS_TM_Instrument "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

End Class