﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Improvement_Report_Tag.aspx.vb" Inherits="EIR.Dashboard_Improvement_Report_Tag" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2>Problem Improved By Plant</h2>
<table cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
  <tr>
	<td>          
        <asp:LinkButton ID="lblBack" runat="server" Text="Back to see all month"></asp:LinkButton>
    </td>
  </tr>
  <tr>
    <td>
         <table border="0" cellpadding="0" cellspacing="0" style="border:1px solid #efefef; width:700px;">
              <tr>
	            <td style="background-color:#00cc33; text-align:center; color:White; font-weight:bold;" colspan="3">          
                    <asp:Label ID="lblHead" runat="server" ></asp:Label>
                </td>
              </tr>
              <tr>
                  <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; border-bottom:1px solid #003366;">
                      Tag</td>
                  <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; border-bottom:1px solid #003366;">
                      Inspection</td>
                  <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; border-bottom:1px solid #003366;">
                      Equipment Category</td>
              </tr>
              <asp:Repeater ID="rptData" runat="server">
                <ItemTemplate>
                    <tr id="tbTag" runat="server" style="cursor:pointer; border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                      <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                          <asp:Label ID="lblTag" runat="server"></asp:Label></td>
                      <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid;padding-bottom:5px; padding-top:5px;">
                          <asp:Label ID="lblIns" runat="server" ></asp:Label></td>
                      <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid;padding-bottom:5px; padding-top:5px;">
                          <asp:Label ID="lblEqm" runat="server" ></asp:Label></td>    
                    </tr>
                </ItemTemplate>
              </asp:Repeater>
          </table>
    </td>
  </tr>
</table>
</asp:Content>