﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Rotating_Routine_Edit3.aspx.vb" Inherits="EIR.Rotating_Routine_Edit3" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register src="GL_Mapping_eMonitor.ascx" tagname="GL_Mapping_eMonitor" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<!-- Page Head -->
       
<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>

	    
			<h2>Edit Rotating Routine Report</h2>
			<asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" style="visibility:hidden;" />
            <cc1:AsyncFileUpload ID="ful1" runat="server" CssClass="button" Width="0px" Height="0px" style="visibility:hidden;" />
            <div class="clear"></div> <!-- End .clear -->
		
		
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server">Report Detail</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabVibration" runat="server" CssClass="default-tab current">Vibration 
                            Analysis</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p style="font-weight:bold;">
								<label class="column-left" style="width:120px;" >Report for: </label>
								<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
								<asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Round <asp:Label ID="lbl_Round" runat="server" Text="Round" CssClass="EditReportHeader"></asp:Label>
								    | Period <asp:Label ID="lbl_Period" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								</p>
								
								<ul class="shortcut-buttons-set">
							      <li>
							        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear all
							          </span>
							        </asp:LinkButton>
							      </li>
							      <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all vibration information for this report permanently?">
                                      </cc1:ConfirmButtonExtender>
								  <li>
								    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" width="48" height="48" /><br />
									    Reset this tab
									    </span>
								    </asp:LinkButton>
								  </li>
								  
								  <li>
								    <asp:LinkButton ID="lnkISO" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/iso_color.png" alt="icon" width="48" height="48" /><br />
									        ISO 10816
								        </span>
								    </asp:LinkButton>
								  </li>
								  
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								  </li>
								  
								 <li>
								    <asp:LinkButton ID="lnkSave" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/save_48.png" alt="icon" width="48" height="48" /><br />
									        Save
								        </span>
								    </asp:LinkButton>
								  </li>
					        </ul>
								
							  
                                <table>
                                  <thead>
                                    <tr>
                                      <th colspan="8" align="center">Vibration Condition Monitoring
									  <br>
									  
									  </th>
                                    </tr>
									</thead>
								 </table>
								 <table class="table_vibration">
                                    <tr>
                                      <th align="center" style="width:150px; text-align:center !important; font-weight:bold; font-size:10px;">
                                          Tag No. </th>
                                      <th align="center" style="text-align:center !important; font-weight:bold; font-size:10px;">
                                          Zone</th>
                                      <th align="center" style="width:80px; text-align:center !important; font-weight:bold; font-size:10px;">
                                          Location</th>
                                      <th align="center" style="width:50px; text-align:center !important; font-weight:bold; font-size:10px;">
                                          Position</th>
                                      <th align="center" style="width:50px; text-align:center !important; font-weight:bold; font-size:10px;">
                                          Data Type</th>
                                      <th align="center" style="width:50px; text-align:center !important; font-weight:bold; font-size:10px;">
                                          Value</th>
                                      <th align="center" style="width:50px; text-align:center !important; font-weight:bold; font-size:10px;">
                                          Unit</th>
                                      <th align="center" style="width:50px; text-align:center !important; font-weight:bold; font-size:10px;">
                                          % Change </th>
                                      <th align="center" style="text-align:center !important; font-weight:bold; font-size:10px;">
                                          Analysis</th>
                                      <th align="center" style="text-align:center !important; font-weight:bold; font-size:10px;">
                                          Comment</th>
                                      <th align="center" style="width:50px; text-align:center !important; font-weight:bold; font-size:10px;">
                                          Action</th>
                                    </tr>
                                  
                                  
                                  <asp:Repeater ID="rptVibration" runat="server">
                                  <ItemTemplate>
                                    <asp:UpdatePanel ID="udpVibration" runat="server">
                                    <ContentTemplate>
                                          <tr id="TRClass" runat="server" style="height:25px;">
                                              <td style="width:150px;">
                                              <asp:DropDownList ID="ddlTag" Font-Size="10px" runat="server" Width="100%" style="border:none;" ></asp:DropDownList>
                                             </td>
                                              <td style="width:50px;" id="TDZone" runat="server">
                                              <asp:TextBox ID="txtZone" runat="server" Width="50px" Font-Size="10px" Height="100%" BorderStyle="None" BorderWidth="0px"></asp:TextBox></td>
                                              <td style="width:80px;"> 
                                              <asp:TextBox ID="txtLocation" Font-Size="10px" runat="server" Width="100%" MaxLength="50" Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                              <td style="width:50px;"> 
                                              <asp:TextBox ID="txtPosition" Font-Size="10px" runat="server" Width="100%" MaxLength="50" Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                              <td style="width:80px;"> 
                                              <asp:TextBox ID="txtDataType" Font-Size="10px" runat="server" Width="100%" MaxLength="10" Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                              <td style="width:50px;"> 
                                              <asp:TextBox ID="txtOverall" Font-Size="10px" Height="100%" runat="server" Width="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                              <td style="width:50px;"> 
                                              <asp:TextBox ID="txtUnit" Font-Size="10px" Height="100%" runat="server" Width="50px" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                              <td style="width:50px;"> 
                                              <asp:TextBox ID="txtPercentChanged" Font-Size="10px"  runat="server" Width="100%" Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                              <td> 
                                              <asp:TextBox ID="txtAnalysis" runat="server" Width="98%" Font-Size="10px"  Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                              <td> <asp:TextBox ID="txtComment" runat="server" Width="98%" Font-Size="10px"  Height="100%" BorderStyle="None" BorderWidth="0px"> </asp:TextBox></td>
                                              <td style="width:40px; text-align:center;" align="center"><!-- Icons -->
                                                        <asp:ImageButton ID="btnMapISO" CommandName="Map" runat="server" ImageUrl="resources/images/icons/colorMgt.png" />                                                        
                                                        <%--<asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/cross.png" />--%>  
                                                        <input type="button" ID="btnDelete" runat="server" style="background-image:url('resources/images/icons/cross.png'); border:none; background-color:White; width:16px; height:16px; cursor:pointer;" title="Click to remove this row" />
                                                        <asp:CheckBox ID="chk_Delete" runat="server" style="position:absolute; visibility:hidden" />                                                      
                                               </td>
                                            </tr>
                                    </ContentTemplate>
                                    </asp:UpdatePanel>
                                  </ItemTemplate>                                  
                                  </asp:Repeater>                                 
								  <tfoot>
                                    <tr>
                                      <td colspan="11"><div class="bulk-actions align-left">                                      
                                      <asp:Button CssClass="button" ID="btnAddRow" runat="server" Text="Add Row" />
                                      <asp:Button CssClass="button" ID="btnISO" runat="server" Text="ISO 10816" />
                                      <asp:Button CssClass="button" ID="btnUpdateColor" runat="server" Text="Update Zone Color" />
                                      <asp:Button CssClass="button" ID="btnClearAll" runat="server" Text="Clear All" />
                                      <asp:Button CssClass="button" ID="btnSave" runat="server" Text="Save"  />
                                      <asp:Button CssClass="button" ID="btnEMonitor" runat="server" Text="From eMonitor" />
                                      </div>
                                        <!-- End .pagination -->
                                          <div class="clear"></div></td>
                                    </tr>
                                  </tfoot>
                                </table>
						
							<p align="right">
								<asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
								<asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
							</p>
								
							
								
							</fieldset>
				    </div>
				  <!-- End #tabDetail -->        
		          
                    <asp:Panel Visible="false" ID="pnlISO" runat="server" CssClass="content-box column-right closed-box" Width="403px" Style="position:absolute; left:0px; top:0px;"  >
                            
                            <div class="content-box-header"> <!-- Add the class "closed" to the Content box header to have it closed by default -->
							<h3 style="width:90%">Evaluation Zone:ISO 10816
							<asp:ImageButton ID="btnCloseISO" runat="server" CssClass="close" ImageUrl="resources/images/icons/cross_grey_small.png" style="position:relative; left:150px; top:-5px;" ToolTip="Close" />							
							</h3>							
							</div>
							<table width="400" border="0" cellpadding="0" cellspacing="0" class="table_ISO">
                                 
                                  <tr>
                                    <td rowspan="2">Vibration Limit<br>
                                        (mm/s - rms.) </td>
                                    <td colspan="3">Motor</td>
                                    <td rowspan="2">Pump &gt; 15kW<br>Ext-CP Rigid</td>
                                    <td rowspan="2"><br>FAN</td>
                                    <td rowspan="2">Bearing<br>(G's-Peak)</td>
                                  </tr>
                                  <tr>
                                    <td>Class I </td>
                                    <td>Class II </td>
                                    <td>Class III </td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">28.0 - 45.0</td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_01" runat="server" Text="D5"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_02" runat="server" Text="D4"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_03" runat="server" Text="D3"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_04" runat="server" Text="D4"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_05" runat="server" Text="D3"></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_06" runat="server" Text="D5 - 17.90"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">18.0 - 28.0 </td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_07" runat="server" Text="D4"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_08" runat="server" Text="D3"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_09" runat="server" Text="D2"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_10" runat="server" Text="D3"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_11" runat="server" Text="D2"></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_12" runat="server" Text="D4 - 11.31"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">11.2 - 18.0 </td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_13" runat="server" Text="D3"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_14" runat="server" Text="D2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_15" runat="server" Text="D1"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_16" runat="server" Text="D2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_17" runat="server" Text="D1"></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_18" runat="server" Text="D3 - 7.14"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">7.1 - 11.2 </td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_19" runat="server" Text="D2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_20" runat="server" Text="D1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_21" runat="server" Text="C2"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_22" runat="server" Text="D1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_23" runat="server" Text="C1"></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_24" runat="server" Text="D2 - 4.50"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">4.5 - 7.1 </td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_25" runat="server" Text="D1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_26" runat="server" Text="C2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_27" runat="server" Text="C1"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_28" runat="server" Text="C1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_29" runat="server" Text="B3"></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_30" runat="server" Text="D1 - 2.84"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">2.8 - 4.5 </td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_31" runat="server" Text="C2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_32" runat="server" Text="C1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_33" runat="server" Text="B2"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_34" runat="server" Text="B2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_35" runat="server" Text="B2" ></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_36" runat="server" Text="C2 - 1.79"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">1.8 - 2.8 </td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_37" runat="server" Text="C1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_38" runat="server" Text="B2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_39" runat="server" Text="B1"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_40" runat="server" Text="B1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_41" runat="server" Text="B1"></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_42" runat="server" Text="C1 - 1.13"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">1.12 - 1.8 </td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_43" runat="server" Text="B2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_44" runat="server" Text="B1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_45" runat="server" Text="A4"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_46" runat="server" Text="A4"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_47" runat="server" Text="A4"></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_48" runat="server" Text="B2 - 0.72"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">0.71 - 1.12 </td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_49" runat="server" Text="B1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_50" runat="server" Text="A3"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_51" runat="server" Text="A3"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_52" runat="server" Text="A3"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_53" runat="server" Text="A3"></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_54" runat="server" Text="B1 - 0.45"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">0.3 - 0.71 </td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_55" runat="server" Text="A2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_56" runat="server" Text="A2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_57" runat="server" Text="A2"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_58" runat="server" Text="A2"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_59" runat="server" Text="A2"></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_60" runat="server" Text="A2 - 0.28"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td style="width:75px;">0 - 0.3 </td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_61" runat="server" Text="A1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_62" runat="server" Text="A1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_63" runat="server" Text="A1"></asp:button></td>
                                    <td><asp:button Width="65px" CssClass="button_ISO" ID="btnISO_64" runat="server" Text="A1"></asp:button></td>
                                    <td><asp:button Width="50px" CssClass="button_ISO" ID="btnISO_65" runat="server" Text="A1"></asp:button></td>
                                    <td><asp:button Width="70px" CssClass="button_ISO" ID="btnISO_66" runat="server" Text="A1 - 0.18"></asp:button></td>
                                  </tr>
                                  <tr>
                                    <td colspan="7" style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;Note : Frequency Rang = 10 Hz - 1000 Hz </td>
                                  </tr>
                                  <tr>
                                    <td class="btnISO_A" style="width:70px;">A</td>
                                    <td colspan="6">Newly Commissioned </td>
                                  </tr>
                                  <tr>
                                    <td class="btnISO_B" style="width:70px;">B</td>
                                    <td colspan="6">Unrestricted long - team operation </td>
                                  </tr>
                                  <tr>
                                    <td class="btnISO_C" style="width:70px;">C</td>
                                    <td colspan="6">Restricted long - team operation</td>
                                  </tr>
                                  <tr>
                                    <td class="btnISO_D" style="width:70px;">D</td>
                                    <td colspan="6">Vibration causes damage </td>
                                  </tr>
                                </table>
                            </asp:Panel>
                    <cc1:DragPanelExtender ID="pnlISO_DragPanelExtender" runat="server" TargetControlID="pnlISO" DragHandleID="pnlISO" >
                    </cc1:DragPanelExtender>
                  <%--  <cc1:ResizableControlExtender ID="pnlISO_ResizableControlExtender" runat="server" TargetControlID="pnlISO" BehaviorID="pnlISO" MinimumWidth="365" MaximumWidth="600" MinimumHeight="300" HandleCssClass="">
                    </cc1:ResizableControlExtender>--%>
		       	 <asp:Panel ID="pnlValidation" runat="server" CssClass="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
		         
	              
			        
		         
	              
			  </div> <!-- End .content-box-content -->
				
				
				<uc1:GL_Mapping_eMonitor ID="Mapper" runat="server" />
				
			</div> <!-- End .content-box -->
			<asp:TextBox ID="txtPosX" runat="server" Text="0" style="visibility:hidden; width:0px; height:0px;" ></asp:TextBox>
			<asp:TextBox ID="txtPosY" runat="server" Text="0"  style="visibility:hidden; width:0px; height:0px;" ></asp:TextBox>
            
</ContentTemplate>
</asp:UpdatePanel>  	
</asp:Content>
