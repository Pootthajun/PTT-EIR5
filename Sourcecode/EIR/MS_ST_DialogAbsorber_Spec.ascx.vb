﻿Imports System.Data
Imports System.Data.SqlClient

Public Class MS_ST_DialogAbsorber_Spec
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Dim C As New Converter

#Region "Property"

    Public Enum PropertyItem
        'All = 0
        'Plant = 1
        'Service = 2
        'Material = 3
        'Equipement = 4
        'Equipement_ID = 5
        'Initial_Year = 6
        'Pipe_Size = 7
        'Pipe_Position = 8
        'Pipe_Location = 9
        'Schedule = 10
        'P_ID_No = 11
        'Norminal_Thickness = 12
        'Minimum_Thickness = 13
        'Corrosion_Allowance = 14
        'Design_Pressure = 15
        'Operating_Pressure = 16
        'Design_Temperature = 17
        'Operting_Temperature = 18

        All = 0
        txt_CODE = 1
        txt_CODE_STAMP = 2
        txt_CORROSION_ALLOWANCE = 3
        txt_JOINT_EFFICIENCY = 4
        txt_POST_WELD_HEAT_TREATMENT = 5
        txt_RADIOGRAPHY_MIN = 6
        '----txt_RADIOGRAPHY_MAX =7
        txt_PNEUMATIC_TEST_PRESS = 8
        txt_HYDRO_TEST_PRESSURE_AT_SHOP = 9
        txt_HYDRO_TEST_PRESSURE_AT_FIELD = 10
        txt_DESIGN_TEMPERATURE_MIN = 11
        '----txt_DESIGN_TEMPERATURE_MAX =12
        txt_MIN_DESIGN_METAL_TEMPERATURE = 13
        txt_DESIGN_INTERNAL_PRESSURE = 14
        txt_DESIGN_EXTERNAL_PRESSURE = 15
        txt_OPERATING_TEMPERATURE_MIN = 16
        '----txt_OPERATING_TEMPERATURE_MAX =17
        txt_OPERATING_PRESSURE_MIN = 18

        '----txt_OPERATING_PRESSURE_MAX=19
        txt_PRESSURE_DROP_THROUGH_INTERNALS = 20
        txt_STATIC_VAPOR_HEAD = 21
        txt_CONTENTS = 22
        txt_LIQUID_SPECIFIC_GRAVITY = 23
        txt_LENGTH_BETWEEN_TANGENT_LINE = 24
        txt_SHELL_INSIDE_DIAMETER = 25
        txt_TYPE_OF_HEADS = 26
        txt_WIND_LOAD = 27
        txt_SEISMIC_LOAD = 28
        txt_INSULATION_THICKNESS = 29
        txt_FIRE_PROOFING_THICKNESS = 30
        txt_REFRACTORY_LINING_THICKNESS = 31
        txt_VOLUME = 32

        txt_Norminal_Thickness = 33
        txt_Calculated_Thickness = 34

        Drawing = 100
    End Enum

    Public Class DrawingDetail
        Public File_ID As Integer = 0
        Public File_Type As String = ""
        Public File_Data As Byte() = {}
    End Class

    Public Property TAG_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("TAG_ID")) Then
                Return 0
            Else
                Return Me.Attributes("TAG_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("TAG_ID") = value
        End Set
    End Property

    Public Property TAG_TYPE_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("TAG_TYPE_ID")) Then
                Return 0
            Else
                Return Me.Attributes("TAG_TYPE_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("TAG_TYPE_ID") = value
        End Set
    End Property

    Public Property TAG_Name() As String
        Get
            Return lblTag.Attributes("TAG_Name")
        End Get
        Set(ByVal value As String)
            lblTag.Attributes("TAG_Name") = value
        End Set
    End Property

    Public Property TAG_TYPE_Name() As String
        Get
            Return lblTag.Attributes("TAG_TYPE_Name")
        End Get
        Set(ByVal value As String)
            lblTag.Attributes("TAG_TYPE_Name") = value
        End Set
    End Property

    Public Property TO_TABLE_MS() As String
        Get
            Return lblTag.Attributes("TO_TABLE_MS")
        End Get
        Set(ByVal value As String)
            lblTag.Attributes("TO_TABLE_MS") = value
        End Set
    End Property

    Public Property TO_TABLE_SPEC() As String
        Get
            Return lblTag.Attributes("TO_TABLE_SPEC")
        End Get
        Set(ByVal value As String)
            lblTag.Attributes("TO_TABLE_SPEC") = value
        End Set
    End Property

    Public Property TAG_CODE() As String
        Get
            Return lblTag.Attributes("TAG_CODE")
        End Get
        Set(ByVal value As String)
            lblTag.Attributes("TAG_CODE") = value
        End Set
    End Property

    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return ViewState("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Public Property New_TagID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("New_TagID")) Then
                Return 0
            Else
                Return Me.Attributes("New_TagID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("New_TagID") = value
        End Set
    End Property

    Public Sub SetPropertyEditable(ByVal PropertyItem As PropertyItem, ByVal Editable As Boolean)

        Select Case PropertyItem
            Case PropertyItem.Drawing
                btnAddFile.Visible = Editable
                rptDrawing.DataSource = DrawingList
                rptDrawing.DataBind()
        End Select
    End Sub

    Public Function GetPropertyEditable(ByVal PropertyItem As PropertyItem) As Boolean
        Select Case PropertyItem

            Case PropertyItem.Drawing
                Return btnAddFile.Visible
            Case Else ' PropertyItem.All

                Return True
        End Select
    End Function

    Public Property Initial_Year As Object
        Get
            If IsNumeric(txt_Initial_Year.Text.Replace(",", "")) Then
                Return CInt(txt_Initial_Year.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Initial_Year.Text = CInt(value)
            Else
                txt_Initial_Year.Text = ""
            End If
        End Set
    End Property



#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then
            UNIQUE_POPUP_ID = Now.ToOADate.ToString.Replace(".", "")
            btnAddFile.Attributes("onclick") = "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "',0,document.getElementById('" & btnUploadFile.ClientID & "'));"
            ImplementJavascriptControl()
        End If
        pnlValidation.Visible = False
        pnlBindingSuccess.Visible = False

    End Sub

    Public Sub Clear_txt()

        Initial_Year = Nothing
        'txt_CODE.Text = ""
        txt_CODE_STAMP.Text = ""
        txt_CORROSION_ALLOWANCE.Text = ""
        txt_JOINT_EFFICIENCY.Text = ""
        txt_POST_WELD_HEAT_TREATMENT.Text = ""
        txt_RADIOGRAPHY_MIN.Text = ""
        'txt_RADIOGRAPHY_MAX.Text = ""
        txt_PNEUMATIC_TEST_PRESS.Text = ""
        txt_HYDRO_TEST_PRESSURE_AT_SHOP.Text = ""
        txt_HYDRO_TEST_PRESSURE_AT_FIELD.Text = ""
        txt_DESIGN_TEMPERATURE_MIN.Text = ""
        'txt_DESIGN_TEMPERATURE_MAX.Text = ""
        txt_MIN_DESIGN_METAL_TEMPERATURE.Text = ""
        txt_DESIGN_INTERNAL_PRESSURE.Text = ""
        txt_DESIGN_EXTERNAL_PRESSURE.Text = ""
        txt_OPERATING_TEMPERATURE_MIN.Text = ""
        'txt_OPERATING_TEMPERATURE_MAX.Text = ""
        txt_OPERATING_PRESSURE_MIN.Text = ""
        'txt_OPERATING_PRESSURE_MAX.Text = ""
        txt_PRESSURE_DROP_THROUGH_INTERNALS.Text = ""
        txt_STATIC_VAPOR_HEAD.Text = ""
        txt_CONTENTS.Text = ""
        txt_LIQUID_SPECIFIC_GRAVITY.Text = ""
        txt_LENGTH_BETWEEN_TANGENT_LINE.Text = ""
        txt_SHELL_INSIDE_DIAMETER.Text = ""
        txt_TYPE_OF_HEADS.Text = ""
        txt_WIND_LOAD.Text = ""
        txt_SEISMIC_LOAD.Text = ""
        txt_INSULATION_THICKNESS.Text = ""
        txt_FIRE_PROOFING_THICKNESS.Text = ""
        txt_REFRACTORY_LINING_THICKNESS.Text = ""
        txt_VOLUME.Text = ""

        txt_Norminal_Thickness.Text = ""
        txt_Calculated_Thickness.Text = ""
        'txt_DRAWING_1.Text = ""
        'txt_DRAWING_2.Text = ""
        'txt_Update_By.Text = ""
        'txt_Update_Time.Text = ""

        DrawingList.Clear()
        rptDrawing.DataSource = DrawingList
        rptDrawing.DataBind()

    End Sub

    Private Sub ImplementJavascriptControl()

        ImplementJavaIntegerText(txt_Initial_Year, False,, "Center")
        'txt_CODE.Text = ""
        ImplementJavaNumericText(txt_CODE_STAMP, "Center")
        ImplementJavaNumericText(txt_CORROSION_ALLOWANCE, "Center")
        ImplementJavaNumericText(txt_JOINT_EFFICIENCY, "Center")
        ImplementJavaNumericText(txt_POST_WELD_HEAT_TREATMENT, "Center")
        ImplementJavaNumericText(txt_RADIOGRAPHY_MIN, "Center")
        'txt_RADIOGRAPHY_MAX.Text = ""
        ImplementJavaNumericText(txt_PNEUMATIC_TEST_PRESS, "Center")
        ImplementJavaNumericText(txt_HYDRO_TEST_PRESSURE_AT_SHOP, "Center")
        ImplementJavaNumericText(txt_HYDRO_TEST_PRESSURE_AT_FIELD, "Center")
        ImplementJavaNumericText(txt_DESIGN_TEMPERATURE_MIN, "Center")
        'txt_DESIGN_TEMPERATURE_MAX.Text = ""
        ImplementJavaNumericText(txt_MIN_DESIGN_METAL_TEMPERATURE, "Center")
        ImplementJavaNumericText(txt_DESIGN_INTERNAL_PRESSURE, "Center")
        ImplementJavaNumericText(txt_DESIGN_EXTERNAL_PRESSURE, "Center")
        ImplementJavaNumericText(txt_OPERATING_TEMPERATURE_MIN, "Center")
        'txt_OPERATING_TEMPERATURE_MAX, "Center")
        ImplementJavaNumericText(txt_OPERATING_PRESSURE_MIN, "Center")
        'txt_OPERATING_PRESSURE_MAX, "Center")
        ImplementJavaNumericText(txt_PRESSURE_DROP_THROUGH_INTERNALS, "Center")
        ImplementJavaNumericText(txt_STATIC_VAPOR_HEAD, "Center")

        ImplementJavaNumericText(txt_LIQUID_SPECIFIC_GRAVITY, "Center")
        ImplementJavaNumericText(txt_LENGTH_BETWEEN_TANGENT_LINE, "Center")
        ImplementJavaNumericText(txt_SHELL_INSIDE_DIAMETER, "Center")
        ImplementJavaNumericText(txt_TYPE_OF_HEADS, "Center")
        ImplementJavaNumericText(txt_WIND_LOAD, "Center")
        ImplementJavaNumericText(txt_SEISMIC_LOAD, "Center")
        ImplementJavaNumericText(txt_INSULATION_THICKNESS, "Center")
        ImplementJavaNumericText(txt_FIRE_PROOFING_THICKNESS, "Center")
        ImplementJavaNumericText(txt_REFRACTORY_LINING_THICKNESS, "Center")
        ImplementJavaNumericText(txt_VOLUME, "Center")

        ImplementJavaNumericText(txt_Norminal_Thickness, "Center")
        ImplementJavaNumericText(txt_Calculated_Thickness, "Center")

        'txt_DRAWING_1.Text = ""
        'txt_DRAWING_2.Text = ""
        'txt_Update_By.Text = ""
        'txt_Update_Time.Text = ""

    End Sub

    Public Sub BindHeader()

        ''ClearDetail()

        'Dim SQL As String = " Select TAG.TAG_CODE,TAG_No,TAG.TAG_ID,TAG_Name,TAG_TYPE_ID,TAG_TYPE_Name,Active_Status,PLANT_ID " & vbNewLine
        'SQL &= ",SPEC.* " & vbNewLine
        'SQL &= "From VW_ST_TA_TAG TAG " & vbNewLine
        'SQL &= "Left Join  " & TO_TABLE & " SPEC On SPEC.TAG_ID = TAG.TAG_ID " & vbNewLine
        'SQL &= "WHERE TAG.TAG_ID = " & TAG_ID & " And TAG.TAG_TYPE_ID = " & TAG_TYPE_ID & " " & vbNewLine



        'Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        'Dim DT As New DataTable
        'DA.Fill(DT)
        'If DT.Rows.Count = 0 Then Exit Sub

        'TAG_Name = DT.Rows(0).Item("TAG_NAME")
        'TAG_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")
        'TAG_TYPE_Name = DT.Rows(0).Item("TAG_TYPE_NAME")

        'lblTag.Text = TAG_CODE & " " & TAG_Name
        'lbl_TagType.Text = TAG_TYPE_Name



        '-------------ตรวจสอบเป็น TAG จาก Station หรือ Turnaround-----------------------

        Dim SQL As String = ""
        SQL = " "
        SQL &= " SELECT * FROM VW_ST_TA_TAG " & vbNewLine

        SQL &= " WHERE TAG_ID=" & TAG_ID

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            Me.CloseDialog()
            Exit Sub
        End If

        Dim PLANT_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then PLANT_ID = DT.Rows(0).Item("PLANT_ID")
        Dim ROUTE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("ROUTE_ID")) Then ROUTE_ID = DT.Rows(0).Item("ROUTE_ID")
        Dim AREA_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("AREA_ID")) Then AREA_ID = DT.Rows(0).Item("AREA_ID")
        Dim PROC_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PROC_ID")) Then PROC_ID = DT.Rows(0).Item("PROC_ID")
        Dim TAG_TYPE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("TAG_TYPE_ID")) Then TAG_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")

        BL.BindDDlPlant(ddl_Edit_Plant, PLANT_ID)
        BL.BindDDl_ST_Route(PLANT_ID, ddl_Edit_Route, ROUTE_ID)
        BL.BindDDlArea(PLANT_ID, ddl_Edit_Area, AREA_ID)
        BL.BindDDlProcess(ddl_Edit_Process, PROC_ID)
        BL.BindDDlST_TA_TagType(ddl_Edit_Type, TAG_TYPE_ID)
        txtTagNo.Text = DT.Rows(0).Item("TAG_NO")
        txtTagNo.Attributes("TagID") = TAG_ID
        txtTagName.Text = DT.Rows(0).Item("TAG_NAME")

        'txtDesc.Text = DT.Rows(0).Item("TAG_Description")
        'chkAvailable.Checked = DT.Rows(0).Item("Active_Status")

        BindData(TAG_ID)

    End Sub


    Public Sub BindData(ByVal TAG_ID As Integer)

        Clear_txt()
        Dim SQL As String = " SELECT * FROM " & TO_TABLE_SPEC & " "
        SQL &= " WHERE TAG_ID=" & TAG_ID

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        Dim DR As DataRow = DT.Rows(0)

        Me.TAG_ID = DR("TAG_ID")

        Initial_Year = DR("Initial_Year")

        If Not IsDBNull(DT.Rows(0).Item("CODE_STAMP")) Then txt_CODE_STAMP.Text = FormatNumber(DR("CODE_STAMP"))
        If Not IsDBNull(DT.Rows(0).Item("CORROSION_ALLOWANCE")) Then txt_CORROSION_ALLOWANCE.Text = FormatNumber(DR("CORROSION_ALLOWANCE"))
        If Not IsDBNull(DT.Rows(0).Item("JOINT_EFFICIENCY")) Then txt_JOINT_EFFICIENCY.Text = FormatNumber(DR("JOINT_EFFICIENCY"))
        If Not IsDBNull(DT.Rows(0).Item("POST_WELD_HEAT_TREATMENT")) Then txt_POST_WELD_HEAT_TREATMENT.Text = FormatNumber(DR("POST_WELD_HEAT_TREATMENT"))
        If Not IsDBNull(DT.Rows(0).Item("RADIOGRAPHY_MIN")) Then txt_RADIOGRAPHY_MIN.Text = FormatNumber(DR("RADIOGRAPHY_MIN"))
        'FormatNumber(DR("RADIOGRAPHY_MAX"))
        If Not IsDBNull(DT.Rows(0).Item("PNEUMATIC_TEST_PRESS")) Then txt_PNEUMATIC_TEST_PRESS.Text = FormatNumber(DR("PNEUMATIC_TEST_PRESS"))
        If Not IsDBNull(DT.Rows(0).Item("HYDRO_TEST_PRESSURE_AT_SHOP")) Then txt_HYDRO_TEST_PRESSURE_AT_SHOP.Text = FormatNumber(DR("HYDRO_TEST_PRESSURE_AT_SHOP"))
        If Not IsDBNull(DT.Rows(0).Item("HYDRO_TEST_PRESSURE_AT_FIELD")) Then txt_HYDRO_TEST_PRESSURE_AT_FIELD.Text = FormatNumber(DR("HYDRO_TEST_PRESSURE_AT_FIELD"))
        If Not IsDBNull(DT.Rows(0).Item("DESIGN_TEMPERATURE_MIN")) Then txt_DESIGN_TEMPERATURE_MIN.Text = FormatNumber(DR("DESIGN_TEMPERATURE_MIN"))
        'FormatNumber(DR("DESIGN_TEMPERATURE_MAX"))
        If Not IsDBNull(DT.Rows(0).Item("MIN_DESIGN_METAL_TEMPERATURE")) Then txt_MIN_DESIGN_METAL_TEMPERATURE.Text = FormatNumber(DR("MIN_DESIGN_METAL_TEMPERATURE"))
        If Not IsDBNull(DT.Rows(0).Item("DESIGN_INTERNAL_PRESSURE")) Then txt_DESIGN_INTERNAL_PRESSURE.Text = FormatNumber(DR("DESIGN_INTERNAL_PRESSURE"))
        If Not IsDBNull(DT.Rows(0).Item("DESIGN_EXTERNAL_PRESSURE")) Then txt_DESIGN_EXTERNAL_PRESSURE.Text = FormatNumber(DR("DESIGN_EXTERNAL_PRESSURE"))
        If Not IsDBNull(DT.Rows(0).Item("OPERATING_TEMPERATURE_MIN")) Then txt_OPERATING_TEMPERATURE_MIN.Text = FormatNumber(DR("OPERATING_TEMPERATURE_MIN"))
        'FormatNumber(DR("OPERATING_TEMPERATURE_MAX"))
        If Not IsDBNull(DT.Rows(0).Item("OPERATING_PRESSURE_MIN")) Then txt_OPERATING_PRESSURE_MIN.Text = FormatNumber(DR("OPERATING_PRESSURE_MIN"))
        'FormatNumber(DR("OPERATING_PRESSURE_MAX"))
        If Not IsDBNull(DT.Rows(0).Item("PRESSURE_DROP_THROUGH_INTERNALS")) Then txt_PRESSURE_DROP_THROUGH_INTERNALS.Text = FormatNumber(DR("PRESSURE_DROP_THROUGH_INTERNALS"))
        If Not IsDBNull(DT.Rows(0).Item("STATIC_VAPOR_HEAD")) Then txt_STATIC_VAPOR_HEAD.Text = FormatNumber(DR("STATIC_VAPOR_HEAD"))
        If Not IsDBNull(DT.Rows(0).Item("CONTENTS")) Then txt_CONTENTS.Text = DR("CONTENTS").ToString()
        If Not IsDBNull(DT.Rows(0).Item("LIQUID_SPECIFIC_GRAVITY")) Then txt_LIQUID_SPECIFIC_GRAVITY.Text = FormatNumber(DR("LIQUID_SPECIFIC_GRAVITY"))
        If Not IsDBNull(DT.Rows(0).Item("LENGTH_BETWEEN_TANGENT_LINE")) Then txt_LENGTH_BETWEEN_TANGENT_LINE.Text = FormatNumber(DR("LENGTH_BETWEEN_TANGENT_LINE"))
        If Not IsDBNull(DT.Rows(0).Item("SHELL_INSIDE_DIAMETER")) Then txt_SHELL_INSIDE_DIAMETER.Text = FormatNumber(DR("SHELL_INSIDE_DIAMETER"))
        If Not IsDBNull(DT.Rows(0).Item("TYPE_OF_HEADS")) Then txt_TYPE_OF_HEADS.Text = FormatNumber(DR("TYPE_OF_HEADS"))
        If Not IsDBNull(DT.Rows(0).Item("WIND_LOAD")) Then txt_WIND_LOAD.Text = FormatNumber(DR("WIND_LOAD"))
        If Not IsDBNull(DT.Rows(0).Item("SEISMIC_LOAD")) Then txt_SEISMIC_LOAD.Text = FormatNumber(DR("SEISMIC_LOAD"))
        If Not IsDBNull(DT.Rows(0).Item("INSULATION_THICKNESS")) Then txt_INSULATION_THICKNESS.Text = FormatNumber(DR("INSULATION_THICKNESS"))
        If Not IsDBNull(DT.Rows(0).Item("FIRE_PROOFING_THICKNESS")) Then txt_FIRE_PROOFING_THICKNESS.Text = FormatNumber(DR("FIRE_PROOFING_THICKNESS"))
        If Not IsDBNull(DT.Rows(0).Item("REFRACTORY_LINING_THICKNESS")) Then txt_REFRACTORY_LINING_THICKNESS.Text = FormatNumber(DR("REFRACTORY_LINING_THICKNESS"))
        If Not IsDBNull(DT.Rows(0).Item("VOLUME")) Then txt_VOLUME.Text = FormatNumber(DR("VOLUME"))

        If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then txt_Norminal_Thickness.Text = FormatNumber(DR("Norminal_Thickness"))
        If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then txt_Calculated_Thickness.Text = FormatNumber(DR("Calculated_Thickness"))


        '--------------- Load Drawing --------------
        DT = New DataTable
        DA = New SqlDataAdapter("SELECT * FROM MS_ST_ABSORBER_Drawing WHERE TAG_ID=" & TAG_ID & " ORDER BY File_ID", BL.ConnStr)
        DA.Fill(DT)
        Dim Path As String = BL.Picture_Path & "\" & "ST_TA" & "\Drawing\Absorber\" & TAG_ID & "\"
        For i As Integer = 0 To DT.Rows.Count - 1
            If Not IsDBNull(DT.Rows(i).Item("File_ID")) Then
                Dim Obj As New EIR_BL.ST_TA_DrawingDetail
                Obj.File_ID = DT.Rows(i).Item("File_ID")
                Obj.File_Type = DT.Rows(i).Item("File_Type")
                If IO.File.Exists(Path & Obj.File_ID) Then
                    Dim F As IO.FileStream = IO.File.Open(Path & Obj.File_ID, IO.FileMode.Open, IO.FileAccess.Read, IO.FileShare.ReadWrite)
                    Obj.File_Data = C.StreamToByte(F)
                    F.Close()
                    DrawingList.Add(Obj)
                End If
            End If
        Next
        rptDrawing.DataSource = DrawingList
        rptDrawing.DataBind()


    End Sub


    '-----Drawing----

    Private Sub rptDrawing_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptDrawing.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim item As HtmlGenericControl = e.Item.FindControl("item")
        Dim img_File As Image = e.Item.FindControl("img_File")
        Dim btnEdit As HtmlInputImage = e.Item.FindControl("btnEdit")
        Dim btnUpload As Button = e.Item.FindControl("btnUpload")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim pnlEditFile As HtmlGenericControl = e.Item.FindControl("pnlEditFile")

        img_File.ImageUrl = "Render_ST_TA_Drawing.aspx?Mode=session&UNIQUE_POPUP_ID=" & UNIQUE_POPUP_ID & "&ImageID=" & e.Item.ItemIndex & "&t=" & Now.ToOADate
        Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_" & DrawingList(e.Item.ItemIndex).File_ID) = DrawingList(e.Item.ItemIndex).File_Data
        btnEdit.Attributes("onclick") = "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "'," & DrawingList(e.Item.ItemIndex).File_ID & ",document.getElementById('" & btnUpload.ClientID & "'));"
        '---------- Get Image Dimension ----------
        Dim img As Drawing.Image = Drawing.Image.FromStream(C.ByteToStream(DrawingList(e.Item.ItemIndex).File_Data))
        Dim w As Integer = img.Width
        Dim h As Integer = img.Height

        Dim MustHeight As Integer = 120
        If GetPropertyEditable(PropertyItem.Drawing) Then
            btnUpload.CommandArgument = DrawingList(e.Item.ItemIndex).File_ID
            btnDelete.CommandArgument = DrawingList(e.Item.ItemIndex).File_ID
        Else
            pnlEditFile.Visible = False
            MustHeight = 150
        End If

        If w > h Then
            img_File.Width = Unit.Pixel(150)
        Else
            img_File.Height = Unit.Pixel(MustHeight)
        End If

    End Sub

    Private Sub rptDrawing_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptDrawing.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                If IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_" & DrawingList(e.Item.ItemIndex).File_ID)) Then Exit Sub
                Dim D As EIR_BL.ST_TA_DrawingDetail = DrawingList(e.Item.ItemIndex)
                D.File_Data = Session("TempImage_" & UNIQUE_POPUP_ID & "_" & D.File_ID)
                Dim img As Drawing.Image = Drawing.Image.FromStream(C.ByteToStream(D.File_Data))
                D.File_Type = BL.GetImageContentType(img)
                rptDrawing.DataSource = DrawingList
                rptDrawing.DataBind()
            Case "Delete"
                DrawingList.RemoveAt(e.Item.ItemIndex)
                rptDrawing.DataSource = DrawingList
                rptDrawing.DataBind()
        End Select
    End Sub


    Public Sub Save()
        If ddl_Edit_Area.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Area"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If ddl_Edit_Process.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Process"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtTagNo.Text = "" Then
            lblValidation.Text = "Please insert Tag number"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtTagName.Text = "" Then
            lblValidation.Text = "Please insert Tag name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Type.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Equipement-Type"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Plant.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Route.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Route"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txt_Initial_Year.Text = "" Then
            lblValidation.Text = "Please Enter Initial Year"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txt_CORROSION_ALLOWANCE.Text = "" Then
            lblValidation.Text = "Please Enter Corrosion Allowance"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txt_INSULATION_THICKNESS.Text = "" Then
            lblValidation.Text = "Please Enter Insulation Thickness"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txt_Norminal_Thickness.Text = "" Then
            lblValidation.Text = "Please Enter Norminal Thickness"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txt_Calculated_Thickness.Text = "" Then
            lblValidation.Text = "Please Enter Calculated Thickness"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim TagID As Integer = txtTagNo.Attributes("TagID")

        Dim To_Table As String = TO_TABLE_MS
        '-------------ตรวจสอบเป็น TAG จาก Station หรือ Turnaround-----------------------

        Dim SQL As String = "SELECT * FROM " & To_Table & " WHERE AREA_ID=" & ddl_Edit_Area.Items(ddl_Edit_Area.SelectedIndex).Value & " AND PROC_ID=" & ddl_Edit_Process.Items(ddl_Edit_Process.SelectedIndex).Value & " AND TAG_No='" & txtTagNo.Text.Replace("'", "''") & "' AND TAG_ID<>" & TagID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        SQL = "SELECT * FROM " & To_Table & " WHERE TAG_ID=" & TagID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            TagID = GetNewTagID(To_Table)
            DR("TAG_ID") = TagID
        Else
            DR = DT.Rows(0)
        End If

        DR("AREA_ID") = ddl_Edit_Area.Items(ddl_Edit_Area.SelectedIndex).Value
        DR("PROC_ID") = ddl_Edit_Process.Items(ddl_Edit_Process.SelectedIndex).Value
        DR("TAG_No") = txtTagNo.Text '.ToString.PadLeft(4, "0")
        DR("ROUTE_ID") = ddl_Edit_Route.Items(ddl_Edit_Route.SelectedIndex).Value
        DR("TAG_Name") = txtTagName.Text
        DR("TAG_TYPE_ID") = ddl_Edit_Type.Items(ddl_Edit_Type.SelectedIndex).Value
        'DR("TAG_Description") = txtDesc.Text
        DR("TAG_Order") = TagID
        'DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try


        '-------------Update Spec----------------

        Dim SQL_Spec As String = " SELECT * FROM " & TO_TABLE_SPEC & " "
        SQL_Spec &= " WHERE TAG_ID=" & TAG_ID

        Dim DA_Spec As New SqlDataAdapter(SQL_Spec, BL.ConnStr)
        Dim DT_Spec As New DataTable
        DA_Spec.Fill(DT_Spec)
        Dim DR_Spec As DataRow

        If DT_Spec.Rows.Count = 0 Then
            DR_Spec = DT_Spec.NewRow
        Else
            DR_Spec = DT_Spec.Rows(0)
        End If

        DR_Spec("TAG_ID") = Me.TAG_ID
        If Not IsNothing(Initial_Year) Then
            If Initial_Year > 1900 And Initial_Year <= Now.Year Then
                DR_Spec("Initial_Year") = Initial_Year
            Else
                DR_Spec("Initial_Year") = DBNull.Value
            End If
        Else
            DR_Spec("Initial_Year") = DBNull.Value
        End If
        If txt_CODE_STAMP.Text <> "" Then
            DR_Spec("CODE_STAMP") = txt_CODE_STAMP.Text
        Else
            DR_Spec("CODE_STAMP") = DBNull.Value
        End If
        If txt_CORROSION_ALLOWANCE.Text <> "" Then
            DR_Spec("CORROSION_ALLOWANCE") = txt_CORROSION_ALLOWANCE.Text
        Else
            DR_Spec("CORROSION_ALLOWANCE") = DBNull.Value
        End If
        If txt_JOINT_EFFICIENCY.Text <> "" Then
            DR_Spec("JOINT_EFFICIENCY") = txt_JOINT_EFFICIENCY.Text
        Else
            DR_Spec("JOINT_EFFICIENCY") = DBNull.Value
        End If
        If txt_POST_WELD_HEAT_TREATMENT.Text <> "" Then
            DR_Spec("POST_WELD_HEAT_TREATMENT") = txt_POST_WELD_HEAT_TREATMENT.Text
        Else
            DR_Spec("POST_WELD_HEAT_TREATMENT") = DBNull.Value
        End If
        If txt_RADIOGRAPHY_MIN.Text <> "" Then
            DR_Spec("RADIOGRAPHY_MIN") = txt_RADIOGRAPHY_MIN.Text
        Else
            DR_Spec("RADIOGRAPHY_MIN") = DBNull.Value
        End If
        If txt_PNEUMATIC_TEST_PRESS.Text <> "" Then
            DR_Spec("PNEUMATIC_TEST_PRESS") = txt_PNEUMATIC_TEST_PRESS.Text
        Else
            DR_Spec("PNEUMATIC_TEST_PRESS") = DBNull.Value
        End If
        If txt_HYDRO_TEST_PRESSURE_AT_SHOP.Text <> "" Then
            DR_Spec("HYDRO_TEST_PRESSURE_AT_SHOP") = txt_HYDRO_TEST_PRESSURE_AT_SHOP.Text
        Else
            DR_Spec("HYDRO_TEST_PRESSURE_AT_SHOP") = DBNull.Value
        End If
        If txt_HYDRO_TEST_PRESSURE_AT_FIELD.Text <> "" Then
            DR_Spec("HYDRO_TEST_PRESSURE_AT_FIELD") = txt_HYDRO_TEST_PRESSURE_AT_FIELD.Text
        Else
            DR_Spec("HYDRO_TEST_PRESSURE_AT_FIELD") = DBNull.Value
        End If
        If txt_HYDRO_TEST_PRESSURE_AT_FIELD.Text <> "" Then
            DR_Spec("HYDRO_TEST_PRESSURE_AT_FIELD") = txt_HYDRO_TEST_PRESSURE_AT_FIELD.Text
        Else
            DR_Spec("HYDRO_TEST_PRESSURE_AT_FIELD") = DBNull.Value
        End If
        If txt_DESIGN_TEMPERATURE_MIN.Text <> "" Then
            DR_Spec("DESIGN_TEMPERATURE_MIN") = txt_DESIGN_TEMPERATURE_MIN.Text
        Else
            DR_Spec("DESIGN_TEMPERATURE_MIN") = DBNull.Value
        End If
        If txt_MIN_DESIGN_METAL_TEMPERATURE.Text <> "" Then
            DR_Spec("MIN_DESIGN_METAL_TEMPERATURE") = txt_MIN_DESIGN_METAL_TEMPERATURE.Text
        Else
            DR_Spec("MIN_DESIGN_METAL_TEMPERATURE") = DBNull.Value
        End If
        If txt_DESIGN_INTERNAL_PRESSURE.Text <> "" Then
            DR_Spec("DESIGN_INTERNAL_PRESSURE") = txt_DESIGN_INTERNAL_PRESSURE.Text
        Else
            DR_Spec("DESIGN_INTERNAL_PRESSURE") = DBNull.Value
        End If
        If txt_DESIGN_EXTERNAL_PRESSURE.Text <> "" Then
            DR_Spec("DESIGN_EXTERNAL_PRESSURE") = txt_DESIGN_EXTERNAL_PRESSURE.Text
        Else
            DR_Spec("DESIGN_EXTERNAL_PRESSURE") = DBNull.Value
        End If
        If txt_OPERATING_TEMPERATURE_MIN.Text <> "" Then
            DR_Spec("OPERATING_TEMPERATURE_MIN") = txt_OPERATING_TEMPERATURE_MIN.Text
        Else
            DR_Spec("OPERATING_TEMPERATURE_MIN") = DBNull.Value
        End If
        If txt_OPERATING_PRESSURE_MIN.Text <> "" Then
            DR_Spec("OPERATING_PRESSURE_MIN") = txt_OPERATING_PRESSURE_MIN.Text
        Else
            DR_Spec("OPERATING_PRESSURE_MIN") = DBNull.Value
        End If
        If txt_PRESSURE_DROP_THROUGH_INTERNALS.Text <> "" Then
            DR_Spec("PRESSURE_DROP_THROUGH_INTERNALS") = txt_PRESSURE_DROP_THROUGH_INTERNALS.Text
        Else
            DR_Spec("PRESSURE_DROP_THROUGH_INTERNALS") = DBNull.Value
        End If
        If txt_STATIC_VAPOR_HEAD.Text <> "" Then
            DR_Spec("STATIC_VAPOR_HEAD") = txt_STATIC_VAPOR_HEAD.Text
        Else
            DR_Spec("STATIC_VAPOR_HEAD") = DBNull.Value
        End If

        DR_Spec("CONTENTS") = txt_CONTENTS.Text

        If txt_LIQUID_SPECIFIC_GRAVITY.Text <> "" Then
            DR_Spec("LIQUID_SPECIFIC_GRAVITY") = txt_LIQUID_SPECIFIC_GRAVITY.Text
        Else
            DR_Spec("LIQUID_SPECIFIC_GRAVITY") = DBNull.Value
        End If
        If txt_LENGTH_BETWEEN_TANGENT_LINE.Text <> "" Then
            DR_Spec("LENGTH_BETWEEN_TANGENT_LINE") = txt_LENGTH_BETWEEN_TANGENT_LINE.Text
        Else
            DR_Spec("LENGTH_BETWEEN_TANGENT_LINE") = DBNull.Value
        End If
        If txt_SHELL_INSIDE_DIAMETER.Text <> "" Then
            DR_Spec("SHELL_INSIDE_DIAMETER") = txt_SHELL_INSIDE_DIAMETER.Text
        Else
            DR_Spec("SHELL_INSIDE_DIAMETER") = DBNull.Value
        End If
        If txt_TYPE_OF_HEADS.Text <> "" Then
            DR_Spec("TYPE_OF_HEADS") = txt_TYPE_OF_HEADS.Text
        Else
            DR_Spec("TYPE_OF_HEADS") = DBNull.Value
        End If
        If txt_WIND_LOAD.Text <> "" Then
            DR_Spec("TYPE_OF_HEADS") = txt_WIND_LOAD.Text
        Else
            DR_Spec("TYPE_OF_HEADS") = DBNull.Value
        End If
        If txt_SEISMIC_LOAD.Text <> "" Then
            DR_Spec("SEISMIC_LOAD") = txt_SEISMIC_LOAD.Text
        Else
            DR_Spec("SEISMIC_LOAD") = DBNull.Value
        End If
        If txt_INSULATION_THICKNESS.Text <> "" Then
            DR_Spec("INSULATION_THICKNESS") = txt_INSULATION_THICKNESS.Text
        Else
            DR_Spec("INSULATION_THICKNESS") = DBNull.Value
        End If
        If txt_FIRE_PROOFING_THICKNESS.Text <> "" Then
            DR_Spec("FIRE_PROOFING_THICKNESS") = txt_FIRE_PROOFING_THICKNESS.Text
        Else
            DR_Spec("FIRE_PROOFING_THICKNESS") = DBNull.Value
        End If
        If txt_REFRACTORY_LINING_THICKNESS.Text <> "" Then
            DR_Spec("REFRACTORY_LINING_THICKNESS") = txt_REFRACTORY_LINING_THICKNESS.Text
        Else
            DR_Spec("REFRACTORY_LINING_THICKNESS") = DBNull.Value
        End If
        If txt_VOLUME.Text <> "" Then
            DR_Spec("VOLUME") = txt_VOLUME.Text
        Else
            DR_Spec("VOLUME") = DBNull.Value
        End If

        If txt_Norminal_Thickness.Text <> "" Then
            DR_Spec("Norminal_Thickness") = txt_Norminal_Thickness.Text
        Else
            DR_Spec("Norminal_Thickness") = DBNull.Value
        End If

        If txt_Calculated_Thickness.Text <> "" Then
            DR_Spec("Calculated_Thickness") = txt_Calculated_Thickness.Text
        Else
            DR_Spec("Calculated_Thickness") = DBNull.Value

        End If

        DR_Spec("Update_By") = Session("USER_ID")
        DR_Spec("Update_Time") = Now

        If DT_Spec.Rows.Count = 0 Then DT_Spec.Rows.Add(DR_Spec)
        cmd = New SqlCommandBuilder(DA_Spec)
        Try
            DA_Spec.Update(DT_Spec)
            DT_Spec.AcceptChanges()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try


#Region "Save Image"
        BL.Drop_ST_TA_Tag_Drawing(TAG_ID, "MS_ST_ABSORBER_Drawing")
        If DrawingList.Count > 0 Then
            Dim FT As New DataTable
            DA = New SqlDataAdapter("SELECT * FROM MS_ST_ABSORBER_Drawing WHERE TAG_ID=" & TAG_ID, BL.ConnStr)
            Dim Path As String = BL.Picture_Path & "\" & "ST_TA" & "\Drawing\Absorber\" & TAG_ID & "\"

            IO.Directory.CreateDirectory(Path)
            Try
                DA.Fill(FT)
                If Not IO.Directory.Exists(Path) Then IO.Directory.CreateDirectory(Path)
                For i As Integer = 0 To DrawingList.Count - 1
                    '---------- Save To Physical --------
                    Dim F = IO.File.OpenWrite(Path & DrawingList(i).File_ID)
                    F.Write(DrawingList(i).File_Data, 0, DrawingList(i).File_Data.Length)
                    F.Close()
                    '---------- Save To Database---------
                    Dim FR As DataRow = FT.NewRow
                    FR("TAG_ID") = TAG_ID
                    FR("File_ID") = DrawingList(i).File_ID
                    FR("File_Type") = DrawingList(i).File_Type
                    FT.Rows.Add(FR)
                Next
                cmd = New SqlCommandBuilder(DA)
                DA.Update(FT)
                FT.AcceptChanges()
            Catch ex As Exception
                Throw (ex)
            End Try
        End If
#End Region

        New_TagID = TagID
        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True


    End Sub

    Private Function GetNewTagID(ByVal Table_Name As String) As Integer

        Dim SQL As String = "SELECT IsNull(MAX(TAG_ID),0)+1 FROM " & Table_Name & " "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

    Public Sub CloseDialog()

        Me.Visible = False
    End Sub

    Public Sub ShowDialog()

        Me.Visible = True

    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CloseDialog()
    End Sub

    Private Sub btnUploadFile_Click(sender As Object, e As EventArgs) Handles btnUploadFile.Click
        If IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_0")) Then Exit Sub

        Dim D As New EIR_BL.ST_TA_DrawingDetail
        D.File_Data = Session("TempImage_" & UNIQUE_POPUP_ID & "_0")
        Dim MaxID As Integer = 0
        For i As Integer = 0 To DrawingList.Count - 1
            If DrawingList(i).File_ID > MaxID Then MaxID = DrawingList(i).File_ID
        Next
        D.File_ID = MaxID + 1
        Dim img As Drawing.Image = Drawing.Image.FromStream(C.ByteToStream(D.File_Data))
        D.File_Type = BL.GetImageContentType(img)
        DrawingList.Add(D)
        rptDrawing.DataSource = DrawingList
        rptDrawing.DataBind()
    End Sub

    Protected ReadOnly Property DrawingList As List(Of EIR_BL.ST_TA_DrawingDetail)
        Get
            If IsNothing(Session("ST_TA_TAG_" & UNIQUE_POPUP_ID)) Then
                Session("ST_TA_TAG_" & UNIQUE_POPUP_ID) = New List(Of EIR_BL.ST_TA_DrawingDetail)
            End If
            Return Session("ST_TA_TAG_" & UNIQUE_POPUP_ID)
        End Get
    End Property


End Class