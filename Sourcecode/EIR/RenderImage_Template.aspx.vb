﻿Public Class RenderImage_Template
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim UNIQUE_ID As String = Request.QueryString("UNIQUE_ID")
        Dim TEMP_ID As String = Request.QueryString("TEMP_ID")
        '---------- Render Database Session ------------
        Dim Sector_ID As String = Request.QueryString("Sector_ID")
        Dim Image As String = Request.QueryString("Image")
        Dim IMG As Byte() = {}
        Try
            If UNIQUE_ID <> "" Then
                IMG = Session("PREVIEW_IMG_" & UNIQUE_ID & "_" & Image)
            ElseIf TEMP_ID <> "" Then
                IMG = Session("TempImage_" & TEMP_ID & "_" & Image)
            End If
            If IMG.Length < 50 Then
                Response.Redirect("resources/images/File_Sector.png", True)
                Exit Sub
            End If
            Response.Clear()
            Response.BinaryWrite(IMG)
        Catch ex As Exception
            Response.Redirect("resources/images/File_Sector.png", True)
            Exit Sub
        End Try
    End Sub

End Class