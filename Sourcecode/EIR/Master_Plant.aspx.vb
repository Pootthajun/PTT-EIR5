﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Master_Plant
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetPlant(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindPlant()

        Dim SQL As String = "SELECT " & vbLf
        SQL &= " MS_Plant.PLANT_ID," & vbLf
        SQL &= " PLANT_Code," & vbLf
        SQL &= " PLANT_Name," & vbLf
        SQL &= " COUNT(ROUTE_ID) TotalRoute," & vbLf
        SQL &= " PLANT_Location," & vbLf
        SQL &= " PLANT_Description," & vbLf
        SQL &= " MS_PLANT.active_status," & vbLf
        SQL &= " PLANT_Order," & vbLf
        SQL &= " MS_Plant.Update_By," & vbLf
        SQL &= " MS_Plant.Update_Time " & vbLf
        SQL &= " FROM MS_Plant " & vbLf
        SQL &= " LEFT JOIN (" & vbLf
        SQL &= "			SELECT * FROM MS_ST_Route WHERE Active_Status=1 " & vbLf
        SQL &= "			UNION ALL " & vbLf
        SQL &= "			SELECT * FROM MS_RO_Route WHERE Active_Status=1) " & vbLf
        SQL &= "			MS_Route ON MS_Plant.PLANT_ID=MS_Route.PLANT_ID" & vbLf
        SQL &= " GROUP BY MS_Plant.PLANT_ID," & vbLf
        SQL &= " PLANT_Code," & vbLf
        SQL &= " PLANT_Name," & vbLf
        SQL &= " PLANT_Location," & vbLf
        SQL &= " PLANT_Description," & vbLf
        SQL &= " MS_PLANT.active_status," & vbLf
        SQL &= " PLANT_Order," & vbLf
        SQL &= " MS_Plant.Update_By," & vbLf
        SQL &= " MS_Plant.Update_Time" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("MS_Plant") = DT

        Navigation.SesssionSourceName = "MS_Plant"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlant
    End Sub

    Protected Sub rptPlant_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlant.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblPlantCode As Label = e.Item.FindControl("lblPlantCode")
        Dim lblPlantName As Label = e.Item.FindControl("lblPlantName")
        Dim lblLocation As Label = e.Item.FindControl("lblLocation")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        Dim lblDesc As Label = e.Item.FindControl("lblDesc")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblPlantCode.Text = e.Item.DataItem("Plant_Code")
        lblPlantName.Text = e.Item.DataItem("Plant_Name")
        lblLocation.Text = e.Item.DataItem("Plant_Location")
        lblRoute.Text = e.Item.DataItem("TotalRoute")
        lblDesc.Text = BL.ReportGridDescription(e.Item.DataItem("PLANT_Description"), 20)
        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.Attributes("Plant_ID") = e.Item.DataItem("Plant_ID")

    End Sub

    Protected Sub rptPlant_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlant.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim Plant_ID As Integer = btnEdit.Attributes("Plant_ID")
        Select Case e.CommandName
            Case "Edit"
                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                'txtPlantCode.ReadOnly = True
                txtPlantName.Focus()
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListPlant.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT * FROM MS_Plant WHERE Plant_ID=" & Plant_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Plant Not Found"
                    pnlBindingError.Visible = True
                    BindPlant()
                    Exit Sub
                End If

                txtPlantCode.Text = DT.Rows(0).Item("PLANT_Code")
                txtPlantCode.Attributes("PlantID") = DT.Rows(0).Item("PLANT_ID")
                txtPlantName.Text = DT.Rows(0).Item("PLANT_Name")
                txtLocation.Text = DT.Rows(0).Item("PLANT_Location")
                txtDesc.Text = DT.Rows(0).Item("PLANT_Description")
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")
                lblValidation.Text = ""

                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_Plant Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  Plant_ID=" & Plant_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindPlant()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select


    End Sub

    Protected Sub ResetPlant(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindPlant()
        '-----------------------------------
        ClearPanelEdit()
        '-----------------------------------
        pnlListPlant.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtPlantCode.Text = ""
        txtPlantCode.Attributes("PlantID") = "0"
        txtPlantName.Text = ""
        txtLocation.Text = ""
        txtDesc.Text = ""
        chkAvailable.Checked = True

        btnCreate.Visible = True
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True

        btnCreate.Visible = False

        txtPlantCode.ReadOnly = False
        txtPlantCode.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListPlant.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtPlantCode.Text = "" Then
            lblValidation.Text = "Please insert Plant Code"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtPlantName.Text = "" Then
            lblValidation.Text = "Please insert Plant Name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim Plant_ID As Integer = txtPlantCode.Attributes("PlantID")

        Dim SQL As String = "SELECT * FROM MS_PLANT WHERE Plant_Code='" & txtPlantCode.Text.Replace("'", "''") & "' AND PLANT_ID<>" & Plant_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Plant Code is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_PLANT WHERE Plant_ID=" & Plant_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            Plant_ID = GetNewPlantID()
            DR("PLANT_ID") = Plant_ID
        Else
            DR = DT.Rows(0)
        End If


        DR("PLANT_Code") = txtPlantCode.Text
        DR("PLANT_Name") = txtPlantName.Text
        DR("PLANT_Location") = txtLocation.Text
        DR("PLANT_Description") = txtDesc.Text
        DR("PLANT_Order") = Plant_ID ' Remain
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID") ' Remain
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetPlant(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetNewPlantID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(Plant_ID),0)+1 FROM MS_PLANT "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

End Class