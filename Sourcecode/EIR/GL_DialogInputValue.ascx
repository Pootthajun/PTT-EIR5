﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_DialogInputValue.ascx.vb" Inherits="EIR.GL_DialogInputValue" %>

		    <div class="MaskDialog"></div>
		    
		        <asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog_Picture"  DefaultButton="btnOk"
                    Width="800px" Height="100px" style="position:fixed; left:400px; top:200px;">
		            <center>
		            <asp:Label ID="lblCaption" Font-Size="16px" Font-Bold="True" runat="server" Width="100%" style="text-align:center;"></asp:Label>
		            <br><br><asp:TextBox ID="txtInput" runat="server" Width="80%" style="text-align:center;" ></asp:TextBox>
		            <br><br>
		            <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="40%" CssClass="button" />&nbsp;
		            <asp:Button ID="btnOk" runat="server" Text="OK" Width="40%" CssClass="button" />
		            </center>
		        </asp:Panel>