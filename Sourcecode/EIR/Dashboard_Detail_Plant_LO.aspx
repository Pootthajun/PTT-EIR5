﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Detail_Plant_LO.aspx.vb" Inherits="EIR.Dashboard_Detail_Plant_LO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UDP1" runat="server">
        <ContentTemplate>
			<h2>
                <asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
			</h2>
        	<table cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
        	    <tr>
        	        <td>
        	            <asp:LinkButton ID="lblBack" runat="server" Tooltip="Back to see on this plant" Text="Back to see on this plant"></asp:LinkButton>
        	        </td>
        	    </tr>
			    <tr>
                  <td style="vertical-align:top; text-align:left;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:700px;">
                      <tr>
                        <td style="background-color:#00cc33; text-align:center; color:White; font-weight:bold;" colspan="5">          
                            <asp:Label ID="lblHead" runat="server" ></asp:Label>
                        </td>
                      </tr>
                      <tr>
                          <td style="text-align:center; background-color:#003366; color:White;">
                              Tag</td> 
                          <td style="text-align:center; background-color:#003366; color:White;">
                              Tan</td>
                          <td style="text-align:center; background-color:#003366; color:White;">
                              Oxd/Anti</td>
                          <td style="text-align:center; background-color:#003366; color:White;">
                              Varnish</td>
                          <td style="text-align:center; background-color:#003366; color:White;">
                              Water</td>                    
                      </tr>
                      <asp:Repeater ID="rptData" runat="server" >
                        <ItemTemplate>
                            <tr>
                                <td style="border-left:1px solid #eeeeee; text-align:left;">
                                    <asp:Label ID="lblTag" Text="-" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                                <td style="text-align:center; ">
                                    <a ID="lblTAN" runat="server" style="color:#666666">-</a>
                                </td>
                                <td style="text-align:center; ">
                                    <a ID="lblOx" runat="server" style="color:#666666;">-</a>
                                </td>
                                <td style="text-align:center; ">
                                    <a ID="lblVarnish" runat="server" style="color:#666666">-</a>
                                </td>
                                <td style="text-align:center; ">
                                    <a ID="lblWater" runat="server" style="color:#666666">-</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                      </asp:Repeater>
                    </table>
                 </td>
		       </tr>
			</table>
			<div style="visibility: hidden">
                <asp:Label ID="lblMonth" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblYear" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblPlantName" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblReport" runat="server" Text="Label"></asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
