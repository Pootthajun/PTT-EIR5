﻿Imports System.Data
Imports System.Data.SqlClient

Public Class MS_ST_DialogFilter_Spec
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Dim C As New Converter

#Region "Property"

    Public Enum PropertyItem
        Drawing = 100
    End Enum


    Public Class DrawingDetail
        Public File_ID As Integer = 0
        Public File_Type As String = ""
        Public File_Data As Byte() = {}
    End Class

    Public Property TAG_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("TAG_ID")) Then
                Return 0
            Else
                Return Me.Attributes("TAG_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("TAG_ID") = value
        End Set
    End Property

    Public Property TAG_TYPE_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("TAG_TYPE_ID")) Then
                Return 0
            Else
                Return Me.Attributes("TAG_TYPE_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("TAG_TYPE_ID") = value
        End Set
    End Property

    Public Property TAG_Name() As String
        Get
            Return lblTag.Attributes("TAG_Name")
        End Get
        Set(ByVal value As String)
            lblTag.Attributes("TAG_Name") = value
        End Set
    End Property

    Public Property TAG_TYPE_Name() As String
        Get
            Return lblTag.Attributes("TAG_TYPE_Name")
        End Get
        Set(ByVal value As String)
            lblTag.Attributes("TAG_TYPE_Name") = value
        End Set
    End Property

    Public Property TO_TABLE_MS() As String
        Get
            Return lblTag.Attributes("TO_TABLE_MS")
        End Get
        Set(ByVal value As String)
            lblTag.Attributes("TO_TABLE_MS") = value
        End Set
    End Property

    Public Property TO_TABLE_SPEC() As String
        Get
            Return lblTag.Attributes("TO_TABLE_SPEC")
        End Get
        Set(ByVal value As String)
            lblTag.Attributes("TO_TABLE_SPEC") = value
        End Set
    End Property

    Public Property TAG_CODE() As String
        Get
            Return lblTag.Attributes("TAG_CODE")
        End Get
        Set(ByVal value As String)
            lblTag.Attributes("TAG_CODE") = value
        End Set
    End Property

    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return ViewState("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Public Property New_TagID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("New_TagID")) Then
                Return 0
            Else
                Return Me.Attributes("New_TagID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("New_TagID") = value
        End Set
    End Property

    Public Sub SetPropertyEditable(ByVal PropertyItem As PropertyItem, ByVal Editable As Boolean)

        Select Case PropertyItem
            Case PropertyItem.Drawing
                btnAddFile.Visible = Editable
                rptDrawing.DataSource = DrawingList
                rptDrawing.DataBind()
        End Select
    End Sub

    Public Function GetPropertyEditable(ByVal PropertyItem As PropertyItem) As Boolean
        Select Case PropertyItem

            Case PropertyItem.Drawing
                Return btnAddFile.Visible
            Case Else ' PropertyItem.All

                Return True
        End Select
    End Function

    Public Property Initial_Year As Object
        Get
            If IsNumeric(txt_Initial_Year.Text.Replace(",", "")) Then
                Return CInt(txt_Initial_Year.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Initial_Year.Text = CInt(value)
            Else
                txt_Initial_Year.Text = ""
            End If
        End Set
    End Property


#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            UNIQUE_POPUP_ID = Now.ToOADate.ToString.Replace(".", "")
            btnAddFile.Attributes("onclick") = "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "',0,document.getElementById('" & btnUploadFile.ClientID & "'));"
            ImplementJavascriptControl()
        End If
        pnlValidation.Visible = False
        pnlBindingSuccess.Visible = False

    End Sub

    Public Sub Clear_txt()

        Initial_Year = Nothing
        txt_DESIGN_PRESSURE_MIN.Text = ""
        txt_DESIGN_TEMPERATURE_MIN.Text = ""
        txt_HYDRO_TEST_PRESSURE_MIN.Text = ""
        txt_PNEUM_TEST_PRESSURE_MIN.Text = ""
        txt_OPERATING_PRESSURE_MIN.Text = ""
        txt_OPERATING_TEMPERATURE_MIN.Text = ""
        txt_CORROSION_ALLOWANCE.Text = ""

        'CORROSION_ALLOWANCE
        DrawingList.Clear()
        rptDrawing.DataSource = DrawingList
        rptDrawing.DataBind()

    End Sub

    Private Sub ImplementJavascriptControl()
        ImplementJavaIntegerText(txt_Initial_Year, False,, "Center")
        ImplementJavaNumericText(txt_DESIGN_PRESSURE_MIN, "Center")
        ImplementJavaNumericText(txt_DESIGN_TEMPERATURE_MIN, "Center")
        ImplementJavaNumericText(txt_HYDRO_TEST_PRESSURE_MIN, "Center")
        ImplementJavaNumericText(txt_PNEUM_TEST_PRESSURE_MIN, "Center")
        ImplementJavaNumericText(txt_OPERATING_PRESSURE_MIN, "Center")
        ImplementJavaNumericText(txt_OPERATING_TEMPERATURE_MIN, "Center")
        ImplementJavaNumericText(txt_CORROSION_ALLOWANCE, "Center")


    End Sub

    Public Sub BindHeader()

        '-------------ตรวจสอบเป็น TAG จาก Station หรือ Turnaround-----------------------
        Dim SQL As String = ""
        SQL = " "
        SQL &= " SELECT " & TO_TABLE_MS & ".* ,MS_PLANT.PLANT_ID" & vbNewLine
        SQL &= " FROM " & TO_TABLE_MS & " LEFT JOIN MS_ST_ROUTE ON " & TO_TABLE_MS & ".ROUTE_ID=MS_ST_ROUTE.ROUTE_ID" & vbNewLine
        SQL &= " LEFT JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_ROUTE.PLANT_ID" & vbNewLine
        SQL &= " LEFT JOIN MS_ST_Tag_Type ON " & TO_TABLE_MS & ".TAG_TYPE_ID=MS_ST_Tag_Type.TAG_TYPE_ID" & vbNewLine
        SQL &= " WHERE " & TO_TABLE_MS & ".TAG_ID=" & TAG_ID

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            Me.CloseDialog()
            Exit Sub
        End If

        Dim PLANT_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then PLANT_ID = DT.Rows(0).Item("PLANT_ID")
        Dim ROUTE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("ROUTE_ID")) Then ROUTE_ID = DT.Rows(0).Item("ROUTE_ID")
        Dim AREA_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("AREA_ID")) Then AREA_ID = DT.Rows(0).Item("AREA_ID")
        Dim PROC_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PROC_ID")) Then PROC_ID = DT.Rows(0).Item("PROC_ID")
        Dim TAG_TYPE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("TAG_TYPE_ID")) Then TAG_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")

        BL.BindDDlPlant(ddl_Edit_Plant, PLANT_ID)
        BL.BindDDl_ST_Route(PLANT_ID, ddl_Edit_Route, ROUTE_ID)
        BL.BindDDlArea(PLANT_ID, ddl_Edit_Area, AREA_ID)
        BL.BindDDlProcess(ddl_Edit_Process, PROC_ID)
        BL.BindDDlST_TA_TagType(ddl_Edit_Type, TAG_TYPE_ID)
        txtTagNo.Text = DT.Rows(0).Item("TAG_NO")
        txtTagNo.Attributes("TagID") = TAG_ID
        txtTagName.Text = DT.Rows(0).Item("TAG_NAME")

        BindData(TAG_ID)

    End Sub

    Public Sub BindData(ByVal TAG_ID As Integer)

        Clear_txt()
        Dim SQL As String = " SELECT * FROM " & TO_TABLE_SPEC & " "
        SQL &= " WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        Dim DR As DataRow = DT.Rows(0)

        Me.TAG_ID = DR("TAG_ID")

        Initial_Year = DR("Initial_Year")
        If Not IsDBNull(DT.Rows(0).Item("DESIGN_PRESSURE_MIN")) Then txt_DESIGN_PRESSURE_MIN.Text = FormatNumber(DR("DESIGN_PRESSURE_MIN"))
        If Not IsDBNull(DT.Rows(0).Item("DESIGN_TEMPERATURE_MIN")) Then txt_DESIGN_TEMPERATURE_MIN.Text = FormatNumber(DR("DESIGN_TEMPERATURE_MIN"))
        If Not IsDBNull(DT.Rows(0).Item("HYDRO_TEST_PRESSURE_MIN")) Then txt_HYDRO_TEST_PRESSURE_MIN.Text = FormatNumber(DR("HYDRO_TEST_PRESSURE_MIN"))
        If Not IsDBNull(DT.Rows(0).Item("PNEUM_TEST_PRESSURE_MIN")) Then txt_PNEUM_TEST_PRESSURE_MIN.Text = FormatNumber(DR("PNEUM_TEST_PRESSURE_MIN"))
        If Not IsDBNull(DT.Rows(0).Item("OPERATING_PRESSURE_MIN")) Then txt_OPERATING_PRESSURE_MIN.Text = FormatNumber(DR("OPERATING_PRESSURE_MIN"))
        If Not IsDBNull(DT.Rows(0).Item("OPERATING_PRESSURE_MIN")) Then txt_OPERATING_PRESSURE_MIN.Text = FormatNumber(DR("OPERATING_PRESSURE_MIN"))
        If Not IsDBNull(DT.Rows(0).Item("CORROSION_ALLOWANCE")) Then txt_CORROSION_ALLOWANCE.Text = FormatNumber(DR("CORROSION_ALLOWANCE"))

        '--------------- Load Drawing --------------
        DT = New DataTable
        DA = New SqlDataAdapter("SELECT * FROM MS_ST_FILTER_Drawing WHERE TAG_ID=" & TAG_ID & " ORDER BY File_ID", BL.ConnStr)
        DA.Fill(DT)
        Dim Path As String = BL.Picture_Path & "\" & "ST_TA" & "\Drawing\Filter\" & TAG_ID & "\"
        For i As Integer = 0 To DT.Rows.Count - 1
            If Not IsDBNull(DT.Rows(i).Item("File_ID")) Then
                Dim Obj As New EIR_BL.ST_TA_DrawingDetail
                Obj.File_ID = DT.Rows(i).Item("File_ID")
                Obj.File_Type = DT.Rows(i).Item("File_Type")
                If IO.File.Exists(Path & Obj.File_ID) Then
                    Dim F As IO.FileStream = IO.File.Open(Path & Obj.File_ID, IO.FileMode.Open, IO.FileAccess.Read, IO.FileShare.ReadWrite)
                    Obj.File_Data = C.StreamToByte(F)
                    F.Close()
                    DrawingList.Add(Obj)
                End If
            End If
        Next
        rptDrawing.DataSource = DrawingList
        rptDrawing.DataBind()


    End Sub

    '-----Drawing----

    Private Sub rptDrawing_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptDrawing.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim item As HtmlGenericControl = e.Item.FindControl("item")
        Dim img_File As Image = e.Item.FindControl("img_File")
        Dim btnEdit As HtmlInputImage = e.Item.FindControl("btnEdit")
        Dim btnUpload As Button = e.Item.FindControl("btnUpload")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim pnlEditFile As HtmlGenericControl = e.Item.FindControl("pnlEditFile")

        img_File.ImageUrl = "Render_ST_TA_Drawing.aspx?Mode=session&UNIQUE_POPUP_ID=" & UNIQUE_POPUP_ID & "&ImageID=" & e.Item.ItemIndex & "&t=" & Now.ToOADate
        Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_" & DrawingList(e.Item.ItemIndex).File_ID) = DrawingList(e.Item.ItemIndex).File_Data
        btnEdit.Attributes("onclick") = "ShowDialogEditImage('" & UNIQUE_POPUP_ID & "'," & DrawingList(e.Item.ItemIndex).File_ID & ",document.getElementById('" & btnUpload.ClientID & "'));"
        '---------- Get Image Dimension ----------
        Dim img As Drawing.Image = Drawing.Image.FromStream(C.ByteToStream(DrawingList(e.Item.ItemIndex).File_Data))
        Dim w As Integer = img.Width
        Dim h As Integer = img.Height

        Dim MustHeight As Integer = 120
        If GetPropertyEditable(PropertyItem.Drawing) Then
            btnUpload.CommandArgument = DrawingList(e.Item.ItemIndex).File_ID
            btnDelete.CommandArgument = DrawingList(e.Item.ItemIndex).File_ID
        Else
            pnlEditFile.Visible = False
            MustHeight = 150
        End If

        If w > h Then
            img_File.Width = Unit.Pixel(150)
        Else
            img_File.Height = Unit.Pixel(MustHeight)
        End If

    End Sub

    Private Sub rptDrawing_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptDrawing.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                If IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_" & DrawingList(e.Item.ItemIndex).File_ID)) Then Exit Sub
                Dim D As EIR_BL.ST_TA_DrawingDetail = DrawingList(e.Item.ItemIndex)
                D.File_Data = Session("TempImage_" & UNIQUE_POPUP_ID & "_" & D.File_ID)
                Dim img As Drawing.Image = Drawing.Image.FromStream(C.ByteToStream(D.File_Data))
                D.File_Type = BL.GetImageContentType(img)
                rptDrawing.DataSource = DrawingList
                rptDrawing.DataBind()
            Case "Delete"
                DrawingList.RemoveAt(e.Item.ItemIndex)
                rptDrawing.DataSource = DrawingList
                rptDrawing.DataBind()
        End Select
    End Sub

    Public Sub Save()
        If ddl_Edit_Area.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Area"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If ddl_Edit_Process.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Process"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtTagNo.Text = "" Then
            lblValidation.Text = "Please insert Tag number"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtTagName.Text = "" Then
            lblValidation.Text = "Please insert Tag name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Type.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Equipement-Type"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Plant.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Route.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Route"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim TagID As Integer = txtTagNo.Attributes("TagID")

        Dim To_Table As String = TO_TABLE_MS
        '-------------ตรวจสอบเป็น TAG จาก Station หรือ Turnaround-----------------------
        Dim SQL As String = "SELECT * FROM " & To_Table & " WHERE AREA_ID=" & ddl_Edit_Area.Items(ddl_Edit_Area.SelectedIndex).Value & " AND PROC_ID=" & ddl_Edit_Process.Items(ddl_Edit_Process.SelectedIndex).Value & " AND TAG_No='" & txtTagNo.Text.Replace("'", "''") & "' AND TAG_ID<>" & TagID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        SQL = "SELECT * FROM " & To_Table & " WHERE TAG_ID=" & TagID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            TagID = GetNewTagID(To_Table)
            DR("TAG_ID") = TagID
        Else
            DR = DT.Rows(0)
        End If

        DR("AREA_ID") = ddl_Edit_Area.Items(ddl_Edit_Area.SelectedIndex).Value
        DR("PROC_ID") = ddl_Edit_Process.Items(ddl_Edit_Process.SelectedIndex).Value
        DR("TAG_No") = txtTagNo.Text '.ToString.PadLeft(4, "0")
        DR("ROUTE_ID") = ddl_Edit_Route.Items(ddl_Edit_Route.SelectedIndex).Value
        DR("TAG_Name") = txtTagName.Text
        DR("TAG_TYPE_ID") = ddl_Edit_Type.Items(ddl_Edit_Type.SelectedIndex).Value
        DR("TAG_Order") = TagID
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try


        '-------------Update Spec----------------

        Dim SQL_Spec As String = " SELECT * FROM " & TO_TABLE_SPEC & " "
        SQL_Spec &= " WHERE TAG_ID=" & TAG_ID

        Dim DA_Spec As New SqlDataAdapter(SQL_Spec, BL.ConnStr)
        Dim DT_Spec As New DataTable
        DA_Spec.Fill(DT_Spec)
        Dim DR_Spec As DataRow

        If DT_Spec.Rows.Count = 0 Then
            DR_Spec = DT_Spec.NewRow
        Else
            DR_Spec = DT_Spec.Rows(0)
        End If

        DR_Spec("TAG_ID") = Me.TAG_ID
        If Not IsNothing(Initial_Year) Then
            If Initial_Year > 1900 And Initial_Year <= Now.Year Then
                DR_Spec("Initial_Year") = Initial_Year
            Else
                DR_Spec("Initial_Year") = DBNull.Value
            End If
        Else
            DR_Spec("Initial_Year") = DBNull.Value
        End If
        If txt_DESIGN_PRESSURE_MIN.Text <> "" Then
            DR_Spec("DESIGN_PRESSURE_MIN") = txt_DESIGN_PRESSURE_MIN.Text
        Else
            DR_Spec("DESIGN_PRESSURE_MIN") = DBNull.Value
        End If
        'DESIGN_PRESSURE_MIN
        If txt_DESIGN_TEMPERATURE_MIN.Text <> "" Then
            DR_Spec("DESIGN_TEMPERATURE_MIN") = txt_DESIGN_TEMPERATURE_MIN.Text
        Else
            DR_Spec("DESIGN_TEMPERATURE_MIN") = DBNull.Value
        End If

        'DESIGN_TEMPERATURE_MIN
        If txt_HYDRO_TEST_PRESSURE_MIN.Text <> "" Then
            DR_Spec("HYDRO_TEST_PRESSURE_MIN") = txt_HYDRO_TEST_PRESSURE_MIN.Text
        Else
            DR_Spec("HYDRO_TEST_PRESSURE_MIN") = DBNull.Value
        End If
        'HYDRO_TEST_PRESSURE_MIN
        If txt_PNEUM_TEST_PRESSURE_MIN.Text <> "" Then
            DR_Spec("PNEUM_TEST_PRESSURE_MIN") = txt_PNEUM_TEST_PRESSURE_MIN.Text
        Else
            DR_Spec("PNEUM_TEST_PRESSURE_MIN") = DBNull.Value
        End If
        'PNEUM_TEST_PRESSURE_MIN
        If txt_OPERATING_PRESSURE_MIN.Text <> "" Then
            DR_Spec("OPERATING_PRESSURE_MIN") = txt_OPERATING_PRESSURE_MIN.Text
        Else
            DR_Spec("OPERATING_PRESSURE_MIN") = DBNull.Value
        End If
        'OPERATING_PRESSURE_MIN
        If txt_OPERATING_TEMPERATURE_MIN.Text <> "" Then
            DR_Spec("OPERATING_TEMPERATURE_MIN") = txt_OPERATING_TEMPERATURE_MIN.Text
        Else
            DR_Spec("OPERATING_TEMPERATURE_MIN") = DBNull.Value
        End If
        'OPERATING_TEMPERATURE_MIN
        If txt_CORROSION_ALLOWANCE.Text <> "" Then
            DR_Spec("CORROSION_ALLOWANCE") = txt_CORROSION_ALLOWANCE.Text
        Else
            DR_Spec("CORROSION_ALLOWANCE") = DBNull.Value
        End If
        'CORROSION_ALLOWANCE

        DR_Spec("Update_By") = Session("USER_ID")
        DR_Spec("Update_Time") = Now

        If DT_Spec.Rows.Count = 0 Then DT_Spec.Rows.Add(DR_Spec)
        cmd = New SqlCommandBuilder(DA_Spec)
        Try
            DA_Spec.Update(DT_Spec)
            DT_Spec.AcceptChanges()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try


#Region "Save Image"
        BL.Drop_ST_TA_Tag_Drawing(TAG_ID, "MS_ST_FILTER_Drawing")
        If DrawingList.Count > 0 Then
            Dim FT As New DataTable
            DA = New SqlDataAdapter("SELECT * FROM MS_ST_FILTER_Drawing WHERE TAG_ID=" & TAG_ID, BL.ConnStr)
            Dim Path As String = BL.Picture_Path & "\" & "ST_TA" & "\Drawing\Filter\" & TAG_ID & "\"

            IO.Directory.CreateDirectory(Path)
            Try
                DA.Fill(FT)
                If Not IO.Directory.Exists(Path) Then IO.Directory.CreateDirectory(Path)
                For i As Integer = 0 To DrawingList.Count - 1
                    '---------- Save To Physical --------
                    Dim F = IO.File.OpenWrite(Path & DrawingList(i).File_ID)
                    F.Write(DrawingList(i).File_Data, 0, DrawingList(i).File_Data.Length)
                    F.Close()
                    '---------- Save To Database---------
                    Dim FR As DataRow = FT.NewRow
                    FR("TAG_ID") = TAG_ID
                    FR("File_ID") = DrawingList(i).File_ID
                    FR("File_Type") = DrawingList(i).File_Type
                    FT.Rows.Add(FR)
                Next
                cmd = New SqlCommandBuilder(DA)
                DA.Update(FT)
                FT.AcceptChanges()
            Catch ex As Exception
                Throw (ex)
            End Try
        End If
#End Region

        New_TagID = TagID
        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True


    End Sub

    Private Function GetNewTagID(ByVal Table_Name As String) As Integer

        Dim SQL As String = "SELECT IsNull(MAX(TAG_ID),0)+1 FROM " & Table_Name & " "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

    Public Sub CloseDialog()
        Me.Visible = False
    End Sub

    Public Sub ShowDialog()
        Me.Visible = True
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CloseDialog()
    End Sub

    Private Sub btnUploadFile_Click(sender As Object, e As EventArgs) Handles btnUploadFile.Click
        If IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_0")) Then Exit Sub

        Dim D As New EIR_BL.ST_TA_DrawingDetail
        D.File_Data = Session("TempImage_" & UNIQUE_POPUP_ID & "_0")
        Dim MaxID As Integer = 0
        For i As Integer = 0 To DrawingList.Count - 1
            If DrawingList(i).File_ID > MaxID Then MaxID = DrawingList(i).File_ID
        Next
        D.File_ID = MaxID + 1
        Dim img As Drawing.Image = Drawing.Image.FromStream(C.ByteToStream(D.File_Data))
        D.File_Type = BL.GetImageContentType(img)
        DrawingList.Add(D)
        rptDrawing.DataSource = DrawingList
        rptDrawing.DataBind()
    End Sub

    Protected ReadOnly Property DrawingList As List(Of EIR_BL.ST_TA_DrawingDetail)
        Get
            If IsNothing(Session("ST_TA_TAG_" & UNIQUE_POPUP_ID)) Then
                Session("ST_TA_TAG_" & UNIQUE_POPUP_ID) = New List(Of EIR_BL.ST_TA_DrawingDetail)
            End If
            Return Session("ST_TA_TAG_" & UNIQUE_POPUP_ID)
        End Get
    End Property

    'DESIGN_PRESSURE_MIN
    'DESIGN_TEMPERATURE_MIN
    'HYDRO_TEST_PRESSURE_MIN
    'PNEUM_TEST_PRESSURE_MIN
    'OPERATING_PRESSURE_MIN
    'OPERATING_TEMPERATURE_MIN
    'CORROSION_ALLOWANCE

End Class

