﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Public Class GL_ST_TA_Report
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Public rptFormula As Formula

    Public Structure Formula
        Dim Header As String
        Dim DocDetail As String
        Dim Plant As String
        Dim ReportNo As String
        Dim Collected_By As String
        Dim Collected_Date As String
    End Structure

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        ''---------------- Save User Stat ---------------------
        'If Not IsNothing(Session("USER_ID")) Then
        '    BL.SaveLog(Session("USER_ID"), Session.SessionID, GetCurrentPageName, GetIPAddress)
        'End If

        If IsNothing(Request.QueryString("RPT_Year")) Or IsNothing(Request.QueryString("RPT_No")) Then
            Response.Redirect("Login.aspx", True)
        End If

        Dim RC As New ReportClass
        Dim OutputFile As String = ""
        Dim Param_Report As String = ""
        Dim Param_Daily As String = ""
        If Not IsNothing(Session("Filter")) Then
            Param_Report = Session("Filter")
        End If
        If Not IsNothing(Session("Filter_Daily")) Then
            Param_Daily = Session("Filter_Daily")
        End If


        OutputFile = RC.Create_ST_TA_Daily_Report(Request.QueryString("RPT_Year"), Request.QueryString("RPT_No"), Param_Report, Param_Daily)

        If OutputFile = "" Then
            Dim ReportName As String = "INSP-E-" & Right(Request.QueryString("RPT_Year"), 2) & "-" & Request.QueryString("RPT_No").PadLeft(4, "0")
            Response.Write("Unable to genetate Report " & ReportName)
            Exit Sub
        End If

        Dim C As New Converter

        '------------------ Change Reading File Method -----------
        Dim FS As FileStream = File.Open(OutputFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
        Dim b As Byte() = C.StreamToByte(FS)
        FS.Close()
        FS.Dispose()

        Dim FN As String() = Split(OutputFile, "_")
        If FN.Length = 3 Then
            OutputFile = FN(1) & "_" & FN(2)
        End If

        Response.ContentType = "application/pdf"
        Response.AddHeader("Content-Disposition", "attachment; filename=" & OriginalFileName(OutputFile) & ".PDF")
        Response.BinaryWrite(b)
        Response.End()

    End Sub

End Class