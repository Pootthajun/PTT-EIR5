﻿Imports System.Data
Imports System.Data.SqlClient
Public Class LO_Tag
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetTag(Nothing, Nothing)
            ClearPanelSearch()
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindTag()

        Dim SQL As String = "SELECT LO_TAG_ID,LO_TAG_NO,LO_TAG_Name,LO_TAG_Description,PLANT_Name,Oil_TYPE_Name," & vbLf
        SQL &= " CASE LO_TAG_TYPE WHEN 1 THEN 'Balance of Plant' WHEN 2 THEN 'Critical Machine' ELSE '' END LO_TAG_TYPE_Name," & vbLf
        SQL &= " MS_LO_TAG.Active_Status" & vbLf
        SQL &= " FROM MS_LO_TAG " & vbLf
        SQL &= " INNER JOIN MS_LO_Oil_Type ON  MS_LO_TAG.Oil_TYPE_ID=MS_LO_Oil_Type.Oil_TYPE_ID" & vbLf
        SQL &= " INNER JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_LO_TAG.PLANT_ID" & vbLf

        Dim WHERE As String = ""

        If ddl_Search_LO_Type.SelectedIndex > 0 Then
            WHERE &= " MS_LO_TAG.Oil_TYPE_ID=" & ddl_Search_LO_Type.Items(ddl_Search_LO_Type.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " MS_PLANT.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_TagType.SelectedIndex > 0 Then
            WHERE &= " LO_TAG_TYPE=" & ddl_Search_TagType.Items(ddl_Search_TagType.SelectedIndex).Value & " AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= " ORDER BY LO_TAG_NO" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("MS_LO_TAG") = DT

        Navigation.SesssionSourceName = "MS_LO_TAG"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        Dim lblPlantName As Label = e.Item.FindControl("lblPlantName")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblTagNo.Text = e.Item.DataItem("LO_TAG_NO")
        lblTagName.Text = e.Item.DataItem("LO_TAG_Name")
        lblTagType.Text = e.Item.DataItem("Oil_TYPE_Name")
        lblPlantName.Text = e.Item.DataItem("PLANT_Name")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblRoute.Text = e.Item.DataItem("LO_TAG_TYPE_Name")
        btnEdit.CommandArgument = e.Item.DataItem("LO_TAG_ID")

    End Sub


    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim LO_TAG_ID As Integer = btnEdit.CommandArgument

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListTag.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT LO_TAG_ID,MS_LO_TAG.Oil_TYPE_ID,LO_TAG_NO,LO_TAG_Name,LO_TAG_Description,MS_LO_TAG.PLANT_ID,PLANT_Name,LO_TAG_TYPE," & vbLf
                SQL &= " MS_LO_TAG.Active_Status, MS_LO_TAG.Update_Time" & vbLf
                SQL &= " FROM MS_LO_TAG " & vbLf
                SQL &= " INNER JOIN MS_LO_Oil_Type ON  MS_LO_TAG.Oil_TYPE_ID=MS_LO_Oil_Type.Oil_TYPE_ID" & vbLf
                SQL &= " INNER JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_LO_TAG.PLANT_ID" & vbLf
                SQL &= " WHERE LO_TAG_ID=" & LO_TAG_ID & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "TAG Not Found"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If

                BL.BindDDlPlant(ddl_Edit_Plant, DT.Rows(0).Item("PLANT_ID"))
                BL.BindDDlLOType(ddl_Edit_Type, DT.Rows(0).Item("Oil_TYPE_ID"))
                txtTagNo.Text = DT.Rows(0).Item("LO_TAG_NO")
                txtTagNo.Attributes("LO_TAG_ID") = LO_TAG_ID
                txtTagName.Text = DT.Rows(0).Item("LO_TAG_Name")
                If Not IsDBNull(DT.Rows(0).Item("LO_TAG_TYPE")) Then
                    ddl_Edit_TagType.SelectedValue = DT.Rows(0).Item("LO_TAG_TYPE")
                End If

                txtDesc.Text = DT.Rows(0).Item("LO_TAG_Description")
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")

                btnSave.Focus()

            Case "ToggleStatus"

                Dim SQL As String = "UPDATE MS_LO_TAG Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  LO_TAG_ID=" & LO_TAG_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTag()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select

    End Sub

    Protected Sub ResetTag(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindTag()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListTag.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""

        BL.BindDDlPlant(ddl_Edit_Plant, False)
        BL.BindDDlLOType(ddl_Edit_Type)
        ddl_Edit_TagType.SelectedIndex = 0
        txtTagNo.Text = ""
        txtTagNo.Attributes("LO_TAG_ID") = "0"
        txtTagName.Text = ""

        txtDesc.Text = ""
        chkAvailable.Checked = True
        btnCreate.Visible = True
        ddl_Search_LO_Type.Enabled = True
        ddl_Search_Plant.Enabled = True
        ddl_Search_TagType.Enabled = True

    End Sub


#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        BL.BindDDlPlant(ddl_Search_Plant, False)
        BL.BindDDlLOType(ddl_Search_LO_Type)
        ddl_Search_TagType.SelectedIndex = 0
    End Sub

    Protected Sub ddl_Search_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_LO_Type.SelectedIndexChanged, ddl_Search_Plant.SelectedIndexChanged, ddl_Search_TagType.SelectedIndexChanged
        BindTag()
    End Sub


#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False
        txtTagNo.ReadOnly = False
        ddl_Search_LO_Type.Enabled = False
        ddl_Search_Plant.Enabled = False
        ddl_Search_TagType.Enabled = False
        txtTagNo.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListTag.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtTagNo.Text = "" Then
            lblValidation.Text = "Please insert Tag No."
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtTagName.Text = "" Then
            lblValidation.Text = "Please insert Equipment name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Type.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Oil Type"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Plant.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_TagType.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Tag Type"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim LO_TAG_ID As Integer = txtTagNo.Attributes("LO_TAG_ID")

        Dim SQL As String = "SELECT * FROM MS_LO_TAG WHERE (LO_TAG_NO='" & txtTagNo.Text.Replace("'", "''") & "' OR LO_TAG_Name='" & txtTagName.Text.Replace("'", "''") & "' ) AND LO_TAG_ID<>" & LO_TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        SQL = "SELECT * FROM MS_LO_TAG WHERE LO_TAG_ID=" & LO_TAG_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            SQL = "SELECT * FROM MS_LO_TAG WHERE LO_TAG_NO = '" & txtTagNo.Text.Replace("'", "''") & "'"
            Dim DA_ As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT_ As New DataTable
            DA_.Fill(DT_)
            If DT_.Rows.Count > 0 Then
                lblValidation.Text = "This tag is already existed"
                pnlValidation.Visible = True
                Exit Sub
            End If

            DR = DT.NewRow
            LO_TAG_ID = GetNewTagID()
            DR("LO_TAG_ID") = LO_TAG_ID
        Else
            DR = DT.Rows(0)
        End If

        DR("PLANT_ID") = ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value
        DR("LO_TAG_TYPE") = ddl_Edit_TagType.Items(ddl_Edit_TagType.SelectedIndex).Value
        DR("Oil_TYPE_ID") = ddl_Edit_Type.Items(ddl_Edit_Type.SelectedIndex).Value
        DR("LO_TAG_NO") = txtTagNo.Text
        DR("LO_TAG_Name") = txtTagName.Text
        DR("LO_TAG_Description") = txtDesc.Text
        DR("TAG_Order") = LO_TAG_ID '----------- make it default
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetTag(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

        '------------ Focus Last Edit -----------
        DT = Session("MS_LO_TAG")
        DT.DefaultView.RowFilter = "LO_TAG_ID=" & LO_TAG_ID
        If DT.DefaultView.Count > 0 Then
            Dim RowIndex As Integer = DT.Rows.IndexOf(DT.DefaultView(0).Row)
            DT.DefaultView.RowFilter = ""
            Navigation.CurrentPage = Math.Ceiling((RowIndex + 1) / Navigation.PageSize)
        End If
    End Sub

    Private Function GetNewTagID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(LO_TAG_ID),0)+1 FROM MS_LO_TAG "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

End Class