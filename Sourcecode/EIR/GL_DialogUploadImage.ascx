﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_DialogUploadImage.ascx.vb" Inherits="EIR.GL_DialogUploadImage" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
        
		    <div class="MaskDialog"></div>
		    <asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog_Picture">
		    
		                            
				 		            <h2 style="width:100%; text-align:left;">Edit Inspection for 
				 		           
				 		            <asp:DropDownList CssClass="select"  ID="ddl_Tag" runat="server" AutoPostBack="True">
                                    </asp:DropDownList><asp:Label ID="lbl_TagType" runat="server" Text="" CssClass="EditReportHeader" ></asp:Label>
                                    </h2>		
					                <table>
					                    <tbody style="border-bottom: 1px none #fff !important;">
					                    <tr>
					                        <td width="320" valign="top" style="vertical-align:top;" id="TDImage" runat="server">
					                        <table width="320" style="border-bottom: 0px none #fff !important;">
									            <tbody style="border-bottom: 0px none #fff !important;">
									            <tr>
									                <td><asp:Image ImageUrl="RenderImage.aspx" width="160px" height="160px" ID="ImgPreview1" runat="server" GenerateEmptyAlternateText="true" AlternateText="..." style="cursor:pointer;"/></td>
									                <td><asp:Image ImageUrl="RenderImage.aspx" width="160px" height="160px" ID="ImgPreview2" runat="server" GenerateEmptyAlternateText="true" AlternateText="..." style="cursor:pointer;"/></td>
									            </tr>
									            <tr>
									                <td colspan="2" align="center"><asp:Button ID="btnRefreshImage" runat="server" Text="Update/Refresh" style="visibility:hidden; width:0px;" /></td>
									            </tr>
									            </tbody>
									        </table>
									        <center>Click image to view/edit image</center>									        
									        <center style="display:none;">
									            <table>
									                <tr>
									                    <td>
									                        Update By :
									                    </td>
									                    <td>
									                        <asp:TextBox ID="txtUpdateBy" runat="server" Width="180px" Enabled="false"></asp:TextBox>
									                    </td>
									                </tr>
									            </table>
                                            </center>
					                        </td>
					                        <td width="530" style="border-bottom:0px None;">
					                         <table align="left" width="530" style="border-bottom: 1px none #fff !important;" cellpadding="0" cellspacing="0">
                                               <tbody style="border-bottom: 1px none #fff !important;">
                                                <tr>
                                                    <td align="left" valign="top" Width="80">
                                                        <b>Inspection</b>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Panel ID="pnl_Inspection" runat="server">
                                                            <asp:Repeater ID="rpt_INSP" runat="Server">
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btnINSP" runat="server" BorderColor="#f4f4f4" BorderWidth="1px"  Font-Size="10px"
                                                                        CommandName="Select" style="cursor:pointer;" Text="INSP" Width="120px" />
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" Width="80">
                                                        <b>Part No.</b>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:DropDownList ID="ddl_Part" runat="server" Width="150px" AutoPostBack="true"></asp:DropDownList>
                                                        <asp:TextBox ID="txt_Part" runat="server" Width="135px" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" Width="80">
                                                        <b>Reference</b>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:DropDownList ID="ddl_Last_Detail" runat="server" Width="300px" AutoPostBack="true" /> &nbsp; 
    									                <asp:LinkButton ID="lnk_Last_Detail" runat="server" Visible="false" >See more..</asp:LinkButton>
    									               
                                                    </td>
                                                </tr>
                                                <tr id="pnlSolved" runat="server" visible="false">
                                                    <td align="left" valign="top" Width="80">
                                                        <b>Fixed :</b>
                                                    </td>
                                                    <td valign="top">
                                                         <asp:Button ID="btnSolveYes" BackColor="White" runat="server" BorderColor="#f4f4f4"   Font-Size="10px"
                                                          BorderWidth="1px" CommandName="Select" style="cursor:pointer;" Text="Yes" 
                                                          Width="90px" />
                                                           <asp:Button ID="btnSolveNo"  BackColor="White" runat="server" BorderColor="#f4f4f4"   Font-Size="10px"
                                                          BorderWidth="1px" CommandName="Select" style="cursor:pointer;" Text="No" 
                                                          Width="90px" />    									              
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" Width="80">
                                                        <b>Status</b>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Panel ID="pnl_Status" runat="server">
                                                            <asp:Repeater ID="rpt_STATUS" runat="Server">
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btnStatus" runat="server" BorderColor="#f4f4f4"   Font-Size="10px"
                                                                        BorderWidth="1px" CommandName="Select" style="cursor:pointer;" Text="RES" 
                                                                        Width="90px" />
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" Width="80">
                                                        <b>Level</b>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Panel ID="pnl_Level" runat="server">
                                                            <asp:Repeater ID="rpt_LEVEL" runat="Server">
                                                                <ItemTemplate>
                                                                    <asp:Button ID="btnLevel" runat="server" BorderColor="#f4f4f4" Font-Size="10px"
                                                                        BorderWidth="1px" CommandName="Select" style="cursor:pointer;" Text="LEVEL" 
                                                                        Width="80px" />
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <b>Detail</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txt_Detail" runat="server" CssClass="text-input" Height="50px" 
                                                            MaxLength="1000" TextMode="MultiLine" Width="500px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <b>Recomment</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txt_Recomment" runat="server" CssClass="text-input" 
                                                            Height="50px" MaxLength="1000" TextMode="MultiLine" Width="500px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="2" style="text-align:right;">
                                                        <asp:Button ID="btnClose" runat="server" Class="button" Text="Close" />
                                                        &nbsp;
                                                        <asp:Button ID="btnUpdate" runat="server" Class="button" 
                                                            Text="Update to report" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                                                            <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                                                                ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                                                            <div>
                                                                <asp:Label ID="lblValidation" runat="server"></asp:Label>
                                                            </div>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                           </tbody>
                                        </table>
					                        </td>
					                    </tr>
					                    </tbody>
					                </table>
					                
	
	  </asp:Panel>
