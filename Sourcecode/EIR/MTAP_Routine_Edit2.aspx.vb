﻿Imports System.Data
Imports System.Data.SqlClient
Public Class MTAP_Routine_Edit2
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim CV As New Converter

    'Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.MTap_Report
    Public Picture_Path As String = ConfigurationManager.AppSettings("Picture_Path").ToString

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property

    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private Property DETAIL_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_ID")) Then
                Return ViewState("DETAIL_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_ID") = value
        End Set
    End Property

    Public Property UNIQUE_ID() As String
        Get
            Return ViewState("UNIQUE_ID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUE_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            UNIQUE_ID = Now.ToOADate.ToString.Replace(".", "")
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("MTAP_Routine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_MTAP_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='MTAP_Routine_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_MTAP_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_MTAP_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_MTAP_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")
            BindGrid()

            ImplementJavaMoneyText(txtFline,, "Center")
            ImplementJavaMoneyText(txtPeak1,, "Center")
            ImplementJavaMoneyText(txtPeak2,, "Center")
            ImplementJavaMoneyText(txtPeak3,, "Center")
            ImplementJavaMoneyText(txtPeak4,, "Center")

            ImplementJavaIntegerText(txtTemperature, False,, "Center")
            ImplementJavaMoneyText(txtResistiveImbalance,, "Center")
            ImplementJavaFloatText(txtOhm12, 5, "Center")
            ImplementJavaFloatText(txtOhm13, 5, "Center")
            ImplementJavaFloatText(txtOhm23, 5, "Center")
            ImplementJavaFloatText(txtmH12, 3, "Center")
            ImplementJavaFloatText(txtmH13, 3, "Center")
            ImplementJavaFloatText(txtmH23, 3, "Center")
            ImplementJavaMoneyText(txtInductiveImbalance,, "Center")
            ImplementJavaIntegerText(txtCapacitance, False,, "Center")
            ImplementJavaMoneyText(txtResistance,, "Center")
            ImplementJavaMoneyText(txtPI_Value,, "Center")
        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()
        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_MTAP_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='MTAP_Routine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY

                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='MTAP_Routine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='MTAP_Routine_Summary.aspx'", True)
            Exit Sub
        End If

    End Sub

    Private Sub BindGrid()
        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_MTAP_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("MTAP_Routine_Summary.aspx")
            Exit Sub
        End If

        '------------------------------Header -----------------------------------
        lbl_Plant.Text = DT.Rows(0).Item("PLANT_Code")
        lbl_Year.Text = RPT_Year
        If Not IsDBNull(DT.Rows(0).Item("RPT_Round")) Then
            lbl_Round.Text = DT.Rows(0).Item("RPT_Round")
        Else
            lbl_Round.Text = ""
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_Period_Start")) Then
            lbl_Period.Text = BL.ReportGridTime(DT.Rows(0).Item("RPT_Period_Start"))
        Else
            lbl_Period.Text = ""
        End If

        SQL = "SELECT * FROM VW_REPORT_MTAP_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbLf
        SQL &= "ORDER BY TAG_Code"
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        rptTAG.DataSource = DT
        rptTAG.DataBind()

        pnlGrid.Visible = True
        pnlData.Visible = False

    End Sub

    Private Sub BindTagData()
        '************ Tag ***********
        Dim SQL As String = "SELECT MS_MTAP_TAG.* FROM RPT_MTAP_Detail" & vbNewLine
        SQL &= " LEFT JOIN MS_MTAP_TAG ON RPT_MTAP_Detail.TAG_ID = MS_MTAP_TAG.TAG_ID" & vbNewLine
        SQL &= " WHERE DETAIL_ID = " & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            Exit Sub
        End If

        lblTagNo.Text = DT.Rows(0).Item("TAG_Code").ToString
        lblTagBrand.Text = DT.Rows(0).Item("TAG_Brand").ToString
        lblTagVoltage.Text = DT.Rows(0).Item("TAG_Voltage").ToString
        lblTagFrequency.Text = DT.Rows(0).Item("TAG_Frequency").ToString
        lblTagType.Text = DT.Rows(0).Item("TAG_Type").ToString
        If IsNumeric(DT.Rows(0).Item("TAG_Current")) Then
            lblTagCurrent.Text = FormatNumber(DT.Rows(0).Item("TAG_Current"))
        End If
        If IsNumeric(DT.Rows(0).Item("TAG_Power_Factor")) Then
            lblTagPowerFactor.Text = FormatNumber(DT.Rows(0).Item("TAG_Power_Factor"))
        End If
        If IsNumeric(DT.Rows(0).Item("TAG_KW")) Then
            lblTagKW.Text = FormatNumber(DT.Rows(0).Item("TAG_KW"))
        End If
        lblTagRPM.Text = DT.Rows(0).Item("TAG_RPM").ToString
        lblTagInsulationClass.Text = DT.Rows(0).Item("TAG_Insulation_Class").ToString


        '************ Online ***********
        SQL = "SELECT * FROM RPT_MTAP_Detail" & vbNewLine
        SQL &= " WHERE DETAIL_ID = " & DETAIL_ID
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            Exit Sub
        End If

        If Not IsDBNull(DT.Rows(0).Item("Inspec_Date")) Then
            txtInspDate.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("Inspec_Date"))
        Else
            txtInspDate.Text = ""
        End If

        If DT.Rows(0).Item("N_Fline").ToString <> "" Then
            txtFline.Text = FormatNumber(DT.Rows(0).Item("N_Fline"))
        Else
            txtFline.Text = ""
        End If
        txtFline_TextChanged(Nothing, Nothing)

        If DT.Rows(0).Item("N_Swirl_Effect").ToString <> "" Then
            ddlSwirlEffect.SelectedValue = DT.Rows(0).Item("N_Swirl_Effect")
        Else
            ddlSwirlEffect.SelectedValue = 0
        End If
        ddlSwirlEffect_SelectedIndexChanged(Nothing, Nothing)

        If DT.Rows(0).Item("N_Peak1").ToString <> "" Then
            txtPeak1.Text = FormatNumber(DT.Rows(0).Item("N_Peak1"))
        Else
            txtPeak1.Text = ""
        End If
        txtPeak1_TextChanged(Nothing, Nothing)

        If DT.Rows(0).Item("N_Peak2").ToString <> "" Then
            txtPeak2.Text = FormatNumber(DT.Rows(0).Item("N_Peak2"))
        Else
            txtPeak2.Text = ""
        End If
        txtPeak2_TextChanged(Nothing, Nothing)

        If DT.Rows(0).Item("N_Peak3").ToString <> "" Then
            txtPeak3.Text = FormatNumber(DT.Rows(0).Item("N_Peak3"))
        Else
            txtPeak3.Text = ""
        End If
        txtPeak3_TextChanged(Nothing, Nothing)

        If DT.Rows(0).Item("N_Peak4").ToString <> "" Then
            txtPeak4.Text = FormatNumber(DT.Rows(0).Item("N_Peak4"))
        Else
            txtPeak4.Text = ""
        End If
        txtPeak4_TextChanged(Nothing, Nothing)
        GetPeakCount()
        If DT.Rows(0).Item("N_Status_Peak_Above").ToString <> "" Then
            ddlPeak.SelectedValue = DT.Rows(0).Item("N_Status_Peak_Above")
        End If
        Select Case ddlPeak.SelectedValue
            Case 0
                'ไม่มีข้อมูล
                tdAbove.Attributes("class") = "PDMA_Eccentricity"
                ddlPeak.Attributes("class") = "PDMA_Eccentricity"
            Case 1
                'Good
                tdAbove.Attributes("class") = "PDMA_Eccentricity_Good"
                ddlPeak.Attributes("class") = "PDMA_Eccentricity_Good"
            Case 2
                'Modelate
                tdAbove.Attributes("class") = "PDMA_Eccentricity_Moderate"
                ddlPeak.Attributes("class") = "PDMA_Eccentricity_Moderate"
            Case 3
                'Severe
                ddlPeak.Attributes("class") = "PDMA_Eccentricity_Severe"
                tdAbove.Attributes("class") = "PDMA_Eccentricity_Severe"
        End Select

        'ddlCapacitance.Attributes("class") = "PDMA_Eccentricity_Severe"
        'tdCapacitance.Attributes("class") = "PDMA_Eccentricity_Severe"

        If DT.Rows(0).Item("N_Status_PowerQuality").ToString <> "" Then
            If DT.Rows(0).Item("N_Status_PowerQuality") = True Then
                'OK
                ddlPowerQualityStatus.SelectedValue = 1
                ddlPowerQualityStatus.BackColor = Drawing.Color.White
                ddlPowerQualityStatus.ForeColor = Drawing.Color.Black
            Else
                'AB
                ddlPowerQualityStatus.SelectedValue = 2
                ddlPowerQualityStatus.BackColor = Drawing.Color.Red
                ddlPowerQualityStatus.ForeColor = Drawing.Color.White
            End If
        Else
            ddlPowerQualityStatus.SelectedValue = 0
            ddlPowerQualityStatus.BackColor = Drawing.Color.White
        End If

        '----------------- Store Picture ------------------
        imgCurrent1.ImageUrl = "RenderImageFromPath.aspx?RPT_Year=" & DT.Rows(0).Item("RPT_Year") & "&RPT_No=" & RPT_No & "&DETAIL_ID=" & DETAIL_ID & "&IMG=1"
        imgCurrent2.ImageUrl = "RenderImageFromPath.aspx?RPT_Year=" & DT.Rows(0).Item("RPT_Year") & "&RPT_No=" & RPT_No & "&DETAIL_ID=" & DETAIL_ID & "&IMG=2"
        imgEccentricity.ImageUrl = "RenderImageFromPath.aspx?RPT_Year=" & DT.Rows(0).Item("RPT_Year") & "&RPT_No=" & RPT_No & "&DETAIL_ID=" & DETAIL_ID & "&IMG=3"
        imgPowerQuality.ImageUrl = "RenderImageFromPath.aspx?RPT_Year=" & DT.Rows(0).Item("RPT_Year") & "&RPT_No=" & RPT_No & "&DETAIL_ID=" & DETAIL_ID & "&IMG=4"


        '************ Offline ***********
        If DT.Rows(0).Item("F_Temperature").ToString <> "" Then
            txtTemperature.Text = FormatNumber(DT.Rows(0).Item("F_Temperature"))
        Else
            txtTemperature.Text = ""
        End If

        If DT.Rows(0).Item("F_Ohm12").ToString <> "" Then
            txtOhm12.Text = FormatNumber(DT.Rows(0).Item("F_Ohm12"), 5)
        Else
            txtOhm12.Text = ""
        End If

        If DT.Rows(0).Item("F_Ohm13").ToString <> "" Then
            txtOhm13.Text = FormatNumber(DT.Rows(0).Item("F_Ohm13"), 5)
        Else
            txtOhm13.Text = ""
        End If

        If DT.Rows(0).Item("F_Ohm23").ToString <> "" Then
            txtOhm23.Text = FormatNumber(DT.Rows(0).Item("F_Ohm23"), 5)
        Else
            txtOhm23.Text = ""
        End If

        If DT.Rows(0).Item("F_Resistive_Imbalance").ToString <> "" Then
            txtResistiveImbalance.Text = FormatNumber(DT.Rows(0).Item("F_Resistive_Imbalance"))
        Else
            txtResistiveImbalance.Text = ""
        End If
        If DT.Rows(0).Item("F_Status_Resistive_Imbalance").ToString <> "" Then
            If DT.Rows(0).Item("F_Status_Resistive_Imbalance") = True Then
                If CInt(lblTagVoltage.Text) <= 600 Then
                    Select Case CDbl(txtResistiveImbalance.Text)
                        Case Is >= 3
                            'Coution
                            lblResistiveImbalance.Text = BL.Get_PDMA_Color_Text("ORANGE")
                            tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("ORANGE")
                        Case Is < 3
                            'OK
                            lblResistiveImbalance.Text = BL.Get_PDMA_Color_Text("Green")
                            tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
                    End Select
                Else
                    Select Case CDbl(txtResistiveImbalance.Text)
                        Case Is >= 2
                            'Coution
                            lblResistiveImbalance.Text = BL.Get_PDMA_Color_Text("ORANGE")
                            tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("ORANGE")
                        Case Is < 2
                            'OK
                            lblResistiveImbalance.Text = BL.Get_PDMA_Color_Text("Green")
                            tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
                    End Select
                End If


            Else
                'Severe
                lblResistiveImbalance.Text = BL.Get_PDMA_Color_Text("Red")
                tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
            End If
        Else
            lblResistiveImbalance.Text = ""
            tdResistiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("")
        End If


        If DT.Rows(0).Item("F_mH12").ToString <> "" Then
            txtmH12.Text = FormatNumber(DT.Rows(0).Item("F_mH12"), 3)
        Else
            txtmH12.Text = ""
        End If

        If DT.Rows(0).Item("F_mH13").ToString <> "" Then
            txtmH13.Text = FormatNumber(DT.Rows(0).Item("F_mH13"), 3)
        Else
            txtmH13.Text = ""
        End If

        If DT.Rows(0).Item("F_mH23").ToString <> "" Then
            txtmH23.Text = FormatNumber(DT.Rows(0).Item("F_mH23"), 3)
        Else
            txtmH23.Text = ""
        End If
        GetAverageInductance()

        If DT.Rows(0).Item("F_Inductive_Imbalance").ToString <> "" Then
            txtInductiveImbalance.Text = FormatNumber(DT.Rows(0).Item("F_Inductive_Imbalance"))
        Else
            txtInductiveImbalance.Text = ""
        End If
        If DT.Rows(0).Item("F_Status_Inductive_Imbalance").ToString <> "" Then
            ddlInductiveImbalance.SelectedValue = DT.Rows(0).Item("F_Status_Inductive_Imbalance")
            Select Case DT.Rows(0).Item("F_Status_Inductive_Imbalance")
                Case 1 'OK
                    ddlInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
                    tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
                Case 2 'Coution
                    ddlInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("ORANGE")
                    tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("ORANGE")
                Case 3 'AB
                    ddlInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
                    tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
            End Select
            'If DT.Rows(0).Item("F_Status_Inductive_Imbalance") = True Then
            '    'OK
            '    lblInductiveImbalance.Text = BL.Get_PDMA_Color_Text("Green")
            '    tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
            'Else
            '    'AB
            '    If CInt(lblTagVoltage.Text) <= 600 Then
            '        Select Case CDbl(txtInductiveImbalance.Text)
            '            Case Is >= 12
            '                lblInductiveImbalance.Text = BL.Get_PDMA_Color_Text("Red")
            '                tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
            '            Case Is >= 8
            '                lblInductiveImbalance.Text = BL.Get_PDMA_Color_Text("ORANGE")
            '                tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("ORANGE")
            '        End Select
            '    Else
            '        Select Case CDbl(txtInductiveImbalance.Text)
            '            Case Is >= 7
            '                lblInductiveImbalance.Text = BL.Get_PDMA_Color_Text("Red")
            '                tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
            '            Case Is >= 5
            '                lblInductiveImbalance.Text = BL.Get_PDMA_Color_Text("ORANGE")
            '                tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("ORANGE")
            '        End Select
            '    End If
            'End If
        Else
            ddlInductiveImbalance.SelectedIndex = 0
            tdInductiveImbalance.Attributes("class") = BL.Get_PDMA_Color_CSS("")
        End If


        If DT.Rows(0).Item("F_Capacitance").ToString <> "" Then
            txtCapacitance.Text = FormatNumber(DT.Rows(0).Item("F_Capacitance"), 0)
        Else
            txtCapacitance.Text = ""
        End If

        If DT.Rows(0).Item("F_Status_Capacitance").ToString <> "" Then
            If DT.Rows(0).Item("F_Status_Capacitance") = True Then
                'OK
                ddlCapacitance.SelectedValue = 1
                ddlCapacitance.Attributes("class") = "PDMA_Eccentricity_Good"
                tdCapacitance.Attributes("class") = "PDMA_Eccentricity_Good"
            Else
                'AB
                ddlCapacitance.SelectedValue = 2
                ddlCapacitance.Attributes("class") = "PDMA_Eccentricity_Severe"
                tdCapacitance.Attributes("class") = "PDMA_Eccentricity_Severe"
            End If
        Else
            ddlCapacitance.SelectedValue = 0
            ddlCapacitance.Attributes("class") = "PDMA_Eccentricity"
            tdCapacitance.Attributes("class") = "PDMA_Eccentricity"
        End If

        If DT.Rows(0).Item("F_Status_Resistance").ToString <> "" Then
            If DT.Rows(0).Item("F_Status_Resistance") = True Then
                'OK
                lblResistance.Text = BL.Get_PDMA_Color_Text("Green")
                tdResistance.Attributes("class") = BL.Get_PDMA_Color_CSS("Green")
            Else
                'AB
                lblResistance.Text = BL.Get_PDMA_Color_Text("Red")
                tdResistance.Attributes("class") = BL.Get_PDMA_Color_CSS("Red")
            End If
        Else
            lblResistance.Text = ""
            tdResistance.Attributes("class") = BL.Get_PDMA_Color_CSS("")
        End If

        If DT.Rows(0).Item("F_Resistance").ToString <> "" Then
            txtResistance.Text = FormatNumber(DT.Rows(0).Item("F_Resistance"))
        Else
            txtResistance.Text = ""
        End If
        'txtResistance_TextChanged(Nothing, Nothing)

        If DT.Rows(0).Item("F_PI_Value").ToString <> "" Then
            txtPI_Value.Text = FormatNumber(DT.Rows(0).Item("F_PI_Value"))
        Else
            txtPI_Value.Text = ""
        End If
        txtPI_Value_TextChanged(Nothing, Nothing)

        imgPolarization.ImageUrl = "RenderImageFromPath.aspx?RPT_Year=" & DT.Rows(0).Item("RPT_Year") & "&RPT_No=" & RPT_No & "&DETAIL_ID=" & DETAIL_ID & "&IMG=5"

        pnlData.Visible = True
        pnlGrid.Visible = False

        '--------------- Set Mode online or offline------------
        If Not IsDBNull(DT.Rows(0).Item("TAG_Mode")) Then
            ddlSelectMode.SelectedIndex = DT.Rows(0).Item("TAG_Mode")
        Else
            ddlSelectMode.SelectedIndex = 0
        End If
        ddlSelectMode_SelectedIndexChanged(Nothing, Nothing)


    End Sub

    Protected Sub rptTAG_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTAG.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblTagCode As LinkButton = e.Item.FindControl("lblTagCode")

        Dim ddlMode As DropDownList = e.Item.FindControl("ddlMode")
        Dim btnSave As Button = e.Item.FindControl("btnSave")
        ddlMode.Attributes("onchange") = "document.getElementById('" & btnSave.ClientID & "').click();"

        Dim lblPowerQuality As LinkButton = e.Item.FindControl("lblPowerQuality")
        Dim tdPowerQuality As HtmlTableCell = e.Item.FindControl("tdPowerQuality")
        Dim ddlPowerQuality As DropDownList = e.Item.FindControl("ddlPowerQuality")

        Dim lblInsulation As LinkButton = e.Item.FindControl("lblInsulation")
        Dim tdInsulation As HtmlTableCell = e.Item.FindControl("tdInsulation")

        Dim lblPowerCircuit As LinkButton = e.Item.FindControl("lblPowerCircuit")
        Dim tdPowerCircuit As HtmlTableCell = e.Item.FindControl("tdPowerCircuit")
        Dim ddlPowerCircuit As DropDownList = e.Item.FindControl("ddlPowerCircuit")

        Dim lblStator As LinkButton = e.Item.FindControl("lblStator")
        Dim tdStator As HtmlTableCell = e.Item.FindControl("tdStator")
        Dim ddlStator As DropDownList = e.Item.FindControl("ddlStator")
        ddlStator.Attributes("onchange") = "document.getElementById('" & btnSave.ClientID & "').click();"

        Dim lblRotor As LinkButton = e.Item.FindControl("lblRotor")
        Dim tdRotor As HtmlTableCell = e.Item.FindControl("tdRotor")

        Dim lblAirGap As LinkButton = e.Item.FindControl("lblAirGap")
        Dim tdAirGap As HtmlTableCell = e.Item.FindControl("tdAirGap")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnReport As HtmlAnchor = e.Item.FindControl("btnReport")
        Dim btnInComplete As ImageButton = e.Item.FindControl("btnInComplete")

        lblNo.Text = e.Item.ItemIndex + 1
        lblTagCode.Text = e.Item.DataItem("TAG_CODE")
        lblTagCode.CommandArgument = e.Item.DataItem("DETAIL_ID")
        If e.Item.DataItem("TAG_Mode").ToString = "" Then
            ddlMode.SelectedValue = 0
        Else
            ddlMode.SelectedValue = e.Item.DataItem("TAG_Mode")
        End If
        btnEdit.CommandArgument = e.Item.DataItem("DETAIL_ID")
        btnSave.CommandArgument = e.Item.DataItem("DETAIL_ID")
        btnReport.Attributes("onClick") = "ShowPreviewLOTag(" & e.Item.DataItem("RPT_Year") & "," & e.Item.DataItem("RPT_No") & "," & e.Item.DataItem("Detail_ID") & ");"
        btnInComplete.CommandArgument = e.Item.DataItem("DETAIL_ID")
        btnInComplete.Visible = e.Item.DataItem("IS_Complete") = 0

        Select Case ddlMode.SelectedValue
            Case 0
                'ไม่เลือก
                ddlPowerQuality.Visible = False
                ddlPowerCircuit.Visible = False
                ddlStator.Visible = False
            Case 1
                '****************** Online ******************
                'PowerQuality
                lblPowerQuality.Visible = False
                If Not IsDBNull(e.Item.DataItem("N_Status_PowerQuality")) Then
                    If e.Item.DataItem("N_Status_PowerQuality") Then
                        'OK
                        ddlPowerQuality.SelectedValue = 1
                    Else
                        'AB
                        ddlPowerQuality.SelectedValue = 2
                        'tdPowerQuality.Attributes("class") = "LevelClassA"
                        ddlPowerQuality.Attributes("class") = "LevelClassA"
                    End If
                End If

                'Insulation
                lblInsulation.Text = "N/A"
                'PowerCircuit
                lblPowerCircuit.Visible = False
                If Not IsDBNull(e.Item.DataItem("N_Status_PowerCircuit")) Then
                    If e.Item.DataItem("N_Status_PowerCircuit") Then
                        'OK
                        ddlPowerCircuit.SelectedValue = 1
                    Else
                        'AB
                        ddlPowerCircuit.SelectedValue = 2
                        'tdPowerCircuit.Attributes("class") = "LevelClassA"
                        ddlPowerCircuit.Attributes("class") = "LevelClassA"
                    End If
                End If

                '------------------ Set Action -------------------
                Dim txtPowerFrom As TextBox = e.Item.FindControl("txtPowerFrom")
                ddlPowerQuality.Attributes("onchange") = "document.getElementById('" & txtPowerFrom.ClientID & "').value='Q';document.getElementById('" & btnSave.ClientID & "').click();"
                ddlPowerCircuit.Attributes("onchange") = "document.getElementById('" & txtPowerFrom.ClientID & "').value='C';document.getElementById('" & btnSave.ClientID & "').click();"

                'Stator
                lblStator.Visible = False
                If Not IsDBNull(e.Item.DataItem("N_Status_Stator")) Then
                    If e.Item.DataItem("N_Status_Stator") Then
                        'OK
                        ddlStator.SelectedValue = 1
                    Else
                        'AB
                        ddlStator.SelectedValue = 2
                        'tdStator.Attributes("class") = "LevelClassA"
                        ddlStator.Attributes("class") = "LevelClassA"
                    End If
                End If

                'Rotor
                If Not IsDBNull(e.Item.DataItem("N_Status_Rotor")) Then
                    If e.Item.DataItem("N_Status_Rotor") Then
                        'OK
                        lblRotor.Text = "OK"
                    Else
                        'AB
                        lblRotor.Text = "AB"
                        tdRotor.Attributes("class") = "LevelClassA"
                        lblRotor.ForeColor = Drawing.Color.White
                        lblRotor.BackColor = Drawing.Color.Red
                    End If
                End If

                'AirGap
                If Not IsDBNull(e.Item.DataItem("N_Status_AirGap")) Then
                    If e.Item.DataItem("N_Status_AirGap") Then
                        'OK
                        lblAirGap.Text = "OK"
                    Else
                        'AB
                        lblAirGap.Text = "AB"
                        tdAirGap.Attributes("class") = "LevelClassA"
                        lblAirGap.ForeColor = Drawing.Color.White
                        lblAirGap.BackColor = Drawing.Color.Red
                    End If
                End If

            Case 2
                '****************** Offline ******************
                'PowerQuality
                lblPowerQuality.Text = "N/A"
                ddlPowerQuality.Visible = False

                'Insulation
                If Not IsDBNull(e.Item.DataItem("F_Status_Insulation")) Then
                    If e.Item.DataItem("F_Status_Insulation") Then
                        'OK
                        lblInsulation.Text = "OK"
                    Else
                        'AB
                        lblInsulation.Text = "AB"
                        tdInsulation.Attributes("class") = "LevelClassA"
                        lblInsulation.ForeColor = Drawing.Color.White
                        lblInsulation.BackColor = Drawing.Color.Red
                    End If
                End If

                'PowerCircuit
                ddlPowerCircuit.Visible = False
                If Not IsDBNull(e.Item.DataItem("F_Status_PowerCircuit")) Then
                    If e.Item.DataItem("F_Status_PowerCircuit") Then
                        'OK
                        lblPowerCircuit.Text = "OK"
                    Else
                        'AB
                        lblPowerCircuit.Text = "AB"
                        tdPowerCircuit.Attributes("class") = "LevelClassA"
                        lblPowerCircuit.ForeColor = Drawing.Color.White
                        lblPowerCircuit.BackColor = Drawing.Color.Red
                    End If
                End If

                'Stator
                lblStator.Visible = False
                If Not IsDBNull(e.Item.DataItem("F_Status_Stator")) Then
                    If e.Item.DataItem("F_Status_Stator") = True Then
                        'OK
                        ddlStator.SelectedValue = 1
                    Else
                        'AB
                        ddlStator.SelectedValue = 2
                        'tdStator.Attributes("class") = "LevelClassA"
                        ddlStator.Attributes("class") = "LevelClassA"
                    End If
                End If

                'Rotor
                lblRotor.Text = "N/A"

                'AirGap
                lblAirGap.Text = "N/A"
        End Select
    End Sub

    Protected Sub rptTAG_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTAG.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        DETAIL_ID = btnEdit.CommandArgument

        Select Case e.CommandName
            Case "Edit"

                BindTagData()

            Case "Save"

                Dim ddlMode As DropDownList = e.Item.FindControl("ddlMode")
                Dim ddlPowerQuality As DropDownList = e.Item.FindControl("ddlPowerQuality")
                Dim ddlPowerCircuit As DropDownList = e.Item.FindControl("ddlPowerCircuit")
                Dim ddlStator As DropDownList = e.Item.FindControl("ddlStator")

                Dim Mode As Integer = ddlMode.Items(ddlMode.SelectedIndex).Value
                If Mode > 0 Then
                    SaveData("TAG_Mode", Mode, DETAIL_ID)
                    If Mode = 1 Then  '------------- Onlilne -----------

                        '------------- For both of Power Quality And Circuit ---------
                        Dim txtPowerFrom As TextBox = e.Item.FindControl("txtPowerFrom")
                        Select Case txtPowerFrom.Text
                            Case "Q" '------------- Change From Power Quality---------
                                If ddlPowerQuality.SelectedIndex = 0 Then
                                    SaveData("N_Status_PowerQuality", "", DETAIL_ID)
                                Else
                                    SaveData("N_Status_PowerQuality", ddlPowerQuality.SelectedIndex = 1, DETAIL_ID)
                                End If
                            Case "C" '------------- Change From Power Circuit---------
                                If ddlPowerCircuit.SelectedIndex = 0 Then
                                    SaveData("N_Status_PowerCircuit", "", DETAIL_ID)
                                Else
                                    SaveData("N_Status_PowerCircuit", ddlPowerCircuit.SelectedIndex = 1, DETAIL_ID)
                                End If
                        End Select
                    End If

                    '------------ ทั้ง Online และ Offline Save เหมือนกันคือ Stator ------------
                    If ddlStator.SelectedIndex = 0 Then
                        SaveData("F_Status_Stator", "", DETAIL_ID)
                        SaveData("N_Status_Stator", "", DETAIL_ID)
                    Else
                        SaveData("F_Status_Stator", ddlStator.SelectedIndex = 1, DETAIL_ID)
                        SaveData("N_Status_Stator", ddlStator.SelectedIndex = 1, DETAIL_ID)
                    End If

                Else
                    SaveData("TAG_Mode", "", DETAIL_ID)
                End If
                BindGrid()
        End Select
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        If pnlData.Visible Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewLOTag(" & RPT_Year & "," & RPT_No & "," & DETAIL_ID & ");", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
        End If

    End Sub

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='MTAP_Routine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='MTAP_Routine_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click

        If pnlData.Visible Then
            BL.Drop_RPT_MTAP_Detail(DETAIL_ID)
            BL.Construct_MTAP_Report_Detail(RPT_Year, RPT_No, Session("USER_ID"))
            BindTagData()
        Else
            BL.Drop_RPT_MTAP_Detail(RPT_Year, RPT_No)
            BL.Construct_MTAP_Report_Detail(RPT_Year, RPT_No, Session("USER_ID"))
            BindGrid()
        End If

    End Sub

    Protected Sub btn_Back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Back.Click
        If pnlData.Visible Then
            BindGrid()
        Else
            HTabHeader_Click(sender, e)
        End If
    End Sub

    Protected Sub btn_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Next.Click
        HTabSummary_Click(sender, e)
    End Sub

    Protected Sub ddlSelectMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSelectMode.SelectedIndexChanged
        pnlOnline.Visible = ddlSelectMode.Items(ddlSelectMode.SelectedIndex).Value = 1
        pnlOffline.Visible = ddlSelectMode.Items(ddlSelectMode.SelectedIndex).Value = 2
        SaveData("TAG_Mode", ddlSelectMode.SelectedValue, DETAIL_ID)
    End Sub

#Region "Online"

#Region "Fline"
    Protected Sub txtFline_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFline.TextChanged
        If txtFline.Text <> "" Then
            lblFline.Text = BL.Get_PDMA_AnalyzingCS_Text(CDbl(txtFline.Text))
            tdFline.Attributes("class") = BL.Get_PDMA_AnalyzingCS_CSS(CDbl(txtFline.Text))
            If tdFline.Attributes("class") = "MTAP_CS_7" Then
                lblFline.ForeColor = Drawing.Color.White
            Else
                lblFline.ForeColor = Drawing.Color.Black
            End If
        Else
            lblFline.Text = ""
            tdFline.Attributes("class") = BL.Get_PDMA_AnalyzingCS_CSS(-1)
        End If

        If Not sender Is Nothing Then
            SaveData("N_Fline", txtFline.Text, DETAIL_ID)
            If lblFline.Text = "Severe" Then
                SaveData("N_Status_Fline", False, DETAIL_ID)
                SaveData("N_Status_Rotor", False, DETAIL_ID)
            Else
                SaveData("N_Status_Fline", True, DETAIL_ID)
                SaveData("N_Status_Rotor", True, DETAIL_ID)
            End If
        End If

    End Sub
#End Region

#Region "SwirlEffect"
    Protected Sub ddlSwirlEffect_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSwirlEffect.SelectedIndexChanged
        Select Case ddlSwirlEffect.SelectedValue
            Case 0
                lblSwirlEffect.Text = BL.Get_PDMA_AnalyzingCS_Text(-1)
                tdSwirlEffect.Attributes("class") = BL.Get_PDMA_AnalyzingCS_CSS(-1)
                lblSwirlEffect.ForeColor = Drawing.Color.Black
            Case 1
                lblSwirlEffect.Text = BL.Get_PDMA_AnalyzingCS_Text(1)
                tdSwirlEffect.Attributes("class") = BL.Get_PDMA_AnalyzingCS_CSS(1)
                lblSwirlEffect.ForeColor = Drawing.Color.White
            Case 2
                lblSwirlEffect.Text = BL.Get_PDMA_AnalyzingCS_Text(100)
                tdSwirlEffect.Attributes("class") = BL.Get_PDMA_AnalyzingCS_CSS(100)
                lblSwirlEffect.ForeColor = Drawing.Color.Black
        End Select
        If Not sender Is Nothing Then
            SaveData("N_Swirl_Effect", ddlSwirlEffect.SelectedValue, DETAIL_ID)
            If lblSwirlEffect.Text = "Severe" Then
                SaveData("N_Status_Swirl_Effect", False, DETAIL_ID)
            Else
                SaveData("N_Status_Swirl_Effect", True, DETAIL_ID)
            End If
        End If

    End Sub
#End Region

#Region "Peak"
    Protected Sub txtPeak1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPeak1.TextChanged
        If txtPeak1.Text <> "" Then
            tdPeak1.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(CDbl(txtPeak1.Text))
        Else
            tdPeak1.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(-1)
        End If
        GetPeakCount()
        If Not sender Is Nothing Then
            SaveData("N_Peak1", txtPeak1.Text, DETAIL_ID)
        End If

    End Sub

    Protected Sub txtPeak2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPeak2.TextChanged
        If txtPeak2.Text <> "" Then
            tdPeak2.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(CDbl(txtPeak2.Text))
        Else
            tdPeak2.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(-1)
        End If
        GetPeakCount()
        If Not sender Is Nothing Then
            SaveData("N_Peak2", txtPeak2.Text, DETAIL_ID)
        End If
    End Sub

    Protected Sub txtPeak3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPeak3.TextChanged
        If txtPeak3.Text <> "" Then
            tdPeak3.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(CDbl(txtPeak3.Text))
        Else
            tdPeak3.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(-1)
        End If
        GetPeakCount()
        If Not sender Is Nothing Then
            SaveData("N_Peak3", txtPeak3.Text, DETAIL_ID)
        End If

    End Sub

    Protected Sub txtPeak4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPeak4.TextChanged
        If txtPeak4.Text <> "" Then
            tdPeak4.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(CDbl(txtPeak4.Text))
        Else
            tdPeak4.Attributes("class") = BL.Get_PDMA_AnalyzingEccentricity_CSS(-1)
        End If
        GetPeakCount()
        If Not sender Is Nothing Then
            SaveData("N_Peak4", txtPeak4.Text, DETAIL_ID)
        End If

    End Sub

    Sub GetPeakCount()
        Dim Count As Integer = 0
        If tdPeak1.Attributes("class") = "PDMA_Eccentricity_Severe" Then
            Count = Count + 1
        End If
        If tdPeak2.Attributes("class") = "PDMA_Eccentricity_Severe" Then
            Count = Count + 1
        End If
        If tdPeak3.Attributes("class") = "PDMA_Eccentricity_Severe" Then
            Count = Count + 1
        End If
        If tdPeak4.Attributes("class") = "PDMA_Eccentricity_Severe" Then
            Count = Count + 1
        End If
        lblPeakCount.Text = Count
    End Sub

    Protected Sub ddlPeak_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPeak.SelectedIndexChanged

        If ddlPeak.SelectedIndex = 0 Then
            SaveData("N_Status_Peak_Above", "", DETAIL_ID)
            SaveData("N_Status_AirGap", "", DETAIL_ID)
        Else
            SaveData("N_Status_Peak_Above", ddlPeak.SelectedValue, DETAIL_ID)
            SaveData("N_Status_AirGap", ddlPeak.SelectedValue <> 3, DETAIL_ID)
        End If
        BindTagData()
    End Sub
#End Region

#Region "Online Image"
    Protected Sub imgCurrent1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCurrent1.Click
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_1") = BL.Get_PDMA_Image(RPT_Year, RPT_No, DETAIL_ID, "N_Img1")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & UNIQUE_ID & "',1,document.getElementById('" & btnSaveImageCurrent1.ClientID & "'));", True)
    End Sub

    Protected Sub btnSaveImageCurrent1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveImageCurrent1.Click
        If IsNothing(Session("TempImage_" & UNIQUE_ID & "_1")) Then Exit Sub
        '------------------------ Save ------------------------
        Dim Pic_Detail As Byte() = Session("TempImage_" & UNIQUE_ID & "_1")
        If BL.Save_Picture_File(Pic_Detail, RPT_Year, RPT_No, DETAIL_ID, 1) = True Then
            Dim Path As String = Picture_Path & "\" & RPT_Year
            Path &= "\" & RPT_No
            Path &= "\" & DETAIL_ID & "_1"
            SaveData("N_Img1", Path, DETAIL_ID)
        End If
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_1") = Session("TempImage_" & UNIQUE_ID & "_1")
        imgCurrent1.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_ID & "&Image=1"

    End Sub

    Protected Sub imgCurrent2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCurrent2.Click
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_2") = BL.Get_PDMA_Image(RPT_Year, RPT_No, DETAIL_ID, "N_Img2")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & UNIQUE_ID & "',2,document.getElementById('" & btnSaveImageCurrent2.ClientID & "'));", True)
    End Sub

    Protected Sub btnSaveImageCurrent2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveImageCurrent2.Click
        If IsNothing(Session("TempImage_" & UNIQUE_ID & "_2")) Then Exit Sub
        '------------------------ Save ------------------------
        Dim Pic_Detail As Byte() = Session("TempImage_" & UNIQUE_ID & "_2")
        If BL.Save_Picture_File(Pic_Detail, RPT_Year, RPT_No, DETAIL_ID, 2) = True Then
            Dim Path As String = Picture_Path & "\" & RPT_Year
            Path &= "\" & RPT_No
            Path &= "\" & DETAIL_ID & "_2"
            SaveData("N_Img2", Path, DETAIL_ID)
        End If
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_2") = Session("TempImage_" & UNIQUE_ID & "_2")
        imgCurrent2.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_ID & "&Image=2"

    End Sub

    Protected Sub imgEccentricity_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEccentricity.Click
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_3") = BL.Get_PDMA_Image(RPT_Year, RPT_No, DETAIL_ID, "N_Img3")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & UNIQUE_ID & "',3,document.getElementById('" & btnSaveImageEccentricity.ClientID & "'));", True)
    End Sub

    Protected Sub btnSaveImageEccentricity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveImageEccentricity.Click
        If IsNothing(Session("TempImage_" & UNIQUE_ID & "_3")) Then Exit Sub
        '------------------------ Save ------------------------
        Dim Pic_Detail As Byte() = Session("TempImage_" & UNIQUE_ID & "_3")
        If BL.Save_Picture_File(Pic_Detail, RPT_Year, RPT_No, DETAIL_ID, 3) = True Then
            Dim Path As String = Picture_Path & "\" & RPT_Year
            Path &= "\" & RPT_No
            Path &= "\" & DETAIL_ID & "_3"
            SaveData("N_Img3", Path, DETAIL_ID)
        End If
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_3") = Session("TempImage_" & UNIQUE_ID & "_3")
        imgEccentricity.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_ID & "&Image=3"
    End Sub

    Protected Sub imgPowerQuality_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPowerQuality.Click
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_4") = BL.Get_PDMA_Image(RPT_Year, RPT_No, DETAIL_ID, "N_Img4")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & UNIQUE_ID & "',4,document.getElementById('" & btnSaveImagePowerQuality.ClientID & "'));", True)
    End Sub

    Protected Sub btnSaveImagePowerQuality_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveImagePowerQuality.Click
        If IsNothing(Session("TempImage_" & UNIQUE_ID & "_4")) Then Exit Sub
        '------------------------ Save ------------------------
        Dim Pic_Detail As Byte() = Session("TempImage_" & UNIQUE_ID & "_4")
        If BL.Save_Picture_File(Pic_Detail, RPT_Year, RPT_No, DETAIL_ID, 4) = True Then
            Dim Path As String = Picture_Path & "\" & RPT_Year
            Path &= "\" & RPT_No
            Path &= "\" & DETAIL_ID & "_4"
            SaveData("N_Img4", Path, DETAIL_ID)
        End If
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_4") = Session("TempImage_" & UNIQUE_ID & "_4")
        imgPowerQuality.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_ID & "&Image=4"
    End Sub

    Protected Sub ddlPowerQualityStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPowerQualityStatus.SelectedIndexChanged
        Select Case ddlPowerQualityStatus.SelectedValue
            Case 0
                SaveData("N_Status_PowerQuality", "", DETAIL_ID)
                ddlPowerQualityStatus.BackColor = Drawing.Color.White
            Case 1
                'OK
                SaveData("N_Status_PowerQuality", True, DETAIL_ID)
                ddlPowerQualityStatus.BackColor = Drawing.Color.White
                ddlPowerQualityStatus.ForeColor = Drawing.Color.Black
            Case 2
                'AB
                SaveData("N_Status_PowerQuality", False, DETAIL_ID)
                ddlPowerQualityStatus.BackColor = Drawing.Color.Red
                ddlPowerQualityStatus.ForeColor = Drawing.Color.White
        End Select
    End Sub
#End Region

#End Region

#Region "Offline"
    Protected Sub txtTemperature_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTemperature.TextChanged
        If Not sender Is Nothing Then
            SaveData("F_Temperature", txtTemperature.Text, DETAIL_ID)
        End If
    End Sub

    Protected Sub txtOhm12_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOhm12.TextChanged
        If Not sender Is Nothing Then
            SaveData("F_Ohm12", txtOhm12.Text, DETAIL_ID)
        End If
    End Sub

    Protected Sub txtOhm13_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOhm13.TextChanged
        If Not sender Is Nothing Then
            SaveData("F_Ohm13", txtOhm13.Text, DETAIL_ID)
        End If
    End Sub

    Protected Sub txtOhm23_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOhm23.TextChanged
        If Not sender Is Nothing Then
            SaveData("F_Ohm23", txtOhm23.Text, DETAIL_ID)
        End If
    End Sub

    Protected Sub txtResistiveImbalance_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtResistiveImbalance.TextChanged
        If txtResistiveImbalance.Text <> "" Then
            If lblTagVoltage.Text <> "" Then
                If CInt(lblTagVoltage.Text) <= 600 Then
                    Select Case CDbl(txtResistiveImbalance.Text)
                        Case Is >= 4
                            SaveData("F_Status_Resistive_Imbalance", False, DETAIL_ID)
                            SaveData("F_Status_PowerCircuit", False, DETAIL_ID)
                        Case Is >= 3
                            SaveData("F_Status_Resistive_Imbalance", True, DETAIL_ID)
                            SaveData("F_Status_PowerCircuit", True, DETAIL_ID)
                        Case Else
                            SaveData("F_Status_Resistive_Imbalance", True, DETAIL_ID)
                            SaveData("F_Status_PowerCircuit", True, DETAIL_ID)
                    End Select
                Else
                    Select Case CDbl(txtResistiveImbalance.Text)
                        Case Is >= 3
                            SaveData("F_Status_Resistive_Imbalance", False, DETAIL_ID)
                            SaveData("F_Status_PowerCircuit", False, DETAIL_ID)
                        Case Is >= 2
                            SaveData("F_Status_Resistive_Imbalance", True, DETAIL_ID)
                            SaveData("F_Status_PowerCircuit", True, DETAIL_ID)
                        Case Else
                            SaveData("F_Status_Resistive_Imbalance", True, DETAIL_ID)
                            SaveData("F_Status_PowerCircuit", True, DETAIL_ID)
                    End Select
                End If
            Else
                SaveData("F_Status_Resistive_Imbalance", "", DETAIL_ID)
                SaveData("F_Status_PowerCircuit", "", DETAIL_ID)
            End If
        Else
            SaveData("F_Status_Resistive_Imbalance", "", DETAIL_ID)
            SaveData("F_Status_PowerCircuit", "", DETAIL_ID)
        End If

        SaveData("F_Resistive_Imbalance", txtResistiveImbalance.Text, DETAIL_ID)
        BindTagData()
    End Sub

    Protected Sub txtmH12_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtmH12.TextChanged
        GetAverageInductance()
        If Not sender Is Nothing Then
            SaveData("F_mH12", txtmH12.Text, DETAIL_ID)
        End If
    End Sub

    Protected Sub txtmH13_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtmH13.TextChanged
        GetAverageInductance()
        If Not sender Is Nothing Then
            SaveData("F_mH13", txtmH13.Text, DETAIL_ID)
        End If
    End Sub

    Protected Sub txtmH23_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtmH23.TextChanged
        GetAverageInductance()
        If Not sender Is Nothing Then
            SaveData("F_mH23", txtmH23.Text, DETAIL_ID)
        End If
    End Sub

    Sub GetAverageInductance()
        Dim Value As Double = 0
        If txtmH12.Text <> "" Then
            Value = Value + txtmH12.Text
        End If
        If txtmH13.Text <> "" Then
            Value = Value + txtmH13.Text
        End If
        If txtmH23.Text <> "" Then
            Value = Value + txtmH23.Text
        End If

        If Value > 0 Then
            lblAverageInductance.Text = FormatNumber(Value / 3, 3)
        Else
            lblAverageInductance.Text = ""
        End If

    End Sub

    Protected Sub txtInductiveImbalance_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInductiveImbalance.TextChanged
        'If txtInductiveImbalance.Text <> "" Then
        '    If lblTagVoltage.Text <> "" Then
        '        If CInt(lblTagVoltage.Text) <= 600 Then
        '            Select Case CDbl(txtInductiveImbalance.Text)
        '                Case Is >= 12
        '                    SaveData("F_Status_Inductive_Imbalance", False, DETAIL_ID)
        '                Case Is >= 8
        '                    SaveData("F_Status_Inductive_Imbalance", False, DETAIL_ID)
        '                Case Else
        '                    SaveData("F_Status_Inductive_Imbalance", True, DETAIL_ID)
        '            End Select
        '        Else
        '            Select Case CDbl(txtInductiveImbalance.Text)
        '                Case Is >= 7
        '                    SaveData("F_Status_Inductive_Imbalance", False, DETAIL_ID)
        '                Case Is >= 5
        '                    SaveData("F_Status_Inductive_Imbalance", False, DETAIL_ID)
        '                Case Else
        '                    SaveData("F_Status_Inductive_Imbalance", True, DETAIL_ID)
        '            End Select
        '        End If
        '    Else
        '        SaveData("F_Status_Inductive_Imbalance", "", DETAIL_ID)
        '    End If
        'Else
        '    SaveData("F_Status_Inductive_Imbalance", "", DETAIL_ID)
        'End If

        SaveData("F_Inductive_Imbalance", txtInductiveImbalance.Text, DETAIL_ID)
        BindTagData()
    End Sub

    Protected Sub ddlCapacitance_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCapacitance.SelectedIndexChanged
        Select Case ddlCapacitance.SelectedValue
            Case 0
                SaveData("F_Status_Capacitance", "", DETAIL_ID)
                BindTagData()
            Case 1
                'OK
                SaveData("F_Status_Capacitance", True, DETAIL_ID)
                BindTagData()
            Case 2
                'AB
                SaveData("F_Status_Capacitance", False, DETAIL_ID)
                BindTagData()
        End Select
        CheckUpdateStatusInsulation()
    End Sub

    Protected Sub ddlInductiveImbalance_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInductiveImbalance.SelectedIndexChanged
        SaveData("F_Status_Inductive_Imbalance", ddlInductiveImbalance.SelectedValue, DETAIL_ID)
        BindTagData()
    End Sub

    Protected Sub txtResistance_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtResistance.TextChanged
        If txtResistance.Text <> "" Then
            If lblTagVoltage.Text <> "" Then
                If CInt(lblTagVoltage.Text) <= 600 Then
                    Select Case CDbl(txtResistance.Text)
                        Case Is <= 5
                            SaveData("F_Status_Resistance", False, DETAIL_ID)
                        Case Else
                            SaveData("F_Status_Resistance", True, DETAIL_ID)
                    End Select
                Else
                    Select Case CDbl(txtResistance.Text)
                        Case Is <= 100
                            SaveData("F_Status_Resistance", False, DETAIL_ID)
                        Case Else
                            SaveData("F_Status_Resistance", True, DETAIL_ID)
                    End Select
                End If
            Else
                SaveData("F_Status_Resistance", "", DETAIL_ID)
            End If
        Else
            SaveData("F_Status_Resistance", "", DETAIL_ID)
        End If

        SaveData("F_Resistance", txtResistance.Text, DETAIL_ID)
        BindTagData()
        CheckUpdateStatusInsulation()
    End Sub

    Protected Sub txtPI_Value_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPI_Value.TextChanged
        If txtPI_Value.Text <> "" Then
            lblPI_Value.Text = BL.Get_PDMA_AnalyzingPI_Text(CDbl(txtPI_Value.Text))
            tdPI_Value.Attributes("class") = BL.Get_PDMA_AnalyzingPI_CSS(CDbl(txtPI_Value.Text))
        Else
            lblPI_Value.Text = ""
            tdPI_Value.Attributes("class") = BL.Get_PDMA_AnalyzingPI_CSS(-1)
        End If

        If Not sender Is Nothing Then
            SaveData("F_PI_Value", txtPI_Value.Text, DETAIL_ID)
            CheckUpdateStatusInsulation()
            If tdPI_Value.Attributes("class") = "MTAP_PI_Severe" Then
                SaveData("F_Status_PI_Value", False, DETAIL_ID)
            Else
                SaveData("F_Status_PI_Value", True, DETAIL_ID)
            End If
        End If
    End Sub

    Sub CheckUpdateStatusInsulation()
        Dim Count As Integer = 0
        If tdCapacitance.Attributes("class") = "MTAP_PI_Severe" Then
            Count = Count + 1
        End If
        If tdResistance.Attributes("class") = "MTAP_PI_Severe" Then
            Count = Count + 1
        End If
        If tdPI_Value.Attributes("class") = "MTAP_PI_Severe" Then
            Count = Count + 1
        End If

        If Count = 0 Then
            SaveData("F_Status_Insulation", True, DETAIL_ID)
        Else
            SaveData("F_Status_Insulation", False, DETAIL_ID)
        End If
    End Sub

    Protected Sub imgPolarization_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPolarization.Click
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_5") = BL.Get_PDMA_Image(RPT_Year, RPT_No, DETAIL_ID, "F_img5")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & UNIQUE_ID & "',5,document.getElementById('" & btnSaveImagePolarization.ClientID & "'));", True)
    End Sub

    Protected Sub btnSaveImagePolarization_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveImagePolarization.Click
        If IsNothing(Session("TempImage_" & UNIQUE_ID & "_5")) Then Exit Sub
        '------------------------ Save ------------------------
        Dim Pic_Detail As Byte() = Session("TempImage_" & UNIQUE_ID & "_5")
        If BL.Save_Picture_File(Pic_Detail, RPT_Year, RPT_No, DETAIL_ID, 5) = True Then
            Dim Path As String = Picture_Path & "\" & RPT_Year
            Path &= "\" & RPT_No
            Path &= "\" & DETAIL_ID & "_5"
            SaveData("F_img5", Path, DETAIL_ID)
        End If
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_5") = Session("TempImage_" & UNIQUE_ID & "_5")
        imgPolarization.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_ID & "&Image=5"
    End Sub
#End Region

    Private Sub SaveData(ByVal Field As String, ByVal Value As String, ByVal TagCode As String)
        Dim Sql As String = ""
        Sql = "select * from RPT_MTAP_Detail where Detail_ID = " & DETAIL_ID
        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If Value <> "" Then
                If Field = "Inspec_Date" Then
                    If Not BL.IsProgrammingDate(Value) Then
                        DT.Rows(0).Item(Field) = DBNull.Value
                    Else
                        DT.Rows(0).Item(Field) = CV.StringToDate(Value, "yyyy-MM-dd")
                    End If
                Else
                    DT.Rows(0).Item(Field) = Value.Replace(",", "")
                End If
            Else
                DT.Rows(0).Item(Field) = DBNull.Value
            End If
            Dim cmb As New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If
    End Sub

    Protected Sub txtCapacitance_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCapacitance.TextChanged
        SaveData("F_Capacitance", txtCapacitance.Text, DETAIL_ID)
        BindTagData()
    End Sub

    Protected Sub txtInspDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInspDate.TextChanged
        SaveData("Inspec_Date", txtInspDate.Text, DETAIL_ID)
    End Sub

End Class