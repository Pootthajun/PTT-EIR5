﻿
Imports System.Data.SqlClient

Public Class UC_ST_TA_TAG_Spec
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Dim C As New Converter

#Region "Public Property"

    Public Enum PropertyItem
        'All = 0
        'PLANT = 1
        'AREA = 2
        ''LOOP_NO = 3
        'PROCESS = 4
        ''LINE_NO = 5
        ''SERVICE = 6
        'SIZE = 7
        ''SCHEDULE = 8
        'INITIAL_YEAR = 9
        ''LOCATION_FROM = 10
        ''LOCATION_TO = 11
        'NORMINAL_THICKNESS = 12
        'CALCULATED_THICKNESS = 13
        'INSULATION_ID = 14
        'INSULATION_THICKNESS = 15
        'DESIGN_PRESSURE = 16
        'OPERATING_PRESSURE = 17
        'DESIGN_TEMPERATURE = 18
        'OPERATING_TEMPERATURE = 19
        ''MATERIAL = 20
        ''PRESSURE_CODE = 21
        'CORROSION_ALLOWANCE = 22
        ''SERIAL_NO = 23
        ''PIPE_CLASS = 24
        ''P_ID_No = 25

        All = 0
        PLANT = 1
        AREA = 2
        PROCESS = 3
        SIZE = 4
        INITIAL_YEAR = 5
        NORMINAL_THICKNESS = 6
        CALCULATED_THICKNESS = 7

        INSULATION_THICKNESS = 8
        DESIGN_PRESSURE = 9
        OPERATING_PRESSURE = 10
        DESIGN_TEMPERATURE = 11
        OPERATING_TEMPERATURE = 12
        CORROSION_ALLOWANCE = 13
        TAG_NO = 14

        ROUTE_ID = 15
        EQUIPMENT_TYPE_ID = 16
        TAG_NAME = 17
        TAG_DESC = 18
    End Enum

    Public Enum AssignedMode
        Auto = 1
        Manual = 2
    End Enum

    Public Event PropertyChangedByUser(ByRef Sender As UC_ST_TA_TAG_Spec, ByVal Prop As PropertyItem)

    Public Property UNIQUE_ID As String
        Get
            Return Me.Attributes("UNIQUE_ID")
        End Get
        Set(value As String)
            Me.Attributes("UNIQUE_ID") = value
        End Set
    End Property

    Public Property TAG_ID As Integer
        Get
            Try
                Return Me.Attributes("TAG_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) Then
                Me.Attributes("TAG_ID") = value
            Else
                Me.Attributes("TAG_ID") = 0
            End If
        End Set
    End Property

    Public Property TAG_CODE As String
        Get
            Return lblCode.Text
        End Get
        Set(value As String)
            lblCode.Text = value
        End Set
    End Property
    Public Property TAG_No As String
        Get
            Return txt_TAG_No.Text
        End Get
        Set(value As String)
            txt_TAG_No.Text = value
        End Set
    End Property
    Public Property PLANT_ID As Integer
        Get
            Try
                Return ddl_PLANT.Items(ddl_PLANT.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                BL.BindDDlPlant(ddl_PLANT, value)
            Else
                BL.BindDDlPlant(ddl_PLANT)
            End If
            BL.BindDDlArea(PLANT_ID, ddl_AREA, AREA_ID)
        End Set
    End Property

    Public Property AREA_ID As Integer
        Get
            Try
                Return ddl_AREA.Items(ddl_AREA.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                If PLANT_ID = 0 Then
                    BL.BindDDlArea(ddl_AREA, value)
                Else
                    BL.BindDDlArea(PLANT_ID, ddl_AREA, value)
                End If
            Else
                If PLANT_ID = 0 Then
                    BL.BindDDlArea(ddl_AREA)
                Else
                    BL.BindDDlArea(PLANT_ID, ddl_AREA)
                End If
            End If
        End Set
    End Property

    Public ReadOnly Property AREA_CODE As String
        Get
            If AREA_ID = 0 Then Return ""
            Try
                Return ddl_AREA.Items(ddl_AREA.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    'Public Property LOOP_NO As String
    '    Get
    '        Return txt_LoopNo.Text
    '    End Get
    '    Set(value As String)
    '        If Not IsDBNull(value) AndAlso Not IsNothing(value) Then
    '            txt_LoopNo.Text = value
    '        Else
    '            txt_LoopNo.Text = ""
    '        End If
    '    End Set
    'End Property

    Public Property PROCESS_ID As Integer
        Get
            Try
                Return ddl_PROCESS.Items(ddl_PROCESS.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                BL.BindDDlProcess(ddl_PROCESS, value)
            Else
                BL.BindDDlProcess(ddl_PROCESS)
            End If
        End Set
    End Property

    Public ReadOnly Property PROCESS_CODE As String
        Get
            If PROCESS_ID = 0 Then Return ""
            Try
                Return ddl_PROCESS.Items(ddl_PROCESS.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property ROUTE_ID As Integer
        Get
            Try
                Return ddl_Route.Items(ddl_Route.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                BL.BindDDl_ST_Route(PLANT_ID, ddl_Route, value)
            Else
                BL.BindDDl_ST_Route(PLANT_ID, ddl_Route)
            End If
        End Set
    End Property



    Public Property EQUIPMENT_TYPE_ID As Integer
        Get
            Try
                Return ddl_Equipement_Type.Items(ddl_Equipement_Type.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then

                BL.BindDDlTagType("ST", ddl_Equipement_Type, value)
            Else
                BL.BindDDlTagType("ST", ddl_Equipement_Type)
            End If

        End Set
    End Property


    Public Property TAG_NAME As String
        Get
            Return txt_Tag_Name.Text
        End Get
        Set(value As String)
            txt_Tag_Name.Text = value
        End Set
    End Property

    Public Property TAG_DESC As String
        Get
            Return txt_Desc.Text
        End Get
        Set(value As String)
            txt_Desc.Text = value
        End Set
    End Property


    'Public Property SERVICE_ID As Integer
    '    Get
    '        Try
    '            Return ddl_SERVICE.Items(ddl_SERVICE.SelectedIndex).Value
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    '    Set(ByVal value As Integer)
    '        If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
    '            PIPE.BindDDl_Service(ddl_SERVICE, value)
    '        Else
    '            PIPE.BindDDl_Service(ddl_SERVICE)
    '        End If
    '    End Set
    'End Property

    'Public ReadOnly Property SERVICE_CODE As String
    '    Get
    '        If SERVICE_ID = 0 Then Return ""
    '        Try
    '            Return ddl_SERVICE.Items(ddl_SERVICE.SelectedIndex).Text
    '        Catch ex As Exception
    '            Return ""
    '        End Try
    '    End Get
    'End Property

    Public Property SIZE As Object
        Get
            If IsNumeric(txt_Size.Text) Then
                Return CDbl(txt_Size.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Size.Text = CDbl(value)
            Else
                txt_Size.Text = ""
            End If
        End Set
    End Property

    'Public Property SCHEDULE As String
    '    Get
    '        Return txt_Schedule.Text
    '    End Get
    '    Set(value As String)
    '        If Not IsDBNull(value) AndAlso Not IsNothing(value) Then
    '            txt_Schedule.Text = value
    '        Else
    '            txt_Schedule.Text = ""
    '        End If
    '    End Set
    'End Property

    Public Property INITIAL_YEAR As Object
        Get
            If IsNumeric(txt_Initial_Year.Text.Replace(",", "")) Then
                Return CInt(txt_Initial_Year.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Initial_Year.Text = CInt(value)
            Else
                txt_Initial_Year.Text = ""
            End If
        End Set
    End Property

    'Public Property LOCATION_FROM As String
    '    Get
    '        Return txt_Location_From.Text
    '    End Get
    '    Set(value As String)
    '        txt_Location_From.Text = value
    '    End Set
    'End Property

    'Public Property LOCATION_TO As String
    '    Get
    '        Return txt_Location_To.Text
    '    End Get
    '    Set(value As String)
    '        txt_Location_To.Text = value
    '    End Set
    'End Property

    Public Property NORMINAL_THICKNESS As Object
        Get
            If IsNumeric(txt_Norminal_Thickness.Text.Replace(",", "")) Then
                Return CDbl(txt_Norminal_Thickness.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Norminal_Thickness.Text = FormatNumber(value, 2)
            Else
                txt_Norminal_Thickness.Text = ""
            End If
        End Set
    End Property

    Public Property CALCULATED_THICKNESS As Object
        Get
            If IsNumeric(txt_Calculated_Thickness.Text.Replace(",", "")) Then
                Return CDbl(txt_Calculated_Thickness.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Calculated_Thickness.Text = FormatNumber(value, 2)
            Else
                txt_Calculated_Thickness.Text = ""
            End If
        End Set
    End Property


    Public Property INSULATION_THICKNESS As Object
        Get
            If IsNumeric(txt_Insulation_Thickness.Text.Replace(",", "")) Then
                Return CDbl(txt_Insulation_Thickness.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Insulation_Thickness.Text = FormatNumber(value, 2)
            Else
                txt_Insulation_Thickness.Text = ""
            End If
        End Set
    End Property


    Public Property PRESSURE_DESIGN As Object
        Get
            If IsNumeric(txt_Pressure_Design.Text.Replace(",", "")) Then
                Return Val(txt_Pressure_Design.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Pressure_Design.Text = FormatNumericText(value)
            Else
                txt_Pressure_Design.Text = ""
            End If
        End Set
    End Property

    Public Property PRESSURE_OPERATING As Object
        Get
            If IsNumeric(txt_Pressure_Operating.Text.Replace(",", "")) Then
                Return Val(txt_Pressure_Operating.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Pressure_Operating.Text = FormatNumericText(value)
            Else
                txt_Pressure_Operating.Text = ""
            End If
        End Set
    End Property

    Public Property TEMPERATURE_DESIGN As Object
        Get
            If IsNumeric(txt_Temperature_Design.Text.Replace(",", "")) Then
                Return Val(txt_Temperature_Design.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Temperature_Design.Text = FormatNumericText(value)
            Else
                txt_Temperature_Design.Text = ""
            End If
        End Set
    End Property

    Public Property TEMPERATURE_OPERATING As Object
        Get
            If IsNumeric(txt_Temperature_Operating.Text.Replace(",", "")) Then
                Return Val(txt_Temperature_Operating.Text.Replace(",", ""))
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                txt_Temperature_Operating.Text = FormatNumericText(value)
            Else
                txt_Temperature_Operating.Text = ""
            End If
        End Set
    End Property

    'Public Property MAT_CODE_ID As Integer
    '    Get
    '        Try
    '            Return ddl_MAT.Items(ddl_MAT.SelectedIndex).Value
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    '    Set(ByVal value As Integer)
    '        If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
    '            PIPE.BindDDl_Material_Code(ddl_MAT, value)
    '        Else
    '            PIPE.BindDDl_Material_Code(ddl_MAT)
    '        End If
    '    End Set
    'End Property

    'Public ReadOnly Property MAT_CODE As String
    '    Get
    '        Try
    '            Return PIPE.Get_Mat_Code_By_ID(MAT_CODE_ID)
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    'End Property

    'Public Property PRESSURE_ID As Integer
    '    Get
    '        Try
    '            Return ddl_PRESSURE.Items(ddl_PRESSURE.SelectedIndex).Value
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    '    Set(ByVal value As Integer)
    '        If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
    '            PIPE.BindDDl_Pressure_Code(ddl_PRESSURE, value)
    '        Else
    '            PIPE.BindDDl_Pressure_Code(ddl_PRESSURE)
    '        End If
    '    End Set
    'End Property

    'Public ReadOnly Property PRESSURE_CODE As String
    '    Get
    '        If PRESSURE_ID = 0 Then Return ""
    '        Try
    '            Return ddl_PRESSURE.Items(ddl_PRESSURE.SelectedIndex).Text
    '        Catch ex As Exception
    '            Return ""
    '        End Try
    '    End Get
    'End Property

    Public Property CA_ID As Integer
        Get
            Return ddl_CA.Items(ddl_CA.SelectedIndex).Value
        End Get
        Set(value As Integer)
            BL.BindDDl_CorrosionAllowance(ddl_CA, value)
        End Set
    End Property

    Public ReadOnly Property CA_Code As String
        Get
            Select Case CA_ID
                Case 1
                    Return "0"
                Case 2
                    Return "1"
                Case 3
                    Return "2"
                Case 4
                    Return "3"
                Case Else
                    Return ""
            End Select
        End Get
    End Property

    'Public Property SERIAL_NO As String
    '    Get
    '        Return txtSerial.Text
    '    End Get
    '    Set(value As String)
    '        txtSerial.Text = value
    '    End Set
    'End Property

    Public WriteOnly Property RaisePropertyChangedByUser As Boolean
        Set(value As Boolean)
            RaiseExternalEvent = value
            ddl_PLANT.AutoPostBack = value
            ddl_AREA.AutoPostBack = value
            ddl_PROCESS.AutoPostBack = value
            txt_Size.AutoPostBack = value
            txt_Initial_Year.AutoPostBack = value
            txt_Norminal_Thickness.AutoPostBack = value
            txt_Calculated_Thickness.AutoPostBack = value
            txt_Insulation_Thickness.AutoPostBack = value
            txt_Pressure_Design.AutoPostBack = value
            txt_Pressure_Operating.AutoPostBack = value
            txt_Temperature_Design.AutoPostBack = value
            txt_Temperature_Operating.AutoPostBack = value
            ddl_CA.AutoPostBack = value
            txt_TAG_No.AutoPostBack = value

            txt_Tag_Name.AutoPostBack = value
            ddl_Route.AutoPostBack = value
            ddl_Equipement_Type.AutoPostBack = value
            txt_Desc.AutoPostBack = value



        End Set
    End Property




    Public Property DisplayMainPointProperty() As Boolean
        Get
            Return trp1.Visible
        End Get
        Set(value As Boolean)
            trp1.Visible = value
            trp2.Visible = value
            trp3.Visible = value
            trp4.Visible = value
            trp5.Visible = value
            trp6.Visible = value

        End Set
    End Property

    Private Property RaiseExternalEvent As Boolean
        Get
            Try
                Return Me.ViewState("RaiseExternalEvent")
            Catch ex As Exception
                Return True
            End Try
        End Get
        Set(value As Boolean)
            Me.ViewState("RaiseExternalEvent") = value
        End Set
    End Property

#End Region

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ImplementJavascriptControl()
        Else
            If RaiseExternalEvent Then Me.RaisePropertyChangedByUser = True
        End If
    End Sub

    Private Sub ImplementJavascriptControl()
        'ImplementJavaOnlyNumberText(txt_LineNo, "Left")
        ImplementJavaNumericText(txt_Size, "Left")
        'ImplementJavaIntegerText(txt_Schedule, False,, "Left")
        ImplementJavaMoneyText(txt_Insulation_Thickness, , "Left")
        ImplementJavaMoneyText(txt_Norminal_Thickness, , "Left")
        ImplementJavaOnlyNumberText(txt_Initial_Year, "Left")
        ImplementJavaMoneyText(txt_Calculated_Thickness, , "Left")
        ImplementJavaNumericText(txt_Pressure_Design, "Left")
        ImplementJavaNumericText(txt_Pressure_Operating, "Left")
        ImplementJavaNumericText(txt_Temperature_Design, "Left")
        ImplementJavaNumericText(txt_Temperature_Operating, "Left")
    End Sub

    Public Sub ClearData()
        ClearPanelEdit()
    End Sub



    Private Sub UpdateTagCode()
        TAG_CODE = AREA_CODE & "-" & PROCESS_CODE & "-" & TAG_No
    End Sub


    Private Sub ClearPanelEdit()

        TAG_ID = 0
        PLANT_ID = 0
        AREA_ID = 0
        PROCESS_ID = 0
        SIZE = Nothing
        INITIAL_YEAR = Nothing
        NORMINAL_THICKNESS = Nothing
        CALCULATED_THICKNESS = Nothing
        INSULATION_THICKNESS = Nothing
        PRESSURE_DESIGN = Nothing
        PRESSURE_OPERATING = Nothing
        TEMPERATURE_DESIGN = Nothing
        TEMPERATURE_OPERATING = Nothing
        CA_ID = 0
        TAG_No = Nothing

        TAG_NAME = Nothing
        ROUTE_ID = 0
        EQUIPMENT_TYPE_ID = Nothing
        TAG_DESC = Nothing

        UpdateTagCode()

        SetPropertyEditable(PropertyItem.All, True)
    End Sub

    Public Sub SetPropertyEditable(ByVal PropertyItem As PropertyItem, ByVal Editable As Boolean)
        Select Case PropertyItem
            Case PropertyItem.All
                For i As Integer = 1 To 18
                    SetPropertyEditable(i, Editable)
                Next
            Case PropertyItem.PLANT
                ddl_PLANT.Enabled = Editable
            Case PropertyItem.AREA
                ddl_AREA.Enabled = Editable
            Case PropertyItem.PROCESS
                ddl_PROCESS.Enabled = Editable
            Case PropertyItem.SIZE
                txt_Size.ReadOnly = Not Editable
            Case PropertyItem.INITIAL_YEAR
                txt_Initial_Year.ReadOnly = Not Editable

            Case PropertyItem.NORMINAL_THICKNESS
                txt_Norminal_Thickness.ReadOnly = Not Editable
            Case CALCULATED_THICKNESS
                txt_Calculated_Thickness.ReadOnly = Not Editable

            Case PropertyItem.INSULATION_THICKNESS
                txt_Insulation_Thickness.ReadOnly = Not Editable
            Case PropertyItem.DESIGN_PRESSURE
                txt_Pressure_Design.ReadOnly = Not Editable
            Case PropertyItem.OPERATING_PRESSURE
                txt_Pressure_Operating.ReadOnly = Not Editable
            Case PropertyItem.DESIGN_TEMPERATURE
                txt_Temperature_Design.ReadOnly = Not Editable
            Case PropertyItem.OPERATING_TEMPERATURE
                txt_Temperature_Operating.ReadOnly = Not Editable
            Case PropertyItem.TAG_NO
                txt_TAG_No.ReadOnly = Not Editable

            Case PropertyItem.TAG_NAME
                txt_Tag_Name.ReadOnly = Not Editable
            Case PropertyItem.ROUTE_ID
                ddl_Route.Enabled = Editable
            Case PropertyItem.EQUIPMENT_TYPE_ID
                ddl_Equipement_Type.Enabled = Editable
            Case PropertyItem.TAG_DESC
                txt_Desc.ReadOnly = Not Editable

            Case PropertyItem.CORROSION_ALLOWANCE
                ddl_CA.Enabled = Editable
        End Select

    End Sub

    Public Function GetPropertyEditable(ByVal PropertyItem As PropertyItem) As Boolean

        Select Case PropertyItem
            Case PropertyItem.PLANT
                Return ddl_PLANT.Enabled
            Case PropertyItem.AREA
                Return ddl_AREA.Enabled
            Case PropertyItem.PROCESS
                Return ddl_PROCESS.Enabled
            Case PropertyItem.SIZE
                Return Not txt_Size.ReadOnly
            Case PropertyItem.INITIAL_YEAR
                Return Not txt_Initial_Year.ReadOnly
            Case PropertyItem.NORMINAL_THICKNESS
                Return Not txt_Norminal_Thickness.ReadOnly
            Case PropertyItem.CALCULATED_THICKNESS
                Return Not txt_Calculated_Thickness.ReadOnly

            Case PropertyItem.INSULATION_THICKNESS
                Return Not txt_Insulation_Thickness.ReadOnly
            Case PropertyItem.DESIGN_PRESSURE
                Return Not txt_Pressure_Design.ReadOnly
            Case PropertyItem.OPERATING_PRESSURE
                Return Not txt_Pressure_Operating.ReadOnly
            Case PropertyItem.DESIGN_TEMPERATURE
                Return Not txt_Temperature_Design.ReadOnly
            Case PropertyItem.OPERATING_TEMPERATURE
                Return Not txt_Temperature_Operating.ReadOnly
            Case PropertyItem.TAG_NO
                Return Not txt_TAG_No.ReadOnly
            Case PropertyItem.CORROSION_ALLOWANCE
                Return ddl_CA.Enabled

            Case PropertyItem.TAG_NAME
                Return Not txt_Tag_Name.ReadOnly
            Case PropertyItem.ROUTE_ID
                Return ddl_Route.Enabled
            Case PropertyItem.EQUIPMENT_TYPE_ID
                Return ddl_Equipement_Type.Enabled
            Case PropertyItem.TAG_DESC
                Return Not txt_Desc.ReadOnly

            Case Else ' PropertyItem.All
                For i As Integer = 1 To 18
                    If Not GetPropertyEditable(i) Then Return False
                Next
                Return True
        End Select
    End Function

    Public Sub BindData(ByVal TAG_ID As Integer)

        ClearPanelEdit()

        Dim SQL As String = ""
        SQL &= " Select   TAG_CODE,TAG_No,TAG_ID,TAG_Name,TAG_TYPE_Name,Active_Status " & vbLf
        SQL &= " ,AREA_CODE,PROC_CODE,TAG_TYPE_ID,PLANT_ID,ROUTE_ID,AREA_ID,Update_Time " & vbLf
        SQL &= " ,TAG_Order,TAG_Description,PROC_ID,To_Table " & vbLf
        SQL &= " ,Size,Location_From,Location_To,Initial_Year,Norminal_Thickness,Calculated_Thickness,IN_Thickness " & vbLf
        SQL &= " ,Pressure_Design,Pressure_Operating,Temperature_Design,Temperature_Operating,CA_ID " & vbLf
        SQL &= " From VW_ST_TA_TAG " & vbLf
        SQL &= " WHERE TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        Me.TAG_ID = DT.Rows(0).Item("TAG_ID")
        TAG_CODE = DT.Rows(0).Item("TAG_CODE")
        TAG_No = DT.Rows(0).Item("TAG_No")
        If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then PLANT_ID = DT.Rows(0).Item("PLANT_ID")
        If Not IsDBNull(DT.Rows(0).Item("AREA_ID")) Then AREA_ID = DT.Rows(0).Item("AREA_ID")
        If Not IsDBNull(DT.Rows(0).Item("PROC_ID")) Then PROCESS_ID = DT.Rows(0).Item("PROC_ID")

        If Not IsDBNull(DT.Rows(0).Item("ROUTE_ID")) Then ROUTE_ID = DT.Rows(0).Item("ROUTE_ID")
        If Not IsDBNull(DT.Rows(0).Item("TAG_TYPE_ID")) Then EQUIPMENT_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")
        TAG_NAME = DT.Rows(0).Item("TAG_Name")
        TAG_DESC = DT.Rows(0).Item("TAG_Description")

        If Not IsDBNull(DT.Rows(0).Item("Size")) Then SIZE = CDbl(DT.Rows(0).Item("Size"))
        If Not IsDBNull(DT.Rows(0).Item("Initial_Year")) Then INITIAL_YEAR = CDbl(DT.Rows(0).Item("Initial_Year"))
        If Not IsDBNull(DT.Rows(0).Item("Norminal_Thickness")) Then NORMINAL_THICKNESS = DT.Rows(0).Item("Norminal_Thickness")
        If Not IsDBNull(DT.Rows(0).Item("Calculated_Thickness")) Then CALCULATED_THICKNESS = DT.Rows(0).Item("Calculated_Thickness")
        If Not IsDBNull(DT.Rows(0).Item("IN_Thickness")) Then INSULATION_THICKNESS = CInt(DT.Rows(0).Item("IN_Thickness"))
        If Not IsDBNull(DT.Rows(0).Item("Pressure_Design")) Then PRESSURE_DESIGN = CInt(DT.Rows(0).Item("Pressure_Design"))
        If Not IsDBNull(DT.Rows(0).Item("Pressure_Operating")) Then PRESSURE_OPERATING = CInt(DT.Rows(0).Item("Pressure_Operating"))
        If Not IsDBNull(DT.Rows(0).Item("Temperature_Design")) Then TEMPERATURE_DESIGN = CInt(DT.Rows(0).Item("Temperature_Design"))
        If Not IsDBNull(DT.Rows(0).Item("Temperature_Operating")) Then TEMPERATURE_OPERATING = CInt(DT.Rows(0).Item("Temperature_Operating"))
        If Not IsDBNull(DT.Rows(0).Item("CA_ID")) Then CA_ID = DT.Rows(0).Item("CA_ID")


        UpdateTagCode()
        Me.TAG_ID = TAG_ID
        '--------------- Load Drawing --------------

    End Sub

    Public Function ValidateIncompleteMessage() As String
        If PLANT_ID = 0 Then
            Return "Please Select Plant"
        End If
        If AREA_ID = 0 Then
            Return "Please Select Area"
        End If
        If PROCESS_ID = 0 Then
            Return "Please Select Process"
        End If


        If IsNothing(TAG_No) Then
            Return "Please Insert Tag No."
        End If
        If ROUTE_ID = 0 Then
            Return "Please Select Route"
        End If
        If EQUIPMENT_TYPE_ID = 0 Then
            Return "Please Select Equipment-Type"
        End If

        If IsNothing(TAG_NAME) Then
            Return "Please Insert Tag Name"
        End If

        If IsNothing(SIZE) Then
            Return "Please Insert Size"
        End If
        If IsNothing(INITIAL_YEAR) Then
            Return "Please Insert Initial Year"
        End If
        If IsNothing(NORMINAL_THICKNESS) Then
            Return "Please Select Norminal Thickness"
        End If
        If IsNothing(CALCULATED_THICKNESS) Then
            Return "Please Select Calculated Thickness"
        End If

        If IsNothing(INSULATION_THICKNESS) Then
            Return "Please Insert Insulation Thickness"
        End If

        If CA_ID = 0 Then
            Return "Please Select Corrosion Allowance"
        End If

        Return ""
    End Function

    Public Function SaveData() As Integer '---------------- Save Only Tag--------------

        '--------------- Validate Required Field--------------
        Dim Msg As String = ValidateIncompleteMessage()
        If Msg <> "" Then
            Dim ER As New Exception(Msg)
            Throw (ER)
        End If

        '--------------- Check Duplicate--------------
        Dim SQL As String = "Select * FROM VW_ST_TA_TAG WHERE TAG_Code='" & TAG_CODE & "' AND TAG_ID<>" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Dim ER As New Exception("This Tag Code is already existed")
            Throw (ER)
        End If

        SQL = "SELECT * FROM MS_ST_TAG WHERE TAG_ID=" & TAG_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            TAG_ID = GetNewTagID()
            DR("TAG_ID") = TAG_ID
            DT.Rows.Add(DR)
        Else
            DR = DT.Rows(0)
        End If
        'DR("PLANT_ID") = PLANT_ID
        DR("AREA_ID") = AREA_ID
        DR("PROC_ID") = PROCESS_ID
        DR("Tag_No") = TAG_No
        DR("TAG_NAME") = TAG_NAME
        DR("ROUTE_ID") = ROUTE_ID
        DR("TAG_Description") = TAG_DESC
        DR("TAG_TYPE_ID") = EQUIPMENT_TYPE_ID

        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            Dim ER As New Exception(ex.Message)
            Throw (ER)
        End Try

        '------------- Save Point(Object Must specific POINT_ID First Before Saving) ---------------
        SQL = "SELECT * FROM MS_ST_SPEC WHERE TAG_ID=" & TAG_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR("TAG_ID") = TAG_ID
            DT.Rows.Add(DR)
        Else
            DR = DT.Rows(0)
        End If

        DR("Size") = SIZE

        DR("IN_Thickness") = INSULATION_THICKNESS
        If INITIAL_YEAR > 1900 And INITIAL_YEAR <= Now.Year Then
            DR("Initial_Year") = INITIAL_YEAR
        Else
            DR("Initial_Year") = DBNull.Value
        End If

        DR("Norminal_Thickness") = NORMINAL_THICKNESS
        DR("CA_ID") = CA_ID
        DR("Calculated_Thickness") = CALCULATED_THICKNESS
        If Not IsNothing(PRESSURE_DESIGN) Then
            DR("Pressure_Design") = PRESSURE_DESIGN
        Else
            DR("Pressure_Design") = DBNull.Value
        End If
        If Not IsNothing(PRESSURE_OPERATING) Then
            DR("Pressure_Operating") = PRESSURE_OPERATING
        Else
            DR("Pressure_Operating") = DBNull.Value
        End If
        If Not IsNothing(TEMPERATURE_DESIGN) Then
            DR("Temperature_Design") = TEMPERATURE_DESIGN
        Else
            DR("Temperature_Design") = DBNull.Value
        End If
        If Not IsNothing(TEMPERATURE_OPERATING) Then
            DR("Temperature_Operating") = TEMPERATURE_OPERATING
        Else
            DR("Temperature_Operating") = DBNull.Value
        End If
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        cmd = New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            Dim ER As New Exception(ex.Message)
            Throw (ER)
        End Try

        Return TAG_ID
    End Function

    Private Function GetNewTagID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(TAG_ID),0)+1 FROM MS_ST_TAG "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Protected Friend Sub Property_Changed(sender As Object, e As EventArgs)
        Select Case True
            Case Equals(sender, ddl_PLANT)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.PLANT)
            Case Equals(sender, ddl_AREA)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.AREA)
            Case Equals(sender, ddl_PROCESS)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.PROCESS)

            Case Equals(sender, txt_Size)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.SIZE)

            Case Equals(sender, txt_Initial_Year)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.INITIAL_YEAR)

            Case Equals(sender, txt_Norminal_Thickness)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.NORMINAL_THICKNESS)
            Case Equals(sender, txt_Calculated_Thickness)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.CALCULATED_THICKNESS)

            Case Equals(sender, txt_Insulation_Thickness)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.INSULATION_THICKNESS)
            Case Equals(sender, txt_Pressure_Design)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.DESIGN_PRESSURE)
            Case Equals(sender, txt_Pressure_Operating)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.OPERATING_PRESSURE)
            Case Equals(sender, txt_Temperature_Design)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.DESIGN_TEMPERATURE)
            Case Equals(sender, txt_Temperature_Operating)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.OPERATING_TEMPERATURE)
            Case Equals(sender, txt_TAG_No)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.TAG_NO)


            Case Equals(sender, txt_Tag_Name)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.TAG_NAME)
            Case Equals(sender, txt_Desc)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.TAG_DESC)
            Case Equals(sender, ddl_Route)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.ROUTE_ID)
            Case Equals(sender, ddl_Equipement_Type)
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.EQUIPMENT_TYPE_ID)

            Case Equals(sender, ddl_CA)
                UpdateTagCode()
                RaiseEvent PropertyChangedByUser(Me, PropertyItem.CORROSION_ALLOWANCE)

        End Select

    End Sub



    Private Sub ddl_PLANT_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_PLANT.SelectedIndexChanged
        If PLANT_ID = 0 Then
            BL.BindDDlArea(ddl_AREA, AREA_ID)
        Else
            BL.BindDDlArea(PLANT_ID, ddl_AREA, AREA_ID)
            BL.BindDDl_ST_Route(PLANT_ID, ddl_Route)
        End If

    End Sub

End Class