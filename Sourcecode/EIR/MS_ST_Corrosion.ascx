<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MS_ST_Corrosion.ascx.vb" Inherits="EIR.MS_ST_Corrosion1" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
 
<link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


  

    <!--CSS-->
    <link rel="stylesheet" href="resources/css/jquery-ui.css" type="text/css" media="screen" />
    <style type="text/css">
        .ChartHighligh {
            display: inline-block;
            float: left !important;
        }
    </style>


                


<asp:Repeater ID="rptLocationTag" runat="server">
    <ItemTemplate>
        <br />
        <h3><asp:Label ID="lblLocation_Name" runat="server"></asp:Label></h3>


                              
                       
                        <table  cellpadding="0" cellspacing="0"  class="propertyTable measurementGrid" >
                        <thead>
                        
                        <tr>                          
                            <th   style=" border :1px solid #CCCCCC; text-align:center;" >Position</th>
                            <asp:Repeater ID="rptItem_HeaderY" runat="server">
                                <ItemTemplate>
                                    <th   style="border :1px solid #CCCCCC; text-align:center;" ><asp:Label ID="lblHeader_Y" runat="server" Font-Size="12px" Font-Bold="false"></asp:Label></th>
                                </ItemTemplate>                            
                            </asp:Repeater>
                        </tr>
                        
                      </thead>
                    <asp:Repeater ID="rptPositionList" runat="server">
                       <ItemTemplate>
                            <tr>
                                <td><asp:Label ID="lblPosition_Name" runat="server"></asp:Label></td>
                                
                                <asp:Repeater ID="rptItemY" runat="server">
                                    <ItemTemplate>
                                        <td><asp:TextBox ID="txtItem_Header_Y" runat="server"></asp:TextBox></td>
                                    </ItemTemplate>                                    
                                </asp:Repeater>
                                
                            </tr>
                       </ItemTemplate>
                       <FooterTemplate>
                           </table>
                       </FooterTemplate>
                      </asp:Repeater>                   

<br />

                <table cellpadding="0" cellspacing="0" class="propertyTable">
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px;" colspan="6">
                           Corrosion Rate & Remaining Life Calculation by Require Thickness = 
                            <asp:DropDownList ID="ddl_Treq" runat="server" AutoPostBack="true" style="width:auto;">
                                <asp:ListItem Text="(Norminal Thickness) - (Corrosion Allowance)" Value="1"></asp:ListItem>
                                <asp:ListItem Text="(Calculated Thickness) + (Corrosion Allowance)" Value="2"></asp:ListItem>
                                <asp:ListItem Text="(Calculated Thickness)" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="propertyGroup" colspan="3">
                          <asp:RadioButton ID="rdoShort"  runat="server" GroupName="Term" AutoPostBack="true" Checked ="true"  /> Short Term
                        </td>
                        <td class="propertyGroup" colspan="3">
                           <asp:RadioButton ID="rdoLong"  runat="server" GroupName="Term" AutoPostBack="true" /> Long Term
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Last measurement thickness(Tlast)</b>:</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_LastThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                        <td style="background-color:#f5f5f5;"><b>Initial thickness(Tinitail)</b>:</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_LastThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Last measurement year</b></td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_LastYear" runat="server"  Font-Bold="true"></asp:Label><asp:Label ID="lbl_s_LastNo" runat="server"  Font-Bold="true" Visible ="false"  ></asp:Label></td>
                        <td style="text-align:center;">&nbsp;</td>
                        <td style="background-color:#f5f5f5;"><b>Initial year</b></td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_LastYear" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Time Between <b>Tlast and Tactual</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Year" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td style="background-color:#f5f5f5;">Time Between <b>Tinitail and Tactual</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Year" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Corrosion rate =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                        <td style="background-color:#f5f5f5;">Corrosion rate =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Minimum <b>Actual Thickness</b>(Tactual):</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Thick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                        <td style="background-color:#f5f5f5;">Minimum <b>Actual Thickness</b>(Tactual):</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Thick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="width:28%; background-color:#f5f5f5;"><b>Requied thickness</b> = </td>
                        <td style="text-align:center; width:15%; background-color:honeydew;"><asp:Label ID="lbl_S_ReqThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center; width:7%;">mm.</td>
                        <td style="width:28%; background-color:#f5f5f5;"><b>Requied thickness</b> = </td>
                        <td style="text-align:center; width:15%; background-color:honeydew;"><asp:Label ID="lbl_l_ReqThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="width:7%; text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Remaining Life</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td style="background-color:#f5f5f5;"><b>Remaining Life</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                </table>
<p align="right">
                <asp:Button ID="btnCalculate" runat="server" CommandName ="Calculate"  CssClass="button" Text="Calculate" Visible="false"  />


</p>


    </ItemTemplate>
</asp:Repeater>


<%--                <table cellpadding="0" cellspacing="0" class="propertyTable">
                    <tr>
                        <td class="propertyGroup" style="border:none; padding-top:20px;" colspan="6">
                           Corrosion Rate & Remaining Life Calculation  =
                            <asp:DropDownList ID="ddl_Treq" runat="server" AutoPostBack="true" style="width:auto;">
                                <asp:ListItem Text="(Norminal Thickness) - (Corrosion Allowance)" Value="1"></asp:ListItem>
                                <asp:ListItem Text="(Calculated Thickness) + (Corrosion Allowance)" Value="2"></asp:ListItem>
                                <asp:ListItem Text="(Calculated Thickness)" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="propertyGroup" colspan="3">
                          <asp:RadioButton ID="rdoShort"  runat="server" GroupName="Term" AutoPostBack="true" /> Short Term
                        </td>
                        <td class="propertyGroup" colspan="3">
                           <asp:RadioButton ID="rdoLong"  runat="server" GroupName="Term" AutoPostBack="true" /> Long Term
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Last measurement thickness(Tlast)</b>:</td>
                        <td style="text-align:center;"><asp:LinkButton ID="lbl_s_LastThick" runat="server"  Font-Bold="true"></asp:LinkButton></td>
                        <td style="text-align:center;">mm.</td>
                        <td style="background-color:#f5f5f5;"><b>Initial thickness(Tinitail)</b>:</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_LastThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Last measurement year</b></td>
                        <td style="text-align:center;"><asp:LinkButton ID="lbl_s_LastYear" runat="server"  Font-Bold="true"></asp:LinkButton></td>
                        <td style="text-align:center;">&nbsp;</td>
                        <td style="background-color:#f5f5f5;"><b>Initial year</b></td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_LastYear" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Time Between <b>Tlast and Tactual</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Year" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td style="background-color:#f5f5f5;">Time Between <b>Tinitail and Tactual</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Year" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Corrosion rate =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                        <td style="background-color:#f5f5f5;">Corrosion rate =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Rate" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm./year</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;">Minimum <b>Actual Thickness</b>(Tactual):</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Thick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                        <td style="background-color:#f5f5f5;">Minimum <b>Actual Thickness</b>(Tactual):</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Thick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="width:28%; background-color:#f5f5f5;"><b>Requied thickness</b> = </td>
                        <td style="text-align:center; width:15%; background-color:honeydew;"><asp:Label ID="lbl_S_ReqThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center; width:7%;">mm.</td>
                        <td style="width:28%; background-color:#f5f5f5;"><b>Requied thickness</b> = </td>
                        <td style="text-align:center; width:15%; background-color:honeydew;"><asp:Label ID="lbl_l_ReqThick" runat="server"  Font-Bold="true"></asp:Label></td>
                        <td style="width:7%; text-align:center;">mm.</td>
                    </tr>
                    <tr>
                        <td style="background-color:#f5f5f5;"><b>Remaining Life</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_s_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                        <td style="background-color:#f5f5f5;"><b>Remaining Life</b> =</td>
                        <td style="text-align:center;"><asp:Label ID="lbl_l_Remain" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="text-align:center;">year</td>
                    </tr>
                </table>--%>


 <asp:Panel ID="pnlbtn" runat ="server" Visible ="False" >
<p align="right">
                <asp:Button ID="btnCalculate" runat="server"  CssClass="button" Text="Calculation Corrosion Rate & Remaining"  />


</p>


</asp:Panel>