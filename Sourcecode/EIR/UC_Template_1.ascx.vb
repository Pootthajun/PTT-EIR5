﻿
Imports EIR

Public Class UC_Template_1
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Dim C As New Converter


    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return lblProperty.Attributes("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            If value <> UNIQUE_POPUP_ID Then
                IMAGE_1 = Nothing
            End If
            lblProperty.Attributes("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Public Property Sector_ID() As String
        Get
            Return lblProperty.Attributes("Sector_ID")
        End Get
        Set(ByVal value As String)

            lblProperty.Attributes("Sector_ID") = value
        End Set
    End Property

    Public Property Detail1 As String
        Get
            Return TextEditor1.HTML
        End Get
        Set(value As String)
            TextEditor1.HTML = value
        End Set
    End Property

#Region "Image Functional"

    Public Property IMAGE_1() As FileAttachment
        Get
            Try
                Dim Attachment As FileAttachment = Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1")
                If IsNothing(Attachment) Then
                    Attachment = New FileAttachment
                    Attachment.UNIQUE_ID = UNIQUE_POPUP_ID
                    Attachment.DocType = FileAttachment.AttachmentType.Cad_Drawing
                    Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1") = Attachment
                End If
                Attachment.Description = Detail1
                Return Attachment
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(ByVal value As FileAttachment)

            Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1") = value
            If Not IsNothing(value) Then
                ImgPreview1.ImageUrl = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
            Else
                ImgPreview1.ImageUrl = "resources/images/File_Sector.png"
            End If

        End Set
    End Property

    Private Sub ImgPreview1_Click(sender As Object, e As ImageClickEventArgs) Handles ImgPreview1.Click

        Dim Attachment As FileAttachment = IMAGE_1
        ShowDialogEditSVG(Me.Page, "PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1", "", btnRefreshImage1.ClientID, "")

    End Sub

    Protected Sub btnRefreshImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshImage1.Click
        Detail1 = CType(Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1"), FileAttachment).Description
        IMAGE_1 = IMAGE_1 '------- Refresh Image --------------
    End Sub

    Private Sub TextEditor1_onChange(ByRef Sender As UC_SummerNote) Handles TextEditor1.onChange

    End Sub

    Public Property Disabled() As Boolean
        Get
            Return Not ImgPreview1.Enabled
        End Get
        Set(ByVal value As Boolean)
            btnRefreshImage1.Visible = Not value
            ImgPreview1.Enabled = Not value
            TextEditor1.Enabled = Not value
            If Not value Then
                ImgPreview1.ToolTip = "Click to edit picture"
            Else
                ImgPreview1.ToolTip = "Click to view picture"
            End If

        End Set
    End Property

#End Region

End Class