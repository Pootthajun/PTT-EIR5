﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="Dashboard_Current_Status_Tag_LO.aspx.vb" Inherits="EIR.Dashboard_Current_Status_Tag_LO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" href="resources/css/DialogDashboard.css" type="text/css" media="screen"/>
</head>
<body>
    <form id="form1" runat="server">
<cc1:toolkitscriptmanager ID="ToolkitScriptManager1" runat="server" />
<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>

<div class="content-box">
				
				<div class="content-box-header">
					<h3 style="margin-top:0"><asp:Label ID="lblRptCode" runat="server"></asp:Label></h3>
				</div>
				
				<div class="content-box-content">
					<p style="font-weight:bold;">
					    <label class="column-left" style="width:100px;" >Report for: </label>
					    <asp:Label ID="lbl_Month" runat="server" Text="Month" CssClass="EditReportHeader" ></asp:Label> 
					    <asp:Label ID="lbl_Year" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>
					    (<asp:Label ID="lbl_Period" runat="server" Text="Period" CssClass="EditReportHeader" ForeColor="Gray"></asp:Label>)
					</p>
					
					<table style="width:100%; border-top:1px solid #cccccc;">
                        <tr>
                            <td style="vertical-align:top;">
                                <p style="font-weight:bold;">
				                    <label class="column-left" style="width:150px; color:Gray;">Equipement Name: </label>
				                    <asp:Label ID="lblEquipement" runat="server"></asp:Label>
				                </p>
				                <p style="font-weight:bold;">
				                    <label class="column-left" style="width:150px; color:Gray;" >Tag No: </label>
				                    <asp:Label ID="lblTagNo" runat="server"></asp:Label>
				                </p>
				                <p style="font-weight:bold;">
				                    <label class="column-left" style="width:150px; color:Gray;" >Area: </label>
				                    <asp:Label ID="lblArea" runat="server"></asp:Label>
				                </p>
				                <p style="font-weight:bold;">
				                    <label class="column-left" style="width:150px; color:Gray;" >Oil Type: </label>
				                    <asp:Label ID="lblOilType" runat="server"></asp:Label>
				                </p>
				                <p>
					                <label class="column-left" style="width:150px;" >Suggession: </label>
					                 <asp:TextBox Width="100%" CssClass="text-input" ID="txt_Suggess" TextMode="MultiLine" stye="height:50px;" MaxLength="1000" runat="server"></asp:TextBox>									
					                <asp:Button ID="btnUpdateDetail" runat="server" style="display:none;" />									                
				                </p>
                            </td>
                            <td style="text-align:right;">
                              <asp:Image ID="imgRadar" runat="server" />
                            </td>
                        </tr>
                        
                    </table>
				    <style type="text/css">
				        .tbEdit{
				            border:1px solid #CCCCCC;
				            text-align:center;
				            vertical-align:middle;
				            background-color:White;
				        }
				        .tbEdit_td
				        {
				            border:1px solid #CCCCCC;
				            text-align:center;
				            vertical-align:middle;
				        }
				        .tbEdit_th
				        {
				            border:1px solid #CCCCCC;
				            text-align:center;
				            font-weight:bold;
				        }
				    </style>	    
				    <table class="tbEdit" id="tb_Oil_Condition" runat="server">
				    <thead>
                        <tr>
                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                Oil Condition
                            </th>
                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                Unit
                            </th>
                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                Current Sample
                            </th>
                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                Sample Date
                            </th>
                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                Standard<br>Practice
                            </th>
                            <th style="background-color:Yellow; text-align:center;" class="tbEdit_th">
                                Alarm Limit
                            </th>                                            
                        </tr>
                    </thead>

                            <tr id="tr_TAN" runat="server">
                                <td style="font-weight:bold; width:100px;" class="tbEdit_td" id="td_TAN" runat="server">TAN</td>
                                <td style="width:100px;" class="tbEdit_td">mgKOH/g</td>
                                <td id="td_TAN_Value" runat="server" class="tbEdit_td"><asp:Label ID="lbl_TAN_Value" runat="server" style="border-width:0px; width:50px; text-align:center; background-color:Transparent;"></asp:Label></td>
                                <td id="td_TAN_Date" runat="server" class="tbEdit_td">
                                    <asp:Label ID="lbl_TAN_Date" runat="server" style="border-width:0px;  width:100px; text-align:center; background-color:Transparent;"></asp:Label>
                                </td>
                                <td style="width:100px;" class="tbEdit_td">ASTM E2412</td>
                                <td style="width:400px;" class="tbEdit_td"><img src="resources/images/LO/TAN_Table.png" alt="Rolls-Royce Gas generator" style="width:400px; cursor:pointer;" onclick="window.open(this.src);"/></td>
                            </tr>
                            <tr id="tr_OX" runat="server">
                                <td style="font-weight:bold; width:100px;" class="tbEdit_td" id="td_OX_Type" runat="server">                                                    
                                </td>
                                <td style="width:100px;"  class="tbEdit_td">
                                    <asp:Label ID="lbl_OX_UNIT" runat="server"></asp:Label>
                                </td>
                                <td class="tbEdit_td" id="td_OX_Value" runat="server" >
                                    <asp:Label ID="lbl_OX_Value" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;"></asp:Label>
                                 </td>
                                <td style="width:100px;" id="td_OX_Date" runat="server" class="tbEdit_td">
                                    <asp:Label ID="lbl_OX_Date" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;"></asp:Label>
                                </td>
                                <td style="width:100px;" class="tbEdit_td">ASTM E2412</td> 
                                <td style="width:400px;" class="tbEdit_td"><img src="resources/images/LO/Oxidation_Table.png" id="imgOxidation" runat="server" alt="Oxidation for Mineral Oil" style="cursor:pointer;" onclick="window.open(this.src);"/></td>                                               
                            </tr>
                            <tr id="tr_Varnish" runat="server">
                                <td style="font-weight:bold; width:100px;" class="tbEdit_td" id="td_Varnish" runat="server">Varnish</td>
                                <td style="width:100px;" class="tbEdit_td">-</td>
                                <td id="td_Varnish_Value" runat="server" class="tbEdit_td">
                                    <asp:Label ID="lbl_Varnish_Value" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;"></asp:Label>
                                </td>
                                <td style="width:100px;" id="td_Varnish_Date" runat="server" class="tbEdit_td">
                                    <asp:Label ID="lbl_Varnish_Date" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;"></asp:Label>
                                </td>
                                <td style="width:100px;" class="tbEdit_td">ASTM D7843</td>
                                <td style="width:400px;" class="tbEdit_td"><img src="resources/images/LO/Varnish_Table.png" alt="Varnish Criteria" style="width:400px; cursor:pointer;" onclick="window.open(this.src);"/></td>
                            </tr>                                            
                    </table>
                    <table class="tbEdit" id="tb_Contamination" runat="server">
				    <thead>
                        <tr>
                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                Contamination
                            </th>
                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                Unit
                            </th>
                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                Current Sample
                            </th>
                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                Sample Date
                            </th>
                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                Standard<br>Practice
                            </th>
                            <th style="background-color:#c6d9f1; text-align:center;" class="tbEdit_th">
                                Alarm Limit
                            </th>                                            
                        </tr>
                    </thead>
                            <tr id="tr_PART_COUNT" runat="server">
                                <td style="font-weight:bold; width:100px;" class="tbEdit_td" id="td_PART_COUNT" runat="server">Particles Count</td>
                                <td style="width:100px;" class="tbEdit_td">Pcs.</td>
                                <td id="td_PART_COUNT_Value" runat="server" class="tbEdit_td">
                                    <asp:Label ID="lblPART_COUNT_Value" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;"></asp:Label>
                                </td>
                                <td style="width:100px;" id="td_PART_COUNT_Date" runat="server" class="tbEdit_td">
                                    <asp:Label ID="lbl_PART_COUNT_Date" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;"></asp:Label>
                                </td>
                                <td style="width:100px;" class="tbEdit_td">NAS 1638 / <br>ISO 4406</td>
                                <td style="width:400px;" class="tbEdit_td"><img src="resources/images/LO/Particle_Table.png" alt="Maximum Number of Particles Per 100 Milimeters (Counts per 100 ml)" style="width:400px; cursor:pointer;" onclick="window.open(this.src);"/></td>
                            </tr>     
                            <tr id="tr_Water" runat="server">
                                <td style="font-weight:bold; width:100px;" class="tbEdit_td" id="td_Water" runat="server">Water</td>
                                <td style="width:100px;" class="tbEdit_td">
                                    ppm
                                </td>
                                <td id="td_Water_Value" runat="server" class="tbEdit_td">
                                    <asp:Label ID="lbl_Water_Value" runat="server" style="border-width:0px; width:100px; text-align:center; background-color:Transparent;"></asp:Label></td>
                                <td style="width:100px;" id="td_Water_Date" runat="server" class="tbEdit_td">
                                    <asp:Label ID="lbl_Water_Date" runat="server" style="border-width:0px; width:100px; text-align:center;"></asp:Label>
                                </td>
                                <td style="width:100px;" class="tbEdit_td">ASTM E2412</td>
                                <td class="tbEdit_td">
                                    <img src="resources/images/LO/Water_Mineral_Table.png" id="imgWater1" runat="server" alt="Water for Mineral Oil" style="cursor:pointer;" onclick="window.open(this.src);"/>
                                    <img src="resources/images/LO/Water_Synthetic_Table.png" id="imgWater2" runat="server" alt="Water for Synthetic Oil" style="cursor:pointer;" onclick="window.open(this.src);"/>
                                </td>                                                 
                            </tr>
                    </table>
				</div>         
	              
</div>
				


</ContentTemplate>
</asp:UpdatePanel>
    </form>
</body>
</html>