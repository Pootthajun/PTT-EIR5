﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Top10_Problem.aspx.vb" Inherits="EIR.Dashboard_Top10_Problem" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>

<%@ Register src="UC_Dashboard_Top10_Problem.ascx" tagname="UC_Dashboard_Top10_Problem" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>


<!-- Page Head -->
<h2>Top 10 Problems</h2>

<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
  <tr>
	<td>
	    <h3>
        <asp:Label ID="Label3" runat="server" ForeColor="#009933" Text="From"></asp:Label>
        &nbsp;<asp:DropDownList ID="ddl_Month_F" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        &nbsp;<asp:DropDownList ID="ddl_Year_F" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        &nbsp;<asp:Label ID="Label4" runat="server" ForeColor="#009933" Text="To"></asp:Label>
&nbsp;<asp:DropDownList ID="ddl_Month_T" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        &nbsp;<asp:DropDownList ID="ddl_Year_T" runat="server" AutoPostBack="true">
        </asp:DropDownList>
        &nbsp;<asp:Label ID="Label2" runat="server" ForeColor="#009933" 
            Text="Equipment Category"></asp:Label>
        &nbsp;
        <asp:DropDownList ID="ddl_Equipment" runat="server" AutoPostBack="true">
            <asp:ListItem Text="Stationary & Rotating" Value="99"></asp:ListItem>
            <asp:ListItem Text="Stationary" Value="1"></asp:ListItem>
            <asp:ListItem Text="Rotating" Value="3"></asp:ListItem>
        </asp:DropDownList>
        </h3>         
     </td>
  </tr>
  <tr>
	<td>
	    <asp:Panel ID="pnlDashboard" runat="server">
            <uc1:UC_Dashboard_Top10_Problem ID="Dashboard_Top10_Problem1" runat="server" />
        </asp:Panel>
    </td>
  </tr>
</table>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>