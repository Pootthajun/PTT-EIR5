﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Detail_Plant_LO
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass
    Dim CV As New Converter

    Public ReadOnly Property PLANT_ID As Integer
        Get
            Try
                If IsNumeric(Request.QueryString("PLANT_ID")) Then
                    Return CInt(Request.QueryString("PLANT_ID"))
                Else
                    Return 0
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dim SQL As String = ""
            Dim DT_PLANT As New DataTable
            Dim DA As New SqlDataAdapter
            SQL = "select PLANT_Name from MS_Plant where PLANT_ID = " & PLANT_ID
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DA.Fill(DT_PLANT)
            If DT_PLANT.Rows.Count > 0 Then
                lblPlantName.Text = DT_PLANT.Rows(0).Item("PLANT_Name").ToString
            End If
            lblBack.Text = "Back to see on this plant " & lblPlantName.Text
            lblMonth.Text = Request.QueryString("MONTH")
            lblYear.Text = Request.QueryString("YEAR")
            Select Case Request.QueryString("REPORT")
                Case EIR_BL.ReportName_Problem.NEW_PROBLEM
                    Select Case Request.QueryString("BACK_EQUIPMENT")
                        Case EIR_BL.Report_Type.All
                            lblBack.PostBackUrl = "Dashboard_Detail_Plant_ALL.aspx?PLANT_ID=" & PLANT_ID & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("BACK_EQUIPMENT") & "&REPORT=" & EIR_BL.ReportName_Problem.NEW_PROBLEM & "&MONTH=" & Request.QueryString("MONTH") & "&YEAR=" & Request.QueryString("YEAR")
                        Case Else
                            lblBack.PostBackUrl = "Dashboard_New_Problem_Report_Plant.aspx?PLANT_ID=" & PLANT_ID & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("EQUIPMENT")
                    End Select
                    lblHeader.Text = "New Problem Occurred"
                    lblReport.Text = EIR_BL.Dashboard.New_Problem_Occurred
                    lblHead.Text = "New Problem  for  " & Dashboard.FindEquipmentName(EIR_BL.Report_Type.Lube_Oil_Report) & "  on  " & lblPlantName.Text & "  at  " & CV.ToMonthNameEN(lblMonth.Text) & "  " & lblYear.Text
                    BindData(PLANT_ID, Request.QueryString("MONTH"), Request.QueryString("YEAR"), Request.QueryString("EQUIPMENT"))
                Case EIR_BL.ReportName_Problem.TOTAL_PROBLEM
                    Select Case Request.QueryString("BACK_EQUIPMENT")
                        Case EIR_BL.Report_Type.All
                            lblBack.PostBackUrl = "Dashboard_Detail_Plant_ALL.aspx?PLANT_ID=" & PLANT_ID & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("BACK_EQUIPMENT") & "&REPORT=" & EIR_BL.ReportName_Problem.TOTAL_PROBLEM & "&MONTH=" & Request.QueryString("MONTH") & "&YEAR=" & Request.QueryString("YEAR")
                        Case Else
                            lblBack.PostBackUrl = "Dashboard_Total_Problem_Report_Plant.aspx?PLANT_ID=" & PLANT_ID & "&MONTH_F=" & Request.QueryString("MONTH_F") & "&MONTH_T=" & Request.QueryString("MONTH_T") & "&YEAR_F=" & Request.QueryString("YEAR_F") & "&YEAR_T=" & Request.QueryString("YEAR_T") & "&EQUIPMENT=" & Request.QueryString("EQUIPMENT")
                    End Select
                    lblHeader.Text = "Total Problem by Month"
                    lblReport.Text = EIR_BL.Dashboard.Total_Problem_by_month
                    lblHead.Text = "Total Problem  for  " & Dashboard.FindEquipmentName(EIR_BL.Report_Type.Lube_Oil_Report) & "  on  " & lblPlantName.Text & "  at  " & CV.ToMonthNameEN(lblMonth.Text) & "  " & lblYear.Text
                    BindData(PLANT_ID, Request.QueryString("MONTH"), Request.QueryString("YEAR"), Request.QueryString("EQUIPMENT"))
            End Select
        End If

    End Sub

    Public Sub BindData(ByVal Plant_ID As Integer, ByVal Month As Integer, ByVal Year As Integer, ByVal Equipment As Integer)
        Dim SQL As String = ""
        Dim DT_Dashboard As New DataTable
        Dim DA As New SqlDataAdapter

        If Year > 2500 Then
            Year = Year - 543
        End If

        Dim DateFrom As Date = CV.StringToDate(Year.ToString & "-" & Month.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        Dim DateTo As Date = CV.StringToDate(Year.ToString & "-" & Month.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        DateTo = DateTo.AddMonths(1).AddDays(-1)
        Select Case lblReport.Text
            Case EIR_BL.Dashboard.New_Problem_Occurred
                DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate, DashboardClass.REPORT_ISSUE.New_Problem)
            Case EIR_BL.Dashboard.Total_Problem_by_month
                DT_Dashboard = Dashboard.GetDashboardData(Equipment, Plant_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate)
                DT_Dashboard.DefaultView.RowFilter = "ICLS_ID > 0"
                DT_Dashboard = DT_Dashboard.DefaultView.ToTable
        End Select
        DT_Dashboard.DefaultView.Sort = "TAG_CODE"
        DT_Dashboard = DT_Dashboard.DefaultView.ToTable

        Dim Col() As String = {"TAG_ID", "TAG_CODE", "RPT_YEAR", "RPT_NO"}
        Dim TAG As DataTable = DT_Dashboard.DefaultView.ToTable(True, Col)

        TAG.Columns.Add("TAN_Value")
        TAG.Columns.Add("Ox_Value")
        TAG.Columns.Add("VANISH_Value")
        TAG.Columns.Add("WATER_Value")

        For i As Integer = 0 To TAG.Rows.Count - 1
            TAG.Rows(i).Item("TAN_Value") = DT_Dashboard.Compute("COUNT(TAG_ID)", "TAG_CODE='" & TAG.Rows(i).Item("TAG_CODE") & "' AND INSPECTION='TAN'")
            TAG.Rows(i).Item("Ox_Value") = DT_Dashboard.Compute("COUNT(TAG_ID)", "TAG_CODE='" & TAG.Rows(i).Item("TAG_CODE") & "' AND INSPECTION='OX'")
            TAG.Rows(i).Item("VANISH_Value") = DT_Dashboard.Compute("COUNT(TAG_ID)", "TAG_CODE='" & TAG.Rows(i).Item("TAG_CODE") & "' AND INSPECTION='VARNISH'")
            TAG.Rows(i).Item("WATER_Value") = DT_Dashboard.Compute("COUNT(TAG_ID)", "TAG_CODE='" & TAG.Rows(i).Item("TAG_CODE") & "' AND INSPECTION='WATER'")
        Next

        rptData.DataSource = TAG
        rptData.DataBind()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblTAN As HtmlAnchor = e.Item.FindControl("lblTAN")
        Dim lblOx As HtmlAnchor = e.Item.FindControl("lblOx")
        Dim lblVarnish As HtmlAnchor = e.Item.FindControl("lblVarnish")
        Dim lblWater As HtmlAnchor = e.Item.FindControl("lblWater")

        lblTag.Text = e.Item.DataItem("TAG_CODE").ToString
        If e.Item.DataItem("TAN_Value").ToString <> "" Then
            If e.Item.DataItem("TAN_Value") = 0 Then
                lblTAN.InnerHtml = "-"
            Else
                lblTAN.InnerHtml = "Abnormal"
                lblTAN.Style("color") = "Red"
                lblTAN.Style("cursor") = "pointer"
                lblTAN.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_LO.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
            End If
        End If
        If e.Item.DataItem("Ox_Value").ToString <> "" Then
            If e.Item.DataItem("Ox_Value") = 0 Then
                lblOx.InnerHtml = "-"
            Else
                lblOx.InnerHtml = "Abnormal"
                lblOx.Style("color") = "Red"
                lblOx.Style("cursor") = "pointer"
                lblOx.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_LO.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
            End If
        End If
        If e.Item.DataItem("VANISH_Value").ToString <> "" Then
            If e.Item.DataItem("VANISH_Value") = 0 Then
                lblVarnish.InnerHtml = "-"
            Else
                lblVarnish.InnerHtml = "Abnormal"
                lblVarnish.Style("color") = "Red"
                lblVarnish.Style("cursor") = "pointer"
                lblVarnish.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_LO.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
            End If
        End If
        If e.Item.DataItem("WATER_Value").ToString <> "" Then
            If e.Item.DataItem("WATER_Value") = 0 Then
                lblWater.InnerHtml = "-"
            Else
                lblWater.InnerHtml = "Abnormal"
                lblWater.Style("color") = "Red"
                lblWater.Style("cursor") = "pointer"
                lblWater.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_LO.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
            End If
        End If
    End Sub

End Class