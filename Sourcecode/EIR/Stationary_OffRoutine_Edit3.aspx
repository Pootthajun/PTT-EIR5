﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Stationary_OffRoutine_Edit3.aspx.vb" Inherits="EIR.Stationary_OffRoutine_Edit3" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register src="GL_DialogInputValue.ascx" tagname="GL_DialogInputValue" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     
<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>
	    
			<h2>Edit Stationary Off-Routine Report</h2>
			<asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" style="visibility:hidden;" />
            <div class="clear"></div> <!-- End .clear -->
		
		
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server">Tag Status</asp:LinkButton></li>
                        <li><asp:LinkButton id="HTabSummary" runat="server" CssClass="default-tab current">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p style="font-weight:bold;">
								<label class="column-left" style="width:120px;" >Report for: </label>
								<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
								<asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Tag <asp:Label ID="lbl_TAG" runat="server" Text="Tag" CssClass="EditReportHeader"></asp:Label>
								</p>
								
								<ul class="shortcut-buttons-set">
							     
								  <li>
								    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" width="48" height="48" /><br />
									    Reset this tab
									    </span>
								    </asp:LinkButton>
								  </li>
								  <li>
								    <asp:LinkButton ID="lnkSave" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/save_48.png" alt="icon" width="48" height="48" /><br />
								         Save changed
								        </span>
								    </asp:LinkButton>
								  </li>
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								  </li>
					        </ul>
						
						        <div class="clear"></div><!-- End .clear -->
                                <table>
                                  <thead>
                                    <tr>
                                      <th colspan="6" style="height:24px;">Condition Summary For This Tag </th>
                                    </tr>
                                    <tr>
                                      
                                      <th style=" text-align:center;">Previous Problems</th>
                                      <th style=" text-align:center;">Problems Fixed</th>
                                      <th style=" text-align:center;">New Problems </th>
                                      <th style=" text-align:center;">Remain Problems </th>
                                    </tr>
                                  
								  </thead>
                                  <tr>
                                    <td class="LevelClassA" style="text-align:center;"><asp:Label ID="lblOldProblem" runat="server" Font-Bold="True" Font-Size="16px"></asp:Label></td>
                                    <td class="LevelNormal" style="text-align:center;"><asp:Label ID="lblFixed" runat="server" Font-Bold="True" Font-Size="16px"></asp:Label></td>
                                    <td class="LevelClassA" style="text-align:center;"><asp:Label ID="lblNewProblem" runat="server" Font-Bold="True" Font-Size="16px"></asp:Label></td>
                                    <td class="LevelClassA" style="text-align:center;"><asp:Label ID="lblSumProblem" runat="server" Font-Bold="True" Font-Size="16px"></asp:Label></td>
                                  </tr>
                                </table>

                                 <div class="clear"></div><!-- End .clear -->
                                <p>&nbsp;</p>

                                  <table>
                                    <thead>
                                         <tr>
                                          <th colspan="5" style="height:24px;">Problem Updated In This Report and Responsible</th>
                                        </tr>
                                        <tr>
                                          <th>Problems</th>
                                          <th>Class</th>
                                          <th>Detail</th>
                                          <th>Comment</th>
                                          <th>Resp.</th>
                                         </tr>
                                    </thead>
                                    <asp:Repeater ID="rptSummary" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                              <td style="vertical-align:top; border-bottom:1px solid #EFEFEF;"><asp:label ID="lblProblem" runat="server"></asp:label></td>
                                              <td style="vertical-align:top; border-bottom:1px solid #EFEFEF;"><asp:label ID="lblClass" runat="server"></asp:label></td>
                                              <td style="vertical-align:top; border-bottom:1px solid #EFEFEF;"><asp:label ID="lblDetail" runat="server"></asp:label></td>
                                              <td style="vertical-align:top; border-bottom:1px solid #EFEFEF;"><asp:label ID="lblComment" BorderStyle="None" runat="server"></asp:label></td>
                                              <td style="width:50px; vertical-align:top; border-bottom:1px solid #EFEFEF;"><asp:TextBox Width="40px" ID="txtResponse" MaxLength="50" runat="server" style="text-align:center;"></asp:TextBox></td>
                                         </tr>
                                    </ItemTemplate>
                                    </asp:Repeater>                                     
                                </table>
								<p>&nbsp;</p>
								
								<p>
									<label style="width:300px;" >Conclusion and Recommendation: </label>
									<asp:TextBox  TextMode="MultiLine" Width="800px" Height="80px" CssClass="text-input" ID="txt_RPT_Result" MaxLength="1000" runat="server"></asp:TextBox>
								</p>
                                  
							    <div class="clear"></div><!-- End .clear -->
									
									<p align="right">
										<asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
										<asp:Button ID="btn_Save" runat="server" CssClass="button" Text="Save Only" />
										<asp:Button ID="btn_Posted" runat="server" CssClass="button" Text="Post approved" />
									</p>
								
							</fieldset>
				    </div>
				  <!-- End #tabDetail -->        
	          
		       	 <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
		         
	              <uc1:GL_DialogInputValue ID="DialogInput" runat="server" Visible="false" />

			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			

</ContentTemplate>
</asp:UpdatePanel>  	
</asp:Content>
