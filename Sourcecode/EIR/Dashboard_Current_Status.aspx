﻿<%@ Page Language="vb" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeBehind="Dashboard_Current_Status.aspx.vb" Inherits="EIR.Dashboard_Current_Status" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>


			<!-- Page Head -->
			<h2><asp:Label ID="lblUserFullname" runat="server"></asp:Label>, welcome to EIR' 
                dashboard</h2>
			<p class="page-intro">What would you like to do?<!-- End .shortcut-buttons-set --></p>
			
        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
                <tr>
                    <td colspan="2"><h3 style="color: #0066CC;">
                        Equipment Category 
                        <asp:DropDownList ID="ddl_Equipment" runat="server" AutoPostBack="true"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="vertical-align:top;">
                        <asp:Repeater ID="rptChart" runat="server">
                            <ItemTemplate>
                              <asp:Chart ID="ChartPlant" runat="server" Height="150px" Width="150px" CssClass="ChartHighligh">
                                    <titles>
                                        <asp:Title Font="Tahoma, 9.75pt, style=Bold" Name="Title1" Text="GSP#X" />
                                        <asp:Title Docking="Bottom" Name="Title2" Text="xxx Tag" />
                                      
                                    </titles>
                                    <series>
                                        <asp:Series ChartType="Pie" Name="Series1" Palette="Bright" ShadowColor="" 
                                            XValueType="String">
                                            <points>
                                                <asp:DataPoint AxisLabel="" BackGradientStyle="None" BackSecondaryColor="" 
                                                    Color="Green" Font="Tahoma, 8.25pt" Label="#VAL" LegendText="Normal" 
                                                    MapAreaAttributes="" ToolTip="" Url="" YValues="0" />
                                                
                                            </points>
                                            <emptypointstyle isvisibleinlegend="False" />
                                        </asp:Series>
                                    </series>
                                    <chartareas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </chartareas>
                                </asp:Chart>
                            </ItemTemplate>
                            <FooterTemplate>
                            
                            </FooterTemplate>
                        </asp:Repeater>
                                
                                
                        <center ><img src="resources/images/LegendClass.png" alt="" style="margin-top:30px;" /></center>
                      </td>
                    <td style="vertical-align:top;" width="350">
                    <table border="0" cellpadding="0" cellspacing="0" width="350">
                        <tr>
                            <td style="text-align:center; background-color:#003366; color:White">
                              Plant</td>
                            <td style="text-align:center; background-color:#003366; color:White">
                              Normal</td>
                            <td style="text-align:center; background-color:#003366; color:White">
                              ClassC</td>
                            <td style="text-align:center; background-color:#003366; color:White">
                              ClassB</td>
                            <td style="text-align:center; background-color:#003366; color:White">
                              ClassA</td>
                            <td style="text-align:center; background-color:#003366; color:White">
                              Total</td>
                            </tr>
                            <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                            <tr id="tbTag" runat="server" style="cursor:pointer; border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                              <td style="text-align:center;">
                                  <asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                              <td style="text-align:center;">
                                  <asp:Label ID="lblNormal" runat="server" CssClass="TextNormal"></asp:Label></td>
                              <td style="text-align:center;">
                                  <asp:Label ID="lblClassC" runat="server" CssClass="TextClassC"></asp:Label></td>
                              <td style="text-align:center;">
                                  <asp:Label ID="lblClassB" runat="server" CssClass="TextClassB"></asp:Label></td>
                              <td style="text-align:center;">
                                  <asp:Label ID="lblClassA" runat="server" CssClass="TextClassA"></asp:Label></td>
                              <td style="text-align:center;">
                                  <asp:Label ID="lblTotal" runat="server" Font-Bold="true"></asp:Label></td>
                            </tr>
                            </ItemTemplate>
                            </asp:Repeater>
                      
                    </table>
                    </td>
                </tr>
			</table>


</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

