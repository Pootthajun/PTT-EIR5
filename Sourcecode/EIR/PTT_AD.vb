﻿Imports System.Data
Imports System.DirectoryServices
Imports System.Data.SqlClient

Public Class PTT_AD

    'Public Function AuthenticateUser(ByVal Domain As String, ByVal Username As String, ByVal Password As String, ByVal LDAP_Path As String) As DomainUserResult
    '    Dim Profile As New DomainUserResult
    '    'Profile.Domain = Domain
    '    Profile.LDAP = LDAP_Path
    '    Profile.Username = Username
    '    Profile.Password = Password
    '    Profile.IsEnabled = False
    '    Profile.Errmsg = ""

    '    'On Error Resume Next
    '    Dim entry As DirectoryEntry
    '    Try
    '        entry = New DirectoryEntry(LDAP_Path, Username, Password, AuthenticationTypes.Secure)
    '    Catch ex As Exception
    '        Profile.Errmsg = "DirectoryEntry : " & ex.Message
    '        Return Profile
    '    End Try

    '    Dim search As DirectorySearcher
    '    Try
    '        search = New DirectorySearcher(entry)
    '    Catch ex As Exception
    '        Profile.Errmsg = "DirectorySearcher : " & ex.Message
    '        Return Profile
    '    End Try

    '    search.Filter = "(sAMAccountName=" & Username & ")"
    '    search.PropertiesToLoad.Add("cn")
    '    Dim result As SearchResult
    '    Try
    '        result = search.FindOne()
    '    Catch ex As Exception
    '        Profile.Errmsg = "SearchResult : " & ex.Message
    '        Return Profile
    '    End Try
    '    If result Is Nothing Or Err.Number <> 0 Then
    '        Profile.Errmsg = "SearchResult : User's not found"
    '        Return Profile
    '    End If

    '    Try
    '        Dim DirEntry As DirectoryEntry = result.GetDirectoryEntry()

    '        Profile.Title = DirEntry.Properties("title")(0).ToString
    '        Profile.FirstName = DirEntry.Properties("givenName")(0).ToString
    '        Profile.Initials = DirEntry.Properties("initials")(0).ToString
    '        Profile.LastName = DirEntry.Properties("sn")(0).ToString
    '        Profile.DisplayName = DirEntry.Properties("displayName")(0).ToString
    '        Profile.Mail = DirEntry.Properties("mail")(0).ToString

    '        Profile.Department = DirEntry.Properties("department")(0).ToString
    '        Profile.Company = DirEntry.Properties("company")(0).ToString
    '        Profile.Manager = DirEntry.Properties("manager")(0).ToString
    '        Profile.Mobile = DirEntry.Properties("mobile")(0).ToString
    '        Profile.User_Profile_path = DirEntry.Properties("profilePath")(0).ToString

    '        Profile.IsEnabled = Not DirEntry.Properties("userAccountControl")(0)
    '        If Not Profile.IsEnabled Then
    '            Profile.Errmsg = "LDAP's user is disabled"
    '        End If
    '    Catch ex As Exception
    '        Profile.Errmsg = ex.Message
    '    End Try

    '    Return Profile
    'End Function

    Public Function AuthenticateUser(ByVal LDAP_Path As String, ByVal Username As String, ByVal Password As String) As DomainUserResult

        Dim Profile As New DomainUserResult
        'Profile.Domain = Domain
        Profile.LDAP = LDAP_Path
        Profile.Username = Username
        Profile.Password = Password
        Profile.IsEnabled = False
        Profile.Errmsg = ""

        'On Error Resume Next
        Dim entry As DirectoryEntry
        Try
            entry = New DirectoryEntry(LDAP_Path, Username, Password, AuthenticationTypes.Secure)
        Catch ex As Exception
            Profile.Errmsg = "DirectoryEntry : " & ex.Message
            Return Profile
        End Try

        Dim search As DirectorySearcher
        Try
            search = New DirectorySearcher(entry)
        Catch ex As Exception
            Profile.Errmsg = "DirectorySearcher : " & ex.Message
            Return Profile
        End Try

        search.Filter = "(sAMAccountName=" & Username & ")"
        search.PropertiesToLoad.Add("cn")
        Dim result As SearchResult
        Try
            result = search.FindOne()
        Catch ex As Exception
            Profile.Errmsg = "SearchResult : " & ex.Message
            Return Profile
        End Try
        If result Is Nothing Or Err.Number <> 0 Then
            Profile.Errmsg = "SearchResult : User's not found"
            Return Profile
        End If

        Try

            Dim DirEntry As DirectoryEntry = result.GetDirectoryEntry()

            '-------------- List All Property--------------
            'Dim Prop As String() = PrintDirectoryEntryProperties(DirEntry)
            'For i As Integer = 0 To Prop.Count - 1
            '    Profile.Errmsg &= Prop(i) & "<br>"
            'Next
            'Return Profile

            If DirEntry.Properties("personalTitle").Count > 0 Then Profile.Title = DirEntry.Properties("personalTitle")(0).ToString
            If DirEntry.Properties("givenName").Count > 0 Then Profile.FirstName = DirEntry.Properties("givenName")(0).ToString
            If DirEntry.Properties("initials").Count > 0 Then Profile.Initials = DirEntry.Properties("initials")(0).ToString
            If DirEntry.Properties("sn").Count > 0 Then Profile.LastName = DirEntry.Properties("sn")(0).ToString
            If DirEntry.Properties("displayName").Count > 0 Then Profile.DisplayName = DirEntry.Properties("displayName")(0).ToString
            If DirEntry.Properties("mail").Count > 0 Then Profile.Mail = DirEntry.Properties("mail")(0).ToString

            If DirEntry.Properties("department").Count > 0 Then Profile.Department = DirEntry.Properties("department")(0).ToString
            If DirEntry.Properties("company").Count > 0 Then Profile.Company = DirEntry.Properties("company")(0).ToString
            'Profile.Manager = DirEntry.Properties("manager")(0).ToString
            If DirEntry.Properties("mobile").Count > 0 Then Profile.Mobile = DirEntry.Properties("mobile")(0).ToString
            'Profile.User_Profile_path = DirEntry.Properties("profilePath")(0).ToString

            If DirEntry.Properties("userAccountControl").Count > 0 Then Profile.IsEnabled = Not DirEntry.Properties("userAccountControl")(0)
            If Not Profile.IsEnabled Then
                Profile.Errmsg = "GetProfile : LDAP's user is disabled "
            End If
        Catch ex As Exception
            Profile.Errmsg = "GetProfile : " & ex.Message
        End Try

        Return Profile

    End Function

    Protected Function PrintDirectoryEntryProperties(entry As System.DirectoryServices.DirectoryEntry) As String()
        Dim BL As New EIR_BL
        Dim Result As String() = {}
        For Each Key As String In entry.Properties.PropertyNames
            Dim sPropertyValues As String = [String].Empty
            For Each Value As Object In entry.Properties(Key)
                sPropertyValues += Convert.ToString(Value) & ";"
            Next
            sPropertyValues = sPropertyValues.Substring(0, sPropertyValues.Length - 1)
            BL.PushString(Result, Key & "=" & sPropertyValues)
        Next
        Return Result
    End Function

    Protected Function GetPISProfile(ByVal Code As String) As DataTable
        Dim SQL As String = "SELECT * FROM personel_info WHERE CODE='" & Code.Replace("'", "''") & "'"
        Dim BL As New EIR_BL
        Dim DA As New SqlDataAdapter(SQL, BL.PISConnection)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    Public Class DomainUserResult
        'Public'Public Domain As String = ""
        Public LDAP As String = ""
        Public Username As String = ""
        Public Password As String = ""
        Public Title As String = ""
        Public FirstName As String = ""
        Public Initials As String = ""
        Public LastName As String = ""
        Public DisplayName As String = ""
        Public Mail As String = ""

        Public Department As String = ""
        Public Company As String = ""
        'Public Manager As String = ""
        Public Mobile As String = ""
        'Public User_Profile_path As String = ""

        Public IsEnabled As Boolean = False
        Public Errmsg As String = ""
    End Class

    Public Function DirectorySearcherPropertyKey() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("Key")
        DT.Columns.Add("Description")
        '------------- Add Rows -------------
        DT.Rows.Add("First Name", "givenName") '---
        DT.Rows.Add("Initials", "initials") '--- 
        DT.Rows.Add("Last name", "sn") '---
        DT.Rows.Add("Display name", "displayName") '---
        DT.Rows.Add("Description", "description")
        DT.Rows.Add("Office", "physicalDeliveryOfficeName")
        DT.Rows.Add("Telephone number", "telephoneNumber")
        DT.Rows.Add("Other Telephone numbers", "otherTelephone")
        DT.Rows.Add("E-mail", "mail") '---
        DT.Rows.Add("Web page", "WWWHomePage")
        DT.Rows.Add("Other Web pages", "url")
        DT.Rows.Add("Street", "streetAddress")
        DT.Rows.Add("P.O. Box", "postOfficeBox")
        DT.Rows.Add("City", "l")
        DT.Rows.Add("State/province", "st")
        DT.Rows.Add("Zip/Postal Code", "postalCode")
        DT.Rows.Add("Country/region", "c, co, countryCode")
        DT.Rows.Add("User logon name", "userPrincipalName")
        DT.Rows.Add("pre-Windows 2000 logon name", "SAMAccountName")
        DT.Rows.Add("Account disabled?", "userAccountControl") '---
        DT.Rows.Add("Logon script", "scriptPath")
        DT.Rows.Add("Home folder, local path", "homeDirectory")
        DT.Rows.Add("Home folder, Connect, Drive", "homeDrive")
        DT.Rows.Add("Home folder, Connect, To:", "homeDirectory")
        DT.Rows.Add("Title", "title")
        DT.Rows.Add("Department", "department")
        DT.Rows.Add("Company", "company")
        DT.Rows.Add("Mobile", "mobile")
        DT.Rows.Add("Fax", "facsimileTelephoneNumber")
        DT.Rows.Add("Notes", "info")

        Return DT
    End Function

End Class
