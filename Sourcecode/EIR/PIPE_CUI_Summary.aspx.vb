﻿Imports System.Data.SqlClient
Imports EIR

Public Class PIPE_CUI_Summary
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE
    Dim C As New Converter

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Pipe_CUI_Reports
    Dim ReportPermissionManager As New Report_CUI_ERO_Permission

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            UNIQUE_POPUP_ID = Now.ToOADate.ToString.Replace(".", "")
            ClearPanelSearch()
            SetUserAuthorization()
        End If

    End Sub

    Public Property PLANT_ID As Integer
        Get
            Try
                Return ddl_Select_Plant.Items(ddl_Select_Plant.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            PIPE.BindDDl_Plant(ddl_Select_Plant, value)
            ddl_Select_Plant_SelectedIndexChanged(Nothing, Nothing)
        End Set
    End Property

    Public Property AREA_ID As Integer
        Get
            Try
                Return ddl_Select_Area.Items(ddl_Select_Area.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            If Not IsDBNull(value) AndAlso Not IsNothing(value) AndAlso IsNumeric(value) Then
                If PLANT_ID = 0 Then
                    PIPE.BindDDl_Area(ddl_Select_Area, value)
                Else
                    PIPE.BindDDl_Area(PLANT_ID, ddl_Select_Area, value)
                End If
            Else
                If PLANT_ID = 0 Then
                    PIPE.BindDDl_Area(ddl_Select_Area)
                Else
                    PIPE.BindDDl_Area(PLANT_ID, ddl_Select_Area)
                End If
            End If
            PIPE.BindDDl_Tag(ddl_Select_Tag, PLANT_ID, value,, True)
        End Set
    End Property

    Private Property TAG_ID As Integer
        Get
            Try
                Return ddl_Select_Tag.Items(ddl_Select_Tag.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            PIPE.BindDDl_Tag(ddl_Select_Tag, PLANT_ID, AREA_ID, value, True)
            ddl_Select_Tag_SelectedIndexChanged(Nothing, Nothing)
        End Set
    End Property

    Private Property POINT_ID As Integer
        Get
            If Not ddl_Select_Tag.Visible Then Return 0
            Try
                Return ddl_Select_Point.Items(ddl_Select_Point.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            PIPE.BindDDl_Point(ddl_Select_Point, TAG_ID, value)
            ddl_Select_Point_SelectedIndexChanged(Nothing, Nothing)
        End Set
    End Property

    Private ReadOnly Property TAG_Code As String
        Get
            Try
                Return ddl_Select_Tag.Items(ddl_Select_Tag.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property RPT_Year() As Integer
        Get
            Return ddl_Select_Year.Items(ddl_Select_Year.SelectedIndex).Value
        End Get
        Set(ByVal value As Integer)
            '------------------- Initiate Year -----------------
            ddl_Select_Year.Items.Clear()
            Dim _item As New ListItem("Choose Year...", 0)
            ddl_Select_Year.Items.Add(_item)
            For i As Integer = Now.Year + 542 To Now.Year + 544 '--- Add To Next Year
                Dim Item As New ListItem(i, i)
                ddl_Select_Year.Items.Add(Item)
                If i = value Then Item.Selected = True
            Next
        End Set
    End Property


#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub


#End Region
    Private Sub SetUserAuthorization()
        btnCreate.Visible = USER_LEVEL <> EIR_BL.User_Level.Viewer And USER_LEVEL <> EIR_BL.User_Level.PTT_Authenticated ' Only PID Can create project
    End Sub

    Private Sub Search_Changed(sender As Object, e As EventArgs) Handles ddl_Search_Year.SelectedIndexChanged, ddl_Search_Area.SelectedIndexChanged,
    ddl_Search_Process.SelectedIndexChanged, ddl_Search_Service.SelectedIndexChanged, ddl_Search_Step.SelectedIndexChanged, ddl_Search_Status.SelectedIndexChanged, txt_Search_Code.TextChanged

        BindPlan()
    End Sub

    Private Sub ddl_Search_Plant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        Dim PLANT_ID As Integer = ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value
        Dim AREA_ID As Integer = ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value
        If PLANT_ID = 0 Then
            PIPE.BindDDl_Area(ddl_Search_Area, AREA_ID)
        Else
            PIPE.BindDDl_Area(PLANT_ID, ddl_Search_Area, AREA_ID)
        End If
        BindPlan()
    End Sub

    Private Sub BindPlan()
        Dim SQL As String = "SELECT * FROM VW_PIPE_CUI_Header" & vbNewLine
        Dim WHERE As String = ""

        If ddl_Search_Year.SelectedIndex > 0 Then
            WHERE &= " RPT_Year=" & ddl_Search_Year.Items(ddl_Search_Year.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Area.SelectedIndex > 0 Then
            WHERE &= " AREA_ID=" & ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Process.SelectedIndex > 0 Then
            WHERE &= " PROC_ID=" & ddl_Search_Process.Items(ddl_Search_Process.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Service.SelectedIndex > 0 Then
            WHERE &= " SERVICE_ID=" & ddl_Search_Service.Items(ddl_Search_Service.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Status.SelectedIndex > 0 Then
            WHERE &= " Finished=" & ddl_Search_Status.Items(ddl_Search_Status.SelectedIndex).Value & " AND " & vbNewLine
        End If
        If ddl_Search_Step.SelectedIndex > 0 Then
            WHERE &= " RPT_STEP=" & ddl_Search_Step.Items(ddl_Search_Step.SelectedIndex).Value & " AND " & vbNewLine
        End If

        If txt_Search_Code.Text <> "" Then
            WHERE &= " TAG_Code LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%' AND " & vbNewLine
        End If

        If WHERE <> "" Then SQL &= "WHERE " & WHERE.Substring(0, WHERE.Length - 6) & vbNewLine
        SQL &= " ORDER BY RPT_CODE" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try
        pnlBindingError.Visible = False

        Session("PIPE_CUI_Summary") = DT

        Navigation.SesssionSourceName = "PIPE_CUI_Summary"
        Navigation.RenderLayout()

        dialogCreateReport.Visible = False

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptReport
    End Sub

    Private Sub rptReport_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptReport.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRptNo As Label = e.Item.FindControl("lblRptNo")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim RPT_Year As Integer = btnEdit.Attributes("RPT_Year")
        Dim RPT_No As Integer = btnEdit.Attributes("RPT_No")

        Select Case e.CommandName
            Case "Edit"
                Dim SQL As String = "SELECT * FROM VW_PIPE_CUI_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                Dim DT As New DataTable
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                DA.Fill(DT)

                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "This report has been removed."
                    pnlBindingError.Visible = True
                    BindPlan()
                    Exit Sub
                End If

                '---------- Save User Lock Report-----------
                SQL = "SELECT * FROM RPT_PIPE_TM_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                DA = New SqlDataAdapter(SQL, BL.ConnStr)
                DT = New DataTable
                DA.Fill(DT)

                If Session("USER_ID") <> 0 Then
                    If IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                        DT.Rows(0).Item("RPT_LOCK_BY") = Session("USER_ID")
                    End If
                End If
                Dim CMD As New SqlCommandBuilder(DA)
                DA.Update(DT)

                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='PIPE_CUI_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "'", True)
                'Response.Redirect("PIPE_CUI_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No, True)
            Case "Delete"
                Try
                    PIPE.Drop_RPT_CUI_Header(RPT_Year, RPT_No)
                    lblBindingSuccess.Text = "Report " & lblRptNo.Text & " deleted successfully"
                    pnlBindingSuccess.Visible = True
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                End Try
                BindPlan()
        End Select

    End Sub

    Private Sub rptReport_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRptNo As Label = e.Item.FindControl("lblRptNo")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblArea As Label = e.Item.FindControl("lblArea")
        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblPoint As Label = e.Item.FindControl("lblPoint")
        Dim lblClass As Label = e.Item.FindControl("lblClass")
        Dim tdClass As HtmlTableCell = e.Item.FindControl("tdClass")
        Dim lblLife As Label = e.Item.FindControl("lblLife")
        Dim lblStart As Label = e.Item.FindControl("lblStart")
        Dim lblStep As Label = e.Item.FindControl("lblStep")
        Dim lblFinish As Label = e.Item.FindControl("lblFinish")

        Dim imgLock As Image = e.Item.FindControl("imgLock")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnReport As HtmlAnchor = e.Item.FindControl("btnReport")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim btnDelete_Confirm As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("btnDelete_Confirm")

        lblRptNo.Text = e.Item.DataItem("RPT_Code")
        lblPlant.Text = e.Item.DataItem("PLANT_Code").ToString
        lblArea.Text = e.Item.DataItem("AREA_Code").ToString
        lblCode.Text = e.Item.DataItem("TAG_Code").ToString
        lblPoint.Text = e.Item.DataItem("POINT_Name").ToString

        lblClass.Text = e.Item.DataItem("ICLS_Description").ToString
        If Not IsDBNull(e.Item.DataItem("ICLS_ID")) Then
            tdClass.Attributes("class") = BL.Get_Inspection_Css_Box_By_Level(e.Item.DataItem("ICLS_ID"))
        End If

        If Not IsDBNull(e.Item.DataItem("Remain_Life")) And Not IsDBNull(e.Item.DataItem("End_Life_Date")) Then
            Dim End_Life_Date As DateTime = e.Item.DataItem("End_Life_Date")
            Dim Remain_Life As Integer = DateDiff(DateInterval.Year, Now, End_Life_Date)
            If Remain_Life < 0 Then Remain_Life = 0
            lblLife.Text = FormatNumber(Remain_Life, 0)
            If Remain_Life <= 5 Then
                lblLife.ForeColor = Drawing.Color.Red
            ElseIf Remain_Life <= 10 Then
                lblLife.ForeColor = Drawing.Color.Orange
            Else
                lblLife.ForeColor = Drawing.Color.Green
            End If
        Else
            lblLife.Text = "N/A"
        End If

        lblStart.Text = C.DateToString(e.Item.DataItem("Started_Time"), "dd MMM yyyy")
        If Not IsDBNull(e.Item.DataItem("RPT_STEP")) And Not IsDBNull(e.Item.DataItem("Step_Name")) Then
            lblStep.Text = e.Item.DataItem("Step_Name")
            lblStep.ForeColor = PIPE.Get_REPORT_Step_Color(e.Item.DataItem("RPT_STEP"))
        Else
            lblStep.Text = "N/A"
            lblStep.ForeColor = Drawing.Color.Silver
        End If

        If Not IsDBNull(e.Item.DataItem("Finished")) Then
            If e.Item.DataItem("Finished") AndAlso Not IsDBNull(e.Item.DataItem("Finished_Time")) Then
                lblFinish.Text = C.DateToString(e.Item.DataItem("Finished_Time"), "dd MMM yyyy")
                lblFinish.ForeColor = Drawing.Color.Green
            ElseIf e.Item.DataItem("Finished") Then
                lblFinish.Text = "Finished"
                lblFinish.ForeColor = Drawing.Color.Green
            Else
                lblFinish.Text = "Inspecting"
                lblFinish.ForeColor = Drawing.Color.SteelBlue
            End If
        Else
            lblFinish.Text = "N/A"
            lblFinish.ForeColor = Drawing.Color.Gray
        End If

        btnEdit.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnEdit.Attributes("RPT_No") = e.Item.DataItem("RPT_No")
        btnDelete.Attributes("RPT_Year") = e.Item.DataItem("RPT_Year")
        btnDelete.Attributes("RPT_No") = e.Item.DataItem("RPT_No")

        btnReport.Attributes("onClick") = "ShowPreviewReport(" & e.Item.DataItem("RPT_Year") & "," & e.Item.DataItem("RPT_No") & ");"

        lblStep.Attributes("RPT_STEP") = e.Item.DataItem("RPT_STEP")
        If Not IsDBNull(e.Item.DataItem("RPT_LOCK_BY")) Then
            lblStep.Attributes("RPT_LOCK_BY") = e.Item.DataItem("RPT_LOCK_BY")
        Else
            lblStep.Attributes("RPT_LOCK_BY") = -1
        End If

        '------------ Set Lock Status ------------
        Dim RPT_STEP As EIR_PIPE.Report_Step = e.Item.DataItem("RPT_STEP")
        Dim USER_LEVEL As EIR_BL.User_Level = Session("USER_LEVEL")
        Dim RPT_LOCK_BY As Integer = lblStep.Attributes("RPT_LOCK_BY")
        With ReportPermissionManager
            .USER_LEVEL = USER_LEVEL
            .USER_ID = Session("USER_ID")
            .RPT_LOCK_BY = RPT_LOCK_BY
            .IsFinished = Not IsDBNull(e.Item.DataItem("Finished")) AndAlso e.Item.DataItem("Finished")

            If .CanEdit Then
                btnEdit.Visible = True
                imgLock.Visible = False
            Else
                btnEdit.Visible = False
                imgLock.Visible = True
                If Not IsDBNull(e.Item.DataItem("Lock_By_Name")) Then
                    imgLock.ToolTip = e.Item.DataItem("Lock_By_Name")
                End If
            End If

            btnDelete.Visible = Not .IsFinished And .CanEdit
            btnDelete_Confirm.ConfirmText = "Are you sure you want to delete " & lblRptNo.Text & " ?"
        End With

    End Sub

    Private Sub ClearPanelSearch()

        PIPE.BindDDl_Plant(ddl_Search_Plant)
        PIPE.BindDDl_Area(ddl_Search_Area)
        PIPE.BindDDl_Process(ddl_Search_Process)
        PIPE.BindDDl_Service(ddl_Search_Service)
        PIPE.BindDDl_ReportStep(ddl_Search_Step, EIR_BL.Report_Type.Pipe_CUI_Reports)

        '------------- Bind Search Year ---------------
        Dim SQL As String = "SELECT ISNULL(MIN(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Min_Year,ISNULL(MAX(RPT_Year),DATEPART(YYYY,GETDATE()) + 543) Max_Year FROM RPT_PIPE_CUI_Header"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddl_Search_Year.Items.Clear()
        ddl_Search_Year.Items.Add("Choose a Year...")

        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddl_Search_Year.Items.Add(i)
        Next
        ddl_Search_Year.SelectedIndex = ddl_Search_Year.Items.Count - 1

        If USER_LEVEL <> EIR_BL.User_Level.Administrator And USER_LEVEL <> EIR_BL.User_Level.Viewer Then
            If Not IsNothing(Request.QueryString("Editable")) AndAlso Request.QueryString("Editable") = "True" Then
                chk_Search_Edit.Checked = True
            End If
        Else
            chk_Search_Edit.Visible = False
            lblEditable.Visible = False
        End If

        txt_Search_Code.Text = ""
        BindPlan()

    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        '------------ Populate Year For Selection----------
        RPT_Year = Now.Year + 543
        PLANT_ID = 0
        AREA_ID = 0
        TAG_ID = 0
        POINT_ID = 0

        Pipe_Info.DisplayMainPointProperty = False
        Pipe_Info.SetPropertyEditable(UC_PIPE_TAG.PropertyItem.All, False)
        dialogCreateReport.Visible = True
    End Sub

#Region "Dialog Create Report"
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        dialogCreateReport.Visible = False
    End Sub

    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return ViewState("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Private Sub btnValidationClose_Click(sender As Object, e As ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Private Sub ddl_Select_Plant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Select_Plant.SelectedIndexChanged
        If PLANT_ID = 0 Then
            PIPE.BindDDl_Area(ddl_Select_Area, AREA_ID) '------------- Display All Area -------------
        Else
            PIPE.BindDDl_Area(PLANT_ID, ddl_Select_Area, AREA_ID) '------------- Filter Area -------------
        End If
        ddl_Select_Area_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub ddl_Select_Area_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Select_Area.SelectedIndexChanged
        If PLANT_ID = 0 Then
            PIPE.BindDDl_Tag(ddl_Select_Tag, -1, AREA_ID, TAG_ID, True)
        Else
            PIPE.BindDDl_Tag(ddl_Select_Tag, PLANT_ID, AREA_ID, TAG_ID, True)
        End If
        ddl_Select_Tag_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub ddl_Select_Tag_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Select_Tag.SelectedIndexChanged
        PIPE.BindDDl_Point(ddl_Select_Point, TAG_ID, POINT_ID, False)
        DisplayTag()
        ddl_Select_Point_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub ddl_Select_Point_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Select_Point.SelectedIndexChanged
        DisplayPoint()
    End Sub

    Private Sub DisplayTag()
        If TAG_ID = 0 Then
            Pipe_Info.Visible = False
            btnOK.Visible = False
        Else
            Pipe_Info.Visible = True
            Pipe_Info.BindData(TAG_ID)
        End If
    End Sub
    Private Sub DisplayPoint()

        If TAG_ID = 0 Or POINT_ID = -1 Then
            Point_Info.Visible = False
            btnOK.Visible = False
            pnlPointPreview.Visible = False
        Else
            Point_Info.Visible = True
            btnOK.Visible = True
            Point_Info.BindData(TAG_ID, POINT_ID)
            Point_Info.SetPropertyEditable(UC_PIPE_POINT.PropertyItem.All, True)
            '------------- Display Image Overview------------
            pnlPointPreview.Visible = True
            pointOverview.BackImageUrl = "SVGEditor/SVGAPI.aspx?Mode=PIPE_POINT_PREVIEW_IMAGE&TAG_ID=" & TAG_ID & "&POINT_ID=" & POINT_ID & "&t=" & Now.ToOADate.ToString.Replace(".", "")
            lnkPointPreview.HRef = pointOverview.BackImageUrl
        End If

    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        ''--------------Validate --------------
        If RPT_Year = 0 Then
            lblValidation.Text = "Please select Year."
            pnlValidation.Visible = True
            Exit Sub
        End If

        If TAG_ID = 0 Then
            lblValidation.Text = "Please select tag."
            pnlValidation.Visible = True
            Exit Sub
        End If

        If POINT_ID = -1 Then
            lblValidation.Text = "Please select point."
            pnlValidation.Visible = True
            Exit Sub
        End If

        '------------- Save Tag Info -------------
        Try
            Pipe_Info.SaveData()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try


        Dim Sql As String = ""
        Dim DA As SqlDataAdapter = Nothing
        Dim DT As DataTable = Nothing

        '------------ Save Point------------
        If POINT_ID = 999 Then
            Try
                Dim pid As Integer = PIPE.Get_New_Point_ID(TAG_ID)
                Point_Info.POINT_ID = pid
                Point_Info.SaveData()
                POINT_ID = pid
            Catch ex As Exception
                lblValidation.Text = "This CML is already existed"
                pnlValidation.Visible = True
                Exit Sub
            End Try
        End If

        '---------- Create Report Starter Detail ---------
        Sql = "SELECT * FROM RPT_PIPE_CUI_Header WHERE 1=0 "
        DA = New SqlDataAdapter(Sql, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim DR As DataRow = DT.NewRow
        Dim RPT_No As Integer = BL.GetNewReportNumber(RPT_Year)
        Dim CMD As SqlCommandBuilder

        DR("RPT_Year") = RPT_Year
        DR("RPT_No") = RPT_No
        DR("RPT_Type_ID") = RPT_Type_ID
        DR("TAG_ID") = TAG_ID
        DR("POINT_ID") = POINT_ID
        DR("RPT_STEP") = EIR_PIPE.Report_Step.Before_Remove

        Select Case USER_LEVEL
            Case EIR_BL.User_Level.Administrator
                DR("RPT_LOCK_BY") = DBNull.Value
            Case EIR_BL.User_Level.Collector,
                 EIR_BL.User_Level.Inspector,
                 EIR_BL.User_Level.Engineer,
                 EIR_BL.User_Level.Approver
                DR("RPT_LOCK_BY") = Session("USER_ID")
            Case EIR_BL.User_Level.Viewer, EIR_BL.User_Level.PTT_Authenticated
                lblValidation.Text = "You have no authorization to create report."
                pnlValidation.Visible = True
                Exit Sub
        End Select

        '------------ Lock Report ---------------
        If Session("USER_ID") <> 0 Then DR.Item("RPT_LOCK_BY") = Session("USER_ID")

        DR("Created_By") = Session("USER_ID")
        DR("Created_Time") = Now
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now
        DR("Finished_Time") = DBNull.Value
        DR("Result_FileName") = DBNull.Value

        Try
            DT.Rows.Add(DR)
            CMD = New SqlCommandBuilder(DA)
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        '------------ Create Next Step Information ----------------------
        PIPE.Drop_RPT_By_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Before_Remove)
        DT = New DataTable
        DA = New SqlDataAdapter("SELECT * FROM RPT_PIPE_Before_Remove WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        DR = DT.NewRow
        DT.Rows.Add(DR)
        DR("RPT_Year") = RPT_Year
        DR("RPT_No") = RPT_No
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now
        CMD = New SqlCommandBuilder(DA)
        DA.Update(DT)
        Response.Redirect("PIPE_CUI_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&r=1")
    End Sub

    Private Sub Pipe_Info_PropertyChangedByUser(ByRef Sender As UC_PIPE_TAG, Prop As UC_PIPE_TAG.PropertyItem) Handles Pipe_Info.PropertyChangedByUser
        If Point_Info.Visible And (POINT_ID = 0 Or POINT_ID = 999) Then
            Select Case Prop
                Case UC_PIPE_TAG.PropertyItem.PIPE_SIZE
                    Point_Info.PIPE_SIZE = Pipe_Info.PIPE_SIZE
                Case UC_PIPE_TAG.PropertyItem.MATERIAL
                    Point_Info.MAT_CODE_ID = Pipe_Info.MAT_CODE_ID
                Case UC_PIPE_TAG.PropertyItem.CORROSION_ALLOWANCE
                    Point_Info.CA_ID = Pipe_Info.CA_ID
            End Select
        End If
    End Sub

#End Region

End Class