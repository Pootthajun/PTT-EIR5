﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class SPH_Summary
    
    '''<summary>
    '''UDPMain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UDPMain As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''ddl_Search_Year control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddl_Search_Year As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddl_Search_Plant control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddl_Search_Plant As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddl_Search_Route control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddl_Search_Route As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lbl1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txt_Search_Start control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Search_Start As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_Search_Start_CalendarExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Search_Start_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''lbl2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txt_Search_End control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Search_End As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txt_Search_End_CalendarExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_Search_End_CalendarExtender As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''chk_Search_Edit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chk_Search_Edit As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''lblEditable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEditable As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''pnlBindingError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlBindingError As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''btnBindingErrorClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBindingErrorClose As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''lblBindingError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBindingError As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''rptPlan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptPlan As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''btnCreate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCreate As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Navigation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Navigation As Global.EIR.PageNavigation
End Class
