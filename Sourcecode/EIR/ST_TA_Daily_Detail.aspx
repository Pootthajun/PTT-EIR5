﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ST_TA_Daily_Detail.aspx.vb" Inherits="EIR.ST_TA_Daily_Detail" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="MS_ST_Corrosion.ascx" TagName="MS_ST_Corrosion" TagPrefix="uc2" %>

<%@ Register Src="MS_ST_DialogAbsorber_Spec.ascx" TagName="MS_ST_DialogAbsorber_Spec" TagPrefix="uc4" %>

<%@ Register Src="UC_ST_TA_SelectTemplate.ascx" TagName="UC_ST_TA_SelectTemplate" TagPrefix="select_uc" %>
<%@ Register Src="UC_ST_TA_Template1.ascx" TagName="UC_ST_TA_Template1" TagPrefix="select_uc1" %>
<%@ Register Src="UC_ST_TA_Template2.ascx" TagName="UC_ST_TA_Template2" TagPrefix="select_uc2" %>
<%@ Register Src="UC_ST_TA_Template3.ascx" TagName="UC_ST_TA_Template3" TagPrefix="select_uc3" %>
<%@ Register Src="UC_ST_TA_Template4.ascx" TagName="UC_ST_TA_Template4" TagPrefix="select_uc4" %>
<%@ Register Src="UC_ST_TA_Template5.ascx" TagName="UC_ST_TA_Template5" TagPrefix="select_uc5" %>
<%@ Register Src="UC_ST_TA_Template6.ascx" TagName="UC_ST_TA_Template6" TagPrefix="select_uc6" %>
<%@ Register Src="UC_ST_TA_Template7.ascx" TagName="UC_ST_TA_Template7" TagPrefix="select_uc7" %>
<%@ Register Src="UC_ST_TA_Template8.ascx" TagName="UC_ST_TA_Template8" TagPrefix="select_uc8" %>
<%@ Register Src="UC_ST_TA_Template9.ascx" TagName="UC_ST_TA_Template9" TagPrefix="select_uc9" %>
<%@ Register Src="~/UC_Select_Template.ascx" TagPrefix="select_uc" TagName="UC_Select_Template" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />
    <style>
        form textarea,
        form .wysiwyg {
            padding: 6px;
            font-size: 13px;
            border: 1px solid #d5d5d5;
            /*color: #333;
            border-style: none;
            background: none;*/
        }
    </style>

     <script type="text/jscript">

         $(function () {
               $('#<%=txtTime_Start.ClientID%>').datetimepicker({
                  datepicker: false,
                  format: 'H:i',
                  step: 5
              });

              $('#<%=txtTime_End.ClientID%>').datetimepicker({
                  datepicker: false,
                  format: 'H:i',
                  step: 5
              }); 
         });
        function MyTime() {
            $('#<%=txtTime_Start.ClientID%>').datetimepicker({
                  datepicker: false,
                  format: 'H:i',
                  step: 5
              });

              $('#<%=txtTime_End.ClientID%>').datetimepicker({
                  datepicker: false,
                  format: 'H:i',
                  step: 5
              }); 

  

        }

         



    </script>

    <!-- Page Head -->
    <asp:UpdatePanel ID="UDPMain" runat="Server">
        <ContentTemplate>
            <asp:Label ID="lblProperty" runat="server" Text="" Style="display: none;"></asp:Label>

            <h2>Turnaround 
              
            </h2>
            <asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" Text=""></asp:TextBox>
            <asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" Text=""></asp:TextBox>
            <asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" />

            <div class="clear"></div>
            <!-- End .clear -->

            <div class="content-box">
                <!-- Start Content Box -->

                <div class="content-box-header">

                    <h3>
                        <asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>

                    <asp:Panel ID="pnlTab" runat="server">
                        <ul class="content-box-tabs">

                            <li>
                                <asp:LinkButton ID="HTabAsFound" runat="server">As Found </asp:LinkButton></li>
                            <li>
                                <asp:LinkButton ID="HTabAfterClean" runat="server">After Clean </asp:LinkButton></li>
                            <li>
                                <asp:LinkButton ID="HTabNDE" runat="server">NDE </asp:LinkButton></li>
                            <li>
                                <asp:LinkButton ID="HTabRepair" runat="server">Repair </asp:LinkButton></li>
                            <li>
                                <asp:LinkButton ID="HTabAfterRepair" runat="server">After Repair </asp:LinkButton></li>
                            <li>
                                <asp:LinkButton ID="HTabFinal" runat="server">Final </asp:LinkButton></li>
                        </ul>
                    </asp:Panel>
                    <div class="clear"></div>

                </div>
                <!-- End .content-box-header -->

                <div class="content-box-content">
                    <!--tabHeader -->
                    <div class="tab-content current">

                        <fieldset>

                            <asp:Panel ID="pnlList" runat="server" Visible ="false" >


                                <p style="font-weight: bold; font-size: 16px;">
                                    <label class="column-left" style="width: 120px; font-size: 16px;">Report for: </label>
                                    <asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
                                    > Year :
                                    <asp:Label ID="lbl_Year" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>

                                    >
                                    <asp:Label ID="lbl_Equipment" runat="server" Text="Equipment" CssClass="EditReportHeader"></asp:Label>

                                    > Tag : 
                                        <asp:Label ID="lbl_Tag_Code" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>
                                    &nbsp;&nbsp;
                                        <asp:Label ID="lbl_Tag_Name" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>

                                </p>





                                <ul class="shortcut-buttons-set">
                                    <li>
                                        <asp:LinkButton ID="lnkAdd_Insp" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/image_add_48.png" alt="icon" /><br />
							            Add Inspection
							          </span>
                                        </asp:LinkButton>
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear all
							          </span>
                                        </asp:LinkButton>
                                        <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender"
                                            runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all inspection for this report permanently?">
                                        </cc1:ConfirmButtonExtender>
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" /><br />
									    Reset this tab
									    </span>
                                        </asp:LinkButton>
                                    </li>

                                    <li>
                                        <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon"/><br />
									        Preview report
								        </span>
                                        </asp:LinkButton>
                                    </li>
                                </ul>


                                <div class="clear"></div>
                                <!-- End .clear -->

                                <%-- <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                          ID="ddl_Search_Status" runat="server" AutoPostBack="True">
                                <asp:ListItem Value="-1" Text="All status"></asp:ListItem>
                                <asp:ListItem Value="0" Text="Inspecting"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Finished"></asp:ListItem>
                        </asp:DropDownList>--%>


                                <table width="900" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                                    <thead>
                                        <tr>
                                            <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Date</th>
                                            <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Parts / Components</th>
                                            <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Conditions / Problems</th>
                                            <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Time Start</th>
                                            <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Time End</th>
                                            <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Finished</th>
                                            <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Action</th>

                                        </tr>
                                    </thead>
                                    <asp:Repeater ID="rptComponents" runat="server">
                                        <ItemTemplate>
                                            <tbody>

                                                <tr>
                                                    <td id="TD" runat="server">
                                                        <asp:Label ID="lblRPT_Date" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                                    <td>
                                                        <asp:Label ID="lblComponents" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>

                                                    <td>
                                                        <asp:Label ID="lblConditions" runat="server"></asp:Label>

                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTimeStart" runat="server"></asp:Label>
                                                        <asp:Label ID="lblActualStart" runat="server" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTimeEnd" runat="server"></asp:Label>
                                                        <asp:Label ID="lblActualEnd" runat="server" Visible="false"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFinished" runat="server"></asp:Label>

                                                    </td>

                                                    <td style="text-align: left;">
                                                        <asp:ImageButton ID="btnEdit" runat="server" CommandName="Action" ImageUrl="resources/images/icons/pencil.png" ToolTip="Upload/edit more detail" />
                                                        <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" ImageUrl="resources/images/icons/cross.png" ToolTip="Clear detail for this inspection point" />
                                                        <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this inspection permanently?" />
                                                        <%-- <asp:Image ID="imgWarning" runat="server" style="cursor:help;" ImageUrl="resources/images/icons/alert.gif" ToolTip="Unable to autosave Please completed all require detail !!" />--%>
                                                    </td>
                                            </tbody>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                                <p align="right"></p>
                                <p align="right">
                                    <asp:Button ID="btn_ReportDetail_List" runat="server" CssClass="button" Text="Report Detail" />

                                    <asp:Button ID="btn_Next_List" runat="server" CssClass="button" Text="Next" />
                                </p>
                            </asp:Panel>

                            <asp:Panel ID="dialogCreateReport" runat="server" Visible="false">


                                <div class="MaskDialog"></div>
                                <asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog_Picture" Style="width: 850px; margin-left: 100px; margin-top: 100px;">
                                    <h3 style="width: 100%; text-align: left;">Create Daily Report for 
					            <asp:Label ID="lbl_Plant_dialog" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
                                        > Year :
                                    <asp:Label ID="lbl_Year_dialog" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>

                                        >
                                        <asp:Label ID="lbl_Equipment_dialog" runat="server" Text="Equipment" CssClass="EditReportHeader"></asp:Label>

                                        > Tag : 
                                        <asp:Label ID="lbl_Tag_Code_dialog" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>
                                        &nbsp;&nbsp;
                                        <asp:Label ID="lbl_Tag_Name_dialog" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>



                                    </h3>
                                    <p></p>

                                    <p style="font-weight: bold; font-size: 16px; margin-left: 20px;">
                                        <label class="column-left" style="width: 200px; font-size: 16px;">Report Date : </label>
                                        <asp:TextBox runat="server" ID="txt_dialog_Date" Width="100px" CssClass="text-input small-input " PlaceHolder="..." Style="text-align: center;"></asp:TextBox>
                                        <cc1:CalendarExtender ID="dialog_Date_Extender" runat="server"
                                            Format="yyyy-MM-dd" TargetControlID="txt_dialog_Date" PopupPosition="Right">
                                        </cc1:CalendarExtender>
                                        <font color="red">**</font> 
                                                


                                    </p>
                                    <p style="font-weight: bold; font-size: 16px; margin-left: 20px;">
                                        <label class="column-left" style="width: 200px; font-size: 16px;">Parts / Components : </label>
                                        <asp:DropDownList CssClass="text-input small-input" ID="ddl_dialog_INSP_Name" Width="250px"
                                            runat="server" Font-Bold="True">
                                        </asp:DropDownList>
                                        <font color="red">**</font>



                                    </p>


                                    <%--<p style="font-weight:bold; font-size:16px; margin-left:20px;">
				            <label class="column-left" style="width:120px; font-size:16px;" >Time : </label>
                            <asp:TextBox ID="txt_dialog_Time" runat="server" CssClass="text-input small-input " Width="50px"></asp:TextBox>
                                
                                &nbsp To &nbsp <asp:TextBox ID="txt_dialog_To" runat="server" CssClass="text-input small-input " Width="50px"></asp:TextBox>

            </p>--%>


                                    <table cellpadding="0" cellspacing="0">
                                        <tbody style="border-bottom: none;">
                                            <tr>
                                                <td colspan="2">
                                                    <%--<uc1:UC_Pipe_Tag_Info runat="server" ID="Pipe_Info" />--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="border: none;">
                                                    <asp:Panel ID="pnlValidation_dialog" runat="server" class="notification error png_bg">
                                                        <asp:ImageButton ID="ImageButton5" runat="server" CssClass="close"
                                                            ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                                                        <div>
                                                            <asp:Label ID="lblValidation_dialog" runat="server"></asp:Label>
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                                <td style="text-align: right; border: none;">
                                                    <asp:Button ID="btnClose_dialogCreateReport" runat="server" Class="button" Text="Close" />
                                                    &nbsp;
                                <asp:Button ID="btnOK_dialogCreateReport" runat="server" Class="button" Text="Create Report" />

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>


                                </asp:Panel>

                            </asp:Panel>

                            <asp:Panel ID="pnlEdit" runat="server">

                                <p style="font-weight: bold; font-size: 16px;">
                                    <label class="column-left" style="width: 120px; font-size: 16px;">Report for: </label>
                                    <asp:Label ID="lbl_Plant_Edit" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
                                    > Year :
                                <asp:Label ID="lbl_Year_Edit" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>

                                    >
                                    <asp:Label ID="lbl_Equipment_Edit" runat="server" Text="Equipment" CssClass="EditReportHeader"></asp:Label>

                                    <asp:Label ID="lblComponents_Edit" runat="server" Text=""></asp:Label>

                                    > Tag : 
                                        <asp:Label ID="lbl_Tag_Code_Edit" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>
                                    &nbsp;&nbsp;
                                        <asp:Label ID="lbl_Tag_Name_Edit" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>

                                    <%--> Report Date :
                                <asp:TextBox runat="server" ID="txt_Rpt_Date" Width="100px" CssClass="text-input small-input " PlaceHolder="..." style="text-align:center;"></asp:TextBox>  
                                <cc1:CalendarExtender ID="txt_Rpt_Date_Extender" runat="server" 
                                         Format="dd MMM yyyy" TargetControlID="txt_Rpt_Date" PopupPosition="Right" >
                                </cc1:CalendarExtender>
                                <font color="red">**</font>   
                                
                                 Time :
                                <asp:TextBox ID="txtTime_Start" runat="server" CssClass="text-input small-input " Width="50px"></asp:TextBox>
                                <font color="red">**</font> 
                                To &nbsp <asp:TextBox ID="txtTime_End" runat="server" CssClass="text-input small-input " Width="50px"></asp:TextBox>
                                    --%>
                                </p>
                                <div>
                                    <ul class="shortcut-buttons-set">

                                        <li>
                                            <asp:LinkButton ID="lnkEdit_ClearAll" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear all
							          </span>
                                            </asp:LinkButton>
                                            <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender2"
                                                runat="server" Enabled="True" TargetControlID="lnkEdit_ClearAll" ConfirmText="Are you sure to delete all Detail for this report permanently?">
                                            </cc1:ConfirmButtonExtender>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="lnkReset_Edit" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" /><br />
									    Reset this tab
									    </span>
                                            </asp:LinkButton>
                                        </li>

                                        <li>
                                            <asp:LinkButton ID="lnkPreview_Edit" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon"/><br />
									        Preview report
								        </span>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>

                                <table style="border: none;">
                                    <tr>
                                        <td>
                                            <p style="font-weight: bold; font-size: 16px;">
                                                <label class="column-left" style="width: 120px; font-size: 16px;">Report Date: </label>

                                                <asp:TextBox runat="server" ID="txt_Rpt_Date" Width="100px" CssClass="text-input small-input " PlaceHolder="..." Style="text-align: center;"></asp:TextBox>

                                                <cc1:CalendarExtender ID="txt_Rpt_Date_Extender" runat="server"
                                                    Format="yyyy-MM-dd" TargetControlID="txt_Rpt_Date" PopupPosition="Right">
                                                </cc1:CalendarExtender>

                                                <font color="red">**</font>
                                                Time :
                                                <asp:TextBox ID="txtTime_Start" runat="server" CssClass="text-input small-input " Width="50px"></asp:TextBox>
                                                <font color="red">**</font>
                                                To &nbsp
                                                <asp:TextBox ID="txtTime_End" runat="server" CssClass="text-input small-input " Width="50px"></asp:TextBox>

                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p style="font-weight: bold; font-size: 16px;">
                                                Parts / Components :  
                                                        &nbsp;&nbsp;<asp:Label ID="lblEditComponents" runat="server" Text=""></asp:Label>

                                                > Condition : 
                                                    <asp:Label ID="lblEditInspection_Status" runat="server" Text="" CssClass="EditReportHeader"></asp:Label>
                                                &nbsp;&nbsp;
                                            
                                            </p>


                                        </td>
                                    </tr>


                                </table>

                                <asp:Panel ID="pnlDialogProperty" runat="server" Visible="false">
                                    <uc4:MS_ST_DialogAbsorber_Spec ID="DialogAbsorber" runat="server" Visible="false" />

                                </asp:Panel>

                                <br />
                                <asp:Panel ID="pnlDialogCorrosion" runat="server" Visible="false">
                                    <uc2:MS_ST_Corrosion ID="UC_DialogCorrosion" runat="server" Visible="true" />
                                </asp:Panel>
                                <br />




                                <%--=========================Template  Start==================================--%>
                                <asp:Panel ID="pnl" runat="server" Style="width: 70%; text-align: center; margin-left: 150px">
                                    <asp:Repeater ID="rptINSP" runat="server">
                                        <ItemTemplate>
                                            <table border="1" cellspacing="0" cellpadding="5" style="background-color: gray;">
                                                <tr style="text-align: center; background-color: white; height: 30px;">
                                                    <td colspan="2"></td>
                                                </tr>
                                                <tr>
                                                    <td style="font-weight: bold; text-align: right; font-size: 18px; color: white; width: 50%;">
                                                        <asp:Label ID="lblNo" runat="server"></asp:Label>. 
                                                <asp:Label ID="lblINSP" runat="server"></asp:Label>
                                                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                        <asp:Label ID="lblClass" runat="server"></asp:Label>
                                                        <asp:Label ID="lblIclsID" runat="server" Style="display: none"></asp:Label>
                                                        <asp:Label ID="lblDetailID" runat="server" Style="display: none"></asp:Label>
                                                        <asp:Label ID="lblOffRoutineTemplateType" runat="server" Style="display: none"></asp:Label>
                                                        <asp:Label ID="lblPROB_Detail" runat="server" Style="display: none"></asp:Label>
                                                    </td>
                                                    <td style="text-align: right">
                                                        <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/cross_48.png" Width="32" Height="32" />
                                                        <cc1:ConfirmButtonExtender ID="cfbDelete" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this section permanently?" />
                                                    </td>
                                                </tr>


                                                <asp:Panel ID="pnlPage" runat="server" Style="background-color: gray;">
                                                    <select_uc1:UC_ST_TA_Template1 ID="UC_ST_TA_Template1" runat="server" Visible="false" />
                                                    <select_uc2:UC_ST_TA_Template2 ID="UC_ST_TA_Template2" runat="server" Visible="false" />
                                                    <select_uc3:UC_ST_TA_Template3 ID="UC_ST_TA_Template3" runat="server" Visible="false" />
                                                    <select_uc4:UC_ST_TA_Template4 ID="UC_ST_TA_Template4" runat="server" Visible="false" />
                                                    <select_uc5:UC_ST_TA_Template5 ID="UC_ST_TA_Template5" runat="server" Visible="false" />
                                                    <select_uc6:UC_ST_TA_Template6 ID="UC_ST_TA_Template6" runat="server" Visible="false" />
                                                    <select_uc7:UC_ST_TA_Template7 ID="UC_ST_TA_Template7" runat="server" Visible="false" />
                                                    <select_uc8:UC_ST_TA_Template8 ID="UC_ST_TA_Template8" runat="server" Visible="false" />
                                                    <select_uc9:UC_ST_TA_Template9 ID="UC_ST_TA_Template9" runat="server" Visible="false" />
                                                </asp:Panel>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                    <table style="width: 100%; border: 0px; padding: 0; border-spacing: 0; margin-top: 20px;">
                                        <tr>
                                            <td style="text-align: right">
                                                <asp:LinkButton ID="lnkAddSection" runat="server">
				                                    <span>
					                                    <img src="resources/images/icons/icon_arrow_down.jpg" alt="Add Section" width="32" height="32" />
				                                    </span>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                        
                                    </table>
                                    
                                   <select_uc:UC_Select_Template runat="server" id="UC_Select_Template" />
                                    <%--=========================Template  END==================================--%>
                                </asp:Panel>
                                <p></p>
                                <br />

                                <p>
                                    <label class="column-left" style="width: 170px;">Condition(s) / Problem(s) : </label>
                                    <asp:TextBox ID="txtCondition_Problem" TextMode="multiline" Rows="3" runat="server" MaxLength="1000"></asp:TextBox>
                                </p>
                                <p>
                                    <label class="column-left" style="width: 150px;">Possible Cause(s) : </label>
                                    <asp:TextBox ID="txtPossible_Cause" TextMode="multiline" Rows="3" runat="server" MaxLength="1000"></asp:TextBox>
                                </p>
                                <p>
                                    <label class="column-left" style="width: 250px;">Recommendation(s) / Solution(s) : </label>
                                    <asp:TextBox ID="txtRecommendation" TextMode="multiline" Rows="3" runat="server" MaxLength="1000"></asp:TextBox>
                                </p>


                                <p>
                                    <label class="column-left" style="width: 120px;">Employees : </label>
                                    <asp:TextBox
                                        runat="server" Style="width: 100px" ID="txtEmployees_Count"
                                        CssClass="text-input small-input " MaxLength="5"></asp:TextBox>
                                </p>

                                <p>
                                    <label class="column-left" style="width: 120px;">Status : </label>
                                    <asp:DropDownList CssClass="text-input small-input" ID="ddl_Level" Width="250px"
                                        runat="server" Font-Bold="True">
                                    </asp:DropDownList>
                                </p>
                                <p>
                                    <label class="column-left" style="width: 120px;">Progress : </label>
                                    <asp:DropDownList CssClass="text-input small-input" ID="ddlProgress" Width="250px"
                                        runat="server" Font-Bold="True">
                                        <asp:ListItem Value="0" Text="Inspecting"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Finished"></asp:ListItem>
                                    </asp:DropDownList>
                                </p>



                                <h3 style="margin-top: 20px;">Officer</h3>

                                <p>
                                    <label class="column-left" style="width: 120px;">Collector : </label>
                                    <asp:DropDownList CssClass="text-input small-input" ID="cmbCollector" Width="250px"
                                        runat="server" Font-Bold="True">
                                    </asp:DropDownList>
                                </p>
                                <p>
                                    <label class="column-left" style="width: 120px;">Inspector : </label>
                                    <asp:DropDownList CssClass="text-input small-input" ID="cmbInspector" Width="250px"
                                        runat="server" Font-Bold="True">
                                    </asp:DropDownList>
                                </p>
                                <p>
                                    <label class="column-left" style="width: 120px;">Engineer : </label>
                                    <asp:DropDownList CssClass="text-input small-input" ID="cmbEngineer" Width="250px"
                                        runat="server" Font-Bold="True">
                                    </asp:DropDownList>
                                </p>
                                <p>
                                    <label class="column-left" style="width: 120px;">Approver : </label>
                                    <asp:DropDownList CssClass="text-input small-input" ID="cmbAnalyst" Width="250px"
                                        runat="server" Font-Bold="True">
                                    </asp:DropDownList>
                                </p>

                                <p align="right">
                                    <asp:Button ID="btn_ReportDetail" runat="server" CssClass="button" Text="Report Detail" />

                                    <asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
                                    <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Save" />
                                    <%-- <asp:Button ID="btnFinish" runat="server" CssClass="button" Text="Finish" />--%>
                                    <asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" Visible ="false"  />
                                </p>

                            </asp:Panel>
                        </fieldset>

                        <div class="clear"></div>
                        <!-- End .clear -->
                    </div>

                    <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">
                        <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div>
                            <asp:Label ID="lblBindingSuccess" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                        <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close"
                            ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                        <div>
                            <asp:Label ID="lblValidation" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderHead" runat="Server">

    <link rel="Stylesheet" href="resources/timepicker/jquery.datetimepicker.css" />

    <style type="text/css">
        .hyperlink {
            text-decoration: none;
        }

        a:visited {
            color: blue;
        }
    </style>
<%--    <script type="text/jscript" src="resources/timepicker/jquery.js"></script>--%>
    <script type="text/jscript" src="resources/timepicker/jquery.datetimepicker.full.js"></script>

   
</asp:Content>


