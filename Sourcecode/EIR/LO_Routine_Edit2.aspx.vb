﻿Imports System.Data
Imports System.Data.SqlClient
Public Class LO_Routine_Edit2
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim CV As New Converter

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Lube_Oil_Report

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property

    Private Property RPT_Month() As Integer
        Get
            If IsNumeric(ViewState("RPT_Month")) Then
                Return ViewState("RPT_Month")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Month") = value
        End Set
    End Property

    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private Property RPT_Period_Type() As EIR_BL.LO_Period_Type
        Get
            If IsNumeric(ViewState("RPT_Period_Type")) Then
                Return ViewState("RPT_Period_Type")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As EIR_BL.LO_Period_Type)
            ViewState("RPT_Period_Type") = value
        End Set
    End Property

    Private Property LO_TAG_TYPE() As EIR_BL.LO_Tag_Type
        Get
            If IsNumeric(ViewState("LO_TAG_TYPE")) Then
                Return ViewState("LO_TAG_TYPE")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As EIR_BL.LO_Tag_Type)
            ViewState("LO_TAG_TYPE") = value
        End Set
    End Property

    Private Property DETAIL_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_ID")) Then
                Return ViewState("DETAIL_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("LO_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_LO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='LO_Summary.aspx'", True)
                    Exit Sub
                End If
                RPT_Month = DT.Rows(0).Item("RPT_Month")
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_LO_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_LO_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_LO_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")

            BindGrid()
        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()
        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_LO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='LO_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY

                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='LO_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='LO_Summary.aspx'", True)
            Exit Sub
        End If

    End Sub

    Private Sub BindGrid()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_LO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("LO_Summary.aspx")
            Exit Sub
        End If

        '------------------------------Header -----------------------------------
        lbl_Month.Text = MonthEng(RPT_Month)
        lbl_Year.Text = RPT_Year
        lbl_Period.Text = DT.Rows(0).Item("RPT_Period_Type_Name")
        RPT_Period_Type = DT.Rows(0).Item("RPT_Period_Type")

        If RPT_Period_Type = EIR_BL.LO_Period_Type.Monthy Then
            HeaderVanish.Visible = False
        End If

        SQL = "SELECT * FROM VW_REPORT_LO_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbLf
        SQL &= "ORDER BY PLANT_ID,LO_TAG_NO"
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        rptTAG.DataSource = DT
        rptTAG.DataBind()

        pnlGrid.Visible = True
        pnlEdit.Visible = False
        'headMenu.Visible = True
    End Sub

    Private Sub BindTagData()
        Dim SQL As String = "SELECT * FROM VW_REPORT_LO_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND Detail_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        pnlEdit.Visible = True
        pnlGrid.Visible = False

        lblEquipement.Text = DT.Rows(0).Item("LO_TAG_Name")
        lblTagNo.Text = DT.Rows(0).Item("LO_TAG_NO")
        'lblArea.Text = DT.Rows(0).Item("PLANT_Name")
        lblOilType.Text = DT.Rows(0).Item("Oil_TYPE_Name")
        LO_TAG_TYPE = DT.Rows(0).Item("LO_TAG_TYPE")

        If Not IsDBNull(DT.Rows(0).Item("Recomment")) Then
            txt_Suggession.Text = DT.Rows(0).Item("Recomment")
        Else
            txt_Suggession.Text = ""
        End If

        '-------------- Add Autosafe Button------------------
        ''txt_Suggess.Attributes("onchange") = "document.getElementById('" & btnUpdateDetail.ClientID & "').click();"

        '--------------- Set Parameter Visibility----------------
        '---------------------- TAN Parameter--------------------
        If LO_TAG_TYPE = EIR_BL.LO_Tag_Type.Critical_Machine Then
            '-------------- Visibility ------------------
            tr_TAN.Visible = True
            '--------------- UI Script ------------------
            td_TAN_Value.Attributes("onclick") = "document.getElementById('" & txt_TAN_Value.ClientID & "').focus();"
            td_TAN_Date.Attributes("onclick") = "document.getElementById('" & txt_TAN_Date.ClientID & "').focus();"
            ImplementJavaMoneyText(txt_TAN_Value)
            '-----------------Value Biding---------------
            If IsNumeric(DT.Rows(0).Item("TAN_Value")) Then
                txt_TAN_Value.Text = FormatNumber(DT.Rows(0).Item("TAN_Value"))
                td_TAN.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Oil_TAN_Css(DT.Rows(0).Item("TAN_Value"))
            Else

                td_TAN.Attributes("Class") = "tbEdit_td"
                txt_TAN_Value.Text = ""
            End If
            If Not IsDBNull(DT.Rows(0).Item("TAN_Date")) Then
                txt_TAN_Date.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("TAN_Date"))
            Else
                txt_TAN_Date.Text = ""
            End If
        Else
            tr_TAN.Visible = False
        End If
        Set_LubeOil_HIST_TAN_ID() '---------------For Dashboard -------

        '---------------------- Oxidation Parameter--------------------
        If LO_TAG_TYPE = EIR_BL.LO_Tag_Type.Critical_Machine Then
            '-------------- Visibility ------------------
            tr_OX.Visible = True
            '--------------- UI Script ------------------
            td_OX_Value.Attributes("onclick") = "document.getElementById('" & txt_OX_Value.ClientID & "').focus();"
            td_OX_Date.Attributes("onclick") = "document.getElementById('" & txt_OX_Date.ClientID & "').focus();"
            ImplementJavaMoneyText(txt_OX_Value)
            txt_OX_Value.Style("text-align") = "center"
            '-----------------Value Biding---------------

            If Not IsDBNull(DT.Rows(0).Item("Oil_Cat")) Then
                Select Case DT.Rows(0).Item("Oil_Cat")
                    Case 1 '----------------Oxidation ---------------
                        lbl_OX_UNIT.Text = "%"
                        td_OX_Type.Attributes("Class") = "tbEdit_td"
                        td_OX_Type.InnerHtml = "Oxidation"
                        If IsNumeric(DT.Rows(0).Item("OX_Value")) Then
                            td_OX_Type.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Oil_OX_Css(DT.Rows(0).Item("OX_Value"))
                        Else
                            td_OX_Type.Attributes("Class") = "tbEdit_td"
                        End If
                        imgWater1.Visible = True
                        imgWater2.Visible = False
                        imgOxidation.Visible = True
                    Case 2 '-----------Anti Oxidation----------------
                        lbl_OX_UNIT.Text = "% remain"
                        td_OX_Type.InnerHtml = "Anti-Oxidation"
                        If IsNumeric(DT.Rows(0).Item("OX_Value")) Then
                            td_OX_Type.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Oil_ANTI_OX_Css(DT.Rows(0).Item("OX_Value"))
                        Else
                            td_OX_Type.Attributes("Class") = "tbEdit_td"
                        End If
                        imgWater1.Visible = False
                        imgWater2.Visible = True
                        imgOxidation.Visible = False
                    Case Else
                        lbl_OX_UNIT.Text = ""
                        td_OX_Type.Attributes("Class") = "tbEdit_td"
                        imgOxidation.Visible = False
                End Select
            Else
                lbl_OX_UNIT.Text = ""
                td_OX_Type.Attributes("Class") = "tbEdit_td"
            End If

            If IsNumeric(DT.Rows(0).Item("OX_Value")) Then
                txt_OX_Value.Text = FormatNumber(DT.Rows(0).Item("OX_Value"))
            Else
                txt_OX_Value.Text = ""
            End If

            If Not IsDBNull(DT.Rows(0).Item("OX_Date")) Then
                txt_OX_Date.Text = CV.DateToString(DT.Rows(0).Item("OX_Date"), "yyyy-MM-dd")
            Else
                txt_OX_Date.Text = ""
            End If

        Else
            tr_OX.Visible = False
        End If
        Set_LubeOil_HIST_OX_ID() '---------------For Dashboard -------

        '---------------------- Water Parameter--------------------
        If LO_TAG_TYPE = EIR_BL.LO_Tag_Type.Critical_Machine Then
            '-------------- Visibility ------------------
            tr_Water.Visible = True
            '--------------- UI Script ------------------
            td_Water_Value.Attributes("onclick") = "document.getElementById('" & txt_Water_Value.ClientID & "').focus();"
            td_Water_Date.Attributes("onclick") = "document.getElementById('" & txt_Water_Date.ClientID & "').focus();"
            ImplementJavaMoneyText(txt_Water_Value)
            txt_Water_Value.Style("text-align") = "center"
            '-----------------Value Biding---------------
            If IsNumeric(DT.Rows(0).Item("Water_Value")) Then
                txt_Water_Value.Text = FormatNumber(DT.Rows(0).Item("Water_Value"))
            Else
                txt_Water_Value.Text = ""
            End If
            If Not IsDBNull(DT.Rows(0).Item("Water_Date")) Then
                txt_Water_Date.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("Water_Date"))
            Else
                txt_Water_Date.Text = ""
            End If

            If Not IsDBNull(DT.Rows(0).Item("Oil_Cat")) Then
                Select Case DT.Rows(0).Item("Oil_Cat")
                    Case 1
                        If IsNumeric(DT.Rows(0).Item("Water_Value")) Then
                            td_Water.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Water_Mineral_Css(DT.Rows(0).Item("Water_Value"))
                        Else
                            td_Water.Attributes("Class") = "tbEdit_td"
                        End If
                    Case 2
                        If IsNumeric(DT.Rows(0).Item("Water_Value")) Then
                            td_Water.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Water_Synthetic_Css(DT.Rows(0).Item("Water_Value"))
                        Else
                            td_Water.Attributes("Class") = "tbEdit_td"
                        End If
                End Select
            End If
        Else
            tr_Water.Visible = False
        End If
        Set_LubeOil_HIST_WATER_ID() '---------------For Dashboard -------

        '---------------------- Particles Count--------------------
        If RPT_Period_Type = EIR_BL.LO_Period_Type.Quaterly Or LO_TAG_TYPE <> EIR_BL.LO_Tag_Type.Balance_Of_Plant Then
            '-------------- Visibility ------------------
            tr_PART_COUNT.Visible = True
            '--------------- UI Script ------------------
            td_PART_COUNT_Value.Attributes("onclick") = "document.getElementById('" & ddl_PART_COUNT_Value.ClientID & "').click();"
            td_PART_COUNT_Date.Attributes("onclick") = "document.getElementById('" & txt_PART_COUNT_Date.ClientID & "').focus();"
            '-----------------Value Biding---------------
            ddl_PART_COUNT_Value.SelectedIndex = 0
            If Not IsDBNull(DT.Rows(0).Item("PART_COUNT_Value")) Then
                For i As Integer = 0 To ddl_PART_COUNT_Value.Items.Count - 1
                    If ddl_PART_COUNT_Value.Items(i).Value = DT.Rows(0).Item("PART_COUNT_Value") Then
                        ddl_PART_COUNT_Value.SelectedIndex = i
                        Exit For
                    End If
                Next
            End If
            If Not IsDBNull(DT.Rows(0).Item("PART_COUNT_Date")) Then
                txt_PART_COUNT_Date.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("PART_COUNT_Date"))
            Else
                txt_PART_COUNT_Date.Text = ""
            End If
        Else
            tr_PART_COUNT.Visible = False
        End If
        Set_LubeOil_HIST_PART_ID() '---------------For Dashboard -------

        '---------------------- Varnish--------------------
        If RPT_Period_Type = EIR_BL.LO_Period_Type.Quaterly Then
            '-------------- Set Enabled ------------------
            txt_Varnish_Value.Enabled = True
            txt_Varnish_Date.Enabled = True
            txt_Varnish_Date_Calendar.Enabled = True
            '--------------- UI Script ------------------
            td_Varnish_Value.Attributes("onclick") = "document.getElementById('" & txt_Varnish_Value.ClientID & "').focus();"
            td_Varnish_Date.Attributes("onclick") = "document.getElementById('" & txt_Varnish_Date.ClientID & "').focus();"
            ImplementJavaIntegerText(txt_Varnish_Value, False)
            txt_Varnish_Value.Style("text-align") = "center"
            '-----------------Value Biding---------------
            If IsNumeric(DT.Rows(0).Item("VANISH_Value")) Then
                txt_Varnish_Value.Text = FormatNumber(DT.Rows(0).Item("VANISH_Value"), 0)
                td_Varnish.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Oil_Varnish_Css(DT.Rows(0).Item("VANISH_Value"))
            Else
                txt_Varnish_Value.Text = ""
                td_Varnish.Attributes("Class") = "tbEdit_td"
            End If

            If Not IsDBNull(DT.Rows(0).Item("VANISH_Date")) Then
                txt_Varnish_Date.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("VANISH_Date"))
            Else
                txt_Varnish_Date.Text = ""
            End If
        Else
            '-------------- Set Enabled ------------------
            txt_Varnish_Value.Enabled = False
            txt_Varnish_Date.Enabled = False
            txt_Varnish_Date_Calendar.Enabled = False
            '--------------- UI Script ------------------
            td_Varnish_Value.Attributes("onclick") = ""
            td_Varnish_Date.Attributes("onclick") = ""
            txt_Varnish_Value.Style("text-align") = "center"
            '-----------------Value Biding---------------
            If IsNumeric(BL.Get_LubeOil_LAST_VANISH_Value(DETAIL_ID)) Then
                txt_Varnish_Value.Text = FormatNumber(BL.Get_LubeOil_LAST_VANISH_Value(DETAIL_ID), 0)
                td_Varnish.Attributes("Class") = "tbEdit_td " & BL.Get_Lube_Oil_Varnish_Css(BL.Get_LubeOil_LAST_VANISH_Value(DETAIL_ID))
            Else
                txt_Varnish_Value.Text = ""
                td_Varnish.Attributes("Class") = "tbEdit_td"
            End If

            If Not IsNothing(BL.Get_LubeOil_LAST_VANISH_Date(DETAIL_ID)) Then
                txt_Varnish_Date.Text = BL.ReportProgrammingDate(BL.Get_LubeOil_LAST_VANISH_Date(DETAIL_ID))
            Else
                txt_Varnish_Date.Text = ""
            End If

        End If
        Set_LubeOil_HIST_VANISH_ID() '---------------For Dashboard -------

        '---------------- Set Parameter Group Visibility ----------------------
        tb_Oil_Condition.Visible = tr_TAN.Visible Or tr_OX.Visible Or tr_Varnish.Visible
        tb_Contamination.Visible = tr_PART_COUNT.Visible Or tr_Water.Visible

        '---------------- Display Radar -----------------
        Dim Visual As New DataVisualizedImage
        Dim B As Byte() = Visual.Get_LubeOil_RadarImage(DETAIL_ID)
        Dim UNIQUE_ID = Now.ToOADate.ToString.Replace(".", "")
        Session("PREVIEW_IMG_" & UNIQUE_ID & "_1") = B
        imgRadar.ImageUrl = "RenderImage.aspx?UNIQUE_ID=" & UNIQUE_ID & "&Image=1&"

    End Sub

    Protected Sub rptTAG_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTAG.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        DETAIL_ID = e.CommandArgument


        BindTagData()






    End Sub

    Dim LastPlant As String = ""
    Protected Sub rptTAG_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTAG.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblArea As LinkButton = e.Item.FindControl("lblArea")
        Dim lblNo As LinkButton = e.Item.FindControl("lblNo")
        'Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblType As LinkButton = e.Item.FindControl("lblType")
        Dim lblOil As LinkButton = e.Item.FindControl("lblOil")
        Dim lblTAN As LinkButton = e.Item.FindControl("lblTAN")
        Dim lblOx As LinkButton = e.Item.FindControl("lblOx")
        Dim lblVarnish As LinkButton = e.Item.FindControl("lblVarnish")
        Dim lblPart As LinkButton = e.Item.FindControl("lblPart")
        Dim lblWater As LinkButton = e.Item.FindControl("lblWater")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnReport As HtmlAnchor = e.Item.FindControl("btnReport")

        Dim btnInComplete As ImageButton = e.Item.FindControl("btnInComplete")

        If e.Item.DataItem("PLANT_Name") <> LastPlant Then
            lblArea.Text = e.Item.DataItem("PLANT_Name")
            LastPlant = e.Item.DataItem("PLANT_Name")
            For i As Integer = 1 To 11
                Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
                If Not IsNothing(td) Then
                    td.Style("border-top") = "1px solid #eeeeee"
                End If
            Next
        End If
        Select Case e.Item.DataItem("LO_TAG_Type")
            Case EIR_BL.LO_Tag_Type.Balance_Of_Plant
                lblType.ForeColor = Drawing.Color.DarkBlue
            Case EIR_BL.LO_Tag_Type.Critical_Machine
                lblType.ForeColor = Drawing.Color.DarkOrange
        End Select

        lblNo.Text = e.Item.DataItem("LO_TAG_NO")

        'Dim td2 As HtmlTableCell = e.Item.FindControl("td2")
        If Not IsDBNull(e.Item.DataItem("Is_Abnormal")) AndAlso e.Item.DataItem("Is_Abnormal") = 1 Then
            'td2.Attributes("class") &= " TextClassA"
            lblNo.CssClass = "TextClassA"
            lblNo.ToolTip = "Abnormal"
        Else
            'td2.Attributes("class") &= " TextNormal"
            lblNo.CssClass = "TextNormal"
            lblNo.ToolTip = "Normal"
        End If

        lblType.Text = e.Item.DataItem("LO_TAG_TYPE_Name")
        lblOil.Text = e.Item.DataItem("Oil_TYPE_Name")

        If Not IsDBNull(e.Item.DataItem("TAN_Value")) Then
            lblTAN.Text = FormatNumber(e.Item.DataItem("TAN_Value"))
        End If

        If Not IsDBNull(e.Item.DataItem("Oil_Cat")) AndAlso e.Item.DataItem("LO_TAG_TYPE") = 2 Then
            If Not IsDBNull(e.Item.DataItem("OX_Value")) Then
                Select Case e.Item.DataItem("Oil_Cat")
                    Case 1
                        lblOx.Text &= "Oxidation " & FormatNumber(e.Item.DataItem("OX_Value")) & " %"
                    Case 2
                        lblOx.Text &= "Anti-Oxidation " & FormatNumber(e.Item.DataItem("OX_Value")) & " % remain"
                End Select
            Else
                lblOx.Text &= "-"
            End If
        End If

        If Not IsDBNull(e.Item.DataItem("VANISH_Value")) Then
            lblVarnish.Text = FormatNumber(e.Item.DataItem("VANISH_Value"), 0)
        End If

        If RPT_Period_Type = EIR_BL.LO_Period_Type.Monthy Then
            Dim td8 As HtmlTableCell = e.Item.FindControl("td8")
            td8.Visible = False
        End If


        lblPart.Text = e.Item.DataItem("PART_COUNT_Display")

        If Not IsDBNull(e.Item.DataItem("WATER_Value")) Then
            lblWater.Text = FormatNumber(e.Item.DataItem("WATER_Value")) & " ppm"
        End If

        lblArea.CommandArgument = e.Item.DataItem("DETAIL_ID")
        lblNo.CommandArgument = e.Item.DataItem("DETAIL_ID")
        lblType.CommandArgument = e.Item.DataItem("DETAIL_ID")
        lblOil.CommandArgument = e.Item.DataItem("DETAIL_ID")
        lblTAN.CommandArgument = e.Item.DataItem("DETAIL_ID")
        lblOx.CommandArgument = e.Item.DataItem("DETAIL_ID")
        lblVarnish.CommandArgument = e.Item.DataItem("DETAIL_ID")
        lblPart.CommandArgument = e.Item.DataItem("DETAIL_ID")
        lblWater.CommandArgument = e.Item.DataItem("DETAIL_ID")
        btnEdit.CommandArgument = e.Item.DataItem("DETAIL_ID")
        btnReport.Attributes("onClick") = "ShowPreviewLOTag(" & e.Item.DataItem("RPT_Year") & "," & e.Item.DataItem("RPT_No") & "," & e.Item.DataItem("Detail_ID") & ");"
        btnInComplete.CommandArgument = e.Item.DataItem("DETAIL_ID")
        btnInComplete.Visible = e.Item.DataItem("IS_Complete") = 0
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        If pnlEdit.Visible Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewLOTag(" & RPT_Year & "," & RPT_No & "," & DETAIL_ID & ");", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
        End If
    End Sub

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='LO_Routine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='LO_Routine_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click

        If pnlEdit.Visible Then
            BL.Drop_RPT_LO_Detail(DETAIL_ID)
            BL.Construct_LO_Report_Detail(RPT_Year, RPT_No, Session("USER_ID"))

            '---------- Get New DETAIL_ID Because previous DETAIL_ID has been cleared ------------
            Dim SQL As String = "SELECT DETAIL_ID FROM VW_REPORT_LO_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND LO_TAG_NO='" & lblTagNo.Text.Replace("'", "''") & "'"
            Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count = 0 Then
                Redirect(Me.Page, "LO_Routine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No)
                Exit Sub
            End If
            DETAIL_ID = DT.Rows(0).Item("DETAIL_ID")

            BindTagData()
        Else
            BL.Drop_RPT_LO_Detail(RPT_Year, RPT_No)
            BL.Construct_LO_Report_Detail(RPT_Year, RPT_No, Session("USER_ID"))
            BindGrid()
        End If

    End Sub

    Protected Sub btn_Back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Back.Click
        If pnlEdit.Visible Then
            BindGrid()
        Else
            HTabHeader_Click(sender, e)
        End If
    End Sub

    Protected Sub btn_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Next.Click
        HTabSummary_Click(sender, e)
    End Sub

#Region "Saving"
    Protected Sub Suggess_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Suggession.TextChanged
        Dim SQL As String = "SELECT * FROM RPT_LO_Detail WHERE Detail_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        DT.Rows(0).Item("Recomment") = Trim(txt_Suggession.Text)
        DT.Rows(0).Item("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        'BindTagData()
    End Sub

    Protected Sub TAN_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_TAN_Value.TextChanged, txt_TAN_Date.TextChanged
        Dim SQL As String = "SELECT * FROM RPT_LO_Detail WHERE Detail_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        If Not IsNumeric(txt_TAN_Value.Text.Replace(",", "")) Then
            DT.Rows(0).Item("TAN_Value") = DBNull.Value
        Else
            DT.Rows(0).Item("TAN_Value") = txt_TAN_Value.Text.Replace(",", "")
        End If
        If Not BL.IsProgrammingDate(txt_TAN_Date.Text) Then
            DT.Rows(0).Item("TAN_Date") = DBNull.Value
        Else
            DT.Rows(0).Item("TAN_Date") = CV.StringToDate(txt_TAN_Date.Text, "yyyy-MM-dd")
        End If
        DT.Rows(0).Item("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        BindTagData()
    End Sub

    Protected Sub OX_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_OX_Value.TextChanged, txt_OX_Date.TextChanged
        Dim SQL As String = "SELECT * FROM RPT_LO_Detail WHERE Detail_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub
        If Not IsNumeric(txt_OX_Value.Text.Replace(",", "")) Then
            DT.Rows(0).Item("OX_Value") = DBNull.Value
        Else
            DT.Rows(0).Item("OX_Value") = txt_OX_Value.Text.Replace(",", "")
        End If
        If Not BL.IsProgrammingDate(txt_OX_Date.Text) Then
            DT.Rows(0).Item("OX_Date") = DBNull.Value
        Else
            DT.Rows(0).Item("OX_Date") = CV.StringToDate(txt_OX_Date.Text, "yyyy-MM-dd")
        End If
        DT.Rows(0).Item("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        BindTagData()
    End Sub

    Protected Sub Water_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Water_Value.TextChanged, txt_Water_Date.TextChanged
        Dim SQL As String = "SELECT * FROM RPT_LO_Detail WHERE Detail_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        If Not IsNumeric(txt_Water_Value.Text.Replace(",", "")) Then
            DT.Rows(0).Item("WATER_Value") = DBNull.Value
        Else
            DT.Rows(0).Item("WATER_Value") = txt_Water_Value.Text.Replace(",", "")
        End If
        If Not BL.IsProgrammingDate(txt_Water_Date.Text) Then
            DT.Rows(0).Item("WATER_Date") = DBNull.Value
        Else
            DT.Rows(0).Item("WATER_Date") = CV.StringToDate(txt_Water_Date.Text, "yyyy-MM-dd")
        End If
        DT.Rows(0).Item("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        BindTagData()
    End Sub


    Protected Sub PART_COUNT_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_PART_COUNT_Value.SelectedIndexChanged, txt_PART_COUNT_Date.TextChanged
        Dim SQL As String = "SELECT * FROM RPT_LO_Detail WHERE Detail_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        If ddl_PART_COUNT_Value.SelectedIndex = 0 Then
            DT.Rows(0).Item("PART_COUNT_Value") = DBNull.Value
        Else
            DT.Rows(0).Item("PART_COUNT_Value") = ddl_PART_COUNT_Value.Items(ddl_PART_COUNT_Value.SelectedIndex).Value
        End If
        If Not BL.IsProgrammingDate(txt_PART_COUNT_Date.Text) Then
            DT.Rows(0).Item("PART_COUNT_Date") = DBNull.Value
        Else
            DT.Rows(0).Item("PART_COUNT_Date") = CV.StringToDate(txt_PART_COUNT_Date.Text, "yyyy-MM-dd")
        End If
        DT.Rows(0).Item("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        BindTagData()
    End Sub

    Protected Sub Varnish_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Varnish_Value.TextChanged, txt_Varnish_Date.TextChanged
        Dim SQL As String = "SELECT * FROM RPT_LO_Detail WHERE Detail_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        If Not IsNumeric(txt_Varnish_Value.Text.Replace(",", "")) Then
            DT.Rows(0).Item("VANISH_Value") = DBNull.Value
        Else
            DT.Rows(0).Item("VANISH_Value") = CInt(txt_Varnish_Value.Text)
        End If
        If Not BL.IsProgrammingDate(txt_Varnish_Date.Text) Then
            DT.Rows(0).Item("VANISH_Date") = DBNull.Value
        Else
            DT.Rows(0).Item("VANISH_Date") = CV.StringToDate(txt_Varnish_Date.Text, "yyyy-MM-dd")
        End If
        DT.Rows(0).Item("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        BindTagData()
    End Sub

    Public Sub Set_LubeOil_HIST_OX_ID()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            Dim His_ID As Integer = BL.Get_LubeOil_HIST_OX_ID(DETAIL_ID)
            If His_ID = 0 Then
                .CommandText = "UPDATE RPT_LO_Detail SET HIST_OX_ID=NULL WHERE DETAIL_ID=" & DETAIL_ID
            Else
                .CommandText = "UPDATE RPT_LO_Detail SET HIST_OX_ID=" & His_ID & " WHERE DETAIL_ID=" & DETAIL_ID
            End If
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
    Public Sub Set_LubeOil_HIST_TAN_ID()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            Dim His_ID As Integer = BL.Get_LubeOil_HIST_TAN_ID(DETAIL_ID)
            If His_ID = 0 Then
                .CommandText = "UPDATE RPT_LO_Detail SET HIST_TAN_ID=NULL WHERE DETAIL_ID=" & DETAIL_ID
            Else
                .CommandText = "UPDATE RPT_LO_Detail SET HIST_TAN_ID=" & His_ID & " WHERE DETAIL_ID=" & DETAIL_ID
            End If
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
    Public Sub Set_LubeOil_HIST_VANISH_ID()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            Dim His_ID As Integer = BL.Get_LubeOil_HIST_VANISH_ID(DETAIL_ID)
            If His_ID = 0 Then
                .CommandText = "UPDATE RPT_LO_Detail SET HIST_VANISH_ID=NULL WHERE DETAIL_ID=" & DETAIL_ID
            Else
                .CommandText = "UPDATE RPT_LO_Detail SET HIST_VANISH_ID=" & His_ID & " WHERE DETAIL_ID=" & DETAIL_ID
            End If
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
    Public Sub Set_LubeOil_HIST_PART_ID()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            Dim His_ID As Integer = BL.Get_LubeOil_HIST_PART_ID(DETAIL_ID)
            If His_ID = 0 Then
                .CommandText = "UPDATE RPT_LO_Detail SET HIST_PART_ID=NULL WHERE DETAIL_ID=" & DETAIL_ID
            Else
                .CommandText = "UPDATE RPT_LO_Detail SET HIST_PART_ID=" & His_ID & " WHERE DETAIL_ID=" & DETAIL_ID
            End If
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
    Public Sub Set_LubeOil_HIST_WATER_ID()
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            Dim His_ID As Integer = BL.Get_LubeOil_HIST_WATER_ID(DETAIL_ID)
            If His_ID = 0 Then
                .CommandText = "UPDATE RPT_LO_Detail SET HIST_WATER_ID=NULL WHERE DETAIL_ID=" & DETAIL_ID
            Else
                .CommandText = "UPDATE RPT_LO_Detail SET HIST_WATER_ID=" & His_ID & " WHERE DETAIL_ID=" & DETAIL_ID
            End If
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
#End Region

End Class