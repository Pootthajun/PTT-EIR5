﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_DialogLawJob.ascx.vb" Inherits="EIR.GL_DialogLawJob" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Panel ID="pnlDialog" runat="server" BackColor="White" Width= "500">
    <table style="border-style: solid; border-width: 1px; width:100%; background-color:white" >
        <tr>
            <td>
                <h2><asp:Label ID="lblAction" runat="server"></asp:Label> Job</h2>
                Path : <b><asp:Label ID="lblFolderPath" runat="server"></asp:Label></b>
                <asp:Label ID="lblParentID" runat="server" Visible="false" ></asp:Label>
                <asp:Label ID="lblFolderID" runat="server" Visible="false" Text="0" ></asp:Label>


                <div class="dialog-col-right" style="width:470px;">
                    <table align="left" width="100%" style="border-bottom: 1px none #fff !important;" cellpadding="0" cellspacing="0">
                        <tbody style="border-bottom: 1px none #fff !important;">
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <b>Job Name</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txt_JobName" runat="server" CssClass="text-input" 
                                    MaxLength="255" Width="460px" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Description</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txt_Description" runat="server" CssClass="text-input" 
                                    Height="80px" MaxLength="500" TextMode="MultiLine" Width="460px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="text-align:right;">
                                <asp:Button ID="btnClose" runat="server" Class="button" Text="Close" />
            &nbsp;
                                <asp:Button ID="btnSave" runat="server" Class="button" 
                                    Text="Save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg" 
                                    Visible="False">
                                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                                    <div>
                                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Button ID="Button1" runat="server" Text="Button" Width="0px" style="display:none"  />
<cc1:ModalPopupExtender ID="dialogLawFolder" runat="server" 
    BackgroundCssClass="MaskDialog" Drag="true"  DropShadow="true" PopupControlID="pnlDialog" 
    TargetControlID="Button1">
</cc1:ModalPopupExtender>

