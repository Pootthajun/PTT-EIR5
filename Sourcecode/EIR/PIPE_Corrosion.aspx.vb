﻿Imports System.Data.SqlClient
Public Class PIPE_Corrosion
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            ResetCA(Nothing, Nothing)
        End If

    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindCA()

        Dim SQL As String = ""
        SQL &= " Select CA.CA_ID,CA_Code,CA_DEPTH_MM,COUNT(TAG.TAG_ID) TotalTag" & vbLf
        SQL &= " FROM MS_PIPE_CA CA" & vbLf
        SQL &= " LEFT JOIN " & vbLf
        SQL &= "(SELECT DISTINCT MS_PIPE_POINT.TAG_ID, MS_PIPE_POINT.CA_ID" & vbLf
        SQL &= " 	FROM MS_PIPE_TAG " & vbLf
        SQL &= " 	INNER JOIN MS_PIPE_POINT On  MS_PIPE_TAG.TAG_ID=MS_PIPE_POINT.TAG_ID" & vbLf
        SQL &= " 	WHERE MS_PIPE_TAG.Active_Status=1" & vbLf
        SQL &= " ) TAG ON CA.CA_ID=TAG.CA_ID" & vbLf


        SQL &= " GROUP BY CA.CA_ID,CA_Code,CA_DEPTH_MM" & vbLf
        SQL &= " ORDER BY CA_Code" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("PIPE_CA") = DT

        Navigation.SesssionSourceName = "PIPE_CA"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptCA
    End Sub

    Protected Sub rptCA_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCA.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblCA As Label = e.Item.FindControl("lblCA")
        Dim lblPipe As Label = e.Item.FindControl("lblPipe")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim cfbDelete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfbDelete")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblCode.Text = e.Item.DataItem("CA_Code").ToString
        If Not IsDBNull(e.Item.DataItem("CA_DEPTH_MM")) Then
            lblCA.Text = CDbl(e.Item.DataItem("CA_DEPTH_MM")) & " mm"
        End If

        If Not IsDBNull(e.Item.DataItem("TotalTag")) AndAlso e.Item.DataItem("TotalTag") > 0 Then
            lblPipe.Text = FormatNumber(e.Item.DataItem("TotalTag"), 0)
            cfbDelete.Enabled = False
            btnDelete.Visible = False
        Else
            lblPipe.Text = "-"
            cfbDelete.Enabled = True
            btnDelete.Visible = True
        End If

        btnEdit.CommandArgument = e.Item.DataItem("CA_ID")

    End Sub

    Private Sub rptCA_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptCA.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim CA_ID As Integer = btnEdit.CommandArgument

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                lblUpdateMode.Text = "Update"
                lblUpdateMode.Attributes("CA_ID") = CA_ID
                '------------------------------------
                pnlListCA.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT CA_ID,CA_Code,CA_DEPTH_MM" & vbLf
                SQL &= " FROM MS_PIPE_CA" & vbLf
                SQL &= " WHERE CA_ID = " & CA_ID & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Corrsion Code is not found"
                    pnlBindingError.Visible = True
                    BindCA()
                    Exit Sub
                End If

                txtCode.Text = DT.Rows(0).Item("CA_Code").ToString
                If Not IsDBNull(DT.Rows(0).Item("CA_DEPTH_MM")) Then
                    txtCA.Text = CDbl(DT.Rows(0).Item("CA_DEPTH_MM"))
                End If

                btnSave.Focus()

            Case "Delete"
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = "DELETE FROM MS_PIPE_CA WHERE CA_ID=" & CA_ID
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try

                Dim PageIndex As Integer = Navigation.CurrentPage
                BindCA()

                lblBindingSuccess.Text = "Delete successfully"
                pnlBindingSuccess.Visible = True

                Try : Navigation.CurrentPage = PageIndex : Catch : End Try
        End Select

    End Sub

    Protected Sub ResetCA(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindCA()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        pnlListCA.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtCode.Text = ""
        txtCA.Text = ""
        ImplementJavaNumericText(txtCA, 1, "left")
        btnCreate.Visible = True
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True
        btnCreate.Visible = False

        lblUpdateMode.Text = "Create"
        lblUpdateMode.Attributes("CA_ID") = 0

        '-----------------------------------
        pnlListCA.Enabled = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtCode.Text = "" Then
            lblValidation.Text = "Please insert corrosion code "
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtCA.Text = "" Then
            lblValidation.Text = "Please insert corrosion allowance"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim CA_ID As Integer = lblUpdateMode.Attributes("CA_ID")

        Dim SQL As String = "SELECT * FROM MS_PIPE_CA WHERE CA_Code='" & txtCode.Text.Replace("'", "''") & "' AND CA_ID<>" & CA_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Corrsion Code is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_PIPE_CA WHERE CA_ID=" & CA_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            CA_ID = GetNewCAID()
            DR("CA_ID") = CA_ID
        Else
            DR = DT.Rows(0)
        End If

        DR("CA_Code") = txtCode.Text
        DR("CA_DEPTH_MM") = txtCA.Text

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetCA(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True
    End Sub

    Private Function GetNewCAID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(CA_ID),0)+1 FROM MS_PIPE_CA "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function


End Class