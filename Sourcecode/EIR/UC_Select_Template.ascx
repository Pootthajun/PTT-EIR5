﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Select_Template.ascx.vb" Inherits="EIR.UC_Select_Template" %>

<div class="MaskDialog"></div>

 <style>
     .Template_Choice:hover {
        border:1px solid #71d3e9;
     }

     .Template_Choice {
        border:1px solid white;
     }
 </style>

<asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog_Picture"  Width ="1100px" Style="margin-top:50px;"  >
    <h2 id="plugin-name">Select Template</h2>

    <center>
        
    <table   >
        <tr>
            <td  class="Template_Choice" align="center" >
                <asp:LinkButton ID="lnkTemplate1" runat="server"   >
					<span>
                        <center>
						<img src="resources/images/Template/Template1.JPG" alt="Template 1" height="150"   />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
            <td  class="Template_Choice" >
                <asp:LinkButton ID="lnkTemplate6" runat="server" >
					<span>
                        <center>
						<img src="resources/images/Template/Template6.JPG" alt="Template 6" height="150"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
            <td class="Template_Choice">
                <asp:LinkButton ID="lnkTemplate4" runat="server" >
					<span>
                        <center>
						<img src="resources/images/Template/Template4.JPG" alt="Template 4" height="150"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
            <td  class="Template_Choice">
                <asp:LinkButton ID="lnkTemplate2" runat="server" >
					<span>
                        <center>
						<img src="resources/images/Template/Template2.JPG" alt="Template 2" height="150"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td class="Template_Choice" >
                <asp:LinkButton ID="lnkTemplate5" runat="server" >
					<span>
                        <center>
						<img src="resources/images/Template/Template5.JPG" alt="Template 5" height="150"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
            <td class="Template_Choice">
                <asp:LinkButton ID="lnkTemplate3" runat="server" >
					<span>
                        <center>
						<img src="resources/images/Template/Template3.JPG" alt="Template 3" height="150" />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
            <td class="Template_Choice" >
                <asp:LinkButton ID="lnkTemplate7" runat="server" >
					<span>
                        <center>
						<img src="resources/images/Template/Template7.JPG" alt="Template 7" height="150"  /> 
                        </center>
					</span>
				</asp:LinkButton>
            </td>
            <td class="Template_Choice" >
                <asp:LinkButton ID="lnkTemplate8" runat="server" >
					<span>
                        <center>
						<img src="resources/images/Template/Template8.JPG" alt="Template 8" height="150"  />
                        </center>
					</span>
				</asp:LinkButton>
            </td>
        </tr>
    </table>
    </center>
    <p align="right" style="margin-top :20px;">
	    <asp:Button ID="btn_Close" runat="server" CssClass="button" Text="Close" />
    </p>
</asp:Panel>