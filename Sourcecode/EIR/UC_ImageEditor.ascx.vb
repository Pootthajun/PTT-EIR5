﻿Imports System.IO
Imports System.Drawing
Imports System.Drawing.Drawing2D

Public Class UC_ImageEditor
    Inherits System.Web.UI.UserControl
    Dim BL As New EIR_BL
    Public Event SaveFile(FileByte As Byte())

    Public Sub ShowDialog()
        DeleteUserTempFile()

        Me.Visible = True
    End Sub

    Public Sub ShowEditDialog(img As Byte())
        DeleteUserTempFile()

        Me.Visible = True
    End Sub

    Private Sub DeleteUserTempFile()
        Dim TempPicturePath As String = BL.Picture_Path
        If TempPicturePath.StartsWith("\") = False Then
            TempPicturePath += "\"
        End If
        TempPicturePath += "Temp\" & Session("USER_Name") & "\"


        If Directory.Exists(TempPicturePath) = True Then
            Directory.Delete(TempPicturePath, True)
        End If

    End Sub

    Public Sub CloseDialog()
        Me.Visible = False
    End Sub


    Private Sub btnSaveImage_Click(sender As Object, e As EventArgs) Handles btnSaveImage.Click
        If Session("DrawingImagePath") IsNot Nothing Then
            If File.Exists(Session("DrawingImagePath")) Then
                Dim dInfo As New FileInfo(Session("DrawingImagePath"))
                Dim upFile As String = dInfo.DirectoryName & "\UploadImage.png"
                If File.Exists(upFile) = True Then
                    Dim b() As Byte = MergeImage(Session("DrawingImagePath"), upFile)
                    'File.WriteAllBytes(dInfo.DirectoryName & "\MergeImage.png", b)
                    RaiseEvent SaveFile(b)
                Else
                    lblValidation.Visible = True
                    lblValidation.Text = "Please Upload File"
                    Me.Visible = True
                End If
            Else
                lblValidation.Visible = True
                lblValidation.Text = "Save Fail"
                Me.Visible = True
            End If
        Else
            lblValidation.Visible = True
            lblValidation.Text = "Invalid Draw Image"
            Me.Visible = True
        End If

    End Sub


    Private Function MergeImage(DrawImage As String, UploadImage As String) As Byte()
        Dim targetHeight As Integer = 400
        Dim targetWidth As Integer = 400
        Dim backImg As Image = Image.FromFile(UploadImage)
        Dim frontImg As Image = Image.FromFile(DrawImage)

        Dim bm As New Bitmap(targetWidth, targetHeight)
        Using canvas As Graphics = Graphics.FromImage(bm)
            canvas.Clear(Color.Transparent)
            canvas.DrawImage(backImg, New Rectangle(0, 0, backImg.Width, backImg.Height))
            canvas.DrawImage(frontImg, New Rectangle(0, 0, frontImg.Width, frontImg.Height))
            canvas.Save()
        End Using

        Dim c As New ImageConverter
        Return c.ConvertTo(bm, GetType(Byte()))

    End Function

    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        If Not FUL_Image.HasFile Then
            lblValidation.Text = "Please select file to update"
            Exit Sub
        End If

        If FUL_Image.PostedFile.ContentType.ToUpper.IndexOf("IMAGE") = -1 Then
            lblValidation.Text = "Allow only image to be uploaded!!"
            Exit Sub
        End If

        Dim TempPicturePath As String = BL.Picture_Path
        If TempPicturePath.StartsWith("\") = False Then
            TempPicturePath += "\"
        End If
        TempPicturePath += "Temp\" & Session("USER_Name") & "\"

        If IO.Directory.Exists(TempPicturePath) = False Then
            IO.Directory.CreateDirectory(TempPicturePath)
        End If

        Dim FileName As String = TempPicturePath & "UploadImage.png"
        If File.Exists(FileName) = True Then
            File.Delete(FileName)
        End If

        FUL_Image.SaveAs(FileName)
        If File.Exists(FileName) = True Then
            Dim img As Image = ResizeImage(FileName, New Size(400, 400))
            File.Delete(FileName)
            img.Save(FileName, Imaging.ImageFormat.Png)

            If File.Exists(FileName) = True Then
                wPaintImg.Attributes.Add("src", "data:image/png;base64," & Convert.ToBase64String(File.ReadAllBytes(FileName)))
            End If
        End If
        Me.Visible = True
    End Sub


    Private Function ResizeImage(ByVal FileName As String, ByVal size As Size, Optional ByVal preserveAspectRatio As Boolean = True) As Image
        Dim OrginalImage As Image = Image.FromFile(FileName)
        Dim newWidth As Integer
        Dim newHeight As Integer
        If preserveAspectRatio Then
            Dim originalWidth As Integer = OrginalImage.Width
            Dim originalHeight As Integer = OrginalImage.Height
            Dim percentWidth As Single = CSng(size.Width) / CSng(originalWidth)
            Dim percentHeight As Single = CSng(size.Height) / CSng(originalHeight)
            Dim percent As Single = If(percentHeight < percentWidth,
                    percentHeight, percentWidth)
            newWidth = CInt(originalWidth * percent)
            newHeight = CInt(originalHeight * percent)
        Else
            newWidth = size.Width
            newHeight = size.Height
        End If
        Dim newImage As Image = New Bitmap(newWidth, newHeight)
        Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
            graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic
            graphicsHandle.DrawImage(OrginalImage, 0, 0, newWidth, newHeight)
        End Using
        OrginalImage.Dispose()

        Return newImage
    End Function
End Class