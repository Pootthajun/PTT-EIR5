﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Total_Problem_Area_Report_RO
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass
    Dim CV As New Converter
    Public Property PLANT_ID() As Integer
        Get
            If Not IsNothing(ViewState("PLANT_ID")) AndAlso IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
            lblPlant.Text = BL.Get_Plant_Name(value)
        End Set
    End Property

    Public Property AREA_ID() As Integer
        Get
            If Not IsNothing(ViewState("AREA_ID")) AndAlso IsNumeric(ViewState("AREA_ID")) Then
                Return ViewState("AREA_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("AREA_ID") = value
            '---------------- Set Relate Plant ID-----------
            Dim SQL As String = ""
            Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT As New DataTable
            SQL = "SELECT Area_Name FROM MS_AREA WHERE AREA_ID=" & value
            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            If DT.Rows.Count = 0 Then
                lblArea.Text = "..."
            Else
                lblArea.Text = DT.Rows(0).Item("Area_Name")
            End If
        End Set
    End Property

    Protected Sub lblBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblBack.Click
        Response.Redirect("Dashboard_Total_Problem_Area_Report_Plant.aspx?PLANT_ID=" & PLANT_ID & "&MONTH_F=" & ddl_Month_F.Text & "&MONTH_T=" & ddl_Month_T.Text & "&YEAR_F=" & ddl_Year_F.Text & "&YEAR_T=" & ddl_Year_T.Text & "&EQUIPMENT=" & EIR_BL.Report_Type.Rotating_Routine_Report)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            PLANT_ID = Request.QueryString("PLANT_ID")
            AREA_ID = Request.QueryString("AREA_ID")

            Dim Month_F As Integer = Request.QueryString("MONTH_F")
            Dim Month_T As Integer = Request.QueryString("MONTH_T")
            Dim Year_F As Integer = Request.QueryString("YEAR_F")
            Dim Year_T As Integer = Request.QueryString("YEAR_T")

            Dashboard.BindDDlMonthEng(ddl_Month_F, Month_F)
            Dashboard.BindDDlMonthEng(ddl_Month_T, Month_T)
            Dashboard.BindDDlYear(ddl_Year_F, Year_F)
            Dashboard.BindDDlYear(ddl_Year_T, Year_T)

        End If
        BindData(ddl_Month_F.SelectedValue, ddl_Month_T.SelectedValue, ddl_Year_F.SelectedValue, ddl_Year_T.SelectedValue)

    End Sub

    Private Sub BindData(ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer)
        Dim DT As New DataTable
        If Year_F > 2500 Then
            Year_F = Year_F - 543
        End If
        If Year_T > 2500 Then
            Year_T = Year_T - 543
        End If

        Dim SQL As String = ""

        Dim DateFrom As Date = CV.StringToDate(Year_F.ToString & "-" & Month_F.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        Dim DateTo As Date = CV.StringToDate(Year_T.ToString & "-" & Month_T.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd")
        DateTo = DateTo.AddMonths(1).AddDays(-1)

        DT = Dashboard.GetDashboardData(EIR_BL.Report_Type.Rotating_Routine_Report, PLANT_ID, 0, DateFrom, DateTo, DashboardClass.ZeroDate, DashboardClass.REPORT_ISSUE.All, EIR_BL.InspectionLevel.All, False, AREA_ID)
        If txtSearchTag.Text <> "" Then
            DT.DefaultView.RowFilter = "TAG_CODE LIKE '%" & txtSearchTag.Text.Replace("'", "''") & "%'"
            DT = DT.DefaultView.ToTable
        End If

        Dim col() As String = {"TAG_ID", "TAG_CODE"}
        Dim DT_DATA As DataTable = DT.DefaultView.ToTable(True, col)
        DT_DATA.Columns.Add("ClassC", System.Type.GetType("System.Int64"))
        DT_DATA.Columns.Add("ClassB", System.Type.GetType("System.Int64"))
        DT_DATA.Columns.Add("ClassA", System.Type.GetType("System.Int64"))
        DT_DATA.Columns.Add("Total", System.Type.GetType("System.Int64"))

        For i As Integer = 0 To DT_DATA.Rows.Count - 1
            DT_DATA.Rows(i).Item("ClassA") = DT.Compute("COUNT(TAG_ID)", "TAG_ID = " & DT_DATA.Rows(i).Item("TAG_ID").ToString & " AND ICLS_ID=3")
            DT_DATA.Rows(i).Item("ClassB") = DT.Compute("COUNT(TAG_ID)", "TAG_ID = " & DT_DATA.Rows(i).Item("TAG_ID").ToString & " AND ICLS_ID=2")
            DT_DATA.Rows(i).Item("ClassC") = DT.Compute("COUNT(TAG_ID)", "TAG_ID = " & DT_DATA.Rows(i).Item("TAG_ID").ToString & " AND ICLS_ID=1")
            DT_DATA.Rows(i).Item("Total") = DT_DATA.Rows(i).Item("ClassA") + DT_DATA.Rows(i).Item("ClassB") + DT_DATA.Rows(i).Item("ClassC")
        Next

        Session("Dashboard_Total_Problem_Area_Report_RO") = DT_DATA
        DisplayChart(ChartMain, New EventArgs)

        rptDataSummary.DataSource = DT_DATA
        rptDataSummary.DataBind()

        DT.DefaultView.RowFilter = ""
        DT.Columns.Add("RPT_CODE")
        For i As Int32 = 0 To DT.Rows.Count - 1
            DT.Rows(i).Item("RPT_CODE") = BL.GetReportCode(DT.Rows(i).Item("RPT_YEAR"), DT.Rows(i).Item("RPT_NO"))
        Next
        DT.DefaultView.Sort = "RPT_YEAR,RPT_Month,TAG_CODE"
        DT = DT.DefaultView.ToTable
        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Protected Sub rptDataSummary_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDataSummary.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblTotal As Label = e.Item.FindControl("lblTotal")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblClassC As Label = e.Item.FindControl("lblClassC")
        Dim lblClassB As Label = e.Item.FindControl("lblClassB")
        Dim lblClassA As Label = e.Item.FindControl("lblClassA")

        Dim ClassC As Integer = 0
        Dim ClassB As Integer = 0
        Dim ClassA As Integer = 0
        lblTotal.Text = e.Item.DataItem("Total")
        lblTag.Text = e.Item.DataItem("TAG_CODE")
        If Not IsDBNull(e.Item.DataItem("ClassC")) AndAlso e.Item.DataItem("ClassC") <> 0 Then
            ClassC = e.Item.DataItem("ClassC")
            lblClassC.Text = FormatNumber(ClassC, 0)
        Else
            lblClassC.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassB")) AndAlso e.Item.DataItem("ClassB") <> 0 Then
            ClassB = e.Item.DataItem("ClassB")
            lblClassB.Text = FormatNumber(ClassB, 0)
        Else
            lblClassB.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassA")) AndAlso e.Item.DataItem("ClassA") <> 0 Then
            ClassA = e.Item.DataItem("ClassA")
            lblClassA.Text = FormatNumber(ClassA, 0)
        Else
            lblClassA.Text = "-"
        End If
    End Sub

    Dim VarDate As String = ""
    Dim VarTagCode As String = ""

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim tdClass As HtmlTableCell = e.Item.FindControl("tdClass")
        Dim lblDate As Label = e.Item.FindControl("lblDate")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblProblem As Label = e.Item.FindControl("lblProblem")
        Dim lblDetail As Label = e.Item.FindControl("lblDetail")
        Dim lblClass As Label = e.Item.FindControl("lblClass")
        Dim lblReportNo As Label = e.Item.FindControl("lblReportNo")

        If Dashboard.FindMonthNameEngShot(e.Item.DataItem("RPT_Month")) & " " & e.Item.DataItem("RPT_Year") <> VarDate Then
            lblDate.Text = Dashboard.FindMonthNameEngShot(e.Item.DataItem("RPT_Month")) & " " & e.Item.DataItem("RPT_Year")
            VarDate = lblDate.Text
        End If
        If e.Item.DataItem("TAG_CODE") <> VarTagCode Then
            lblTag.Text = e.Item.DataItem("TAG_CODE")
            VarTagCode = lblTag.Text
        End If
        lblProblem.Text = e.Item.DataItem("INSPECTION") & "/" & e.Item.DataItem("STATUS_Name")
        lblDetail.Text = e.Item.DataItem("Detail")
        lblClass.Text = e.Item.DataItem("ICLS_Description")
        If e.Item.DataItem("ICLS_Description").ToString <> "" Then
            tdClass.Attributes("Class") = "Level" & e.Item.DataItem("ICLS_Description").ToString
        End If
        lblReportNo.Text = e.Item.DataItem("RPT_CODE")

        'lblTag.Text = e.Item.DataItem("TAG_CODE")
        'If Not IsDBNull(e.Item.DataItem("ClassC")) AndAlso e.Item.DataItem("ClassC") <> 0 Then
        '    ClassC = e.Item.DataItem("ClassC")
        '    lblClassC.Text = FormatNumber(ClassC, 0)
        'Else
        '    lblClassC.Text = "-"
        'End If

        'If Not IsDBNull(e.Item.DataItem("ClassB")) AndAlso e.Item.DataItem("ClassB") <> 0 Then
        '    ClassB = e.Item.DataItem("ClassB")
        '    lblClassB.Text = FormatNumber(ClassB, 0)
        'Else
        '    lblClassB.Text = "-"
        'End If

        'If Not IsDBNull(e.Item.DataItem("ClassA")) AndAlso e.Item.DataItem("ClassA") <> 0 Then
        '    ClassA = e.Item.DataItem("ClassA")
        '    lblClassA.Text = FormatNumber(ClassA, 0)
        'Else
        '    lblClassA.Text = "-"
        'End If

        tbTag.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_RO.aspx?TAG_ID=" & e.Item.DataItem("TAG_ID") & "&RPT_YEAR=" & e.Item.DataItem("RPT_YEAR") & "&RPT_NO=" & e.Item.DataItem("RPT_NO") & "','Dialog_Tag_RO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")

    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ChartType As DataVisualization.Charting.SeriesChartType = ChartMain.Series(0).ChartType
        ChartType = DataVisualization.Charting.SeriesChartType.Pie
        ChartMain.Series(0).ChartType = ChartType

        Dim DT As DataTable = Session("Dashboard_Total_Problem_Area_Report_RO")
        Dim ClassC As Double = 0
        Dim ClassB As Double = 0
        Dim ClassA As Double = 0
        If DT.Rows.Count > 0 Then
            ClassC = DT.Compute("SUM(ClassC)", "")
            ClassB = DT.Compute("SUM(ClassB)", "")
            ClassA = DT.Compute("SUM(ClassA)", "")
        End If


        Dim YValue As Double() = {ClassC, ClassB, ClassA}
        ChartMain.Series("Series1").Points.DataBindY(YValue)
        ChartMain.Series("Series1").Points(0).LegendText = "ClassC"
        ChartMain.Series("Series1").Points(1).LegendText = "ClassB"
        ChartMain.Series("Series1").Points(2).LegendText = "ClassA"

        ChartMain.Series("Series1").Points(0).ToolTip = "ClassC : " & FormatNumber(ClassC, 0) & " tag(s)"
        ChartMain.Series("Series1").Points(1).ToolTip = "ClassB : " & FormatNumber(ClassB, 0) & " tag(s)"
        ChartMain.Series("Series1").Points(2).ToolTip = "ClassA : " & FormatNumber(ClassA, 0) & " tag(s)"

        ChartMain.Series("Series1").Points(0).Color = Drawing.Color.Yellow
        ChartMain.Series("Series1").Points(1).Color = Drawing.Color.Orange
        ChartMain.Series("Series1").Points(2).Color = Drawing.Color.Red

        ChartMain.Titles(1).Text = "Total " & FormatNumber(+ClassC + ClassB + ClassA, 0) & " tag(s)"
    End Sub

End Class