﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Improvement_Sheet
    Inherits System.Web.UI.Page

    Dim Dashboard As New DashboardClass
    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            Dashboard.BindDDlYear(ddl_Year, Date.Now.Year)
            Dashboard.BindDDlEquipment(ddl_Equipment, 1)
            BindData()
        End If
    End Sub

    Private Sub BindData()
        UC_Dashboard_Improvement_Sheet.BindData(ddl_Year.SelectedValue, ddl_Equipment.SelectedValue)
    End Sub

    Protected Sub ALL_ddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Year.SelectedIndexChanged, ddl_Equipment.SelectedIndexChanged
        BindData()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim URL As String = "Dashboard_Improvement_Sheet_Export.aspx?Year=" & ddl_Year.SelectedValue & "&Equipment=" & ddl_Equipment.SelectedValue
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Export", "openPrintWindow('" & URL & "',1000,700);", True)
        BindData()
    End Sub

End Class