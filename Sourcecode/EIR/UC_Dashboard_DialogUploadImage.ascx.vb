﻿Imports System.Data
Imports System.Data.SqlClient
Public Class UC_Dashboard_DialogUploadImage
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL

#Region "Dynamic Property"

    Public Property TAG_CLASS() As EIR_BL.Tag_Class
        Get
            If ViewState("TagClass") = "RO" Then
                Return EIR_BL.Tag_Class.Rotating
            Else
                Return EIR_BL.Tag_Class.Stationary
            End If
        End Get
        Set(ByVal value As EIR_BL.Tag_Class)
            Select Case value
                Case EIR_BL.Tag_Class.Rotating
                    ViewState("TagClass") = "RO"
                Case Else
                    ViewState("TagClass") = "ST"
            End Select
        End Set
    End Property

    Public Property RPT_Year() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_Year")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_Year")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_Year") = value
        End Set
    End Property

    Public Property RPT_No() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_No")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_No")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_No") = value
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If ddl_Tag.SelectedIndex > -1 Then
                Return ddl_Tag.Items(ddl_Tag.SelectedIndex).Value
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            BindTag()
            For i As Integer = 0 To ddl_Tag.Items.Count - 1
                If value = ddl_Tag.Items(i).Value Then
                    ddl_Tag.SelectedIndex = i
                End If
            Next
            ddl_Tag_SelectedIndexChanged(ddl_Tag, Nothing)
        End Set
    End Property

    Public Property PARTNO() As String
        Get
            Return txt_Part.Text
        End Get
        Set(ByVal value As String)
            BindPartNo()
            For i As Integer = 0 To ddl_Part.Items.Count - 1
                If ddl_Part.Items(i).Value = value Then
                    ddl_Part.SelectedIndex = i
                End If
            Next
            If ddl_Part.SelectedIndex = ddl_Part.Items.Count - 1 Then
                txt_Part.Text = value
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "focus", "document.getElementById('" & txt_Part.ClientID & "').select();", True)
            End If
            ddl_Part_SelectedIndexChanged(ddl_Part, New EventArgs)
        End Set
    End Property

    Public Property INSP_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_INSP.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnINSP As Button = item.FindControl("btnINSP")
                If btnINSP.Font.Underline Then
                    Return btnINSP.CommandArgument
                End If
            Next
            Return 0
        End Get
        Set(ByVal value As Integer)
            'FROM TAG
            BindInspection()
            For Each item As RepeaterItem In rpt_INSP.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnINSP As Button = item.FindControl("btnINSP")
                If btnINSP.CommandArgument = value Then
                    btnINSP.Font.Underline = True
                Else
                    btnINSP.Font.Underline = False
                End If
            Next
            STATUS_ID = 0
            PARTNO = ""

            '-------------------Check Require Picture --------------
            Dim RequiredPicture As Boolean = BL.IsInspectionRequirePicture(value)
            TDImage.Visible = RequiredPicture
        End Set
    End Property

    Public Property STATUS_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_STATUS.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnStatus As Button = item.FindControl("btnStatus")
                If btnStatus.Font.Underline Then
                    Return btnStatus.CommandArgument
                End If
            Next
            Return 0
        End Get
        Set(ByVal value As Integer)
            BindStatus()
            For Each item As RepeaterItem In rpt_STATUS.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnStatus As Button = item.FindControl("btnStatus")
                If btnStatus.CommandArgument = value Then
                    btnStatus.Font.Underline = True
                Else
                    btnStatus.Font.Underline = False
                End If
            Next
            LEVEL_ID = LEVEL_ID
        End Set
    End Property

    Public Property LEVEL_ID() As Integer
        Get
            For Each item As RepeaterItem In rpt_LEVEL.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnLevel As Button = item.FindControl("btnLevel")
                If btnLevel.Font.Underline Then
                    Return btnLevel.CommandArgument
                End If
            Next
            Return -1
        End Get
        Set(ByVal value As Integer)
            BindLevel()
            For Each item As RepeaterItem In rpt_LEVEL.Items
                If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
                Dim btnLevel As Button = item.FindControl("btnLevel")
                If btnLevel.CommandArgument = value Then
                    btnLevel.Font.Underline = True
                Else
                    btnLevel.Font.Underline = False
                End If
            Next
            '--------Update Color---------
            UpdateSelectedColor()
        End Set
    End Property

    Private Sub UpdateSelectedColor()

        Dim CSS As String = "LevelDeselect"
        Select Case INSP_ID
            Case 12
                CSS = BL.Get_Inspection_Css_Box_By_Zone(LEVEL_ID)
            Case Else
                CSS = BL.Get_Inspection_Css_Box_By_Level(LEVEL_ID)
        End Select

        '--------------Inspection---------------
        For Each item As RepeaterItem In rpt_INSP.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnINSP As Button = item.FindControl("btnINSP")
            If btnINSP.Font.Underline Then
                btnINSP.CssClass = CSS
            Else
                btnINSP.CssClass = "LevelDeselect"
            End If
        Next
        '--------------Status---------------
        For Each item As RepeaterItem In rpt_STATUS.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnStatus As Button = item.FindControl("btnStatus")
            If btnStatus.Font.Underline Then
                btnStatus.CssClass = CSS
            Else
                btnStatus.CssClass = "LevelDeselect"
            End If
        Next
        '---------------Level---------------
        For Each item As RepeaterItem In rpt_LEVEL.Items
            If item.ItemType <> ListItemType.AlternatingItem And item.ItemType <> ListItemType.Item Then Continue For
            Dim btnLevel As Button = item.FindControl("btnLevel")
            If btnLevel.Font.Underline Then
                btnLevel.CssClass = CSS
            Else
                btnLevel.CssClass = "LevelDeselect"
            End If
        Next
    End Sub

    Public Property DETAIL_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("DETAIL_ID")) Then
                Return 0
            Else
                Return Me.Attributes("DETAIL_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("DETAIL_ID") = value
        End Set
    End Property

    Public Property LAST_DETAIL_ID() As Integer
        Get
            If ddl_Last_Detail.SelectedIndex > -1 Then
                Return ddl_Last_Detail.Items(ddl_Last_Detail.SelectedIndex).Value
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            BindLastDetail()
            For i As Integer = 0 To ddl_Last_Detail.Items.Count - 1
                If value = ddl_Last_Detail.Items(i).Value Then
                    ddl_Last_Detail.SelectedIndex = i
                    Exit For
                End If
            Next
            ddl_Last_Detail_SelectedIndexChanged(ddl_Last_Detail, Nothing)
        End Set
    End Property

    Public Property INSP_Detail() As String
        Get
            Return txt_Detail.Text
        End Get
        Set(ByVal value As String)
            txt_Detail.Text = value
        End Set
    End Property

    Public Property INSP_Recomment() As String
        Get
            Return txt_Recomment.Text
        End Get
        Set(ByVal value As String)
            txt_Recomment.Text = value
        End Set
    End Property

    Public Property Fixed() As TriState
        Get
            Select Case True
                Case Equals(btnSolveYes.BackColor, Drawing.Color.Green)
                    Return TriState.True
                Case Equals(btnSolveNo.BackColor, Drawing.Color.OrangeRed)
                    Return TriState.False
                Case Else
                    Return TriState.UseDefault
            End Select
        End Get
        Set(ByVal value As TriState)
            Select Case value
                Case TriState.UseDefault
                    btnSolveYes.BackColor = Drawing.Color.White
                    btnSolveNo.BackColor = Drawing.Color.White
                    btnSolveYes.ForeColor = Drawing.Color.Black
                Case TriState.True
                    btnSolveYes.BackColor = Drawing.Color.Green
                    btnSolveYes.ForeColor = Drawing.Color.White
                    btnSolveNo.BackColor = Drawing.Color.White
                Case TriState.False
                    btnSolveYes.BackColor = Drawing.Color.White
                    btnSolveYes.ForeColor = Drawing.Color.Black
                    btnSolveNo.BackColor = Drawing.Color.OrangeRed
            End Select
        End Set
    End Property

    Public Property MY_PREVIEW1() As Byte()
        Get
            If BL.IsInspectionRequirePicture(INSP_ID) Then
                Return Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1")
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Byte())
            Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1") = value
            ImgPreview1.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=1&Class=" & CInt(TAG_CLASS)
        End Set
    End Property

    Public Property MY_PREVIEW2() As Byte()
        Get
            If BL.IsInspectionRequirePicture(INSP_ID) Then
                Return Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2")
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Byte())
            If BL.IsInspectionRequirePicture(INSP_ID) Then
                Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2") = value
                ImgPreview2.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=2&Class=" & CInt(TAG_CLASS)
            Else
                Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2") = Nothing
            End If
        End Set
    End Property

    Protected Sub btnRefreshImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshImage.Click
        If Not IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_1")) Then
            MY_PREVIEW1 = Session("TempImage_" & UNIQUE_POPUP_ID & "_1")
            Session("TempImage_" & UNIQUE_POPUP_ID & "_1") = Nothing
        End If

        If Not IsNothing(Session("TempImage_" & UNIQUE_POPUP_ID & "_2")) Then
            MY_PREVIEW2 = Session("TempImage_" & UNIQUE_POPUP_ID & "_2")
            Session("TempImage_" & UNIQUE_POPUP_ID & "_2") = Nothing
        End If

        ImgPreview1.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=1&Class=" & CInt(TAG_CLASS)
        ImgPreview2.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & UNIQUE_POPUP_ID & "&Image=2&Class=" & CInt(TAG_CLASS)
    End Sub

#End Region

#Region "Static Property"
    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return Me.Attributes("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            If value <> UNIQUE_POPUP_ID Then
                MY_PREVIEW1 = Nothing
                MY_PREVIEW2 = Nothing
            End If
            Me.Attributes("UNIQUE_POPUP_ID") = value
        End Set
    End Property
#End Region


    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        MY_PREVIEW1 = Nothing
        MY_PREVIEW2 = Nothing
        CloseDialog()
    End Sub

    Public Sub CloseDialog()

        Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_1") = Nothing
        Session("PREVIEW_IMG_" & UNIQUE_POPUP_ID & "_2") = Nothing

        Me.Visible = False
    End Sub

    Public Sub ShowDialog(ByVal Init_RPT_Year As Integer,
                          ByVal Init_RPT_No As Integer,
                          Optional ByVal Init_TAG_ID As Integer = 0,
                          Optional ByVal Init_INSP_ID As Integer = 0)

        RPT_Year = Init_RPT_Year
        RPT_No = Init_RPT_No
        TAG_ID = Init_TAG_ID

        INSP_ID = Init_INSP_ID

        LEVEL_ID = -1
        STATUS_ID = 0

        INSP_Detail = ""
        INSP_Recomment = ""
        Fixed = TriState.UseDefault ' And Hidden/ Cloak

        DETAIL_ID = 0

        Me.Visible = True

    End Sub

    Public Sub ShowDialog(ByVal DETAIL_ID As Integer) ' For Case Edit

        Me.DETAIL_ID = 0

        Dim SQL As String = "SELECT * FROM RPT_" & ViewState("TagClass").ToString & "_DETAIL WHERE DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to collect information for this inspection point');", True)
            CloseDialog()
            Exit Sub
        End If

        Me.DETAIL_ID = DT.Rows(0).Item("DETAIL_ID")
        RPT_Year = DT.Rows(0).Item("RPT_Year")
        RPT_No = DT.Rows(0).Item("RPT_No")
        TAG_ID = DT.Rows(0).Item("TAG_ID")
        INSP_ID = DT.Rows(0).Item("INSP_ID")
        If Not IsDBNull(DT.Rows(0).Item("STATUS_ID")) Then
            STATUS_ID = DT.Rows(0).Item("STATUS_ID")
        End If
        If Not IsDBNull(DT.Rows(0).Item("ICLS_ID")) Then
            LEVEL_ID = DT.Rows(0).Item("ICLS_ID")
        Else
            LEVEL_ID = -1
        End If
        If Not IsDBNull(DT.Rows(0).Item("COMP_NO")) Then
            PARTNO = DT.Rows(0).Item("COMP_NO")
        End If
        If Not IsDBNull(DT.Rows(0).Item("LAST_DETAIL_ID")) Then
            LAST_DETAIL_ID = DT.Rows(0).Item("LAST_DETAIL_ID")
        End If
        If Not IsDBNull(DT.Rows(0).Item("PROB_Detail")) Then
            INSP_Detail = DT.Rows(0).Item("PROB_Detail")
        End If
        If Not IsDBNull(DT.Rows(0).Item("PROB_Recomment")) Then
            INSP_Recomment = DT.Rows(0).Item("PROB_Recomment")
        End If

        If Not IsDBNull(DT.Rows(0).Item("Fixed")) Then
            If DT.Rows(0).Item("Fixed") Then
                Fixed = TriState.True
            Else
                Fixed = TriState.False
            End If
        Else
            Fixed = TriState.UseDefault
        End If

        Me.Visible = True

        DisableMainInformation()

    End Sub

    Private Sub DisableMainInformation()
        ddl_Tag.Enabled = False
        ddl_Last_Detail.Enabled = False
        pnl_Inspection.Enabled = False
        ddl_Part.Visible = False
        txt_Part.Enabled = False
        btnSolveYes.Enabled = False
        btnSolveNo.Enabled = False
        pnl_Status.Enabled = False
        pnl_Level.Enabled = False
        txt_Detail.Enabled = False
        txt_Recomment.Enabled = False
        ImgPreview1.Enabled = False
        ImgPreview2.Enabled = False
    End Sub

    Private Sub FindLastDetailPicture()
        Dim SQL As String = "EXEC dbo.SP_Get_Previous_Tag_Inspection_Pictures " & RPT_Year & "," & RPT_No & "," & TAG_ID & "," & INSP_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            MY_PREVIEW1 = Nothing
            MY_PREVIEW2 = Nothing
        Else
            If Not IsDBNull(DT.Rows(0).Item("PIC_Detail1")) Then MY_PREVIEW1 = DT.Rows(0).Item("PIC_Detail1")
            If Not IsDBNull(DT.Rows(0).Item("PIC_Detail2")) Then MY_PREVIEW2 = DT.Rows(0).Item("PIC_Detail2")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlValidation.Visible = False
    End Sub

    Private Sub BindTag()
        Dim SQL As String = "SELECT TAG_ID,TAG_CODE FROM " & vbNewLine
        SQL &= " VW_ALL_ACTIVE_" & ViewState("TagClass").ToString & "_TAG TAG" & vbNewLine
        SQL &= " WHERE TAG.ROUTE_ID=(SELECT ROUTE_ID FROM RPT_" & ViewState("TagClass").ToString & "_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & ")" & vbNewLine
        SQL &= " ORDER BY TAG_CODE" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl_Tag.Items.Clear()
        ddl_Tag.Items.Add(New ListItem("", 0))
        ddl_Tag.SelectedIndex = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TAG_Code"), DT.Rows(i).Item("TAG_ID"))
            ddl_Tag.Items.Add(Item)
        Next
    End Sub

    Private Sub BindPartNo()
        Dim SQL As String = "SELECT DISTINCT ISNULL(COMP_NO,'') COMP_NO FROM RPT_" & ViewState("TagClass").ToString & "_Detail" & vbNewLine
        SQL &= " WHERE TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID & " AND ISNULL(COMP_NO,'')<>''" & vbNewLine
        SQL &= " ORDER BY ISNULL(COMP_NO,'')" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl_Part.Items.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("COMP_NO"))
        Next
        ddl_Part.Items.Add(New ListItem("Insert new part...", ""))
        ddl_Part.SelectedIndex = ddl_Part.Items.Count - 1
    End Sub

    Private Sub BindInspection()
        Dim SQL As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbNewLine
        SQL &= " DECLARE @RPT_No As INT =" & RPT_No & vbNewLine
        SQL &= " DECLARE @TAG_ID As INT =" & TAG_ID & vbNewLine

        SQL &= " SELECT DISTINCT * FROM " & vbNewLine
        SQL &= " (" & vbNewLine
        SQL &= "    SELECT DISTINCT INSP.INSP_ID,DNSP.INSP_Name" & vbNewLine
        SQL &= "    FROM MS_" & ViewState("TagClass").ToString & "_TAG " & vbNewLine
        SQL &= "    LEFT JOIN MS_" & ViewState("TagClass").ToString & "_TAG_Inspection INSP ON MS_" & ViewState("TagClass").ToString & "_TAG.TAG_TYPE_ID=INSP.TAG_TYPE_ID" & vbNewLine
        SQL &= "    LEFT JOIN MS_" & ViewState("TagClass").ToString & "_Default_Inspection DNSP ON INSP.INSP_ID=DNSP.INSP_ID" & vbNewLine
        SQL &= "    WHERE TAG_ID = @TAG_ID " & vbNewLine
        SQL &= "    AND (" & vbNewLine
        SQL &= "    INSP.REF_INSP_ID IS NULL OR NOT EXISTS" & vbNewLine
        SQL &= "    (SELECT DETAIL_ID FROM RPT_" & ViewState("TagClass").ToString & "_Detail WHERE RPT_Year=@RPT_Year AND RPT_No=@RPT_No AND TAG_ID=@TAG_ID" & vbNewLine
        SQL &= "    AND INSP_ID=INSP.REF_INSP_ID AND ISNULL(STATUS_ID,0)<>INSP.REF_STATUS_ID)" & vbNewLine
        SQL &= "    )" & vbNewLine
        SQL &= "    UNION ALL " & vbNewLine
        SQL &= "    ("
        SQL &= "    SELECT DISTINCT VW.INSP_ID,VW.INSP_Name" & vbNewLine
        SQL &= "    FROM VW_REPORT_" & ViewState("TagClass").ToString & "_DETAIL VW" & vbNewLine
        SQL &= "    WHERE CURRENT_LEVEL IS NOT NULL AND RPT_Year=@RPT_Year AND RPT_No=@RPT_No AND TAG_ID=@TAG_ID" & vbNewLine
        SQL &= "    )" & vbNewLine
        SQL &= " ) INSP" & vbNewLine
        SQL &= "    ORDER BY INSP_ID" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rpt_INSP.DataSource = DT
        rpt_INSP.DataBind()
    End Sub

    Private Sub BindStatus()
        Dim SQL As String = "DECLARE @TAG_ID As INT =" & TAG_ID & vbNewLine
        SQL &= " DECLARE @INSP_ID As INT =" & INSP_ID & vbNewLine
        SQL &= " SELECT DISTINCT IST.STATUS_ID,IST.STATUS_Name, IST.STATUS_Order" & vbNewLine
        SQL &= " FROM MS_" & ViewState("TagClass").ToString & "_TAG " & vbNewLine
        SQL &= " LEFT JOIN MS_" & ViewState("TagClass").ToString & "_TAG_Inspection INSP ON MS_" & ViewState("TagClass").ToString & "_TAG.TAG_TYPE_ID=INSP.TAG_TYPE_ID" & vbNewLine
        SQL &= " LEFT JOIN MS_" & ViewState("TagClass").ToString & "_Default_Inspection_Status IST ON INSP.STATUS_ID=IST.STATUS_ID" & vbNewLine
        SQL &= " WHERE TAG_ID = @TAG_ID AND INSP_ID=@INSP_ID" & vbNewLine
        SQL &= " ORDER BY IST.STATUS_Order" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rpt_STATUS.DataSource = DT
        rpt_STATUS.DataBind()
    End Sub

    Private Sub BindLevel()

        Dim SQL As String = "SELECT * FROM ISPT_Class"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rpt_LEVEL.DataSource = DT
        rpt_LEVEL.DataBind()

    End Sub

    Private Sub BindLastDetail()
        Dim SQL As String = "DECLARE @RPT_Year As INT =" & RPT_Year & vbNewLine
        SQL &= " DECLARE @RPT_No As INT =" & RPT_No & vbNewLine
        SQL &= " DECLARE @TAG_ID As INT =" & TAG_ID & vbNewLine
        SQL &= " DECLARE @INSP_ID As INT =" & INSP_ID & vbNewLine
        SQL &= " DECLARE @PARTNO As varchar(100)='" & Replace(PARTNO, "'", "''") & "'" & vbNewLine

        SQL &= " SELECT DETAIL_ID,RPT_CODE,INSP_Name,ISNULL(CURRENT_STATUS_Name,'') STATUS_Name,CURRENT_LEVEL " & vbNewLine
        SQL &= " FROM VW_REPORT_" & ViewState("TagClass").ToString & "_DETAIL VW" & vbNewLine

        SQL &= " WHERE dbo.UDF_RPT_Code(RPT_Year,RPT_No)=" & vbNewLine
        SQL &= " (	SELECT MAX(dbo.UDF_RPT_Code(RPT_Year,RPT_No)) " & vbNewLine
        SQL &= " FROM VW_REPORT_" & ViewState("TagClass").ToString & "_DETAIL " & vbNewLine
        SQL &= " WHERE TAG_ID=@TAG_ID AND dbo.UDF_RPT_Code(RPT_Year,RPT_No)<dbo.UDF_RPT_Code(@RPT_Year,@RPT_No)" & vbNewLine
        SQL &= " ) AND INSP_ID=@INSP_ID AND TAG_ID=@TAG_ID --AND ISNULL(VW.CURRENT_COMPONENT,'')=@PARTNO" & vbNewLine
        SQL &= " ORDER BY STATUS_Name" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl_Last_Detail.Items.Clear()
        ddl_Last_Detail.Items.Add(New ListItem("", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Txt As String = DT.Rows(i).Item("INSP_Name") & " " & DT.Rows(i).Item("STATUS_Name")
            If Not IsDBNull(DT.Rows(i).Item("CURRENT_LEVEL")) Then
                Select Case CInt(DT.Rows(i).Item("CURRENT_LEVEL"))
                    Case 0
                        Txt &= IIf(INSP_ID = 12, " ZoneA", " Normal")
                    Case 1
                        Txt &= IIf(INSP_ID = 12, " ZoneB", " ClassC")
                    Case 2
                        Txt &= IIf(INSP_ID = 12, " ZoneC", " ClassB")
                    Case 3
                        Txt &= IIf(INSP_ID = 12, " ZoneD", " ClassA")
                End Select
            End If
            Dim Item As New ListItem(Txt, DT.Rows(i).Item("DETAIL_ID"))
            ddl_Last_Detail.Items.Add(Item)
        Next
    End Sub

    Protected Sub ddl_Tag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Tag.SelectedIndexChanged
        Dim SQL As String = "SELECT TAG_TYPE_ID,TAG_TYPE_Name FROM "
        SQL &= " VW_ALL_ACTIVE_" & ViewState("TagClass").ToString & "_TAG TAG "
        SQL &= " WHERE TAG.TAG_ID=" & TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 OrElse IsDBNull(DT.Rows(0).Item("TAG_TYPE_Name")) Then
            lbl_TagType.Text = ""
            lbl_TagType.Attributes("TAG_Type_ID") = 0
        Else
            lbl_TagType.Text = "(" & DT.Rows(0).Item("TAG_TYPE_Name") & ")"
            lbl_TagType.Attributes("TAG_Type_ID") = DT.Rows(0).Item("TAG_TYPE_ID")
        End If

        '------------ Relative Item--------
        INSP_ID = 0
    End Sub

    Protected Sub ddl_Part_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Part.SelectedIndexChanged
        If ddl_Part.SelectedIndex = ddl_Part.Items.Count - 1 Then
            txt_Part.Focus()
            txt_Part.Visible = True
        Else
            txt_Part.Text = ddl_Part.Items(ddl_Part.SelectedIndex).Text
            txt_Part.Visible = False
        End If
        '------------- Get Old-Last Detail Reference-----------
        LAST_DETAIL_ID = LAST_DETAIL_ID
    End Sub

    Protected Sub ddl_Last_Detail_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Last_Detail.SelectedIndexChanged
        '---------------------------- New Code -------------------------
        Select Case TAG_CLASS
            Case EIR_BL.Tag_Class.Stationary
                MY_PREVIEW1 = BL.Get_ST_Image(RPT_Year, RPT_No, DETAIL_ID, 1)
                MY_PREVIEW2 = BL.Get_ST_Image(RPT_Year, RPT_No, DETAIL_ID, 2)
            Case EIR_BL.Tag_Class.Rotating
                MY_PREVIEW1 = BL.Get_RO_Image(RPT_Year, RPT_No, DETAIL_ID, 1)
                MY_PREVIEW2 = BL.Get_RO_Image(RPT_Year, RPT_No, DETAIL_ID, 2)
        End Select

        If IsNothing(MY_PREVIEW1) Then
            Select Case TAG_CLASS
                Case EIR_BL.Tag_Class.Stationary
                    MY_PREVIEW1 = BL.Get_ST_Image(ddl_Last_Detail.Items(ddl_Last_Detail.SelectedIndex).Value, 1)
                Case EIR_BL.Tag_Class.Rotating
                    MY_PREVIEW1 = BL.Get_RO_Image(ddl_Last_Detail.Items(ddl_Last_Detail.SelectedIndex).Value, 1)
            End Select
        End If
        If IsNothing(MY_PREVIEW2) Then
            Select Case TAG_CLASS
                Case EIR_BL.Tag_Class.Stationary
                    MY_PREVIEW2 = BL.Get_ST_Image(ddl_Last_Detail.Items(ddl_Last_Detail.SelectedIndex).Value, 2)
                Case EIR_BL.Tag_Class.Rotating
                    MY_PREVIEW2 = BL.Get_RO_Image(ddl_Last_Detail.Items(ddl_Last_Detail.SelectedIndex).Value, 2)
            End Select
        End If

        pnlSolved.Visible = ddl_Last_Detail.SelectedIndex > 0

    End Sub

    Protected Sub lnk_Last_Detail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk_Last_Detail.Click

    End Sub

    Protected Sub rpt_INSP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_INSP.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnINSP As Button = e.Item.FindControl("btnINSP")
        btnINSP.Text = e.Item.DataItem("INSP_NAME")
        btnINSP.CssClass = "LevelDeselect"
        btnINSP.CommandArgument = e.Item.DataItem("INSP_ID")
    End Sub

    Protected Sub rpt_INSP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_INSP.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnINSP As Button = e.Item.FindControl("btnINSP")
        If INSP_ID <> btnINSP.CommandArgument Then INSP_ID = btnINSP.CommandArgument
    End Sub

    Protected Sub rpt_STATUS_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_STATUS.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnStatus As Button = e.Item.FindControl("btnStatus")
        btnStatus.Text = e.Item.DataItem("STATUS_Name")
        btnStatus.CssClass = "LevelDeselect"
        btnStatus.CommandArgument = e.Item.DataItem("STATUS_ID")

    End Sub

    Protected Sub rpt_STATUS_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_STATUS.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnStatus As Button = e.Item.FindControl("btnStatus")
        If STATUS_ID <> btnStatus.CommandArgument Then STATUS_ID = btnStatus.CommandArgument
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub rpt_LEVEL_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_LEVEL.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnLevel As Button = e.Item.FindControl("btnLevel")
        btnLevel.CommandArgument = e.Item.DataItem("ICLS_LEVEL")
        Select Case INSP_ID
            Case 12
                Select Case e.Item.DataItem("ICLS_LEVEL")
                    Case 0
                        btnLevel.Text = "ZoneA"
                    Case 1
                        btnLevel.Text = "ZoneB"
                    Case 2
                        btnLevel.Text = "ZoneC"
                    Case 3
                        btnLevel.Text = "ZoneD"
                End Select
            Case Else
                btnLevel.Text = e.Item.DataItem("ICLS_Description")
        End Select

    End Sub

    Protected Sub rpt_LEVEL_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_LEVEL.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnLevel As Button = e.Item.FindControl("btnLevel")
        If LEVEL_ID <> btnLevel.CommandArgument Then LEVEL_ID = btnLevel.CommandArgument
    End Sub

    Protected Sub btnSolveYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSolveYes.Click
        If Fixed = TriState.True Then
            Fixed = TriState.UseDefault
        Else
            Fixed = TriState.True
        End If
    End Sub

    Protected Sub btnSolveNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSolveNo.Click
        If Fixed = TriState.False Then
            Fixed = TriState.UseDefault
        Else
            Fixed = TriState.False
        End If
    End Sub

End Class