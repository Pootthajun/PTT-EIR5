﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GL_Dialog_SearchTagClass.aspx.vb" Inherits="EIR.GL_Dialog_SearchTagClass" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" >
<title>PTT-EIR :: Electronic-Inspection-Report</title>
	  	
	<link rel="icon" type="image/ico" href="resources/images/icons/Logo.ico" />
    <link rel="shortcut icon" href="resources/images/icons/Logo.ico" />
    <link rel="stylesheet" href="resources/css/style.css" type="text/css" media="screen" />
</head>

	  
<!-- Main Stylesheet -->
		
		
<!-- Custom Script -->
		<script type="text/javascript" src="js/PTTScript.js"></script>
<body style="background-image:None; background-color:#f5f5f5; font-size: 12px; font-family:Tahoma;">
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="scrip1" runat="server" />
    <h2 style="padding: 0px;">Detail for Report No. : <asp:Label ID="lblReportCode" runat="server" Font-Bold="true"></asp:Label> </h2>
    <h2 style="padding: 0px;">Raise By : <asp:Label ID="lblRaiseBy" runat="server" Font-Bold="true"></asp:Label> </h2>
 <table width="100%" border="0" cellspacing="2" cellpadding="4">
   <tr>
     <td height="18" align="center" bgcolor="#336699"><span style="color:White; font-weight:bold;"><b>Tag Code</b></span></td>
     <td height="18" align="center" bgcolor="#336699"><span style="color:White; font-weight:bold;"><b>Problem</b></span></td>
     <td height="18" align="center" bgcolor="#336699"><span style="color:White; font-weight:bold;"><b>Detail</b></span></td>
   </tr>
   <asp:Repeater ID="rptHistory" runat="server">
   <ItemTemplate>
       <tr id="tdTag" runat="server" bgcolor="#FFFFFF" style="cursor:pointer; border-bottom:solid 1px #eeeeee;" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
         <td height="18" style="border-bottom:solid 1px #eeeeee;"><asp:Label ID="lblTag" Font-Bold="True" runat="server"></asp:Label></td>
         <td height="18" style="border-bottom:solid 1px #eeeeee;" id="tdLevel" runat="server"><asp:Label ID="lblInspection" runat="server" Height="20px"></asp:Label></td>
         <td height="18" style="border-bottom:solid 1px #eeeeee;"><asp:Label ID="lblDetail" runat="server"></asp:Label></td>
       </tr>
   </ItemTemplate>
   </asp:Repeater>
   
 </table>
 
   </form>
</body>
</html>
