﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_SummerNote.ascx.vb" Inherits="EIR.UC_SummerNote" %>

<asp:UpdatePanel ID="UDP" runat="server">
    <ContentTemplate>

<div ID="RTE" runat="server" data-plugin="summernote" data-plugin-options='{"toolbar":[["style", ["bold", "italic", "underline", "clear"]],["color", ["color"]],["para", ["ul", "ol", "paragraph"]]]}'></div>
<asp:TextBox ID="TXT" TextMode="MultiLine" runat="server" Width="100%" Height="300px" style="display:none;" AutoPostBack="true"></asp:TextBox>
<asp:Label ID="lblView" runat="server" ></asp:Label>

</ContentTemplate>
</asp:UpdatePanel>
