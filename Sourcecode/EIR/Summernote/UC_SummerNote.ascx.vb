﻿Public Class UC_SummerNote
    Inherits System.Web.UI.UserControl

    Public Property HTML As String
        Get
            Return rte2html(TXT.Text)
        End Get
        Set(value As String) '---------- ส่ง html มา -------------
            TXT.Text = html2rte(value)
            UpdateRTE()
            lblView.Text = value
        End Set
    End Property

    Public Property Enabled As Boolean
        Get
            Return Not TXT.ReadOnly
        End Get
        Set(value As Boolean)
            TXT.ReadOnly = Not value
        End Set
    End Property

    Public Event onChange(ByRef Sender As UC_SummerNote)

    '--------------- ต้องใช้ Update Panel ครอบทั้งหน้า ----------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Enabled Then
            If Not IsPostBack Then
                SetFirstTimeScript()
            End If

            initScript()
            UpdateRTE()
            RaiseEvent onChange(Me)
        End If

        RTE.Visible = Enabled
        TXT.Visible = Enabled
        lblView.Visible = Not Enabled

    End Sub

    '--------------- ต้องใช้ Update Panel ครอบทั้งหน้า ----------------
    Private Sub SetFirstTimeScript()
        ScriptManager.RegisterOnSubmitStatement(Me, GetType(String), "SummerNote_" & Me.ClientID, "PrepareSummerNoteContent('" & TXT.ClientID & "'); ")
    End Sub

    Private Sub initScript()
        Dim Script As String = ""
        Script &= "$.components.register('summernote',{mode:'default',defaults:{height:300}});" & vbLf
        Script &= "$('#" & RTE.ClientID & "').summernote();" & vbLf
        Script &= "$('.note-editable').css('font-family', ""'Arial Unicode MS'"");" & vbLf

        '----------Config Change Event----------
        Script &= "addSummerNoteChangeEvent('" & UDP.ClientID & "','" & TXT.ClientID & "');"

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "initSummerNote_" & Me.ClientID, Script, True)
    End Sub

    Private Sub UpdateRTE()
        RTE.InnerHtml = rte2html(TXT.Text)
        lblView.Text = RTE.InnerHtml
    End Sub

#Region "For Rich Text Summernote ใน Database เป็น HTML , ใน txt เป็น เข้ารหัส , ใน RTE เป็น html"

    Public Function rte2html(ByVal Str As String) As String
        Return Str.Replace("[lt]", "<").Replace("[gt]", ">")
    End Function

    Public Function html2rte(ByVal Str As String) As String
        Return Str.Replace("<", "[lt]").Replace(">", "[gt]")
    End Function

    Private Sub TXT_TextChanged(sender As Object, e As EventArgs) Handles TXT.TextChanged
        RaiseEvent onChange(Me)
    End Sub

#End Region

End Class