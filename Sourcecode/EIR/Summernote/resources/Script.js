﻿function saveWithRTE() {
    var html = $('.note-editable').html();
    $('#PageContent_txtNote').text(replaceHTMLFromRTE(html));  // ชื่อ Textbox ที่มารับ
    document.getElementById('PageContent_btnSave').click();    // ชื่อ ปุ่ม Save   
}

function PrepareSummerNoteContent(TXT) {
    TXT = '#' + TXT;
    var rte = $('.note-editable').html();
    rte = html2rte(rte);
    $(TXT).text(rte);
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function rte2html(str) {
    return str.replaceAll("[lt]", "<").replaceAll("[gt]", ">");
}

function html2rte(str) {
    return str.replaceAll("<", "[lt]").replaceAll(">", "[gt]");
}

function addSummerNoteChangeEvent(UDP, TXT) {

    var RTE = $('#' + UDP + " > .note-editor > .note-editable");
    RTE.bind('DOMNodeInserted DOMSubtreeModified DOMNodeRemoved', function (event) {
        var _buffer = html2rte( RTE.html());
        $('#' + TXT).val(_buffer);
    });

    
}