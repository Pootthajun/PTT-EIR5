﻿Imports System.IO
Public Class Render_THM_File
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Select Case Request.QueryString("Mode").ToUpper
                Case "TAG"
                    Dim ServerMapPath As String = ConfigurationManager.AppSettings("ServerMapPath").ToString
                    Dim Folder As String = BL.PostedReport_Path & "\THM"
                    Dim FileName As String = Request.QueryString("File")
                    Dim ReportCode As String = FileName.Substring(0, 14)
                    Dim FilePath As String = Folder & "\" & ReportCode & "\" & FileName

                    Dim F As FileStream = File.Open(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                    Dim C As New Converter
                    Dim B As Byte() = C.StreamToByte(F)
                    F.Close()
                    Response.Clear()
                    Response.BinaryWrite(B)
                    Response.AddHeader("content-disposition", "attachment; filename=" + FileName & ".PDF")
                    Response.ContentType = "application/pdf"

            End Select
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

End Class