﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Stationary_OffRoutine_Edit2.aspx.vb" Inherits="EIR.Stationary_OffRoutine_Edit2" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="GL_DialogUploadImage.ascx" tagname="GL_DialogUploadImage" tagprefix="uc2" %>
<%@ Register src="GL_TagInspection.ascx" tagname="GL_TagInspection" tagprefix="uc1" %>
<%@ Register Src="UC_ST_Detail.ascx" TagPrefix="uc1" TagName="UC_ST_Detail" %>
<%@ Register Src="UC_Select_Template.ascx" TagName="UC_Select_Template" TagPrefix="TemplateSelector" %>
<%@ Register Src="UC_Template_1.ascx" TagPrefix="Template" TagName="UC_Template_1" %>
<%@ Register Src="UC_Template_2.ascx" TagPrefix="Template" TagName="UC_Template_2" %>
<%@ Register Src="UC_Template_3.ascx" TagPrefix="Template" TagName="UC_Template_3" %>
<%@ Register Src="UC_Template_4.ascx" TagPrefix="Template" TagName="UC_Template_4" %>
<%@ Register Src="UC_Template_5.ascx" TagPrefix="Template" TagName="UC_Template_5" %>
<%@ Register Src="UC_Template_6.ascx" TagPrefix="Template" TagName="UC_Template_6" %>
<%@ Register Src="UC_Template_7.ascx" TagPrefix="Template" TagName="UC_Template_7" %>
<%@ Register Src="UC_Template_8.ascx" TagPrefix="Template" TagName="UC_Template_8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <link href="Summernote/resources/bootstrap.css" rel="stylesheet" />
    <link href="Summernote/resources/font-awesome.min.css" rel="stylesheet" />
    <link href="Summernote/resources/summernote.css" rel="stylesheet" />

<asp:UpdatePanel ID="udp1" runat="server">
<ContentTemplate>
			<h2>Edit Stationary Off-Routine Report</h2>
			<asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" style="visibility:hidden;" Text=""></asp:TextBox>
			<asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" style="visibility:hidden;" />
            <div class="clear"></div> <!-- End .clear -->
		
		
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server" CssClass="default-tab current">Tag Status</asp:LinkButton></li>
                        <li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			
								<p style="font-weight:bold;">
								<label class="column-left" style="width:120px;" >Report for: </label>
								<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
								<asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Tag <asp:Label ID="lbl_TAG" runat="server" Text="Tag" CssClass="EditReportHeader"></asp:Label>
								</p>
							
                            <asp:Panel ID="pnlList" runat="server">

                            
							    <ul class="shortcut-buttons-set">
							        <li>
							        <asp:LinkButton ID="lnkUpload" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/image_add_48.png" alt="icon" /><br />
							            Add Inspection
							            </span>
							        </asp:LinkButton>
							        </li>
							        <li>
							        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button">
							        <span> 
							            <img src="resources/images/icons/cross_48.png" alt="icon" /><br />
							            Clear all
							            </span>
							        </asp:LinkButton>
							            <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                            runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete all inspection for this report permanently?">
                                        </cc1:ConfirmButtonExtender>
							        </li>
							 
								    <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								    </li>
					            </ul>
							  <asp:Panel ID="pnlRemain" runat="server">
							   <table cellpadding="0" cellspacing="0" class="table_inspection">
								   <thead>
                                    <tr>
                                        <th colspan="8" style="background-color:#EEEEEE; vertical-align:middle;"><h3>Remaining <asp:Label ID="lblTotalOld" runat="server"></asp:Label> and <asp:Label ID="lblTotalUpdate" runat="server"></asp:Label></h3></th>
                                        <th style="background-color:#EEEEEE; vertical-align:middle; width:50px;">
                                            <img src="resources/images/space.png" width="16" />
                                            <asp:ImageButton ID="btnClearOld" runat="server" ImageUrl="~/resources/images/icons/cross.png" ToolTip="Clear all updated from this report" />
                                            <cc1:ConfirmButtonExtender ID="confirmClearRemain" runat="server" TargetControlID="btnClearOld" ConfirmText="Are you sure to reset remaining problem permanently?" />
                                        </th>
                                    </tr>
                                    <tr>
                                      <th>Inspection Point</th>
                                      <th>Problem</th>                                      
                                      <th>Previous</th>
                                      <th>Fixed</th>
                                      <th>Current</th>
                                      <th>Trace</th>
                                      <th>Detail</th>
                                      <th>Attachment(s)</th>
                                      <th>Action</th>
                                    </tr>
								    </thead>
                                   <tbody>
                                    <asp:Repeater ID="rptRemain" runat="server">
                                        <ItemTemplate>                                 
                                        <tr>
                                            <td style="text-align:left;">
                                                <asp:Image ID="imgUpdateYes" runat="server" ImageUrl="resources/images/icons/hand_point.png" />
                                                <asp:Image ID="imgUpdateNo" runat="server" ImageUrl="resources/images/space.png" Width="16px" Height="16px" />
                                                <asp:label ID="lbl_Inspection" runat="server"></asp:label>
                                            </td>
                                            <td><asp:label ID="lbl_Problem" runat="server"></asp:label></td>
                                            <td id="tdLastLevel" runat="server"><asp:label ID="lbl_LastLevel" runat="server"></asp:label></td>
                                            <td><asp:label ID="lbl_Fixed" runat="server"></asp:label></td>
                                            <td id="tdCurrentLevel" runat="server"><asp:label ID="lbl_CurrentLevel" runat="server"></asp:label></td>
                                            <td><asp:label ID="lbl_Trace" runat="server" Font-Italic="true"></asp:label></td>
                                            <td style="text-align:left;"><asp:label ID="lbl_Detail" runat="server"></asp:label></td>
                                            <td><asp:label ID="lbl_Doc" runat="server"></asp:label></td>                                            
                                            <td style="text-align:left;">
                                                <asp:ImageButton ID="btnEdit" runat="server" CommandName="Edit" ImageUrl="resources/images/icons/pencil.png" ToolTip="Upload/edit more detail" />
                                                <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" ImageUrl="resources/images/icons/cross.png" ToolTip="Clear updated detail from this report" />
                                                <cc1:ConfirmButtonExtender ID="confirm" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete information for this inspection?" />
                                            </td>
                                        </tr>                                    
                                        </ItemTemplate>
                                    </asp:Repeater>                                  
                                   </tbody>
								</table>
							  </asp:Panel>
                              <asp:Panel ID="pnlNew" runat="server">                              
                                <table width="100%" cellpadding="0" cellspacing="0" class="table_inspection">
								   <thead>
                                    <tr>
                                        <th colspan="6" style="background-color:#EEEEEE; vertical-align:middle;"><h3>New problem(s) found (<asp:Label ID="lblTotalNew" runat="server"></asp:Label>)</h3></th>
                                        <th style="background-color:#EEEEEE; vertical-align:middle; width:50px;">
                                            <asp:ImageButton ID="btnAddNew" runat="server" ImageUrl="~/resources/images/icons/add.png" ToolTip="Add new problem" />
                                            <asp:ImageButton ID="btnClearNew" runat="server" ImageUrl="~/resources/images/icons/cross.png" ToolTip="Clear all new problem(s)" />
                                            <cc1:ConfirmButtonExtender ID="confirmClearNew" runat="server" TargetControlID="btnClearNew" ConfirmText="Are you sure to clear all new problem permanently?" />
                                        </th>
                                    </tr>
                                    <tr>
                                      <th>Inspection Point</th>
                                      <th>Problem</th>
                                      <th>Current Level</th>
                                      <th>Trace</th>
                                      <th>Detail</th>
                                      <th>Attachment(s)</th>
                                      <th>Action</th>
                                    </tr>
								    </thead>
                                   <tbody>
                                    <asp:Repeater ID="rptNew" runat="server">
                                        <ItemTemplate>                                 
                                        <tr>
                                            <td style="text-align:left;">
                                                <img src="resources/images/icons/hand_point.png" Width="16" Height="16" />
                                                <asp:label ID="lbl_Inspection" runat="server"></asp:label>
                                            </td>
                                            <td><asp:label ID="lbl_Problem" runat="server"></asp:label></td>
                                            <td id="tdCurrentLevel" runat="server"><asp:label ID="lbl_CurrentLevel" runat="server"></asp:label></td>
                                            <td><asp:label ID="lbl_Trace" runat="server"></asp:label></td>                                            
                                            <td style="text-align:left;"><asp:label ID="lbl_Detail" runat="server"></asp:label></td>
                                            <td><asp:label ID="lbl_Doc" runat="server"></asp:label></td>
                                            <td>
                                                <asp:ImageButton ID="btnEdit" runat="server" CommandName="Edit" ImageUrl="resources/images/icons/pencil.png" ToolTip="Upload/edit more detail" />
                                                <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" ImageUrl="resources/images/icons/cross.png" ToolTip="Remove this problem" />
                                                <cc1:ConfirmButtonExtender ID="confirm" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this inspection permanently?" />
                                            </td>
                                        </tr>                              
                                        </ItemTemplate>
                                    </asp:Repeater>          
                                   </tbody>
								</table>
                              </asp:Panel>	
							    <p align="right">
								    <asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
								    <asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
							    </p>
																
						    </asp:Panel>
                          <asp:Panel ID="pnlEdit" runat="server" style=" padding-top:20px;">
                              <uc1:UC_ST_Detail runat="server" id="UC_Detail" SaveButton="false" CloseButton="false" />

                              <h3>Attachment : <asp:Label ID="lblTotalDoc" runat="server"></asp:Label></h3>

                              <asp:Panel ID="pnlDoc" runat="server" Style="width: 90%; text-align: right; padding-left:50px; ">
                                    <asp:Repeater ID="rptDoc" runat="server">
                                        <ItemTemplate>
                                            <table border="1" cellspacing="0" cellpadding="5" style="background-color: gray;" align="right">
                                                <tr>
                                                    <td style="font-weight: bold; text-align: right; font-size: 18px; color: white; width: 50%;">
                                                        <asp:Label ID="lblNo" runat="server"></asp:Label>.                       
                                                    </td>
                                                    <td style="text-align: right">
                                                        <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/cross_48.png" Width="32" Height="32" />
                                                        <cc1:ConfirmButtonExtender ID="cfbDelete" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this section permanently?" />
                                                    </td>
                                                </tr>
                                                <asp:Panel ID="pnlPage" runat="server" Style="background-color: gray;">
                                                    <Template:UC_Template_1 runat="server" id="Template_1" Visible="false" />   
                                                    <Template:UC_Template_2 runat="server" id="Template_2" Visible="false" />
                                                    <Template:UC_Template_3 runat="server" id="Template_3" Visible="false" />
                                                    <Template:UC_Template_4 runat="server" id="Template_4" Visible="false" />
                                                    <Template:UC_Template_5 runat="server" id="Template_5" Visible="false" />
                                                    <Template:UC_Template_6 runat="server" id="Template_6" Visible="false" />
                                                    <Template:UC_Template_7 runat="server" id="Template_7" Visible="false" />
                                                    <Template:UC_Template_8 runat="server" id="Template_8" Visible="false" />
                                                </asp:Panel>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                    <table style="width: 100%; border: 0px; padding: 0; border-spacing: 0; margin-top: 20px;">
                                        <tr>
                                            <td style="text-align: right">
                                                <asp:LinkButton ID="lnkAddSection" runat="server">
				                                    <span>
					                                    <img src="resources/images/icons/icon_arrow_down.jpg" alt="Add Section" width="32" height="32" />
				                                    </span>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>

                                    <TemplateSelector:UC_Select_Template ID="TemplateSelector" runat="server" />

                                    <%--=========================Template  END==================================--%>
                            </asp:Panel>

                              <p align="right">
								    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Back" />
								    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
							  </p>
                          </asp:Panel>
				    </div>

		          
		       	 <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
		         
	              
			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			

</ContentTemplate>
</asp:UpdatePanel>  
    
<script src="Summernote/resources/bootstrap.js"></script>
<script src="Summernote/resources/core.js"></script>
<script src="Summernote/resources/summernote.js"></script>
<script src="Summernote/resources/Script.js"></script>
    	
</asp:Content>
