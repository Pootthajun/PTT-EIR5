﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="THM_Routine_Edit2.aspx.vb" Inherits="EIR.THM_Routine_Edit2" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style type="text/css">
    .ValueMapingTable
    {
    	width:100%;
    	font-size:10px;    	
    	}
    .ValueMapingTable tr td
    {
    	padding:3px;
    	text-align:center;
    	border:1px solid #ccc;
    	}

        .tbTag tr td {
            border-bottom:1px solid #efefef;
        }

         .btnBrowse {
            border-radius:2px;
            border:1px solid #eee;
            background-color:cornflowerblue;
            color:white;
            cursor:pointer;
        }


        .btnDeselect {
            border-radius:2px;
            border:1px solid #eee;
            background-color:#eee;
            color:white;
            width:40px;
            cursor:pointer;
        }

        .btnNA {
            border-radius:2px;
            border:1px solid #ccc;
            background-color:darkgray;
            color:white;
            width:40px;
            cursor:pointer;
        }
        .btnOK {
            border-radius:2px;
            border:1px solid green;
            background-color:green;
            color:white;
            width:40px;
            cursor:pointer;
        }
        .btnAB{
            border-radius:2px;
            border:1px solid red;
            background-color:red;
            color:white;
            width:40px;
            cursor:pointer;
        }
</style>

<asp:UpdatePanel ID="udp1" runat="server">   
<ContentTemplate>

	    
			<h2>Edit Thermography Report</h2>
			
            <div class="clear"></div> <!-- End .clear -->		
		
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server" CssClass="default-tab current">Report Detail</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
                  
				  <div class="tab-content current">
				  			<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->								
								<p style="font-weight:bold; font-size:16px;">
								    <label class="column-left" style="width:120px; font-size:16px;" >Report for: </label>
								    <asp:Label ID="lbl_Type" runat="server" Text="Type" CssClass="EditReportHeader" ></asp:Label>
								    | <asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader" ></asp:Label>
								    <asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader" ></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>
								    | Round <asp:Label ID="lbl_Round" runat="server" Text="Round" CssClass="EditReportHeader"></asp:Label>
								    | Period <asp:Label ID="lbl_Period" runat="server" Text="Period" CssClass="EditReportHeader" ></asp:Label>
							    </p>
								
								<ul class="shortcut-buttons-set">
							      <li>
							        <asp:LinkButton ID="lnkClear" runat="server" CssClass="shortcut-button"> <span><img src="resources/images/icons/cross_48.png" alt="icon" /><br />Clear </span></asp:LinkButton>
							      </li>
							      <cc1:ConfirmButtonExtender ID="lnkClear_ConfirmButtonExtender" 
                                          runat="server" Enabled="True" TargetControlID="lnkClear" ConfirmText="Are you sure to delete current information permanently?">
                                    </cc1:ConfirmButtonExtender>
								 						  
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button"> <span><img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />Preview report </span></asp:LinkButton>
								  </li>
					        </ul>	
					            <table>
					                <tr>
                                        <td height="25" style="text-align:Left; width:100px;">
                                            Inspected Date
                                        </td>
                                        <td height="25" style="text-align:Left;">
                                            <asp:TextBox style="text-align:center;" runat="server" ID="txtInspectedDate" CssClass="text-input small-input" MaxLength="20" Width="120px"></asp:TextBox>
		                                    <cc1:CalendarExtender ID="txtInspectedDate_CalendarExtender" runat="server" 
                                             Format="yyyy-MM-dd" TargetControlID="txtInspectedDate" PopupPosition="Right" >
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td></td>             
                                     </tr> 
					            </table>
					            <table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="tbTag">
								   <thead>
                                    <tr>
                                      <th bgcolor="#EEEEEE" style="text-align:Left; width:30px;">No</th>
                                      <th bgcolor="#EEEEEE" style="text-align:Left;">Tag Code</th>
                                      <th bgcolor="#EEEEEE" style="text-align:Left;">Tag Name</th>
                                      <th bgcolor="#EEEEEE" style="text-align:center;">Action</th>
                                      <th bgcolor="#EEEEEE" style="text-align:center;">Uploaded Report</th>
                                      <th bgcolor="#EEEEEE" style="text-align:center; width:40px;"></th>
								    </tr>
								    </thead>							    
								    <asp:Repeater ID="rptTag" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                                    <asp:Label ID="lblDetailID" runat="server" style="display:none;"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTagCode" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTagName" runat="server"></asp:Label>
                                                </td>
                                                <td style="text-align:center;">
                                                    <asp:Button ID="btnNA" runat="server" CssClass="btnDeselect" Text="N/A" Width="40px" CommandName="NA" />
                                                    <asp:Button ID="btnOK" runat="server" CssClass="btnDeselect" Text="OK" Width="40" CommandName="OK"/>
                                                    <asp:Button ID="btnAB" runat="server" CssClass="btnDeselect" Text="AB" Width="40" CommandName="AB"/>   
                                                </td> 
                                                <td>
                                                    <a id="aView" target="_blank" runat="server"></a>&nbsp;
                                                </td>
                                                <td>                                                    
                                                    <asp:UpdatePanel ID="udpFile" runat="server">
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnBrowse" />       
                                                        </Triggers>
                                                        <ContentTemplate>
                                                        <a href="javascript:;" id="aUpload" runat="server"><img alt="Choose file to upload" border="0" src="resources/images/icons/pencil.png" height="16" width="16" /></a>
                                                        <asp:FileUpload ID="ful" runat="server" style="display:none;"/>
                                                        <asp:Button ID="btnBrowse" runat="server" CommandName="Upload" style="display:none;"/>

                                                            <asp:ImageButton ID="btnDel" CommandName="Delete" runat="server" ToolTip="Delete information" ImageUrl="resources/images/icons/cross.png" Height="16px" Width="16px" />
                                                            <cc1:ConfirmButtonExtender ID="btnDel_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDel">
                                                            </cc1:ConfirmButtonExtender>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                    
                                                </td>                                                                           
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
								</table>							
                            </fieldset>
                            <p align="right">
								<asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
								<asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
							</p>                       
				    </div>
		       	 <asp:Panel ID="pnlValidation" runat="server" CssClass="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>	         
	              
			  </div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			
</ContentTemplate>
</asp:UpdatePanel>  
	 
</asp:Content>

