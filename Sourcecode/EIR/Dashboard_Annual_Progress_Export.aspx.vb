﻿Public Class Dashboard_Annual_Progress_Export
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim Year As Integer = Request.QueryString("Year")
            Dim Equipment As Integer = Request.QueryString("Equipment")
            Dim Plant As Integer = Request.QueryString("Plant")
            Dim Plant_Name As String = Request.QueryString("Plant_Name")
            UC_Dashboard_Annual_Progress.BindData(Year, Equipment, Plant, Plant_Name)
        End If
    End Sub

End Class