﻿Imports System.Data
Imports System.Data.SqlClient
Public Class ST_TA_Inspection_Edit4
    Inherits System.Web.UI.Page
    Dim C As New Converter
    Dim BL As New EIR_BL
    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private Property PLANT_ID() As Integer
        Get
            If IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
        End Set
    End Property

    Private Property TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property


    Private Property TAG_TYPE_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_TYPE_ID")) Then
                Return ViewState("TAG_TYPE_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_TYPE_ID") = value
        End Set
    End Property


    Private Property DETAIL_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_ID")) Then
                Return ViewState("DETAIL_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '---------------Hide Dialog------------------
        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            BL.BindDDl_RPT_ST_TA_Tag(ddl_Tag, RPT_Year, RPT_No)

            BL.BindDDl_RPT_ST_TA_Tag(ddl_Search_Tag, RPT_Year, RPT_No)
            BL.BindDDl_RPT_ST_TA_INSP_ID(ddl_Search_Components, RPT_Year, RPT_No)
            BL.BindDDl_RPT_ST_TA_INSP_STATUS_ID(ddl_Search_Condition, RPT_Year, RPT_No)
            BL.BindDDl_RPT_ST_TA_Class(ddl_Search_Class)


            BindHeader()
            BindList()

            HTabSummary_List.Attributes("class") = "default-tab current"
            HTabCheck_sheet.Attributes("class") = ""
            pnlList.Visible = True
            pnlCheckList.Visible = False

            pnlValidation.Visible = False


        End If
    End Sub

#Region "Navigator"




    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click

        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit1.aspx?" & Param & "';", True)

    End Sub

    Protected Sub HTabHTabDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDetail.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit2.aspx?" & Param & "';", True)
    End Sub


    Private Sub HTabAsFound_Click(sender As Object, e As EventArgs) Handles HTabAsFound.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.As_Found
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub

    Private Sub HTabAfterClean_Click(sender As Object, e As EventArgs) Handles HTabAfterClean.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.After_Clean
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub

    Private Sub HTabNDE_Click(sender As Object, e As EventArgs) Handles HTabNDE.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.NDE
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub

    Private Sub HTabRepair_Click(sender As Object, e As EventArgs) Handles HTabRepair.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.Repair
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub

    Private Sub HTabAfterRepair_Click(sender As Object, e As EventArgs) Handles HTabAfterRepair.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.After_Repair
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub

    Private Sub HTabFinal_Click(sender As Object, e As EventArgs) Handles HTabFinal.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & EIR_BL.ST_TA_STEP.Final
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Inspection_Edit3.aspx?" & Param & "';", True)
    End Sub


    Private Sub HTabSummary_List_Click(sender As Object, e As EventArgs) Handles HTabSummary_List.Click

        HTabSummary_List.Attributes("class") = "default-tab current"
        HTabCheck_sheet.Attributes("class") = ""
        pnlList.Visible = True
        pnlCheckList.Visible = False
        BindList()
        'BindCheckSheet()
    End Sub

    Private Sub HTabCheck_sheet_Click(sender As Object, e As EventArgs) Handles HTabCheck_sheet.Click

        HTabSummary_List.Attributes("class") = ""
        HTabCheck_sheet.Attributes("class") = "default-tab current"
        pnlList.Visible = False
        pnlCheckList.Visible = True
        'BindList()
        BindCheckSheet()
    End Sub


#End Region



    Private Sub BindHeader()

        '--Page--
        Dim SQL As String = ""
        SQL &= "    Select TAG_ID,TAG_CODE,TAG_No,TAG_TYPE_ID,TAG_Name,TAG_TYPE_Name FROM VW_ST_TA_TAG " & vbNewLine
        SQL &= "        WHERE   TAG_ID = " & TAG_ID & " And " & vbNewLine
        SQL &= "        TAG_TYPE_ID = " & TAG_TYPE_ID & "  " & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        '------------------------------Report For -----------------------------------
        SQL = " Select * FROM VW_REPORT_ST_TA_HEADER WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("ST_TA_Inspection_Summary.aspx")
            Exit Sub
        End If
        PLANT_ID = DT.Rows(0).Item("PLANT_ID")
        TAG_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")
        lbl_Plant_Edit.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Equipment_Edit.Text = DT.Rows(0).Item("TAG_TYPE_NAME")
        lbl_Year_Edit.Text = RPT_Year



    End Sub

    Private Sub BindList_rpt()

        Dim lblRPT_Date_AsFound As String = ""
        Dim lblRPT_Date_AfterClean As String = ""
        Dim lblRPT_Date_NDE As String = ""
        Dim lblRPT_Date_Repair As String = ""
        Dim lblRPT_Date_AfterRepair As String = ""
        Dim lblRPT_Date_Final As String = ""


        Dim DT_List As DataTable = Session("DT_List")
        DT_List.Columns.Add("AsFound")
        DT_List.Columns.Add("AfterClean")
        DT_List.Columns.Add("NDE")
        DT_List.Columns.Add("Repair")
        DT_List.Columns.Add("AfterRepair")
        DT_List.Columns.Add("Final")

        DT_List.Columns.Add("Class_AsFound")
        DT_List.Columns.Add("Class_AfterClean")
        DT_List.Columns.Add("Class_NDE")
        DT_List.Columns.Add("Class_Repair")
        DT_List.Columns.Add("Class_AfterRepair")
        DT_List.Columns.Add("Class_Final")


        Dim DR As DataRow
        For j As Integer = 0 To DT_List.Rows.Count - 1
            DR = DT_List.Rows(j)
            Dim DT_Date_Step As DataTable = DT_Daily_Date(RPT_Year, RPT_No, DT_List.Rows(j).Item("TAG_ID"), DT_List.Rows(j).Item("INSP_ID"), DT_List.Rows(j).Item("INSP_STATUS_ID"))

            If (DT_Date_Step.Rows.Count > 0) Then
                For i As Integer = 0 To DT_Date_Step.Rows.Count - 1

                    Select Case DT_Date_Step.Rows(i).Item("STEP_ID")
                        Case EIR_BL.ST_TA_STEP.As_Found
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_AsFound += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                            End If
                        Case EIR_BL.ST_TA_STEP.After_Clean
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_AfterClean += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                            End If
                        Case EIR_BL.ST_TA_STEP.NDE
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_NDE += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                            End If
                        Case EIR_BL.ST_TA_STEP.Repair
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_Repair += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                            End If
                        Case EIR_BL.ST_TA_STEP.After_Repair
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_AfterRepair += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                            End If
                        Case EIR_BL.ST_TA_STEP.Final
                            If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                                lblRPT_Date_Final += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                            End If

                    End Select


                Next

                DR("AsFound") = lblRPT_Date_AsFound
                DR("AfterClean") = lblRPT_Date_AfterClean
                DR("NDE") = lblRPT_Date_NDE
                DR("Repair") = lblRPT_Date_Repair
                DR("AfterRepair") = lblRPT_Date_AfterRepair
                DR("Final") = lblRPT_Date_Final

                '---Class--
                'DR("Class_AsFound")
                'DR("Class_AfterClean")
                'DR("Class_NDE")
                'DR("Class_Repair")
                'DR("Class_AfterRepair")
                'DR("Class_Final")


                For i As Integer = 1 To 6
                    DT_Date_Step.DefaultView.RowFilter = "STEP_ID=" & i & " AND STATUS_ID IS NOT NULL AND RPT_DATE IS NOT NULL "
                    Dim DT_RowFilter = DT_Date_Step.DefaultView.ToTable()
                    If (DT_Date_Step.DefaultView.Count > 0) Then

                        Dim Max_CLASS As Integer = DT_Date_Step.DefaultView(0).Item("STATUS_ID") '-- สถานะล่าสุด

                        Select Case i
                            Case EIR_BL.ST_TA_STEP.As_Found
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_AsFound") = "white"

                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_AsFound") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_AsFound") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_AsFound") = "red"
                                    End If
                                Else
                                    DR("Class_AsFound") = "white"
                                End If
                            Case EIR_BL.ST_TA_STEP.After_Clean
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_AfterClean") = "white"

                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_AfterClean") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_AfterClean") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_AfterClean") = "red"
                                    End If
                                Else
                                    DR("Class_AfterClean") = "white"
                                End If
                            Case EIR_BL.ST_TA_STEP.NDE
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_NDE") = "white"

                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_NDE") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_NDE") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_NDE") = "red"
                                    End If
                                Else
                                    DR("Class_NDE") = "white"
                                End If
                            Case EIR_BL.ST_TA_STEP.Repair
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_Repair") = "white"

                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_Repair") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_Repair") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_Repair") = "red"
                                    End If
                                Else
                                    DR("Class_Repair") = "white"
                                End If
                            Case EIR_BL.ST_TA_STEP.After_Repair
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_AfterRepair") = "white"

                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_AfterRepair") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_AfterRepair") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_AfterRepair") = "red"
                                    End If
                                Else
                                    DR("Class_AfterRepair") = "white"
                                End If
                            Case EIR_BL.ST_TA_STEP.Final
                                If Not IsDBNull(Max_CLASS) Then
                                    If Max_CLASS = 0 Then
                                        DR("Class_Final") = "white"
                                    ElseIf (Max_CLASS = 1) Then
                                        DR("Class_Final") = "yellow"

                                    ElseIf (Max_CLASS = 2) Then
                                        DR("Class_Final") = "Orange"
                                    ElseIf (Max_CLASS = 3) Then
                                        DR("Class_Final") = "red"
                                    End If
                                Else
                                    DR("Class_Final") = "white"
                                End If

                        End Select


                    End If
                Next



            End If

        Next


    End Sub


    Private Sub BindList()
        Dim SQL As String = ""
        SQL &= "  					DECLARE @RPT_Year As integer = " & RPT_Year & "																																						" & vbNewLine
        SQL &= "  					DECLARE @RPT_No As integer = " & RPT_No & "																																							" & vbNewLine
        SQL &= "           			Select DISTINCT																																										" & vbNewLine
        SQL &= "      				Detail_Step.DETAIL_ID																																									" & vbNewLine
        SQL &= "      				--,Detail_Step.DETAIL_STEP_ID																																								" & vbNewLine
        SQL &= "      				,Detail_Step.TAG_ID																																										" & vbNewLine
        SQL &= "      				,Detail_Step.INSP_ID																																									" & vbNewLine
        SQL &= "      				,INSP.INSP_Name																																											" & vbNewLine
        SQL &= "      				,Detail_Step.INSP_STATUS_ID																																								" & vbNewLine
        SQL &= "      				,INSP.STATUS_Name																																										" & vbNewLine
        SQL &= "      				,Detail.Tag_Type_ID																																										" & vbNewLine
        SQL &= "      				,MS_ST_TAG_TYPE.TAG_TYPE_Name																																							" & vbNewLine
        SQL &= "      				,VW_ST_TA_TAG.TAG_CODE ,VW_ST_TA_TAG.TAG_Name																																			" & vbNewLine
        SQL &= "           			FROM																																												" & vbNewLine
        SQL &= "      				RPT_ST_TA_Detail_Step  AS Detail_Step  																																					" & vbNewLine
        SQL &= "      				LEFT JOIN 																																												" & vbNewLine
        SQL &= "      				 (                                                      																																" & vbNewLine
        SQL &= "                    	    Select MS_ST_TAG_Inspection.* , 'MS_ST_TAG_Inspection' To_Table ,MS_ST_Default_Inspection.INSP_Name       ,	MS_ST_Default_Inspection_Status.STATUS_Name       					" & vbNewLine
        SQL &= "                    	    FROM MS_ST_TAG_Inspection                                                                                        																" & vbNewLine
        SQL &= "                    	    LEFT JOIN MS_ST_Default_Inspection On MS_ST_Default_Inspection.INSP_ID = MS_ST_TAG_Inspection.INSP_ID            																" & vbNewLine
        SQL &= "                    	    LEFT JOIN MS_ST_Default_Inspection_Status On MS_ST_Default_Inspection_Status.STATUS_ID = MS_ST_TAG_Inspection.STATUS_ID          												" & vbNewLine
        SQL &= "                    	   																																													" & vbNewLine
        'SQL &= "      				    UNION ALL                                                                                                        																	" & vbNewLine
        'SQL &= "                          Select  MS_ST_TA_TAG_Inspection.* ,'MS_ST_TA_TAG_Inspection' To_Table ,MS_ST_TA_Default_Inspection.INSP_Name ,	MS_ST_TA_Default_Inspection_Status.STATUS_Name					" & vbNewLine
        'SQL &= "                    	    FROM MS_ST_TA_TAG_Inspection                                                                                     																" & vbNewLine
        'SQL &= "                    	    LEFT JOIN MS_ST_TA_Default_Inspection On MS_ST_TA_Default_Inspection.INSP_ID = MS_ST_TA_TAG_Inspection.INSP_ID   																" & vbNewLine
        'SQL &= "                    	    LEFT JOIN MS_ST_TA_Default_Inspection_Status On MS_ST_TA_Default_Inspection_Status.STATUS_ID = MS_ST_TA_TAG_Inspection.STATUS_ID          										" & vbNewLine
        SQL &= "                        ) AS INSP  																																										" & vbNewLine
        SQL &= "           			 ON  Detail_Step.INSP_ID = INSP.INSP_ID AND  Detail_Step.INSP_STATUS_ID = INSP.STATUS_ID 																							" & vbNewLine
        SQL &= "      				 LEFT JOIN RPT_ST_TA_DETAIL Detail ON  Detail.DETAIL_ID = Detail_Step.DETAIL_ID																											" & vbNewLine
        SQL &= "      				 LEFT JOIN MS_ST_TA_Step ON  MS_ST_TA_Step.STEP_ID = Detail_Step.STEP_ID																												" & vbNewLine
        SQL &= "      				 LEFT JOIN MS_ST_TAG_TYPE ON  MS_ST_TAG_TYPE.TAG_TYPE_ID = Detail.TAG_TYPE_ID																											" & vbNewLine
        SQL &= "      				 LEFT JOIN VW_ST_TA_TAG On VW_ST_TA_TAG.TAG_ID=Detail.TAG_ID And VW_ST_TA_TAG.TAG_TYPE_ID=Detail.TAG_TYPE_ID																			" & vbNewLine
        SQL &= "  																																																			" & vbNewLine
        SQL &= "      	WHERE 	Detail_Step.RPT_Year =@RPT_Year AND Detail_Step.RPT_No =@RPT_No																																" & vbNewLine

        Dim Filter As String = ""
        If ddl_Search_Tag.SelectedIndex > 0 Then
            Filter &= " AND Detail.TAG_ID=" & ddl_Search_Tag.Items(ddl_Search_Tag.SelectedIndex).Value & "   " & vbNewLine
        End If

        If txt_Search_Code.Text <> "" Then
            Filter &= " AND  VW_ST_TA_TAG.TAG_CODE LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%'   " & vbNewLine
        End If


        If ddl_Search_Components.SelectedIndex > 0 Then
            Filter &= " AND Detail_Step.INSP_ID=" & ddl_Search_Components.Items(ddl_Search_Components.SelectedIndex).Value & "   " & vbNewLine
        End If

        If ddl_Search_Condition.SelectedIndex > 0 Then
            Filter &= " AND Detail_Step.INSP_STATUS_ID=" & ddl_Search_Condition.Items(ddl_Search_Condition.SelectedIndex).Value & "   " & vbNewLine
        End If
        If Filter <> "" Then
            SQL &= Filter
        End If

        SQL &= "         ORDER BY TAG_ID,INSP.INSP_Name,INSP.STATUS_Name, INSP_ID																																			" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Session("DT_List") = DT
        rptList.DataSource = DT
        rptList.DataBind()

    End Sub

    Dim LastTag As String = ""
    Dim LastComponents As String = ""
    Dim LastStatus As String = ""
    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound

        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblComponents As Label = e.Item.FindControl("lblComponents")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        Dim lblRPT_Date_AsFound As Label = e.Item.FindControl("lblRPT_Date_AsFound")
        Dim lblRPT_Date_AfterClean As Label = e.Item.FindControl("lblRPT_Date_AfterClean")
        Dim lblRPT_Date_NDE As Label = e.Item.FindControl("lblRPT_Date_NDE")
        Dim lblRPT_Date_Repair As Label = e.Item.FindControl("lblRPT_Date_Repair")
        Dim lblRPT_Date_AfterRepair As Label = e.Item.FindControl("lblRPT_Date_AfterRepair")
        Dim lblRPT_Date_Final As Label = e.Item.FindControl("lblRPT_Date_Final")

        Dim trTag As HtmlTableRow = e.Item.FindControl("trTag")
        Dim DuplicatedStyleTop As String = "border-top:none;"
        Dim tdComponents As HtmlTableCell = e.Item.FindControl("tdComponents")
        Dim tdStatus As HtmlTableCell = e.Item.FindControl("tdStatus")

        Dim Tag As String = e.Item.DataItem("TAG_CODE").ToString() & " : " & e.Item.DataItem("TAG_Name").ToString()

        '--------LastStep------------
        If LastTag <> Tag Then
            LastTag = Tag
            lblTag.Text = LastTag
            trTag.Visible = True
            LastComponents = ""
        Else
            trTag.Visible = False
        End If
        '--------LastComponents------------
        If LastComponents <> e.Item.DataItem("INSP_Name").ToString() & Tag Then
            LastComponents = e.Item.DataItem("INSP_Name").ToString() & Tag
            lblComponents.Text = e.Item.DataItem("INSP_Name").ToString()
            LastStatus = ""
        Else
            tdComponents.Attributes("style") &= DuplicatedStyleTop
        End If

        '--------LastStatus------------
        If LastStatus <> e.Item.DataItem("STATUS_Name").ToString & e.Item.DataItem("INSP_Name").ToString() & Tag Then
            LastStatus = e.Item.DataItem("STATUS_Name").ToString & e.Item.DataItem("INSP_Name").ToString() & Tag
            lblStatus.Text = e.Item.DataItem("STATUS_Name").ToString
        Else
            lblStatus.Text = ""
            tdStatus.Attributes("style") &= DuplicatedStyleTop
        End If



        Dim DT_Date_Step As DataTable = DT_Daily_Date(RPT_Year, RPT_No, e.Item.DataItem("TAG_ID"), e.Item.DataItem("INSP_ID"), e.Item.DataItem("INSP_STATUS_ID"))
        If (DT_Date_Step.Rows.Count > 0) Then
            For i As Integer = 0 To DT_Date_Step.Rows.Count - 1

                Select Case DT_Date_Step.Rows(i).Item("STEP_ID")
                    Case EIR_BL.ST_TA_STEP.As_Found
                        If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                            lblRPT_Date_AsFound.Text += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                        End If
                    Case EIR_BL.ST_TA_STEP.After_Clean
                        If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                            lblRPT_Date_AfterClean.Text += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                        End If
                    Case EIR_BL.ST_TA_STEP.NDE
                        If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                            lblRPT_Date_NDE.Text += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                        End If
                    Case EIR_BL.ST_TA_STEP.Repair
                        If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                            lblRPT_Date_Repair.Text += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                        End If
                    Case EIR_BL.ST_TA_STEP.After_Repair
                        If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                            lblRPT_Date_AfterRepair.Text += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                        End If
                    Case EIR_BL.ST_TA_STEP.Final
                        If Not IsDBNull(DT_Date_Step.Rows(i).Item("RPT_DATE")) Then
                            lblRPT_Date_Final.Text += BL.ReportProgrammingDate(DT_Date_Step.Rows(i).Item("RPT_DATE")) & " <br>"
                        End If

                End Select


            Next



            For i As Integer = 1 To 6
                DT_Date_Step.DefaultView.RowFilter = "STEP_ID=" & i & " AND STATUS_ID IS NOT NULL AND RPT_DATE IS NOT NULL "
                Dim DT_RowFilter = DT_Date_Step.DefaultView.ToTable()
                If (DT_Date_Step.DefaultView.Count > 0) Then

                    'Dim Max_CLASS As Integer = DT_Date_Step.Compute("MAX(STATUS_ID)", "")
                    Dim Max_CLASS As Integer = DT_Date_Step.DefaultView(0).Item("STATUS_ID") '-- สถานะล่าสุด

                    Select Case i
                        Case EIR_BL.ST_TA_STEP.As_Found
                            Dim td1 As HtmlTableCell = e.Item.FindControl("td1")
                            If Not IsDBNull(Max_CLASS) Then
                                If Max_CLASS = 0 Then
                                    td1.Style("background-color") = "white"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 1) Then
                                    td1.Style("background-color") = "yellow"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 2) Then
                                    td1.Style("background-color") = "Orange"
                                    'lblRPT_Date.Style("color") = "gray"
                                ElseIf (Max_CLASS = 3) Then
                                    td1.Style("background-color") = "red"
                                    'lblRPT_Date.Style("color") = "gray"
                                End If
                            Else
                                td1.Style("background-color") = "white"
                                'lblRPT_Date.Style("color") = "gray"
                            End If
                        Case EIR_BL.ST_TA_STEP.After_Clean
                            Dim td2 As HtmlTableCell = e.Item.FindControl("td2")
                            If Not IsDBNull(Max_CLASS) Then
                                If Max_CLASS = 0 Then
                                    td2.Style("background-color") = "white"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 1) Then
                                    td2.Style("background-color") = "yellow"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 2) Then
                                    td2.Style("background-color") = "Orange"
                                    'lblRPT_Date.Style("color") = "gray"
                                ElseIf (Max_CLASS = 3) Then
                                    td2.Style("background-color") = "red"
                                    'lblRPT_Date.Style("color") = "gray"
                                End If
                            Else
                                td2.Style("background-color") = "white"
                                'lblRPT_Date.Style("color") = "gray"
                            End If
                        Case EIR_BL.ST_TA_STEP.NDE
                            Dim td3 As HtmlTableCell = e.Item.FindControl("td3")
                            If Not IsDBNull(Max_CLASS) Then
                                If Max_CLASS = 0 Then
                                    td3.Style("background-color") = "white"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 1) Then
                                    td3.Style("background-color") = "yellow"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 2) Then
                                    td3.Style("background-color") = "Orange"
                                    'lblRPT_Date.Style("color") = "gray"
                                ElseIf (Max_CLASS = 3) Then
                                    td3.Style("background-color") = "red"
                                    'lblRPT_Date.Style("color") = "gray"
                                End If
                            Else
                                td3.Style("background-color") = "white"
                                'lblRPT_Date.Style("color") = "gray"
                            End If
                        Case EIR_BL.ST_TA_STEP.Repair
                            Dim td4 As HtmlTableCell = e.Item.FindControl("td4")
                            If Not IsDBNull(Max_CLASS) Then
                                If Max_CLASS = 0 Then
                                    td4.Style("background-color") = "white"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 1) Then
                                    td4.Style("background-color") = "yellow"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 2) Then
                                    td4.Style("background-color") = "Orange"
                                    'lblRPT_Date.Style("color") = "gray"
                                ElseIf (Max_CLASS = 3) Then
                                    td4.Style("background-color") = "red"
                                    'lblRPT_Date.Style("color") = "gray"
                                End If
                            Else
                                td4.Style("background-color") = "white"
                                'lblRPT_Date.Style("color") = "gray"
                            End If
                        Case EIR_BL.ST_TA_STEP.After_Repair
                            Dim td5 As HtmlTableCell = e.Item.FindControl("td5")
                            If Not IsDBNull(Max_CLASS) Then
                                If Max_CLASS = 0 Then
                                    td5.Style("background-color") = "white"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 1) Then
                                    td5.Style("background-color") = "yellow"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 2) Then
                                    td5.Style("background-color") = "Orange"
                                    'lblRPT_Date.Style("color") = "gray"
                                ElseIf (Max_CLASS = 3) Then
                                    td5.Style("background-color") = "red"
                                    'lblRPT_Date.Style("color") = "gray"
                                End If
                            Else
                                td5.Style("background-color") = "white"
                                'lblRPT_Date.Style("color") = "gray"
                            End If
                        Case EIR_BL.ST_TA_STEP.Final
                            Dim Final As HtmlTableCell = e.Item.FindControl("Final")
                            If Not IsDBNull(Max_CLASS) Then
                                If Max_CLASS = 0 Then
                                    Final.Style("background-color") = "white"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 1) Then
                                    Final.Style("background-color") = "yellow"
                                    'lblRPT_Date.Style("color") = "gray"

                                ElseIf (Max_CLASS = 2) Then
                                    Final.Style("background-color") = "Orange"
                                    'lblRPT_Date.Style("color") = "gray"
                                ElseIf (Max_CLASS = 3) Then
                                    Final.Style("background-color") = "red"
                                    'lblRPT_Date.Style("color") = "gray"
                                End If
                            Else
                                Final.Style("background-color") = "white"
                                'lblRPT_Date.Style("color") = "gray"
                            End If

                    End Select


                End If
            Next



        End If








    End Sub


    Function DT_Daily_Date(ByRef _RPT_Year As Integer, ByRef _RPT_No As Integer, ByRef _TAG_ID As Integer, ByRef _INSP_ID As Integer, ByRef _INSP_STATUS_ID As Integer) As DataTable
        Dim Sql As String = ""
        Sql &= "   DECLARE @RPT_Year As integer = " & _RPT_Year & "									" & vbNewLine
        Sql &= "   DECLARE @RPT_No As integer = " & _RPT_No & "										" & vbNewLine
        Sql &= "   DECLARE @TAG_ID As integer = " & _TAG_ID & "										" & vbNewLine
        Sql &= "   DECLARE @INSP_ID As integer = " & _INSP_ID & "										" & vbNewLine
        Sql &= "   DECLARE @INSP_STATUS_ID As integer = " & _INSP_STATUS_ID & "								" & vbNewLine
        Sql &= "   SELECT * FROM ( SELECT DISTINCT  													" & vbNewLine
        Sql &= "          STEP_ID														" & vbNewLine
        Sql &= "         ,RPT_DATE														" & vbNewLine
        Sql &= "         ,STATUS_ID														" & vbNewLine
        Sql &= "         ,MAX(STATUS_ID) MAX_CLASS										" & vbNewLine
        Sql &= "     FROM RPT_ST_TA_Detail_Step											" & vbNewLine
        Sql &= "     WHERE RPT_Year = @RPT_Year AND RPT_No = @RPT_No					" & vbNewLine
        Sql &= "     AND TAG_ID=@TAG_ID													" & vbNewLine
        Sql &= "     AND INSP_ID =@INSP_ID												" & vbNewLine
        Sql &= "     AND INSP_STATUS_ID= @INSP_STATUS_ID								" & vbNewLine

        Sql &= "     GROUP BY STEP_ID  ,RPT_DATE ,STATUS_ID								" & vbNewLine
        Sql &= "     )	TB								" & vbNewLine
        Sql &= "     WHERE 1=1								" & vbNewLine

        If txt_Search_Start.Text <> "" Then
            Sql &= " AND RPT_DATE>='" & txt_Search_Start.Text.Replace("'", "''") & "'   " & vbNewLine
        End If
        If txt_Search_End.Text <> "" Then
            Sql &= " AND RPT_DATE<='" & txt_Search_End.Text.Replace("'", "''") & "'  " & vbNewLine
        End If

        If ddl_Search_Class.SelectedValue > -1 Then
            Sql &= " AND STATUS_ID=" & ddl_Search_Class.Items(ddl_Search_Class.SelectedIndex).Value & "   " & vbNewLine
        End If


        If ddl_Search_Step.SelectedValue > 0 Then
            Sql &= " AND  STEP_ID=" & ddl_Search_Step.SelectedValue & " " & vbNewLine
        End If

        Sql &= "     ORDER BY RPT_DATE 	DESC						" & vbNewLine

        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)


        Return DT
    End Function


    '---Check Sheet---
    Dim Inspection_ As New DataTable

    Dim Inspection As New DataTable
    Dim Inspection_Status As New DataTable

    Private Sub BindCheckSheet()
        'Inspection_ = BL.Get_TA_Default_Inspection

        '------------------Launch Default Form -----------------------
        Inspection = BL.Get_Stationary_Default_Inspection
        Inspection_Status = BL.Get_Stationary_Default_Inspection_Status

        rptInsp.DataSource = Inspection
        rptInsp.DataBind()

        'Dim INSP As DataTable = BL.Get_TA_TagType_Inspection(TAG_TYPE_ID)

        Dim SQL As String = ""
        SQL &= " Select RPT_Year, RPT_No , TAG_ID , INSP_ID , INSP_STATUS_ID   " & vbLf
        SQL &= " From RPT_ST_TA_Detail_Step " & vbLf
        SQL &= " where RPT_Year = " & RPT_Year & " And rpt_No = " & RPT_No & vbLf
        If ddl_Tag.SelectedIndex > 0 Then
            SQL &= " And TAG_ID =" & ddl_Tag.Items(ddl_Tag.SelectedIndex).Value & "   " & vbNewLine
        End If
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim INSP As New DataTable
        DA.Fill(INSP)

        LoadInspectionItem(INSP)

    End Sub

    Private Sub LoadInspectionItem(ByVal INSP As DataTable)

        For Each Item As RepeaterItem In rptInsp.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For

            Dim lblNo As Label = Item.FindControl("lblNo")
            Dim rptInspItem As Repeater = Item.FindControl("rptInspItem")
            'Dim ddl_Ref_Item As DropDownList = Item.FindControl("ddl_Ref_Item")
            'Dim ddl_Ref_Status As DropDownList = Item.FindControl("ddl_Ref_Status")

            Dim INSP_ID As Integer = lblNo.Attributes("INSP_ID")

            For Each SItem As RepeaterItem In rptInspItem.Items
                If SItem.ItemType <> ListItemType.Item And SItem.ItemType <> ListItemType.AlternatingItem Then Continue For

                Dim chkUse As CheckBox = SItem.FindControl("chkUse")
                Dim STATUS_ID As Integer = chkUse.Attributes("STATUS_ID")
                INSP.DefaultView.RowFilter = "INSP_ID=" & INSP_ID & " AND INSP_STATUS_ID=" & STATUS_ID
                chkUse.Checked = INSP.DefaultView.Count > 0
            Next

            'For i As Integer = ddl_Ref_Item.Items.Count - 1 To 1 Step -1
            '    INSP.DefaultView.RowFilter = "INSP_ID=" & INSP_ID & " AND REF_INSP_ID=" & ddl_Ref_Item.Items(i).Value
            '    If INSP.DefaultView.Count > 0 Then
            '        ddl_Ref_Item.SelectedIndex = i
            '        Exit For
            '    End If
            'Next

            'For i As Integer = ddl_Ref_Status.Items.Count - 1 To 0 Step -1
            '    INSP.DefaultView.RowFilter = "INSP_ID=" & INSP_ID & " AND REF_STATUS_ID=" & ddl_Ref_Status.Items(i).Value
            '    If INSP.DefaultView.Count > 0 Then
            '        ddl_Ref_Status.SelectedIndex = i
            '        Exit For
            '    End If
            'Next
        Next
    End Sub


    Private Sub rptInsp_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptInsp.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Header
                Dim thTotalCol As HtmlTableCell = e.Item.FindControl("thTotalCol")
                Dim rptStatus As Repeater = e.Item.FindControl("rptStatus")
                Dim thStatusCol As HtmlTableCell = e.Item.FindControl("thStatusCol")

                thStatusCol.ColSpan = Inspection_Status.Rows.Count + 1
                thTotalCol.ColSpan = thStatusCol.ColSpan + 2

                AddHandler rptStatus.ItemDataBound, AddressOf rptStatusHeader_ItemDataBound
                rptStatus.DataSource = Inspection_Status
                rptStatus.DataBind()

            Case ListItemType.Item, ListItemType.AlternatingItem

                Dim lblNo As Label = e.Item.FindControl("lblNo")
                Dim lblInspItem As Label = e.Item.FindControl("lblInspItem")


                Dim rptInspItem As Repeater = e.Item.FindControl("rptInspItem")
                'Dim ddl_Ref_Item As DropDownList = e.Item.FindControl("ddl_Ref_Item")
                'Dim ddl_Ref_Status As DropDownList = e.Item.FindControl("ddl_Ref_Status")

                lblNo.Text = e.Item.ItemIndex + 1
                lblNo.Attributes("INSP_ID") = e.Item.DataItem("INSP_ID")
                lblInspItem.Text = e.Item.DataItem("INSP_Name")

                'ddl_Ref_Item.Items.Add(New ListItem("Alway Availble", 0))
                'For i As Integer = 0 To Inspection.Rows.Count - 1
                '    If Inspection.Rows(i).Item("INSP_ID") <> e.Item.DataItem("INSP_ID") Then
                '        ddl_Ref_Item.Items.Add(New ListItem(Inspection.Rows(i).Item("INSP_Name"), Inspection.Rows(i).Item("INSP_ID")))
                '    End If
                'Next
                'ddl_Ref_Status.Items.Add(New ListItem("Choose ..", 0))
                'For i As Integer = 0 To Inspection_Status.Rows.Count - 1
                '    ddl_Ref_Status.Items.Add(New ListItem(Inspection_Status.Rows(i).Item("STATUS_Name"), Inspection_Status.Rows(i).Item("STATUS_ID")))
                'Next

                AddHandler rptInspItem.ItemDataBound, AddressOf rptInspItem_ItemDataBound
                rptInspItem.DataSource = Inspection_Status
                rptInspItem.DataBind()

            Case ListItemType.Footer

        End Select
    End Sub

    Private Sub rptStatusHeader_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        lblStatus.Text = e.Item.DataItem("STATUS_Name")
    End Sub

    Private Sub rptInspItem_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim chkUse As CheckBox = e.Item.FindControl("chkUse")
        chkUse.Attributes("STATUS_ID") = e.Item.DataItem("STATUS_ID")
    End Sub

    Dim RC As New ReportClass
    Private Sub lnkPreview_Click(sender As Object, e As EventArgs) Handles lnkPreview.Click
        Dim Filter As String = ""
        Dim Filter_Daily As String = ""
        If ddl_Search_Tag.SelectedIndex > 0 Then
            Filter &= " AND Detail.TAG_ID=" & ddl_Search_Tag.Items(ddl_Search_Tag.SelectedIndex).Value & "   " & vbNewLine
        End If

        If txt_Search_Code.Text <> "" Then
            Filter &= " AND  VW_ST_TA_TAG.TAG_CODE LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%'   " & vbNewLine
        End If

        If ddl_Search_Components.SelectedIndex > 0 Then
            Filter &= " AND Detail_Step.INSP_ID=" & ddl_Search_Components.Items(ddl_Search_Components.SelectedIndex).Value & "   " & vbNewLine
        End If

        If ddl_Search_Condition.SelectedIndex > 0 Then
            Filter &= " AND Detail_Step.INSP_STATUS_ID=" & ddl_Search_Condition.Items(ddl_Search_Condition.SelectedIndex).Value & "   " & vbNewLine
        End If

        '====================

        If txt_Search_Start.Text <> "" Then
            Filter_Daily &= " AND RPT_DATE>='" & txt_Search_Start.Text.Replace("'", "''") & "'   " & vbNewLine
        End If
        If txt_Search_End.Text <> "" Then
            Filter_Daily &= " AND RPT_DATE<='" & txt_Search_End.Text.Replace("'", "''") & "'  " & vbNewLine
        End If
        If ddl_Search_Components.SelectedIndex > 0 Then
            Filter_Daily &= " AND INSP_ID=" & ddl_Search_Components.Items(ddl_Search_Components.SelectedIndex).Value & "   " & vbNewLine
        End If
        If ddl_Search_Class.SelectedValue > -1 Then
            Filter_Daily &= " AND STATUS_ID=" & ddl_Search_Class.Items(ddl_Search_Class.SelectedIndex).Value & "   " & vbNewLine
        End If
        If ddl_Search_Condition.SelectedIndex > 0 Then
            Filter_Daily &= " AND INSP_STATUS_ID=" & ddl_Search_Condition.Items(ddl_Search_Condition.SelectedIndex).Value & "   " & vbNewLine
        End If
        If ddl_Search_Step.SelectedValue > 0 Then
            Filter_Daily &= " AND  STEP_ID=" & ddl_Search_Step.SelectedValue & " " & vbNewLine
        End If

        Session("Filter_Daily") = Filter_Daily
        Session("Filter") = Filter
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('GL_ST_TA_Report.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "');", True)

    End Sub

    Private Sub ddl_Tag_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Tag.SelectedIndexChanged
        BindCheckSheet()
    End Sub

    Private Sub ddl_Search_Tag_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Tag.SelectedIndexChanged, txt_Search_Code.TextChanged, ddl_Search_Step.SelectedIndexChanged, ddl_Search_Class.SelectedIndexChanged
        BindList()
    End Sub

    Private Sub ddl_Search_Components_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Components.SelectedIndexChanged
        BindList()
    End Sub

    Private Sub ddl_Search_Condition_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Condition.SelectedIndexChanged
        BindList()
    End Sub

    Private Sub txt_Search_Start_TextChanged(sender As Object, e As EventArgs) Handles txt_Search_Start.TextChanged
        BindList()
    End Sub

    Private Sub txt_Search_End_TextChanged(sender As Object, e As EventArgs) Handles txt_Search_End.TextChanged
        BindList()
    End Sub
End Class