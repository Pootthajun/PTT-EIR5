﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Master_ST_TagType.aspx.vb" Inherits="EIR.Master_ST_TagType" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>



			
			<!-- Page Head -->
			<h2>Stationary Equipment-Type Setting</h2>
			
			<!-- End .shortcut-buttons-set -->
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div class="content-box-header">
               
              </div>
			  

             
              <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                  <!-- This is the target div. id must match the href of this div's tab -->
                  <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <asp:Panel ID="pnlListTagType" runat="server">
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">Equipment-Type Name </a></th>
                        <th><a href="#">Tag(s)</a></th>
                        <th><a href="#">Inspection Point(s)</a></th>
                        <th><a href="#">Status</a> </th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>
                    
                    <asp:Repeater ID="rptTagType" runat="server">
                           <HeaderTemplate>                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                                  <tr>
                                    <td><asp:Label ID="lblTypeName" runat="server"></asp:Label></td>
                                    <td style="text-align:center;"><asp:Label ID="lblTag" runat="server"></asp:Label></td>
                                    <td style="text-align:center;"><asp:Label ID="lblInspection" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                    <td><!-- Icons -->
                                      <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                      <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                       
                                   </td>
                                  </tr>                   
                     
                            </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                    <tfoot>
                      <tr>
                        <td colspan="5">
                         <div class="bulk-actions align-left">                             
                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                         </div>
                            <uc1:PageNavigation ID="Navigation" runat="server" />
                          <!-- End .pagination -->
                          <div class="clear"></div>   
                        </td>
                      </tr>
                    </tfoot>
                  </table>
				  
				   <div class="clear"></div>
				  </asp:Panel>
					
			    <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
				  
				  
				  <asp:Panel ID="pnlEdit" runat="server">
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Equipment-Type </h3>
                    <div class="clear"></div>
                  </div>

                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                    <p>&nbsp;</p>
                      <p>
                        <label class="column-left" style="width:180px;" >Equipment-Type Name : </label>
                        <asp:TextBox runat="server" ID="txtTypeName" CssClass="text-input small-input " MaxLength="20"></asp:TextBox>
                    </p>
                     
					<p>&nbsp;</p>
                    
                    
                      <asp:Repeater ID="rptInsp" runat="server">
                       <HeaderTemplate>
                        <table>
                        <thead>
                        <tr>
                          <th id="thTotalCol" runat="server" style="height:24px;"> 
                              Inspection Points  </th>
                        </tr>
                        <tr>
                          <th rowspan="2">No</th>
                          
                          <th rowspan="2">Insp. Point</th>
                          <th id="thStatusCol" runat="server"  style="text-align:center;">Possible Status </th>
                        </tr>
                        
                        <tr>
                            <asp:Repeater ID="rptStatus" runat="server">
                                <ItemTemplate>
                                    <th><asp:Label ID="lblStatus" runat="server" Font-Size="12px" Font-Bold="false"></asp:Label></th>
                                </ItemTemplate>                            
                            </asp:Repeater>
                          <th>Availabel If .. </th>
                        </tr>
                        
                      </thead>
                       </HeaderTemplate>
                       <ItemTemplate>
                            <tr>
                                <td><asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                
                                <td><asp:Label ID="lblInspItem" runat="server"></asp:Label></td>
                                <asp:Repeater ID="rptInspItem" runat="server">
                                    <ItemTemplate>
                                        <td><asp:CheckBox ID="chkUse" runat="server" /></td>
                                    </ItemTemplate>                                    
                                </asp:Repeater>
                                <td>
                                  <asp:DropDownList CssClass="select" ID="ddl_Ref_Item" runat="server">
                                  </asp:DropDownList>
                                   <asp:DropDownList CssClass="select" ID="ddl_Ref_Status" runat="server">
                                  </asp:DropDownList>
                                 </td>
                            </tr>
                       </ItemTemplate>
                       <FooterTemplate>
                           </table>
                       </FooterTemplate>
                      </asp:Repeater>                   
					 
					<div class="clear"></div>
					  
                   
                                <p></p>
                                <p>
                                    <label class="column-left" style="width:120px;">
                                    Available :
                                    </label>
                                    &nbsp;<asp:CheckBox ID="chkAvailable" runat="server" Text="" />
                                    <!-- End .clear -->
                                </p>
                                <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                                    <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                                    <div><asp:Label ID="lblValidation" runat="server"></asp:Label></div>
                                </asp:Panel>
                                <p align="right">
                                    
                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                                    <asp:Button ID="btnCopy" runat="server" CssClass="button" 
                                        Text="Copy this settings" />
                                    <asp:Button ID="btnPaste" runat="server" CssClass="button" 
                                        Text="Paste settings" />
                                    
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                                </p>
                    
                    
                    </fieldset>
                      </table>
                </asp:Panel>
                </div>
                <!-- End #tab1 -->
					
				
					
			  </div> <!-- End .content-box-content -->
				
		  </div> 
		  
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>