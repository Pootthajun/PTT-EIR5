﻿Public Class RenderPipeRoutineImage
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '---------- Render Temporary Session ------------
        Dim UNIQUE_ID As String = Request.QueryString("UNIQUE_ID")
        Dim TEMP_ID As String = Request.QueryString("TEMP_ID")
        '---------- Render Database Session ------------
        Dim RPT_Year As String = Request.QueryString("RPT_Year")
        Dim RPT_No As String = Request.QueryString("RPT_No")
        Dim TAG_ID As String = Request.QueryString("TAG_ID")
        Dim INSP_ID As String = Request.QueryString("INSP_ID")

        Dim DETAIL_ID As Integer = Request.QueryString("DETAIL_ID")
        Dim PIC_ID As String = Request.QueryString("PIC_ID")

        Dim IMAGE As String = Request.QueryString("Image")
        Dim IMG As Byte() = {}
        Try
            If UNIQUE_ID <> "" Then
                IMG = Session("PREVIEW_IMG_" & UNIQUE_ID & "_" & IMAGE)
            ElseIf TEMP_ID <> "" Then
                IMG = Session("TempImage_" & TEMP_ID & "_" & IMAGE)
            ElseIf PIC_ID <> "" Then
                IMG = PIPE.Get_ROUTINE_Image(DETAIL_ID, PIC_ID)
            End If
            If IMG.Length < 50 Then
                Response.Redirect("resources/images/Sample_40.png", True)
                Exit Sub
            End If
            Response.Clear()
            Response.BinaryWrite(IMG)
        Catch ex As Exception
            Response.Redirect("resources/images/Sample_40.png", True)
            Exit Sub
        End Try
    End Sub

End Class