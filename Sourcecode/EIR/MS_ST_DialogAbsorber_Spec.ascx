﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MS_ST_DialogAbsorber_Spec.ascx.vb" Inherits="EIR.MS_ST_DialogAbsorber_Spec" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>

<style type="text/css">
    .Dialog_Profile
{
	width:1200px;
	background-color:white;
	position:fixed;
	left:100px;
	top:20px;
	z-index:10;
	padding:10px;
	border-style:solid;
	border-color:gray;
	border-width:1px;
	}

        input {
    font-size :11px;
    text-align :center ;
    }

</style>

<%--<div class="MaskDialog"></div>--%>
<asp:Panel ID="pnl" runat="server" >
<asp:Label ID="lblTag" runat="server" Text="" Visible ="false" ></asp:Label>
<%--CssClass="Dialog_Profile"--%>
    <%--<h2 style="width: 100%; text-align: left;">Specs for :  
    &nbsp;&nbsp;
        &nbsp;&nbsp; (<asp:Label ID="lbl_TagType" runat="server" Text=""></asp:Label>)
    </h2>--%>

</asp:Panel>




    <table cellpadding="0" cellspacing="0" class="propertyTable">
        <tr>					            
					        
		<td class="propertyGroup" style="border:none;" colspan="6">Description</td>
        <td class="propertyGroup" style="border:none;">Cad / Drawing&nbsp;
        <asp:Button ID="btnUploadFile" runat="server" Text="" style="display:none;" />
        </td>
        <td class="propertyGroup" style="border:none; text-align:right;"> 
            <input type="button" ID="btnAddFile" TAG_ID="0" Active_Status="True" TAG_CODE="" TAG_NAME="" 
                runat="server" class="button" value="+ Add" style="cursor:pointer;" />
        </td>
	</tr>	
        <tr>
            <td style="width:15%;" class="propertyCaption">Plant <font color="red">**</font></td>
            <td style="width:15%;" colspan="2">
                <asp:DropDownList ID="ddl_Edit_Plant" runat="server" AutoPostBack="True" CssClass="select">
                </asp:DropDownList>                
            </td>
            <td style="width:15%;" class="propertyCaption">Route <font color="red">**</font></td>
            <td style="width:15%;" colspan="2">
                <asp:DropDownList ID="ddl_Edit_Route" runat="server" CssClass="select">
                </asp:DropDownList>
            </td>
            <td style="width:40%;"  rowspan="20" colspan="2">                                  
                <asp:Panel ID="DrawingAlbum" runat="Server" CssClass="FileAlbum">
                    <asp:Repeater ID="rptDrawing" runat="server">
                            <ItemTemplate>
                                    <div class="item" id="item" runat="server">
                                        <div class="thumbnail">
                                            <a ID="lnk_File_Dialog" runat="server" target="_blank" title="Drawing">                                                                                                                               
                                                    <asp:Image ID="img_File" runat="server" AlternateText="" onClick="window.open(this.src);" style="cursor:pointer;"/>                                                                    
                                            </a>
                                        </div>                                                        
                                        <div ID="pnlEditFile" runat="server" class="toolbar">
                                            <asp:Button ID="btnUpload" runat="server" Text="" style="display:none;" CommandName="Edit"  />
                                            <input type="image" id="btnEdit" runat="server" src="resources/images/icons/edit_white_16.png" style="margin-right:10px;" />
                                            <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left:10px;" />
                                            <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" ConfirmText="Confirm to delete file?" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                                        </div>                                                                                                                        
                                    </div>                       
                            </ItemTemplate>
                    </asp:Repeater>                                 
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="propertyCaption">Tag No <font color="red">**</font></td>
            <td colspan="2">
                <asp:DropDownList ID="ddl_Edit_Area" runat="server" AutoPostBack="True" CssClass="select">
                </asp:DropDownList> 
            </td>
            <td >
                <asp:DropDownList ID="ddl_Edit_Process" runat="server" AutoPostBack="True" CssClass="select">
                </asp:DropDownList> 
            </td>
            <td colspan="2">
                <asp:TextBox ID="txtTagNo" runat="server" MaxLength="10"  Font-Size ="10" Style=" margin-left :7px;" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="propertyCaption">Tag Name <font color="red">**</font></td>
            <td colspan="5">
            <asp:TextBox ID="txtTagName" runat="server" MaxLength="50" Font-Size ="10" style="text-align:left ; margin-left :7px;" ></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td class="propertyCaption">Equipement <font color="red">**</font></td>
            <td colspan="2" style="border-right-style :none ;">
                <asp:DropDownList ID="ddl_Edit_Type" runat="server" CssClass="select">
                </asp:DropDownList>
            </td>

            <td colspan="3"  style="border-left-style  :none ;">
                
            </td>
            
        </tr>
        <tr>
            <td class="propertyGroup" style="border:none;" colspan="6">Specification</td>
        </tr>

        <tr>					            
					        
		<td class="propertyCaption">Initial Year  <font color="red">**</font></td>					        
		<td colspan="2"><asp:TextBox ID="txt_Initial_Year" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>
		<td class="propertyCaption">Operating Pressure</td>
        <td><asp:TextBox ID="txt_OPERATING_PRESSURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
	</tr>	

        <tr>					            
					        
		<td class="propertyCaption">Code Stamp</td>					        
		<td colspan="2"><asp:TextBox ID="txt_CODE_STAMP" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>
		<td class="propertyCaption">Pressure Drop Through Internals</td>
        <td><asp:TextBox ID="txt_PRESSURE_DROP_THROUGH_INTERNALS" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
	</tr>	
        <tr>
            <td class="propertyCaption">Corrosion Allowance  <font color="red">**</font></td>
            <td><asp:TextBox ID="txt_CORROSION_ALLOWANCE" runat="server" MaxLength="50"></asp:TextBox></td>
            <td width="5%" style="text-align:center;">mm</td>
            <td class="propertyCaption">Insulation Thickness  <font color="red">**</font></td>
            <td><asp:TextBox ID="txt_INSULATION_THICKNESS" runat="server" MaxLength="10"></asp:TextBox></td>
			<td style="text-align:center;">Bar.g</td>
        </tr>

        <tr>
            <td class="propertyCaption">Norminal Thickness <font color="red">**</font></td>
            <td><asp:TextBox ID="txt_Norminal_Thickness" runat="server" MaxLength="50"></asp:TextBox></td>
            <td width="5%" style="text-align:center;">mm</td>
            <td class="propertyCaption">Calculated Thickness <font color="red">**</font></td>
            <td><asp:TextBox ID="txt_Calculated_Thickness" runat="server" MaxLength="10"></asp:TextBox></td>
			<td style="text-align:center;">mm</td>
        </tr>



        <tr>
            <td class="propertyCaption">Joint Efficiency</td>
            <td><asp:TextBox ID="txt_JOINT_EFFICIENCY" runat="server" MaxLength="50"></asp:TextBox></td>
            <td width="5%" style="text-align:center;">%</td>
            <td class="propertyCaption">Contents</td>
		<td colspan="2"><asp:TextBox ID="txt_CONTENTS" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>
        
        </tr>
        <tr>
            <td class="propertyCaption">Post Weld Heat Treatment</td>
            <td><asp:TextBox ID="txt_POST_WELD_HEAT_TREATMENT" runat="server" MaxLength="10"></asp:TextBox></td>
		<td style="text-align:center;">° C</td>
            <td class="propertyCaption">Liquid Specific Gravity</td>
            		<td colspan="2"><asp:TextBox ID="txt_LIQUID_SPECIFIC_GRAVITY" runat="server" MaxLength="50" style="text-align:center;" ></asp:TextBox></td>

        </tr>
        <tr>					            
		<td class="propertyCaption">Radiography</td>
		<td>
           <asp:TextBox ID="txt_RADIOGRAPHY_MIN" runat="server" MaxLength="10"></asp:TextBox>
           </td>
		<td style="text-align:center;">° C</td>
            <td class="propertyCaption">Length Between Tangent Line</td>
		<td><asp:TextBox ID="txt_LENGTH_BETWEEN_TANGENT_LINE" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">mm</td>
	</tr>	
    <tr>					            
		<td class="propertyCaption">Pneumatic Test Press</td>
		<td><asp:TextBox ID="txt_PNEUMATIC_TEST_PRESS" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
            <td class="propertyCaption">Shell Inside Diameter</td>
		<td><asp:TextBox ID="txt_SHELL_INSIDE_DIAMETER" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">mm</td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Hydro Test Pressure (At Shop)</td>
		<td><asp:TextBox ID="txt_HYDRO_TEST_PRESSURE_AT_SHOP" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
            <td class="propertyCaption">Type Of Heads</td>
		<td><asp:TextBox ID="txt_TYPE_OF_HEADS" runat="server" MaxLength="10"></asp:TextBox></td>
		<td style="text-align:center;">ELLIP</td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Hydro Test Pressure (At Field)</td>
		<td><asp:TextBox ID="txt_HYDRO_TEST_PRESSURE_AT_FIELD" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
            <td class="propertyCaption">W.I.N.D Load</td>
		<td><asp:TextBox ID="txt_WIND_LOAD" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">m/s <span style="font-size :9px" >(ASCE 7-95)</span> </td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Design Temperature</td>
		<td><asp:TextBox ID="txt_DESIGN_TEMPERATURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
		<td style="text-align:center;">° C</td>
            <td class="propertyCaption">Seismic Load</td>
		<td><asp:TextBox ID="txt_SEISMIC_LOAD" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">m/s <span style="font-size :9px" >(ASCE 7-95)</span> </td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Min Design Metal Temperature</td>
		<td><asp:TextBox ID="txt_MIN_DESIGN_METAL_TEMPERATURE" runat="server" MaxLength="10"></asp:TextBox></td>
		<td style="text-align:center;">° C</td>
            <td class="propertyCaption">Static Vapor Head</td>
		<td><asp:TextBox ID="txt_STATIC_VAPOR_HEAD" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">mm</td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Design Internal Pressure</td>
		<td><asp:TextBox ID="txt_DESIGN_INTERNAL_PRESSURE" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
            <td class="propertyCaption">Fire Proofing Thickness</td>
		<td><asp:TextBox ID="txt_FIRE_PROOFING_THICKNESS" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">mm</td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Design External Pressure</td>
		<td><asp:TextBox ID="txt_DESIGN_EXTERNAL_PRESSURE" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">Bar.g</td>
            <td class="propertyCaption">Refractory Lining Thickness Volume</td>
		<td><asp:TextBox ID="txt_REFRACTORY_LINING_THICKNESS" runat="server" MaxLength="10"></asp:TextBox></td>
            <td style="text-align:center;">mm</td>
	</tr>
    <tr>					            
		<td class="propertyCaption">Operating Temperature</td>
		<td><asp:TextBox ID="txt_OPERATING_TEMPERATURE_MIN" runat="server" MaxLength="10"></asp:TextBox></td>
		<td style="text-align:center;">° C</td>
            <td class="propertyCaption">Volume</td>
        <td><asp:TextBox ID="txt_VOLUME" runat="server" MaxLength="10"></asp:TextBox></td>
		        <td style="text-align:center;"><p>m<sup>3</sup></p> </td>
	</tr>

	<tr>
        <asp:Button ID="btnClose" runat="server" Class="button" Text="Close" Visible ="false"  />
                                                        &nbsp;
                                                        <asp:Button ID="btnUpdate" runat="server" Class="button" 
                                                            Text="Update to Tag"  Visible ="false"  />

	</tr>




</table>
<asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
<asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>
