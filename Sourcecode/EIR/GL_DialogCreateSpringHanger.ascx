﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_DialogCreateSpringHanger.ascx.vb" Inherits="EIR.GL_DialogCreateSpringHanger" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<div class="MaskDialog"></div>
<asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog" style="top:250px; left:20%; width:auto;">
    <h2>
        Create Report for
        <asp:DropDownList CssClass="select" ID="ddl_Plant" runat="server" AutoPostBack="True">
        </asp:DropDownList>		
		<asp:DropDownList CssClass="select" ID="ddl_Route" runat="server">
        </asp:DropDownList>                           
        <asp:DropDownList ID="ddl_Year" runat="server" CssClass="select">
        </asp:DropDownList>
        
    </h2>
   <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                            <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                                ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                            <div>
                                <asp:Label ID="lblValidation" runat="server"></asp:Label>
                            </div>
    </asp:Panel>
    <asp:Button ID="btnClose" runat="server" Class="button" Text="Cancel" />
    &nbsp;
    <asp:Button ID="btnCreate" runat="server" Class="button" Text="Create report" />

</asp:Panel>
<cc1:DragPanelExtender ID="pnlDialog_DragPanelExtender" runat="server" 
    DragHandleID="pnlDialog" Enabled="True" TargetControlID="pnlDialog">
</cc1:DragPanelExtender>