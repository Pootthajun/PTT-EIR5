﻿Imports System.IO
Public Class GL_DialogCanvasEditor
    Inherits System.Web.UI.Page

    Private ReadOnly Property UNIQUE_POPUP_ID() As String
        Get
            If Not IsNothing(Request.QueryString("UNIQUE_POPUP_ID")) Then
                Return Request.QueryString("UNIQUE_POPUP_ID")
            Else
                Return ""
            End If
        End Get
    End Property

    Private Property DisplayTitle As Boolean
        Get
            Return pTitle.Visible
        End Get
        Set(value As Boolean)
            pTitle.Visible = value
        End Set
    End Property

    Private Property DisplayType As Boolean
        Get
            Return pType.Visible
        End Get
        Set(value As Boolean)
            pType.Visible = value
        End Set
    End Property

    Private Property DisplayDesc As Boolean
        Get
            Return pDesc.Visible
        End Get
        Set(value As Boolean)
            pDesc.Visible = value
        End Set
    End Property

    Private ReadOnly Property InputTextbox As String
        Get
            Try
                Return Request.QueryString("InputTextbox")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private Property _ReadOnly As Boolean
        Get
            Return pnlInfo.Enabled
        End Get
        Set(value As Boolean)
            pnlInfo.Enabled = Not value
            btnPresave.Visible = Not value
            btnSave.Visible = Not value
        End Set
    End Property

    Private ReadOnly Property CancelButton As String
        Get
            Try
                Return Request.QueryString("CancelButton")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private ReadOnly Property OKButton As String
        Get
            Try
                Return Request.QueryString("OKButton")
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If UNIQUE_POPUP_ID = "" OrElse IsNothing(Session(UNIQUE_POPUP_ID)) Then
            Alert(Me.Page, "Invalid Information")
            CloseTopWindow(Page)
            Exit Sub
        End If

        If Not IsPostBack Then
            BindData()
            If Not IsNothing(Request.QueryString("DisplayTitle")) Then
                DisplayTitle = CBool(Request.QueryString("DisplayTitle"))
            End If
            If Not IsNothing(Request.QueryString("DisplayType")) Then
                DisplayType = CBool(Request.QueryString("DisplayType"))
            End If
            If Not IsNothing(Request.QueryString("DisplayDesc")) Then
                DisplayDesc = CBool(Request.QueryString("DisplayDesc"))
            End If
            If Not IsNothing(Request.QueryString("_ReadOnly")) Then
                _ReadOnly = CBool(Request.QueryString("_ReadOnly"))
            End If
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub
#End Region

    Private Sub BindData()

        Dim Obj As FileAttachment = Session(UNIQUE_POPUP_ID)

        '------------- Write Temp SVG File--------------------
        Dim BL As New EIR_BL
        Dim Path As String = BL.ServerMapPath & "\Temp\" & UNIQUE_POPUP_ID & ".svg"
        Try
            If File.Exists(Path) Then DeleteFile(Path)
        Catch
        End Try
        If Obj.Content.Count > 100 Then
            Dim ST As FileStream = File.Open(Path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
            ST.Write(Obj.Content, 0, Obj.Content.Count)
            ST.Close()
            ST.Dispose()
            '------------- Send Javascript To Open SVG--------------
            LoadSVGFromPath("../Temp/" & UNIQUE_POPUP_ID & ".svg")
        End If

        '------------ Bind Detail For This Part--------------
        txtTitle.Text = Obj.Title
        lblDesc.Text = Obj.Description
        BL.BindDDl_AttachmentType(ddlType, Obj.DocType)

        Dim LastEdit As String = ""
        If Obj.LastEditTime = DateTime.FromOADate(0) Then
            LastEdit = "New attachment"
        Else
            LastEdit = Obj.LastEditTime.ToString("dd-MMM-yyyy HH:mm")
            If Obj.LastEditBy = Session("USER_ID") Then
                LastEdit &= vbNewLine & " by you"
            Else
                Dim UT As DataTable = BL.GetUserDetail(Obj.LastEditBy)
                If UT.Rows.Count > 0 Then
                    LastEdit &= vbNewLine & UT.Rows(0).Item("User_Full_Name").ToString
                End If
            End If
        End If
        txtLastEdit.Text = LastEdit
        btnPresave.Attributes("onclick") = "storeSVGToSession('" & UNIQUE_POPUP_ID & "');"

    End Sub


    Private Sub LoadSVGFromPath(ByVal Path As String)
        Dim Script As String = ""
        Script &= "$(document).ready(function(){" & vbNewLine
        Script &= "                              LoadSVGFromFile('" & Path & "','" & UNIQUE_POPUP_ID & "');" & vbNewLine
        Script &= "                             }" & vbNewLine
        Script &= ");"
        ScriptManager.RegisterStartupScript(Me, GetType(String), "SVGLoad", Script, True)
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim Obj As FileAttachment = Session(UNIQUE_POPUP_ID)

        With Obj
            .Title = txtTitle.Text
            .Description = lblDesc.Text
            .Extension = FileAttachment.ExtensionType.SVG
            If ddlType.SelectedIndex > 0 Then
                .DocType = ddlType.Items(ddlType.SelectedIndex).Value
            Else
                .DocType = FileAttachment.AttachmentType.Other
            End If
            .LastEditTime = Now
            .LastEditBy = Session("USER_ID")
        End With
        If InputTextbox <> "" Then
            Dim Script As String = "top.opener.document.getElementById('" & InputTextbox & "').value='" & UNIQUE_POPUP_ID & "';"
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "StoreUniqueID", Script, True)
        End If
        If OKButton <> "" Then
            Dim Script As String = "top.opener.document.getElementById('" & OKButton & "').click();"
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "RefreshToOpener", Script, True)
        End If
        CloseTopWindow(Page)
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Dim Obj As FileAttachment = Session(UNIQUE_POPUP_ID)
        '------------------ User Create New File Attachment ------------------
        If Not IsNothing(Obj) AndAlso Obj.LastEditTime = DateTime.FromOADate(0) Then
            Obj = Nothing 'Clear Memory
        End If
        CloseTopWindow(Page)
    End Sub
End Class