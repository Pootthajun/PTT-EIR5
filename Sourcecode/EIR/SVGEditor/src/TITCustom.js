﻿
function LoadSVGFromFile(path, UNIQUE_POPUP_ID) {
    jQuery.get(path, function (data) {
        LoadSVGFromData(data, UNIQUE_POPUP_ID); //After Read Success Callback This Function
    });
}

function LoadSVGFromData(data, UNIQUE_POPUP_ID) {
    
    var xmlString = "";
    if (window.ActiveXObject) //IE
        xmlString = data.xml;
    else  // code for Mozilla, Firefox, Opera, etc.
        xmlString = (new XMLSerializer()).serializeToString(data);
    svgCanvas.setSvgString(xmlString);    
    ClearTempSVG(UNIQUE_POPUP_ID);
}

function ClearTempSVG(UNIQUE_POPUP_ID) {
    $.ajax({
        url: "SVGAPI.aspx?MODE=DELETE_TEMP_SVG&UNIQUE_POPUP_ID=" + UNIQUE_POPUP_ID +"&",
        data: { },
        type: "POST",
        dataType: "json",
        success: function (data) {
        },
        error: function () {
        }
    });
}

function storeSVGToSession(UNIQUE_POPUP_ID) {
    ////var svgCanvas = $("#SVGEditor")[0].contentWindow.svgCanvas;
    //var svgCanvas = parent.svgCanvas;
    var SVGContent = svgCanvas.getSvgString();
    var xhr = new XMLHttpRequest();
    var url = "SVGAPI.aspx?MODE=STORE_SVG_TO_SESSION&UNIQUE_POPUP_ID=" + UNIQUE_POPUP_ID + "&";
    xhr.open("POST", url, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == xhr.DONE) {
            $('#btnSave').click();
        }
    };     
    xhr.send(SVGContent);   
}