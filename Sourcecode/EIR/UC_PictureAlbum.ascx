﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_PictureAlbum.ascx.vb" Inherits="EIR.UC_PictureAlbum" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
    <link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />
    <link rel="stylesheet" href="resources/css/File_Album.css" type="text/css" media="all" />
    <link rel="stylesheet" href="resources/css/Dropdown_Popover.css" type="text/css" media="all" />
<asp:Label ID="lblUNIQUE_ID" runat="server" Visible="false"></asp:Label>
<div class="File_Album">
    <asp:Panel CssClass="File_Toolbar" ID="Toolbar" runat="server">
        <div class="File_Menu_Dropdown">
            <asp:Label ID="lblHead" runat="server" Font-Bold="true"></asp:Label>
            <asp:Button ID="btnAdd" runat="server" style="background-image:url('resources/images/icons/add.png')" CssClass="File_Menu_Dropdown_Button" Text="Add" />
            <div style="position:absolute;">
                <div class="File_Menu_Dropdown_List" style="position:relative; right:-50px;">
                    <asp:Repeater ID="rptAddType" runat="server">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnk" runat="server" CommandName="Add"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <asp:Button ID="btnRefreshAdd" runat="server" Style="display:none;" />
            <asp:TextBox ID="txtUpdateID" runat="server" Style="display:none;"/>
        </div>
        <div class="File_Menu_Dropdown" id="groupClearAll" runat="server">
            <asp:Button ID="btnClearAll" runat="server" style="background-image:url('resources/images/icons/cross.png')" CssClass="File_Menu_Dropdown_Button" Text="Clear" />
            <div class="File_Menu_Dropdown_List">
                <asp:LinkButton ID="lnkClearAll" runat="server">All type</asp:LinkButton>
                <Ajax:ConfirmButtonExtender ID="cfmlnkClearAll" runat="server" BehaviorID="lnkClearAll" TargetControlID="lnkClearAll" ConfirmText="Are you sure to clear all documents?"></Ajax:ConfirmButtonExtender>
                <Ajax:ConfirmButtonExtender ID="cfmbtnClearAll" runat="server" BehaviorID="btnClearAll" TargetControlID="btnClearAll" ></Ajax:ConfirmButtonExtender>
                <asp:Repeater ID="rptClear" runat="server">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnk" runat="server" CommandName="Clear"></asp:LinkButton>
                        <Ajax:ConfirmButtonExtender ID="cfm" runat="server" BehaviorID="lnk" TargetControlID="lnk"></Ajax:ConfirmButtonExtender>
                    </ItemTemplate>
                </asp:Repeater>
            </div>                              
        </div>
        <ul class="content-box-tabs">
			<li><asp:LinkButton id="lnkDisplayAll" runat="server" CssClass="default-tab current">All type</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
			<asp:Repeater ID="rptDisplayType" runat="server">
                <ItemTemplate>
                    <li><asp:LinkButton id="lnk" runat="server"  CommandName="Select">xxxxxxxx</asp:LinkButton></li>
                </ItemTemplate>                                    
			</asp:Repeater>
		</ul>
    </asp:Panel>
    <asp:Panel ID="pnlThumbnail" runat="server">
        <asp:Repeater ID="rptFile" runat="server">
            <ItemTemplate>
                <asp:Panel CssClass="File_Thumbnail" id="FileBlock" runat="server">
                    <a class="File_Zoom_Mask" ID="aZoomMask" runat="server" target="_blank"></a>
                    <asp:Panel ID="pnlImage" runat="server" CssClass="File_Image" BackImageUrl="resources/images/ISO.png">
                        <div class="File_Command">
                            <asp:ImageButton  ID="btnEdit" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" CommandName="Edit"/>
                            <asp:ImageButton  ID="btnDelete" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" CommandName="Delete"/>
                            <Ajax:ConfirmButtonExtender ID="cfm" runat="server" BehaviorID="btnDelete" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                        </div>  
                    </asp:Panel>  
                    <div class="File_Title" id="divTitle" runat="server"><asp:Label ID="lblTitle" runat="server"></asp:Label></div>
                    <asp:Button ID="btnRefresh" runat="server" Style="display:none;" CommandName="Refresh" />                    
                </asp:Panel>
            </ItemTemplate>
        </asp:Repeater>
    </asp:Panel>
                                                             
</div>