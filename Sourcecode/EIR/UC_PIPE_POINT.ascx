﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_PIPE_POINT.ascx.vb" Inherits="EIR.UC_PIPE_POINT" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>

<table cellpadding="0" cellspacing="0" class="propertyTable">
        
    <tr>		        
		<td class="propertyGroup" style="width:25%; font-weight:bold;">
            Point Name</td>
					        
		<td class="propertyGroup" style="border:none; border-top:1px solid #CCCCCC; border-right:1px solid #CCCCCC;" colspan="5" >
            <asp:TextBox ID="txt_PointName" runat="server" MaxLength="50" AutoPostBack="True" Font-Bold="true" Font-Size="14px" Width="100%" style="text-align-last:left;" OnTextChanged="Property_Changed"></asp:TextBox>
		</td>					        
	</tr>	
    <tr>					            
	<td class="propertyCaption">Component/Position</td>
	<td colspan="2">
            <asp:DropDownList ID="ddl_COMP" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
        </td>
        <td class="propertyCaption">
            Initial Year <font color="red">**</font></td>
	<td>
            <asp:TextBox ID="txt_Initial_Year" runat="server" MaxLength="4" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
	<td style="text-align:center;" class="propertyUnit">&nbsp;</td>
	</tr>	
    <tr>					            
	<td class="propertyCaption">Serial No <font color="red">**</font></td>
	<td colspan="2">
            <asp:DropDownList ID="ddl_Serial" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
    </td>
    <td class="propertyCaption">
            Norminal Thickness <font color="red">**</font></td>
	<td>
            <asp:TextBox ID="txt_Norminal_Thickness" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
	<td style="text-align:center;" class="propertyUnit">mm</td>
	</tr>	
    <tr>					            
	<td class="propertyCaption">Material Code<font color="red">**</font></td>
	<td colspan="2">
            <asp:DropDownList ID="ddl_MAT" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
        </td>
        <td class="propertyCaption">
            Calculated Thickness <font color="red">**</font></td>
	<td>
            <asp:TextBox ID="txt_Calculated_Thickness" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
	<td style="text-align:center;" class="propertyUnit">mm</td>
	</tr>	
    <tr>					            
	<td class="propertyCaption">Insulation <font color="red">**</font></td>
	<td colspan="2">
            <asp:DropDownList ID="ddl_INSULATION" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
        </td>
        <td class="propertyCaption">
            Insulation Thickness <font color="red">**</font></td>
	<td>
            <asp:TextBox ID="txt_Insulation_Thickness" runat="server" MaxLength="3" AutoPostBack="True"  OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
	<td style="text-align:center;" class="propertyUnit">mm</td>
	</tr>	
    <tr>					            
	<td class="propertyCaption">Pressure Code <font color="red">**</font></td>
	<td colspan="2">
            <asp:DropDownList ID="ddl_PRESSURE" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
            </asp:DropDownList>
        </td>
        <td class="propertyCaption">
            Corrosion Allowance <font color="red">**</font></td>
	<td colspan="2">
                <asp:DropDownList ID="ddl_CA" runat="server" CssClass="select" AutoPostBack="True" OnSelectedIndexChanged="Property_Changed">
                </asp:DropDownList>
        </td>
	</tr>	
    <tr>					            
	<td class="propertyCaption">Size <font color="red">**</font></td>
	<td>
            <asp:TextBox ID="txt_Size" runat="server" MaxLength="5" AutoPostBack="True" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
	<td style="text-align:center;" class="propertyUnit">inch</td>
        <td class="propertyCaption">
            Design Pressure</td>
	<td>
            <asp:TextBox ID="txt_Pressure_Design" runat="server" MaxLength="10" AutoPostBack="true" Style="text-align:left;"  OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
	<td style="text-align:center;" class="propertyUnit">Bar.g</td>
	</tr>	
    <tr>					            
	<td class="propertyCaption">Schedule</td>
	<td colspan="2">
            <asp:TextBox ID="txt_Schedule" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td class="propertyCaption">
            Operating Pressure</td>
	<td>
            <asp:TextBox ID="txt_Pressure_Operating" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
	<td style="text-align:center;" class="propertyUnit">Bar.g</td>
	</tr>	
    <tr>					            
	<td class="propertyCaption">From Location </td>
	<td colspan="2">
            <asp:TextBox ID="txt_Location_From" runat="server" MaxLength="100" Style="text-align:left;" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td class="propertyCaption">
            Design Temperature</td>
	<td>
            <asp:TextBox ID="txt_Temperature_Design" runat="server" MaxLength="10" AutoPostBack="true"  OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
		<td style="text-align:center;" class="propertyUnit">° C</td>
	</tr>	
    <tr>					            
	<td class="propertyCaption">To Location</td>
	<td colspan="2">
            <asp:TextBox ID="txt_Location_To" runat="server" MaxLength="100" Style="text-align:left;" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
        <td class="propertyCaption">
            Operating Temperature</td>
	<td>
            <asp:TextBox ID="txt_Temperature_Operating" runat="server" MaxLength="10" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox>
        </td>
		<td style="text-align:center;" class="propertyUnit">° C</td>
	</tr>	
    <tr>
        <td class="propertyGroup propertyCaption" style="font-weight:bold;">P &amp; ID No</td>
        <td colspan="5"><asp:TextBox ID="txt_P_ID_No" runat="server" MaxLength="50" style="text-align-last:left; padding-left:15px; width:100%;" AutoPostBack="true" OnTextChanged="Property_Changed"></asp:TextBox></td>
    </tr>
        
    </table>