﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_Mapping_eMonitor.ascx.vb" Inherits="EIR.GL_Mapping_eMonitor" %>

<div class="MaskDialog"></div>
		    
<asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog_Picture"  DefaultButton="btnOk"
    Width="400px" style="position:absolute; left:400px; top:150px; text-align:center;">
	<h3>Tag Mapping</h3>
	<table width="400">
		<thead>
		    <th Width="200" style="text-align:center">
		        EIR
		    </th>
		    <th Width="200" style="text-align:center">
		        eMonitor
		    </th>
		</thead>
		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
		<asp:Repeater ID="rptMapping" runat="server">
		    <ItemTemplate>
		        <tr>
		            <td align="center">
		                <asp:Label ID="lblEIR" runat="server" Width="180px"></asp:Label>
		            </td>
		            <td align="center">
		                <asp:TextBox ID="txtMapping" runat="server" Width="180px" MaxLength="100"></asp:TextBox>
		            </td>
		        </tr>
		    </ItemTemplate>
		</asp:Repeater>
		<tr>
		    <td colspan="2">&nbsp;</td>
		</tr>
		<tr>
		    <td align="center" style="border-top:1px solid #cfcfcf;">
		        <asp:Label ID="lblZone" runat="server" Width="180px" Text="At lease zone"></asp:Label>
		    </td>
		    <td align="Left" style="border-top:1px solid #cfcfcf;">
		        <asp:DropDownList ID="ddlZone" runat="server" Width="180px">
		            <asp:ListItem Value="" Selected="true">All zone</asp:ListItem>
		            <asp:ListItem Value="A1">A1</asp:ListItem>
		            <asp:ListItem Value="A2">A2</asp:ListItem>
		            <asp:ListItem Value="A3">A3</asp:ListItem>
		            <asp:ListItem Value="A4">A4</asp:ListItem>
		            <asp:ListItem Value="B1">B1</asp:ListItem>
		            <asp:ListItem Value="B2">B2</asp:ListItem>
		            <asp:ListItem Value="B3">B3</asp:ListItem>
		            <asp:ListItem Value="C1">C1</asp:ListItem>
		            <asp:ListItem Value="C2">C2</asp:ListItem>
		            <asp:ListItem Value="D1">D1</asp:ListItem>
		            <asp:ListItem Value="D2">D2</asp:ListItem>
		            <asp:ListItem Value="D3">D3</asp:ListItem>
		            <asp:ListItem Value="D4">D4</asp:ListItem>
		            <asp:ListItem Value="D5">D5</asp:ListItem>
		        </asp:DropDownList>
		    </td>
		</tr>
		<tr>
		    <td align="center">
		        <asp:Label ID="lblCalibrate" runat="server" Width="180px" Text="Calibration"></asp:Label>
		    </td>
		    <td align="Left">
		        <asp:CheckBox ID="chk_Pk" runat="server" Checked="True" /> Pk<br>
		        <asp:CheckBox ID="chk_PkPk" runat="server" Checked="True" /> Pk-Pk<br>
		        <asp:CheckBox ID="chk_RMS" runat="server" Checked="True" /> RMS<br>
		        <asp:CheckBox ID="chk_Other" runat="server" Checked="True" /> Other
		    </td>
		</tr>
		<tr>
		    <td align="center" style="border-bottom:1px solid #cfcfcf;">
		        <asp:Label ID="lblDataType" runat="server" Width="180px" Text="Type"></asp:Label>
		    </td>
		    <td align="left" style="border-bottom:1px solid #cfcfcf;">
		        <asp:CheckBox ID="chk_Magnitude" runat="server" Checked="True" /> MAGNITUDE<br>
		        <asp:CheckBox ID="chk_Spectrum" runat="server" Checked="True" /> SPECTRUM	                        
		    </td>
		</tr>
		<tr>
		    <td colspan="2">&nbsp;</td>
		</tr>
		<tr>
		    <td align="center">
		        <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="95%" CssClass="button" />
		    </td>
		    <td align="center">
		        <asp:Button ID="btnOk" runat="server" Text="OK" Width="95%" CssClass="button" />
		    </td>
		</tr>
	</table> 
	<br>
		            
</asp:Panel>