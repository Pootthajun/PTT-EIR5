﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Current_Status_Plant_ST.aspx.vb" Inherits="EIR.Dashboard_Current_Status_Plant_ST" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>


			<!-- Page Head -->
			<h2>Stationary Current Status for <asp:LinkButton ID="lblPlant" runat="server"></asp:LinkButton></h2>
        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
			  <tr>
				<td colspan="3"><h3 style="color: #0066CC;">Chart Type 
				    &nbsp;<asp:DropDownList ID="ddl_ChartType" runat="server" AutoPostBack="true" >
				<asp:ListItem Value="Bar"></asp:ListItem>
				<asp:ListItem Value="StackedBar"></asp:ListItem>	
				<asp:ListItem Value="Column"></asp:ListItem>
				<asp:ListItem Value="StackedColumn"></asp:ListItem>
				<asp:ListItem Value="StackedArea"></asp:ListItem>
                </asp:DropDownList>
                                </h3> 
                                
                                <asp:LinkButton ID="lblBack" runat="server"></asp:LinkButton>
                                </td>
			  </tr>
			  <tr>
				<td width="100%" style="vertical-align:top; width: 0%;">
				<asp:Chart ID="ChartMain" runat="server" Width="500px" Height="400px" CssClass="ChartHighligh" >
                    <legends>
                        <asp:Legend Docking="Bottom" LegendStyle="Row" Name="Legend1">
                        </asp:Legend>
                    </legends>
                    <Series>
                        <asp:Series Name="Series1" ChartType="StackedBar" Color="Green" 
                            Legend="Legend1" LegendText="Normal">
                        </asp:Series>
                        <asp:Series ChartArea="ChartArea1" ChartType="StackedBar" Color="Yellow" 
                            Legend="Legend1" LegendText="ClassC" Name="Series2">
                        </asp:Series>
                        <asp:Series ChartArea="ChartArea1" ChartType="StackedBar" 
                            Color="255, 128, 0" Legend="Legend1" LegendText="ClassB" Name="Series3">
                        </asp:Series>
                        <asp:Series ChartArea="ChartArea1" ChartType="StackedBar" Color="Red" 
                            Legend="Legend1" LegendText="ClassA" Name="Series4">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                            <axisy intervalautomode="VariableCount">
                            </axisy>
                            <axisx intervalautomode="VariableCount">
                            </axisx>
                            <axisx2 intervalautomode="VariableCount">
                            </axisx2>
                            <axisy2 intervalautomode="VariableCount">
                            </axisy2>
                        </asp:ChartArea>
                    </ChartAreas>
                    </asp:Chart>
                    &nbsp;</td>
                <td style="width:100px;"></td>
			      <td style="vertical-align:top;">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                              <td style="text-align:center; background-color:#003366; color:White">
                                  Area</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Normal</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  ClassC</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  ClassB</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  ClassA</td>
                              <td style="text-align:center; background-color:#003366; color:White;">
                                  Total</td>
                          </tr>
                          <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="tbTag" runat="server" style="cursor:pointer; border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                  <td style="text-align:center;">
                                      <asp:Label ID="lblArea" runat="server"></asp:Label></td>
                                  <td style="text-align:center;">
                                      <asp:Label ID="lblNormal" runat="server" CssClass="TextNormal"></asp:Label></td>
                                  <td style="text-align:center;">
                                      <asp:Label ID="lblClassC" runat="server" CssClass="TextClassC"></asp:Label></td>
                                  <td style="text-align:center;">
                                      <asp:Label ID="lblClassB" runat="server" CssClass="TextClassB"></asp:Label></td>
                                  <td style="text-align:center;">
                                      <asp:Label ID="lblClassA" runat="server" CssClass="TextClassA"></asp:Label></td>
                                  <td style="text-align:center;">
                                      <asp:Label ID="lblTotal" runat="server" Font-Bold="true"></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                          </asp:Repeater>
                          
                      </table>
                  </td>
		      </tr>
			</table>

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>