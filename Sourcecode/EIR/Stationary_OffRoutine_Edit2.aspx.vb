﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization


Public Class Stationary_OffRoutine_Edit2
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Stationary_Off_Routine_Report

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Public Property DETAIL_ID As Integer
        Get
            Return UC_Detail.DETAIL_ID
        End Get
        Set(value As Integer)
            UC_Detail.DETAIL_ID = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("Stationary_OffRoutine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_ST_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND RPT_Type_ID=" & RPT_Type_ID, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='Stationary_OffRoutine_Summary.aspx';", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_ST_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_ST_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_ST_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            BindTabData()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")
            '---------------Hide Dialog-------------------------
            'GL_DialogUploadImage1.CloseDialog()
        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_ST_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                '.RPT_Type = RPT_Type_ID
                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Stationary_OffRoutine_Summary.aspx'", True)
            Exit Sub
        End If

    End Sub

    Private Sub BindTabData()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_ST_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND RPT_Type_ID=" & EIR_BL.Report_Type.Stationary_Off_Routine_Report
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed'); window.location.href='Stationary_OffRoutine_Summary.aspx';", True)
            Exit Sub
        End If

        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        lbl_Year.Text = RPT_Year

        '-------------- Get TAG_ID, Tag_Code, Tag Name -----------
        SQL = "SELECT * FROM VW_REPORT_ST_DETAIL WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to get tag and report info'); window.location.href='Stationary_OffRoutine_Summary.aspx';", True)
            Exit Sub
        Else
            TAG_ID = DT.Rows(0).Item("TAG_ID")
            lbl_TAG.Text = DT.Rows(0).Item("TAG_CODE").ToString & " : " & DT.Rows(0).Item("TAG_Name").ToString
        End If

        BindRemain()

        BindNew()

        pnlList.Visible = True
        pnlEdit.Visible = False

    End Sub

    Private Sub BindRemain()
        '------------------------------Bind Problem -----------------------------------
        Dim SQL As String = "SELECT VW.* FROM VW_REPORT_ST_DETAIL_COUNT_SECTOR VW" & vbNewLine
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & " AND Create_Flag='Main'" & vbNewLine
        SQL &= " AND ISSUE<>" & EIR_BL.ISSUE.No_Problem & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptRemain.DataSource = DT
        rptRemain.DataBind()
        lblTotalOld.Text = DT.Rows.Count & " problem"
        If DT.Rows.Count > 1 Then
            lblTotalOld.Text &= "s"
        End If
        pnlRemain.Visible = DT.Rows.Count > 0


        DT.DefaultView.RowFilter = "CURRENT_Fixed IS NOT NULL AND CURRENT_LEVEL IS NOT NULL AND CURRENT_PROB_Detail IS NOT NULL AND CURRENT_PROB_Recomment IS NOT NULL"
        If DT.DefaultView.Count = 0 Then
            lblTotalUpdate.Text = "No remaining problem updated"
        Else
            lblTotalUpdate.Text = DT.DefaultView.Count & " problem" & IIf(DT.DefaultView.Count > 1, "s", "") & " updated"
        End If
    End Sub

    Private Sub BindNew()
        '------------------------------Bind Problem -----------------------------------
        Dim SQL As String = "SELECT VW.* FROM VW_REPORT_ST_DETAIL_COUNT_SECTOR VW" & vbNewLine
        SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & " AND Create_Flag='New'" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptNew.DataSource = DT
        rptNew.DataBind()
        lblTotalNew.Text = DT.Rows.Count
    End Sub

    Private Sub rptRemain_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptRemain.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim imgUpdateYes As Image = e.Item.FindControl("imgUpdateYes")
        Dim imgUpdateNo As Image = e.Item.FindControl("imgUpdateNo")

        Dim lbl_Inspection As Label = e.Item.FindControl("lbl_Inspection")
        Dim lbl_Problem As Label = e.Item.FindControl("lbl_Problem")
        Dim lbl_LastLevel As Label = e.Item.FindControl("lbl_LastLevel")
        Dim lbl_Fixed As Label = e.Item.FindControl("lbl_Fixed")
        Dim lbl_CurrentLevel As Label = e.Item.FindControl("lbl_CurrentLevel")
        Dim lbl_Trace As Label = e.Item.FindControl("lbl_Trace")
        Dim lbl_Doc As Label = e.Item.FindControl("lbl_Doc")
        Dim lbl_Detail As Label = e.Item.FindControl("lbl_Detail")

        Dim tdLastLevel As HtmlTableCell = e.Item.FindControl("tdLastLevel")
        Dim tdCurrentLevel As HtmlTableCell = e.Item.FindControl("tdCurrentLevel")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        lbl_Inspection.Text = e.Item.DataItem("INSP_Name").ToString
        lbl_Problem.Text = e.Item.DataItem("CURRENT_STATUS_Name").ToString

        Dim LAST_LEVEL As EIR_BL.InspectionLevel = EIR_BL.InspectionLevel.Unknown
        If Not IsDBNull(e.Item.DataItem("LAST_LEVEL")) Then
            LAST_LEVEL = e.Item.DataItem("LAST_LEVEL")
            tdLastLevel.Attributes("class") = BL.Get_Inspection_Css_Box_By_Level(LAST_LEVEL)
        End If
        Dim CURRENT_LEVEL As EIR_BL.InspectionLevel = EIR_BL.InspectionLevel.Unknown
        If Not IsDBNull(e.Item.DataItem("CURRENT_LEVEL")) Then
            CURRENT_LEVEL = e.Item.DataItem("CURRENT_LEVEL")
            tdCurrentLevel.Attributes("class") = BL.Get_Inspection_Css_Box_By_Level(CURRENT_LEVEL)
        End If

        lbl_LastLevel.Text = e.Item.DataItem("LAST_LEVEL_Desc").ToString

        lbl_CurrentLevel.Text = e.Item.DataItem("CURRENT_LEVEL_Desc").ToString
        If IsDBNull(e.Item.DataItem("PROBLEM_Detail")) OrElse e.Item.DataItem("PROBLEM_Detail") = "" Then
            lbl_Detail.Text = e.Item.DataItem("LAST_PROB_Detail").ToString
        Else
            lbl_Detail.Text = e.Item.DataItem("PROBLEM_Detail").ToString
        End If

        btnEdit.CommandArgument = e.Item.DataItem("Detail_ID").ToString
        btnDelete.CommandArgument = e.Item.DataItem("Detail_ID").ToString

        '--------------- Update Trace (Copy Logic From GL_Inspection)-------------
        Dim Fixed As Boolean = Not IsDBNull(e.Item.DataItem("CURRENT_Fixed")) AndAlso e.Item.DataItem("CURRENT_Fixed")
        If Fixed Then
            lbl_Fixed.Text = "Fixed"
            lbl_Fixed.ForeColor = Drawing.Color.Green
        Else
            lbl_Fixed.Text = "Not Fix"
            lbl_Fixed.ForeColor = Drawing.Color.Red
        End If

        tdCurrentLevel.Attributes("class") = BL.Get_Inspection_Css_Box_By_Level(CURRENT_LEVEL)
        lbl_CurrentLevel.Text = BL.Get_Problem_Level_Name(CURRENT_LEVEL)

        If LAST_LEVEL > 0 And CURRENT_LEVEL = 0 Then
            lbl_Trace.Text = "Problem fixed"
            lbl_Trace.ForeColor = Drawing.Color.Green
        ElseIf LAST_LEVEL < 1 And CURRENT_LEVEL > 0 Then
            lbl_Trace.Text = "New Problem"
            lbl_Trace.ForeColor = Drawing.Color.Red
        ElseIf LAST_LEVEL > 0 And (CURRENT_LEVEL > 0 Or CURRENT_LEVEL = -1) Then
            If Fixed Then
                lbl_Trace.Text = "Fixed incompletely"
            Else
                lbl_Trace.Text = "Still has problem"
            End If
            lbl_Trace.ForeColor = Drawing.Color.Red
        Else
            lbl_Trace.Text = "No problem found"
            lbl_Trace.ForeColor = Drawing.Color.Silver
        End If

        lbl_Doc.Text = e.Item.DataItem("Total_Sector").ToString

        '------------ Display Updated Status--------------
        If Not IsDBNull(e.Item.DataItem("CURRENT_Fixed")) And Not IsDBNull(e.Item.DataItem("CURRENT_LEVEL")) And Not IsDBNull(e.Item.DataItem("CURRENT_PROB_Detail")) And Not IsDBNull(e.Item.DataItem("CURRENT_PROB_Recomment")) Then
            imgUpdateYes.Visible = True
            imgUpdateNo.Visible = False
            btnDelete.Visible = True
        Else
            imgUpdateYes.Visible = False
            imgUpdateNo.Visible = True
            btnDelete.Visible = False
        End If
    End Sub

    Private Sub rptInspection_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptRemain.ItemCommand, rptNew.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                Dim UNIQUE_POPUP_ID As String = Now.ToOADate.ToString.Replace(".", "")
                UC_Detail.UNIQUE_POPUP_ID = UNIQUE_POPUP_ID
                UC_Detail.ShowDialog(e.CommandArgument)

                BindAttachement()

                pnlEdit.Visible = True
                pnlList.Visible = False
            Case "Delete"
                BL.Drop_RPT_ST_Detail_CheckFlag(e.CommandArgument)
                BindTabData()
        End Select
    End Sub

    Private Sub rptNew_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptNew.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Inspection As Label = e.Item.FindControl("lbl_Inspection")
        Dim lbl_Problem As Label = e.Item.FindControl("lbl_Problem")
        Dim lbl_CurrentLevel As Label = e.Item.FindControl("lbl_CurrentLevel")
        Dim lbl_Trace As Label = e.Item.FindControl("lbl_Trace")
        Dim lbl_Doc As Label = e.Item.FindControl("lbl_Doc")
        Dim lbl_Detail As Label = e.Item.FindControl("lbl_Detail")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Dim tdCurrentLevel As HtmlTableCell = e.Item.FindControl("tdCurrentLevel")

        lbl_Inspection.Text = e.Item.DataItem("INSP_Name").ToString
        lbl_Problem.Text = e.Item.DataItem("CURRENT_STATUS_Name").ToString

        Dim CURRENT_LEVEL As EIR_BL.InspectionLevel = EIR_BL.InspectionLevel.Unknown
        If Not IsDBNull(e.Item.DataItem("CURRENT_LEVEL")) Then
            CURRENT_LEVEL = e.Item.DataItem("CURRENT_LEVEL")
            tdCurrentLevel.Attributes("class") = BL.Get_Inspection_Css_Box_By_Level(CURRENT_LEVEL)
        End If

        Dim LAST_LEVEL As EIR_BL.InspectionLevel = EIR_BL.InspectionLevel.Normal

        lbl_CurrentLevel.Text = e.Item.DataItem("CURRENT_LEVEL_Desc").ToString
        If IsDBNull(e.Item.DataItem("PROBLEM_Detail")) OrElse e.Item.DataItem("PROBLEM_Detail") = "" Then
            lbl_Detail.Text = e.Item.DataItem("LAST_PROB_Detail").ToString
        Else
            lbl_Detail.Text = e.Item.DataItem("PROBLEM_Detail").ToString
        End If

        btnEdit.CommandArgument = e.Item.DataItem("Detail_ID").ToString
        btnDelete.CommandArgument = e.Item.DataItem("Detail_ID").ToString

        tdCurrentLevel.Attributes("class") = BL.Get_Inspection_Css_Box_By_Level(CURRENT_LEVEL)
        lbl_CurrentLevel.Text = BL.Get_Problem_Level_Name(CURRENT_LEVEL)

        If CURRENT_LEVEL > 0 Then
            lbl_Trace.Text = "New Problem"
            lbl_Trace.ForeColor = Drawing.Color.Red
        Else
            lbl_Trace.Text = "No problem found"
            lbl_Trace.ForeColor = Drawing.Color.Silver
        End If

        lbl_Doc.Text = e.Item.DataItem("Total_Sector").ToString

    End Sub


#Region "Navigator"

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click, btn_Back.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click, btn_Next.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Stationary_OffRoutine_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Private Sub btnClearNew_Click(sender As Object, e As ImageClickEventArgs) Handles btnClearNew.Click
        Dim DETAIL_ID As Integer() = {}
        For Each Item As RepeaterItem In rptNew.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim btnEdit As ImageButton = Item.FindControl("btnEdit")
            BL.Drop_RPT_ST_Detail_CheckFlag(btnEdit.CommandArgument)
        Next
        BindTabData()
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

#End Region

    Private Sub lnkAddSection_Click(sender As Object, e As EventArgs) Handles lnkAddSection.Click
        TemplateSelector.ShowDialog()
    End Sub

    Private Function SectorData() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("UNIQUE_POPUP_ID")
        DT.Columns.Add("Sector_ID", GetType(Integer))
        DT.Columns.Add("Template_ID", GetType(Integer))
        DT.Columns.Add("File_1", GetType(FileAttachment))
        DT.Columns.Add("File_2", GetType(FileAttachment))
        DT.Columns.Add("File_3", GetType(FileAttachment))
        DT.Columns.Add("File_4", GetType(FileAttachment))

        For Each Item As RepeaterItem In rptDoc.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblNo As Label = Item.FindControl("lblNo")
            Dim DR As DataRow = DT.NewRow
            DT.Rows.Add(DR)

            DR("Sector_ID") = lblNo.Attributes("Sector_ID")
            DR("Template_ID") = lblNo.Attributes("Template_ID")

            Dim TemplateType As UC_Select_Template.TemplateType = dr("Template_ID")
            Select Case TemplateType
                Case UC_Select_Template.TemplateType.Template1
                    Dim Template As UC_Template_1 = Item.FindControl("Template_1")
                    DR("UNIQUE_POPUP_ID") = Template.UNIQUE_POPUP_ID
                    DR("File_1") = Template.IMAGE_1

                Case UC_Select_Template.TemplateType.Template2
                    Dim Template As UC_Template_2 = Item.FindControl("Template_2")
                    DR("UNIQUE_POPUP_ID") = Template.UNIQUE_POPUP_ID
                    DR("File_1") = Template.IMAGE_1

                Case UC_Select_Template.TemplateType.Template3
                    Dim Template As UC_Template_3 = Item.FindControl("Template_3")
                    DR("UNIQUE_POPUP_ID") = Template.UNIQUE_POPUP_ID
                    DR("File_1") = Template.IMAGE_1
                    DR("File_2") = Template.IMAGE_2
                    DR("File_3") = Template.IMAGE_3

                Case UC_Select_Template.TemplateType.Template4
                    Dim Template As UC_Template_4 = Item.FindControl("Template_4")
                    DR("UNIQUE_POPUP_ID") = Template.UNIQUE_POPUP_ID
                    DR("File_1") = Template.IMAGE_1

                Case UC_Select_Template.TemplateType.Template5
                    Dim Template As UC_Template_5 = Item.FindControl("Template_5")
                    DR("UNIQUE_POPUP_ID") = Template.UNIQUE_POPUP_ID
                    DR("File_1") = Template.IMAGE_1

                Case UC_Select_Template.TemplateType.Template6
                    Dim Template As UC_Template_6 = Item.FindControl("Template_6")

                    DR("UNIQUE_POPUP_ID") = Template.UNIQUE_POPUP_ID
                    DR("File_1") = Template.IMAGE_1
                    DR("File_2") = Template.IMAGE_2
                    DR("File_3") = Template.IMAGE_3
                    DR("File_4") = Template.IMAGE_4

                Case UC_Select_Template.TemplateType.Template7
                    Dim Template As UC_Template_7 = Item.FindControl("Template_7")

                    DR("UNIQUE_POPUP_ID") = Template.UNIQUE_POPUP_ID
                    DR("File_1") = Template.IMAGE_1
                    DR("File_2") = Template.IMAGE_2
                    DR("File_3") = Template.IMAGE_3

                Case UC_Select_Template.TemplateType.Template8
                    Dim Template As UC_Template_8 = Item.FindControl("Template_8")

                    DR("UNIQUE_POPUP_ID") = Template.UNIQUE_POPUP_ID
                    DR("File_1") = Template.IMAGE_1
                    DR("File_2") = Template.IMAGE_2

            End Select
        Next

        Return DT

    End Function

    Private Sub TemplateSelector_SelectTemplate(TemplateType As UC_Select_Template.TemplateType) Handles TemplateSelector.SelectTemplate

        Dim DT As DataTable = SectorData()

        Dim DR As DataRow
        DR = DT.NewRow

        DR("UNIQUE_POPUP_ID") = GenerateNewUniqueID()
        DR("Sector_ID") = 0
        DR("Template_ID") = TemplateType

        DT.Rows.Add(DR)
        rptDoc.DataSource = DT
        rptDoc.DataBind()

    End Sub

    Private Sub rptDoc_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptDoc.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        If IsDBNull(e.Item.DataItem("Template_ID")) Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim lblDetailID As Label = e.Item.FindControl("lblDetailID")

        lblNo.Text = e.Item.ItemIndex + 1
        lblNo.Attributes("Sector_ID") = e.Item.DataItem("Sector_ID")
        lblNo.Attributes("Template_ID") = e.Item.DataItem("Template_ID")
        btnDelete.CommandArgument = e.Item.DataItem("Sector_ID")

        Dim TemplateType As UC_Select_Template.TemplateType = e.Item.DataItem("Template_ID")
        Select Case TemplateType
            Case UC_Select_Template.TemplateType.Template1

                Dim Template As UC_Template_1 = e.Item.FindControl("Template_1")
                Template.Visible = True

                Template.UNIQUE_POPUP_ID = e.Item.DataItem("UNIQUE_POPUP_ID")
                Template.Sector_ID = e.Item.DataItem("Sector_ID")
                Template.Disabled = False

                If Not IsDBNull(e.Item.DataItem("File_1")) AndAlso Not IsNothing(e.Item.DataItem("File_1")) Then
                    Dim File_1 As FileAttachment = e.Item.DataItem("File_1")
                    Template.IMAGE_1 = File_1
                    Template.Detail1 = File_1.Description
                End If

            Case UC_Select_Template.TemplateType.Template2
                Dim Template As UC_Template_2 = e.Item.FindControl("Template_2")
                Template.Visible = True

                Template.UNIQUE_POPUP_ID = e.Item.DataItem("UNIQUE_POPUP_ID")
                Template.Sector_ID = e.Item.DataItem("Sector_ID")
                Template.Disabled = False

                If Not IsDBNull(e.Item.DataItem("File_1")) AndAlso Not IsNothing(e.Item.DataItem("File_1")) Then
                    Dim File_1 As FileAttachment = e.Item.DataItem("File_1")
                    Template.IMAGE_1 = File_1
                    Template.Detail1 = File_1.Description
                End If

            Case UC_Select_Template.TemplateType.Template3
                Dim Template As UC_Template_3 = e.Item.FindControl("Template_3")
                Template.Visible = True

                Template.UNIQUE_POPUP_ID = e.Item.DataItem("UNIQUE_POPUP_ID")
                Template.Sector_ID = e.Item.DataItem("Sector_ID")
                Template.Disabled = False

                If Not IsDBNull(e.Item.DataItem("File_1")) AndAlso Not IsNothing(e.Item.DataItem("File_1")) Then
                    Dim File_1 As FileAttachment = e.Item.DataItem("File_1")
                    Template.IMAGE_1 = File_1
                    Template.Detail1 = File_1.Description
                End If

                If Not IsDBNull(e.Item.DataItem("File_2")) AndAlso Not IsNothing(e.Item.DataItem("File_2")) Then
                    Dim File_2 As FileAttachment = e.Item.DataItem("File_2")
                    Template.IMAGE_2 = File_2
                    Template.Detail2 = File_2.Description
                End If

                If Not IsDBNull(e.Item.DataItem("File_3")) AndAlso Not IsNothing(e.Item.DataItem("File_3")) Then
                    Dim File_3 As FileAttachment = e.Item.DataItem("File_3")
                    Template.IMAGE_3 = File_3
                    Template.Detail3 = File_3.Description
                End If

            Case UC_Select_Template.TemplateType.Template4
                Dim Template As UC_Template_4 = e.Item.FindControl("Template_4")
                Template.Visible = True

                Template.UNIQUE_POPUP_ID = e.Item.DataItem("UNIQUE_POPUP_ID")
                Template.Sector_ID = e.Item.DataItem("Sector_ID")
                Template.Disabled = False

                If Not IsDBNull(e.Item.DataItem("File_1")) AndAlso Not IsNothing(e.Item.DataItem("File_1")) Then
                    Dim File_1 As FileAttachment = e.Item.DataItem("File_1")
                    Template.IMAGE_1 = File_1
                End If

            Case UC_Select_Template.TemplateType.Template5
                Dim Template As UC_Template_5 = e.Item.FindControl("Template_5")
                Template.Visible = True

                Template.UNIQUE_POPUP_ID = e.Item.DataItem("UNIQUE_POPUP_ID")
                Template.Sector_ID = e.Item.DataItem("Sector_ID")
                Template.Disabled = False

                If Not IsDBNull(e.Item.DataItem("File_1")) AndAlso Not IsNothing(e.Item.DataItem("File_1")) Then
                    Dim File_1 As FileAttachment = e.Item.DataItem("File_1")
                    Template.IMAGE_1 = File_1
                    Template.Detail1 = File_1.Description
                End If

            Case UC_Select_Template.TemplateType.Template6
                Dim Template As UC_Template_6 = e.Item.FindControl("Template_6")
                Template.Visible = True

                Template.UNIQUE_POPUP_ID = e.Item.DataItem("UNIQUE_POPUP_ID")
                Template.Sector_ID = e.Item.DataItem("Sector_ID")
                Template.Disabled = False

                If Not IsDBNull(e.Item.DataItem("File_1")) AndAlso Not IsNothing(e.Item.DataItem("File_1")) Then
                    Dim File_1 As FileAttachment = e.Item.DataItem("File_1")
                    Template.IMAGE_1 = File_1
                    Template.Detail1 = File_1.Description
                End If

                If Not IsDBNull(e.Item.DataItem("File_2")) AndAlso Not IsNothing(e.Item.DataItem("File_2")) Then
                    Dim File_2 As FileAttachment = e.Item.DataItem("File_2")
                    Template.IMAGE_2 = File_2
                    Template.Detail2 = File_2.Description
                End If

                If Not IsDBNull(e.Item.DataItem("File_3")) AndAlso Not IsNothing(e.Item.DataItem("File_3")) Then
                    Dim File_3 As FileAttachment = e.Item.DataItem("File_3")
                    Template.IMAGE_3 = File_3
                    Template.Detail3 = File_3.Description
                End If

                If Not IsDBNull(e.Item.DataItem("File_4")) AndAlso Not IsNothing(e.Item.DataItem("File_4")) Then
                    Dim File_4 As FileAttachment = e.Item.DataItem("File_4")
                    Template.IMAGE_4 = File_4
                    Template.Detail4 = File_4.Description
                End If

            Case UC_Select_Template.TemplateType.Template7
                Dim Template As UC_Template_7 = e.Item.FindControl("Template_7")
                Template.Visible = True

                Template.UNIQUE_POPUP_ID = e.Item.DataItem("UNIQUE_POPUP_ID")
                Template.Sector_ID = e.Item.DataItem("Sector_ID")
                Template.Disabled = False

                If Not IsDBNull(e.Item.DataItem("File_1")) AndAlso Not IsNothing(e.Item.DataItem("File_1")) Then
                    Dim File_1 As FileAttachment = e.Item.DataItem("File_1")
                    Template.IMAGE_1 = File_1
                    Template.Detail1 = File_1.Description
                End If

                If Not IsDBNull(e.Item.DataItem("File_2")) AndAlso Not IsNothing(e.Item.DataItem("File_2")) Then
                    Dim File_2 As FileAttachment = e.Item.DataItem("File_2")
                    Template.IMAGE_2 = File_2
                    Template.Detail2 = File_2.Description
                End If

                If Not IsDBNull(e.Item.DataItem("File_3")) AndAlso Not IsNothing(e.Item.DataItem("File_3")) Then
                    Dim File_3 As FileAttachment = e.Item.DataItem("File_3")
                    Template.IMAGE_3 = File_3
                    Template.Detail3 = File_3.Description
                End If


            Case UC_Select_Template.TemplateType.Template8
                Dim Template As UC_Template_8 = e.Item.FindControl("Template_8")
                Template.Visible = True

                Template.UNIQUE_POPUP_ID = e.Item.DataItem("UNIQUE_POPUP_ID")
                Template.Sector_ID = e.Item.DataItem("Sector_ID")
                Template.Disabled = False

                If Not IsDBNull(e.Item.DataItem("File_1")) AndAlso Not IsNothing(e.Item.DataItem("File_1")) Then
                    Dim File_1 As FileAttachment = e.Item.DataItem("File_1")
                    Template.IMAGE_1 = File_1
                    Template.Detail1 = File_1.Description
                End If

                If Not IsDBNull(e.Item.DataItem("File_2")) AndAlso Not IsNothing(e.Item.DataItem("File_2")) Then
                    Dim File_2 As FileAttachment = e.Item.DataItem("File_2")
                    Template.IMAGE_2 = File_2
                    Template.Detail2 = File_2.Description
                End If

        End Select

    End Sub

    Private Sub rptDoc_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptDoc.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Select Case e.CommandName
            Case "Delete"
                Dim DT As DataTable = SectorData()
                DT.Rows.RemoveAt(e.Item.ItemIndex)
                rptDoc.DataSource = DT
                rptDoc.DataBind()
        End Select
    End Sub

    Private Sub BindAttachement()
        TemplateSelector.Visible = False

        Dim SQL As String = " SELECT Sector_ID,Template_ID" & vbLf
        SQL &= " FROM RPT_Template_Sector " & vbLf
        SQL &= " WHERE RPT_Type_ID=" & RPT_Type_ID & " AND DETAIL_ID=" & DETAIL_ID & vbLf
        SQL &= " ORDER BY Sector_ID" & vbLf

        Dim DA As SqlDataAdapter = New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As DataTable = New DataTable
        DA.Fill(DT)

        '------------- Get File -----------
        DT.Columns.Add("UNIQUE_POPUP_ID")
        DT.Columns.Add("File_1", GetType(FileAttachment))
        DT.Columns.Add("File_2", GetType(FileAttachment))
        DT.Columns.Add("File_3", GetType(FileAttachment))
        DT.Columns.Add("File_4", GetType(FileAttachment))
        For i As Integer = 0 To DT.Rows.Count - 1
            DT.Rows(i).Item("UNIQUE_POPUP_ID") = GenerateNewUniqueID()
            For f As Integer = 1 To 4
                Dim Attachemant As FileAttachment = BL.Get_Stationary_Template_File(DT.Rows(i).Item("Sector_ID"), f)
                If Not IsNothing(Attachemant) Then
                    DT.Rows(i).Item("File_" & f) = Attachemant
                Else
                    DT.Rows(i).Item("File_" & f) = DBNull.Value
                End If
            Next
        Next

        rptDoc.DataSource = DT
        rptDoc.DataBind()

        If DT.Rows.Count = 0 Then
            lblTotalDoc.Text = "None"
        Else
            lblTotalDoc.Text = DT.Rows.Count & " page"
            If DT.Rows.Count > 1 Then
                lblTotalDoc.Text &= "s"
            End If
        End If

    End Sub

    Private Sub Add_Click(sender As Object, e As EventArgs) Handles lnkUpload.Click, btnAddNew.Click
        Dim UNIQUE_POPUP_ID As String = Now.ToOADate.ToString.Replace(".", "")
        UC_Detail.UNIQUE_POPUP_ID = UNIQUE_POPUP_ID
        UC_Detail.ShowDialog(RPT_Year, RPT_No, TAG_ID)

        BindAttachement()

        pnlEdit.Visible = True
        pnlList.Visible = False
    End Sub

    Protected Sub btn_Buffer_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Buffer_Refresh.Click
        BindTabData()
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click
        Try
            BL.Drop_RPT_ST_Detail(RPT_Year, RPT_No, TAG_ID)
            BL.Construct_ST_Report_Detail(Session("User_ID"), RPT_Year, RPT_No, TAG_ID)
        Catch ex As Exception
            Exit Sub
        End Try
        BindTabData()
    End Sub

    Private Sub btnClearOld_Click(sender As Object, e As ImageClickEventArgs) Handles btnClearOld.Click
        Dim DETAIL_ID As Integer() = {}
        For Each Item As RepeaterItem In rptRemain.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim btnEdit As ImageButton = Item.FindControl("btnEdit")
            BL.Drop_RPT_ST_Detail_CheckFlag(btnEdit.CommandArgument)
        Next
        BindTabData()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        BindTabData()
    End Sub

    '---------------- Save Each Detail -----------
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        'Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("th-TH")

        Dim _tmp As Integer = UC_Detail.Save() '----------- Return DETAIL_ID If successed----
        If _tmp = 0 Then Exit Sub

        DETAIL_ID = _tmp '------------- For Saving Document -------------
        If Not SaveDocument() Then Exit Sub

        BindTabData()

    End Sub

    Private Function SaveDocument() As Boolean

        Dim Sector As DataTable = SectorData()

        '--------------Drop First-------------
        Dim SQL As String = "SELECT Sector_ID FROM RPT_Template_Sector WHERE RPT_Type_ID=" & RPT_Type_ID & " AND DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        For i As Integer = 0 To DT.Rows.Count - 1
            BL.Drop_Stationary_Template_Sector(DT.Rows(i).Item("Sector_ID"))
        Next

        '------------- And Then Save All--------
        SQL = "SELECT Top 0 Sector_ID,RPT_Type_ID,DETAIL_ID,Template_ID" & vbLf
        SQL &= " FROM RPT_Template_Sector" & vbLf
        Dim cmd As SqlCommandBuilder
        For i As Integer = 0 To Sector.Rows.Count - 1

            DA = New SqlDataAdapter(SQL, BL.ConnStr)
            DT = New DataTable
            DA.Fill(DT)

            Dim Sector_ID As Integer = BL.GetNew_Table_ID("RPT_Template_Sector", "Sector_ID")
            Dim DR As DataRow = DT.NewRow
            DR("Sector_ID") = Sector_ID
            DR("RPT_Type_ID") = EIR_BL.Report_Type.Stationary_Off_Routine_Report
            DR("DETAIL_ID") = DETAIL_ID
            DR("Template_ID") = Sector.Rows(i).Item("Template_ID")
            DT.Rows.Add(DR)

            cmd = New SqlCommandBuilder(DA)
            DA.Update(DT)

            For s As Integer = 1 To 4
                If Not IsDBNull(Sector.Rows(i).Item("File_" & s)) AndAlso Not IsNothing(Sector.Rows(i).Item("File_" & s)) Then
                    BL.Save_Stationary_Template_File(Sector_ID, s, Sector.Rows(i).Item("File_" & s))
                End If
            Next
        Next

        Return True

    End Function

End Class