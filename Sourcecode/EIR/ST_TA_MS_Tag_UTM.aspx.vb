﻿
Imports System.Data
Imports System.Data.SqlClient

Public Class ST_TA_MS_Tag_UTM
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL


    Private Property EDIT_TAG_TYPE_ID() As Integer
        Get
            If IsNumeric(ViewState("EDIT_TAG_TYPE_ID")) Then
                Return ViewState("EDIT_TAG_TYPE_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("EDIT_TAG_TYPE_ID") = value
        End Set
    End Property

    Private Property EDIT_TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("EDIT_TAG_ID")) Then
                Return ViewState("EDIT_TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("EDIT_TAG_ID") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            BindTag()
            pnlEdit.Visible = False



            BL.BindDDlPlant(ddl_Search_Plant)
            BL.BindDDlST_TA_TagType(ddl_Search_Tag_Type)
            BL.BindDDlArea(ddl_Search_Area)

        End If
        HideValidator()
    End Sub


    Private Sub BindTag()
        '----ใช้ตาราง Tag เดียวกับ station
        Dim SQL As String = " SELECT *,(SELECT COUNT(TAG_ID) FROM MS_ST_TAG_UTM WHERE TAG_ID=TB.TAG_ID)  Point_Amount FROM (" & vbNewLine
        SQL &= "   Select AREA_CODE+'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TAG.ROUTE_ID,MS_ST_TAG.AREA_ID,MS_ST_TAG.Update_Time" & vbNewLine
        SQL &= "   , 'MS_ST_TAG' To_Table " & vbNewLine
        SQL &= "   FROM MS_ST_TAG" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_TAG_TYPE ON  MS_ST_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_ST_Route ON MS_ST_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Area ON MS_ST_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
        SQL &= "   INNER JOIN MS_Process ON MS_ST_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine

        'SQL &= "   UNION ALL" & vbNewLine
        'SQL &= "   Select  AREA_CODE +'-'+PROC_CODE+'-'+TAG_No TAG_CODE,TAG_No ,TAG_ID,TAG_Name,TAG_TYPE_Name,MS_ST_TA_TAG.Active_Status,AREA_CODE,PROC_CODE,MS_ST_TA_TAG.TAG_TYPE_ID,MS_ST_Route.PLANT_ID,MS_ST_TA_TAG.ROUTE_ID,MS_ST_TA_TAG.AREA_ID,MS_ST_TA_TAG.Update_Time" & vbNewLine
        'SQL &= "   , 'MS_ST_TA_TAG' To_Table " & vbNewLine
        'SQL &= "   From MS_ST_TA_TAG" & vbNewLine
        'SQL &= "   INNER Join MS_ST_TAG_TYPE On  MS_ST_TA_TAG.TAG_TYPE_ID=MS_ST_TAG_TYPE.TAG_TYPE_ID" & vbNewLine
        'SQL &= "   INNER Join MS_ST_Route ON MS_ST_TA_TAG.ROUTE_ID=MS_ST_Route.ROUTE_ID" & vbNewLine
        'SQL &= "   INNER Join MS_PLANT On MS_PLANT.PLANT_ID=MS_ST_Route.PLANT_ID" & vbNewLine
        'SQL &= "   INNER Join MS_Area ON MS_ST_TA_TAG.AREA_ID=MS_Area.AREA_ID" & vbNewLine
        'SQL &= "   INNER Join MS_Process On MS_ST_TA_TAG.PROC_ID=MS_Process.PROC_ID" & vbNewLine
        SQL &= " ) TB " & vbNewLine
        SQL &= " WHERE TAG_TYPE_ID in (" & BL.TAG_TYPE_For_TA & ") " & vbNewLine

        Dim WHERE As String = ""

        If ddl_Search_Tag_Type.SelectedIndex > 0 Then
            WHERE &= " TB.TAG_TYPE_ID=" & ddl_Search_Tag_Type.Items(ddl_Search_Tag_Type.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " TB.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " TB.ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " And "
        End If
        If ddl_Search_Area.SelectedIndex > 0 Then
            WHERE &= " TB.AREA_ID=" & ddl_Search_Area.Items(ddl_Search_Area.SelectedIndex).Value & " And "
        End If

        If WHERE <> "" Then
            SQL &= " AND " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= "ORDER BY AREA_CODE+'-'+PROC_CODE +'-' + TAG_NO" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("MS_ST_TA_TAG") = DT

        Navigation.SesssionSourceName = "MS_ST_TA_TAG"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")
        Dim lblPointAmount As Label = e.Item.FindControl("lblPointAmount")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblTagNo.Text = e.Item.DataItem("TAG_CODE")
        lblTagName.Text = e.Item.DataItem("TAG_Name")
        lblTagType.Text = e.Item.DataItem("TAG_TYPE_Name")
        lblTo_Table.Text = e.Item.DataItem("To_Table")

        If (e.Item.DataItem("Point_Amount") > 0) Then
            lblPointAmount.Text = e.Item.DataItem("Point_Amount")
        End If
        lblTo_Table.Text = e.Item.DataItem("To_Table")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

    End Sub

    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblTagType As Label = e.Item.FindControl("lblTagType")

        EDIT_TAG_ID = btnEdit.Attributes("TAG_ID")
        Dim lblTo_Table As Label = e.Item.FindControl("lblTo_Table")
        Dim To_Table As String = lblTo_Table.Text


        Select Case e.CommandName
            Case "Edit"
                ClearPanelEdit()

                'btnCreate.Visible = False
                pnlEdit.Visible = True
                pnlListTag.Visible = False
                lblUpdateMode.Text = "Update"

                '------------------------------------

                '-------------ตรวจสอบเป็น TAG จาก Station หรือ Turnaround-----------------------

                Dim SQL As String = ""
                SQL = " "
                SQL &= " SELECT " & To_Table & ".* ,MS_PLANT.PLANT_ID" & vbNewLine
                SQL &= " FROM " & To_Table & " LEFT JOIN MS_ST_ROUTE ON " & To_Table & ".ROUTE_ID=MS_ST_ROUTE.ROUTE_ID" & vbNewLine
                SQL &= " LEFT JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_ST_ROUTE.PLANT_ID" & vbNewLine
                SQL &= " LEFT JOIN MS_ST_Tag_Type ON " & To_Table & ".TAG_TYPE_ID=MS_ST_Tag_Type.TAG_TYPE_ID" & vbNewLine
                SQL &= " WHERE " & To_Table & ".TAG_ID=" & EDIT_TAG_ID

                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "TAG Not Found"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If

                Dim PLANT_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then PLANT_ID = DT.Rows(0).Item("PLANT_ID")
                Dim ROUTE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("ROUTE_ID")) Then ROUTE_ID = DT.Rows(0).Item("ROUTE_ID")
                Dim AREA_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("AREA_ID")) Then AREA_ID = DT.Rows(0).Item("AREA_ID")
                Dim PROC_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PROC_ID")) Then PROC_ID = DT.Rows(0).Item("PROC_ID")
                Dim TAG_TYPE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("TAG_TYPE_ID")) Then TAG_TYPE_ID = DT.Rows(0).Item("TAG_TYPE_ID")

                btnCreate_Template.Focus()
                EDIT_TAG_TYPE_ID = TAG_TYPE_ID
                lblTag_Edit.Text = lblTagNo.Text & " : " & lblTagName.Text
                lblEquipement_Type.Text = lblTagType.Text
                BindTag_UTM()
            Case "ToggleStatus"

                Dim SQL As String = "UPDATE " & To_Table & " Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  TAG_ID=" & EDIT_TAG_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTag()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select

    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""

        lblTag_Edit.Text = ""
        lblEquipement_Type.Text = ""
        'btnCreate.Visible = True


    End Sub


    Private Sub BindTag_UTM()
        Dim SQL As String = ""
        SQL = " "
        SQL &= " SELECT * FROM MS_ST_TAG_UTM WHERE TAG_ID=" & EDIT_TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If (DT.Rows.Count = 0) Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
        End If

        rptTemplate.DataSource = DT
        rptTemplate.DataBind()

    End Sub

    Private Sub rptTemplate_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptTemplate.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub


        Dim txtLocation_Name As TextBox = e.Item.FindControl("txtLocation_Name")
        Dim txtPoint_Code As TextBox = e.Item.FindControl("txtPoint_Code")
        Dim txtPoint_number As TextBox = e.Item.FindControl("txtPoint_number")

        'Dim txtX_Start As TextBox = e.Item.FindControl("txtX_Start")
        'Dim txtX_End As TextBox = e.Item.FindControl("txtX_End")
        Dim ddlY As DropDownList = e.Item.FindControl("ddlY")

        If Not IsDBNull(e.Item.DataItem("TAG_UTM_ID")) Then
            txtLocation_Name.Attributes("TAG_UTM_ID") = e.Item.DataItem("TAG_UTM_ID")
        Else
            txtLocation_Name.Attributes("TAG_UTM_ID") = 0
        End If

        ImplementJavaIntegerText(txtPoint_number, False)
        txtLocation_Name.Text = e.Item.DataItem("Location_Name").ToString()
        txtPoint_Code.Text = e.Item.DataItem("Point_Code").ToString()
        If Not IsDBNull(e.Item.DataItem("Point_number")) Then
            txtPoint_number.Text = e.Item.DataItem("Point_number")
        End If

        'If Not IsDBNull(e.Item.DataItem("X_Start")) Then
        '    txtX_Start.Text = e.Item.DataItem("X_Start")
        'End If
        'If Not IsDBNull(e.Item.DataItem("X_End")) Then
        '    txtX_End.Text = e.Item.DataItem("X_End")
        'End If

        If Not IsDBNull(e.Item.DataItem("Y_Template")) Then
            ddlY.SelectedValue = e.Item.DataItem("Y_Template")
        End If

        'Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")
        'If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then

        '    btnToggle.ImageUrl = "resources/images/icons/tick.png"
        'Else

        '    btnToggle.ImageUrl = "resources/images/icons/cross.png"
        'End If

    End Sub


    Function GetCurrentData() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("TAG_UTM_ID")
        dt.Columns.Add("Location_Name")
        'dt.Columns.Add("X_Start")
        'dt.Columns.Add("X_End")
        dt.Columns.Add("Point_Code")
        dt.Columns.Add("Point_number")
        dt.Columns.Add("Y_Template")

        Dim dr As DataRow
        For i As Integer = 0 To rptTemplate.Items.Count - 1

            Dim txtLocation_Name As TextBox = rptTemplate.Items(i).FindControl("txtLocation_Name")
            'Dim txtX_Start As TextBox = rptTemplate.Items(i).FindControl("txtX_Start")
            'Dim txtX_End As TextBox = rptTemplate.Items(i).FindControl("txtX_End")
            Dim txtPoint_Code As TextBox = rptTemplate.Items(i).FindControl("txtPoint_Code")
            Dim txtPoint_number As TextBox = rptTemplate.Items(i).FindControl("txtPoint_number")
            Dim ddlY As DropDownList = rptTemplate.Items(i).FindControl("ddlY")

            dr = dt.NewRow

            dr("TAG_UTM_ID") = txtLocation_Name.Attributes("TAG_UTM_ID")
            dr("Location_Name") = txtLocation_Name.Text
            'dr("X_Start") = txtX_Start.Text
            'dr("X_End") = txtX_End.Text
            dr("Point_Code") = txtPoint_Code.Text
            dr("Point_number") = txtPoint_number.Text
            dr("Y_Template") = ddlY.SelectedValue

            dt.Rows.Add(dr)

        Next

        Return dt
    End Function

    Private Sub btnAddLocation_Click(sender As Object, e As EventArgs) Handles btnAddLocation.Click
        Dim DT As DataTable = GetCurrentData()
        Dim DR As DataRow

        DR = DT.NewRow
        DT.Rows.Add(DR)

        rptTemplate.DataSource = DT
        rptTemplate.DataBind()

    End Sub

    Private Sub btnCreate_Template_Click(sender As Object, e As EventArgs) Handles btnCreate_Template.Click
        Dim DT_Current As DataTable = GetCurrentData()
        '--Validate--


        If DT_Current.Rows.Count > 0 Then
            DT_Current.DefaultView.RowFilter = "Location_Name ='' OR Point_Code='' OR Point_number='' OR Y_Template =0"
            If DT_Current.DefaultView.Count > 0 Then
                lblValidation.Text = " กรุณาตรวจสอบข้อมูลให้ครบ "
                pnlValidation.Visible = True
                Exit Sub
            End If

        End If

        '--Save--
        Dim SQL As String = ""
        SQL &= " SELECT * FROM MS_ST_TAG_UTM WHERE TAG_ID=" & EDIT_TAG_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        '--Delete--

        Dim SQL_Del As String = "Delete FROM MS_ST_TAG_UTM WHERE TAG_ID=" & EDIT_TAG_ID
        Dim Command As New SqlCommand
        Dim Conn As New SqlConnection(BL.ConnStr)
        Try
            Conn.Open()
            With Command
                .Connection = Conn
                .CommandType = CommandType.Text
                .CommandText = SQL_Del
                .ExecuteNonQuery()
                .Dispose()
            End With
            Conn.Close()
            Conn.Dispose()
        Catch ex As Exception
            lblBindingError.Text = "Invalid parameter"
            pnlBindingError.Visible = True
            Exit Sub
        End Try


        '--Save--
        For i As Integer = 0 To DT_Current.Rows.Count - 1
                DR = DT.NewRow
                DR("TAG_UTM_ID") = BL.GetNew_Table_ID("MS_ST_TAG_UTM", "TAG_UTM_ID")
                DR("TAG_ID") = EDIT_TAG_ID
                DR("Location_Name") = DT_Current.Rows(i).Item("Location_Name").ToString()
                'DR("X_Start") = DT_Current.Rows(i).Item("X_Start")
                'DR("X_End") = DT_Current.Rows(i).Item("X_End")
                DR("Point_Code") = DT_Current.Rows(i).Item("Point_Code")
                DR("Point_number") = DT_Current.Rows(i).Item("Point_number")
                DR("Y_Template") = DT_Current.Rows(i).Item("Y_Template")

                DT.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA)
                Try
                    DA.Update(DT)
                Catch ex As Exception

                End Try
            Next
        BindTag()
        pnlEdit.Visible = False
        pnlListTag.Visible = True
        'Response.Redirect("ST_TA_MS_Tag_UTM.aspx")

    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        BindTag()
        pnlEdit.Visible = False
        pnlListTag.Visible = True
        'Response.Redirect("ST_TA_MS_Tag_UTM.aspx")

    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub

    Private Sub ddl_Search_Tag_Type_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_Search_Tag_Type.SelectedIndexChanged, ddl_Search_Plant.SelectedIndexChanged, ddl_Search_Area.SelectedIndexChanged
        BindTag()
    End Sub

    Private Sub rptTemplate_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptTemplate.ItemCommand

        Select Case e.CommandName
            Case "Delete"

                '--ก่อน ลบให้ตรวจสอบก่อนว่าใบประเมินมีใช้ Template นี้หรือไม่



                '--เอาแถวที่ต้องการลบออก
                Dim DT_Current As DataTable = GetCurrentData()

                DT_Current.Rows.RemoveAt(e.Item.ItemIndex)

                rptTemplate.DataSource = DT_Current
                rptTemplate.DataBind()




        End Select
    End Sub






#End Region

End Class