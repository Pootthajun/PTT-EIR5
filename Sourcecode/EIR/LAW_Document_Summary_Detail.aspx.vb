﻿Imports EIR

Public Class LAW_Document_Summary_Detail
    Inherits System.Web.UI.Page

    Dim CL As New LawClass
    Dim BL As New EIR_BL
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            lblDocumentPlanID.Text = Request("id")
            BindDocumentInfo(Request("id"))
        End If
    End Sub

    Private Sub BindDocumentInfo(DocumentPlanID As Long)
        Dim dt As DataTable = CL.GetDataDocumentPlan(DocumentPlanID)
        If dt.Rows.Count > 0 Then
            lblDocumentName.Text = dt.Rows(0)("document_name")
            lblPlantCode.Text = dt.Rows(0)("plant_code")
            lblTagNo.Text = dt.Rows(0)("tag_no")
            lblYear.Text = dt.Rows(0)("document_year")
            'If Convert.IsDBNull(dt.Rows(0)("next_notice_date")) = False Then txtNextNoticeDate.Text = Convert.ToDateTime(dt.Rows(0)("next_notice_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))

            Dim pStatus As DocumentPlanStatusClass = CL.GetDocumentPlanStatus(DocumentPlanID)
            Select Case pStatus.PlanStatus
                Case "WAITING"
                    lblPlanStatus.Text = "<span style='color:blue'><b>WAITING</b></span>"
                Case "NOTICE"
                    lblPlanStatus.Text = "<span style='color:orange'><b>NOTICE</b></span>"
                Case "CRITICAL"
                    lblPlanStatus.Text = "<span style='color:red'><b>LATE</b></span><img src='resources/images/icons/alert.gif' />"
                Case "COMPLETE"
                    lblPlanStatus.Text = "<span style='color:green'><b>COMPLETE</b></span>"
            End Select

            'Display Chart

            Dim PlantDT As DataTable = CL.GetDocumentPlanTable(DocumentPlanID)
            rptData.DataSource = PlantDT
            rptData.DataBind()

            Dim ChartDT As DataTable = CL.GetDocumentChartData(DocumentPlanID)
            DisplayChart(ChartDT, lblYear.Text, dt.Rows(0)("plant_id"), lblPlantCode.Text, DocumentPlanID)

            Dim pDt As DataTable = CL.GetListDocumentPlanPaper(DocumentPlanID)
            UC_DocumentTreePlanPaper1.SetPlanPaperData(pDt)
        End If
        dt.Dispose()
    End Sub

    Private Sub rptData_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblCriticalDate As Label = e.Item.FindControl("lblCriticalDate")
        Dim lblDocumentName As Label = e.Item.FindControl("lblDocumentName")
        Dim lblPlan As Label = e.Item.FindControl("lblPlan")
        Dim lblActual As Label = e.Item.FindControl("lblActual")

        'lblCriticalDate.Text = Convert.ToDateTime(e.Item.DataItem("plan_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))
        lblCriticalDate.Text = BL.ReportGridTime(e.Item.DataItem("plan_date"))
        lblDocumentName.Text = e.Item.DataItem("paper_name")
        lblPlan.Text = e.Item.DataItem("plan_percent")
        lblActual.Text = e.Item.DataItem("actual_percent")
    End Sub




    Protected Sub DisplayChart(ByVal DT_ As DataTable, ByVal Year As Integer, ByVal Plant_ID As Integer, ByVal Plant_Name As String, ByVal DocumentPlanID As Long)

        ChartMain.ChartAreas("ChartArea1").AxisX.CustomLabels.Clear()
        ChartMain.Titles("Title1").Text = "Overall this progress "

        Dim culture As New System.Globalization.CultureInfo("en-US")
        Dim C As New Converter

        Dim Plant As String = ""
        If Plant_ID > 0 Then
            Plant = "  Plant  " & Plant_Name
        End If

        Dim PlanValue As Integer = 0
        Dim ActualValue As Integer = 0

        '---หาวันทั้งหมดที่มี action
        Dim DT_Date As DataTable = CL.GetAllDate_PlanActual(DocumentPlanID)
        Dim DT_StartDate As DataTable = CL.GetStartDate_PlanActual(DocumentPlanID)
        Dim DR_Start As DataRow
        If DT_StartDate.Rows.Count > 0 Then
            DR_Start = DT_Date.NewRow
            DR_Start("Action_date") = DT_StartDate.Rows(0).Item("Action_date")
            DT_Date.Rows.Add(DR_Start)
        End If
        ''---หาวันที่ปัจจุบัน เพื่อแสดง series3
        'Dim _Now As String = Convert.ToDateTime(Now).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))
        'DT_Date.DefaultView.RowFilter = "plan_date='" & _Now.ToString() & "'"
        'If DT_Date.DefaultView.Count = 0 Then
        '    DR_Start = DT_Date.NewRow
        '    DR_Start("Action_date") = Now
        '    DT_Date.Rows.Add(DR_Start)
        'End If
        'DT_Date.DefaultView.Sort = "Action_date asc"
        'DT_Date = DT_Date.DefaultView.ToTable


        '=====================

        Dim DT As DataTable = CL.GetDocumentChartData_PlanActual(DocumentPlanID)
        '----จัด Data Plan----
        Dim DT_Plan As New DataTable
        DT_Plan.Columns.Add("plan_date", GetType(Date))
        DT_Plan.Columns.Add("paper_name")
        DT_Plan.Columns.Add("plan_percent")
        Dim DR As DataRow

        DT.DefaultView.Sort = " plan_date asc"
        DT = DT.DefaultView.ToTable
        Dim _plan_notice_date As String = ""
        For i As Integer = 0 To DT.Rows.Count - 1
            If (_plan_notice_date <> DT.Rows(i).Item("plan_date").ToString()) Then
                _plan_notice_date = DT.Rows(i).Item("plan_date").ToString()

                Dim DT_paper_Name As New DataTable
                DT.DefaultView.RowFilter = "plan_date='" & DT.Rows(i).Item("plan_date").ToString() & "'"
                DT_paper_Name = DT.DefaultView.ToTable
                Dim List_paper_Name As String = ""
                If (DT_paper_Name.Rows.Count > 1) Then
                    For paper As Integer = 0 To DT_paper_Name.Rows.Count - 1
                        List_paper_Name &= DT_paper_Name.Rows(paper).Item("paper_name").ToString() & " : " & DT_paper_Name.Rows(paper).Item("plan_percent") & " %" & vbNewLine
                    Next
                Else
                    List_paper_Name = DT.Rows(i).Item("paper_name").ToString() & " : " & (DT.Rows(i).Item("plan_percent")) & " %"
                End If
                DR = DT_Plan.NewRow
                DR("plan_date") = DT.Rows(i).Item("plan_date")
                DR("paper_name") = List_paper_Name
                DR("plan_percent") = DT.Compute("SUM(plan_percent)", " plan_date='" & DT.Rows(i).Item("plan_date") & "'")
                DT_Plan.Rows.Add(DR)

            End If


        Next

        If DT_StartDate.Rows.Count > 0 Then
            DR_Start = DT_Plan.NewRow
            DR_Start("plan_date") = DT_StartDate.Rows(0).Item("Action_date")
            DR_Start("paper_name") = ""
            DR_Start("plan_percent") = 0
            DT_Plan.Rows.Add(DR_Start)
        End If
        DT_Plan.DefaultView.Sort = "plan_date asc"
        DT_Plan = DT_Plan.DefaultView.ToTable



        '----จัด Data Actual----
        Dim DT_Actual As New DataTable
        DT_Actual.Columns.Add("Actual_date", GetType(Date))
        DT_Actual.Columns.Add("paper_name")
        DT_Actual.Columns.Add("actual_percent")

        DT.DefaultView.Sort = " actual_date asc"
        DT.DefaultView.RowFilter = " actual_date IS NOT NULL"
        DT = DT.DefaultView.ToTable


        Dim SUM_actual_percent As Integer = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            If (_plan_notice_date <> DT.Rows(i).Item("Actual_date").ToString()) Then
                _plan_notice_date = DT.Rows(i).Item("Actual_date").ToString()

                Dim DT_paper_Name As New DataTable
                DT.DefaultView.RowFilter = "Actual_date='" & DT.Rows(i).Item("Actual_date").ToString() & "'"
                DT_paper_Name = DT.DefaultView.ToTable
                Dim List_paper_Name As String = ""
                If (DT_paper_Name.Rows.Count > 1) Then
                    For paper As Integer = 0 To DT_paper_Name.Rows.Count - 1
                        List_paper_Name &= DT_paper_Name.Rows(paper).Item("paper_name").ToString() & " : " & DT_paper_Name.Rows(paper).Item("actual_percent") & " %" & vbNewLine
                    Next
                Else
                    List_paper_Name = DT.Rows(i).Item("paper_name").ToString() & " : " & (DT.Rows(i).Item("actual_percent")) & " %"
                End If
                DR = DT_Actual.NewRow
                DR("Actual_date") = DT.Rows(i).Item("Actual_date")
                DR("paper_name") = List_paper_Name
                DR("actual_percent") = DT.Compute("SUM(actual_percent)", " Actual_date='" & DT.Rows(i).Item("Actual_date") & "'")
                SUM_actual_percent = SUM_actual_percent + DR("actual_percent")
                DT_Actual.Rows.Add(DR)

            End If
        Next

        If DT_StartDate.Rows.Count > 0 Then
            DR_Start = DT_Actual.NewRow
            DR_Start("Actual_date") = DT_StartDate.Rows(0).Item("Action_date")
            DR_Start("paper_name") = ""
            DR_Start("actual_percent") = 0
            DT_Actual.Rows.Add(DR_Start)
        End If
        DT_Actual.DefaultView.Sort = "Actual_date asc"
        DT_Actual = DT_Actual.DefaultView.ToTable



        '----ตรวจสอบ
        'If (SUM_actual_percent < 100) Then
        '    '---หาวันที่ปัจจุบัน เพื่อแสดง series3
        '    Dim _Now As String = Convert.ToDateTime(Now).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))
        '    Dim DT_Now As DataTable = DT_Date
        '    DT_Now.DefaultView.RowFilter = "Action_date='" & _Now.ToString() & "'"
        '    If DT_Now.DefaultView.Count = 0 Then
        '        DR_Start = DT_Date.NewRow
        '        DR_Start("Action_date") = Convert.ToDateTime(Now).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))
        '        DT_Date.Rows.Add(DR_Start)
        '    End If
        'End If

        DT_Date.DefaultView.RowFilter = ""
        DT_Date.DefaultView.Sort = "Action_date asc"
        DT_Date = DT_Date.DefaultView.ToTable

        Dim Actual_Point As Integer = 0
        For i As Integer = 0 To DT_Date.Rows.Count - 1
            Dim XDate As String = Convert.ToDateTime(DT_Date.Rows(i).Item("Action_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))

            If XDate = Convert.ToDateTime(Now).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US")) Then
                ChartMain.Series("Series3").Points.AddXY(i + 1, 0)
                ChartMain.Series("Series3").Points.AddXY(i + 1, 100)
                ChartMain.Series("Series3").BorderDashStyle = DataVisualization.Charting.ChartDashStyle.DashDot
                ChartMain.Series("Series3").Points(0).ToolTip = "Today"
            End If

            'Plan

            DT_Plan.DefaultView.RowFilter = "plan_date='" & DT_Date.Rows(i).Item("Action_date").ToString() & "'"
            If DT_Plan.DefaultView.Count > 0 Then
                PlanValue = (PlanValue + DT_Plan.DefaultView(0).Item("plan_percent"))
                Dim P As Integer = DT_Plan.Rows.IndexOf(DT_Plan.DefaultView(0).Row)
                ChartMain.Series("Series1").Points.AddXY(i + 1, PlanValue)

                'If i > 0 Then
                '    ChartMain.Series("Series1").Points(P).ToolTip = "Plan = " & PlanValue & " %" & vbNewLine & XDate & vbNewLine & DT_Plan.DefaultView(0)("paper_name").ToString()
                'End If

                If i > 0 Then
                    ChartMain.Series("Series1").Points(P).ToolTip = "Plan = " & PlanValue & " %" & vbNewLine & XDate & vbNewLine & DT_Plan.DefaultView(0)("paper_name").ToString()
                Else
                    ChartMain.Series("Series1").Points(P).ToolTip = "Notice Date =  " & XDate

                End If

                ChartMain.Series("Series1").MarkerStyle = DataVisualization.Charting.MarkerStyle.Circle
                ChartMain.Series("Series1").MarkerSize = 6


            End If


            'Actual  
            DT_Actual.DefaultView.RowFilter = "Actual_date='" & DT_Date.Rows(i).Item("Action_date").ToString() & "'"
            If DT_Actual.DefaultView.Count > 0 Then

                For j As Integer = 0 To DT_Actual.DefaultView.Count - 1

                    ActualValue = (ActualValue + DT_Actual.DefaultView(j).Item("actual_percent"))
                    Dim A As Integer = DT_Actual.Rows.IndexOf(DT_Actual.DefaultView(j).Row)

                    '--แก้ไขเป็นเริ่มจาก 0  ไม่ต้องย้อนหลัง 5 วัน
                    'ChartMain.Series("Series2").Points.AddXY(i + 1, ActualValue)

                    ChartMain.Series("Series2").Points.AddXY(i + 1, ActualValue)
                    If i > 0 Then
                        ChartMain.Series("Series2").Points(Actual_Point).ToolTip = "Actual = " & ActualValue & " %" & vbNewLine & XDate & vbNewLine & DT_Actual.DefaultView(j)("paper_name").ToString()
                    End If

                    ChartMain.Series("Series2").Points(Actual_Point).MarkerSize = 6
                    ChartMain.Series("Series2").MarkerStyle = DataVisualization.Charting.MarkerStyle.Circle

                    Actual_Point = Actual_Point + 1
                Next

            Else
                If (DT_Date.Rows(i).Item("Action_date") < DT_Actual.Rows(DT_Actual.Rows.Count - 1).Item("Actual_date")) Then

                    ChartMain.Series("Series2").Points.AddXY(i + 1, ActualValue)
                    If i > 0 Then
                        ChartMain.Series("Series2").Points(Actual_Point).ToolTip = ""
                    End If

                    ChartMain.Series("Series2").Points(Actual_Point).MarkerSize = 3
                    ChartMain.Series("Series2").MarkerStyle = DataVisualization.Charting.MarkerStyle.Circle
                    Actual_Point = Actual_Point + 1
                End If


            End If

            'If i = 0 Then
            '    '---หาวันที่ Notic ,มาแปะแทน
            '    'ChartMain.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(i, i + 1, "rrr")
            'Else
            '    ChartMain.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(i, i + 2, XDate)

            'End If

            If i = 0 Then
                ChartMain.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(i, i + 2, XDate & vbNewLine & "Notice Date")
                ChartMain.ChartAreas("ChartArea1").AxisX.LabelStyle.ForeColor = Drawing.Color.Orange
            Else
                ChartMain.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(i, i + 2, XDate)
                ChartMain.ChartAreas("ChartArea1").AxisX.LabelStyle.ForeColor = Drawing.Color.Black
            End If



        Next






    End Sub

    Private Sub UC_DocumentTreePlanPaper1_SetPercentComplete(sender As UC_DocumentTreePlanPaper, PercentComplete As Integer) Handles UC_DocumentTreePlanPaper1.SetPercentComplete
        BindDocumentInfo(lblDocumentPlanID.Text)

        'Dim UC As UC_DocumentTreePlanPaper = DirectCast(sender, UC_DocumentTreePlanPaper)
        'Dim RowItem As RepeaterItem = UC.Parent.Parent.Parent

        'Dim lblPercentComplete As Label = RowItem.FindControl("lblPercentComplete")
        'Dim lblUploadDate As Label = RowItem.FindControl("lblUploadDate")

        'lblPercentComplete.Text = PercentComplete
        'lblUploadDate.Text = DateTime.Now.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("th-TH"))
    End Sub

    'Private Sub txtNextNoticeDate_TextChanged(sender As Object, e As EventArgs) Handles txtNextNoticeDate.TextChanged
    '    If txtNextNoticeDate.Text <> "" Then
    '        If IsDate(txtNextNoticeDate.Text) = True Then
    '            Dim c As New Converter
    '            CL.UpdateNextNoticeDate(lblDocumentPlanID.Text, c.StringToDate(txtNextNoticeDate.Text, "dd-MMM-yyyy"))
    '        End If
    '    End If

    '    BindDocumentInfo(lblDocumentPlanID.Text)
    'End Sub




End Class