﻿Imports System.Data
Imports System.Data.SqlClient
Imports EIR

Public Class ST_TA_Daily_Detail
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New EIR_BL
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Turnaround_Inspection_Reports

    'Public Event UpdateCompleted(ByRef sender As GL_DialogUploadImage)

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private Property DETAIL_STEP_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_STEP_ID")) Then
                Return ViewState("DETAIL_STEP_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_STEP_ID") = value
        End Set
    End Property

    Private Property TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private Property TAG_TYPE_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_TYPE_ID")) Then
                Return ViewState("TAG_TYPE_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_TYPE_ID") = value
        End Set
    End Property

    Private Property INSP_ID() As Integer
        Get
            If IsNumeric(ViewState("INSP_ID")) Then
                Return ViewState("INSP_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("INSP_ID") = value
        End Set
    End Property

    Private Property PLANT_ID() As Integer
        Get
            If IsNumeric(ViewState("PLANT_ID")) Then
                Return ViewState("PLANT_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
        End Set
    End Property

    Private Property INSP_STATUS_ID() As Integer    '-- สถานะของจุดตรวจ
        Get
            If IsNumeric(ViewState("INSP_STATUS_ID")) Then
                Return ViewState("INSP_STATUS_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("INSP_STATUS_ID") = value
        End Set
    End Property

    Private Property Step_ID() As Integer    '-- Step การตรวจ 1as found ...
        Get
            If IsNumeric(ViewState("Step_ID")) Then
                Return ViewState("Step_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("Step_ID") = value
        End Set
    End Property

    Private Property DETAIL_ID() As Integer
        Get
            If IsNumeric(ViewState("DETAIL_ID")) Then
                Return ViewState("DETAIL_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DETAIL_ID") = value
        End Set
    End Property

    Private Property RPT_DATE() As String
        Get
            If (ViewState("RPT_DATE") <> "") Then
                Return ViewState("RPT_DATE")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("RPT_DATE") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Public Property TAG_CLASS() As EIR_BL.Tag_Class
        Get
            If ViewState("TagClass") = "RO" Then
                Return EIR_BL.Tag_Class.Rotating
            Else
                Return EIR_BL.Tag_Class.Stationary
            End If
        End Get
        Set(ByVal value As EIR_BL.Tag_Class)
            Select Case value
                Case EIR_BL.Tag_Class.Rotating
                    ViewState("TagClass") = "RO"
                Case Else
                    ViewState("TagClass") = "ST"
            End Select
        End Set
    End Property


    Private Property UNIQUE_SECTOR_POPUP_ID() As String
        Get
            If (ViewState("UNIQUE_SECTOR_POPUP_ID") <> "") Then
                Return ViewState("UNIQUE_SECTOR_POPUP_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUE_SECTOR_POPUP_ID") = value
        End Set
    End Property



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MyTime", "MyTime();", True)

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            TAG_ID = Request.QueryString("TAG_ID")
            TAG_TYPE_ID = Request.QueryString("TAG_TYPE_ID")
            DETAIL_ID = Request.QueryString("DETAIL_ID")

            dialogCreateReport.Visible = False
            BL.BindDDl_Type_INSP_Name(ddl_dialog_INSP_Name, TAG_TYPE_ID)
            'pnlList.Visible = True
            pnlEdit.Visible = True

            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("ST_TA_Insp_Absorber_Edit2.aspx", True)
                Exit Sub
            Else
                'Dim DA As New SqlDataAdapter("SELECT * FROM RPT_ST_TA_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
                'Dim DT As New DataTable
                'DA.Fill(DT)
                'If DT.Rows.Count = 0 Then
                '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='ST_TA_Inspection_Summary.aspx'", True)
                '    Exit Sub
                'End If
            End If

            '--------------Check Permisson----------------------


            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")

            pnlList.Visible = False
            pnlEdit.Visible = True

            BindTabData()
            Data_Detail_Step()

            '--------------ตรวจสอบเข้ามาครั้งแรกถ้าไม่มีรายการให้แสดง dialog create report------------------------------
            'Dim DT As DataTable = BL.Get_ST_TA_Inspection_For_Equipment(RPT_Year, RPT_No, TAG_TYPE_ID, DETAIL_ID, "RPT_ST_TA_Detail_Step")

            'If (DT.Rows.Count = 0) Then
            '    dialogCreateReport.Visible = True
            '    pnlValidation_dialog.Visible = False

            'Else
            '    BindComponents()
            'End If

        End If
        pnlValidation.Visible = False
        pnlBindingSuccess.Visible = False


    End Sub


    Private Sub BindData_Detail()

        pnlValidation.Visible = False
        pnlBindingSuccess.Visible = False

        pnlList.Visible = False
        pnlEdit.Visible = True
        BindTemplate() '--
        '--------------Unvisible Select Template------------
        UC_Select_Template.CloseDialog()

        '---------------Header------------------------
        '---Header--
        lbl_Tag_Code_Edit.Text = lbl_Tag_Code.Text
        lbl_Tag_Name_Edit.Text = lbl_Tag_Name.Text

        lbl_Plant_Edit.Text = lbl_Plant.Text
        lbl_Equipment_Edit.Text = lbl_Equipment.Text
        lbl_Year_Edit.Text = lbl_Year.Text

        ImplementJavaIntegerText(txtEmployees_Count, True)
        BL.BindDDlLevel(ddl_Level)
        '----------------------------------------------

        '---------------Property-------------------------------
        DialogAbsorber.TAG_CODE = ""
        DialogAbsorber.TAG_ID = TAG_ID
        DialogAbsorber.TAG_TYPE_ID = TAG_TYPE_ID
        DialogAbsorber.TAG_Name = ""
        DialogAbsorber.TAG_TYPE_Name = TAG_TYPE_ID
        DialogAbsorber.TO_TABLE_MS = "MS_ST_TA_TAG"
        DialogAbsorber.TO_TABLE_SPEC = "MS_ST_ABSORBER_SPEC"
        DialogAbsorber.BindHeader()
        DialogAbsorber.ShowDialog()

        '-------------------Detail-----------------------------------
        Dim SQL_Date As String = ""
        SQL_Date &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        'SQL_Date &= "    WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & "  AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID & " AND RPT_DATE='" & RPT_DATE & "'"
        SQL_Date &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA_Date As New SqlDataAdapter(SQL_Date, BL.ConnStr)
        Dim DT_Date As New DataTable
        DA_Date.Fill(DT_Date)

        If Not IsDBNull(DT_Date.Rows(0).Item("RPT_Date")) Then
            txt_Rpt_Date.Text = BL.ReportProgrammingDate(DT_Date.Rows(0).Item("RPT_Date"))

            'UC_DialogCorrosion.RPT_Date = BL.ReportProgrammingDate(DT_Date.Rows(0).Item("RPT_Date"))
        Else
            txt_Rpt_Date.Text = ""
        End If

        'txtTime_Start.Text = lblTimeStart.Text
        'txtTime_End.Text = lblTimeEnd.Text

        If Not IsDBNull(DT_Date.Rows(0).Item("Employees")) Then
            txtEmployees_Count.Text = DT_Date.Rows(0).Item("Employees")  '-------แสดงจำนวนพรักงานที่ทำงาน
        End If
        If Not IsDBNull(DT_Date.Rows(0).Item("STATUS_ID")) Then
            ddl_Level.SelectedValue = DT_Date.Rows(0).Item("STATUS_ID")
        End If
        If Not IsDBNull(DT_Date.Rows(0).Item("RPT_Status")) Then
            ddlProgress.SelectedValue = DT_Date.Rows(0).Item("RPT_Status")
        Else
            ddlProgress.SelectedValue = 0
        End If

        '----------------Dialog image-----------------
        TAG_CLASS = TAG_CLASS

        Me.Visible = True

        '------------------ Officer ------------------
        BL.BindCmbReportOfficer(cmbCollector, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Collector)
        BL.BindCmbReportOfficer(cmbInspector, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Inspector)
        BL.BindCmbReportOfficer(cmbEngineer, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Engineer)
        BL.BindCmbReportOfficer(cmbAnalyst, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Approver)

        '------1. ตรวจสอบการการ Insp
        Dim SQL As String = ""
        SQL &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        SQL &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If (DT.Rows.Count = 0) Then
            '------2. สร้างรายการของ Insp
            Dim DR As DataRow
            DR = DT.NewRow
            DR("DETAIL_ID") = DETAIL_ID
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("TAG_ID") = TAG_ID
            DR("INSP_ID") = INSP_ID
            DR("RPT_STEP") = 0
            DR("RPT_Status") = 0
            DR("Created_Time") = Now
            DR("Created_By") = Session("USER_ID")
            DT.Rows.Add(DR)

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
                DT.AcceptChanges()
            Catch ex As Exception
                lblValidation.Text = "Invalid parameter"
                pnlValidation.Visible = True
                Exit Sub
            End Try
        Else
            ''------3. ถ้ามีรายการ  ดึงข้อมูลมาแสดง


            '------------------ Officer ------------------
            If Not IsDBNull(DT.Rows(0).Item("Officer_Collector")) Then
                BL.BindCmbST_TAReportOfficer(cmbCollector, RPT_Year, RPT_No, PLANT_ID, EIR_BL.User_Level.Collector, DT.Rows(0).Item("Officer_Collector"))
            Else
                BL.BindCmbST_TAReportOfficer(cmbCollector, RPT_Year, RPT_No, PLANT_ID, EIR_BL.User_Level.Collector)
            End If

            If Not IsDBNull(DT.Rows(0).Item("Officer_Inspector")) Then
                BL.BindCmbST_TAReportOfficer(cmbInspector, RPT_Year, RPT_No, PLANT_ID, EIR_BL.User_Level.Inspector, DT.Rows(0).Item("Officer_Inspector"))
            ElseIf Not IsDBNull(DT.Rows(0).Item("Officer_Engineer")) Then
                BL.BindCmbST_TAReportOfficer(cmbInspector, RPT_Year, RPT_No, PLANT_ID, EIR_BL.User_Level.Inspector, DT.Rows(0).Item("Officer_Engineer"))
            Else
                BL.BindCmbST_TAReportOfficer(cmbInspector, RPT_Year, RPT_No, PLANT_ID, EIR_BL.User_Level.Inspector)
            End If

            If Not IsDBNull(DT.Rows(0).Item("Officer_Engineer")) Then
                BL.BindCmbST_TAReportOfficer(cmbEngineer, RPT_Year, RPT_No, PLANT_ID, EIR_BL.User_Level.Engineer, DT.Rows(0).Item("Officer_Engineer"))
            Else
                BL.BindCmbST_TAReportOfficer(cmbEngineer, RPT_Year, RPT_No, PLANT_ID, EIR_BL.User_Level.Engineer)
            End If

            If Not IsDBNull(DT.Rows(0).Item("Officer_Analyst")) Then
                BL.BindCmbST_TAReportOfficer(cmbAnalyst, RPT_Year, RPT_No, PLANT_ID, EIR_BL.User_Level.Approver, DT.Rows(0).Item("Officer_Analyst"))
            Else
                BL.BindCmbST_TAReportOfficer(cmbAnalyst, RPT_Year, RPT_No, PLANT_ID, EIR_BL.User_Level.Approver)
            End If


        End If


    End Sub

    Private Sub BindTabData()
        '----------------Data Step----------------------------------------
        RPT_Year = Request.QueryString("RPT_Year")
        RPT_No = Request.QueryString("RPT_No")
        DETAIL_STEP_ID = Request.QueryString("DETAIL_STEP_ID")
        Dim DT_Detail_Step As DataTable = BL.Get_ST_TA_Inspection_For_Equipment(RPT_Year, RPT_No, DETAIL_STEP_ID)
        If (DT_Detail_Step.Rows.Count > 0) Then
            DETAIL_ID = DT_Detail_Step.Rows(0)("DETAIL_ID")
            TAG_ID = DT_Detail_Step.Rows(0)("TAG_ID")
            INSP_ID = DT_Detail_Step.Rows(0)("INSP_ID")
            INSP_STATUS_ID = DT_Detail_Step.Rows(0)("INSP_STATUS_ID")
            Step_ID = DT_Detail_Step.Rows(0)("Step_ID")
            TAG_TYPE_ID = DT_Detail_Step.Rows(0)("TAG_TYPE_ID")



            HTabAsFound.Attributes("class") = ""
            HTabAfterClean.Attributes("class") = ""
            HTabNDE.Attributes("class") = ""
            HTabRepair.Attributes("class") = ""
            HTabAfterRepair.Attributes("class") = ""
            HTabFinal.Attributes("class") = ""
            pnlDialogCorrosion.Visible = False
            Select Case Step_ID
                Case 1
                    HTabAsFound.Attributes("class") = "default-tab current"

                Case 2
                    HTabAfterClean.Attributes("class") = "default-tab current"

                Case 3
                    HTabNDE.Attributes("class") = "default-tab current"
                    pnlDialogCorrosion.Visible = True
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "MyTime", "MyTime();", True)
                    UC_DialogCorrosion.Bind_rptLocationTag(TAG_ID, TAG_TYPE_ID)

                Case 4
                    HTabRepair.Attributes("class") = "default-tab current"

                Case 5
                    HTabAfterRepair.Attributes("class") = "default-tab current"
                Case 6
                    HTabFinal.Attributes("class") = "default-tab current"


            End Select
        End If

        '------------------------------Header -----------------------------------
        Dim SQL As String = ""
        SQL &= "    Select TAG_ID,TAG_CODE,TAG_No,TAG_TYPE_ID,TAG_Name,TAG_TYPE_Name FROM VW_ST_TA_TAG " & vbNewLine
        SQL &= "        WHERE   TAG_ID = " & TAG_ID & " And " & vbNewLine
        SQL &= "        TAG_TYPE_ID = " & TAG_TYPE_ID & "  " & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        On Error Resume Next
        '---Header--
        lbl_Tag_Code.Text = DT.Rows(0).Item("TAG_CODE")
        lbl_Tag_Name.Text = DT.Rows(0).Item("TAG_Name")
        '--------dialog--------
        lbl_Tag_Code_dialog.Text = DT.Rows(0).Item("TAG_CODE")
        lbl_Tag_Name_dialog.Text = DT.Rows(0).Item("TAG_Name")


        '------------------------------Report For -----------------------------------
        SQL = " Select * FROM VW_REPORT_ST_TA_HEADER WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("ST_TA_Inspection_Summary.aspx")
            Exit Sub
        End If
        PLANT_ID = DT.Rows(0).Item("PLANT_ID")
        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Equipment.Text = DT.Rows(0).Item("TAG_TYPE_NAME")
        lbl_Year.Text = RPT_Year

        '--------dialog--------
        lbl_Plant_dialog.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Equipment_dialog.Text = DT.Rows(0).Item("TAG_TYPE_NAME")
        lbl_Year_dialog.Text = RPT_Year


    End Sub

    Private Sub Data_Detail_Step()
        pnlValidation.Visible = False
        pnlBindingSuccess.Visible = False

        pnlList.Visible = False
        pnlEdit.Visible = True

        BindTemplate()
        '--------------Unvisible Select Template------------
        UC_Select_Template.CloseDialog()
        'UC_DialogCorrosion.ClearTextbox()
        '---------------Header------------------------
        '---Header--
        lbl_Tag_Code_Edit.Text = lbl_Tag_Code.Text
        lbl_Tag_Name_Edit.Text = lbl_Tag_Name.Text

        lbl_Plant_Edit.Text = lbl_Plant.Text
        lbl_Equipment_Edit.Text = lbl_Equipment.Text
        lbl_Year_Edit.Text = lbl_Year.Text

        ImplementJavaIntegerText(txtEmployees_Count, True)
        BL.BindDDlLevel(ddl_Level)
        '----------------------------------------------

        '---------------Property-------------------------------
        DialogAbsorber.TAG_CODE = ""
        DialogAbsorber.TAG_ID = TAG_ID
        DialogAbsorber.TAG_TYPE_ID = TAG_TYPE_ID
        DialogAbsorber.TAG_Name = ""
        DialogAbsorber.TAG_TYPE_Name = TAG_TYPE_ID
        DialogAbsorber.TO_TABLE_MS = "MS_ST_TA_TAG"
        DialogAbsorber.TO_TABLE_SPEC = "MS_ST_ABSORBER_SPEC"
        DialogAbsorber.BindHeader()
        DialogAbsorber.ShowDialog()

        '-------------------Detail-----------------------------------
        Dim DT_Detail_Step As DataTable = BL.Get_ST_TA_Inspection_For_Equipment(RPT_Year, RPT_No, DETAIL_STEP_ID)
        If (DT_Detail_Step.Rows.Count > 0) Then
            lblEditComponents.Text = DT_Detail_Step.Rows(0).Item("INSP_Name").ToString()
            lblEditInspection_Status.Text = DT_Detail_Step.Rows(0).Item("STATUS_Name").ToString()

            If Not IsDBNull(DT_Detail_Step.Rows(0).Item("RPT_Date")) Then
                txt_Rpt_Date.Text = BL.ReportProgrammingDate(DT_Detail_Step.Rows(0).Item("RPT_Date"))
            Else
                txt_Rpt_Date.Text = ""
            End If
            If Not IsDBNull(DT_Detail_Step.Rows(0).Item("TIME_Start")) Then
                txtTime_Start.Text = DT_Detail_Step.Rows(0).Item("TIME_Start")
            End If
            If Not IsDBNull(DT_Detail_Step.Rows(0).Item("TIME_End")) Then
                txtTime_End.Text = DT_Detail_Step.Rows(0).Item("TIME_End")
            End If

            If Not IsDBNull(DT_Detail_Step.Rows(0).Item("Employees")) Then
                txtEmployees_Count.Text = DT_Detail_Step.Rows(0).Item("Employees")  '-------แสดงจำนวนพรักงานที่ทำงาน
            End If
            If Not IsDBNull(DT_Detail_Step.Rows(0).Item("STATUS_ID")) Then
                ddl_Level.SelectedValue = DT_Detail_Step.Rows(0).Item("STATUS_ID")
            Else
                ddl_Level.SelectedIndex = -1
            End If
            If Not IsDBNull(DT_Detail_Step.Rows(0).Item("RPT_Status")) Then
                ddlProgress.SelectedValue = DT_Detail_Step.Rows(0).Item("RPT_Status")
            Else
                ddlProgress.SelectedValue = 0
            End If

            txtCondition_Problem.Text = DT_Detail_Step.Rows(0).Item("Condition_Problem").ToString()
            txtPossible_Cause.Text = DT_Detail_Step.Rows(0).Item("Possible_Cause").ToString()
            txtRecommendation.Text = DT_Detail_Step.Rows(0).Item("Recommendation").ToString()


        End If

        '----------------Dialog image-----------------
        TAG_CLASS = TAG_CLASS

        Me.Visible = True

        '------------------ Officer ------------------
        BL.BindCmbReportOfficer(cmbCollector, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Collector)
        BL.BindCmbReportOfficer(cmbInspector, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Inspector)
        BL.BindCmbReportOfficer(cmbEngineer, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Engineer)
        BL.BindCmbReportOfficer(cmbAnalyst, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Approver)

        '------1. ตรวจสอบการการ Insp
        Dim SQL As String = ""
        SQL &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        SQL &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If (DT.Rows.Count = 0) Then
            '------2. สร้างรายการของ Insp
            Dim DR As DataRow
            DR = DT.NewRow
            DR("DETAIL_ID") = DETAIL_ID
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("TAG_ID") = TAG_ID
            DR("INSP_ID") = INSP_ID
            DR("RPT_STEP") = 0
            DR("RPT_Status") = 0
            DR("Created_Time") = Now
            DR("Created_By") = Session("USER_ID")
            DT.Rows.Add(DR)

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
                DT.AcceptChanges()
            Catch ex As Exception
                lblValidation.Text = "Invalid parameter"
                pnlValidation.Visible = True
                Exit Sub
            End Try
        Else

            '------------------ Officer ------------------
            If Not IsDBNull(DT.Rows(0).Item("Officer_Collector")) Then
                BL.BindCmbST_TAReportOfficer(cmbCollector, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Collector, DT.Rows(0).Item("Officer_Collector"))
            Else
                BL.BindCmbST_TAReportOfficer(cmbCollector, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Collector)
            End If

            If Not IsDBNull(DT.Rows(0).Item("Officer_Inspector")) Then
                BL.BindCmbST_TAReportOfficer(cmbInspector, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Inspector, DT.Rows(0).Item("Officer_Inspector"))
            ElseIf Not IsDBNull(DT.Rows(0).Item("Officer_Engineer")) Then
                BL.BindCmbST_TAReportOfficer(cmbInspector, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Inspector, DT.Rows(0).Item("Officer_Engineer"))
            Else
                BL.BindCmbST_TAReportOfficer(cmbInspector, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Inspector)
            End If

            If Not IsDBNull(DT.Rows(0).Item("Officer_Engineer")) Then
                BL.BindCmbST_TAReportOfficer(cmbEngineer, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Engineer, DT.Rows(0).Item("Officer_Engineer"))
            Else
                BL.BindCmbST_TAReportOfficer(cmbEngineer, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Engineer)
            End If

            If Not IsDBNull(DT.Rows(0).Item("Officer_Analyst")) Then
                BL.BindCmbST_TAReportOfficer(cmbAnalyst, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Approver, DT.Rows(0).Item("Officer_Analyst"))
            Else
                BL.BindCmbST_TAReportOfficer(cmbAnalyst, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Approver)
            End If


        End If

        'UC_DialogCorrosion.BindCorrasion_Rate()

    End Sub


    Private Sub BindComponents()
        dialogCreateReport.Visible = False
        '------------------------------Header -----------------------------------
        'Dim DT As DataTable = BL.Get_ST_TA_Inspection_For_Equipment(RPT_Year, RPT_No, TAG_TYPE_ID, DETAIL_ID, "RPT_ST_TA_Detail_Step")
        'rptComponents.DataSource = DT
        'rptComponents.DataBind()


    End Sub

    Private Sub rptComponents_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptComponents.ItemDataBound
        Dim lblRPT_Date As Label = e.Item.FindControl("lblRPT_Date")
        Dim lblComponents As Label = e.Item.FindControl("lblComponents")
        Dim lblConditions As Label = e.Item.FindControl("lblConditions")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Dim lblActualStart As Label = e.Item.FindControl("lblActualStart")
        Dim lblActualEnd As Label = e.Item.FindControl("lblActualEnd")
        Dim lblFinished As Label = e.Item.FindControl("lblFinished")


        'Time
        Dim lblTimeStart As Label = e.Item.FindControl("lblTimeStart")
        Dim lblTimeEnd As Label = e.Item.FindControl("lblTimeEnd")

        lblFinished.Text = e.Item.DataItem("STATUS_NAME").ToString()
        If (e.Item.DataItem("STATUS_NAME").ToString() = "Inspecting") Then
            lblFinished.ForeColor = Drawing.Color.SteelBlue
        Else
            lblFinished.ForeColor = Drawing.Color.Green
        End If



        If Not IsDBNull(e.Item.DataItem("RPT_DATE")) Then
            lblRPT_Date.Text = BL.ReportProgrammingDate(e.Item.DataItem("RPT_DATE"))
            lblRPT_Date.Attributes("RPT_Date") = e.Item.DataItem("RPT_DATE")
        Else
            lblRPT_Date.Attributes("RPT_Date") = ""
        End If
        lblComponents.Text = e.Item.DataItem("INSP_Name")
        lblComponents.Attributes("INSP_ID") = e.Item.DataItem("INSP_ID")
        lblComponents.Attributes("DETAIL_STEP_ID") = e.Item.DataItem("DETAIL_STEP_ID")


        lblConditions.Text = e.Item.DataItem("Condition_Problem").ToString()

        btnEdit.CommandArgument = e.Item.DataItem("INSP_ID")

        If Not IsDBNull(e.Item.DataItem("RPT_Period_Start")) Then
            lblActualStart.Text = BL.ReportGridTime(e.Item.DataItem("RPT_Period_Start"))
        End If
        If Not IsDBNull(e.Item.DataItem("RPT_Period_End")) Then
            lblActualEnd.Text = BL.ReportGridTime(e.Item.DataItem("RPT_Period_End"))
        End If

        'Time
        lblTimeStart.Text = e.Item.DataItem("TIME_Start").ToString()
        lblTimeEnd.Text = e.Item.DataItem("TIME_End").ToString()

        If Not IsDBNull(e.Item.DataItem("DETAIL_ID")) Then
            btnDelete.Visible = True
        Else
            btnDelete.Visible = False
        End If

        Dim TD As HtmlTableCell = e.Item.FindControl("TD")
        If Not IsDBNull(e.Item.DataItem("STATUS_ID")) Then
            If e.Item.DataItem("STATUS_ID") = 0 Then
                TD.Style("background-color") = "white"
                lblRPT_Date.Style("color") = "gray"

            ElseIf (e.Item.DataItem("STATUS_ID") = 1) Then
                TD.Style("background-color") = "yellow"
                lblRPT_Date.Style("color") = "gray"

            ElseIf (e.Item.DataItem("STATUS_ID") = 2) Then
                TD.Style("background-color") = "Orange"
                lblRPT_Date.Style("color") = "gray"
            ElseIf (e.Item.DataItem("STATUS_ID") = 3) Then
                TD.Style("background-color") = "red"
                lblRPT_Date.Style("color") = "gray"


            End If
        Else
            TD.Style("background-color") = "white"
            lblRPT_Date.Style("color") = "gray"


        End If

    End Sub



    Private Sub rptComponents_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptComponents.ItemCommand

        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRPT_Date As Label = e.Item.FindControl("lblRPT_Date")
        Dim lblComponents As Label = e.Item.FindControl("lblComponents")
        Dim txtConditions As TextBox = e.Item.FindControl("txtConditions")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim lblTimeStart As Label = e.Item.FindControl("lblTimeStart")
        Dim lblTimeEnd As Label = e.Item.FindControl("lblTimeEnd")


        Dim _Year As Integer = C.DateToString(lblRPT_Date.Attributes("RPT_Date"), "yyyy")
        If _Year > 2500 Then
            _Year = _Year - 543
        End If

        RPT_DATE = _Year.ToString() & C.DateToString(lblRPT_Date.Attributes("RPT_Date"), "-MM-dd").ToString()
        DETAIL_STEP_ID = lblComponents.Attributes("DETAIL_STEP_ID")
        INSP_ID = btnEdit.CommandArgument

        Select Case e.CommandName
            Case "Action"
                pnlValidation.Visible = False
                pnlBindingSuccess.Visible = False

                pnlList.Visible = False
                pnlEdit.Visible = True
                lblEditComponents.Text = lblComponents.Text

                BindTemplate()
                '--------------Unvisible Select Template------------
                UC_Select_Template.CloseDialog()

                '---------------Header------------------------
                '---Header--
                lbl_Tag_Code_Edit.Text = lbl_Tag_Code.Text
                lbl_Tag_Name_Edit.Text = lbl_Tag_Name.Text

                lbl_Plant_Edit.Text = lbl_Plant.Text
                lbl_Equipment_Edit.Text = lbl_Equipment.Text
                lbl_Year_Edit.Text = lbl_Year.Text

                ImplementJavaIntegerText(txtEmployees_Count, True)
                BL.BindDDlLevel(ddl_Level)
                '----------------------------------------------

                '---------------Property-------------------------------
                DialogAbsorber.TAG_CODE = ""
                DialogAbsorber.TAG_ID = TAG_ID
                DialogAbsorber.TAG_TYPE_ID = TAG_TYPE_ID
                DialogAbsorber.TAG_Name = ""
                DialogAbsorber.TAG_TYPE_Name = TAG_TYPE_ID
                DialogAbsorber.TO_TABLE_MS = "MS_ST_TA_TAG"
                DialogAbsorber.TO_TABLE_SPEC = "MS_ST_ABSORBER_SPEC"
                DialogAbsorber.BindHeader()
                DialogAbsorber.ShowDialog()

                '-------------------Detail-----------------------------------
                Dim SQL_Date As String = ""
                SQL_Date &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
                'SQL_Date &= "    WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & "  AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID & " AND RPT_DATE='" & RPT_DATE & "'"
                SQL_Date &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
                Dim DA_Date As New SqlDataAdapter(SQL_Date, BL.ConnStr)
                Dim DT_Date As New DataTable
                DA_Date.Fill(DT_Date)

                If Not IsDBNull(DT_Date.Rows(0).Item("RPT_Date")) Then
                    txt_Rpt_Date.Text = BL.ReportProgrammingDate(DT_Date.Rows(0).Item("RPT_Date"))
                Else
                    txt_Rpt_Date.Text = ""
                End If

                txtTime_Start.Text = lblTimeStart.Text
                txtTime_End.Text = lblTimeEnd.Text

                If Not IsDBNull(DT_Date.Rows(0).Item("Employees")) Then
                    txtEmployees_Count.Text = DT_Date.Rows(0).Item("Employees")  '-------แสดงจำนวนพรักงานที่ทำงาน
                End If
                If Not IsDBNull(DT_Date.Rows(0).Item("STATUS_ID")) Then
                    ddl_Level.SelectedValue = DT_Date.Rows(0).Item("STATUS_ID")
                End If
                If Not IsDBNull(DT_Date.Rows(0).Item("RPT_Status")) Then
                    ddlProgress.SelectedValue = DT_Date.Rows(0).Item("RPT_Status")
                Else
                    ddlProgress.SelectedValue = 0
                End If

                txtCondition_Problem.Text = DT_Date.Rows(0).Item("Condition_Problem").ToString()
                txtPossible_Cause.Text = DT_Date.Rows(0).Item("Possible_Cause").ToString()
                txtRecommendation.Text = DT_Date.Rows(0).Item("Recommendation").ToString()

                '----------------Dialog image-----------------
                TAG_CLASS = TAG_CLASS

                Me.Visible = True

                '------------------ Officer ------------------
                BL.BindCmbReportOfficer(cmbCollector, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Collector)
                BL.BindCmbReportOfficer(cmbInspector, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Inspector)
                BL.BindCmbReportOfficer(cmbEngineer, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Engineer)
                BL.BindCmbReportOfficer(cmbAnalyst, RPT_Year, RPT_No, PLANT_ID, RPT_Type_ID, EIR_BL.User_Level.Approver)

                '------1. ตรวจสอบการการ Insp
                Dim SQL As String = ""
                SQL &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
                SQL &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If (DT.Rows.Count = 0) Then
                    '------2. สร้างรายการของ Insp
                    Dim DR As DataRow
                    DR = DT.NewRow
                    DR("DETAIL_ID") = DETAIL_ID
                    DR("RPT_Year") = RPT_Year
                    DR("RPT_No") = RPT_No
                    DR("TAG_ID") = TAG_ID
                    DR("INSP_ID") = INSP_ID
                    DR("RPT_STEP") = 0
                    DR("RPT_Status") = 0
                    DR("Created_Time") = Now
                    DR("Created_By") = Session("USER_ID")
                    DT.Rows.Add(DR)

                    Dim cmd As New SqlCommandBuilder(DA)
                    Try
                        DA.Update(DT)
                        DT.AcceptChanges()
                    Catch ex As Exception
                        lblValidation.Text = "Invalid parameter"
                        pnlValidation.Visible = True
                        Exit Sub
                    End Try
                Else

                    '------------------ Officer ------------------
                    If Not IsDBNull(DT.Rows(0).Item("Officer_Collector")) Then
                        BL.BindCmbST_TAReportOfficer(cmbCollector, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Collector, DT.Rows(0).Item("Officer_Collector"))
                    Else
                        BL.BindCmbST_TAReportOfficer(cmbCollector, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Collector)
                    End If

                    If Not IsDBNull(DT.Rows(0).Item("Officer_Inspector")) Then
                        BL.BindCmbST_TAReportOfficer(cmbInspector, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Inspector, DT.Rows(0).Item("Officer_Inspector"))
                    ElseIf Not IsDBNull(DT.Rows(0).Item("Officer_Engineer")) Then
                        BL.BindCmbST_TAReportOfficer(cmbInspector, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Inspector, DT.Rows(0).Item("Officer_Engineer"))
                    Else
                        BL.BindCmbST_TAReportOfficer(cmbInspector, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Inspector)
                    End If

                    If Not IsDBNull(DT.Rows(0).Item("Officer_Engineer")) Then
                        BL.BindCmbST_TAReportOfficer(cmbEngineer, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Engineer, DT.Rows(0).Item("Officer_Engineer"))
                    Else
                        BL.BindCmbST_TAReportOfficer(cmbEngineer, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Engineer)
                    End If

                    If Not IsDBNull(DT.Rows(0).Item("Officer_Analyst")) Then
                        BL.BindCmbST_TAReportOfficer(cmbAnalyst, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Approver, DT.Rows(0).Item("Officer_Analyst"))
                    Else
                        BL.BindCmbST_TAReportOfficer(cmbAnalyst, DETAIL_STEP_ID, PLANT_ID, EIR_BL.ST_TA_STEP.NDE, EIR_BL.User_Level.Approver)
                    End If


                End If

            Case "Delete"


                BL.Drop_RPT_ST_TA(RPT_Year, RPT_No, TAG_ID, DETAIL_ID, INSP_ID, "RPT_ST_TA_Detail_Step", RPT_DATE)

                Try
                    BL.Drop_RPT_ST_TA(RPT_Year, RPT_No, TAG_ID, DETAIL_ID, INSP_ID, "RPT_ST_TA_Detail_Step", RPT_DATE)
                    BindComponents()
                    lblBindingSuccess.Text = "successfully"
                    pnlBindingSuccess.Visible = True

                Catch ex As Exception
                    lblValidation.Text = "Invalid parameter"
                    pnlValidation.Visible = True
                    Exit Sub
                End Try

        End Select


    End Sub





    Private Sub Save(ByVal Status As Integer)
        '----------Validate------------

        If Not BL.IsProgrammingDate(txt_Rpt_Date.Text) Then
            lblValidation.Text = "Please select Date"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtTime_Start.Text.Trim = "" Or txtTime_End.Text.Trim = "" Then
            lblValidation.Text = "Please select period of time"
            txtTime_Start.Focus()
            pnlValidation.Visible = True
            Exit Sub
        End If

        If (Convert.ToInt64(txtTime_End.Text.Replace(":", "")) <= Convert.ToInt64(txtTime_Start.Text.Replace(":", ""))) Then
            lblValidation.Text = "Please check period of time"
            txtTime_Start.Focus()
            pnlValidation.Visible = True
            Exit Sub
        End If


        If (Status = EIR_BL.ST_TA_STATUS.Finish) Then

            '------------
            If txtEmployees_Count.Text = "" Then
                lblValidation.Text = "Enter Employees"
                txtEmployees_Count.Focus()
                pnlValidation.Visible = True
                Exit Sub
            End If

            If ddl_Level.SelectedIndex = 0 Then
                lblValidation.Text = "Please select status"
                ddl_Level.Focus()
                pnlValidation.Visible = True
                Exit Sub
            End If



        Else

        End If


        '-------------------------------------------

        '----------Save----------------
        '------1. ตรวจสอบการการ Insp
        Dim SQL As String = ""
        SQL &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        SQL &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to save." & vbNewLine & "This report has been removed!'); window.location.href='ST_TA_AsFound.aspx';", True)
            Exit Sub
        End If

        DR = DT.Rows(0)
        Try
            Dim StartDate As Date = C.StringToDate(txt_Rpt_Date.Text, "yyyy-MM-dd")
            DR("RPT_Date") = StartDate
        Catch ex As Exception
            DR("RPT_Date") = DBNull.Value
        End Try


        If (txtTime_Start.Text <> "") Then
            DR("TIME_Start") = txtTime_Start.Text
        Else
            DR("TIME_Start") = DBNull.Value
        End If

        If (txtTime_End.Text <> "") Then
            DR("TIME_End") = txtTime_End.Text
        Else
            DR("TIME_End") = DBNull.Value
        End If
        If (txtEmployees_Count.Text <> "") Then
            DR("Employees") = txtEmployees_Count.Text
        Else
            DR("Employees") = DBNull.Value
        End If
        If (ddl_Level.SelectedIndex > 0) Then
            DR("STATUS_ID") = ddl_Level.SelectedValue
        Else
            DR("STATUS_ID") = DBNull.Value
        End If

        If (Status = EIR_BL.ST_TA_STATUS.In_Progress) Then
            DR("RPT_Status") = EIR_BL.ST_TA_STATUS.In_Progress
        Else
            DR("RPT_Status") = EIR_BL.ST_TA_STATUS.Finish
        End If

        DR("Condition_Problem") = txtCondition_Problem.Text
        DR("Possible_Cause") = txtPossible_Cause.Text
        DR("Recommendation") = txtRecommendation.Text


        DR("INSP_Name") = lblEditComponents.Text
        DR("RPT_LOCK_BY") = Session("USER_ID")


        DR("Update_Time") = Now
        DR("Update_By") = Session("USER_ID")

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
            DT.AcceptChanges()

            Save_Officer()
            If (ddlProgress.SelectedValue = EIR_BL.ST_TA_STATUS.Finish) Then
                'BindComponents()
                'pnlList.Visible = True
                'pnlEdit.Visible = False
            End If
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

#Region "Save Sector"

        '==================Save รายละเอียด MS_Template_Sector=========================
        '----เพิ่มรายการใน Database ----

        Dim DT_Template As DataTable = BuiltDataGridview()

        If (DT_Template.Rows.Count > 0) Then
            Dim DR_Insert As DataRow

            For i As Integer = 0 To DT_Template.Rows.Count - 1

                SQL = ""
                SQL = " Select * FROM MS_Template_Sector where Sector_ID=" & DT_Template.Rows(i).Item("Sector_ID")
                DA = New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT_Insert As DataTable = New DataTable
                DA.Fill(DT_Insert)
                If (DT_Insert.Rows.Count = 1) Then
                    DR_Insert = DT_Insert.Rows(0)
                    Select Case DT_Template.Rows(i).Item("Template_ID")
                        Case 1
                            DR_Insert("Text_1") = DT_Template.Rows(i).Item("Text_1").ToString()

                        Case 2
                            DR_Insert("Text_1") = DT_Template.Rows(i).Item("Text_1").ToString()

                        Case 3
                            DR_Insert("Text_1") = DT_Template.Rows(i).Item("Text_1").ToString()
                        Case 4
                            DR_Insert("Text_1") = DT_Template.Rows(i).Item("Text_1").ToString()
                        Case 5
                            DR_Insert("Text_1") = DT_Template.Rows(i).Item("Text_1").ToString()
                        Case 6
                            DR_Insert("Text_1") = DT_Template.Rows(i).Item("Text_1").ToString()
                            DR_Insert("Text_2") = DT_Template.Rows(i).Item("Text_2").ToString()
                            DR_Insert("Text_3") = DT_Template.Rows(i).Item("Text_3").ToString()
                            DR_Insert("Text_4") = DT_Template.Rows(i).Item("Text_4").ToString()
                        Case 7
                            DR_Insert("Text_1") = DT_Template.Rows(i).Item("Text_1").ToString()

                            DR_Insert("Text_3") = DT_Template.Rows(i).Item("Text_3").ToString()
                            DR_Insert("Text_4") = DT_Template.Rows(i).Item("Text_4").ToString()
                        Case 8
                            DR_Insert("Text_1") = DT_Template.Rows(i).Item("Text_1").ToString()
                            DR_Insert("Text_2") = DT_Template.Rows(i).Item("Text_2").ToString()

                    End Select


                    cmd = New SqlCommandBuilder(DA)
                    Try
                        DA.Update(DT_Insert)
                        DT_Insert.AcceptChanges()
                    Catch ex As Exception
                        lblValidation.Text = "Invalid parameter"
                        pnlValidation.Visible = True
                        Exit Sub
                    End Try
                End If
            Next

        End If
#End Region

#Region "Save CUI POINT"

        If (Step_ID = 3) Then
            UC_DialogCorrosion.SaveCorrasion_Rate()
        End If
#End Region


#Region "Save CUI POINT"
        Dim DT_CUI_POINT As DataTable = UC_DialogCorrosion.GetCurrentData_rpt()

        If (DT_CUI_POINT.Rows.Count > 0) Then
            '-- ลงตาราง RPT_ST_TA_CUI_POINT

            '--1. Clear Data--
            Dim SQL_POINT As String = ""
            SQL_POINT &= "    SELECT * FROM RPT_ST_TA_CUI_POINT " & vbNewLine
            SQL_POINT &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
            Dim DA_POINT As New SqlDataAdapter(SQL_POINT, BL.ConnStr)
            Dim DT_POINT As New DataTable
            DA_POINT.Fill(DT_POINT)

            '-- Clear Data--
            SQL_POINT = ""
            SQL_POINT &= "    DELETE  FROM RPT_ST_TA_CUI_POINT " & vbNewLine
            SQL_POINT &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
            DA_POINT = New SqlDataAdapter(SQL_POINT, BL.ConnStr)
            DT_POINT = New DataTable
            DA_POINT.Fill(DT_POINT)

            '--2. Insert Data--
            SQL_POINT = ""
            SQL_POINT = " Select * FROM RPT_ST_TA_CUI_POINT where 0=1"
            DA_POINT = New SqlDataAdapter(SQL_POINT, BL.ConnStr)
            DT_POINT = New DataTable
            DA_POINT.Fill(DT_POINT)
            Dim DR_POINT As DataRow
            For i As Integer = 0 To DT_CUI_POINT.Rows.Count - 1
                DR_POINT = DT_POINT.NewRow
                DR_POINT("RPT_Year") = RPT_Year
                DR_POINT("RPT_No") = RPT_No
                DR_POINT("DETAIL_STEP_ID") = DETAIL_STEP_ID
                DR_POINT("TAG_UTM_ID") = DT_CUI_POINT.Rows(i).Item("TAG_UTM_ID")

                DR_POINT("Position_Name") = DT_CUI_POINT.Rows(i).Item("Position_Name")
                DR_POINT("Point_Code") = DT_CUI_POINT.Rows(i).Item("Position_Code")
                DR_POINT("Point_number") = DT_CUI_POINT.Rows(i).Item("Position_Number")

                For j As Integer = 5 To DT_CUI_POINT.Columns.Count - 1
                    DR_POINT(DT_CUI_POINT.Columns(j).ToString()) = DT_CUI_POINT.Rows(i).Item(DT_CUI_POINT.Columns(j).ToString())
                Next

                DT_POINT.Rows.Add(DR_POINT)
            Next

            cmd = New SqlCommandBuilder(DA_POINT)
            Try
                DA_POINT.Update(DT_POINT)
                DT_POINT.AcceptChanges()
            Catch ex As Exception
                lblValidation.Text = "Invalid parameter"
                pnlValidation.Visible = True
                Exit Sub
            End Try

        Else

        End If

#End Region

#Region "Save CUI Detail"
        '--ตรวจสอบมีการเลือก Term หรือไม่ของแต่ละ Location

        Dim DT_CUI_Detail As DataTable = UC_DialogCorrosion.GetCurrentData_Corosion_rpt()

        If (DT_CUI_Detail.Rows.Count > 0) Then
            '--Save ลงตาราง RPT_ST_TA_CUI_DETAIL

            '--1. Select Data--
            Dim SQL_DETAIL As String = ""
            SQL_DETAIL &= "    SELECT * FROM RPT_ST_TA_CUI_DETAIL " & vbNewLine
            SQL_DETAIL &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
            Dim DA_DETAIL As New SqlDataAdapter(SQL_DETAIL, BL.ConnStr)
            Dim DT_DETAIL As New DataTable
            DA_DETAIL.Fill(DT_DETAIL)

            '-- Clear Data-- 
            SQL_DETAIL = ""
            SQL_DETAIL &= "    DELETE  FROM RPT_ST_TA_CUI_DETAIL " & vbNewLine
            SQL_DETAIL &= "    WHERE DETAIL_STEP_ID=" & DETAIL_STEP_ID
            DA_DETAIL = New SqlDataAdapter(SQL_DETAIL, BL.ConnStr)
            DT_DETAIL = New DataTable
            DA_DETAIL.Fill(DT_DETAIL)

            '--2. Insert Data--
            SQL_DETAIL = ""
            SQL_DETAIL = " Select * FROM RPT_ST_TA_CUI_DETAIL where 0=1"
            DA_DETAIL = New SqlDataAdapter(SQL_DETAIL, BL.ConnStr)
            DT_DETAIL = New DataTable
            DA_DETAIL.Fill(DT_DETAIL)
            Dim DR_DETAIL As DataRow
            For i As Integer = 0 To DT_CUI_Detail.Rows.Count - 1
                DR_DETAIL = DT_DETAIL.NewRow

                DR_DETAIL("RPT_Year") = RPT_Year
                DR_DETAIL("RPT_No") = RPT_No
                DR_DETAIL("DETAIL_STEP_ID") = DETAIL_STEP_ID
                DR_DETAIL("TAG_UTM_ID") = DT_CUI_Detail.Rows(i).Item("TAG_UTM_ID")
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Last_RPT_No")) Then
                    DR_DETAIL("Last_RPT_Year") = Val(DT_CUI_Detail.Rows(i).Item("Last_RPT_Year").Replace(",", ""))
                Else
                    DR_DETAIL("Last_RPT_Year") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Last_RPT_No")) Then
                    DR_DETAIL("Last_RPT_No") = Val(DT_CUI_Detail.Rows(i).Item("Last_RPT_No").Replace(",", ""))
                Else
                    DR_DETAIL("Last_RPT_No") = DBNull.Value
                End If

                DR_DETAIL("Last_DETAIL_STEP_ID") = DBNull.Value
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Last_Measure_Year")) Then
                    DR_DETAIL("Last_Measure_Year") = Val(DT_CUI_Detail.Rows(i).Item("Last_Measure_Year").Replace(",", ""))
                Else
                    DR_DETAIL("Last_Measure_Year") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Last_Thickness")) Then
                    DR_DETAIL("Last_Thickness") = Val(DT_CUI_Detail.Rows(i).Item("Last_Thickness").Replace(",", ""))
                Else
                    DR_DETAIL("Last_Thickness") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Initial_Year")) Then
                    DR_DETAIL("Initial_Year") = Val(DT_CUI_Detail.Rows(i).Item("Initial_Year").Replace(",", ""))
                Else
                    DR_DETAIL("Initial_Year") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Initial_Thickness")) Then
                    DR_DETAIL("Initial_Thickness") = Val(DT_CUI_Detail.Rows(i).Item("Initial_Thickness").Replace(",", ""))
                Else
                    DR_DETAIL("Initial_Thickness") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Min_Thickness")) Then
                    DR_DETAIL("Min_Thickness") = Val(DT_CUI_Detail.Rows(i).Item("Min_Thickness").Replace(",", ""))
                Else
                    DR_DETAIL("Min_Thickness") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Required_Thickness_Mode")) Then
                    DR_DETAIL("Required_Thickness_Mode") = Val(DT_CUI_Detail.Rows(i).Item("Required_Thickness_Mode").Replace(",", ""))
                Else
                    DR_DETAIL("Required_Thickness_Mode") = DBNull.Value
                End If
                DR_DETAIL("Use_Term") = DT_CUI_Detail.Rows(i).Item("Use_Term")
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Norminal_Thickness")) Then
                    DR_DETAIL("Norminal_Thickness") = Val(DT_CUI_Detail.Rows(i).Item("Norminal_Thickness").Replace(",", ""))
                Else
                    DR_DETAIL("Norminal_Thickness") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Corrosion_Allowance")) Then
                    DR_DETAIL("Corrosion_Allowance") = Val(DT_CUI_Detail.Rows(i).Item("Corrosion_Allowance").Replace(",", ""))
                Else
                    DR_DETAIL("Corrosion_Allowance") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Calculated_Thickness")) Then
                    DR_DETAIL("Calculated_Thickness") = Val(DT_CUI_Detail.Rows(i).Item("Calculated_Thickness").Replace(",", ""))
                Else
                    DR_DETAIL("Calculated_Thickness") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Required_Thickness")) Then
                    DR_DETAIL("Required_Thickness") = Val(DT_CUI_Detail.Rows(i).Item("Required_Thickness").Replace(",", ""))
                Else
                    DR_DETAIL("Required_Thickness") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Corrosion_Rate")) Then
                    DR_DETAIL("Corrosion_Rate") = Val(DT_CUI_Detail.Rows(i).Item("Corrosion_Rate").Replace(",", ""))
                Else
                    DR_DETAIL("Corrosion_Rate") = DBNull.Value
                End If
                If Not IsDBNull(DT_CUI_Detail.Rows(i).Item("Remain_Life")) Then
                    DR_DETAIL("Remain_Life") = Val(DT_CUI_Detail.Rows(i).Item("Remain_Life").Replace(",", ""))
                Else
                    DR_DETAIL("Remain_Life") = DBNull.Value
                End If


                DT_DETAIL.Rows.Add(DR_DETAIL)

            Next

            cmd = New SqlCommandBuilder(DA_DETAIL)
            Try
                DA_DETAIL.Update(DT_DETAIL)
                DT_DETAIL.AcceptChanges()
            Catch ex As Exception
                lblValidation.Text = "Invalid parameter"
                pnlValidation.Visible = True
                Exit Sub
            End Try

        Else




        End If

        'RPT_Year
        'RPT_No
        'DETAIL_STEP_ID
        'TAG_UTM_ID
        'Last_RPT_Year
        'Last_RPT_No
        'Last_DETAIL_STEP_ID
        'Last_Measure_Year
        'Last_Thickness
        'Initial_Year
        'Initial_Thickness
        'Min_Thickness
        'Required_Thickness_Mode
        'Use_Term
        'Norminal_Thickness
        'Corrosion_Allowance
        'Calculated_Thickness
        'Required_Thickness
        'Corrosion_Rate
        'Remain_Life




#End Region


        '===========================================


    End Sub


    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click

        If (ddlProgress.SelectedValue = EIR_BL.ST_TA_STATUS.In_Progress) Then
            Save(EIR_BL.ST_TA_STATUS.In_Progress)
        Else
            Save(EIR_BL.ST_TA_STATUS.Finish)
            Add_InStep_Repair()
        End If

        'UC_DialogCorrosion.SavrCorrasion_Rate()
    End Sub

#Region "Saving"
    Private Sub Save_Officer(Optional ByVal ReportSuccess As Boolean = True)

        Dim SQL As String = ""
        SQL &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        SQL &= "    WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & "  AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID & " " & " AND DETAIL_STEP_ID=" & DETAIL_STEP_ID & " "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & TAG_TYPE_ID & "&DETAIL_ID=" & DETAIL_ID
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Unable to save." & vbNewLine & "This report has been removed!'); window.location.href='ST_TA_Absorber_AsFound.aspx';", True)
            Exit Sub
        End If

        Dim NeedSave As Boolean = False
        '------------------ Officer ------------------
        If cmbCollector.SelectedIndex > 0 Then
            If IsDBNull(DT.Rows(0).Item("Officer_Collector")) Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Collector") = cmbCollector.Text
            ElseIf DT.Rows(0).Item("Officer_Collector") <> cmbCollector.Text Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Collector") = cmbCollector.Text
            End If
        ElseIf Not IsDBNull(DT.Rows(0).Item("Officer_Collector")) AndAlso DT.Rows(0).Item("Officer_Collector") <> "" Then
            NeedSave = True
            DT.Rows(0).Item("Officer_Collector") = ""
        Else
            NeedSave = True
            DT.Rows(0).Item("Officer_Collector") = ""
        End If
        '----------------------- Inspector -----------------------
        Dim Inspector As Object = DBNull.Value
        Select Case True
            Case Not IsDBNull(DT.Rows(0).Item("Officer_Engineer"))
                Inspector = DT.Rows(0).Item("Officer_Engineer")
            Case Not IsDBNull(DT.Rows(0).Item("Officer_Inspector"))
                Inspector = DT.Rows(0).Item("Officer_Inspector")
        End Select
        If cmbInspector.SelectedIndex > 0 Then
            If IsDBNull(Inspector) Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Inspector") = cmbInspector.Text
            ElseIf Inspector <> cmbInspector.Text Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Inspector") = cmbInspector.Text
            End If
        ElseIf Not IsDBNull(Inspector) AndAlso Inspector <> "" Then
            NeedSave = True
            DT.Rows(0).Item("Officer_Inspector") = ""
        Else
            NeedSave = True
            DT.Rows(0).Item("Officer_Inspector") = ""
        End If

        '--------------------- Engineer -----------
        If cmbEngineer.SelectedIndex > 0 Then
            If IsDBNull(DT.Rows(0).Item("Officer_Engineer")) Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Engineer") = cmbEngineer.Text
            ElseIf DT.Rows(0).Item("Officer_Engineer") <> cmbEngineer.Text Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Engineer") = cmbEngineer.Text
            End If
        ElseIf Not IsDBNull(DT.Rows(0).Item("Officer_Engineer")) AndAlso DT.Rows(0).Item("Officer_Engineer") <> "" Then
            NeedSave = True
            DT.Rows(0).Item("Officer_Engineer") = ""
        Else
            NeedSave = True
            DT.Rows(0).Item("Officer_Engineer") = ""
        End If

        '----------------- Approver -------------
        If cmbAnalyst.SelectedIndex > 0 Then
            If IsDBNull(DT.Rows(0).Item("Officer_Analyst")) Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Analyst") = cmbAnalyst.Text
            ElseIf DT.Rows(0).Item("Officer_Analyst") <> cmbAnalyst.Text Then
                NeedSave = True
                DT.Rows(0).Item("Officer_Analyst") = cmbAnalyst.Text
            End If
        ElseIf Not IsDBNull(DT.Rows(0).Item("Officer_Analyst")) AndAlso DT.Rows(0).Item("Officer_Analyst") <> "" Then
            NeedSave = True
            DT.Rows(0).Item("Officer_Analyst") = ""
        Else
            NeedSave = True
            DT.Rows(0).Item("Officer_Analyst") = ""
        End If

        If Not NeedSave Then
            If ReportSuccess Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('There is nothing to be saved!\nThe report does not changed');", True)
            End If
            Exit Sub
        End If

        DT.Rows(0).Item("Update_By") = Session("USER_ID")
        DT.Rows(0).Item("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        If ReportSuccess Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Save successfully');", True)
        End If

    End Sub
#End Region



#Region "Navigator"


    'Private Sub HTabAsFound_Click(sender As Object, e As EventArgs) Handles HTabAsFound.Click
    '    Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & TAG_TYPE_ID & "&DETAIL_ID=" & DETAIL_ID
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Absorber_AsFound.aspx?" & Param & "';", True)
    'End Sub

    'Private Sub HTabAfterClean_Click(sender As Object, e As EventArgs) Handles HTabAfterClean.Click
    '    Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & TAG_TYPE_ID & "&DETAIL_ID=" & DETAIL_ID
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Absorber_AfterClean.aspx?" & Param & "';", True)
    'End Sub

    'Private Sub HTabNDE_Click(sender As Object, e As EventArgs) Handles HTabNDE.Click
    '    Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & TAG_TYPE_ID & "&DETAIL_ID=" & DETAIL_ID
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Absorber_NDE.aspx?" & Param & "';", True)
    'End Sub

    'Private Sub HTabRepair_Click(sender As Object, e As EventArgs) Handles HTabRepair.Click
    '    Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & TAG_TYPE_ID & "&DETAIL_ID=" & DETAIL_ID
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Absorber_Repair.aspx?" & Param & "';", True)
    'End Sub

    'Private Sub HTabAfterRepair_Click(sender As Object, e As EventArgs) Handles HTabAfterRepair.Click
    '    Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & TAG_TYPE_ID & "&DETAIL_ID=" & DETAIL_ID
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Absorber_AfterRepair.aspx?" & Param & "';", True)
    'End Sub

    'Private Sub HTabFinal_Click(sender As Object, e As EventArgs) Handles HTabFinal.Click
    '    Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & TAG_TYPE_ID & "&DETAIL_ID=" & DETAIL_ID
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='ST_TA_Absorber_Final.aspx?" & Param & "';", True)
    'End Sub



#End Region

    Private Sub btn_Back_Click(sender As Object, e As EventArgs) Handles btn_Back.Click
        'Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & TAG_TYPE_ID & "&DETAIL_ID=" & DETAIL_ID
        '"ST_TA_Inspection_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID="& Step_ID
        Response.Redirect("ST_TA_Inspection_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & Step_ID)

        'BindComponents()

        'pnlList.Visible = True
        'pnlEdit.Visible = False

    End Sub

    Private Sub btn_ReportDetail_Click(sender As Object, e As EventArgs) Handles btn_ReportDetail.Click, btn_ReportDetail_List.Click
        Response.Redirect("ST_TA_Inspection_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No)
    End Sub


    Private Sub btn_Next_Click(sender As Object, e As EventArgs) Handles btn_Next.Click, btn_Next_List.Click
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & TAG_TYPE_ID & "&DETAIL_ID=" & DETAIL_ID
        Response.Redirect("ST_TA_Absorber_Repair.aspx?" & Param)
    End Sub

    'Private Sub btn_Preview_Click(sender As Object, e As EventArgs) Handles btn_Preview.Click, btn_Preview_List.Click
    '    Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&TAG_ID=" & TAG_ID & "&TAG_TYPE_ID=" & TAG_TYPE_ID & "&DETAIL_ID=" & DETAIL_ID
    '    Response.Redirect("ST_TA_Absorber_AfterClean.aspx?" & Param)
    'End Sub

    '---BTN---
    Private Sub lnkAdd_Insp_Click(sender As Object, e As EventArgs) Handles lnkAdd_Insp.Click
        BL.BindDDl_Type_INSP_Name(ddl_dialog_INSP_Name, TAG_TYPE_ID)
        ddl_dialog_INSP_Name.SelectedIndex = -1
        pnlValidation_dialog.Visible = False
        dialogCreateReport.Visible = True
        txt_dialog_Date.Text = ""
    End Sub

    Private Sub btnClose_dialogCreateReport_Click(sender As Object, e As EventArgs) Handles btnClose_dialogCreateReport.Click
        dialogCreateReport.Visible = False
    End Sub

    Private Sub btnOK_dialogCreateReport_Click(sender As Object, e As EventArgs) Handles btnOK_dialogCreateReport.Click


        If Not BL.IsProgrammingDate(txt_dialog_Date.Text) Then
            lblValidation_dialog.Text = "Please select Date"
            pnlValidation_dialog.Visible = True
            Exit Sub
        End If
        If (ddl_dialog_INSP_Name.SelectedIndex = 0) Then
            lblValidation_dialog.Text = "Please select Parts / Components"
            pnlValidation_dialog.Visible = True
            Exit Sub
        End If


        '---------------ตรวจสอบ step As found ว่าจุดตรวจที่ต้องการตรวจ ดำเนินการ finish ทุกใบหรือยัง-----------------------
        Dim SQL_Status As String = ""
        SQL_Status &= "    SELECT * FROM RPT_Absorber_After_Clean " & vbNewLine
        SQL_Status &= "    WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & "  AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & ddl_dialog_INSP_Name.SelectedValue & " AND RPT_Status=0 AND DETAIL_ID=" & DETAIL_ID
        Dim DA_Status As New SqlDataAdapter(SQL_Status, BL.ConnStr)
        Dim DT_Status As New DataTable
        DA_Status.Fill(DT_Status)
        If (DT_Status.Rows.Count > 0) Then
            lblValidation_dialog.Text = "This report cannot be created in step NDE"
            lblValidation_dialog.Text &= "<li>Try to check report status After Clean <b>" & vbNewLine
            pnlValidation_dialog.Visible = True
            Exit Sub
        End If

        Dim _Year As Integer = C.DateToString(C.StringToDate(txt_dialog_Date.Text, dialog_Date_Extender.Format), "yyyy")
        If _Year > 2500 Then
            _Year = _Year - 543
        End If

        '-----ตรวจสอบรอบว่ามีรอบและจุดตรวจหรือไม่----
        Dim SQL As String = ""
        SQL &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
        SQL &= "    WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & "  AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & ddl_dialog_INSP_Name.SelectedValue & " " & " AND RPT_DATE='" & _Year & C.DateToString(C.StringToDate(txt_dialog_Date.Text, dialog_Date_Extender.Format), "-MM-dd") & "'  AND DETAIL_ID=" & DETAIL_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            lblValidation_dialog.Text = "This report cannot be created due to follow these reasons"
            lblValidation_dialog.Text &= "<li>Report duplicates<li>Try to check report <b>" & vbNewLine
            pnlValidation_dialog.Visible = True
            Exit Sub

        Else
            '---สร้างรายการ

            pnlList.Visible = True
            pnlEdit.Visible = False

            '------2. สร้างรายการของ Insp
            Dim DR As DataRow
            DR = DT.NewRow
            DR("DETAIL_ID") = DETAIL_ID
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("TAG_ID") = TAG_ID
            DR("INSP_ID") = ddl_dialog_INSP_Name.SelectedValue


            Try
                Dim StartDate As Date = C.StringToDate(txt_dialog_Date.Text, "yyyy-MM-dd")
                DR("RPT_Date") = StartDate
            Catch ex As Exception
                DR("RPT_Date") = DBNull.Value
            End Try

            'Try
            '    DR("RPT_Date") = C.StringToDate(txt_dialog_Date.Text, dialog_Date_Extender.Format)
            'Catch ex As Exception
            '    DR("RPT_Date") = DBNull.Value
            'End Try

            DR("RPT_STEP") = 0
            DR("RPT_Status") = 0
            DR("Created_Time") = Now
            DR("Created_By") = Session("USER_ID")
            DT.Rows.Add(DR)

            Dim cmd As New SqlCommandBuilder(DA)
            Try
                DA.Update(DT)
                DT.AcceptChanges()
            Catch ex As Exception
                lblValidation.Text = "Invalid parameter"
                pnlValidation.Visible = True
                Exit Sub
            End Try



        End If


        '--- ตรวจสอบ Daily Report Finish ทุกวัน สร้างรายการใหม่ใน Step ถัดไป








        BindComponents()



    End Sub


#Region "Image Functional"

    Private Sub lnkEdit_ClearAll_Click(sender As Object, e As EventArgs) Handles lnkEdit_ClearAll.Click
        BL.Drop_RPT_ST_TA(RPT_Year, RPT_No, TAG_ID, DETAIL_ID, INSP_ID, "RPT_ST_TA_Detail_Step", RPT_DATE)

        Try
            BL.Drop_RPT_ST_TA(RPT_Year, RPT_No, TAG_ID, DETAIL_ID, INSP_ID, "RPT_ST_TA_Detail_Step", RPT_DATE)
            'BindComponents()
            'lblBindingSuccess.Text = "successfully"
            'pnlBindingSuccess.Visible = True
            'pnlList.Visible = True
            'pnlEdit.Visible = False

            Response.Redirect("ST_TA_Inspection_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Step_ID=" & Step_ID)


        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try


    End Sub

#End Region





#Region "Template"

#Region "Section"
    Private Sub lnkAddSection_Click(sender As Object, e As EventArgs) Handles lnkAddSection.Click
        UC_Select_Template.ShowDialog()
    End Sub

    Private Sub BindTemplate()

        Dim SQL As String = ""

        SQL &= " Select  RPT_ST_TA_Detail_Sector.RPT_Year,RPT_ST_TA_Detail_Sector.RPT_No, MS_Template_Sector.*  " & vbLf
        SQL &= " From RPT_ST_TA_Detail_Sector  " & vbLf
        SQL &= " Left Join MS_Template_Sector On MS_Template_Sector.Sector_ID=RPT_ST_TA_Detail_Sector.Sector_ID  " & vbLf
        SQL &= " where RPT_Year = " & RPT_Year & " And RPT_No = " & RPT_No & " And RPT_ST_TA_Detail_Sector.Sector_ID Is Not NULL  AND DETAIL_STEP_ID=" & DETAIL_STEP_ID & vbLf
        SQL &= " ORDER BY RPT_ST_TA_Detail_Sector.Sector_ID  " & vbLf
        Dim DA As SqlDataAdapter = New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As DataTable = New DataTable
        DA.Fill(DT)

        rptINSP.DataSource = DT
        rptINSP.DataBind()

    End Sub

    Private Sub UC_Select_Template_SelectTemplate(TemplateType As UC_Select_Template.TemplateType) Handles UC_Select_Template.SelectTemplate
        Dim dt As DataTable = BuiltDataGridview()


        'Dim dr As DataRow = dt.NewRow
        'dr("OffRoutineTemplateType") = TemplateType.ToString
        'dr("INSP_Name") = ""
        'dr("STATUS_Name") = ""
        ''dr("ICLS_ID") = ""
        ''dr("PIC_Detail1") = ""
        'dr("DETAIL_ID") = ""
        'dr("PROB_Detail") = ""
        'dr("UNIQUE_POPUP_ID") = Now.ToOADate.ToString.Replace(".", "")

        ''---------
        'dr("Template_ID") = Convert.ToInt32(TemplateType)



        'dt.Rows.Add(dr)

        '==================MS_Template_Sector=========================
        '----เพิ่มรายการใน Database ----
        Dim Sql As String = ""
        Sql = " Select * FROM MS_Template_Sector where 0=1"
        Dim DA As SqlDataAdapter = New SqlDataAdapter(Sql, BL.ConnStr)
        Dim DT_Insert As DataTable = New DataTable
        DA.Fill(DT_Insert)

        Dim DR_Insert As DataRow
        DR_Insert = DT_Insert.NewRow
        DR_Insert("Sector_ID") = BL.GetNew_Table_ID("MS_Template_Sector", "Sector_ID")
        DR_Insert("Template_ID") = Convert.ToInt32(TemplateType)

        DT_Insert.Rows.Add(DR_Insert)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT_Insert)
            DT_Insert.AcceptChanges()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        '===========================================
        '----เพิ่มรายการใน Database ----
        If (DT_Insert.Rows.Count > 0) Then
            Dim _Sector_ID As Integer = DT_Insert.Rows(0).Item("Sector_ID")
            Sql = ""
            Sql = " Select * FROM RPT_ST_TA_Detail_Sector where DETAIL_STEP_ID=" & DETAIL_STEP_ID & " And Sector_ID=" & DT_Insert.Rows(0).Item("Sector_ID")
            DA = New SqlDataAdapter(Sql, BL.ConnStr)
            DT_Insert = New DataTable
            DA.Fill(DT_Insert)

            If (DT_Insert.Rows.Count = 0) Then

                Dim DR_Insert_Sub As DataRow
                DR_Insert_Sub = DT_Insert.NewRow
                DR_Insert_Sub("RPT_Year") = RPT_Year
                DR_Insert_Sub("RPT_No") = RPT_No
                DR_Insert_Sub("Sector_ID") = _Sector_ID
                DR_Insert_Sub("DETAIL_STEP_ID") = DETAIL_STEP_ID
                DR_Insert_Sub("Sector_SEQ") = dt.Rows.Count + 1

                DT_Insert.Rows.Add(DR_Insert_Sub)

                cmd = New SqlCommandBuilder(DA)
                Try
                    DA.Update(DT_Insert)
                    DT_Insert.AcceptChanges()
                Catch ex As Exception
                    lblValidation.Text = "Invalid parameter"
                    pnlValidation.Visible = True
                    Exit Sub
                End Try
            End If

        End If



        '===========================================
        BindTemplate()


        'rptINSP.DataSource = dt
        'rptINSP.DataBind()
    End Sub

    Private Function BuiltDataGridview() As DataTable
        Dim dt As New DataTable

        ''----
        dt.Columns.Add("Sector_ID")
        dt.Columns.Add("Template_ID")
        dt.Columns.Add("Text_1")
        dt.Columns.Add("Text_2")
        dt.Columns.Add("Text_3")
        dt.Columns.Add("Text_4")


        For i As Integer = 0 To rptINSP.Items.Count - 1
            Dim lblNo As Label = rptINSP.Items(i).FindControl("lblNo")
            Dim lblINSP As Label = rptINSP.Items(i).FindControl("lblINSP")
            Dim lblStatus As Label = rptINSP.Items(i).FindControl("lblStatus")
            Dim lblIclsID As Label = rptINSP.Items(i).FindControl("lblIclsID")
            Dim lblDetailID As Label = rptINSP.Items(i).FindControl("lblDetailID")
            Dim lblOffRoutineTemplateType As Label = rptINSP.Items(i).FindControl("lblOffRoutineTemplateType")
            Dim lblPROB_Detail As Label = rptINSP.Items(i).FindControl("lblPROB_Detail")


            Dim dr As DataRow = dt.NewRow

            dr("Sector_ID") = lblDetailID.Text
            dr("Template_ID") = lblNo.Attributes("Template_ID")

            Select Case lblNo.Attributes("Template_ID")
                Case 1
                    Dim UC As UC_ST_TA_Template1 = rptINSP.Items(i).FindControl("UC_ST_TA_Template1")
                    dr("Text_1") = UC.TextDetail.Text
                Case 2
                    Dim UC As UC_ST_TA_Template2 = rptINSP.Items(i).FindControl("UC_ST_TA_Template2")
                    dr("Text_1") = UC.TextDetail.Text
                Case 3
                    Dim UC As UC_ST_TA_Template3 = rptINSP.Items(i).FindControl("UC_ST_TA_Template3")
                    dr("Text_1") = UC.TextDetail.Text
                Case 4
                    Dim UC As UC_ST_TA_Template4 = rptINSP.Items(i).FindControl("UC_ST_TA_Template4")

                Case 5
                    Dim UC As UC_ST_TA_Template5 = rptINSP.Items(i).FindControl("UC_ST_TA_Template5")
                    dr("Text_1") = UC.TextDetail.Text
                Case 6
                    Dim UC As UC_ST_TA_Template6 = rptINSP.Items(i).FindControl("UC_ST_TA_Template6")
                    'dr("Text_1") = UC.TextDetail.Text
                    'dr("Text_2") = UC.TextDetail2.Text
                    'dr("Text_3") = UC.TextDetail3.Text
                    'dr("Text_4") = UC.TextDetail4.Text

                    dr("Text_1") = UC.TextDetail_Pic1
                    dr("Text_2") = UC.TextDetail_Pic2
                    dr("Text_3") = UC.TextDetail_Pic3
                    dr("Text_4") = UC.TextDetail_Pic4
                Case 7
                    Dim UC As UC_ST_TA_Template7 = rptINSP.Items(i).FindControl("UC_ST_TA_Template7")
                    dr("Text_1") = UC.TextDetail_Pic1

                    dr("Text_3") = UC.TextDetail_Pic3
                    dr("Text_4") = UC.TextDetail_Pic4

                Case 8
                    Dim UC As UC_ST_TA_Template8 = rptINSP.Items(i).FindControl("UC_ST_TA_Template8")
                    dr("Text_1") = UC.TextDetail_Pic1
                    dr("Text_2") = UC.TextDetail_Pic2

                Case 9
                    Dim UC As UC_ST_TA_Template9 = rptINSP.Items(i).FindControl("UC_ST_TA_Template9")
                    dr("Text_1") = UC.TextDetail_Pic1
                    dr("Text_2") = UC.TextDetail_Pic2
                    dr("Text_3") = UC.TextDetail_Pic3
            End Select

            dt.Rows.Add(dr)
        Next

        Return dt
    End Function
#End Region

    Protected Sub rptINSP_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptINSP.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Select Case e.CommandName
            Case "Delete"
                Dim dt As DataTable = BuiltDataGridview()
                dt.Rows.RemoveAt(e.Item.ItemIndex)

                '----ลบใน DB ด้วย---

                'btnDelete.CommandArgument
                Dim SQL As String = ""
                SQL = " DELETE FROM MS_Template_Sector " & vbNewLine
                SQL &= " WHERE  Sector_ID =" & btnDelete.CommandArgument

                SQL &= " DELETE FROM RPT_ST_TA_Detail_Sector " & vbNewLine
                SQL &= " WHERE  Sector_ID =" & btnDelete.CommandArgument
                BL.Execute_Command(SQL)


                rptINSP.DataSource = dt
                rptINSP.DataBind()

        End Select


    End Sub


    Protected Sub rptINSP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptINSP.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub


        If Convert.IsDBNull(e.Item.DataItem("Template_ID")) = False Then
            Dim lblNo As Label = e.Item.FindControl("lblNo")
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
            Dim lblDetailID As Label = e.Item.FindControl("lblDetailID")

            lblNo.Text = e.Item.ItemIndex + 1
            lblNo.Attributes("Sector_ID") = e.Item.DataItem("Sector_ID")
            lblNo.Attributes("Template_ID") = e.Item.DataItem("Template_ID")
            btnDelete.CommandArgument = e.Item.DataItem("Sector_ID")
            Select Case e.Item.DataItem("Template_ID")

                '-----------------------Template 1----------------------
                Case Convert.ToInt32(UC_ST_TA_SelectTemplate.ST_TA_TemplateType.Template1)
                    Dim UC As UC_ST_TA_Template1 = e.Item.FindControl("UC_ST_TA_Template1")
                    UC.Visible = True
                    '==============Picture 1=================

                    '----------------Dialog image-----------------
                    Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                    UC.UNIQUE_POPUP_ID = UNIQUEKEY
                    UC.Sector_ID = e.Item.DataItem("Sector_ID")
                    UC.Disabled = False
                    UC.RPT_Absorber_Step_ID = DETAIL_STEP_ID

                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")) Then
                        UC.MY_PREVIEW1 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1") = Nothing
                    End If


                    Dim Para As String = "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Sector_ID=" & e.Item.DataItem("Sector_ID")
                    UC.ImageUrl(Para)

                    '==============Text 1=================
                    If Not IsDBNull(e.Item.DataItem("Text_1")) Then
                        UC.TextDetail.Text = e.Item.DataItem("Text_1")
                        lblDetailID.Attributes("Text_1") = e.Item.DataItem("Text_1").ToString()
                    End If

                    '===============================
                    lblDetailID.Text = e.Item.DataItem("Sector_ID")

                '---------------------End Template 1----------------------


                '-----------------------Template 2----------------------
                Case Convert.ToInt32(UC_ST_TA_SelectTemplate.ST_TA_TemplateType.Template2)
                    Dim UC As UC_ST_TA_Template2 = e.Item.FindControl("UC_ST_TA_Template2")
                    UC.Visible = True
                    '----------------Dialog image-----------------
                    Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                    UC.UNIQUE_POPUP_ID = UNIQUEKEY
                    UC.Sector_ID = e.Item.DataItem("Sector_ID")
                    UC.Disabled = False
                    UC.RPT_Absorber_Step_ID = DETAIL_STEP_ID

                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")) Then
                        UC.MY_PREVIEW1 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1") = Nothing
                    End If


                    Dim Para As String = "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Sector_ID=" & e.Item.DataItem("Sector_ID")
                    UC.ImageUrl(Para)

                    '==============Text 1=================
                    If Not IsDBNull(e.Item.DataItem("Text_1")) Then
                        UC.TextDetail.Text = e.Item.DataItem("Text_1")
                        lblDetailID.Attributes("Text_1") = e.Item.DataItem("Text_1").ToString()
                    End If


                    '==============================
                    lblDetailID.Text = e.Item.DataItem("Sector_ID")

                    '---------------------End Template 2----------------------

                '-----------------------Template 3----------------------
                Case Convert.ToInt32(UC_ST_TA_SelectTemplate.ST_TA_TemplateType.Template3)
                    Dim UC As UC_ST_TA_Template3 = e.Item.FindControl("UC_ST_TA_Template3")
                    UC.Visible = True
                    '----------------Dialog image-----------------
                    Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                    UC.UNIQUE_POPUP_ID = UNIQUEKEY
                    UC.Sector_ID = e.Item.DataItem("Sector_ID")
                    UC.Disabled = False
                    UC.RPT_Absorber_Step_ID = DETAIL_STEP_ID

                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")) Then
                        UC.MY_PREVIEW1 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1") = Nothing
                    End If


                    Dim Para As String = "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Sector_ID=" & e.Item.DataItem("Sector_ID")
                    UC.ImageUrl(Para)

                    '==============Text 1=================
                    If Not IsDBNull(e.Item.DataItem("Text_1")) Then
                        UC.TextDetail.Text = e.Item.DataItem("Text_1")
                        lblDetailID.Attributes("Text_1") = e.Item.DataItem("Text_1").ToString()
                    End If
                    lblDetailID.Text = e.Item.DataItem("Sector_ID")

                    '---------------------End Template 3----------------------

                '-----------------------Template 4----------------------
                Case Convert.ToInt32(UC_ST_TA_SelectTemplate.ST_TA_TemplateType.Template4)
                    Dim UC As UC_ST_TA_Template4 = e.Item.FindControl("UC_ST_TA_Template4")
                    UC.Visible = True
                    '----------------Dialog image-----------------
                    Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                    UC.UNIQUE_POPUP_ID = UNIQUEKEY
                    UC.Sector_ID = e.Item.DataItem("Sector_ID")
                    UC.Disabled = False
                    UC.RPT_Absorber_Step_ID = DETAIL_STEP_ID

                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")) Then
                        UC.MY_PREVIEW1 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1") = Nothing
                    End If


                    Dim Para As String = "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Sector_ID=" & e.Item.DataItem("Sector_ID")
                    UC.ImageUrl(Para)

                    '==============Text 1=================
                    If Not IsDBNull(e.Item.DataItem("Text_1")) Then

                    End If

                    lblDetailID.Text = e.Item.DataItem("Sector_ID")

                    '---------------------End Template 4----------------------

                '-----------------------Template 5----------------------
                Case Convert.ToInt32(UC_ST_TA_SelectTemplate.ST_TA_TemplateType.Template5)
                    Dim UC As UC_ST_TA_Template5 = e.Item.FindControl("UC_ST_TA_Template5")
                    UC.Visible = True
                    '----------------Dialog image-----------------
                    Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                    UC.UNIQUE_POPUP_ID = UNIQUEKEY
                    UC.Sector_ID = e.Item.DataItem("Sector_ID")
                    UC.Disabled = False
                    UC.RPT_Absorber_Step_ID = DETAIL_STEP_ID

                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")) Then
                        UC.MY_PREVIEW1 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1") = Nothing
                    End If


                    Dim Para As String = "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Sector_ID=" & e.Item.DataItem("Sector_ID")
                    UC.ImageUrl(Para)

                    '==============Text 1=================
                    If Not IsDBNull(e.Item.DataItem("Text_1")) Then
                        UC.TextDetail.Text = e.Item.DataItem("Text_1")
                        lblDetailID.Attributes("Text_1") = e.Item.DataItem("Text_1").ToString()
                    End If
                    '==============Picture 2=================

                    '==============Text 2=================
                    lblDetailID.Text = e.Item.DataItem("Sector_ID")

                    '---------------------End Template 5----------------------


           '-----------------------Template 6----------------------
                Case Convert.ToInt32(UC_ST_TA_SelectTemplate.ST_TA_TemplateType.Template6)
                    Dim UC As UC_ST_TA_Template6 = e.Item.FindControl("UC_ST_TA_Template6")
                    UC.Visible = True
                    '----------------Dialog image-----------------
                    Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                    UC.UNIQUE_POPUP_ID = UNIQUEKEY
                    UC.Sector_ID = e.Item.DataItem("Sector_ID")
                    UC.Disabled = False
                    UC.RPT_Absorber_Step_ID = DETAIL_STEP_ID

                    '==============Picture 1=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")) Then
                        UC.MY_PREVIEW1 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1") = Nothing
                    End If


                    Dim Para As String = "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Sector_ID=" & e.Item.DataItem("Sector_ID")
                    UC.ImageUrl(Para)

                    '==============Text 1=================
                    If Not IsDBNull(e.Item.DataItem("Text_1")) Then
                        UC.TextDetail_Pic1 = e.Item.DataItem("Text_1")
                        lblDetailID.Attributes("Text_1") = e.Item.DataItem("Text_1").ToString()
                    End If


                    '==============Picture 2=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2")) Then
                        UC.MY_PREVIEW2 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2") = Nothing
                    End If


                    '==============Text 2=================
                    If Not IsDBNull(e.Item.DataItem("Text_2")) Then
                        UC.TextDetail_Pic2 = e.Item.DataItem("Text_2")
                        lblDetailID.Attributes("Text_2") = e.Item.DataItem("Text_2").ToString()
                    End If

                    '==============Picture 3=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_3")) Then
                        UC.MY_PREVIEW3 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_3")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_3") = Nothing
                    End If


                    '==============Text 3=================
                    If Not IsDBNull(e.Item.DataItem("Text_3")) Then
                        UC.TextDetail_Pic3 = e.Item.DataItem("Text_3")
                        lblDetailID.Attributes("Text_3") = e.Item.DataItem("Text_3").ToString()
                    End If

                    '==============Picture 4=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_4")) Then
                        UC.MY_PREVIEW4 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_4")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_4") = Nothing
                    End If


                    '==============Text 4=================
                    If Not IsDBNull(e.Item.DataItem("Text_4")) Then
                        UC.TextDetail_Pic4 = e.Item.DataItem("Text_4")
                        lblDetailID.Attributes("Text_4") = e.Item.DataItem("Text_4").ToString()
                    End If

                    lblDetailID.Text = e.Item.DataItem("Sector_ID")


            '---------------------End Template 6----------------------


            '-----------------------Template 7----------------------
                Case Convert.ToInt32(UC_ST_TA_SelectTemplate.ST_TA_TemplateType.Template7)
                    Dim UC As UC_ST_TA_Template7 = e.Item.FindControl("UC_ST_TA_Template7")
                    UC.Visible = True
                    '----------------Dialog image-----------------
                    Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                    UC.UNIQUE_POPUP_ID = UNIQUEKEY
                    UC.Sector_ID = e.Item.DataItem("Sector_ID")
                    UC.Disabled = False
                    UC.RPT_Absorber_Step_ID = DETAIL_STEP_ID

                    '==============Picture 1=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")) Then
                        UC.MY_PREVIEW1 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1") = Nothing
                    End If

                    '==============Text 1=================
                    If Not IsDBNull(e.Item.DataItem("Text_1")) Then
                        UC.TextDetail_Pic1 = e.Item.DataItem("Text_1")
                        lblDetailID.Attributes("Text_1") = e.Item.DataItem("Text_1").ToString()
                    End If

                    Dim Para As String = "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Sector_ID=" & e.Item.DataItem("Sector_ID")
                    UC.ImageUrl(Para)


                    ''==============Picture 2=================
                    'If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2")) Then
                    '    UC.MY_PREVIEW2 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2")
                    '    Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2") = Nothing
                    'End If



                    '==============Picture 3=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_3")) Then
                        UC.MY_PREVIEW3 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_3")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_3") = Nothing
                    End If
                    '==============Text 3=================
                    If Not IsDBNull(e.Item.DataItem("Text_3")) Then
                        UC.TextDetail_Pic3 = e.Item.DataItem("Text_3")
                        lblDetailID.Attributes("Text_3") = e.Item.DataItem("Text_3").ToString()
                    End If

                    '==============Picture 4=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_4")) Then
                        UC.MY_PREVIEW4 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_4")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_4") = Nothing
                    End If
                    '==============Text 4=================
                    If Not IsDBNull(e.Item.DataItem("Text_4")) Then
                        UC.TextDetail_Pic4 = e.Item.DataItem("Text_4")
                        lblDetailID.Attributes("Text_4") = e.Item.DataItem("Text_4").ToString()
                    End If

                    lblDetailID.Text = e.Item.DataItem("Sector_ID")


                    '---------------------End Template 7----------------------


                     '-----------------------Template 8----------------------
                Case Convert.ToInt32(UC_ST_TA_SelectTemplate.ST_TA_TemplateType.Template8)
                    Dim UC As UC_ST_TA_Template8 = e.Item.FindControl("UC_ST_TA_Template8")
                    UC.Visible = True
                    '----------------Dialog image-----------------
                    Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                    UC.UNIQUE_POPUP_ID = UNIQUEKEY
                    UC.Sector_ID = e.Item.DataItem("Sector_ID")
                    UC.Disabled = False
                    UC.RPT_Absorber_Step_ID = DETAIL_STEP_ID

                    '==============Picture 1=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")) Then
                        UC.MY_PREVIEW1 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1") = Nothing
                    End If


                    Dim Para As String = "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Sector_ID=" & e.Item.DataItem("Sector_ID")
                    UC.ImageUrl(Para)

                    '==============Text 1=================
                    If Not IsDBNull(e.Item.DataItem("Text_1")) Then
                        UC.TextDetail_Pic1 = e.Item.DataItem("Text_1")
                        lblDetailID.Attributes("Text_1") = e.Item.DataItem("Text_1").ToString()
                    End If


                    '==============Picture 2=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2")) Then
                        UC.MY_PREVIEW2 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2") = Nothing
                    End If


                    '==============Text 2=================
                    If Not IsDBNull(e.Item.DataItem("Text_2")) Then
                        UC.TextDetail_Pic2 = e.Item.DataItem("Text_2")
                        lblDetailID.Attributes("Text_2") = e.Item.DataItem("Text_2").ToString()
                    End If

                    lblDetailID.Text = e.Item.DataItem("Sector_ID")


                    '---------------------End Template 8----------------------


                          '-----------------------Template 9----------------------
                Case Convert.ToInt32(UC_ST_TA_SelectTemplate.ST_TA_TemplateType.Template9)
                    Dim UC As UC_ST_TA_Template9 = e.Item.FindControl("UC_ST_TA_Template9")
                    UC.Visible = True
                    '----------------Dialog image-----------------
                    Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                    UC.UNIQUE_POPUP_ID = UNIQUEKEY
                    UC.Sector_ID = e.Item.DataItem("Sector_ID")
                    UC.Disabled = False
                    UC.RPT_Absorber_Step_ID = DETAIL_STEP_ID

                    '==============Picture 1=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")) Then
                        UC.MY_PREVIEW1 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_1") = Nothing
                    End If


                    Dim Para As String = "&RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Sector_ID=" & e.Item.DataItem("Sector_ID")
                    UC.ImageUrl(Para)

                    '==============Text 1=================
                    If Not IsDBNull(e.Item.DataItem("Text_1")) Then
                        UC.TextDetail_Pic1 = e.Item.DataItem("Text_1")
                        lblDetailID.Attributes("Text_1") = e.Item.DataItem("Text_1").ToString()
                    End If


                    '==============Picture 2=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2")) Then
                        UC.MY_PREVIEW2 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_2") = Nothing
                    End If


                    '==============Text 2=================
                    If Not IsDBNull(e.Item.DataItem("Text_2")) Then
                        UC.TextDetail_Pic2 = e.Item.DataItem("Text_2")
                        lblDetailID.Attributes("Text_2") = e.Item.DataItem("Text_2").ToString()
                    End If


                    '==============Picture 3=================
                    If Not IsNothing(Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_3")) Then
                        UC.MY_PREVIEW3 = Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_3")
                        Session("TempImage_" & UNIQUE_SECTOR_POPUP_ID & "_3") = Nothing
                    End If


                    '==============Text 3=================
                    If Not IsDBNull(e.Item.DataItem("Text_3")) Then
                        UC.TextDetail_Pic3 = e.Item.DataItem("Text_3")
                        lblDetailID.Attributes("Text_3") = e.Item.DataItem("Text_3").ToString()
                    End If


                    lblDetailID.Text = e.Item.DataItem("Sector_ID")



                    '---------------------End Template 9----------------------
            End Select

        End If

    End Sub



#End Region



    Private Sub Add_InStep_Repair()
        If (Step_ID = EIR_BL.ST_TA_STEP.After_Clean) Then
            '--Insert ใน Step Repair--
            Dim DR As DataRow
            '----สร้างรายงาน รอใน Step ถัดไป
            '-----ตรวจสอบรอบว่ามีรอบและจุดตรวจหรือไม่----
            Dim SQL_InsertNextStep As String = ""
            SQL_InsertNextStep &= "    SELECT * FROM RPT_ST_TA_Detail_Step " & vbNewLine
            SQL_InsertNextStep &= "    WHERE DETAIL_ID=" & DETAIL_ID & " AND RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & "  AND TAG_ID=" & TAG_ID & " AND INSP_ID=" & INSP_ID & " AND INSP_STATUS_ID=" & INSP_STATUS_ID & " AND Step_ID=" & EIR_BL.ST_TA_STEP.Repair
            Dim DA_InsertNextStep As New SqlDataAdapter(SQL_InsertNextStep, BL.ConnStr)
            Dim DT_InsertNextStep As New DataTable
            DA_InsertNextStep.Fill(DT_InsertNextStep)
            '---ตรวจสอบว่ามีการเพิ่มรายการตรวจรอแล้วหรือยัง  ?
            If (DT_InsertNextStep.Rows.Count = 0) Then

                '---สร้างรายการ
                Dim DETAIL_STEP_ID_NextStep = BL.GetNew_Table_ID("RPT_ST_TA_Detail_Step", "DETAIL_STEP_ID")
                '------2. สร้างรายการของ Insp

                DR = DT_InsertNextStep.NewRow
                DR("DETAIL_STEP_ID") = DETAIL_STEP_ID_NextStep
                DR("DETAIL_ID") = DETAIL_ID
                DR("RPT_Year") = RPT_Year
                DR("RPT_No") = RPT_No
                DR("TAG_ID") = TAG_ID
                DR("INSP_ID") = INSP_ID
                DR("INSP_STATUS_ID") = INSP_STATUS_ID

                Dim DT_Date_Step As DataTable = BL.DT_Daily_Date(RPT_Year, RPT_No, TAG_ID, INSP_ID, INSP_STATUS_ID)
                DT_Date_Step.DefaultView.RowFilter = "STEP_ID=" & Step_ID & " AND STATUS_ID IS NOT NULL "
                Dim DT_RowFilter = DT_Date_Step.DefaultView.ToTable()
                Dim Max_CLASS As Integer = 0
                If (DT_Date_Step.DefaultView.Count > 0) Then

                    Max_CLASS = DT_Date_Step.DefaultView(0).Item("STATUS_ID") '-- สถานะล่าสุด
                End If

                DR("STATUS_ID") = Max_CLASS      '--Level Class A-C หา status ล่าสุด
                '----รายการรอให้วันที่ null
                DR("RPT_Date") = DBNull.Value
                DR("Created_Time") = Now
                DR("Created_By") = Session("USER_ID")


                DR("Step_ID") = EIR_BL.ST_TA_STEP.Repair

                DT_InsertNextStep.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA_InsertNextStep)
                Try


                    '----สร้างรายงาน หลัก
                    DA_InsertNextStep.Update(DT_InsertNextStep)
                    DT_InsertNextStep.AcceptChanges()
                Catch ex As Exception
                    lblValidation_dialog.Text = "Invalid parameter"
                    pnlValidation_dialog.Visible = True
                    Exit Sub
                End Try

            End If


        End If


    End Sub

    Private Sub lnkPreview_Edit_Click(sender As Object, e As EventArgs) Handles lnkPreview_Edit.Click

        Dim Filter As String = ""
        Dim Filter_Daily As String = ""
        Filter &= " AND Detail_Step.DETAIL_STEP_ID=" & DETAIL_STEP_ID & "   " & vbNewLine
        Filter_Daily = "  AND DETAIL_STEP_ID =" & DETAIL_STEP_ID & " "

        Session("Filter_Daily") = Filter_Daily
        Session("Filter") = Filter
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('GL_ST_TA_Report.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "');", True)

    End Sub

End Class


