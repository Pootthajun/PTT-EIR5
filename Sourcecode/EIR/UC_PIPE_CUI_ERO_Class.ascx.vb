﻿Public Class UC_PIPE_CUI_ERO_Class
    Inherits System.Web.UI.UserControl

    Public Property SuggessClass As Object
        Get
            Select Case lnkSuggessClass.Text
                Case "Suggess : ClassA"
                    Return EIR_BL.InspectionLevel.ClassA
                Case "Suggess : ClassB"
                    Return EIR_BL.InspectionLevel.ClassB
                Case "Suggess : ClassC"
                    Return EIR_BL.InspectionLevel.ClassC
                Case "Suggess : Normal"
                    Return EIR_BL.InspectionLevel.Normal
                Case Else
                    Return Nothing
            End Select
        End Get
        Set(value As Object)

            lnkSuggessClass.Text = ""
            If IsNumeric(value) Then
                Select Case value
                    Case EIR_BL.InspectionLevel.ClassC
                        lnkSuggessClass.Text = "Suggess : ClassC"
                        lnkSuggessClass.CssClass = "LevelClassC"
                    Case EIR_BL.InspectionLevel.ClassA
                        lnkSuggessClass.Text = "Suggess : ClassA"
                        lnkSuggessClass.CssClass = "LevelClassA"
                    Case EIR_BL.InspectionLevel.ClassB
                        lnkSuggessClass.Text = "Suggess : ClassB"
                        lnkSuggessClass.CssClass = "LevelClassB"
                    Case EIR_BL.InspectionLevel.Normal
                        lnkSuggessClass.Text = "Suggess : Normal"
                        lnkSuggessClass.CssClass = "LevelNormal"
                End Select
            End If

        End Set
    End Property

    Public Property ICLS_ID As Object
        Get
            Select Case True
                Case rdoClassA.Checked
                    Return EIR_BL.InspectionLevel.ClassA
                Case rdoClassB.Checked
                    Return EIR_BL.InspectionLevel.ClassB
                Case rdoClassC.Checked
                    Return EIR_BL.InspectionLevel.ClassC
                Case rdoNormal.Checked
                    Return EIR_BL.InspectionLevel.Normal
                Case Else
                    Return DBNull.Value
            End Select
        End Get
        Set(value As Object)
            rdoClassA.Checked = IsNumeric(value) AndAlso value = EIR_BL.InspectionLevel.ClassA
            rdoClassB.Checked = IsNumeric(value) AndAlso value = EIR_BL.InspectionLevel.ClassB
            rdoClassC.Checked = IsNumeric(value) AndAlso value = EIR_BL.InspectionLevel.ClassC
            rdoNormal.Checked = IsNumeric(value) AndAlso value = EIR_BL.InspectionLevel.Normal
            UpdateSelected()
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        UpdateSelected()
    End Sub

    Private Sub lnkSuggessClass_Click(sender As Object, e As EventArgs) Handles lnkSuggessClass.Click
        ICLS_ID = SuggessClass
    End Sub

    Private Sub rdoPrefer_CheckedChanged(sender As Object, e As EventArgs) Handles rdoClassA.CheckedChanged, rdoClassB.CheckedChanged, rdoClassC.CheckedChanged, rdoNormal.CheckedChanged
        UpdateSelected()
    End Sub

    Private Sub UpdateSelected()
        trClassA.Attributes("class") = ""
        trClassB.Attributes("class") = ""
        trClassC.Attributes("class") = ""
        trClassNormal.Attributes("class") = ""

        If Not IsDBNull(ICLS_ID) Then
            Select Case ICLS_ID
                Case EIR_BL.InspectionLevel.ClassA
                    trClassA.Attributes("class") = "trClassA"
                Case EIR_BL.InspectionLevel.ClassB
                    trClassB.Attributes("class") = "trClassB"
                Case EIR_BL.InspectionLevel.ClassC
                    trClassC.Attributes("class") = "trClassC"
                Case EIR_BL.InspectionLevel.Normal
                    trClassNormal.Attributes("class") = "trNormal"
            End Select
        End If

    End Sub

End Class