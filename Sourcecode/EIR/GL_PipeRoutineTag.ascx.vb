﻿Imports System.Data
Imports System.Data.SqlClient

Public Class GL_PipeRoutineTag
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE

#Region "Property"
    Public Property RPT_Year() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_Year")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_Year")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_Year") = value
        End Set
    End Property

    Public Property RPT_No() As Integer
        Get
            If Not IsNumeric(Me.Attributes("RPT_No")) Then
                Return 0
            Else
                Return Me.Attributes("RPT_No")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_No") = value
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If Not IsNumeric(Me.Attributes("TAG_ID")) Then
                Return 0
            Else
                Return Me.Attributes("TAG_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("TAG_ID") = value
        End Set
    End Property

    Public Property TAG_CODE() As String
        Get
            Return lbl_Code.InnerHtml
        End Get
        Set(ByVal value As String)
            lbl_Code.InnerHtml = value
        End Set
    End Property

    Public Property REMAIN_PROBLEM() As Integer
        Get
            If Not IsNumeric(lbl_Remain.InnerHtml) Then
                Return 0
            Else
                Return lbl_Remain.InnerHtml
            End If
        End Get
        Set(ByVal value As Integer)
            lbl_Remain.InnerHtml = value
        End Set
    End Property

    Public Property NEW_UPLOAD() As Integer
        Get
            If Not IsNumeric(lbl_New.InnerHtml) Then
                Return 0
            Else
                Return lbl_New.InnerHtml
            End If
        End Get
        Set(ByVal value As Integer)
            lbl_New.InnerHtml = value
        End Set
    End Property

    Public Property TRACE_COUNT() As Integer
        Get
            If Not IsNumeric(lbl_TraceTag.InnerHtml) Then
                Return 0
            Else
                Return lbl_TraceTag.InnerHtml
            End If
        End Get
        Set(ByVal value As Integer)
            lbl_TraceTag.InnerHtml = value
        End Set
    End Property

    Public Property LEVEL() As EIR_BL.InspectionLevel
        Get
            Select Case lbl_Level.InnerHtml
                Case "ClassA"
                    Return EIR_BL.InspectionLevel.ClassA
                Case "ClassB"
                    Return EIR_BL.InspectionLevel.ClassB
                Case "ClassC"
                    Return EIR_BL.InspectionLevel.ClassC
                Case Else
                    Return EIR_BL.InspectionLevel.Normal
            End Select
        End Get
        Set(ByVal value As EIR_BL.InspectionLevel)
            Select Case value
                Case EIR_BL.InspectionLevel.ClassA
                    lbl_Level.InnerHtml = "ClassA"
                Case EIR_BL.InspectionLevel.ClassB
                    lbl_Level.InnerHtml = "ClassB"
                Case EIR_BL.InspectionLevel.ClassC
                    lbl_Level.InnerHtml = "ClassC"
                Case EIR_BL.InspectionLevel.Normal, -1
                    lbl_Level.InnerHtml = "Normal"
                    value = EIR_BL.InspectionLevel.Normal
            End Select
            lbl_Level.Attributes("class") = BL.Get_Inspection_Css_Text_By_Level(value)
            TDTree.Attributes("Class") = BL.Get_Inspection_Css_Box_By_Level(value)
        End Set
    End Property

    Public Property IsExpand() As Boolean
        Get
            Return TrInspection.Visible
        End Get
        Set(ByVal value As Boolean)
            TrInspection.Visible = value
            '------------------Bind Inspection ----------------
            If Not value Then Exit Property

            Dim SQL As String = "SELECT * FROM VW_PIPE_ROUTINE_DETAIL " & vbNewLine
            SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID & vbNewLine
            SQL &= "ORDER BY INSP_ID" & vbNewLine

            Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)

            DT.DefaultView.RowFilter = ""
            rptInspection.DataSource = DT
            rptInspection.DataBind()
        End Set
    End Property

    Public Property WarningType() As EIR_BL.Warning
        Get
            If Not imgWarning.Visible Then Return EIR_BL.Warning.Normal
            Select Case imgWarning.ImageUrl
                Case "resources/images/icons/alert.gif"
                    Return EIR_BL.Warning.Alert
                Case "resources/images/icons/warning.gif"
                    Return EIR_BL.Warning.Warning
            End Select
            Return EIR_BL.Warning.Normal
        End Get
        Set(ByVal value As EIR_BL.Warning)
            Select Case value
                Case EIR_BL.Warning.Normal
                    imgWarning.Visible = False
                Case EIR_BL.Warning.Warning
                    imgWarning.Visible = True
                    imgWarning.ImageUrl = "resources/images/icons/warning.gif"
                Case EIR_BL.Warning.Alert
                    imgWarning.Visible = True
                    imgWarning.ImageUrl = "resources/images/icons/alert.gif"
            End Select
        End Set
    End Property

#End Region


    Public Sub ClearDetail()
        TAG_CODE = ""
        REMAIN_PROBLEM = 0
        NEW_UPLOAD = 0
        TRACE_COUNT = 0
        WarningType = EIR_BL.Warning.Normal
    End Sub

    Public Sub BindHeader()
        ClearDetail()

        '---------------- Bind Tag Detail ---------------
        Dim SQL As String = "SELECT TAG_CODE,COUNT(1) TotalINSP," & vbNewLine
        SQL &= "dbo.UDF_Calculate_Incomplete_PIPE_Routine_Tag_In_Report(" & RPT_Year & "," & RPT_No & "," & TAG_ID & ") Issue" & vbNewLine & vbNewLine
        SQL &= "FROM VW_PIPE_TAG TAG" & vbNewLine
        SQL &= "LEFT JOIN (SELECT DISTINCT TAG_TYPE_ID,INSP_ID FROM MS_ST_TAG_Inspection ) INSP ON INSP.TAG_TYPE_ID=" & EIR_PIPE.TAG_TYPE_PIPE & vbNewLine
        SQL &= "WHERE TAG_ID=" & TAG_ID & vbNewLine
        SQL &= "GROUP BY TAG_CODE" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Exit Sub

        TAG_CODE = DT.Rows(0).Item("TAG_CODE")

        If Not IsDBNull(DT.Rows(0).Item("Issue")) AndAlso DT.Rows(0).Item("Issue") > 0 Then
            imgWarning.ToolTip = "Please upload image and more detail for all required inspection point !!" & vbNewLine & "Click expand tag detail to see more information"
            WarningType = EIR_BL.Warning.Alert
        End If
        '------Total -------------
        lbl_Item.InnerHtml = FormatNumber(DT.Rows(0).Item("TotalINSP"), 0)

        '--------------------- Bind Current Status ------------------
        SQL = "SELECT INSP_ID,LAST_LEVEL,DETAIL_ID,CURRENT_COMPONENT,LAST_COMPONENT,CURRENT_LEVEL" & vbNewLine
        SQL &= "FROM VW_PIPE_ROUTINE_DETAIL " & vbNewLine
        SQL &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID=" & TAG_ID

        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            REMAIN_PROBLEM = 0
            NEW_UPLOAD = 0
            TRACE_COUNT = 0
            Exit Sub
        End If

        '---------------------- Get Summary -----------------
        Dim HCol() As String = {"INSP_ID"}
        Dim AllCol() As String = {"INSP_ID", "LAST_LEVEL", "CURRENT_LEVEL", "DETAIL_ID"}
        '------Remain-------------
        DT.DefaultView.RowFilter = "LAST_LEVEL>0"
        lbl_Remain.InnerHtml = FormatNumber(DT.DefaultView.ToTable(True, AllCol).Copy.Rows.Count, 0)
        '------New ---------------
        DT.DefaultView.RowFilter = "(LAST_LEVEL IS NULL OR LAST_LEVEL=0) AND CURRENT_LEVEL>0"
        lbl_New.InnerHtml = FormatNumber(DT.DefaultView.ToTable(True, AllCol).Copy.Rows.Count, 0)
        '------Trace -------------
        DT.DefaultView.RowFilter = "LAST_LEVEL>0 AND CURRENT_LEVEL IS NOT NULL"
        lbl_TraceTag.InnerHtml = FormatNumber(DT.DefaultView.ToTable(True, AllCol).Copy.Rows.Count, 0)

        DT.Columns.Add("L", GetType(Integer), "IIf(CURRENT_LEVEL IS NULL,LAST_LEVEL,CURRENT_LEVEL)")
        Dim Cur_Level As Object = DT.Compute("MAX(L)", "")
        If Not IsDBNull(Cur_Level) Then
            LEVEL = Cur_Level
        Else
            LEVEL = EIR_BL.InspectionLevel.Normal
        End If

        DialogDetail.CloseDialog()

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        TDTree.Attributes("onclick") = "document.getElementById('" & btnExpand.ClientID & "').click();"

    End Sub

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lbl_Code.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_Item.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_Level.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_New.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_Remain.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
        lbl_TraceTag.Attributes("onClick") = "document.getElementById('" & btnExpand.ClientID & "').click();"
    End Sub

    Protected Sub Expand_Command(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExpand.Click
        IsExpand = Not IsExpand
    End Sub

    Protected Sub rptInspection_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInspection.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim GLI As GL_PipeRoutineInspection = e.Item.FindControl("GLI")

        GLI.RPT_Year = RPT_Year
        GLI.RPT_No = RPT_No
        GLI.TAG_ID = TAG_ID
        GLI.INSP_ID = e.Item.DataItem("INSP_ID")

        If Not IsDBNull(e.Item.DataItem("DETAIL_ID")) AndAlso e.Item.DataItem("DETAIL_ID") > 0 Then
            GLI.DETAIL_ID = e.Item.DataItem("DETAIL_ID")
        Else
            GLI.DETAIL_ID = 0
        End If

        GLI.BindData()

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click

        PIPE.DROP_ROUTINE_Detail(RPT_Year, RPT_No, TAG_ID)
        PIPE.Construct_Routine_Report_Detail(Session("User_ID"), RPT_Year, RPT_No, TAG_ID)

        BindHeader()
        IsExpand = IsExpand
    End Sub

    Protected Sub DialogDetail_UpdateCompleted(ByRef sender As GL_DialogPipeRoutineDetail) Handles DialogDetail.UpdateCompleted
        BindHeader()
        IsExpand = IsExpand
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click
        Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
        DialogDetail.UNIQUE_POPUP_ID = UNIQUEKEY
        DialogDetail.ShowDialog(RPT_Year, RPT_No, TAG_ID)
    End Sub

End Class