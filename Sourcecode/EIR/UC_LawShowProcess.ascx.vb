﻿Public Class UC_LawShowProcess
    Inherits System.Web.UI.UserControl

    Dim CL As New LawClass

    Public Sub SetProcessData(dt As DataTable)
        If dt.Rows.Count > 0 Then
            rptShowProcess.DataSource = dt
            rptShowProcess.DataBind()
        Else
            rptShowProcess.DataSource = Nothing
            rptShowProcess.DataBind()
        End If
    End Sub


    Private Sub rptShowProcess_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptShowProcess.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim imgUrl As String = GetPaperStatusImage(e.Item.DataItem("document_plan_paper_id"))

        Dim lblProcess As Label = e.Item.FindControl("lblProcess")
        Dim txt As New StringBuilder
        txt.AppendLine("<td style=""background-image:url('" & imgUrl & "');"" class=""processBGImage"" >")
        txt.AppendLine("</td>")

        lblProcess.Text = txt.ToString
    End Sub

    Private Function GetPaperStatusImage(DocumentPlanPaperID As Long) As String
        Dim ret As String = ""
        Dim pStatus As DocumentPlanStatusClass = CL.GetDocumentPaperStatus(DocumentPlanPaperID)

        Select Case pStatus.PlanStatus
            Case "WAITING"
                ret = "resources/images/process_waiting.png"
            Case "NOTICE"
                ret = "resources/images/process_notice.png"
            Case "CRITICAL"
                ret = "resources/images/process_critical.png"
            Case "COMPLETE"
                ret = "resources/images/process_complete.png"
        End Select

        Return ret
    End Function
End Class