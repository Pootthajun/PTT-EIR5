﻿Imports System.Data
Imports System.Data.SqlClient
Public Class UC_Dashboard_Top10_Problem
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass
    Dim CV As New Converter

    Public Property Month_From() As Integer
        Get
            Try
                Return lblMONTH_F.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            lblMONTH_F.Text = value
        End Set
    End Property

    Public Property Month_To() As Integer
        Get
            Try
                Return lblMONTH_T.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            lblMONTH_T.Text = value
        End Set
    End Property

    Public Property Year_From() As Integer
        Get
            Try
                Return lblYEAR_F.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            lblYEAR_F.Text = value
        End Set
    End Property

    Public Property Year_To() As Integer
        Get
            Try
                Return lblYEAR_T.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            lblYEAR_T.Text = value
        End Set
    End Property

    Public Property Equipment() As EIR_BL.Report_Type
        Get
            Try
                Return lblEQUIPMENT.Text
            Catch ex As Exception
                Return EIR_BL.Report_Type.All
            End Try
        End Get
        Set(ByVal value As EIR_BL.Report_Type)
            lblEQUIPMENT.Text = value
        End Set
    End Property

    Public Property IsPrintPage() As Boolean
        Get
            Try
                Return CBool(lblPrintPage.Text)
            Catch ex As Exception
                Return False
            End Try

        End Get
        Set(ByVal value As Boolean)
            lblPrintPage.Text = value
        End Set
    End Property


    Public Sub BindData()

        Dim SQL As String = ""

        Dim DA As New SqlDataAdapter

        Dim DateFrom As Date = DateAdd(DateInterval.Year, -543, CV.StringToDate(Year_From.ToString & "-" & Month_From.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd"))
        Dim DateTo As Date = DateAdd(DateInterval.Year, -543, CV.StringToDate(Year_To.ToString & "-" & Month_To.ToString.PadLeft(2, "0") & "-01", "yyyy-MM-dd"))
        DateTo = DateTo.AddMonths(1).AddDays(-1)

        '----------------------- ดึงข้อมูลทั้งหมดตามเงื่อนไข -------------------------
        Dim DT_Dashboard As DataTable = Nothing
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                DT_Dashboard = GetDashboardData(EIR_BL.Report_Type.Stationary_Routine_Report, , , DateFrom, DateTo, DashboardClass.ZeroDate)
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                DT_Dashboard = GetDashboardData(EIR_BL.Report_Type.Rotating_Routine_Report, , , DateFrom, DateTo, DashboardClass.ZeroDate)
            Case EIR_BL.Report_Type.All
                Dim DT_S As DataTable = GetDashboardData(EIR_BL.Report_Type.Stationary_Routine_Report, , , DateFrom, DateTo, DashboardClass.ZeroDate)
                Dim DT_R As DataTable = GetDashboardData(EIR_BL.Report_Type.Rotating_Routine_Report, , , DateFrom, DateTo, DashboardClass.ZeroDate)
                DT_S.Merge(DT_R)
                DT_Dashboard = DT_S.Copy
                DT_S.Dispose()
        End Select
        '----------------------- จัดรูป Table --------------------------------
        DT_Dashboard.DefaultView.RowFilter = "ICLS_ID>0"
        DT_Dashboard = DT_Dashboard.DefaultView.ToTable
        'DT_Dashboard.Columns.Add("Status_Text", GetType(String), "IIF(Status_Name IS NULL OR Status_Name='Other','Unspecific',Status_Name)")
        For i As Integer = 0 To DT_Dashboard.Rows.Count - 1
            If IsDBNull(DT_Dashboard.Rows(i).Item("Status_Name")) OrElse DT_Dashboard.Rows(i).Item("Status_Name") = "" OrElse DT_Dashboard.Rows(i).Item("Status_Name") = "Other" Then
                DT_Dashboard.Rows(i).Item("Status_Name") = "Unspecific"
            End If
            If DT_Dashboard.Rows(i).Item("Status_Name") = "Abnormal" Then '--------------- Rotating ---------------
                If DT_Dashboard.Rows(i).Item("INSPECTION") = "Other" Then
                    DT_Dashboard.Rows(i).Item("Status_Name") = "Unspecific"
                Else
                    DT_Dashboard.Rows(i).Item("Status_Name") = DT_Dashboard.Rows(i).Item("INSPECTION")
                End If

            ElseIf DT_Dashboard.Rows(i).Item("Status_Name") <> "Unspecific" Then
                DT_Dashboard.Rows(i).Item("Status_Name") = DT_Dashboard.Rows(i).Item("INSPECTION") & " " & DT_Dashboard.Rows(i).Item("Status_Name")
            End If
        Next

        '----------------------- จำแนกผลรวมตามปัญหา --------------------------
        Dim Col_Status As String() = {"STATUS_NAME"}
        Dim DT_Problem As DataTable = DT_Dashboard.DefaultView.ToTable(True, Col_Status)
        DT_Problem.Columns.Add("Total", GetType(Long))
        DT_Problem.Columns.Add("No", GetType(Integer))
        For i As Integer = 0 To DT_Problem.Rows.Count - 1
            DT_Problem.Rows(i).Item("Total") = DT_Dashboard.Compute("COUNT(DETAIL_ID)", "STATUS_NAME='" & DT_Problem.Rows(i).Item("STATUS_NAME").ToString.Replace("'", "''") & "'")
        Next

        DT_Dashboard.Dispose()
        DT_Problem.DefaultView.Sort = "Total DESC"
        For i As Integer = 0 To DT_Problem.DefaultView.Count - 1
            DT_Problem.DefaultView(i).Row.Item("No") = i + 1
        Next
        DT_Problem.DefaultView.Sort = "[No] ASC"
        DT_Problem = DT_Problem.DefaultView.ToTable
        DT_Problem.DefaultView.RowFilter = ""

        If DT_Problem.Rows.Count > 10 Then
            Dim OtherProblem As Long = DT_Problem.Compute("SUM(Total)", "[No]>10")
            DT_Problem.DefaultView.RowFilter = "[No]<=10"
            DT_Problem = DT_Problem.DefaultView.ToTable
            Dim DR As DataRow = DT_Problem.NewRow
            DR("No") = DBNull.Value
            DR("STATUS_NAME") = "Other"
            DR("Total") = OtherProblem
            DT_Problem.Rows.Add(DR)
        End If

        DisplayChart(DT_Problem)
        rptData.DataSource = DT_Problem
        rptData.DataBind()


    End Sub

    Protected Sub DisplayChart(ByVal DT As DataTable)
        Dim DisplayText As String = ""
        If Year_From < 2500 Then
            Year_From = Year_From + 543
        End If
        If Year_To < 2500 Then
            Year_To = Year_To + 543
        End If

        If Year_From = Year_To Then
            If Month_From = Month_To Then
                DisplayText = "on  " & CV.ToMonthNameEN(Month_From) & " " & Year_From
            Else
                DisplayText = "between  " & CV.ToMonthNameEN(Month_From) & " - " & CV.ToMonthNameEN(Month_To) & " " & Year_From
            End If
        Else
            DisplayText = "between  " & CV.ToMonthNameEN(Month_From) & " " & Year_From & " - " & CV.ToMonthNameEN(Month_To) & " " & Year_To
        End If

        Dim txt_Equipment As String = ""
        If Equipment = EIR_BL.Report_Type.All Then
            txt_Equipment = "Stationary & Rotating"
        Else
            txt_Equipment = Dashboard.FindEquipmentName(Equipment) & " Equipment "
        End If

        ChartMain.Titles("Title1").Text = "Top 10  problems for " & txt_Equipment & vbCrLf & DisplayText
        ChartMain.Series("Series1").ChartType = DataVisualization.Charting.SeriesChartType.Column
        ChartMain.Series("Series1").Points.Clear()

        If DT.Rows.Count = 0 Then
            ChartMain.Series("Series1").Points.AddXY("No problem found", 0)
            ChartMain.ChartAreas(0).AxisX.LabelStyle.Angle = 0
        Else
            ChartMain.ChartAreas(0).AxisX.LabelStyle.Angle = -90
            For i As Integer = 0 To DT.Rows.Count - 1
                '        Dim Url As String = "Dashboard_Total_Problem_Report_Plant.aspx?PLANT_ID=" & DT.Rows(i).Item("PLANT_ID") & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & Equipment
                ChartMain.Series("Series1").Points.AddXY(DT.Rows(i).Item("Status_Name") & " ", DT.Rows(i).Item("Total"))

                Dim Tooltip As String = DT.Rows(i).Item("Status_Name") & " found : " & DT.Rows(i).Item("Total").ToString & " Problem(s)"
                ChartMain.Series("Series1").Points(i).ToolTip = Tooltip
                'ChartMain.Series("Series1").Points(i).Url = "#"
                '        ChartMain.Series("Series1").Points(i).Url = Url
                '        ChartMain.Series("Series2").Points(i).Url = Url
                '        ChartMain.Series("Series3").Points(i).Url = Url
                If i < 10 Then
                    ChartMain.Series("Series1").Points(i).Color = Drawing.Color.Red
                Else
                    ChartMain.Series("Series1").Points(i).Color = Drawing.Color.Silver
                End If


            Next
        End If


    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblProblem As Label = e.Item.FindControl("lblProblem")
        Dim lblFound As Label = e.Item.FindControl("lblFound")

        lblNo.Text = e.Item.DataItem("No").ToString

        lblProblem.Text = e.Item.DataItem("STATUS_NAME").ToString
        lblFound.Text = FormatNumber(e.Item.DataItem("Total"), 0)

        If e.Item.ItemIndex >= 10 Then
            lblFound.ForeColor = Drawing.Color.Gray
        End If
    End Sub

    Public Function GetDashboardData(ByVal Equipment As EIR_BL.Report_Type, Optional ByVal PLANT_ID As Integer = 0, Optional ByVal TAG_ID As Integer = 0, Optional ByVal Plan_Date_From As DateTime = DashboardClass.ZeroDate, Optional ByVal Plan_Date_To As DateTime = DashboardClass.ZeroDate, Optional ByVal Inspected_Date As DateTime = DashboardClass.ZeroDate, Optional ByVal ISSUE As DashboardClass.REPORT_ISSUE = DashboardClass.REPORT_ISSUE.All, Optional ByVal ICLS_Level As EIR_BL.InspectionLevel = EIR_BL.InspectionLevel.All) As DataTable
        '--------------------- Function มันต้องรับ Parameter ได้ตามข้างบน ไม่งั้นมันก็ดึงมาทั้งฐานข้อมูล และฝั่ง UI ต้องมา WHERE เองทั้งหมด ------------------

        Dim Filter As String = " AND "
        If PLANT_ID > 0 Then
            Filter = Filter & " PLANT_ID = " & PLANT_ID & " AND"
        End If
        If TAG_ID > 0 Then
            Filter = Filter & " TAG_ID = " & TAG_ID & " AND"
        End If
        If Plan_Date_From <> DashboardClass.ZeroDate Then
            Filter = Filter & " CONVERT(VARCHAR(8),RPT_Period_Start,112) >= '" & BL.ReportProgrammingDate(Plan_Date_From) & "' AND"
        End If
        If Plan_Date_To <> DashboardClass.ZeroDate Then
            Filter = Filter & " CONVERT(VARCHAR(8),RPT_Period_Start,112) <= '" & BL.ReportProgrammingDate(Plan_Date_To) & "' AND"
        End If
        If Inspected_Date <> DashboardClass.ZeroDate Then
            Filter = Filter & " CONVERT(VARCHAR(8),Inspected_Date,112) = '" & BL.ReportProgrammingDate(Inspected_Date) & "' AND"
        End If
        Select Case ISSUE
            Case DashboardClass.REPORT_ISSUE.Normal
                Filter = Filter & " ISSUE = " & DashboardClass.REPORT_ISSUE.Normal & " AND"
            Case DashboardClass.REPORT_ISSUE.New_Problem
                Filter = Filter & " ISSUE = " & DashboardClass.REPORT_ISSUE.New_Problem & " AND"
            Case DashboardClass.REPORT_ISSUE.Not_Fixed
                Filter = Filter & " ISSUE = " & DashboardClass.REPORT_ISSUE.Not_Fixed & " AND"
            Case DashboardClass.REPORT_ISSUE.Fixed_Completed
                Filter = Filter & " ISSUE = " & DashboardClass.REPORT_ISSUE.Fixed_Completed & " AND"
            Case DashboardClass.REPORT_ISSUE.Fixed_Incompleted
                Filter = Filter & " ISSUE = " & DashboardClass.REPORT_ISSUE.Fixed_Incompleted & " AND"
        End Select
        Select Case ICLS_Level
            Case EIR_BL.InspectionLevel.Normal
                Filter = Filter & " ICLS_ID = " & EIR_BL.InspectionLevel.Normal & " AND"
            Case EIR_BL.InspectionLevel.ClassC
                Filter = Filter & " ICLS_ID = " & EIR_BL.InspectionLevel.ClassC & " AND"
            Case EIR_BL.InspectionLevel.ClassB
                Filter = Filter & " ICLS_ID = " & EIR_BL.InspectionLevel.ClassB & " AND"
            Case EIR_BL.InspectionLevel.ClassA
                Filter = Filter & " ICLS_ID = " & EIR_BL.InspectionLevel.ClassA & " AND"
        End Select
        If Filter <> " AND " Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        Else
            Filter = ""
        End If


        Dim ST As String = "SELECT 1 AS RPT_TYPE_ID,DETAIL_ID,RPT_Year,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,AREA_ID,AREA_Name," & vbNewLine
        ST &= "INSPECTED_DATE,RPT_Period_Start,INSP_Name INSPECTION,STATUS_NAME,ISSUE,ICLS_ID,ICLS_Description" & vbNewLine
        ST &= "FROM VW_DASHBOARD_ST WHERE 1=1 " & Filter & vbNewLine

        Dim RO As String = "SELECT 3 AS RPT_TYPE_ID,DETAIL_ID,RPT_Year,RPT_No,TAG_ID,TAG_Code,PLANT_ID,PLANT_Name,AREA_ID,AREA_Name," & vbNewLine
        RO &= "INSPECTED_DATE,RPT_Period_Start,INSP_Name INSPECTION,STATUS_NAME,ISSUE,ICLS_ID,ICLS_Description" & vbNewLine
        RO &= "FROM VW_DASHBOARD_RO WHERE 1=1 " & Filter & vbNewLine

        Dim ALL As String = ST & vbNewLine
        ALL &= "UNION ALL" & vbNewLine
        ALL &= RO & vbNewLine

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        Select Case Equipment
            Case EIR_BL.Report_Type.Stationary_Routine_Report
                DA = New SqlDataAdapter(ST, BL.ConnStr)
            Case EIR_BL.Report_Type.Rotating_Routine_Report
                DA = New SqlDataAdapter(RO, BL.ConnStr)
            Case 99
                DA = New SqlDataAdapter(ALL, BL.ConnStr)
        End Select

        DA.Fill(DT)
        Return DT
    End Function

End Class