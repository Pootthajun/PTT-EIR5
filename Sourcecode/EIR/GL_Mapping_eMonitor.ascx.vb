﻿Imports System.Data
Imports System.Data.SqlClient
Public Class GL_Mapping_eMonitor
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL

    Public Event CancelDialog()
    Public Event AnswerDialog()

    Public ReadOnly Property ControlToFocus() As String
        Get
            Return ViewState("ControlToFocus")
        End Get
    End Property

    Public Property PLANT_ID() As Integer
        Get
            If Not IsNumeric(ViewState("PLANT_ID")) Then
                Return 0
            Else
                Return ViewState("PLANT_ID")
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("PLANT_ID") = value
        End Set
    End Property

    Public Property UNIQUEPOPUPID() As String
        Get
            Return ViewState("UNIQUEPOPUPID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUEPOPUPID") = value
        End Set
    End Property

    Public Sub ShowDialog(ByVal POPUPID As String) ' For Case Edit
        UNIQUEPOPUPID = POPUPID
        BindMapping()
        Me.Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        RaiseEvent CancelDialog()
        Me.Visible = False
    End Sub

    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click

        '----------------- Update eMonitor Mapping to EIR Database --------------
        Dim EM As DataTable = MappingList()
        Dim Sql As String = "SELECT * FROM MS_Tag_eMonitor"
        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        Dim EIR As New DataTable
        DA.Fill(EIR)
        Dim CMD As New SqlCommandBuilder(DA)

        For i As Integer = 0 To EM.Rows.Count - 1
            EIR.DefaultView.RowFilter = "TAG_ID=" & EM.Rows(i).Item("TAG_ID")
            If EIR.DefaultView.Count > 0 Then
                If EM.Rows(i).Item("eMonitor_Code") <> "" Then
                    EIR.DefaultView(0).Item("eMonitor_Code") = EM.Rows(i).Item("eMonitor_Code")
                Else
                    EIR.DefaultView(0).Row.Delete()
                End If
                Try
                    DA.Update(EIR)
                Catch ex As Exception
                End Try
                EIR.AcceptChanges()
            ElseIf EM.Rows(i).Item("eMonitor_Code") <> "" Then
                Dim DR As DataRow = EIR.NewRow
                DR("TAG_ID") = EM.Rows(i).Item("TAG_ID")
                DR("eMonitor_Code") = EM.Rows(i).Item("eMonitor_Code")
                EIR.Rows.Add(DR)
                Try
                    DA.Update(EIR)
                Catch ex As Exception
                End Try
                EIR.AcceptChanges()
            End If
        Next


        EM.DefaultView.RowFilter = "eMonitor_Code<>'' AND eMonitor_Code IS NOT NULL"
        Dim Col() As String = {"eMonitor_Code"}
        EM = EM.DefaultView.ToTable(True, Col)

        '------------------- Get eMonitor Data ------------------
        Dim TAG() As String = {}
        For i As Integer = 0 To EM.Rows.Count - 1
            BL.PushString(TAG, EM.Rows(i).Item("eMonitor_Code"))
        Next

        Dim eMonitor As New eMonitor
        Dim DT As DataTable = eMonitor.Get_eMonitor_Data(BL.Get_eMonitor_Plant(PLANT_ID), TAG)
        If IsNothing(DT) OrElse DT.Rows.Count = 0 Then
            Session("Mapping_eMonitor_" & UNIQUEPOPUPID) = New DataTable
            RaiseEvent AnswerDialog()
            Me.Visible = False
        End If

        Dim Filter As String = " "
        Filter &= " COLLECTION IS NOT NULL " & vbLf
        Filter &= " AND ((TIME_STAMP_MAGNITUDE IS NOT NULL AND AMPLITUDE_MAX_MAGNITUDE IS NOT NULL) OR (TIME_STAMP_SPECTRUM IS NOT NULL AND AMPLITUDE_MAX_SPECTRUM IS NOT NULL)) " & vbLf
        Filter &= " AND POS IS NOT NULL AND POS<>''" & vbLf

        '---------------- Remove Unused Records -----------
        DT.DefaultView.RowFilter = Filter
        DT = DT.DefaultView.ToTable

        DT.Columns.Remove("HIERARCHY_ID")
        DT.Columns.Remove("STATION")
        DT.Columns.Remove("DIRECTION")
        DT.Columns.Remove("DESCRIPTION")
        '------------- Remain --------------
        'TAG
        'LOCATION
        'POS
        'CATEGORY
        'COLLECTION
        '-------------- Value --------------
        'TIME_STAMP_MAGNITUDE
        'AMPLITUDE_MAX_MAGNITUDE
        'TIME_STAMP_SPECTRUM
        'AMPLITUDE_MAX_SPECTRUM

        '----------------- Remain Analyse ----------

        'Construct result table
        Dim Result As New DataTable
        Result.Columns.Add("TAG_ID")
        Result.Columns.Add("Vibration_Sererity")
        Result.Columns.Add("Location") '-----------
        Result.Columns.Add("Position")
        Result.Columns.Add("Category")
        Result.Columns.Add("Data_type") '-----------
        Result.Columns.Add("Overall_Value")
        Result.Columns.Add("Unit")
        Result.Columns.Add("Percent_Change")
        Result.Columns.Add("Analysis_Comment")
        Result.Columns.Add("PROB_Recomment")
        Result.Columns.Add("Responsible")
        Result.Columns.Add("Upload_By")
        Result.Columns.Add("Upload_Time")

        '--------------- Calculate Display Value ----------------
        DT.Columns.Add("DISPLAY_VALUE")
        DT.Columns.Add("UNIT")
        DT.Columns.Add("DATA_TYPE")
        For i As Integer = 0 To DT.Rows.Count - 1

            Dim COLLECTION As String = DT.Rows(i).Item("COLLECTION").ToString.ToUpper.Replace(" ", "")
            Dim Value As Object

            If Not IsDBNull(DT.Rows(i).Item("AMPLITUDE_MAX_MAGNITUDE")) And Not IsDBNull(DT.Rows(i).Item("TIME_STAMP_MAGNITUDE")) Then
                Value = DT.Rows(i).Item("AMPLITUDE_MAX_MAGNITUDE")
                DT.Rows(i).Item("DATA_TYPE") = "MAGNITUDE"
            Else
                Value = DT.Rows(i).Item("AMPLITUDE_MAX_SPECTRUM")
                DT.Rows(i).Item("DATA_TYPE") = "SPECTRUM"
            End If

            If COLLECTION.IndexOf("-PK-PK") > -1 Then
                DT.Rows(i).Item("DISPLAY_VALUE") = Value * 2.828
                DT.Rows(i).Item("UNIT") = "g's (Pk-Pk)"
            ElseIf COLLECTION.IndexOf("-PK") > -1 Then
                DT.Rows(i).Item("DISPLAY_VALUE") = Value * 1.414
                DT.Rows(i).Item("UNIT") = "g's (Pk)"
            ElseIf COLLECTION.IndexOf("-RMS") > -1 Then
                DT.Rows(i).Item("DISPLAY_VALUE") = Value
                DT.Rows(i).Item("UNIT") = "mm/sec"
            Else
                DT.Rows(i).Item("DISPLAY_VALUE") = ""
                DT.Rows(i).Item("UNIT") = ""
            End If
        Next

        DT.DefaultView.RowFilter = "Display_Value<>'' AND Display_Value IS NOT NULL"
        DT.DefaultView.Sort = "TAG ASC,POS ASC,UNIT ASC,DATA_TYPE ASC"
        Dim eCol() As String = {"TAG", "LOCATION", "POS", "CATEGORY", "DATA_TYPE", "COLLECTION"}
        Dim TMP As DataTable = DT.DefaultView.ToTable(True, eCol).Copy


        '--------------- กลายเป็นว่า ตรวจในแต่ละ TAG,POS ถ้าไม่มี Magnitude เอา Spectrum มาคิด ------------------------

        For i As Integer = 0 To TMP.Rows.Count - 1  '------------- Each TAG,LOCATION,POS ------------

            Filter = "TAG='" & TMP.Rows(i).Item("TAG").ToString.Replace("'", "''") & "' "
            Filter &= " AND LOCATION='" & TMP.Rows(i).Item("LOCATION").ToString.Replace("'", "''") & "' "
            Filter &= " AND POS='" & TMP.Rows(i).Item("POS").ToString.Replace("'", "''") & "' "
            If TMP.Rows(i).Item("CATEGORY").ToString.Replace("'", "''") = "" Then
                Filter &= " AND (CATEGORY ='' OR CATEGORY IS NULL) "
            Else
                Filter &= " AND CATEGORY ='" & TMP.Rows(i).Item("CATEGORY").ToString.Replace("'", "''") & "' "
            End If
            Filter &= " AND DATA_TYPE='" & TMP.Rows(i).Item("DATA_TYPE").ToString.Replace("'", "''") & "' "
            Filter &= " AND COLLECTION='" & TMP.Rows(i).Item("COLLECTION").ToString.Replace("'", "''") & "' "

            DT.DefaultView.RowFilter = Filter
            If DT.DefaultView.Count = 0 Then Continue For
            EIR.DefaultView.RowFilter = "eMonitor_Code='" & TMP.Rows(i).Item("TAG").ToString.Replace("'", "''") & "'"
            If EIR.DefaultView.Count = 0 Then Continue For

            Dim DR As DataRow = Result.NewRow
            DR("TAG_ID") = EIR.DefaultView(0).Item("TAG_ID")
            DR("Location") = DT.DefaultView(0).Item("LOCATION")
            DR("Position") = DT.DefaultView(0).Item("POS")

            DR("Unit") = DT.DefaultView(0).Item("UNIT")
            DR("Category") = DT.DefaultView(0).Item("Category")
            DR("Data_Type") = DT.DefaultView(0).Item("Data_Type")

            If DR("Data_Type") = "MAGNITUDE" Then
                DT.DefaultView.Sort = "TIME_STAMP_MAGNITUDE DESC"
            Else
                DT.DefaultView.Sort = "TIME_STAMP_SPECTRUM DESC"
            End If

            '----------------------Calculate Percent Change --------------
            DR("Overall_Value") = DT.DefaultView(0).Item("Display_Value")
            If DT.DefaultView.Count > 1 Then
                Dim CurM As Double = DT.DefaultView(0).Item("Display_Value")
                Dim PreM As Double = DT.DefaultView(1).Item("Display_Value")
                DR("Percent_Change") = (CurM - PreM) * 100 / PreM
            Else
                DR("Percent_Change") = ""
            End If
            '---------------DR("Analysis_Comment")
            '---------------DR("PROB_Recomment")
            '---------------DR("Responsible")

            '--------------------- Check Sererity  -------------------
            Try
                DR("Vibration_Sererity") = BL.Get_Vibration_Zone(DR("UNIT"), DR("Position"), DR("CATEGORY"), DR("Overall_Value"))
            Catch ex As Exception
                DR("Vibration_Sererity") = ""
            End Try

            'DR("File_Extension")
            DR("Upload_By") = Session("USER_ID")
            DR("Upload_Time") = Now
            Result.Rows.Add(DR)
        Next

        '----------------- Filter Out -------------------
        Filter = ""
        If Not chk_Pk.Checked And Not chk_PkPk.Checked And Not chk_RMS.Checked And Not chk_Other.Checked Then
            Filter &= "1=0 AND "
        Else
            Dim lst As String = ""
            If chk_Pk.Checked Then
                lst &= "UNIT = 'g''s (Pk)' OR "
            End If
            If chk_PkPk.Checked Then
                lst &= "UNIT = 'g''s (Pk-Pk)' OR "
            End If
            If chk_RMS.Checked Then
                lst &= "UNIT = 'mm/sec' OR "
            End If
            If chk_Other.Checked Then
                lst &= " UNIT = '' OR UNIT IS NULL OR "
            End If
            Filter &= "(" & lst.Substring(0, lst.Length - 3) & ") AND "
        End If
        If Not chk_Magnitude.Checked And Not chk_Spectrum.Checked Then
            Filter &= "1=0 AND "
        Else
            Dim lst As String = ""
            If chk_Magnitude.Checked Then
                lst &= "Data_Type = 'MAGNITUDE' OR "
            End If
            If chk_Spectrum.Checked Then
                lst &= "Data_Type = 'SPECTRUM' OR "
            End If
            Filter &= "(" & lst.Substring(0, lst.Length - 3) & ") AND "
        End If
        If ddlZone.SelectedIndex > 0 Then
            Filter &= "Vibration_Sererity>='" & ddlZone.Text & "' AND "
        End If
        Filter = Filter.Substring(0, Filter.Length - 4)
        Result.DefaultView.RowFilter = Filter
        Result = Result.DefaultView.ToTable.Copy

        Session("Mapping_eMonitor_" & UNIQUEPOPUPID) = Result
        RaiseEvent AnswerDialog()
        Me.Visible = False

    End Sub

    Private Sub BindMapping()
        Dim EIR As DataTable = CType(Session("Mapping_eMonitor_" & UNIQUEPOPUPID), DataTable).Copy
        Dim SQL As String = "SELECT eM.TAG_ID ,TAG_CODE,ISNULL(eMonitor_Code,'') eMonitor_Code" & vbLf
        SQL &= " FROM VW_ALL_ACTIVE_RO_TAG VW INNER JOIN MS_Tag_eMonitor eM" & vbLf
        SQL &= " ON VW.TAG_ID=eM.TAG_ID" & vbLf

        Mapper = New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(Mapper)

        EIR.DefaultView.Sort = "TAG_CODE ASC"
        EIR = EIR.DefaultView.ToTable
        rptMapping.DataSource = EIR
        rptMapping.DataBind()
    End Sub

    Dim Mapper As New DataTable

    Protected Sub rptMapping_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMapping.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblEIR As Label = e.Item.FindControl("lblEIR")
        Dim txtMapping As TextBox = e.Item.FindControl("txtMapping")

        lblEIR.Text = e.Item.DataItem("TAG_CODE")
        txtMapping.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

        Mapper.DefaultView.RowFilter = "TAG_ID=" & e.Item.DataItem("TAG_ID") & " AND eMonitor_Code<>''"
        If Mapper.DefaultView.Count > 0 Then
            txtMapping.Text = Mapper.DefaultView(0).Item("eMonitor_Code")
        End If

        If e.Item.ItemIndex = 0 Then
            ViewState("ControlToFocus") = txtMapping.ClientID
        End If

    End Sub

    Public Function MappingList() As DataTable
        Dim DT As New DataTable

        DT.Columns.Add("TAG_ID")
        DT.Columns.Add("TAG_CODE")
        DT.Columns.Add("eMonitor_Code")
        For Each Item As RepeaterItem In rptMapping.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblEIR As Label = Item.FindControl("lblEIR")
            Dim txtMapping As TextBox = Item.FindControl("txtMapping")
            Dim DR As DataRow = DT.NewRow
            DR("TAG_ID") = txtMapping.Attributes("TAG_ID")
            DR("TAG_CODE") = lblEIR.Text
            DR("eMonitor_Code") = txtMapping.Text
            DT.Rows.Add(DR)
        Next
        Return DT

    End Function

End Class