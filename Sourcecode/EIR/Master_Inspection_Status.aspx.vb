﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Master_Inspection_Status
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetPlant(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindPlant()

        Dim SQL As String = " SELECT * FROM MS_ST_Default_Inspection_Status Order By STATUS_Order" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("MS_Inspection_Status") = DT

        Navigation.SesssionSourceName = "MS_Inspection_Status"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptPlant
    End Sub

    Protected Sub rptPlant_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPlant.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblInspectionName As Label = e.Item.FindControl("lblInspectionName")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblInspectionName.Text = e.Item.DataItem("STATUS_Name")

        btnEdit.Attributes("STATUS_ID") = e.Item.DataItem("STATUS_ID")

    End Sub

    Protected Sub rptPlant_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptPlant.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblInspectionName As Label = e.Item.FindControl("lblInspectionName")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")

        Dim STATUS_ID As Integer = btnEdit.Attributes("STATUS_ID")
        Select Case e.CommandName
            Case "Edit"
                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                'txtPlantCode.ReadOnly = True
                txtConditionName.Focus()
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListPlant.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT * FROM MS_ST_Default_Inspection_Status WHERE STATUS_ID=" & STATUS_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Inspection Not Found"
                    pnlBindingError.Visible = True
                    BindPlant()
                    Exit Sub
                End If

                txtConditionName.Text = DT.Rows(0).Item("STATUS_Name")
                txtConditionName.Attributes("STATUS_ID") = DT.Rows(0).Item("STATUS_ID")
                lblValidation.Text = ""

                btnSave.Focus()
        End Select


    End Sub

    Protected Sub ResetPlant(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindPlant()
        '-----------------------------------
        ClearPanelEdit()
        '-----------------------------------
        pnlListPlant.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtConditionName.Text = ""

        btnCreate.Visible = True
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True

        btnCreate.Visible = False

        txtConditionName.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListPlant.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtConditionName.Text = "" Then
            lblValidation.Text = "Please insert Condition Name"
            pnlValidation.Visible = True
            Exit Sub
        End If


        Dim STATUS_ID As Integer = txtConditionName.Attributes("STATUS_ID")

        Dim SQL As String = "SELECT * FROM MS_ST_Default_Inspection_Status WHERE STATUS_Name='" & txtConditionName.Text.Replace("'", "''") & "'" & " AND STATUS_ID<>" & STATUS_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Condition Code is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_ST_Default_Inspection_Status WHERE STATUS_ID=" & STATUS_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            STATUS_ID = GetNewInspectionStatusID()
            DR("STATUS_ID") = STATUS_ID
        Else
            DR = DT.Rows(0)
        End If
        DR("STATUS_Name") = txtConditionName.Text
        DR("STATUS_Order") = STATUS_ID

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetPlant(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetNewInspectionStatusID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(STATUS_ID),0)+1 FROM MS_ST_Default_Inspection_Status "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

End Class