﻿Imports System.Data.SqlClient

Public Class PIPE_ERO_Edit3
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim PIPE As New EIR_PIPE
    Dim C As New Converter
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Pipe_ERO_Reports
    Dim RPT_Step As EIR_PIPE.Report_Step = EIR_PIPE.Report_Step.Thickness_Measurement

#Region "Page Property"
    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property

    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private Property RPT_Date As Object
        Get
            Try
                Return C.StringToDate(txt_Rpt_Date.Text, txt_Rpt_Date_Extender.Format)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(ByVal value As Object)
            If IsNothing(value) Then
                txt_Rpt_Date.Text = ""
            Else
                txt_Rpt_Date.Text = C.DateToString(value, txt_Rpt_Date_Extender.Format)
            End If
        End Set
    End Property

    Private Property Last_RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("Last_RPT_Year")) Then
                Return ViewState("Last_RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("Last_RPT_Year") = value
        End Set
    End Property

    Private Property Last_RPT_No() As Integer
        Get
            If IsNumeric(ViewState("Last_RPT_No")) Then
                Return ViewState("Last_RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("Last_RPT_No") = value
        End Set
    End Property

    Private Property Last_RPT_Date() As Object
        Get
            If IsNumeric(ViewState("Last_RPT_Date")) Then
                Return ViewState("Last_RPT_Date")
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            ViewState("Last_RPT_Date") = value
        End Set
    End Property

    Private Property Last_Measure_Year() As Object
        Get
            If Not IsNumeric(lbl_s_LastYear.Text) Then
                Return Nothing
            Else
                Return CInt(lbl_s_LastYear.Text)
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_s_LastYear.Text = ""
            Else
                lbl_s_LastYear.Text = CInt(value)
            End If
        End Set
    End Property

    Private Property Last_Thickness() As Object
        Get
            If IsNumeric(lbl_s_LastThick.Text) Then
                Return CDbl(lbl_s_LastThick.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_s_LastThick.Text = ""
            Else
                lbl_s_LastYear.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property

    Private Property Initial_Year As Object
        Get
            If IsNumeric(lbl_l_LastYear.Text) Then
                Return CDbl(lbl_l_LastYear.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(value As Object)
            If Not IsNumeric(value) Then
                lbl_l_LastYear.Text = ""
            Else
                lbl_l_LastYear.Text = CInt(value)
            End If
            If Not Equals(Point_Info.INITIAL_YEAR, value) Then
                Point_Info.INITIAL_YEAR = value
                Point_Info_PropertyChangedByUser(Point_Info, UC_PIPE_POINT.PropertyItem.INITIAL_YEAR)
            End If
        End Set
    End Property

    Private Property Minimum_Actual_Thickness As Object
        Get
            If IsNumeric(lbl_s_Thick.Text) Then
                Return CDbl(lbl_s_Thick.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_s_Thick.Text = ""
                lbl_l_Thick.Text = ""
            Else
                lbl_s_Thick.Text = FormatNumericTextLimitPlace(value, True, 2)
                lbl_l_Thick.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property

    Private Property Diff_Year_Short As Object
        Get
            If IsNumeric(lbl_s_Year.Text) Then
                Return CInt(lbl_s_Year.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_s_Year.Text = ""
            Else
                lbl_s_Year.Text = CInt(value)
            End If
        End Set
    End Property

    Private Property Diff_Year_Long As Object
        Get
            If IsNumeric(lbl_l_Year.Text) Then
                Return CInt(lbl_l_Year.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_l_Year.Text = ""
            Else
                lbl_l_Year.Text = CInt(value)
            End If
        End Set
    End Property

    Private Property Required_Thickness_Mode() As Integer
        Get
            If ddl_Treq.SelectedIndex > -1 And ddl_Treq.SelectedIndex < 3 Then
                Return ddl_Treq.SelectedIndex + 1
            Else
                ddl_Treq.SelectedIndex = 0
                Return 0
            End If
        End Get
        Set(value As Integer)
            Select Case value
                Case 2
                    ddl_Treq.SelectedIndex = 1
                Case 3
                    ddl_Treq.SelectedIndex = 2
                Case 1
                    ddl_Treq.SelectedIndex = 0
            End Select
        End Set
    End Property

    Private Property Use_Term As Integer
        Get
            If rdoLong.Checked Then
                Return 2
            Else
                rdoShort.Checked = True
                Return 1
            End If
        End Get
        Set(value As Integer)
            Select Case value
                Case 2
                    rdoLong.Checked = True
                Case 1
                    rdoShort.Checked = True
            End Select
        End Set
    End Property

    Private Property Norminal_Thickness As Object
        Get
            Return Point_Info.NORMINAL_THICKNESS
        End Get
        Set(value As Object)
            If Not IsNumeric(value) Then
                lbl_l_LastThick.Text = ""
            Else
                lbl_l_LastThick.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
            If Not Equals(Point_Info.NORMINAL_THICKNESS, value) Then
                Point_Info.NORMINAL_THICKNESS = value
                Point_Info_PropertyChangedByUser(Point_Info, UC_PIPE_POINT.PropertyItem.NORMINAL_THICKNESS)
            End If
        End Set
    End Property

    Private Property CA_ID As Integer
        Get
            Return Point_Info.CA_ID
        End Get
        Set(value As Integer)
            If Point_Info.CA_ID <> value Then
                Point_Info.CA_ID = value
                Point_Info_PropertyChangedByUser(Point_Info, UC_PIPE_POINT.PropertyItem.CORROSION_ALLOWANCE)
            End If
        End Set
    End Property

    Private ReadOnly Property Corrosion_Allowance As Double
        Get
            Return PIPE.Get_Corrosion_Depth_mm(CA_ID)
        End Get
    End Property

    Private Property Calculated_Thickness() As Object
        Get
            Return Point_Info.CALCULATED_THICKNESS
        End Get
        Set(value As Object)
            If Not Equals(Point_Info.CALCULATED_THICKNESS, value) Then
                Point_Info.CALCULATED_THICKNESS = value
                Point_Info_PropertyChangedByUser(Point_Info, UC_PIPE_POINT.PropertyItem.CALCULATED_THICKNESS)
            End If
        End Set
    End Property

    Private Property Corrosion_rate_Short As Object
        Get
            If IsNumeric(lbl_s_Rate.Text) Then
                Return CDbl(lbl_s_Rate.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_s_Rate.Text = ""
            Else
                lbl_s_Rate.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property

    Private Property Corrosion_rate_Long As Object
        Get
            If IsNumeric(lbl_l_Rate.Text) Then
                Return CDbl(lbl_l_Rate.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_l_Rate.Text = ""
            Else
                lbl_l_Rate.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property

    Private Property Required_Thickness As Object
        Get
            If IsNumeric(lbl_S_ReqThick.Text) Then
                Return CDbl(lbl_S_ReqThick.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_S_ReqThick.Text = ""
                lbl_l_ReqThick.Text = ""
            Else
                lbl_S_ReqThick.Text = FormatNumericTextLimitPlace(value, True, 2)
                lbl_l_ReqThick.Text = FormatNumericTextLimitPlace(value, True, 2)
            End If
        End Set
    End Property

    Private Property Remain_Life_Short As Object
        Get
            If IsNumeric(lbl_s_Remain.Text) Then
                Return CDbl(lbl_s_Remain.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_s_Remain.Text = ""
            Else
                lbl_s_Remain.Text = FormatNumber(value, 0)
            End If
        End Set
    End Property

    Private Property Remain_Life_Long As Object
        Get
            If IsNumeric(lbl_l_Remain.Text) Then
                Return CDbl(lbl_l_Remain.Text)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Object)
            If Not IsNumeric(value) Then
                lbl_l_Remain.Text = ""
            Else
                lbl_l_Remain.Text = FormatNumber(value, 0)
            End If
        End Set
    End Property

    Private Property PAGE_UNIQUE_ID() As String
        Get
            If IsNothing(ViewState("PAGE_UNIQUE_ID")) OrElse ViewState("PAGE_UNIQUE_ID") = "" Then
                ViewState("PAGE_UNIQUE_ID") = Now.ToOADate.ToString.Replace(".", "")
            End If
            Return ViewState("PAGE_UNIQUE_ID")
        End Get
        Set(ByVal value As String)
            ViewState("PAGE_UNIQUE_ID") = value
        End Set
    End Property

    Private Property TAG_CODE As String
        Get
            Return lbl_Tag.Text
        End Get
        Set(ByVal value As String)
            lbl_Tag.Text = value
        End Set
    End Property

    Private Property TAG_ID As Integer
        Get
            Try
                Return ViewState("TAG_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private Property POINT_ID As Integer
        Get
            Try
                Return ViewState("POINT_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            ViewState("POINT_ID") = value
        End Set
    End Property

    Private Property HeaderProgress As EIR_PIPE.Report_Step
        Get
            Try
                Return ViewState("HeaderProgress")
            Catch ex As Exception
                Return EIR_PIPE.Report_Step.NA
            End Try
        End Get
        Set(ByVal value As EIR_PIPE.Report_Step)
            ViewState("HeaderProgress") = value
        End Set
    End Property

    Private Property INSM_ID As Integer
        Get
            Try
                Return ddlInstrument.Items(ddlInstrument.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            PIPE.BindDDl_TM_Instrument(ddlInstrument, value)
            ddlInstrument_SelectedIndexChanged(ddlInstrument, Nothing)
        End Set
    End Property

    Private Property IsFinished As Boolean
        Get
            Try
                Return ViewState("IsFinished")
            Catch ex As Exception
                Return False
            End Try
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsFinished") = value
        End Set
    End Property

    Private Property ImageFile1 As FileAttachment
        Get
            Try
                Return Session("PIPE_ERO_Edit3_1_" & PAGE_UNIQUE_ID)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As FileAttachment)
            Session("PIPE_ERO_Edit3_1_" & PAGE_UNIQUE_ID) = value
            If Not IsNothing(value) Then
                img_File1.BackImageUrl = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=PIPE_ERO_Edit3_1_" & PAGE_UNIQUE_ID & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
                ZoomMask1.Attributes("onclick") = "window.open('" & img_File1.BackImageUrl & "');"
                ZoomMask1.Visible = True
                btnDelete1.Visible = True
            Else
                img_File1.BackImageUrl = "Resources/Images/Sample_40.png"
                ZoomMask1.Visible = False
                btnDelete1.Visible = False
            End If
        End Set
    End Property

    Private Property ImageFile2 As FileAttachment
        Get
            Try
                Return Session("PIPE_ERO_Edit3_2_" & PAGE_UNIQUE_ID)
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As FileAttachment)
            Session("PIPE_ERO_Edit3_2_" & PAGE_UNIQUE_ID) = value
            If Not IsNothing(value) Then
                img_File2.BackImageUrl = "SVGEditor/SVGAPI.aspx?UNIQUE_POPUP_ID=PIPE_ERO_Edit3_2_" & PAGE_UNIQUE_ID & "&Mode=RENDER_TO_IMAGE&t=" & Now.ToOADate.ToString.Replace(".", "")
                ZoomMask2.Attributes("onclick") = "window.open('" & img_File2.BackImageUrl & "');"
                ZoomMask2.Visible = True
                btnDelete2.Visible = True
            Else
                img_File2.BackImageUrl = "Resources/Images/Sample_40.png"
                ZoomMask2.Visible = False
                btnDelete2.Visible = False
            End If
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Private Property MeasurementDatasource As DataTable
        Get
            Dim DT As DataTable = PIPE.Get_Empty_Measurement_DataTable
            For Each Item As RepeaterItem In rptMeasure.Items
                If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim DR As DataRow = DT.NewRow
                Dim lblPoint As Label = Item.FindControl("lblPoint")
                DR("Point") = lblPoint.Text
                For i As Integer = 1 To 9
                    Dim txt As TextBox = Item.FindControl("txt" & i)
                    If IsNumeric(txt.Text) Then DR("Sub" & i) = Val(txt.Text.Replace(",", ""))
                Next
                DT.Rows.Add(DR)
            Next
            Return DT
        End Get
        Set(ByVal value As DataTable)
            rptMeasure.DataSource = value
            rptMeasure.DataBind() '------------Also Calculate In rptMeasurement Databound:Footer------------
        End Set
    End Property

    Private Sub btnClearURM_Click(sender As Object, e As EventArgs) Handles btnClearURM.Click
        Dim MT As DataTable = MeasurementDatasource
        '-------- Clear All Data----------
        For i As Integer = 0 To MT.Rows.Count - 1
            For j As Integer = 1 To MT.Columns.Count - 1
                MT.Rows(i).Item(j) = DBNull.Value
            Next
        Next

        rptMeasure.DataSource = MT
        rptMeasure.DataBind()
    End Sub
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()

        If Not IsPostBack Then
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("PIPE_ERO_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("Select * FROM RPT_PIPE_ERO_Header WHERE RPT_Year=" & RPT_Year & " And RPT_No=" & RPT_No, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='PIPE_ERO_Summary.aspx'", True)
                    Exit Sub
                End If
            End If
            '--------------UPDATE RESPONSIBLE PERSON (Do Nothing)------------
            ddl_Treq.SelectedIndex = 0
            rdoShort.Checked = True
            rdoLong.Checked = False
            Try
                SetUserAuthorization()
                BindTabData()
                BindTabNavigation()
                ImplementJavascriptControl()
            Catch ex As Exception
                lblBindingError.Text = "Invalid parameter"
                pnlBindingError.Visible = True
            End Try
        End If

        StoreJS()
    End Sub

    Private Sub StoreJS()
        Dim Script As String = "        window.onresize = resize_PIPE_Report_Window;
                                        $(document).ready(resize_PIPE_Report_Window);"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "StoreJS", Script, True)
    End Sub

    Private Sub ImplementJavascriptControl()
        btnPreUploadExcel.Attributes("onClick") = "$('#" & fulExcel.ClientID & "').click();"
        fulExcel.Attributes("onChange") = "$('#" & btnUploadExcel.ClientID & "').click();"

        ImplementJavaNumericText(txtStep1, 2, "center")
        ImplementJavaNumericText(txtStep2, 2, "center")
        ImplementJavaNumericText(txtStep3, 2, "center")
        ImplementJavaNumericText(txtStep4, 2, "center")
        ImplementJavaNumericText(txtMeasure1, 2, "center")
        ImplementJavaNumericText(txtMeasure2, 2, "center")
        ImplementJavaNumericText(txtMeasure3, 2, "center")
        ImplementJavaNumericText(txtMeasure4, 2, "center")
        ImplementJavaNumericText(txtVelocity, 2, "center")

        txtManualDate.Attributes("ReadOnly") = "true"

    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Private Sub btnValidationClose_Click(sender As Object, e As ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

#End Region

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_PIPE_ERO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='PIPE_ERO_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            Dim PM As New Report_CUI_ERO_Permission
            With PM

                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                .IsFinished = Not IsDBNull(DT.Rows(0).Item("Finished")) AndAlso DT.Rows(0).Item("Finished")

                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    Msg &= "-Report is posted completely\n" & vbNewLine
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='PIPE_ERO_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

            '---------- Collect Information ------------
            HeaderProgress = DT.Rows(0).Item("RPT_STEP")
            IsFinished = DT.Rows(0).Item("Finished")

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='PIPE_ERO_Summary.aspx'", True)
            Exit Sub
        End If
    End Sub

    Public Sub BindTabData()
        If HeaderProgress < RPT_Step Or Not PIPE.Is_Existing_Step(RPT_Year, RPT_No, RPT_Step) Then
            Response.Redirect("PIPE_ERO_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&")
            Response.End()
            Exit Sub
        End If

        Dim SQL As String = "SELECT * FROM VW_PIPE_ERO_TM_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("PIPE_ERO_Summary.aspx")
            Exit Sub
        End If

        lblReportCode.Text = DT.Rows(0).Item("RPT_Code")
        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        TAG_ID = DT.Rows(0).Item("TAG_ID")
        POINT_ID = DT.Rows(0).Item("POINT_ID")
        TAG_CODE = DT.Rows(0).Item("TAG_CODE")
        POINT_ID = DT.Rows(0).Item("POINT_ID")
        lbl_Point.Text = DT.Rows(0).Item("POINT_Name")

        If Not IsDBNull(DT.Rows(0).Item("RPT_Date")) Then
            RPT_Date = DT.Rows(0).Item("RPT_Date")
        Else
            RPT_Date = Nothing
        End If
        UpdatePreviousReportDetail(Nothing, Nothing) '----------- Get Last Measurement Detail

        '----------- Pipe Info----------
        With Pipe_Info
            .BindData(TAG_ID)
            .DisplayMainPointProperty = False
            .SetPropertyEditable(UC_PIPE_TAG.PropertyItem.All, False)
        End With
        '----------- Point Info----------
        With Point_Info
            .BindData(TAG_ID, POINT_ID)
            .RaisePropertyChangedByUser = True
            .SetPropertyEditable(UC_PIPE_POINT.PropertyItem.All, True)
            '------------- Set Required Property For Calculation ------------
            'Initial_Thickness = .NORMINAL_THICKNESS
            Initial_Year = .INITIAL_YEAR
            Norminal_Thickness = .NORMINAL_THICKNESS
            Calculated_Thickness = .CALCULATED_THICKNESS
            CA_ID = .CA_ID
        End With
        '---------- TM Instrument/Location---------
        If Not IsDBNull(DT.Rows(0).Item("INSM_ID")) Then
            INSM_ID = DT.Rows(0).Item("INSM_ID")
        Else
            INSM_ID = 0
        End If

        '-------Step Weight Measurement--------
        If IsNumeric(DT.Rows(0).Item("StepWeight_Thickness1")) Then
            txtStep1.Text = FormatNumericTextLimitPlace(DT.Rows(0).Item("StepWeight_Thickness1"), True, 2)
        Else
            txtStep1.Text = ""
        End If
        If IsNumeric(DT.Rows(0).Item("StepWeight_Thickness2")) Then
            txtStep2.Text = FormatNumericTextLimitPlace(DT.Rows(0).Item("StepWeight_Thickness2"), True, 2)
        Else
            txtStep2.Text = ""
        End If
        If IsNumeric(DT.Rows(0).Item("StepWeight_Thickness3")) Then
            txtStep3.Text = FormatNumericTextLimitPlace(DT.Rows(0).Item("StepWeight_Thickness3"), True, 2)
        Else
            txtStep3.Text = ""
        End If
        If IsNumeric(DT.Rows(0).Item("StepWeight_Thickness4")) Then
            txtStep4.Text = FormatNumericTextLimitPlace(DT.Rows(0).Item("StepWeight_Thickness4"), True, 2)
        Else
            txtStep4.Text = ""
        End If

        If IsNumeric(DT.Rows(0).Item("Measurement1")) Then
            txtMeasure1.Text = FormatNumericTextLimitPlace(DT.Rows(0).Item("Measurement1"), True, 2)
        Else
            txtMeasure1.Text = ""
        End If
        If IsNumeric(DT.Rows(0).Item("Measurement2")) Then
            txtMeasure2.Text = FormatNumericTextLimitPlace(DT.Rows(0).Item("Measurement2"), True, 2)
        Else
            txtMeasure2.Text = ""
        End If
        If IsNumeric(DT.Rows(0).Item("Measurement3")) Then
            txtMeasure3.Text = FormatNumericTextLimitPlace(DT.Rows(0).Item("Measurement3"), True, 2)
        Else
            txtMeasure3.Text = ""
        End If
        If IsNumeric(DT.Rows(0).Item("Measurement4")) Then
            txtMeasure4.Text = FormatNumericTextLimitPlace(DT.Rows(0).Item("Measurement4"), True, 2)
        Else
            txtMeasure4.Text = ""
        End If

        If IsNumeric(DT.Rows(0).Item("Velocity")) Then
            txtVelocity.Text = FormatNumericTextLimitPlace(DT.Rows(0).Item("Velocity"), True, 2)
        Else
            txtVelocity.Text = ""
        End If

        '----------- Photography -----------
        ImageFile1 = PIPE.Get_FileAttachment_By_ID(DT.Rows(0).Item("IMG1").ToString)
        ImageFile2 = PIPE.Get_FileAttachment_By_ID(DT.Rows(0).Item("IMG2").ToString)

        txtFileDesc1.Text = Trim(DT.Rows(0).Item("IMG1_Desc").ToString)
        txtFileDesc2.Text = Trim(DT.Rows(0).Item("IMG2_Desc").ToString)

        If Not IsDBNull(DT.Rows(0).Item("Note")) Then
            txt_Note.Text = Trim(DT.Rows(0).Item("Note"))
        Else
            txt_Note.Text = ""
        End If

        If Not IsDBNull(DT.Rows(0).Item("Use_Term")) Then
            Use_Term = DT.Rows(0).Item("Use_Term")
        Else
            Use_Term = 1
        End If
        If Not IsDBNull(DT.Rows(0).Item("Required_Thickness_Mode")) Then
            Use_Term = DT.Rows(0).Item("Required_Thickness_Mode")
        Else
            Use_Term = 1
        End If

        '---------- Thickness Measurement From Instrument -------------------------
        Dim MT As DataTable = PIPE.Get_Thickness_Mesaurement_Detail(RPT_Year, RPT_No)
        MeasurementDatasource = MT
        '-------------- And Then Calculate All----------
        rdoPreferAuto.Checked = Not IsDBNull(DT.Rows(0).Item("Next_Inspection_Mode")) AndAlso DT.Rows(0).Item("Next_Inspection_Mode") = 1
        rdoPreferManual.Checked = Not IsDBNull(DT.Rows(0).Item("Next_Inspection_Mode")) AndAlso DT.Rows(0).Item("Next_Inspection_Mode") = 2
        If Not IsDBNull(DT.Rows(0).Item("Next_Manual_Inspection_Date")) Then
            txtManualDate.Text = C.DateToString(DT.Rows(0).Item("Next_Manual_Inspection_Date"), calendarNextInspect.Format)
            lblManualInterval.Text = ReportFriendlyDiffTime(RPT_Date, C.StringToDate(txtManualDate.Text, calendarNextInspect.Format), DateInterval.Month)
        Else
            txtManualDate.Text = ""
            lblManualInterval.Text = ""
        End If
        UpdateSelectedChoice()

        '------------ Class----------------------------------
        ERO_Class.ICLS_ID = DT.Rows(0).Item("ICLS_ID")

        '----------- Officer ---------------------------------------------
        If Not IsDBNull(DT.Rows(0).Item("RPT_Outsource")) Then
            PIPE.BindDDL_ReportOfficer(cmbCollector, RPT_Year, RPT_No, RPT_Step, EIR_BL.User_Level.Collector, DT.Rows(0).Item("RPT_Outsource"))
        Else
            PIPE.BindDDL_ReportOfficer(cmbCollector, RPT_Year, RPT_No, RPT_Step, EIR_BL.User_Level.Collector)
        End If

        If Not IsDBNull(DT.Rows(0).Item("RPT_Engineer")) Then
            PIPE.BindDDL_ReportOfficer(cmbInspector, RPT_Year, RPT_No, RPT_Step, EIR_BL.User_Level.Inspector, DT.Rows(0).Item("RPT_Engineer"))
        Else
            PIPE.BindDDL_ReportOfficer(cmbInspector, RPT_Year, RPT_No, RPT_Step, EIR_BL.User_Level.Inspector)
        End If

        If Not IsDBNull(DT.Rows(0).Item("RPT_Approver")) Then
            PIPE.BindDDL_ReportOfficer(cmbAnalyst, RPT_Year, RPT_No, RPT_Step, EIR_BL.User_Level.Approver, DT.Rows(0).Item("RPT_Approver"))
        Else
            PIPE.BindDDL_ReportOfficer(cmbAnalyst, RPT_Year, RPT_No, RPT_Step, EIR_BL.User_Level.Approver)
        End If

        '-------------- Bind Tab Navigation ---------------
        lblStep.Text = PIPE.Get_ReportStep_Name(RPT_Step)

    End Sub

#Region "Display Tab Navigation"
    Private Sub BindTabNavigation()
        Dim DT As DataTable = PIPE.Get_ReportStep(EIR_BL.Report_Type.Pipe_ERO_Reports)
        rptTab.DataSource = DT
        rptTab.DataBind()

        '------------- Bind Bottom Command Button ---------------
        If Not PIPE.Is_Existing_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.After_Repair) Then
            btnAfterRepair.CssClass = "button button-default"
        End If
        If Not PIPE.Is_Existing_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Insulating_Inspection) Then
            btnInsulating.CssClass = "button button-default"
        End If
        If Not PIPE.Is_Existing_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Final) Then
            btnFinalReport.CssClass = "button button-default"
        End If

    End Sub

    Private Sub rptTab_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptTab.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim ThisStep As EIR_PIPE.Report_Step = e.Item.DataItem("RPT_STEP")
        If HeaderProgress < ThisStep Then
            e.Item.Visible = False
            Exit Sub
        ElseIf Not PIPE.Is_Existing_Step(RPT_Year, RPT_No, ThisStep) Then
            e.Item.Visible = False
            Exit Sub
        End If

        Dim HTab As HtmlAnchor = e.Item.FindControl("HTab")
        HTab.InnerHtml = e.Item.DataItem("STEP_Name")
        If CInt(ThisStep) <> RPT_Step Then
            HTab.HRef = "PIPE_ERO_Edit" & CInt(ThisStep) & ".aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No
            HTab.Title = "Go to see " & e.Item.DataItem("STEP_Name")
        Else
            HTab.Attributes("Class") = "default-tab current"
        End If
    End Sub
#End Region

    Private Sub ddlInstrument_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlInstrument.SelectedIndexChanged
        Dim DT As DataTable = BL.Get_TM_Instrument_Info(INSM_ID)
        DT.DefaultView.RowFilter = "INSM_ID=" & INSM_ID
        If DT.DefaultView.Count > 0 AndAlso Not IsDBNull(DT.DefaultView(0).Item("INSM_SERIAL")) Then
            lblTMSerial.Text = DT.DefaultView(0).Item("INSM_SERIAL")
        Else
            lblTMSerial.Text = ""
        End If
        DT.Dispose()
    End Sub


#Region "Image Functional"
    Private Sub btnEditFile_Click(sender As Object, e As ImageClickEventArgs) Handles btnEdit1.Click, btnEdit2.Click
        Dim btnEdit As ImageButton = sender
        Dim ImageID As Integer = btnEdit.ID.Replace("btnEdit", "")
        Dim RefreshButton As Button = Nothing

        Dim Obj As FileAttachment = Nothing
        Dim txtFileDesc As TextBox = Nothing
        Select Case ImageID
            Case 1
                Obj = ImageFile1
                RefreshButton = btnRefresh1
                txtFileDesc = txtFileDesc1
            Case 2
                Obj = ImageFile2
                RefreshButton = btnRefresh2
                txtFileDesc = txtFileDesc2
        End Select

        If IsNothing(Obj) Then
            Obj = New FileAttachment
            With Obj
                .GenerateUniqueID()
                Select Case ImageID
                    Case 1
                        .DocType = FileAttachment.AttachmentType.Photograph
                        ImageFile1 = Obj
                    Case 2
                        .DocType = FileAttachment.AttachmentType.ReferenceDocument
                        ImageFile2 = Obj
                End Select
            End With
        End If
        Obj.Description = txtFileDesc.Text
        ShowDialogEditSVG(Me.Page, "PIPE_ERO_Edit3_" & ImageID & "_" & PAGE_UNIQUE_ID, "", RefreshButton.ClientID, "")
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh1.Click, btnRefresh2.Click
        Dim btnRefresh As Button = sender
        Dim ImageID As Integer = btnRefresh.ID.Replace("btnRefresh", "")

        Select Case ImageID
            Case 1
                ImageFile1 = ImageFile1
                txtFileDesc1.Text = ImageFile1.Description
            Case 2
                ImageFile2 = ImageFile2
                txtFileDesc2.Text = ImageFile2.Description
        End Select
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As ImageClickEventArgs) Handles btnDelete1.Click, btnDelete2.Click
        Dim btnDelete As ImageButton = sender
        Dim ImageID As Integer = btnDelete.ID.Replace("btnDelete", "")

        Select Case ImageID
            Case 1
                ImageFile1 = Nothing
            Case 2
                ImageFile2 = Nothing
        End Select
    End Sub


#End Region

#Region "Corrosion Auto Calculation"

    Private Sub btnUploadExcel_Click(sender As Object, e As EventArgs) Handles btnUploadExcel.Click
        If fulExcel.PostedFile.ContentType.ToString.ToLower.IndexOf("excel") = -1 And fulExcel.PostedFile.ContentType.ToString.ToLower.IndexOf("spreadsheetml") = -1 Then
            ScriptManager.RegisterStartupScript(Me, GetType(String), "Excel", "alert('Invalid file type (You must upload MS-Excel file only)');", True)
            Exit Sub
        End If
        Dim Path As String = BL.ServerMapPath & "\Temp\" & Now.ToOADate.ToString.Replace(".", "") & "_TM.xls"
        fulExcel.SaveAs(Path)

        Dim DT As DataTable = Nothing
        Try
            DT = ReadExcelToDataTable(Path)
            DT = TrimExcelToDataTable(DT)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, GetType(String), "Excel2", "alert('" & ex.Message.Replace("'", "\'") & "');", True)
            Try : My.Computer.FileSystem.DeleteFile(Path) : Catch : End Try
            Exit Sub
        End Try
        Try : My.Computer.FileSystem.DeleteFile(Path) : Catch : End Try

        If IsNothing(DT) OrElse DT.Columns.Count = 0 OrElse DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me, GetType(String), "Excel", "alert('There is no any data in the excel file.');", True)
            Exit Sub
        End If

        Dim MT As DataTable = MeasurementDatasource
        '-------- Clear All Data----------
        For i As Integer = 0 To MT.Rows.Count - 1
            For j As Integer = 1 To MT.Columns.Count - 1
                MT.Rows(i).Item(j) = DBNull.Value
            Next
        Next

        For i As Integer = 0 To MT.Rows.Count - 1
            Dim Point As String = MT.Rows(i).Item(0) '--------- A
            '----- Get Start Position -----------
            For j As Integer = 1 To 9
                Dim _value As Double = 0
                Try
                    _value = GetExcelValuePosition(DT, Point, j)
                Catch : End Try
                If _value <> 0 Then MT.Rows(i).Item("Sub" & j) = _value
            Next
        Next

        rptMeasure.DataSource = MT
        rptMeasure.DataBind()

    End Sub

    Private Function GetExcelValuePosition(ByVal DT As DataTable, ByVal Point As String, ByVal Index As String) As Double
        Dim ColIndex As Integer = -1
        Dim HeaderRowIndex As Integer = -1
        For i As Integer = 0 To DT.Rows.Count - 1
            For j As Integer = 1 To DT.Columns.Count - 1
                If DT.Rows(i).Item(j).ToString.ToUpper = Point.ToUpper Then
                    ColIndex = j
                    HeaderRowIndex = i
                    Exit For
                End If
            Next
            If ColIndex <> -1 And HeaderRowIndex <> -1 Then Exit For
        Next

        If ColIndex = -1 Or HeaderRowIndex = -1 Then Return 0
        Dim RowIndex As Integer = -1
        For i As Integer = HeaderRowIndex + 1 To DT.Rows.Count - 1
            For j As Integer = 0 To ColIndex - 1
                If DT.Rows(i).Item(j).ToString = Index Then
                    RowIndex = i
                    Exit For
                End If
            Next
            If RowIndex <> -1 Then Exit For
        Next
        If RowIndex = -1 Then Return 0

        If IsNumeric(DT.Rows(RowIndex).Item(ColIndex)) Then
            Return CDbl(DT.Rows(RowIndex).Item(ColIndex))
        Else
            Return 0
        End If
    End Function

    Private Sub Point_Info_PropertyChangedByUser(ByRef Sender As UC_PIPE_POINT, Prop As UC_PIPE_POINT.PropertyItem) Handles Point_Info.PropertyChangedByUser
        Select Case Prop
            Case UC_PIPE_POINT.PropertyItem.INITIAL_YEAR
                Initial_Year = Point_Info.INITIAL_YEAR
                CalculateAll()
            Case UC_PIPE_POINT.PropertyItem.NORMINAL_THICKNESS
                Norminal_Thickness = Point_Info.NORMINAL_THICKNESS
                CalculateAll()
            Case UC_PIPE_POINT.PropertyItem.CALCULATED_THICKNESS
                Calculated_Thickness = Point_Info.CALCULATED_THICKNESS
                CalculateAll()
            Case UC_PIPE_POINT.PropertyItem.CORROSION_ALLOWANCE
                CA_ID = Point_Info.CA_ID
                CalculateAll()
        End Select
    End Sub

    Private Sub rptMeasure_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptMeasure.ItemDataBound

        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim lblPoint As Label = e.Item.FindControl("lblPoint")
                lblPoint.Text = e.Item.DataItem("Point")
                For i As Integer = 1 To 9
                    Dim txt As TextBox = e.Item.FindControl("txt" & i)
                    If IsNumeric(e.Item.DataItem("Sub" & i)) Then
                        txt.Text = FormatNumericTextLimitPlace(e.Item.DataItem("Sub" & i), True, 2)
                    End If
                    ImplementJavaNumericText(txt, 2, "center")
                    txt.Attributes("onChange") &= "$('#" & btnCalculate.ClientID & "').click();"
                Next
            Case ListItemType.Footer
                CalculateAll()
        End Select

    End Sub

    Private Sub UpdatePreviousReportDetail(sender As Object, e As EventArgs) Handles txt_Rpt_Date.TextChanged
        If IsNothing(RPT_Date) Then
            Last_Thickness = Nothing
            Last_Measure_Year = Nothing
            Last_RPT_Year = Nothing
            Last_RPT_No = Nothing
            Last_RPT_Date = Nothing
        Else
            Dim DT As DataTable = PIPE.Get_Previous_Thickness_Mesaurement_Detail(TAG_ID, POINT_ID, RPT_Date)
            If DT.Rows.Count = 0 Then
                Last_Thickness = Nothing
                Last_Measure_Year = Nothing
                Last_RPT_Year = Nothing
                Last_RPT_No = Nothing
                Last_RPT_Date = Nothing
            Else
                If Not IsDBNull(DT.Rows(0).Item("Last_Thickness")) Then
                    Last_Thickness = FormatNumericTextLimitPlace(DT.Rows(0).Item("Last_Thickness"), True, 2)
                Else
                    Last_Thickness = Nothing
                End If
                If Not IsDBNull(DT.Rows(0).Item("Last_Measure_Year")) Then
                    Last_Measure_Year = CInt(DT.Rows(0).Item("Last_Measure_Year"))
                Else
                    Last_Measure_Year = Nothing
                End If
                If Not IsDBNull(DT.Rows(0).Item("Last_RPT_Year")) Then
                    Last_RPT_Year = DT.Rows(0).Item("Last_RPT_Year")
                Else
                    Last_RPT_Year = Nothing
                End If
                If Not IsDBNull(DT.Rows(0).Item("Last_RPT_No")) Then
                    Last_RPT_No = DT.Rows(0).Item("Last_RPT_No")
                Else
                    Last_RPT_No = Nothing
                End If
                If Not IsDBNull(DT.Rows(0).Item("Last_Measure_Date")) Then
                    Last_RPT_Date = DT.Rows(0).Item("Last_Measure_Date")
                Else
                    Last_RPT_Date = Nothing
                End If

                Dim Tooltip As String = "Last measure on report : " & BL.GetReportCode(Last_RPT_Year, Last_RPT_No) & " measured on #" & BL.ReportProgrammingDate(Last_RPT_Date)
                lbl_s_LastThick.ToolTip = Tooltip
                lbl_s_LastYear.ToolTip = Tooltip
            End If
        End If

        If Not IsNothing(sender) Then CalculateAll()
    End Sub

    Private Sub CalculateAll()

        '------------ Calculate Diff Year--------------
        If Not IsNothing(Last_Measure_Year) And Not IsNothing(RPT_Date) Then
            Diff_Year_Short = DatePart(DateInterval.Year, RPT_Date) - Last_Measure_Year
        Else
            Diff_Year_Short = Nothing
        End If

        If Not IsNothing(Initial_Year) And Not IsNothing(RPT_Date) Then
            Diff_Year_Long = DatePart(DateInterval.Year, RPT_Date) - Initial_Year
        Else
            Diff_Year_Long = Nothing
        End If
        '--------- Get Minimum Actual Thickness And Summary------------
        Dim DT As DataTable = MeasurementDatasource
        Dim MinTM As Double = Double.MaxValue
        For i As Integer = 1 To 9
            Dim AVG As Object = DT.Compute("AVG(Sub" & i & ")", "")
            Dim lblAVG As Label = CType(UDPMain.FindControl("lblAVG" & i), Label)
            If Not IsDBNull(AVG) Then
                lblAVG.Text = FormatNumericTextLimitPlace(AVG, True, 2)
            Else
                lblAVG.Text = ""
            End If

            Dim MIN As Object = DT.Compute("MIN(Sub" & i & ")", "")
            Dim lblMIN As Label = CType(UDPMain.FindControl("lblMIN" & i), Label)
            If Not IsDBNull(MIN) Then
                lblMIN.Text = FormatNumericTextLimitPlace(MIN, True, 2)
                If MIN < MinTM Then MinTM = MIN
            Else
                lblMIN.Text = ""
            End If

            Dim MAX As Object = DT.Compute("MAX(Sub" & i & ")", "")
            Dim lblMAX As Label = CType(UDPMain.FindControl("lblMAX" & i), Label)
            If Not IsDBNull(MAX) Then
                lblMAX.Text = FormatNumericTextLimitPlace(MAX, True, 2)
            Else
                lblMAX.Text = ""
            End If
        Next
        If MinTM < Double.MaxValue Then
            Minimum_Actual_Thickness = MinTM
        Else
            Minimum_Actual_Thickness = Nothing
        End If

        '-------------- Get Required Thickness ---------------
        Select Case Required_Thickness_Mode
            Case 1 'Norminal_Thickness -Corrosion_Allowance
                If Not IsNothing(Norminal_Thickness) Then
                    Required_Thickness = CDbl(Norminal_Thickness) - Corrosion_Allowance
                Else
                    Required_Thickness = Nothing
                End If
            Case 2 '
                If Not IsNothing(Calculated_Thickness) Then
                    Required_Thickness = CDbl(Calculated_Thickness) + Corrosion_Allowance
                Else
                    Required_Thickness = Nothing
                End If
            Case 3
                If Not IsNothing(Calculated_Thickness) Then
                    Required_Thickness = CDbl(Calculated_Thickness)
                Else
                    Required_Thickness = Nothing
                End If
        End Select

        '-------------- Calculate For Short Term--------------
        If Not IsShortTermCalculationCompleted OrElse Diff_Year_Short = 0 Then
            Corrosion_rate_Short = Nothing
            Remain_Life_Short = Nothing
        Else
            Corrosion_rate_Short = (Last_Thickness - Minimum_Actual_Thickness) / Diff_Year_Short
            If Corrosion_rate_Short <= 0 Then Corrosion_rate_Short = 0
            Remain_Life_Short = (Minimum_Actual_Thickness - Required_Thickness) / Corrosion_rate_Short
            If Remain_Life_Short <= 0 Then Remain_Life_Short = 0
        End If


        '-------------- Calculate For Long Term--------------
        If Not IsLongTermCalculationCompleted OrElse Diff_Year_Long = 0 Then
            Corrosion_rate_Long = Nothing
            Remain_Life_Long = Nothing
        Else
            Corrosion_rate_Long = (Norminal_Thickness - Minimum_Actual_Thickness) / Diff_Year_Long
            If Corrosion_rate_Long <= 0 Then Corrosion_rate_Long = 0
            Remain_Life_Long = (Minimum_Actual_Thickness - Required_Thickness) / Corrosion_rate_Long
            If Remain_Life_Long <= 0 Then Remain_Life_Long = 0
        End If

        If Not IsNothing(RPT_Date) Then
            lblReportDate.Text = " for Inspection date : " & C.DateToString(RPT_Date, calendarNextInspect.Format) & " ( " & ReportFriendlyDiffTime(RPT_Date, Now, DateInterval.Day) & " )"
        Else
            lblReportDate.Text = ""
        End If

        '------------ Set Display Auto Next Inspection -----------
        Dim Life As Object = Nothing
        Select Case Use_Term
            Case 1 '------------ Short Term ------------
                Life = Remain_Life_Short
            Case 2 '------------ Long Term ------------
                Life = Remain_Life_Long
        End Select
        If Not IsNumeric(Life) Then
            lblPreferDate.Text = ""
            lblPreferInterval.Text = ""
        Else
            If Life > 4 Then '--------- ถ้ามากกว่า 4 ปี ตรวจอีก 2 ปีข้างหน้า 
                lblPreferDate.Text = C.DateToString(DateAdd(DateInterval.Year, 2, RPT_Date), calendarNextInspect.Format)
                lblPreferInterval.Text = "2 years"
            Else
                Dim NextDate As DateTime = DateAdd(DateInterval.Year, Life / 2, RPT_Date)
                lblPreferDate.Text = C.DateToString(DateAdd(DateInterval.Year, Life / 2, RPT_Date), calendarNextInspect.Format)
                lblPreferInterval.Text = ReportFriendlyDiffTime(RPT_Date, NextDate, DateInterval.Month)
            End If
        End If

        '------------ Suggess Class ---------------
        If IsNumeric(Minimum_Actual_Thickness) And IsNumeric(Norminal_Thickness) Then

            Dim Corrosion_Deep As Double = Norminal_Thickness - Minimum_Actual_Thickness

            'If Corrosion_Deep < Corrosion_Allowance Then
            '    ERO_Class.SuggessClass = EIR_BL.InspectionLevel.ClassC '<<------ ตก Class C
            'ElseIf Minimum_Actual_Thickness > Calculated_Thickness Then
            '    ERO_Class.SuggessClass = EIR_BL.InspectionLevel.ClassB '<<------ ตก Class B
            'ElseIf Minimum_Actual_Thickness <= Calculated_Thickness Then
            '    ERO_Class.SuggessClass = EIR_BL.InspectionLevel.ClassA '<<------ ตก Class A
            'End If

            If Corrosion_Deep < Corrosion_Allowance Then '<<------ ตก Class C
                ERO_Class.SuggessClass = EIR_BL.InspectionLevel.ClassC
            ElseIf Minimum_Actual_Thickness > Calculated_Thickness + Corrosion_Allowance Then '<<------ ตก Class B
                ERO_Class.SuggessClass = EIR_BL.InspectionLevel.ClassB
            ElseIf Minimum_Actual_Thickness <= Corrosion_Allowance + Calculated_Thickness Then '<<------ ตก Class A
                ERO_Class.SuggessClass = EIR_BL.InspectionLevel.ClassA
            Else
                ERO_Class.SuggessClass = EIR_BL.InspectionLevel.Normal
            End If

            'If Minimum_Actual_Thickness < Corrosion_Allowance + Calculated_Thickness Then
            '    CUI_Class.SuggessClass = EIR_BL.InspectionLevel.ClassA
            'ElseIf Corrosion_Deep >= Corrosion_Allowance Then
            '    CUI_Class.SuggessClass = EIR_BL.InspectionLevel.ClassB
            'ElseIf Corrosion_Deep < Corrosion_Allowance Then
            '    CUI_Class.SuggessClass = EIR_BL.InspectionLevel.ClassC
            'Else
            '    CUI_Class.SuggessClass = EIR_BL.InspectionLevel.Normal
            'End If

            '--------------- Auto Selected Class ------------
            If IsDBNull(ERO_Class.ICLS_ID) Then
                ERO_Class.ICLS_ID = ERO_Class.SuggessClass
            End If
        Else

        End If

        '------------- Set Display Manual Next Inspection ---------
        If Not IsNumeric(Life) Or txtManualDate.Text = "" Then
            lblManualInterval.Text = ""
        Else
            lblManualInterval.Text = ReportFriendlyDiffTime(RPT_Date, C.StringToDate(txtManualDate.Text, calendarNextInspect.Format), DateInterval.Month)
        End If

    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click, ddl_Treq.SelectedIndexChanged, rdoShort.CheckedChanged, rdoLong.CheckedChanged, txtManualDate.TextChanged
        CalculateAll()
    End Sub

    Private Sub lbl_LastDetail_Click(sender As Object, e As EventArgs) Handles lbl_s_LastThick.Click, lbl_s_LastYear.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "ShowLastDetail", "ShowPreviewReport(" & Last_RPT_Year & "," & Last_RPT_No & ");", True)
    End Sub

    Public ReadOnly Property IsShortTermCalculationCompleted As Boolean
        Get
            Return Not IsNothing(Required_Thickness) And Not IsNothing(Last_Thickness) And Not IsNothing(Minimum_Actual_Thickness) And
                Not IsNothing(Last_Measure_Year) And Not IsNothing(Diff_Year_Short)
        End Get
    End Property

    Public ReadOnly Property IsLongTermCalculationCompleted As Boolean
        Get
            'Return Not IsNothing(Required_Thickness) And Not IsNothing(Initial_Thickness) And Not IsNothing(Minimum_Actual_Thickness) And
            '    Not IsNothing(Initial_Year) And Not IsNothing(Diff_Year_Long)
            Return Not IsNothing(Required_Thickness) And Not IsNothing(Norminal_Thickness) And Not IsNothing(Minimum_Actual_Thickness) And
                Not IsNothing(Initial_Year) And Not IsNothing(Diff_Year_Long)
        End Get
    End Property

    Public ReadOnly Property IsInformationCompleted() As Boolean
        Get
            If IsNothing(RPT_Date) Then
                lblValidation.Text = "Select report date"
                pnlValidation.Visible = True
                Return False
            End If
            If Pipe_Info.ValidateIncompleteMessage <> "" Then
                lblValidation.Text = Pipe_Info.ValidateIncompleteMessage
                pnlValidation.Visible = True
                Return False
            End If
            If Point_Info.ValidateIncompleteMessage <> "" Then
                lblValidation.Text = Point_Info.ValidateIncompleteMessage
                pnlValidation.Visible = True
                Return False
            End If
            If INSM_ID = 0 Then
                lblValidation.Text = "Select instrument"
                pnlValidation.Visible = True
                Return False
            End If

            Select Case Use_Term
                Case 1
                    If IsNothing(Remain_Life_Short) Then
                        lblValidation.Text = "Remaining Life Calculation must be completed"
                        pnlValidation.Visible = True
                        Return False
                    End If
                Case 2
                    If IsNothing(Remain_Life_Long) Then
                        lblValidation.Text = "Remaining Life Calculation must be completed"
                        pnlValidation.Visible = True
                        Return False
                    End If
            End Select

            If IsNothing(ImageFile1) And txtFileDesc1.Text <> "" Then
                lblValidation.Text = "Upload isomitric drawing neither leave information to blank"
                pnlValidation.Visible = True
                Return False
            End If
            If IsNothing(ImageFile2) And txtFileDesc2.Text <> "" Then
                lblValidation.Text = "Upload photograph 1 neither leave information to blank"
                pnlValidation.Visible = True
                Return False
            End If

            If Not rdoPreferAuto.Checked And Not rdoPreferManual.Checked Then
                lblValidation.Text = "Select next inspection preference"
                rdoPreferAuto.Focus()
                pnlValidation.Visible = True
                Return False
            End If

            If IsDBNull(ERO_Class.ICLS_ID) Then
                lblValidation.Text = "Select Class Specification"
                ERO_Class.Focus()
                pnlValidation.Visible = True
                Return False
            End If

            If rdoPreferManual.Checked And txtManualDate.Text = "" Then
                lblValidation.Text = "Specific next inspection date"
                txtManualDate.Focus()
                pnlValidation.Visible = True
                Return False
            End If

            If cmbCollector.SelectedIndex = 0 Then
                lblValidation.Text = "Choose creater"
                pnlValidation.Visible = True
                Return False
            End If
            If cmbInspector.SelectedIndex = 0 Then
                lblValidation.Text = "Choose evaluater"
                pnlValidation.Visible = True
                Return False
            End If
            If cmbAnalyst.SelectedIndex = 0 Then
                lblValidation.Text = "Choose approver"
                pnlValidation.Visible = True
                Return False
            End If


            Return True
        End Get
    End Property
#End Region

#Region "Saving"
    Private Sub SaveTab()

        '---------------- Save Point_Info----------
        If Point_Info.ValidateIncompleteMessage <> "" Then
            Dim ER As New Exception(Point_Info.ValidateIncompleteMessage)
            Throw (ER)
        End If
        Try
            Point_Info.SaveData()
        Catch ex As Exception
            Dim ER As New Exception(ex.Message)
            Throw (ER)
        End Try

        '---------------- Save Step Detail---------
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM RPT_PIPE_TM_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
        Else
            DR = DT.Rows(0)
        End If
        Try
            DR("RPT_Date") = C.StringToDate(txt_Rpt_Date.Text, txt_Rpt_Date_Extender.Format)
        Catch ex As Exception
            DR("RPT_Date") = DBNull.Value
        End Try

        If Not IsNothing(Last_RPT_Year) Then
            DR("Last_RPT_Year") = Last_RPT_Year
        Else
            DR("Last_RPT_Year") = DBNull.Value
        End If
        If Not IsNothing(Last_RPT_No) Then
            DR("Last_RPT_No") = Last_RPT_No
        Else
            DR("Last_RPT_No") = DBNull.Value
        End If
        If Not IsNothing(Last_Measure_Year) Then
            DR("Last_Measure_Year") = Last_Measure_Year
        Else
            DR("Last_Measure_Year") = DBNull.Value
        End If
        If Not IsNothing(Last_Thickness) Then
            DR("Last_Thickness") = Last_Thickness
        Else
            DR("Last_Thickness") = DBNull.Value
        End If

        If Not IsNothing(Initial_Year) Then
            DR("Initial_Year") = Initial_Year
        Else
            DR("Initial_Year") = DBNull.Value
        End If
        'If Not IsNothing(Initial_Thickness) Then
        '    DR("Initial_Thickness") = Initial_Thickness
        'Else
        '    DR("Initial_Thickness") = DBNull.Value
        'End If
        If Not IsNothing(Norminal_Thickness) Then
            DR("Initial_Thickness") = Norminal_Thickness
        Else
            DR("Initial_Thickness") = DBNull.Value
        End If
        If Not IsNothing(Minimum_Actual_Thickness) Then
            DR("Min_Thickness") = Minimum_Actual_Thickness
        Else
            DR("Min_Thickness") = DBNull.Value
        End If

        If INSM_ID <> 0 Then
            DR("INSM_ID") = INSM_ID
        Else
            DR("INSM_ID") = DBNull.Value
        End If
        If IsNumeric(txtStep1.Text) Then
            DR("StepWeight_Thickness1") = Val(txtStep1.Text.Replace(",", ""))
        Else
            DR("StepWeight_Thickness1") = DBNull.Value
        End If
        If IsNumeric(txtStep2.Text) Then
            DR("StepWeight_Thickness2") = Val(txtStep2.Text.Replace(",", ""))
        Else
            DR("StepWeight_Thickness2") = DBNull.Value
        End If
        If IsNumeric(txtStep3.Text) Then
            DR("StepWeight_Thickness3") = Val(txtStep3.Text.Replace(",", ""))
        Else
            DR("StepWeight_Thickness3") = DBNull.Value
        End If
        If IsNumeric(txtStep4.Text) Then
            DR("StepWeight_Thickness4") = Val(txtStep4.Text.Replace(",", ""))
        Else
            DR("StepWeight_Thickness4") = DBNull.Value
        End If

        If IsNumeric(txtMeasure1.Text) Then
            DR("Measurement1") = Val(txtMeasure1.Text.Replace(",", ""))
        Else
            DR("Measurement1") = DBNull.Value
        End If
        If IsNumeric(txtMeasure2.Text) Then
            DR("Measurement2") = Val(txtMeasure2.Text.Replace(",", ""))
        Else
            DR("Measurement2") = DBNull.Value
        End If
        If IsNumeric(txtMeasure3.Text) Then
            DR("Measurement3") = Val(txtMeasure3.Text.Replace(",", ""))
        Else
            DR("Measurement3") = DBNull.Value
        End If
        If IsNumeric(txtMeasure4.Text) Then
            DR("Measurement4") = Val(txtMeasure4.Text.Replace(",", ""))
        Else
            DR("Measurement4") = DBNull.Value
        End If
        If IsNumeric(txtVelocity.Text) Then
            DR("Velocity") = Val(txtVelocity.Text.Replace(",", ""))
        Else
            DR("Velocity") = DBNull.Value
        End If

        DR("IMG1_Desc") = txtFileDesc1.Text
        DR("IMG2_Desc") = txtFileDesc2.Text
        DR("Note") = txt_Note.Text

        If Not IsNothing(Minimum_Actual_Thickness) Then
            DR("Min_Thickness") = Minimum_Actual_Thickness
        Else
            DR("Min_Thickness") = DBNull.Value
        End If

        If Not IsNothing(Required_Thickness_Mode) Then
            DR("Required_Thickness_Mode") = Required_Thickness_Mode
        Else
            DR("Required_Thickness_Mode") = DBNull.Value
        End If

        DR("Use_Term") = Use_Term
        '---------------- Corrosion Rate,Remaining Life  ------------
        Select Case Use_Term
            Case 1 '----- Short --------
                If Not IsNothing(Corrosion_rate_Short) Then
                    DR("Corrosion_Rate") = Corrosion_rate_Short
                Else
                    DR("Corrosion_Rate") = DBNull.Value
                End If
                If Not IsNothing(Remain_Life_Short) Then
                    DR("Remain_Life") = Remain_Life_Short
                Else
                    DR("Remain_Life") = DBNull.Value
                End If
            Case 2 '----- Long --------
                If Not IsNothing(Corrosion_rate_Long) Then
                    DR("Corrosion_Rate") = Corrosion_rate_Long
                Else
                    DR("Corrosion_Rate") = DBNull.Value
                End If
                If Not IsNothing(Remain_Life_Long) Then
                    DR("Remain_Life") = Remain_Life_Long
                Else
                    DR("Remain_Life") = DBNull.Value
                End If
        End Select
        If Not IsNothing(Norminal_Thickness) Then
            DR("Norminal_Thickness") = Norminal_Thickness
        Else
            DR("Norminal_Thickness") = DBNull.Value
        End If
        If CA_ID <> 0 Then
            DR("CA_ID") = CA_ID
        Else
            DR("CA_ID") = DBNull.Value
        End If
        If Not IsNothing(Calculated_Thickness) Then
            DR("Calculated_Thickness") = Calculated_Thickness
        Else
            DR("Calculated_Thickness") = DBNull.Value
        End If
        If Not IsNothing(Required_Thickness) Then
            DR("Required_Thickness") = Required_Thickness
        Else
            DR("Required_Thickness") = DBNull.Value
        End If
        '---------------- Next Inspection --------------
        If rdoPreferAuto.Checked Then
            DR("Next_Inspection_Mode") = 1
        ElseIf rdoPreferManual.Checked Then
            DR("Next_Inspection_Mode") = 2
        Else
            DR("Next_Inspection_Mode") = 0
        End If
        If lblPreferDate.Text <> "" Then
            DR("Next_Auto_Inspection_Date") = C.StringToDate(lblPreferDate.Text, calendarNextInspect.Format)
        Else
            DR("Next_Auto_Inspection_Date") = DBNull.Value
        End If
        If txtManualDate.Text <> "" Then
            DR("Next_Manual_Inspection_Date") = C.StringToDate(txtManualDate.Text, calendarNextInspect.Format)
        Else
            DR("Next_Manual_Inspection_Date") = DBNull.Value
        End If
        '---------------- Class --------------
        DR("ICLS_ID") = ERO_Class.ICLS_ID

        '---------------- Officer --------------
        If cmbCollector.SelectedIndex > 0 Then
            DR("RPT_Outsource") = cmbCollector.Items(cmbCollector.SelectedIndex).Text
        Else
            DR("RPT_Outsource") = DBNull.Value
        End If
        If cmbInspector.SelectedIndex > 0 Then
            DR("RPT_Engineer") = cmbInspector.Items(cmbInspector.SelectedIndex).Text
        Else
            DR("RPT_Engineer") = DBNull.Value
        End If
        If cmbAnalyst.SelectedIndex > 0 Then
            DR("RPT_Approver") = cmbAnalyst.Items(cmbAnalyst.SelectedIndex).Text
        Else
            DR("RPT_Approver") = DBNull.Value
        End If
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now
        Dim CMD As New SqlCommandBuilder(DA)
        DA.Update(DT)
        '---------------- Save Image File---------------------------
        '---------------- DROP ALL File---------------------------
        PIPE.Drop_RPT_Image(RPT_Year, RPT_No, RPT_Step)
        If Not IsNothing(ImageFile1) Then
            PIPE.SAVE_RPT_FILE(RPT_Year, RPT_No, RPT_Step, 1, ImageFile1)
        End If
        If Not IsNothing(ImageFile2) Then
            PIPE.SAVE_RPT_FILE(RPT_Year, RPT_No, RPT_Step, 2, ImageFile2)
        End If
        '----------------- Save TM Detail--------------
        Dim MT As DataTable = MeasurementDatasource
        PIPE.Drop_RPT_TM_Detail(RPT_Year, RPT_No)
        DT = New DataTable
        DA = New SqlDataAdapter("SELECT * FROM RPT_PIPE_TM_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        For i As Integer = 0 To MT.Rows.Count - 1
            Dim P As String = MT.Rows(i).Item("Point")
            For j As Integer = 1 To 9
                If Not IsDBNull(MT.Rows(i).Item("Sub" & j)) Then
                    Dim R As DataRow = DT.NewRow
                    R("RPT_Year") = RPT_Year
                    R("RPT_No") = RPT_No
                    R("Measure_Point") = P & j
                    R("Thickness") = MT.Rows(i).Item("Sub" & j)
                    DT.Rows.Add(R)
                End If
            Next
        Next
        If DT.Rows.Count > 0 Then
            CMD = New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If
    End Sub
#End Region

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try
        Response.Redirect("PIPE_ERO_Summary.aspx", True)
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try
        Response.Redirect("PIPE_ERO_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&", True)
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Saving", "alert('Save successfully');", True)
        BindTabData()
    End Sub

    Private Sub btnStep_Click(sender As Object, e As EventArgs) Handles btnAfterRepair.Click, btnInsulating.Click, btnFinalReport.Click
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try
        BindTabData()
        '------------ Validate For Completion For This Step--------------
        If Not IsInformationCompleted Then Exit Sub

        Dim TableName As String = ""
        Dim NextStep As EIR_PIPE.Report_Step
        Select Case True
            Case Equals(sender, btnAfterRepair)
                TableName = "RPT_PIPE_After_Repair"
                NextStep = EIR_PIPE.Report_Step.After_Repair
            Case Equals(sender, btnInsulating)
                TableName = "RPT_PIPE_Insulating"
                NextStep = EIR_PIPE.Report_Step.Insulating_Inspection
            Case Equals(sender, btnFinalReport)
                TableName = "RPT_PIPE_Final_Report"
                NextStep = EIR_PIPE.Report_Step.Final
        End Select

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM " & TableName & " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)

        '-----------------Create Default Info If Not Existed------------
        If DT.Rows.Count = 0 Then
            Dim DR As DataRow
            DR = DT.NewRow
            DT.Rows.Add(DR)
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
            DR("ICLS_ID") = ERO_Class.ICLS_ID
            DR("Update_By") = Session("USER_ID")
            DR("Update_Time") = Now
            Dim CMD As New SqlCommandBuilder(DA)
            DA.Update(DT)
            '------------------Update ERO_Header Step-----------------------
            If HeaderProgress < NextStep Then
                PIPE.Update_ERO_Header_Step(RPT_Year, RPT_No, NextStep, Session("USER_ID"))
            End If
        End If

        '------------ Redirect to next step-----------------------------
        Response.Redirect("PIPE_ERO_Edit" & NextStep & ".aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&r=1", True)

    End Sub

    Private Sub btnPost_Click(sender As Object, e As EventArgs) Handles btnPost.Click
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try
        BindTabData()
        '------------ Validate For Completion For This Step--------------
        If Not IsInformationCompleted Then Exit Sub

        '------------ Drop Unused Step ----------------------------------
        PIPE.Drop_RPT_By_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Final)
        PIPE.Drop_RPT_By_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Insulating_Inspection)
        PIPE.Drop_RPT_By_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Coating_Inspection)
        PIPE.Drop_RPT_By_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.After_Repair)
        PIPE.Update_ERO_Header_Step(RPT_Year, RPT_No, RPT_Step, Session("User_ID"))

        '------------ Request Filename To Save Result PDF ---------------
        Dim DefaultValue As String = Now.Month.ToString.PadLeft(2, "0") & "-" & Now.Day.ToString.PadLeft(2, "0") & "-" & (Now.Year + 543).ToString.Substring(2, 2) & "_" & lbl_Plant.Text.Replace("#", "").Replace("_", "") & "_PIPE_ERO_"
        DefaultValue &= lbl_Tag.Text.Replace("""", "(inch)") & "_" & UCase(Session("USER_Name")) & "_" & lblReportCode.Text & ".PDF"

        DefaultValue = DefaultValue.Replace(":", "_").Replace("\", "_").Replace("/", "_")

        DialogInput.ShowDialog("Please insert finalize report file name..", DefaultValue)
    End Sub

    Protected Sub DialogInput_AnswerDialog(ByVal Result As String) Handles DialogInput.AnswerDialog
        If Result = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('Please insert file name to be saved');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If
        If Not BL.IsFormatFileName(Result) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('File name must not contained following excepted charactors /\:*?""<>|;');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If

        Result = Result.Replace(":", "_").Replace("\", "_").Replace("/", "_")

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT dbo.UDF_RPT_Code(RPT_Year,RPT_No) RPT_Code FROM RPT_PIPE_ERO_Header WHERE Result_FileName='" & Replace(Result, "'", "''") & "' AND RPT_Year<>" & RPT_Year & " AND RPT_No<>" & RPT_No, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This file name is already exists. And has been reserved for report " & DT.Rows(0).Item("RPT_Code") & "');", True)
            DialogInput.Visible = True
            DialogInput.Enable()
            Exit Sub
        End If

        '---------------- Generate Posted Report-----------
        If Result.Length >= 4 AndAlso Right(Result, 4).ToUpper <> ".PDF".ToUpper Then
            Result = Result & ".PDF"
        ElseIf Result.Length < 4 Then
            Result = Result & ".PDF"
        End If
        Dim DestinationPath As String = BL.PostedReport_Path & "\" & Result
        Dim GenerateResult = BL.GeneratePostedReport(RPT_Year, RPT_No, DestinationPath)
        If Not GenerateResult.Success Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PostedReport", "alert('Unavailable to posted report !!'); alert('" & GenerateResult.Message.Replace("'", """") & "');", True)
            DialogInput.Visible = False
            Exit Sub
        End If
        DialogInput.Visible = False
        '--------------------- Delete Next Later Step -------------------------------
        PIPE.Drop_RPT_By_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Final)
        PIPE.Drop_RPT_By_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Insulating_Inspection)
        PIPE.Drop_RPT_By_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.Coating_Inspection)
        PIPE.Drop_RPT_By_Step(RPT_Year, RPT_No, EIR_PIPE.Report_Step.After_Repair)
        '--------------------- Update Report Status ---------------------------------
        Dim Conn As New SqlConnection(BL.ConnStr)
        Conn.Open()
        Dim Comm As New SqlCommand
        Comm.Connection = Conn
        Comm.CommandType = CommandType.Text
        Comm.CommandText = "UPDATE RPT_PIPE_ERO_Header set RPT_LOCK_BY=NULL,RPT_STEP=" & CInt(RPT_Step) & ",Finished_Time=GETDATE(),Update_By=" & Session("USER_ID") & ",UPDATE_TIME=GETDATE(),Result_FileName='" & Replace(Result, "'", "''") & "'" & vbNewLine
        Comm.CommandText &= "WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Comm.ExecuteNonQuery()
        Comm.Dispose()
        Conn.Close()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PostedReport", "alert('Report has been approved and job done!!'); ShowPreviewReport(" & RPT_Year & "," & RPT_No & "); window.location.href='PIPE_ERO_Summary.aspx';", True)

    End Sub

    Private Sub lnkClear_Click(sender As Object, e As EventArgs) Handles lnkClear.Click
        '------------- Clear Image-------------
        PIPE.Drop_RPT_Image(RPT_Year, RPT_No, RPT_Step)

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM RPT_PIPE_TM_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
        DA.Fill(DT)

        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
            DR("RPT_Year") = RPT_Year
            DR("RPT_No") = RPT_No
        Else
            DR = DT.Rows(0)
        End If

        DR("Last_RPT_Year") = DBNull.Value
        DR("Last_RPT_No") = DBNull.Value
        DR("Last_Measure_Year") = DBNull.Value
        DR("Last_Thickness") = DBNull.Value
        DR("RPT_Date") = DBNull.Value
        DR("INSM_ID") = DBNull.Value
        DR("StepWeight_Thickness1") = DBNull.Value
        DR("StepWeight_Thickness2") = DBNull.Value
        DR("StepWeight_Thickness3") = DBNull.Value
        DR("StepWeight_Thickness4") = DBNull.Value
        DR("Measurement1") = DBNull.Value
        DR("Measurement2") = DBNull.Value
        DR("Measurement3") = DBNull.Value
        DR("Measurement4") = DBNull.Value
        DR("Velocity") = DBNull.Value
        DR("LOC_ID") = DBNull.Value
        DR("IMG1_Desc") = DBNull.Value
        DR("IMG2_Desc") = DBNull.Value
        DR("Note") = DBNull.Value
        DR("Min_Thickness") = DBNull.Value
        DR("Corrosion_Rate") = DBNull.Value
        DR("Remain_Life") = DBNull.Value
        DR("Next_Inspection_Mode") = DBNull.Value
        DR("Next_Auto_Inspection_Date") = DBNull.Value
        DR("Next_Manual_Inspection_Date") = DBNull.Value
        DR("ICLS_ID") = DBNull.Value
        DR("RPT_Outsource") = DBNull.Value
        DR("RPT_Engineer") = DBNull.Value
        DR("RPT_Approver") = DBNull.Value
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        Dim CMD As New SqlCommandBuilder(DA)
        DA.Update(DT)
        BindTabData()

    End Sub

    Private Sub lnkPreview_Click(sender As Object, e As EventArgs) Handles lnkPreview.Click
        '------------- Save First ---------
        Try
            SaveTab()
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try
        BindTabData()

        '------------- Show Dialog-------------
        Dim Param As String = "RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "&Detail_ID=" & CInt(RPT_Step) & "&"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "PreviewReport", "ShowPreviewReportWithParam('" & Param & "');", True)
    End Sub

    Private Sub rdoPrefer_CheckedChanged(sender As Object, e As EventArgs) Handles rdoPreferAuto.CheckedChanged, rdoPreferManual.CheckedChanged
        UpdateSelectedChoice()
    End Sub

    Private Sub UpdateSelectedChoice()
        trAuto.Attributes("class") = ""
        trManual.Attributes("class") = ""
        If rdoPreferAuto.Checked Then
            trAuto.Attributes("class") = "trSelected"
        ElseIf rdoPreferManual.Checked Then
            trManual.Attributes("class") = "trSelected"
        End If
    End Sub
End Class