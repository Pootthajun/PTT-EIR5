﻿Imports System.Data
Imports System.Data.SqlClient
Public Class UC_Dashboard_Summary_Route
    Inherits System.Web.UI.UserControl
    Dim Dashboard As New DashboardClass
    Dim BL As New EIR_BL

    Public Sub BindData(ByVal Plant_ID As Integer, ByVal Route_ID As Integer, ByVal Route_Name As String, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As Integer, Optional ByVal PrintPage As Boolean = False)
        Dim SQL As String = ""
        Dim DT_Dashboard As New DataTable
        Dim DA As New SqlDataAdapter

        If Year_F > 2500 Then
            Year_F = Year_F - 543
        End If
        If Year_T > 2500 Then
            Year_T = Year_T - 543
        End If

        lblMONTH_F.Text = Month_F
        lblMONTH_T.Text = Month_T
        lblYEAR_F.Text = Year_F
        lblYEAR_T.Text = Year_T
        lblEQUIPMENT.Text = Equipment
        lblPrintPage.Text = PrintPage
        lblPlantID.Text = Plant_ID
        lblRouteID.Text = Route_ID
        lblRouteName.Text = Route_Name

        DT_Dashboard = Dashboard.SummaryReportRoute(Plant_ID, Route_ID, Month_F, Month_T, Year_F, Year_T, Equipment)
        Session("Dashboard_Summary_Route") = DT_Dashboard

        DisplayChart(ChartMain, New EventArgs, Month_F, Month_T, Year_F, Year_T, Equipment, PrintPage)
        rptData.DataSource = DT_Dashboard
        rptData.DataBind()

    End Sub

    Protected Sub DisplayChart(ByVal sender As Object, ByVal e As System.EventArgs, ByVal Month_F As Integer, ByVal Month_T As Integer, ByVal Year_F As Integer, ByVal Year_T As Integer, ByVal Equipment As Integer, ByVal PrintPage As Boolean)
        Dim DisplayText As String = ""
        Dim PlantName As String = ""
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        SQL = "select PLANT_Name from MS_Plant where PLANT_ID = " & lblPlantID.Text
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            PlantName = DT.Rows(0).Item("PLANT_Name").ToString
        End If

        If Year_F < 2500 Then
            Year_F = Year_F + 543
        End If
        If Year_T < 2500 Then
            Year_T = Year_T + 543
        End If

        If Year_F = Year_T Then
            If Month_F = Month_T Then
                DisplayText = "on  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F
            Else
                DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_F
            End If
        Else
            DisplayText = "between  " & Dashboard.FindMonthNameEng(Month_F) & " " & Year_F & " - " & Dashboard.FindMonthNameEng(Month_T) & " " & Year_T
        End If

        Dim F As New System.Drawing.Font("Microsoft Sans Serif", 12, Drawing.FontStyle.Bold)

        ChartMain.Titles("Title1").Font = F
        ChartMain.Titles("Title1").ForeColor = Drawing.Color.Navy
        ChartMain.Titles("Title1").Text = "Total  Problem  for  " & Dashboard.FindEquipmentName(Equipment) & "  Equipement" & vbNewLine & vbNewLine & DisplayText & "  for  " & PlantName & "  #" & lblRouteName.Text
        ChartMain.Titles("Title1").Alignment = Drawing.ContentAlignment.TopCenter

        ChartMain.ChartAreas("ChartArea1").AxisY.TitleFont = F
        ChartMain.ChartAreas("ChartArea1").AxisY.TitleForeColor = Drawing.Color.Navy
        ChartMain.ChartAreas("ChartArea1").AxisY.Title = "Problem(s)"
        ChartMain.ChartAreas("ChartArea1").AxisX.MajorGrid.LineColor = Drawing.Color.Silver
        ChartMain.ChartAreas("ChartArea1").AxisY.MajorGrid.LineColor = Drawing.Color.Silver


        ChartMain.Series("Series1").ChartType = DataVisualization.Charting.SeriesChartType.Column
        ChartMain.Series("Series2").ChartType = DataVisualization.Charting.SeriesChartType.Column
        ChartMain.Series("Series3").ChartType = DataVisualization.Charting.SeriesChartType.Column

        DT = New DataTable
        DT = Session("Dashboard_Summary_Route")

        ChartMain.Series("Series1").Points.Clear()
        ChartMain.Series("Series2").Points.Clear()
        ChartMain.Series("Series3").Points.Clear()

        If DT.Rows.Count > 5 Then
            Dim NewWidth As Unit = Unit.Pixel(77 * DT.Rows.Count)
            ChartMain.Width = NewWidth
        End If

        ChartMain.Series("Series1").Points.Clear()
        ChartMain.Series("Series2").Points.Clear()
        ChartMain.Series("Series3").Points.Clear()

        For i As Integer = 0 To DT.Rows.Count - 1

            Dim Tooltip_ClassC As String = DT.Rows(i).Item("ClassC").ToString
            ChartMain.Series("Series1").Points.AddXY("Round " & DT.Rows(i).Item("RPT_Round"), DT.Rows(i).Item("ClassC"))
            ChartMain.Series("Series1").Points(i).ToolTip = Tooltip_ClassC

            Dim Tooltip_ClassB As String = DT.Rows(i).Item("ClassB").ToString
            ChartMain.Series("Series2").Points.AddXY("Round " & DT.Rows(i).Item("RPT_Round"), DT.Rows(i).Item("ClassB"))
            ChartMain.Series("Series2").Points(i).ToolTip = Tooltip_ClassB

            Dim Tooltip_ClassA As String = DT.Rows(i).Item("ClassA").ToString
            ChartMain.Series("Series3").Points.AddXY("Round " & DT.Rows(i).Item("RPT_Round"), DT.Rows(i).Item("ClassA"))
            ChartMain.Series("Series3").Points(i).ToolTip = Tooltip_ClassA

        Next

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblRound As Label = e.Item.FindControl("lblRound")
        Dim lblClassC As Label = e.Item.FindControl("lblClassC")
        Dim lblClassB As Label = e.Item.FindControl("lblClassB")
        Dim lblClassA As Label = e.Item.FindControl("lblClassA")

        lblRound.Text = "Round " & e.Item.DataItem("RPT_Round").ToString

        Dim ClassC As Integer = 0
        Dim ClassB As Integer = 0
        Dim ClassA As Integer = 0

        If Not IsDBNull(e.Item.DataItem("ClassC")) AndAlso e.Item.DataItem("ClassC") <> 0 Then
            ClassC = e.Item.DataItem("ClassC")
            lblClassC.Text = FormatNumber(ClassC, 0)
        Else
            lblClassC.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassB")) AndAlso e.Item.DataItem("ClassB") <> 0 Then
            ClassB = e.Item.DataItem("ClassB")
            lblClassB.Text = FormatNumber(ClassB, 0)
        Else
            lblClassB.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("ClassA")) AndAlso e.Item.DataItem("ClassA") <> 0 Then
            ClassA = e.Item.DataItem("ClassA")
            lblClassA.Text = FormatNumber(ClassA, 0)
        Else
            lblClassA.Text = "-"
        End If

    End Sub

End Class