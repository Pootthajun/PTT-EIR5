﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="LAW_Document_Plan.aspx.vb" Inherits="EIR.LAW_Document_Plan" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="UC_LawShowProcess.ascx" tagname="UC_LawShowProcess" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
    <h2>Document Master Plan</h2>
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
                <div class="content-box-header">
                    <h3>Display condition</h3>


                    <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                        ID="ddlSearchDocTemplete" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                        ID="ddlSearchYear" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                        ID="ddlSearchPlant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                        ID="ddlSearchTag" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <div class="clear"></div>
                </div>
                  
                <div class="content-box-content">
                    
                     <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  
                   <asp:Panel ID="pnlListDocument" runat="server">
                  
                  <!-- This is the target div. id must match the href of this div's tab -->
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">Document Name</a></th>
                        <th><a href="#">Year</a></th>
                        <th><a href="#">Plant</a></th>
                        <th><a href="#">Tag No</a></th>
                        <th><a href="#">Status</a></th>
                        <th><a href="#">%</a></th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>
                    <asp:Repeater ID="rptPlan" runat="server">
                        <HeaderTemplate>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><asp:Label ID="lblDocumentName" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblDocumentYear" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblPlantCode" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblTagNo" runat="server"  Style="color :blue ;"></asp:Label></td>
                                <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblPercentComplete" runat="server"></asp:Label></td>
                                    
                                <td><!-- Icons -->
                                    <asp:Label ID="lblDocumentPlanID" runat="server" Visible="false" ></asp:Label>
                                    
                                    <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                    <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                    <cc1:ConfirmButtonExtender ID="cfbToggle" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete this plan permanently?" />
                                </td>
                            </tr>
                            <tr id="trRowProcess" runat="server" visible="false"  >
                                <td colspan="6">
                                    <uc2:UC_LawShowProcess ID="UC_LawShowProcess1" runat="server" />
                                </td>
                            </tr>
                        </ItemTemplate>
                                  
                        <FooterTemplate>
                            </tbody>
                        </FooterTemplate>
                    </asp:Repeater>
                    <tfoot>
                      <tr>
                        <td colspan="6">
                            <div class="bulk-actions align-left">                             
                                <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                            </div>
                            <uc1:PageNavigation ID="Navigation" runat="server"  />
                              <!-- End .pagination -->
                              <div class="clear"></div>  
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                 
                   </asp:Panel>
                   
                   <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
                   
                <asp:Panel ID="pnlEdit" runat="server">
                    <div class="content-box-header">
                        <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Plan</h3>
                        <div class="clear"></div>
                        <asp:Label ID="lblDocumentPlanID" runat="server" Text="0" Visible="false" ></asp:Label>
                    </div>
                   
                    <fieldset>
                        <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                        <p>&nbsp;</p>

					    <p>
                            <label class="column-left" style="width:200px;" >Document Template : </label>
                            <asp:DropDownList CssClass="select"
                                 ID="ddl_Edit_Document_Template" runat="server" AutoPostBack="True" Width="400px" >
                            </asp:DropDownList>
                        </p>
					    <p>
                            <label class="column-left" style="width:200px;" >Document Name : </label>
                            <asp:TextBox ID="txtDocumentName" runat="server" CssClass="text-input small-input " MaxLength="255" Width="400px" ></asp:TextBox>
                        </p>
                       <%-- <p>
                            <label class="column-left" style="width:200px;" >Year : </label>
                            <asp:TextBox ID="txtDocumentYear" runat="server" CssClass="text-input small-input"  MaxLength="4" Width="100px" ></asp:TextBox>
                        </p>--%>
                        <p>
                            <label class="column-left" style="width:200px;" >Year : </label>
                            <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                        ID="ddlYear" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                        </p>
                        <p>
                            <label class="column-left" style="width:200px;" >Plant : </label>
                            <asp:DropDownList CssClass="select"
                                 ID="ddlPlantID" runat="server" AutoPostBack="True" Width="400px" >
                            </asp:DropDownList>
                        </p>
                        <p>
                            <label class="column-left" style="width:200px;" >Tag No : </label>
                            <asp:DropDownList CssClass="select"
                                 ID="ddlPlantArea" runat="server"  Width="120px" AutoPostBack="true" >
                            </asp:DropDownList>
                            <asp:DropDownList CssClass="select"
                                 ID="ddlProcess" runat="server"  Width="120px" AutoPostBack="true" >
                            </asp:DropDownList>
                            <asp:DropDownList CssClass="select"
                                 ID="ddlTagID" runat="server"  Width="160px" >
                            </asp:DropDownList>
                        </p>
                        <p>
                            <label class="column-left" style="width:200px;" >Paper List : </label>
                        </p>
                        <table cellpadding="0" cellspacing="0" class="propertyTable">
                            <asp:Repeater ID="rptPaperList" runat="server">
                                <HeaderTemplate>
                                    <thead>
                                        <tr>
                                            <th rowspan="2" style="width:5%;height:24px;text-align:center;" class="propertyCaption" >No.</th>
                                            <th rowspan="2" style="text-align:center;" class="propertyCaption">Paper Name</th>
                                            <th rowspan="2" style="text-align:center;" class="propertyCaption">Organize</th>
                                            <th rowspan="2" style="width:5%;text-align:center;" class="propertyCaption">%</th>
                                            <th style="text-align:center;" colspan="2" class="propertyCaption">Plan Upload Date</th>
                                        </tr>
                                        <tr>
                                            <th style="width:3%;text-align:center;" >Notice Date</th>
                                            <th style="width:3%;text-align:center;" >Critical Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align:center;" >
                                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        </td>
                                        <td >
                                            <asp:Label ID="lbl_Paper_Name" runat="server"></asp:Label>
                                            <asp:Label ID="lblDocumentSettingPaperID" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblOrgName" runat="server"></asp:Label>
                                        </td>
                                        <td style="text-align:center;">
                                            <asp:TextBox runat="server" ID="txtPercentComplete" CssClass="text-input small-input " MaxLength="20" ></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtPlanNoticeDate" CssClass="text-input small-input " MaxLength="20" ></asp:TextBox>
				                             <cc1:CalendarExtender ID="txtPlanNoticeDate_CalendarExtender" runat="server" 
                                                 Format="dd-MMM-yyyy" TargetControlID="txtPlanNoticeDate" PopupPosition="Right" >
                                             </cc1:CalendarExtender>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtPlanCriticalDate" CssClass="text-input small-input " MaxLength="20" ></asp:TextBox>
				                             <cc1:CalendarExtender ID="txtPlanCriticalDate_CalendarExtender" runat="server" 
                                                 Format="dd-MMM-yyyy" TargetControlID="txtPlanCriticalDate" PopupPosition="Right" >
                                             </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                        </table>
				        <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				            <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                            <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                        </asp:Panel>
                        <p align="right">
                          <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                          <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                        </p>
                    </fieldset>

                </asp:Panel>
                    
                </div>
                <!-- End #tab1 -->
                
    </div>
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>