﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Dashboard_Annual_Progress.ascx.vb" Inherits="EIR.UC_Dashboard_Annual_Progress" %>

<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
    <tr>
        <td style="vertical-align:top; width:700px;">
           <asp:Chart ID="ChartMain" runat="server" Height="400px" BackColor="" CssClass="ChartHighligh" Width="700px"  >
            <legends>
                <asp:Legend Name="Legend1" LegendStyle="Row" 
                    Docking="Bottom"></asp:Legend>
            </legends>
            <titles>
                <asp:Title Name="Title1" font="Microsoft Sans Serif, 12pt, style=Bold" 
                    forecolor="SteelBlue">
            </asp:Title>
            </titles>
            <Series>
                <asp:Series Name="Series1" ChartType="Line" ChartArea="ChartArea1" 
                    Color="Green" BorderWidth="3" Legend="Legend1" LegendText="Plan" ShadowColor="">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Line" ChartArea="ChartArea1" 
                    Color="MediumSlateBlue" BorderWidth="3" Legend="Legend1" LegendText="Actual">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <axisy intervalautomode="VariableCount" IsLabelAutoFit="False" LineColor="Gray" 
                        Maximum="100" TitleForeColor="Gray" Title="Progress %">
                        <majorgrid linecolor="Gainsboro" />
                        <minorgrid enabled="True" interval="5" linecolor="WhiteSmoke" />
                        <majortickmark enabled="False" />
                        <LabelStyle Font="Microsoft Sans Serif, 8.25pt" ForeColor="Gray" 
                            Interval="10" />
                    </axisy>
                    <axisx IsLabelAutoFit="False" 
                        IsMarginVisible="False" LineColor="Gray" IntervalType="Months" 
                        LabelAutoFitStyle="IncreaseFont, DecreaseFont, StaggeredLabels, LabelsAngleStep30, LabelsAngleStep45" 
                        TitleAlignment="Far" TitleForeColor="DimGray">
                        <majorgrid enabled="False" linecolor="DimGray" />
                        <majortickmark enabled="False" />
                        <LabelStyle Font="Microsoft Sans Serif, 6.75pt" ForeColor="Gray" 
                            Interval="Auto" />
                    </axisx>
                    <axisx2 intervalautomode="VariableCount">
                    </axisx2>
                    <axisy2 intervalautomode="VariableCount">
                    </axisy2>
                </asp:ChartArea>
            </ChartAreas>
            </asp:Chart>
        </td>
        <td align="center" style="vertical-align:top; text-align:center; width:100%;"> 
            <center>
                <table border="0" cellpadding="0" cellspacing="0" style="width:270px; border:1px solid #efefef;">
                  <tr>
                      <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:90px; border-bottom:1px solid #003366;">
                          Month</td>
                      <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:90px; border-bottom:1px solid #003366;">
                          Plan (%)</td>
                      <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px; width:90px; border-bottom:1px solid #003366;">
                          Actual (%)</td>
                  </tr>
                  <asp:Repeater ID="rptData" runat="server">
                    <ItemTemplate>
                        <tr id="tbTag" runat="server" style="border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                          <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                              <asp:Label ID="lblMonth" runat="server"></asp:Label></td>
                          <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                              <asp:Label ID="lblPlan" runat="server" ForeColor="Green"></asp:Label></td>
                          <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                              <asp:Label ID="lblProgress" runat="server" ForeColor="Blue"></asp:Label></td>
                        </tr>
                    </ItemTemplate>
                  </asp:Repeater>
                </table> 
            </center>
        </td>
    </tr>
    <tr>
        <td style="font-size:small">
            Plan = วันที่กำหนดเสร็จจากการตั้งแผนการตรวจ
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td style="font-size:small">
            Actual = วันที่ Analyser (ยืนยันผลการตรวจสอบ)</td>
        <td>
        </td>
    </tr>
    <tr style="display:none">
        <td colspan="2">
            <asp:Label ID="lblYear" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblEquipment" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblPlantID" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
        </td>
    </tr>
</table>
