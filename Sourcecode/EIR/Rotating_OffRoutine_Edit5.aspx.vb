﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Rotating_OffRoutine_Edit5
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim C As New Converter
    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Rotating_Off_Routine_Report

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If IsNumeric(ViewState("TAG_ID")) Then
                Return ViewState("TAG_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("TAG_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("Rotating_OffRoutine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_RO_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND RPT_Type_ID=" & RPT_Type_ID, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_RO_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_RO_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_RO_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")

            BindTabData()

        End If

        '-------------- Implement Repeater Command--------
        pnlValidation.Visible = False

    End Sub

    Private Sub SetUserAuthorization()

        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_RO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY
                '.RPT_Type = RPT_Type_ID
                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='Rotating_OffRoutine_Summary.aspx'", True)
            Exit Sub
        End If

    End Sub

    Private Sub BindTabData()
        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_RO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("Rotating_OffRoutine_Summary.aspx")
            Exit Sub
        End If

        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        lbl_Year.Text = RPT_Year

        '------------------ GET TAG ID------------------------------
        SQL = "SELECT TOP 1 TAG_ID FROM RPT_RO_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("Rotating_OffRoutine_Summary.aspx")
            Exit Sub
        End If
        TAG_ID = DT.Rows(0).Item("TAG_ID")
        lbl_TAG.Text = BL.Get_Tag_Code_Rotating(TAG_ID)
        '------------------------------Bind Document -----------------------------------
        rpt_Doc.DataSource = BL.Get_Rotating_OffRoutine_Document(RPT_Year, RPT_No)
        rpt_Doc.DataBind()
    End Sub

    Protected Sub rpt_Doc_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_Doc.ItemCommand
        'If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Select Case e.CommandName
            Case "Add"

                lnkAdd_Click(Nothing, Nothing)

            Case "UploadImage"

                Dim btnRemove As Button = e.Item.FindControl("btnRemove")
                Dim imgPreview As WebControls.Image = e.Item.FindControl("imgPreview")
                Dim btnSaveImage As Button = e.Item.FindControl("btnSaveImage")
                Dim DOC_ID As Integer = e.CommandArgument

                Session("PREVIEW_IMG_" & DOC_ID & "_1") = BL.Get_Rotating_Document_Image(DOC_ID)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowDialogEditImage('" & DOC_ID & "',1,document.getElementById('" & btnSaveImage.ClientID & "'));", True)

            Case "Save"

                Dim txtDocName As TextBox = e.Item.FindControl("txtDocName")
                Dim chkText As CheckBox = e.Item.FindControl("chkText")
                Dim txtRecomment As TextBox = e.Item.FindControl("txtRecomment")
                Dim btnUpdate As Button = e.Item.FindControl("btnUpdate")

                Dim SQL As String = "SELECT * FROM RO_Doc WHERE DOC_ID=" & e.CommandArgument
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    DT.Rows(0).Item("DOC_Name") = txtDocName.Text
                    DT.Rows(0).Item("DOC_Recomment") = txtRecomment.Text
                    DT.Rows(0).Item("ShowRecomment") = chkText.Checked
                    Dim cmb As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                End If
                ''--------------------- Update Picture --------------
                Dim imgPreview As ImageButton = e.Item.FindControl("imgPreview")
                imgPreview.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & e.CommandArgument & "&Image=1"

            Case "SaveImage"

                Dim imgPreview As WebControls.Image = e.Item.FindControl("imgPreview")
                Dim DOC_ID As Integer = e.CommandArgument

                If IsNothing(Session("TempImage_" & DOC_ID & "_1")) Then Exit Sub
                '------------------------ Save ------------------------
                Dim SQL As String = "SELECT DOC_ID,DOC_Detail,DOC_Align,Content_Type FROM RO_Doc WHERE DOC_ID=" & DOC_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then

                    Dim img As Drawing.Image = Drawing.Image.FromStream(C.ByteToStream(Session("TempImage_" & DOC_ID & "_1")))
                    If img.Width > img.Height Then
                        DT.Rows(0).Item("DOC_Align") = "H"
                    Else
                        DT.Rows(0).Item("DOC_Align") = "V"
                    End If

                    DT.Rows(0).Item("Content_Type") = BL.GetImageContentType(img)
                    DT.Rows(0).Item("DOC_Detail") = Session("TempImage_" & DOC_ID & "_1")

                    Dim cmb As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                End If

                Session("PREVIEW_IMG_" & DOC_ID & "_1") = Session("TempImage_" & DOC_ID & "_1")
                imgPreview.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & DOC_ID & "&Image=1"

            Case "Delete"
                BL.Drop_Rotating_OffRoutine_Document(RPT_Year, RPT_No, e.CommandArgument)
                BindTabData()
        End Select
    End Sub

    Protected Sub rpt_Doc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_Doc.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim lblNo As Label = e.Item.FindControl("lblNo")
                Dim txtDocName As TextBox = e.Item.FindControl("txtDocName")
                Dim chkText As CheckBox = e.Item.FindControl("chkText")
                Dim btnRemove As Button = e.Item.FindControl("btnRemove")
                Dim btnUpdate As Button = e.Item.FindControl("btnUpdate")
                Dim btnSaveImage As Button = e.Item.FindControl("btnSaveImage")
                Dim imgPreview As ImageButton = e.Item.FindControl("imgPreview")
                Dim txtRecomment As TextBox = e.Item.FindControl("txtRecomment")

                lblNo.Text = e.Item.ItemIndex + 1
                txtDocName.Text = e.Item.DataItem("DOC_Name")
                chkText.Checked = CBool(e.Item.DataItem("ShowRecomment"))
                txtRecomment.Text = e.Item.DataItem("DOC_Recomment")

                btnRemove.CommandArgument = e.Item.DataItem("DOC_ID")
                btnUpdate.CommandArgument = e.Item.DataItem("DOC_ID")
                btnSaveImage.CommandArgument = e.Item.DataItem("DOC_ID")
                imgPreview.CommandArgument = e.Item.DataItem("DOC_ID")

                '------------------- Set Image -----------------
                If Not IsDBNull(e.Item.DataItem("DOC_Detail")) Then
                    Session("PREVIEW_IMG_" & e.Item.DataItem("DOC_ID") & "_1") = e.Item.DataItem("DOC_Detail")
                    imgPreview.ImageUrl = "RenderImage.aspx?T=" & Now.ToOADate & "&UNIQUE_ID=" & e.Item.DataItem("DOC_ID") & "&Image=1"
                    Dim img As Drawing.Image = Drawing.Image.FromStream(C.ByteToStream(e.Item.DataItem("DOC_Detail")))
                    '--------------- Set Alignment -------------
                    If img.Width > img.Height Then
                        imgPreview.Width = Unit.Parse("400px")
                        imgPreview.Height = Unit.Empty
                    Else
                        imgPreview.Width = Unit.Empty
                        imgPreview.Height = Unit.Parse("400px")
                    End If
                Else
                    Session("PREVIEW_IMG_" & e.Item.DataItem("DOC_ID") & "_1") = Nothing
                    imgPreview.ImageUrl = "resources/images/Sample_40.png"
                    imgPreview.Width = Unit.Parse("400px")
                    imgPreview.Height = Unit.Parse("400px")
                End If

                '--------------- Set Auto Update ---------------
                txtRecomment.Attributes("onchange") = "document.getElementById('" & btnUpdate.ClientID & "').click();"

                chkText.Attributes("onchange") = "document.getElementById('" & btnUpdate.ClientID & "').click();"
                txtDocName.Attributes("onchange") = "document.getElementById('" & btnUpdate.ClientID & "').click();"

            Case ListItemType.Footer
                Dim btnAdd As Button = e.Item.FindControl("btnAdd")
                e.Item.Visible = Not IsNothing(rpt_Doc.DataSource) AndAlso CType(rpt_Doc.DataSource, DataTable).Rows.Count > 0
        End Select

    End Sub

#Region "Navigator"

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabDetail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabDetail.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit2.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabPhoto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabPhoto.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabVibration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabVibration.Click, btn_Back.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit4.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click, btn_Next.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Rotating_OffRoutine_Edit6.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

#End Region

    Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdd.Click
        Dim SQL As String = "SELECT * FROM RO_Doc WHERE 1=0"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow = DT.NewRow
        DR("DOC_ID") = BL.Get_New_Rotating_Doc_ID()
        DR("RPT_Year") = RPT_Year
        DR("RPT_No") = RPT_No
        DR("DOC_Detail") = DBNull.Value
        DR("DOC_Name") = ""
        DR("DOC_Recomment") = ""
        DR("Content_Type") = ""
        DR("DOC_Align") = ""
        DR("ShowRecomment") = False
        DR("Update_Time") = Now
        DR("Update_By") = Session("User_ID")
        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        '------------------------------Bind Document -----------------------------------
        rpt_Doc.DataSource = BL.Get_Rotating_OffRoutine_Document(RPT_Year, RPT_No)
        rpt_Doc.DataBind()

        '------------------- Set Focus New Document ------------------------
        For Each Item As RepeaterItem In rpt_Doc.Items
            If Item.ItemType <> ListItemType.AlternatingItem And Item.ItemType <> ListItemType.Item Then Continue For
            If Item.ItemIndex = rpt_Doc.Items.Count - 1 Then
                Dim imgPreview As ImageButton = Item.FindControl("imgPreview")
                Dim txtDocName As TextBox = Item.FindControl("txtDocName")
                Dim Script As String = "window.location.href='#" & imgPreview.ClientID & "';"

                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Focus", Script, True)
            End If

        Next
    End Sub

    Protected Sub btn_Buffer_Refresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Buffer_Refresh.Click
        BindTabData()
    End Sub

    Protected Sub lnkRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRefresh.Click
        BindTabData()
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click
        Try
            BL.Drop_Rotating_OffRoutine_Document(RPT_Year, RPT_No)
        Catch ex As Exception
            Exit Sub
        End Try
        BindTabData()
    End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

End Class