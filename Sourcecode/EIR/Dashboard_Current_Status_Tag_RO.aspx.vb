﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Current_Status_Tag_RO
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL

    Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.Stationary_Routine_Report

    Private Property Tag_ID() As Integer
        Get
            If IsNumeric(ViewState("Tag_ID")) Then
                Return ViewState("Tag_ID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("Tag_ID") = value
        End Set
    End Property

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            Tag_ID = Request.QueryString("Tag_ID")
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")

            Dim Sql As String = ""
            Sql &= "SELECT DISTINCT MS_RO_TAG.TAG_ID,TAG_Name,MS_Area.AREA_CODE+'-'+MS_Process.PROC_CODE +'-' + MS_RO_TAG.TAG_NO TAG_CODE" & vbCrLf
            Sql &= "FROM MS_Area" & vbCrLf
            Sql &= "INNER JOIN MS_RO_TAG ON MS_RO_TAG.AREA_ID=MS_Area.AREA_ID AND MS_RO_TAG.Active_Status=1" & vbCrLf
            Sql &= "INNER JOIN MS_RO_TAG_TYPE ON MS_RO_TAG.TAG_TYPE_ID=MS_RO_TAG_TYPE.TAG_TYPE_ID AND MS_RO_TAG_TYPE.Active_Status=1" & vbCrLf
            Sql &= "LEFT JOIN MS_Process ON MS_RO_TAG.PROC_ID=MS_Process.PROC_ID AND MS_Process.Active_Status=1" & vbCrLf
            Sql &= "WHERE TAG_ID = " & Tag_ID
            Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            lblTagCode.Text = DT.Rows(0).Item("TAG_CODE").ToString
            lblTagName.Text = DT.Rows(0).Item("TAG_Name").ToString

            BindTabData()

            '---------------Hide Dialog-------------------------
            UC_Dashboard_DialogUploadImage.CloseDialog()
        End If

        pnlValidation.Visible = False

    End Sub

    Private Sub BindTabData()

        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_RO_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        lbl_Plant.Text = DT.Rows(0).Item("PLANT_CODE")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_CODE")
        lbl_Year.Text = RPT_Year
        If Not IsDBNull(DT.Rows(0).Item("RPT_Round")) Then
            lbl_Round.Text = DT.Rows(0).Item("RPT_Round")
        Else
            lbl_Round.Text = ""
        End If

        If Not IsDBNull(DT.Rows(0).Item("RPT_Period_Start")) Then
            lbl_Period.Text = BL.ReportGridTime(DT.Rows(0).Item("RPT_Period_Start"))
        Else
            lbl_Period.Text = ""
        End If
        '------------------------------Header -----------------------------------

        SQL = "SELECT *,0 CHK FROM VW_REPORT_RO_DETAIL WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & " AND TAG_ID = " & Tag_ID & " ORDER BY INSP_ID"
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        Dim DT_TEMP As New DataTable
        DT.DefaultView.RowFilter = "INSP_ID = 11"
        DT_TEMP = DT.DefaultView.ToTable
        Select Case DT_TEMP.Rows(0).Item("CURRENT_STATUS_ID").ToString
            Case "10"
                DT.DefaultView.RowFilter = "REF_INSP_ID IS NULL OR CURRENT_LEVEL IS NOT NULL"
                DT = DT.DefaultView.ToTable
                For I As Integer = 0 To DT.Rows.Count - 1
                    DT.Rows(I).Item("CHK") = 1
                Next
            Case Else
                DT.DefaultView.RowFilter = ""
        End Select

        rptInspection.DataSource = DT
        rptInspection.DataBind()

    End Sub

    Protected Sub rptInspection_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptInspection.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnView As ImageButton = e.Item.FindControl("btnView")
        Dim DETAIL_ID As Integer = btnView.Attributes("DETAIL_ID")

        Select Case e.CommandName
            Case "View"
                Dim UNIQUEKEY As String = Now.ToOADate.ToString.Replace(".", "")
                UC_Dashboard_DialogUploadImage.UNIQUE_POPUP_ID = UNIQUEKEY
                UC_Dashboard_DialogUploadImage.TAG_CLASS = EIR_BL.Tag_Class.Rotating
                UC_Dashboard_DialogUploadImage.ShowDialog(DETAIL_ID)
        End Select

    End Sub

    Protected Sub rptInspection_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInspection.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblInspection As Label = e.Item.FindControl("lblInspection")
        Dim lblPart As Label = e.Item.FindControl("lblPart")
        Dim lblLastStatus As Label = e.Item.FindControl("lblLastStatus")
        Dim lblFixed As Label = e.Item.FindControl("lblFixed")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblLevel As Label = e.Item.FindControl("lblLevel")
        Dim lblTrace As Label = e.Item.FindControl("lblTrace")
        Dim lblDetail As Label = e.Item.FindControl("lblDetail")
        Dim TDLevel As HtmlTableCell = e.Item.FindControl("TDLevel")
        Dim btnView As ImageButton = e.Item.FindControl("btnView")

        lblInspection.Text = e.Item.DataItem("INSP_Name").ToString
        lblPart.Text = e.Item.DataItem("CURRENT_COMPONENT").ToString
        lblLastStatus.Text = e.Item.DataItem("LAST_STATUS_Name").ToString
        Select Case e.Item.DataItem("CURRENT_Fixed").ToString
            Case "False"
                lblFixed.Text = "Not fix"
                lblFixed.ForeColor = Drawing.Color.Red
            Case "True"
                lblFixed.Text = "Fixed"
                lblFixed.ForeColor = Drawing.Color.Green
        End Select
        lblStatus.Text = e.Item.DataItem("CURRENT_STATUS_Name").ToString
        lblLevel.Text = e.Item.DataItem("CURRENT_LEVEL_Desc").ToString
        lblDetail.Text = e.Item.DataItem("CURRENT_PROB_DETAIL").ToString

        Dim l As Integer = 0
        Dim c As Integer = 0
        If e.Item.DataItem("LAST_LEVEL").ToString <> "" Then
            l = e.Item.DataItem("LAST_LEVEL")
        End If
        If e.Item.DataItem("CURRENT_LEVEL").ToString <> "" Then
            c = e.Item.DataItem("CURRENT_LEVEL")
        End If

        Select Case e.Item.DataItem("INSP_ID")
            Case 11 '----------------- Machine Running ----------------
                lblTrace.Text = "No problem found"
                lblTrace.ForeColor = Drawing.Color.Gray
                TDLevel.Attributes("Class") = "LevelNormal"
            Case 12 ' Vibration leve
                If l <= 1 And c > 1 Then
                    lblTrace.Text = "New Problem"
                    lblTrace.ForeColor = Drawing.Color.Red
                ElseIf l > 1 And c > 1 Then
                    Select Case e.Item.DataItem("CURRENT_Fixed").ToString
                        Case "True"
                            lblTrace.Text = "Fixed incompletely"
                        Case Else
                            lblTrace.Text = "Still has problem"
                    End Select
                    lblTrace.ForeColor = Drawing.Color.Red
                ElseIf l > 1 And c <= 1 And c >= 0 Then
                    lblTrace.Text = "Problem fixed"
                    lblTrace.ForeColor = Drawing.Color.Green
                ElseIf l > 1 And c < 0 Then
                    lblTrace.Text = "Still has problem"
                    lblTrace.ForeColor = Drawing.Color.Red
                Else
                    lblTrace.Text = "No problem found"
                    lblTrace.ForeColor = Drawing.Color.Silver
                End If

                Select Case e.Item.DataItem("CURRENT_LEVEL")
                    Case 3
                        TDLevel.Attributes("Class") = "LevelZoneD"
                        lblLevel.Text = "Zone D"
                        lblStatus.Attributes("Class") = "TextZoneD"
                        lblLevel.Attributes("Class") = "TextZoneD"
                    Case 2
                        TDLevel.Attributes("Class") = "LevelZoneC"
                        lblLevel.Text = "Zone C"
                        lblStatus.Attributes("Class") = "TextZoneC"
                        lblLevel.Attributes("Class") = "TextZoneC"
                    Case 1
                        TDLevel.Attributes("Class") = "LevelZoneB"
                        lblLevel.Text = "Zone B"
                        lblStatus.Attributes("Class") = "TextZoneB"
                        lblLevel.Attributes("Class") = "TextZoneB"
                    Case 0
                        TDLevel.Attributes("Class") = "LevelZoneA"
                        lblLevel.Text = "Zone A"
                        lblStatus.Attributes("Class") = "TextZoneA"
                        lblLevel.Attributes("Class") = "TextZoneA"
                    Case Else
                        lblLevel.Text = ""
                End Select

            Case Else
                If l > 0 And c = 0 Then
                    lblTrace.Text = "Problem fixed"
                    lblTrace.ForeColor = Drawing.Color.Green
                ElseIf l < 1 And c > 0 Then
                    lblTrace.Text = "New Problem"
                    lblTrace.ForeColor = Drawing.Color.Red
                ElseIf l > 0 And c > 0 Then
                    Select Case e.Item.DataItem("CURRENT_Fixed").ToString
                        Case "True"
                            lblTrace.Text = "Fixed incompletely"
                        Case Else
                            lblTrace.Text = "Still has problem"
                    End Select
                    lblTrace.ForeColor = Drawing.Color.Red
                Else
                    lblTrace.Text = "No problem found"
                    lblTrace.ForeColor = Drawing.Color.Silver
                End If
        End Select

        If e.Item.DataItem("CHK").ToString = "1" Then
            If e.Item.DataItem("REF_INSP_ID").ToString <> "" Then
                lblInspection.Attributes.Add("style", "text-decoration: line-through;")
            End If
        End If

        If e.Item.DataItem("CURRENT_LEVEL_Desc").ToString <> "" And e.Item.DataItem("INSP_ID") <> 12 Then
            TDLevel.Attributes("Class") = "Level" & e.Item.DataItem("CURRENT_LEVEL_Desc").ToString
            lblStatus.Attributes("Class") = "Text" & e.Item.DataItem("CURRENT_LEVEL_Desc").ToString
            lblLevel.Attributes("Class") = "Text" & e.Item.DataItem("CURRENT_LEVEL_Desc").ToString
        End If

        If e.Item.DataItem("LAST_LEVEL_Desc").ToString <> "" Then
            lblLastStatus.Attributes("Class") = "Text" & e.Item.DataItem("LAST_LEVEL_Desc").ToString
        End If

        btnView.Attributes("DETAIL_ID") = e.Item.DataItem("DETAIL_ID")

    End Sub

End Class