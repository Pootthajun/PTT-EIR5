﻿Imports System.Data.SqlClient
Imports System.Globalization

Public Class History_Search_Rotating
    Inherits System.Web.UI.Page


    Dim BL As New EIR_BL
    Dim C As New Converter

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        HideValidator()
        Threading.Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")

        If Not IsPostBack Then
            ClearPanelSearch()

            '------------ Lock Display Date-----------
            Dim SDate As New DateTime(Now.Year, 1, 1)
            txt_Prob_Start.Text = C.DateToString(SDate, txt_Prob_Start_CalendarExtender.Format)
            Dim EndDate As DateTime = Now
            txt_Prob_End.Text = C.DateToString(EndDate, txt_Prob_End_CalendarExtender.Format)
            BindTag()
        End If

    End Sub

#Region "Hide Validator"

    Private Sub HideValidator()
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub

#End Region

#Region "Search Form"

    Private Sub ClearPanelSearch()

        BL.BindDDlPlant(ddl_Plant) : ddl_Plant.Items(0).Text = ""
        BL.BindDDl_RO_Route(0, ddl_Route) : ddl_Route.Items(0).Text = ""
        BL.BindDDlArea(ddl_Area) : ddl_Area.Items(0).Text = ""
        BL.BindDDlProcess(ddl_Process) : ddl_Process.Items(0).Text = ""
        BL.BindDDlTagType("ST", ddl_Type) : ddl_Type.Items(0).Text = ""
        txt_Code.Text = ""
        txt_Desc.Text = ""
        txt_Prob_Start.Text = ""
        txt_Prob_End.Text = ""
        txt_User.Text = ""

        chkOK.Checked = True
        chkClassA.Checked = True
        chkClassB.Checked = True
        chkClassC.Checked = True
        Prob_Status_Changed(Nothing, Nothing)
        BindInspection()
    End Sub

    Private Sub BindInspection()
        Dim DT As DataTable = BL.Get_Rotating_Default_Inspection()
        DT.DefaultView.RowFilter = "INSP_ID<>11"
        rptRotating.DataSource = DT
        rptRotating.DataBind()
    End Sub

    Protected Sub rptINSP_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptRotating.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim chbINSP As CheckBox = e.Item.FindControl("chbINSP")
        Dim lblINSP As Label = e.Item.FindControl("lblINSP")
        chbINSP.Text = ""
        chbINSP.Attributes("INSP_ID") = e.Item.DataItem("INSP_ID")
        lblINSP.Text = e.Item.DataItem("INSP_Name")
    End Sub

    Protected Sub Prob_Status_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOK.CheckedChanged, chkClassA.CheckedChanged, chkClassB.CheckedChanged, chkClassC.CheckedChanged
        Dim ForProblem = chkClassA.Checked Or chkClassB.Checked Or chkClassC.Checked
        pnl_Inspection.Visible = ForProblem
        tdCaptionEvent.Visible = ForProblem
        tdTimeEvent.Visible = ForProblem
    End Sub

    Protected Sub ddl_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Plant.SelectedIndexChanged
        BL.BindDDlArea(ddl_Plant.Items(ddl_Plant.SelectedIndex).Value, ddl_Area, ddl_Area.Items(ddl_Area.SelectedIndex).Value) : ddl_Area.Items(0).Text = ""
        BL.BindDDl_RO_Route(ddl_Plant.Items(ddl_Plant.SelectedIndex).Value, ddl_Route, ddl_Route.Items(ddl_Route.SelectedIndex).Value) : ddl_Route.Items(0).Text = ""
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ClearPanelSearch()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindTag()
    End Sub

    Dim ReportDetail As New DataTable

    Private Sub BindTag()

        DisplaySearchCondition()
        Dim Filter As String = ""

        If ddl_Plant.SelectedIndex > 0 Then
            Filter &= " PLANT_ID=" & ddl_Plant.Items(ddl_Plant.SelectedIndex).Value & " AND "
        End If
        If ddl_Route.SelectedIndex > 0 Then
            Filter &= " ROUTE_ID=" & ddl_Route.Items(ddl_Route.SelectedIndex).Value & " AND "
        End If
        If ddl_Area.SelectedIndex > 0 Then
            Filter &= " AREA_ID=" & ddl_Area.Items(ddl_Area.SelectedIndex).Value & " AND "
        End If
        If ddl_Process.SelectedIndex > 0 Then
            Filter &= " PROC_ID=" & ddl_Process.Items(ddl_Process.SelectedIndex).Value & " AND "
        End If

        If ddl_Type.SelectedIndex > 0 Then
            Filter &= " TAG_TYPE_ID=" & ddl_Type.Items(ddl_Type.SelectedIndex).Value & " AND "
        End If
        If Trim(txt_Code.Text) <> "" Then
            Filter &= " TAG_CODE LIKE '%" & Trim(txt_Code.Text) & "%' AND "
        End If

        If Trim(txt_Desc.Text) <> "" Then
            Filter &= " (" & vbNewLine
            Filter &= "  TAG_Name LIKE '%" & Trim(txt_Desc.Text) & "%' OR" & vbNewLine
            Filter &= "  PROB_Detail LIKE '%" & Trim(txt_Desc.Text) & "%' OR" & vbNewLine
            Filter &= "  PROB_Recomment LIKE '%" & Trim(txt_Desc.Text) & "%' OR" & vbNewLine
            Filter &= " ) AND "
        End If

        Dim INSP_ID As String = ""
        For Each Item As RepeaterItem In rptRotating.Items
            If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim chbINSP As CheckBox = Item.FindControl("chbINSP")
            If chbINSP.Checked Then INSP_ID &= chbINSP.Attributes("INSP_ID") & ","
        Next
        If INSP_ID <> "" Then
            Filter &= " INSP_ID IN (" & INSP_ID.Substring(0, INSP_ID.Length - 1) & ") AND "
        End If

        If txt_Prob_Start.Text <> "" Then
            Dim d As DateTime = C.StringToDate(txt_Prob_Start.Text, txt_Prob_Start_CalendarExtender.Format)
            Filter &= " RPT_Period_End>='" & d.Year & "-" & d.Month.ToString.PadLeft(2, "0") & "-" & d.Day.ToString.PadLeft(2, "0") & "' AND "
        End If
        If txt_Prob_End.Text <> "" Then
            Dim d As DateTime = C.StringToDate(txt_Prob_End.Text, txt_Prob_End_CalendarExtender.Format)
            Filter &= " RPT_Period_Start<='" & d.Year & "-" & d.Month.ToString.PadLeft(2, "0") & "-" & d.Day.ToString.PadLeft(2, "0") & "' AND "
        End If

        Dim _level As String = ""
        If chkOK.Checked Then
            _level &= "0,"
        End If
        If chkClassC.Checked Then
            _level &= "1,"
        End If
        If chkClassB.Checked Then
            _level &= "2,"
        End If
        If chkClassA.Checked Then
            _level &= "3,"
        End If
        If _level <> "" Then
            Filter &= " LEVEL_CODE IN (" & _level.Substring(0, _level.Length - 1) & ") AND "
        End If

        Dim SQL As String = ""
        SQL &= " SELECT TAG_ID,TAG_Code,TAG_Name,ROUTE_ID,AREA_ID,ROUTE_Code,AREA_Code,PLANT_ID,PLANT_Code" & vbNewLine
        SQL &= "       ,TAG_TYPE_ID,TAG_TYPE_Name,RPT_CODE,DETAIL_ID,RPT_Year,RPT_No,PROC_ID,PROC_Code,PROC_Name" & vbNewLine
        SQL &= "       ,INSP_ID,INSP_Name,STATUS_ID,STATUS_Name,Responsible,ISSUE" & vbNewLine
        SQL &= "       ,PROBLEM_DETAIL,PROBLEM_RECOMMENT,LEVEL_CODE,LEVEL_DESC" & vbNewLine
        SQL &= "       ,Officer_Collector,Officer_Inspector,Officer_Engineer,Officer_Analyst" & vbNewLine
        SQL &= "       ,RPT_Period_Start,RPT_Period_End,PICTURE1,PICTURE2" & vbNewLine
        SQL &= "   FROM VW_RO_History" & vbNewLine

        If Filter <> "" Then
            SQL &= " WHERE " & Filter.Substring(0, Filter.Length - 4) & vbNewLine
        End If

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        '------------ Group By TAG -------------
        Dim _col() As String = {"TAG_ID", "TAG_Code", "TAG_Name", "ROUTE_ID", "AREA_ID", "ROUTE_Code", "AREA_Code", "PLANT_ID", "PLANT_Code", "TAG_TYPE_ID", "TAG_TYPE_Name", "PROC_ID", "PROC_Name"}
        DT.DefaultView.Sort = "TAG_Code"
        Dim TAG As DataTable = DT.DefaultView.ToTable(True, _col).Copy
        TAG.DefaultView.RowFilter = ""

        ''------------ List All Report TAG -------------
        'DT.DefaultView.Sort = "RPT_CODE"
        'Dim Report As DataTable = DT.DefaultView.ToTable
        'Report.DefaultView.RowFilter = ""

        If TAG.Rows.Count = 0 Then
            lblCondition.Text &= " ... found nothing"
            lblCondition.ForeColor = Drawing.Color.Red
        Else
            lblCondition.Text &= " ... Found <b style='color:green;'>" & FormatNumber(TAG.Rows.Count, 0) & "</b> tag(s) and <b style='color:red;'>" & FormatNumber(DT.Rows.Count, 0) & "</b> event(s)"
            lblCondition.ForeColor = Drawing.Color.SteelBlue
        End If

        pnlBindingError.Visible = False

        Session("MS_HIS_RO_TAG") = TAG

        Navigation.SesssionSourceName = "MS_HIS_RO_TAG"
        Navigation.RenderLayout()

    End Sub

    Private Sub DisplaySearchCondition()
        Dim Filter As String = ""

        If ddl_Plant.SelectedIndex > 0 Then
            Filter &= " locate at " & ddl_Plant.Items(ddl_Plant.SelectedIndex).Text
        End If

        If ddl_Route.SelectedIndex > 0 Then
            Filter &= " " & ddl_Route.Items(ddl_Route.SelectedIndex).Text
        End If

        If ddl_Area.SelectedIndex > 0 Then
            Filter &= " area " & ddl_Area.Items(ddl_Area.SelectedIndex).Text
        End If

        If ddl_Process.SelectedIndex > 0 Then
            Filter &= " use for process " & ddl_Process.Items(ddl_Process.SelectedIndex).Text
        End If

        If Trim(txt_Code.Text) <> "" Then
            Filter &= " tag-code contained '" & Trim(txt_Code.Text) & "'"
        End If

        If Trim(txt_Desc.Text) <> "" Then
            Filter &= " description contained '" & Trim(txt_Desc.Text) & "'"
        End If

        Dim _Status As String = ""
        If chkOK.Checked Then
            _Status &= "OK, "
        End If
        If chkClassA.Checked Then
            _Status &= "ClassA, "
        End If
        If chkClassB.Checked Then
            _Status &= "ClassB, "
        End If
        If chkClassA.Checked Then
            _Status &= "ClassC, "
        End If
        If _Status <> "" Then
            Filter &= " status : " & _Status.Substring(0, _Status.Length - 2)
        End If

        Dim INSP As String = ""
        If pnl_Inspection.Visible Then
            For Each Item As RepeaterItem In rptRotating.Items
                If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim chbINSP As CheckBox = Item.FindControl("chbINSP")
                Dim lblINSP As Label = Item.FindControl("lblINSP")
                If chbINSP.Checked Then INSP &= lblINSP.Text & ","
            Next
            If INSP <> "" Then
                Filter &= " contained problem for " & INSP.Substring(0, INSP.Length - 1)
            End If
        End If

        If (txt_Prob_Start.Text <> "" Or txt_Prob_End.Text <> "") Then
            Filter &= " problem(s) occured "
            If txt_Prob_Start.Text <> "" And txt_Prob_End.Text <> "" Then
                Filter &= " between " & txt_Prob_Start.Text & " to " & txt_Prob_End.Text
            ElseIf txt_Prob_Start.Text <> "" Then
                Filter &= " after " & txt_Prob_Start.Text
            ElseIf txt_Prob_End.Text <> "" Then
                Filter &= " before " & txt_Prob_End.Text
            End If
        End If

        If txt_User.Text <> "" Then
            Filter &= ", " & txt_User.Text & " has involved reporting."
        End If

        If Filter <> "" Then
            lblCondition.Text = "Display for " & Filter
        Else
            lblCondition.Text = "Display for all tags"
        End If

    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim divTag As HtmlGenericControl = e.Item.FindControl("divTag")
        Dim lblTagCode As Label = e.Item.FindControl("lblTagCode")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblArea As Label = e.Item.FindControl("lblArea")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")

        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblProcess As Label = e.Item.FindControl("lblProcess")

        lblTagCode.Text = e.Item.DataItem("TAG_CODE")
        lblTagName.Text = e.Item.DataItem("TAG_Name")

        Dim Location As String = ""
        If Not IsDBNull(e.Item.DataItem("PLANT_CODE")) Then
            lblPlant.Text = e.Item.DataItem("PLANT_CODE")
        End If
        If Not IsDBNull(e.Item.DataItem("ROUTE_CODE")) Then
            lblRoute.Text = e.Item.DataItem("ROUTE_CODE")
        End If
        If Not IsDBNull(e.Item.DataItem("AREA_CODE")) Then
            lblArea.Text = " Area " & e.Item.DataItem("AREA_CODE")
        End If
        lblType.Text = e.Item.DataItem("TAG_TYPE_Name").ToString
        If Not IsDBNull(e.Item.DataItem("PROC_Name")) Then
            lblProcess.Text &= e.Item.DataItem("PROC_Name")
        End If

        '---------- Tracking Last Status --------------
        divTag.Attributes.Add("onClick", "window.open('GL_Dialog_RO_Tag.aspx?TAG_ID=" & e.Item.DataItem("TAG_ID") & "','GL_Dialog_Tag_" & e.Item.DataItem("TAG_ID") & "','Height=600,Width=1200,scrollbars,resizable,center');")
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

#End Region

End Class