<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="EIR.Login" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <title>PTT-EIR :: Electronic-Inspection-Report</title>
	  	<link rel="icon" type="image/ico" href="resources/images/icons/Logo.ico"></link>
        <link rel="shortcut icon" href="resources/images/icons/Logo.ico"></link>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-874" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <style type="text/css">
            body {
	            background-image: url(resources/images/Login/bg.jpg);
                background-repeat: no-repeat;        
                position: relative;        
                background-size:cover;
                background-position:center center;
                background-attachment: fixed;

	            font-family:Tahoma, Geneva, sans-serif;
	            margin:0px 0px 0px 0px;
            }

            .main_table{
	            background-color:white; 
		
	            /* Rounded Corners 
	            -moz-border-radius: 5px 5px 5px 5px;
	            -webkit-border-radius: 5px 5px 5px 5px;*/
	            margin-top:70px;
	            border-radius:5px 5px 5px 5px;
	            }

            .head_SignIn{
	            padding-top:20px; 
	            font-size:12px;
	            }
	
            .input_container{
		            height:20px; 
		            padding-top:10px;
		            font-size:12px;
		            }

            input[type=text],input[type=password]{
	            height:24px;
	            padding:4px;
	            font-size:16px;
	            cursor:text;
	            }
	
            .flat_login_button{
	            border:1px solid #03F;
	            font-size:14px;
	            color:white;
	            width:100px;
	            height:24px;
	            background-color:#03C;
	            cursor:pointer;
	            }

            .footer{
		            font-size:11px;
		            background-color:#CCC;
		            height:60px;
		            text-align:left;
		            color:gray;
		            padding:0px 0px 20px 20px;
		            vertical-align:bottom;
		            border-radius:0px 0px 5px 5px;
	            }
        </style>
        <%--<script language="javascript" type="text/javascript" src="js/jquery-3.4.1.min.js"></script>--%>

        <%--<script language="javascript" type="text/javascript" src="js/bxSlider/jquery.bxslider.min.js"></script>
        <link href="js/bxSlider/jquery.bxslider.css" rel="stylesheet" />--%>
</head>

<body>
<form id="form2" runat="server">

<%--<asp:ScriptManager ID="ScriptManager1" runat="server" />
		
	  <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>--%>
	  <table width="800" border="0" cellspacing="0" cellpadding="0" class="main_table" align="center" border-collapse="collapse" >
    		  <tr>
    		    <td width="400" rowspan="8" style="padding-left:20px;">
                	<div style="width:400px; ">
                        <li><img src="resources/images/Login/Slide01.png" /></li>
                        <%--<ul class="bxslider">
                          <li><img src="resources/images/Login/Slide01.png" /></li>
                          <li><img src="resources/images/Login/Slide02.png" /></li>
                          <li><img src="resources/images/Login/Slide03.png" /></li>
                          <li><img src="resources/images/Login/Slide04.png" /></li>
                          <li><img src="resources/images/Login/Slide05.png" /></li>
                          <li><img src="resources/images/Login/Slide06.png" /></li>
                        </ul>--%> 
                    </div>            	
                </td>
    		  
  		    </tr>
    		  <tr>
    		    <td width="300" align="left">                    
                        <img src="resources/images/Login/Logo.png" width="260" style="margin-top:20px;" />
                </td>
  		    </tr>
    		  <tr>
    		    <td  class="head_SignIn" style="color:#069;">Signed in with your own PTT' account</td>
  		    </tr>
    		  <tr>
    		    <td class="input_container">
    		        <asp:TextBox ID="txtUserName" runat="server" placeholder="User Name"></asp:TextBox>                
                </td>
  		    </tr>
    		  <tr>
    		    <td  class="input_container">
    		        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>
    		    </td>
  		    </tr>
    		  <tr>
    		    <td  class="input_container"><asp:CheckBox ID="chkRemember" runat="server"/> Keep me signed in</td>
  		    </tr>
            <tr>
    		    <td>&nbsp;</td>
  		    </tr>
            <tr>
    		    <td class="input_container">
    		        <asp:Button CssClass="flat_login_button" Text="Sign in" ID="btnLogin" runat="server" />
                   
    		        <asp:LinkButton ID="lblError" runat="server" style="text-decoration:none; color:#FF6600;"></asp:LinkButton>
    		    </td>
  		    </tr>
    		  <tr>
    		    <td>&nbsp;</td>
  		    </tr>
    		 <tr>
    		    <td colspan="2" class="footer">PTT-EIR V<asp:Label ID="lblVersion" runat="server"></asp:Label> &#8482; Thai Imagination Technology (TIT) 2012-2017. Any problem please <a href="mailto:support@tit-tech.co.th&subject=PTT-EIR>...">contact us. </td>
   		    </tr>
    </table>
<%--      </ContentTemplate>
    </asp:UpdatePanel>--%>

</form>
<%--<script language="javascript" type="text/javascript">
    	$(document).ready(function(){
    	  $('.bxslider').bxSlider({
                    infiniteLoop: true,
                    auto: true,
                    pager: true
                });			
    	});
	document.getElementById('txtUserName').focus();
</script>--%>
</body>
</html>