﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ST_TA_Inspection_Edit4.aspx.vb" Inherits="EIR.ST_TA_Inspection_Edit4" %>

<%@ Register Src="PageNavigation.ascx" TagName="PageNavigation" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="GL_TagInspection.ascx" TagName="GL_TagInspection" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- Page Head -->

    <asp:UpdatePanel ID="udp1" runat="server">
        <ContentTemplate>


            <h2>Create Turnaround & Shutdown Routine Report</h2>
            <asp:TextBox ID="txt_Buffer_RPT_Year" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" Text=""></asp:TextBox>
            <asp:TextBox ID="txt_Buffer_RPT_No" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" Text=""></asp:TextBox>
            <asp:Button ID="btn_Buffer_Refresh" runat="server" Width="0px" Height="0px" Style="visibility: hidden;" />
            <div class="clear"></div>
            <!-- End .clear -->


            <div class="content-box">
                <!-- Start Content Box -->

                <div class="content-box-header">

                    <h3>
                        <asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>


                    <ul class="content-box-tabs">

                        <li>
                            <asp:LinkButton ID="HTabHeader" runat="server">Report Header</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabDetail" runat="server">Report Detail</asp:LinkButton></li>

                        <li>
                            <asp:LinkButton ID="HTabAsFound" runat="server">As Found </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabAfterClean" runat="server">After Clean </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabNDE" runat="server">NDE </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabRepair" runat="server">Repair </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabAfterRepair" runat="server">After Repair </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabFinal" runat="server">Final </asp:LinkButton></li>

                        <li>
                            <asp:LinkButton ID="HTabSummary" runat="server" CssClass="default-tab current">Report Summary</asp:LinkButton></li>
                    </ul>

                    <div class="clear"></div>

                </div>
                <!-- End .content-box-header -->

                <div class="content-box-content">

                    <div class="tab-content current">
                        <fieldset>
                            <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->

                            <p style="font-weight: bold; font-size: 16px;">
                                <label class="column-left" style="width: 120px; font-size: 16px;">Report for: </label>
                                <asp:Label ID="lbl_Plant_Edit" runat="server" Text="Plant" CssClass="EditReportHeader"></asp:Label>
                                > Year :
                                <asp:Label ID="lbl_Year_Edit" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>

                                >
                                    <asp:Label ID="lbl_Equipment_Edit" runat="server" Text="Equipment" CssClass="EditReportHeader"></asp:Label>


                            </p>




                            <h3>
                                <asp:Label ID="Label1" runat="server"></asp:Label></h3>



                            <p style="font-weight: bold; font-size: 16px;">
                                <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon"/><br />
									        Preview report
								        </span>
                                </asp:LinkButton>
                            </p>
                            <div class="clear"></div>
                    </div>
                    <div class="tab-content current">

                        <ul class="shortcut-buttons-set">


                            <li></li>
                        </ul>
                    </div>

                    <%-- <div class="content-box">--%>
                    <%-- <div class="content-box-header">--%>
                    <ul class="content-box-tabs">

                        <li>
                            <asp:LinkButton ID="HTabSummary_List" runat="server">Summary</asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="HTabCheck_sheet" runat="server">Check sheet</asp:LinkButton></li>

                    </ul>
                    <%--</div>--%>
                    <%--</div>--%>

                    <asp:Panel ID="pnlList" runat="server">

                        <div class="content-box-header"  style="height:auto; padding-top:5px; padding-bottom:5px;">
                            <h3>Filter</h3>

                            <asp:DropDownList CssClass="select" Style="position: relative; top: 5px; left: 0px;"
                                ID="ddl_Search_Tag" runat="server" AutoPostBack="True" Visible ="false" >
                            </asp:DropDownList>
                              <asp:TextBox runat="server" ID="txt_Search_Code" AutoPostBack="True" CssClass="text-input small-input " 
                            Width="200px" MaxLength="50"  Style="position: relative; top: 5px;" PlaceHolder="Search for Tag code..."></asp:TextBox>  
                            <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                                ID="ddl_Search_Components" runat="server" AutoPostBack="True">
                            </asp:DropDownList>

                            <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                                ID="ddl_Search_Condition" runat="server" AutoPostBack="True">
                            </asp:DropDownList>

                            <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                                ID="ddl_Search_Class" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                           <asp:DropDownList CssClass="select" Style="position: relative; top: 5px;"
                                ID="ddl_Search_Step" runat="server" AutoPostBack="True">
                               <asp:ListItem Value ="0" Text =""></asp:ListItem>
                               <asp:ListItem Value ="1" Text ="As Found"></asp:ListItem>
                               <asp:ListItem Value ="2" Text ="After Clean"></asp:ListItem>
                               <asp:ListItem Value ="3" Text ="NDE"></asp:ListItem>
                               <asp:ListItem Value ="4" Text ="Repair"></asp:ListItem>
                               <asp:ListItem Value ="5" Text ="After Repair"></asp:ListItem>
                               <asp:ListItem Value ="6" Text ="Final"></asp:ListItem>

                            </asp:DropDownList>
                            <asp:Label ID="lbl1" runat="server" Style="position: relative; top: 5px;" Text="Period"></asp:Label>
                            <asp:TextBox runat="server" ID="txt_Search_Start" AutoPostBack="True"
                                Style="position: relative; top: 5px; left: 0px;"
                                CssClass="text-input small-input " Width="70px" MaxLength="15"></asp:TextBox>
                            <cc1:CalendarExtender ID="txt_Search_Start_CalendarExtender" runat="server"
                                Format="yyyy-MM-dd" TargetControlID="txt_Search_Start">
                            </cc1:CalendarExtender>
                            <asp:Label ID="lbl2" runat="server" Style="position: relative; top: 5px;" Text="to"></asp:Label>
                            <asp:TextBox runat="server" ID="txt_Search_End" Style="position: relative; top: 5px;" AutoPostBack="True"
                                CssClass="text-input small-input " Width="70px" MaxLength="15"></asp:TextBox>
                            <cc1:CalendarExtender ID="txt_Search_End_CalendarExtender" runat="server"
                                TargetControlID="txt_Search_End" Format="yyyy-MM-dd">
                            </cc1:CalendarExtender>
                            &nbsp; &nbsp; 
                          
                  <div class="clear"></div>
                        </div>


                        <table width="900" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                            <thead>
                                <tr>
                                    <th rowspan="2" bgcolor="#EEEEEE" style="text-align: center; vertical-align: middle;">Parts / Components</th>
                                    <th rowspan="2" bgcolor="#EEEEEE" style="text-align: center; vertical-align: middle;">Condition</th>
                                    <th colspan="6" bgcolor="#EEEEEE" style="text-align: center;">Step</th>
                                </tr>
                                <tr>
                                    <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">As Found</th>
                                    <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">After Clean</th>
                                    <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">NDE</th>
                                    <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Repair</th>
                                    <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">After Repair</th>
                                    <th bgcolor="#EEEEEE" style="text-align: center; max-width: 300px;">Final</th>
                                </tr>

                            </thead>
                            <asp:Repeater ID="rptList" runat="server">
                                <ItemTemplate>
                                    <tbody>
                                        <tr id="trTag" runat="server" style="background-color:darkblue;">
                                            <td colspan="8">
                                                <asp:Label ID="lblTag" runat="server" Style="font-weight: bold; color: white ;  "></asp:Label></td>
                                        </tr>

                                        <tr>
                                            <td id="tdComponents" runat="server">
                                                <asp:Label ID="lblComponents" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                            <td id="tdStatus" runat="server">
                                                <asp:Label ID="lblStatus" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>

                                            <td id="td1" runat="server">
                                                <asp:Label ID="lblRPT_Date_AsFound" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                            <td id="td2" runat="server">
                                                <asp:Label ID="lblRPT_Date_AfterClean" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                            <td id="td3" runat="server">
                                                <asp:Label ID="lblRPT_Date_NDE" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                            <td id="td4" runat="server">
                                                <asp:Label ID="lblRPT_Date_Repair" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                            <td id="td5" runat="server">
                                                <asp:Label ID="lblRPT_Date_AfterRepair" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                            <td id="Final" runat="server">
                                                <asp:Label ID="lblRPT_Date_Final" runat="server" Style="font-weight: bold; color: gray;"></asp:Label></td>
                                        </tr>

                                    </tbody>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </asp:Panel>


                    <asp:Panel ID="pnlCheckList" runat="server">


                        <%--<p>
                            <h4>Tag :
                                <asp:DropDownList CssClass="select" ID="ddl_Tag" runat="server" AutoPostBack="True"></asp:DropDownList></h4>
                        </p>--%>


                        <div class="content-box-header">
                            <h3>Tag</h3>
                           
                             <asp:DropDownList CssClass="select" style="position:relative; top:5px; left: 0px;" 
                          ID="ddl_Tag" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                  <div class="clear"></div>
                        </div>

                        <asp:Repeater ID="rptInsp" runat="server">
                            <HeaderTemplate>
                                <table>
                                    <thead>
                                        <tr>
                                            <th id="thTotalCol" runat="server" style="height: 24px;"></th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2">No</th>

                                            <th rowspan="2">Check List</th>
                                            <th id="thStatusCol" runat="server" style="text-align: center;">Condition </th>
                                        </tr>

                                        <tr>
                                            <asp:Repeater ID="rptStatus" runat="server">
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label ID="lblStatus" runat="server" Font-Size="12px" Font-Bold="false"></asp:Label>
                                                    </th>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <th>Remark(s) </th>
                                        </tr>

                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label></td>

                                    <td>
                                        <asp:Label ID="lblInspItem" runat="server"></asp:Label>
                                        <asp:Label ID="lblTo_Table" runat="server" Visible="false"></asp:Label>
                                    </td>
                                    <asp:Repeater ID="rptInspItem" runat="server">
                                        <ItemTemplate>
                                            <td>
                                                <asp:CheckBox ID="chkUse" runat="server" Enabled="false" /></td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <td>
                                        <asp:TextBox ID="txtRemark" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>

                    </asp:Panel>







                    </fieldset>
                </div>
                <!-- End #tabDetail -->

                <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close"
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>


            </div>
            <!-- End .content-box-content -->

            </div>
            <!-- End .content-box -->


        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
