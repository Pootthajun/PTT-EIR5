﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_ImageEditor.ascx.vb" Inherits="EIR.UC_ImageEditor" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>


l
        <!-- jQuery UI -->
        <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/lib/jquery.ui.core.1.10.3.min.js")%>" ></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/lib/jquery.ui.widget.1.10.3.min.js")%>" ></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/lib/jquery.ui.mouse.1.10.3.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/lib/jquery.ui.draggable.1.10.3.min.js")%>"></script>
      
        <!-- wColorPicker -->
        <link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/wPaintImageEditor/lib/wColorPicker.min.css")%>" />
        <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/lib/wColorPicker.min.js")%>"></script>

        <!-- wPaint -->
        <link rel="Stylesheet" type="text/css" href="<%=ResolveUrl("~/wPaintImageEditor/wPaint.min.css")%>" />
        <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/wPaint.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/plugins/main/wPaint.menu.main.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/plugins/text/wPaint.menu.text.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/plugins/shapes/wPaint.menu.main.shapes.min.js")%>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/wPaintImageEditor/plugins/file/wPaint.menu.main.file.min.js")%>"></script>


<div class="MaskDialog"></div>
<asp:Panel ID="pnlDialog" runat="server" CssClass="Dialog_Picture">
    <h2 id="plugin-name">Image Editor</h2>

        Upload <asp:FileUpload ID="FUL_Image" runat="server" style=" width:400px; height:22px; border:none; background-color:White;" />
        <asp:Button ID="btnUpload" runat="server"  Text="Upload" CssClass="button" style="position:relative; top:2px;" />
        <div id="wPaint" style="position:relative; width:400px; height:400px; background-color:#7a7a7a; margin:70px auto 20px auto;">
            <img id="wPaintImg" runat="server" src="" />
        </div>
  
        <asp:Button ID="btnSaveImage" runat="server" style="display:none" />
        <asp:Label ID="lblValidation" runat="server" Text=""  Font-Bold="true" ForeColor="Red"></asp:Label> 
        <script type="text/javascript">
            $('#wPaint').wPaint('menuOrientation', $('#wPaint').wPaint('menuOrientation') === 'vertical' ? 'horizontal' : 'vertical');

        function saveImg(image) {
            var _this = this;

            var formData = new FormData();
            formData.append("action", "SaveImage");
            formData.append("filecontent", image);

            $.ajax({
            type: 'POST',
            url: 'RenderImageEditor.aspx',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (resp) {
                if (resp != "") {
                    $("#<%=btnSaveImage.ClientID %>").click();
                } else {

                }

                //// internal function for displaying status messages in the canvas
                //_this._displayStatus('Image saved successfully');

                //// doesn't have to be json, can be anything
                //// returned from server after upload as long
                //// as it contains the path to the image url
                //// or a base64 encoded png, either will work
                //resp = $.parseJSON(resp);

                //// update images array / object or whatever
                //// is being used to keep track of the images
                //// can store path or base64 here (but path is better since it's much smaller)
                //images.push(resp.img);

                //// do something with the image
                //$('#wPaint-img').attr('src', image);
            }
            });
        }

        //function loadImgBg () {

        //  // internal function for displaying background images modal
        //  // where images is an array of images (base64 or url path)
        //  // NOTE: that if you can't see the bg image changing it's probably
        //  // becasue the foregroud image is not transparent.
        //  this._showFileModal('bg', images);
        //}

        //function loadImgFg () {

        //  // internal function for displaying foreground images modal
        //  // where images is an array of images (base64 or url path)
        //  this._showFileModal('fg', images);
        //}

        // init wPaint
        $('#wPaint').wPaint({
            menuOffsetLeft: -35,
            menuOffsetTop: -50,
            saveImg: saveImg//,
            //loadImgBg: loadImgBg,
            //loadImgFg: loadImgFg
        });
        </script>
</asp:Panel>