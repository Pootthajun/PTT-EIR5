﻿Imports System.Data
Imports System.Data.SqlClient
Imports EIR

Public Class LAW_Organization_Setting
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL
    Dim CL As New LawClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetOrg(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindOrg()
        Dim dt As DataTable = CL.GetListLawOrg
        Session("MS_Organize") = dt

        Navigation.SesssionSourceName = "MS_Organize"
        Navigation.RenderLayout()
    End Sub

    Private Sub Navigation_PageChanging(Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptOrg
    End Sub

    Private Sub rptOrg_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptOrg.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblOrgName As Label = e.Item.FindControl("lblOrgName")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblOrgName.Text = e.Item.DataItem("org_name")
        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If
        btnEdit.Attributes("Law_Organize_ID") = e.Item.DataItem("law_organize_id")
    End Sub

    Private Sub rptOrg_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptOrg.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim LawOrgID As Integer = btnEdit.Attributes("Law_Organize_ID")
        Select Case e.CommandName
            Case "Edit"
                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                '------------------------------------
                pnlListPlant.Enabled = False

                ''--------------Bind Value------------
                Dim dt As DataTable = CL.GetDataLawOrganize(LawOrgID)
                If dt.Rows.Count > 0 Then
                    txtOrgName.Attributes("OrgID") = dt.Rows(0).Item("law_organize_id")
                    txtOrgName.Text = dt.Rows(0).Item("org_Name")
                    chkAvailable.Checked = dt.Rows(0).Item("Active_Status")
                    lblValidation.Text = ""

                    btnSave.Focus()
                End If
            Case "ToggleStatus"

                Dim ret As String = CL.UpdateLawORGStatus(LawOrgID)
                If ret.ToLower = "true" Then
                    BindOrg()

                    lblBindingSuccess.Text = "Change status successfully"
                    pnlBindingSuccess.Visible = True
                Else
                    lblBindingError.Text = ret
                    pnlBindingError.Visible = True
                End If


            Case "Delete"


                '----เชคการใช้งานในระบบ
                Dim sql As String = "SELECT * FROM LAW_Document_Setting_Paper where law_organize_id=" & LawOrgID
                Dim p_Select(1) As SqlParameter
                Dim DT_Select As DataTable = BL.Execute_DataTable(sql)
                If DT_Select.Rows.Count > 0 Then
                    lblBindingError.Text = "Can not delete : Use in report "
                    pnlBindingError.Visible = True

                Else

                    Dim ret As String = CL.DeleteLawOrganize(LawOrgID)
                    If ret.ToLower = "true" Then
                        BindOrg()

                        lblBindingSuccess.Text = "Delete successfully"
                        pnlBindingSuccess.Visible = True
                    Else
                        lblBindingError.Text = ret
                        pnlBindingError.Visible = True
                    End If


                    'If ret.ToLower = "true" Then
                    '    BindOrg()

                    '    lblBindingSuccess.Text = "Change status successfully"
                    '    pnlBindingSuccess.Visible = True
                    'Else
                    '    lblBindingError.Text = ret
                    '    pnlBindingError.Visible = True
                    'End If
                End If

        End Select
    End Sub

    Protected Sub ResetOrg(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindOrg()
        '-----------------------------------
        ClearPanelEdit()
        '-----------------------------------
        pnlListPlant.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtOrgName.Text = ""
        txtOrgName.Attributes("OrgID") = "0"
        chkAvailable.Checked = True

        btnCreate.Visible = True
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True

        btnCreate.Visible = False
        txtOrgName.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListPlant.Enabled = False
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtOrgName.Text = "" Then
            lblValidation.Text = "Please insert Organization Name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim LawOrgID As Integer = txtOrgName.Attributes("OrgID")

        Dim ret As String = CL.CheckDuplicateLawOrgName(LawOrgID, txtOrgName.Text)
        If ret.ToLower <> "false" Then
            lblValidation.Text = ret
            pnlValidation.Visible = True
            Exit Sub
        End If

        ret = CL.SaveLawOrganization(LawOrgID, txtOrgName.Text, chkAvailable.Checked, Session("USER_ID"))
        Dim tmp() As String = Split(ret, "|")
        If tmp.Length = 2 Then
            If tmp(0).ToLower = "true" Then
                ResetOrg(Nothing, Nothing)
                lblBindingSuccess.Text = "Save successfully"
                pnlBindingSuccess.Visible = True
            Else
                lblValidation.Text = ret
                pnlValidation.Visible = True
            End If
        Else
            lblValidation.Text = ret
            pnlValidation.Visible = True
        End If





    End Sub
End Class