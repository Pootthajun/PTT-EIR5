﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Public Class THM_Routine_Edit2
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim CV As New Converter

    'Dim RPT_Type_ID As EIR_BL.Report_Type = EIR_BL.Report_Type.THM_Report
    Public Picture_Path As String = ConfigurationManager.AppSettings("Picture_Path").ToString

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property

    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property

    Public Property UNIQUE_ID() As String
        Get
            Return ViewState("UNIQUE_ID")
        End Get
        Set(ByVal value As String)
            ViewState("UNIQUE_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            UNIQUE_ID = Now.ToOADate.ToString.Replace(".", "")
            '--------------Check Initialize Report--------------
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")
            If RPT_Year = 0 Or RPT_No = 0 Then
                Response.Redirect("THM_Routine_Summary.aspx", True)
                Exit Sub
            Else
                Dim DA As New SqlDataAdapter("SELECT * FROM RPT_THM_Header WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Unavailable", "alert('Unable to gather report detail'); window.location.href='THM_Routine_Summary.aspx'", True)
                    Exit Sub
                End If
            End If

            '--------------UPDATE RESPONSIBLE PERSON------------
            Dim COM As New SqlCommand
            Dim Conn As New SqlConnection(BL.ConnStr)
            Conn.Open()
            COM.Connection = Conn
            COM.CommandType = CommandType.Text
            Dim SQL As String = ""
            Select Case USER_LEVEL
                Case EIR_BL.User_Level.Collector
                    SQL = "UPDATE RPT_THM_Header SET RPT_COL_Date=GETDATE(),RPT_COL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Inspector
                    SQL = "UPDATE RPT_THM_Header SET RPT_INSP_Date=GETDATE(),RPT_INSP_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
                Case EIR_BL.User_Level.Approver
                    SQL = "UPDATE RPT_THM_Header SET RPT_ANL_Date=GETDATE(),RPT_ANL_By=" & Session("USER_ID")
                    SQL &= " WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
                    COM.CommandText = SQL
                    COM.ExecuteNonQuery()
            End Select
            COM.Dispose()
            Conn.Close()
            Conn.Dispose()

            '--------------Check Permisson----------------------
            SetUserAuthorization()

            '--------------Init Layout -------------------------
            lblReportCode.Text = "INSP-E-" & RPT_Year.ToString.Substring(2) & "-" & RPT_No.ToString.PadLeft(4, "0")
            BindGrid()
        End If

        pnlValidation.Visible = False



    End Sub

    Private Sub SetUserAuthorization()
        '----------------------- Check Permission First------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_THM_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim RPT_STEP As EIR_BL.Report_Step
        Dim RPT_LOCK_BY As Integer = -1

        If DT.Rows.Count > 0 Then

            If Not IsDBNull(DT.Rows(0).Item("RPT_STEP")) Then
                RPT_STEP = DT.Rows(0).Item("RPT_STEP")
            Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='THM_Routine_Summary.aspx'", True)
                Exit Sub
            End If

            If Not IsDBNull(DT.Rows(0).Item("RPT_LOCK_BY")) Then
                RPT_LOCK_BY = DT.Rows(0).Item("RPT_LOCK_BY")
            End If

            With BL.ReportPermissionManager
                .RPT_STEP = RPT_STEP
                .USER_LEVEL = USER_LEVEL
                .USER_ID = Session("USER_ID")
                .RPT_LOCK_BY = RPT_LOCK_BY

                If Not .CanEdit Then
                    Dim Msg As String = "This report cannot be edited due to follow these reasonn\n" & vbNewLine
                    Msg &= "-Permission exception due to conflict report step\n" & vbNewLine
                    Msg &= "-This report has been locked by others"
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('" & Msg & "!'); window.location.href='THM_Routine_Summary.aspx'", True)
                    Exit Sub
                End If
            End With

        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Permission", "alert('Unable to gather report detail\nPlease contact administrator!'); window.location.href='THM_Routine_Summary.aspx'", True)
            Exit Sub
        End If

    End Sub

    Private Sub BindGrid()
        '------------------------------Header -----------------------------------
        Dim SQL As String = "SELECT * FROM VW_REPORT_THM_HEADER WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('This report has been removed.');", True)
            Response.Redirect("THM_Routine_Summary.aspx")
            Exit Sub
        End If

        '------------------------------Header -----------------------------------
        lbl_Type.Text = DT.Rows(0).Item("THM_TYPE_Name")
        lbl_Plant.Text = DT.Rows(0).Item("PLANT_Code")
        lbl_Route.Text = DT.Rows(0).Item("ROUTE_Code")
        lbl_Year.Text = RPT_Year
        If Not IsDBNull(DT.Rows(0).Item("RPT_Round")) Then
            lbl_Round.Text = DT.Rows(0).Item("RPT_Round")
        Else
            lbl_Round.Text = ""
        End If
        If Not IsDBNull(DT.Rows(0).Item("RPT_Period_Start")) Then
            lbl_Period.Text = BL.ReportGridTime(DT.Rows(0).Item("RPT_Period_Start"))
        Else
            lbl_Period.Text = ""
        End If
        If Not IsDBNull(DT.Rows(0).Item("Inspec_Date")) Then
            txtInspectedDate.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("Inspec_Date"))
        Else
            txtInspectedDate.Text = ""
        End If

        SQL = "SELECT * FROM RPT_THM_Detail WHERE RPT_Year=" & RPT_Year & " AND RPT_No=" & RPT_No & vbLf
        SQL &= "ORDER BY TAG_Code"
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        rptTag.DataSource = DT
        rptTag.DataBind()
    End Sub

    Protected Sub rptTAG_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblDetailID As Label = e.Item.FindControl("lblDetailID")
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblTagCode As Label = e.Item.FindControl("lblTagCode")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim aView As HtmlAnchor = e.Item.FindControl("aView")

        Dim btnNA As Button = e.Item.FindControl("btnNA")
        Dim btnOK As Button = e.Item.FindControl("btnOK")
        Dim btnAB As Button = e.Item.FindControl("btnAB")

        Dim aUpload As HtmlAnchor = e.Item.FindControl("aUpload")
        Dim btnDel As ImageButton = e.Item.FindControl("btnDel")

        Dim ful As FileUpload = e.Item.FindControl("ful")
        Dim btnBrowse As Button = e.Item.FindControl("btnBrowse")

        lblNo.Text = e.Item.ItemIndex + 1
        lblDetailID.Text = e.Item.DataItem("DETAIL_ID").ToString
        lblTagCode.Text = e.Item.DataItem("TAG_CODE").ToString
        lblTagName.Text = e.Item.DataItem("TAG_NAME").ToString
        aView.InnerHtml = e.Item.DataItem("Result_FileName").ToString

        btnDel.Visible = aView.InnerHtml.Trim <> ""

        If IsDBNull(e.Item.DataItem("TAG_STATUS")) Then
            btnNA.CssClass = "btnNA"
        ElseIf e.Item.DataItem("TAG_STATUS") = 0 Then
            btnOK.CssClass = "btnOK"
        Else
            btnAB.CssClass = "btnAB"
        End If

        Dim pathfile As String = "Render_THM_File.aspx?Mode=TAG&File=" & e.Item.DataItem("Result_FileName").ToString
        aView.HRef = pathfile
        aUpload.Attributes("onClick") = "$('#" & ful.ClientID & "').click();"
        ful.Attributes("onchange") = "$('#" & btnBrowse.ClientID & "').click();"

    End Sub

    Protected Sub rptTAG_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblDetailID As Label = e.Item.FindControl("lblDetailID")
        Dim btnNA As Button = e.Item.FindControl("btnNA")
        Dim btnOK As Button = e.Item.FindControl("btnOK")
        Dim btnAB As Button = e.Item.FindControl("btnAB")
        Dim ful As FileUpload = e.Item.FindControl("ful")
        Dim aView As HtmlAnchor = e.Item.FindControl("aView")
        Dim btnDel As ImageButton = e.Item.FindControl("btnDel")

        Select Case e.CommandName
            Case "NA"

                btnNA.CssClass = "btnNA"
                btnOK.CssClass = "btnDeselect"
                btnAB.CssClass = "btnDeselect"

                Dim Sql As String = ""
                Sql = "select * from RPT_THM_Detail WHERE Detail_ID = " & lblDetailID.Text
                Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                DT.Rows(0).Item("TAG_STATUS") = DBNull.Value
                Dim cmb As New SqlCommandBuilder(DA)
                DA.Update(DT)

            Case "OK"

                btnNA.CssClass = "btnDeselect"
                btnOK.CssClass = "btnOK"
                btnAB.CssClass = "btnDeselect"

                Dim Sql As String = ""
                Sql = "select * from RPT_THM_Detail WHERE Detail_ID = " & lblDetailID.Text
                Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                DT.Rows(0).Item("TAG_STATUS") = 0
                Dim cmb As New SqlCommandBuilder(DA)
                DA.Update(DT)

            Case "AB"

                btnNA.CssClass = "btnDeselect"
                btnOK.CssClass = "btnDeselect"
                btnAB.CssClass = "btnAB"

                Dim Sql As String = ""
                Sql = "select * from RPT_THM_Detail WHERE Detail_ID = " & lblDetailID.Text
                Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                DT.Rows(0).Item("TAG_STATUS") = 1
                Dim cmb As New SqlCommandBuilder(DA)
                DA.Update(DT)

            Case "Upload"
                If Not ful.HasFile Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select file to update');", True)
                    Exit Sub
                End If

                Dim Extension As String = ModuleGlobal.OriginalFileType(ful.PostedFile.FileName)
                If ful.PostedFile.ContentType.ToUpper.IndexOf("PDF") > 0 Or Extension.ToUpper = "PDF" Then
                    Extension = ".pdf"
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Only PDF file supported');", True)
                    Exit Sub
                End If

                Dim c As New Converter
                BL.Save_THM_File(lblReportCode.Text, lblNo.Text, lblDetailID.Text, c.StreamToByte(ful.FileContent))


                aView.InnerHtml = lblReportCode.Text & "-" & lblNo.Text.PadLeft(3, "0")
                Dim FileName As String = lblReportCode.Text & "-" & lblNo.Text.ToString.PadLeft(3, "0")
                Dim pathfile As String = "Render_THM_File.aspx?Mode=TAG&File=" & FileName
                aView.HRef = pathfile

                btnDel.Visible = True

            Case "Delete"
                BL.Drop_THM_File(lblReportCode.Text, lblNo.Text, lblDetailID.Text)
                aView.InnerHtml = ""
                btnDel.Visible = False

        End Select
    End Sub

    'Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
    '    pnlValidation.Visible = False
    'End Sub

    Protected Sub lnkPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreview.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Preview", "ShowPreviewReport(" & RPT_Year & "," & RPT_No & ");", True)
    End Sub

    Protected Sub HTabHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabHeader.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='THM_Routine_Edit1.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub HTabSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HTabSummary.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='THM_Routine_Edit3.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "';", True)
    End Sub

    Protected Sub lnkClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClear.Click
        BL.Drop_RPT_THM_Detail(RPT_Year, RPT_No)
        BL.Construct_THM_Report_Detail(RPT_Year, RPT_No, Session("USER_ID"))
        BindGrid()
    End Sub

    Protected Sub btn_Back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Back.Click
        HTabHeader_Click(sender, e)
    End Sub

    Protected Sub btn_Next_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Next.Click
        HTabSummary_Click(sender, e)
    End Sub

    Protected Sub txtInspectedDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInspectedDate.TextChanged
        Dim Sql As String = ""
        Sql = "select * from RPT_THM_Header where RPT_Year = " & RPT_Year & " AND RPT_NO = " & RPT_No
        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If Not BL.IsProgrammingDate(txtInspectedDate.Text) Then
                DT.Rows(0).Item("Inspec_Date") = DBNull.Value
            Else
                DT.Rows(0).Item("Inspec_Date") = CV.StringToDate(txtInspectedDate.Text, "yyyy-MM-dd")
            End If
            Dim cmb As New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If
    End Sub

End Class