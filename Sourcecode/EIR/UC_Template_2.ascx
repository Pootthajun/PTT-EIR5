﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Template_2.ascx.vb" Inherits="EIR.UC_Template_2" %>
<%@ Register Src="~/Summernote/UC_SummerNote.ascx" TagPrefix="uc1" TagName="UC_SummerNote" %>

<asp:Label ID="lblProperty" runat="server" Text="" Style="display: none;"></asp:Label>

<tr  style="text-align: center">
    <td style="width: 50%; text-align: center;"  >
         <table style="background-color: white;min-height: 500px;max-height: 500px;">
            <tr style="border-style: solid; border-width: thin; border-color: #C0C0C0;">
                <td style="vertical-align: middle;text-align :center ;">
                    <asp:ImageButton ImageUrl="RenderImage_ST_TA_Sector.aspx" ID="ImgPreview1"  onError="this.src='resources/images/File_Sector.png'" runat="server" AlternateText="..." Style="cursor: pointer; text-align: center; max-width: 100%; min-width: 50%;max-height:500px;" />
                    <asp:Button ID="btnRefreshImage1" runat="server" Style="display: none;" />
                </td>
            </tr>
        </table>
    </td>
    <td style="width: 50%; vertical-align :top ;"  >
        <table style="background-color: white;min-height: 500px;max-height: 700px;">
            <tr>
                <td>
                    <uc1:UC_SummerNote runat="server" ID="TextEditor1" />
                </td>
            </tr>
        </table>
    </td>   
</tr>

