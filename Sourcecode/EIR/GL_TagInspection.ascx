﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GL_TagInspection.ascx.vb" Inherits="EIR.GL_TagInspection" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="GL_DialogUploadImage.ascx" tagname="GL_DialogUploadImage" tagprefix="uc2" %>
<%@ Register src="GL_Inspection.ascx" tagname="GL_Inspection" tagprefix="uc1" %>
                                
                                <table width="900" align="right" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:None !important;  background-color:White;">
                                    <tr>
                                      <td id="TDTree" runat="server" style="cursor:pointer;  Width:25px;" class="LevelClassA">&nbsp;<asp:Button ID="btnExpand" runat="server" Width="10px" Height="0px" style="display:none;" Text="" /></td>
                                      <td style="width:100px;"><a ID="lbl_Code" runat="server" style="font-weight:bold; color:gray;" href="javascript:;"></a></td>
                                      <td style="width:150px; overflow:hidden; text-align:left;"><a ID="lblTagName" style="font-size:10px; color:gray" runat="server" href="javascript:;"></a></td>
                                      <td style="width:150px;"><a ID="lbl_Type" Font-Size="10px" runat="server" ForeColor="Gray" Text="Type" href="javascript:;"></a></td>
                                      <td style="text-align:center; width:100px;"><a ID="lbl_Item" style="font-size:10px; color:gray" runat="server" href="javascript:;"></a></td>
                                      <td style="text-align:center; width:75px;"><a ID="lbl_Remain" style="font-size:10px; color:gray" runat="server" href="javascript:;"></a></td>
                                      <td style="text-align:center; width:50px;"><a ID="lbl_New" style="font-size:10px; color:gray" runat="server" href="javascript:;"></a></td>
                                      <td style="text-align:center; width:75px;"><a ID="lbl_TraceTag" style="font-size:10px; color:gray" runat="server" href="javascript:;"></a></td>
                                      <td style="text-align:center; width:100px;"><a ID="lbl_Level" style="font-size:10px;" runat="server" href="javascript:;"></a></td>
                                      <td style="text-align:left; width:70px;">
                                      <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="resources/images/icons/add_small.png" ToolTip="Add more detail for this TAG" />
                                      <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="resources/images/icons/cross.png" ToolTip="Clear all this tag inspections" />
                                      <asp:Image ID="imgWarning" runat="server" style="cursor:help;" ImageUrl="resources/images/icons/alert.gif" Visible="false"  />
                                      <cc1:ConfirmButtonExtender ID="cfbDelete" runat="server" TargetControlID="btnDelete" ConfirmText="Are you sure to delete all inspection for this TAG permanently?" />
                                     </td>
                                    </tr>
                                   </table>
                                   
                                         <table width="100%" border="0" id="TrInspection" runat="server" visible="false"  cellspacing="0" cellpadding="0" style="font-size:11px; border-bottom:None; border-top:None; background-color:White;">                                        
                                                <tr style="border:None;">
                                                
                                                  <td bgcolor="#F3F3F3" style="text-align:center; width:100px;">Inspection</td>
                                                  <td bgcolor="#F3F3F3" style="text-align:center; width:100px;">Part</td>
                                                  <td bgcolor="#F3F3F3" style="text-align:center; width:80px;">Last status </td>
                                                  <td bgcolor="#F3F3F3" style="text-align:center; width:70px;">Fixed <font style="color:red">*</font></td>
                                                  <td bgcolor="#F3F3F3" style="text-align:center; width:200px;">Current Status <font style="color:red">*</font></td>
                                                  <td bgcolor="#F3F3F3" style="text-align:center; width:100px;">Trace</td>
                                                  <td bgcolor="#F3F3F3" style="text-align:center;">Detail</td>
                                                  <td bgcolor="#F3F3F3" style="text-align:center; width:70px;">Action</td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td colspan="8" align="right" style="text-align:right" >
                                                                   <asp:Repeater   ID="rptInspection" runat="server">
                                                                      <ItemTemplate>                                                                            
                                                                         <uc1:GL_Inspection ID="GLI" runat="server" />                                                                            
                                                                       </ItemTemplate>                                                                                                                                            
                                                                    </asp:Repeater>    
                                                     </td>
                                                 </tr>
                                           </table>                                    
                                            <uc2:GL_DialogUploadImage ID="GL_DialogUploadImage1" runat="server" />