﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Rotating_Routine_Plan.aspx.vb" Inherits="EIR.Rotating_Routine_Plan" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>

    <h2>Rotating Routine Master Plan</h2>
			
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div class="content-box-header">
                <h3>Display condition </h3>
                
                  
					<asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Year" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
				   				
    				<asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;"
                      ID="ddl_Search_Route" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
				    <div class="clear"></div>
                  </div>
                   

             
                <div class="content-box-content">
                    
                     <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  
                   <asp:Panel ID="pnlListPlan" runat="server">
                  
                  <!-- This is the target div. id must match the href of this div's tab -->
                  <table>
                    <thead>
                      <tr>
                       <th><a href="#">Code</a></th>
                        <th><a href="#">Year</a></th>
                        <th><a href="#">Plant</a></th>
                        <th><a href="#">Route</a></th>
                        <th><a href="#">Round</a></th>
                        <th><a href="#">Start</a></th>
                        <th><a href="#">End</a></th>
                        <th><a href="#">Estimate(days)</a></th>
                        <th><a href="#">Status</a></th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>
                    <asp:Repeater ID="rptPlan" runat="server">
                           <HeaderTemplate>
                           
                          
                                <tbody>
                             </HeaderTemplate>
                              <ItemTemplate>
                                  <tr>
                                    <td><asp:Label ID="lblCode" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblYear" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblRoute" runat="server"></asp:Label></td>  
                                    <td><asp:TextBox ID="txtRound" runat="server" style="border:none; background-color:Yellow;" Width="50px"></asp:TextBox></td>                      
                                    <td><asp:Label ID="lblStart" runat="server"></asp:Label></td>                                    
                                    <td><asp:Label ID="lblEnd" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblEstimate" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                    <td><!-- Icons -->
                                       <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                       <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                       <cc1:ConfirmButtonExtender ID="cfbToggle" runat="server" TargetControlID="btnToggle" ConfirmText="Are you sure to delete this report permanently?" />
                                       
                                       <asp:Button ID="btnUpdateRound" CommandName="Round" runat="server" Width="0px" Height="0px" Style="visibility:hidden;"/>
                                       
                                    </td>
                                  </tr>
                               </ItemTemplate>
                                  
                               <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                        </asp:Repeater>
                    <tfoot>
                      <tr>
                        <td colspan="7">
                        <div class="bulk-actions align-left">                             
                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                        </div>
                            <uc1:PageNavigation ID="Navigation" runat="server" />
                          <!-- End .pagination -->
                          <div class="clear"></div>  
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                 
                   </asp:Panel>
                   
                   <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
                   
                <asp:Panel ID="pnlEdit" runat="server">
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Plan</h3>
                    <div class="clear"></div>
                  </div>
                   
                    
                    
                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                    <p>&nbsp;</p>
					
					<p>
                      <label class="column-left" style="width:120px;" >Define to Plant : </label>
                        <asp:DropDownList CssClass="select"
                             ID="ddl_Edit_Plant" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </p>
                    
                    <p>
                      <label class="column-left" style="width:120px;" >Define to Route : </label>
                        <asp:DropDownList CssClass="select"
                             ID="ddl_Edit_Route" runat="server">
                        </asp:DropDownList>
                    </p>
					 <p>
                      <label class="column-left" style="width:120px;" >Start date :</label><asp:TextBox 
                             runat="server" ID="txtStart" CssClass="text-input small-input " MaxLength="20" 
                             AutoPostBack="True"></asp:TextBox>
				         <cc1:CalendarExtender ID="txtStart_CalendarExtender" runat="server" 
                             Format="yyyy-MM-dd" TargetControlID="txtStart" PopupPosition="Right" 
                             >
                         </cc1:CalendarExtender>
				      <!-- End .clear -->
                    </p>
					
					<p>
                      <label class="column-left" style="width:120px;" >Estimate :</label><asp:TextBox 
                            runat="server" style="width:100px" ID="txtEstimate" 
                            CssClass="text-input small-input " MaxLength="5" AutoPostBack="True"></asp:TextBox>
				        &nbsp;<span style="font-weight:bold">days</span>
				      <!-- End .clear -->
                    </p>
                    
                     <p>
                      <label class="column-left" style="width:120px;" >End date :</label><asp:TextBox 
                             runat="server" ID="txtEnd" CssClass="text-input small-input " MaxLength="20" 
                             AutoPostBack="True"></asp:TextBox>
				         <cc1:CalendarExtender ID="txtEnd_CalendarExtender" runat="server" 
                             Format="yyyy-MM-dd" TargetControlID="txtEnd" PopupPosition="Right" >
                         </cc1:CalendarExtender>
				      <!-- End .clear -->
                    </p>
                    
                    <p id="pnl_Recurring" runat="server">
                      <label class="column-left" style="width:120px;" ></label>
                      <span style="font-weight:bold">
                            
                            <asp:CheckBox runat="server" 
                            ID="chkRecurring" Text=" " style="position:relative; top:3px;" 
                            Width="20px" AutoPostBack="True" /> 
                        Recurring every
                            <asp:TextBox ID="txt_Interval" runat="server" 
                            CssClass="text-input small-input " MaxLength="3" Width="50px"></asp:TextBox>
                        &nbsp;<asp:DropDownList CssClass="ddl_Period" ID="ddl_Repeat" runat="server">
                            <asp:ListItem></asp:ListItem>
                            <asp:ListItem Value="D">Day</asp:ListItem>
                            <asp:ListItem Value="W">Week</asp:ListItem>
                            <asp:ListItem Value="M">Month</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;for&nbsp; <asp:TextBox runat="server" ID="txt_Repeat" 
                            CssClass="text-input small-input " Width="50px" MaxLength="3"></asp:TextBox> 
                        &nbsp;times</span>
				      
				      <!-- End .clear -->
                    </p>
					
				    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>
                    <p align="right">
                      <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                      <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                    </p>
                    </fieldset>
                    
                    </asp:Panel>
                    
                </div>
                <!-- End #tab1 -->
                
    </div>
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>