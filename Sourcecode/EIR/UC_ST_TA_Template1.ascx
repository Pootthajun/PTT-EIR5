﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_ST_TA_Template1.ascx.vb" Inherits="EIR.UC_ST_TA_Template1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="UC_TextEditor.ascx" TagName="UC_TextEditor" TagPrefix="uc1" %>

<asp:Label ID="lblProperty" runat="server" Text="" Style="display: none;"></asp:Label>

<%--------------------------------------------------------------------------------------%>
<tr style="text-align: center">
    <td style="width: 50%; text-align: center;" colspan="2">
        <%-- <asp:Image ImageUrl="RenderImage_ST_TA_Sector.aspx" ID="ImgPreview1" runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer;max-width: 100%;cursor: pointer;max-height: 400px;min-height:400px; text-align :center;" />--%>


        <table style="background-color: white;">
            <tr style="border-style: solid; border-width: thin; border-color: #C0C0C0;">
                <td style="vertical-align: middle;text-align :center ;">
                    <asp:Image ImageUrl="RenderImage_ST_TA_Sector.aspx" ID="ImgPreview1" runat="server" GenerateEmptyAlternateText="true" AlternateText="..." Style="cursor: pointer; cursor: pointer; text-align: center; max-width: 100%; min-width: 50%;" />

                    <asp:Button ID="btnRefreshImage" runat="server" Text="Update/Refresh" Style="visibility: hidden; width: 0px;" />
                </td>
            </tr>
        </table>
    </td>

</tr>


<tr id="trimage_1" runat="server" visible="false" style="text-align: center">
    <td style="width: 50%; background-color: black;" colspan="2">
        <div class="thumbnail">


            <a id="lnk_File_Dialog1" runat="server" target="_blank" title="Figure No.1">
                <asp:Image ID="img_File1" Width="100%" runat="server" AlternateText="" onClick="window.open(this.src);" Style="cursor: pointer;" />
            </a>
        </div>
    </td>

</tr>
<%--------------------------------------------------------------------------------------%>
<tr id="trbtn_1" runat="server" visible="false">
    <td style="width: 50%;" class="toolbar" colspan="2">
        <asp:Button ID="btnUpload1" runat="server" Text="" Style="display: none;" />
        <asp:ImageButton ID="btnEdit1" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" Style="margin-right: 10px;" />
        <asp:ImageButton ID="btnDelete1" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" Style="margin-left: 10px;" />
        <cc1:ConfirmButtonExtender ID="btnDelete1_Confirm" runat="server" ConfirmText="Confirm to delete Photographic location?" TargetControlID="btnDelete1"></cc1:ConfirmButtonExtender>
    </td>
</tr>
<!-- End Table Images -->

<%--------------------------------------------------------------------------------------%>



<tr>
    <td colspan="2">
        <table style="background-color: white;">
            <tr>
                <td>
                    <uc1:UC_TextEditor ID="UC_TextEditor1" runat="server" />
                </td>
            </tr>
        </table>
    </td>

</tr>


