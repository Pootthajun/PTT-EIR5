﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="PIPE_Tag.aspx.vb" Inherits="EIR.PIPE_Tag" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register src="~/UC_PIPE_TAG.ascx" tagname="UC_PIPE_TAG" tagprefix="uc" %>
<%@ Register Src="~/UC_PictureAlbum.ascx" TagPrefix="uc" TagName="UC_PictureAlbum" %>
<%@ Register Src="~/UC_PIPE_POINT.ascx" TagPrefix="uc" TagName="UC_PIPE_POINT" %>
<%@ Register Src="~/UC_DocumentAlbum.ascx" TagPrefix="uc" TagName="UC_DocumentAlbum" %>

<asp:Content ID="ContentHeader" runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    <link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" />
    <link rel="stylesheet" href="resources/css/File_Album.css" type="text/css" media="all" />
    <link rel="stylesheet" href="resources/css/Dropdown_Popover.css" type="text/css" media="all" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
			
			<!-- Page Head -->
			<h2>Pipe Setting </h2>			
						
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <asp:Panel ID="pnlSearch" runat="server">
              <div class="content-box-header" style="height:auto; padding-top:5px; padding-bottom:5px;">
                <div>
                    <h3 style="margin-top:-5px; width:120px;">Search TAG</h3>
				    <asp:DropDownList CssClass="select" ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" ID="ddl_Search_Area" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                     <asp:DropDownList CssClass="select" ID="ddl_Search_Route" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                   <asp:DropDownList CssClass="select" ID="ddl_Search_Process" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:TextBox runat="server" ID="txt_Search_LineNo" AutoPostBack="True" 
                        CssClass="text-input small-input " Width="50px" PlaceHolder="Line No"></asp:TextBox>
                    
                    <asp:TextBox runat="server" ID="txt_Search_Size" AutoPostBack="True" 
                        CssClass="text-input small-input " Width="50px" PlaceHolder="Size"></asp:TextBox>

                    <asp:DropDownList CssClass="select" ID="ddl_Search_Material" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" ID="ddl_Search_Pressure" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:DropDownList CssClass="select" ID="ddl_Search_CA" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" ID="ddl_Search_Service" runat="server" AutoPostBack="true">
                    </asp:DropDownList>

                    <asp:DropDownList CssClass="select" ID="ddl_Search_Insulation" runat="server" AutoPostBack="True">
                    </asp:DropDownList>

                    <asp:TextBox runat="server" ID="txt_search_Insulation_Thickness" AutoPostBack="True" 
                        CssClass="text-input small-input " Width="100px" PlaceHolder="Ins Thickness"></asp:TextBox>

                </div>
                <div>
                    <h3 style="margin-top:-5px; width:120px;">Search Code</h3>
                    <asp:TextBox runat="server" ID="txt_Search_Code" AutoPostBack="True" 
                        CssClass="text-input small-input " Width="150px" MaxLength="50" PlaceHolder="Tag code"></asp:TextBox>
                 </div>
              </div>
            <div class="content-box-content">
                <div class="tab-content default-tab">
                  <asp:Panel ID="pnlListTag" runat="server">
                       <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                          <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                          <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                       </asp:Panel>
                  
                      <table class="tbTagList">
                        <thead>
                          <tr>
                            <th style="text-align:center;"><a href="javascript:;">Tag Code</a></th>
                            <th style="text-align:center;"><a href="javascript:;">Plant</a></th>
                             <th style="text-align:center;"><a href="javascript:;">Route</a></th>
                            <th style="text-align:center;"><a href="javascript:;">Material</a></th>
                            <th style="text-align:center;"><a href="javascript:;">CML(s)</a></th>
                            <th style="text-align:center;"><a href="javascript:;">For service</a></th>
                            <th style="text-align:center;"><a href="javascript:;">Loop No</a></th>
                            <th style="text-align:center;"><a href="javascript:;">Action</a></th>
                          </tr>
                        </thead>
                   
                        <asp:Repeater ID="rptTag" runat="server">
                               <HeaderTemplate>
                                <tbody>
                               </HeaderTemplate>
                               <ItemTemplate>
                                      <tr>
                                        <td><asp:Label ID="lblCode" runat="server"></asp:Label></td>
                                        <td style="text-align:center;"><asp:Label ID="lblPlant" runat="server"></asp:Label></td>
                                        <td style="text-align:center;"><asp:Label ID="lblRoute" runat="server"></asp:Label></td>
                                        <td style="text-align:center;"><asp:Label ID="lblMaterial" runat="server"></asp:Label></td>
                                        <td><asp:Label ID="lblPoint" runat="server"></asp:Label></td>
                                        <td style="text-align:center;"><asp:Label ID="lblService" runat="server"></asp:Label></td>
                                        <td style="text-align:center;"><asp:Label ID="lblLoopNo" runat="server"></asp:Label></td>
                                        <td><!-- Icons -->
                                              <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                              <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                              <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/del.png" />
                                              <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete ?" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                                        </td>
                                      </tr>                                  
                                 </ItemTemplate>
                                <FooterTemplate>
                                 </tbody>
                                </FooterTemplate>
                               </asp:Repeater>
                         <tfoot>
                          <tr>
                            <td colspan="6">
                            <div class="bulk-actions align-left">                             
                                <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                            </div>
                                <uc1:PageNavigation ID="Navigation" runat="server" />
                              <!-- End .pagination -->
                              <div class="clear"></div>  
                            </td>
                          </tr>
                        </tfoot>
                      </table>				  
				         <div class="clear"></div>
				     <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                          <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				          <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				     </asp:Panel>
			        </asp:Panel>
                  
                    </div>
                  </div>
          </asp:Panel>
            
          <asp:Panel ID="pnlEdit" runat="server">
                             
                  <!-- This is the target div. id must match the href of this div's tab -->			    
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Pipe Info : <asp:Label ID="lblTagCode" runat="server"></asp:Label></h3>
                    <ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabTag" runat="server" CssClass="default-tab current">Tag Info</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabPoint" runat="server">CML(s)</asp:LinkButton></li>
					</ul>					
					<div class="clear"></div>
                  </div>  
                  
                 <asp:Panel ID="TabTag" runat="server">

                     <table style="width:100%;">
                         <tr>
                             <td style="width:60%; text-align-last:start; vertical-align:top;">
                                 <uc:UC_PIPE_TAG ID="Pipe_Info" runat="server" />
                             </td>
                             <td style="width:40%; text-align-last:start; vertical-align:top;">
                                 <div style="width: 100%; vertical-align: middle; line-height: 30px; padding-left: 10px; font-size: 14px; font-weight: bold;">
                                     Tag Overview
                                 </div>
                                 <!-------------- Tag Preview ---------------->
                                 <div class="File_Thumbnail" style="width:350px; height:350px;">
                                    <a class="File_Zoom_Mask" ID="aZoomMaskMainPreview" runat="server" target="_blank" style="width:350px; height:330px;"></a>
                                    <asp:Panel ID="pnlMainPreview" runat="server" CssClass="File_Image" Width="350px" Height="350px" BackImageUrl="">
                                        <div class="File_Command" style="text-align-last:center; width:100%;">
                                            <asp:ImageButton  ID="btnEditMainPreview" runat="server" ImageUrl="resources/images/icons/edit_white_16.png"/>
                                            <asp:ImageButton  ID="btnDeleteMainPreview" runat="server" ImageUrl="resources/images/icons/delete_white_16.png"/>
                                            <Ajax:ConfirmButtonExtender ID="cfmMainDeletePreview" runat="server" ConfirmText="Are you sure to delete tag overview?" BehaviorID="btnDeleteMainPreview" TargetControlID="btnDeleteMainPreview"></Ajax:ConfirmButtonExtender>
                                        </div>  
                                    </asp:Panel>  
                                    <div class="File_Title" id="divTitleMainPreview" runat="server"><asp:Label ID="lblTitleMainPreview" runat="server"></asp:Label></div>
                                    <asp:Button ID="btnRefreshMainPreview" runat="server" Style="display:none;" CommandName="Refresh" />    
                                     <asp:TextBox ID="txtMainPreviewID" runat="server" style="display:none;"></asp:TextBox>                
                                </div>
                                 <!-------------- Tag Preview ---------------->
                             </td>
                         </tr>
                         <tr>
                             <td valign="top" style="vertical-align:top;">
                                 <uc:UC_PictureAlbum ID="Pipe_File" runat="server" HeaderTitle="Picture Attachment" />
                             </td>
                             <td valign="top" style="vertical-align:top;">
                                 <uc:UC_DocumentAlbum runat="server" id="Pipe_Doc" HeaderTitle="Document Attachment" />
                             </td>
                         </tr>
                     </table>

                </asp:Panel>
                 <asp:Panel ID="TabPoint" runat="server">
                     
                     <asp:Panel ID="pnlListPoint" runat="server">
                     <div class="content-box-header" style="background-color:white; background-image:none;">
                            <h3>Total CML(s)</h3>
                     </div>  
 
                      <table style="width:100%;">
                          <tr>
                              <td style="width:60%; text-align-last:start; vertical-align:top;">
                                  <table class="tbPointList">
                                    <thead>
                                      <tr>
                                        <th style="text-align-last:center;"><a href="javascript:;">#</a></th>
                                        <th style="text-align-last:center;"><a href="javascript:;">Name</a></th>
                                        <th style="text-align-last:center;"><a href="javascript:;">Location/Component</a></th>
                                        <th style="text-align-last:center;"><a href="javascript:;">Attachment(s)</a></th>
                                        <th style="text-align-last:center;"><a href="javascript:;">Remaining Life(s)</a></th>
                                        <th style="text-align-last:center;"><a href="javascript:;">Action</a></th>
                                      </tr>
                                    </thead>
                   
                                    <asp:Repeater ID="rptPoint" runat="server">
                                           <HeaderTemplate>
                                                <tbody>
                                           </HeaderTemplate>
                                           <ItemTemplate>
                                                  <tr id="trPoint" runat="server">
                                                    <td style="text-align-last:center;"><asp:Label ID="lblNo" runat="server"></asp:Label></td>
                                                    <td><asp:Label ID="lblName" runat="server"></asp:Label></td>
                                                    <td style="text-align-last:center;"><asp:Label ID="lblComponent" runat="server"></asp:Label></td>
                                                    <td style="text-align-last:center;"><asp:Label ID="lblFile" runat="server"></asp:Label></td>
                                                    <td style="text-align-last:center;"><asp:Label ID="lblLife" runat="server"></asp:Label></td>
                                                    <td style="text-align-last:center;"><!-- Icons -->
                                                          <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                                          <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ImageUrl="resources/images/icons/del.png" />
                                                          <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" TargetControlID="btnDelete" ConfirmText="Are you sure you want to delete this point ?"></Ajax:ConfirmButtonExtender>
                                                          <asp:Button ID="btnPreview" CommandName="Preview" runat="server" style="display:none;"/>
                                                    </td>
                                                  </tr>                                  
                                             </ItemTemplate>
                                            <FooterTemplate>
                                             </tbody>
                                            </FooterTemplate>
                                           </asp:Repeater>                                     
                                  </table>
								  <asp:LinkButton ID="btnAddPoint" runat="server" style="margin-top:15px;" CssClass="button" Text="Add"></asp:LinkButton>
                              </td>
                              <td style="width:40%">
                                <div class="File_Thumbnail" style="width:350px; height:350px;">
                                    <a class="File_Zoom_Mask" ID="lnkPointPreview" runat="server" target="_blank" style="width:350px; height:350px;"></a>
                                    <asp:Panel ID="pointOverview" CssClass="File_Image" runat="server" width="350px" Height="350px" ></asp:Panel>
                                </div>
                                  
                              </td>
                          </tr>
                      </table>
                      </asp:Panel>
                      <asp:Panel ID="pnlEditPoint" runat="server">
                          <div class="content-box-header" style="background-color:white; background-image:none;">
                            <h3 style="float:left; width:45%; display:inline-block;"><asp:Label ID="lblEditPoint" runat="server"></asp:Label></h3>    
                            <h3 style="display:inline-block; width:45%; float:right; text-align-last:end; ">
                                         <asp:Button ID="btnCancelPoint_Top" runat="server" CssClass="button" Text="Back to all CMLs"   />  
                            </h3>     
                        </div> 

                        <table style="width:100%;">
                            <tr>
                                <td style="width:60%; text-align-last:start; vertical-align:top;">
                                    <uc:UC_PIPE_POINT runat="server" id="Point_Info" />
                                </td>
                                <td style="width:40%; text-align-last:start; vertical-align:top;">
                                    <div style="margin:0; vertical-align: middle; line-height: 30px; display:inline-block; display:inline-block; padding-left: 10px; font-size: 14px; font-weight: bold;">
                                     Point Overview
                                    </div>
                                   
                                    <!-------------- Point Preview ---------------->
                                     <div class="File_Thumbnail" style="width:350px; height:350px;">
                                        <a class="File_Zoom_Mask" ID="aZoomMaskPointPreview" runat="server" target="_blank" style="width:350px; height:330px;"></a>
                                        <asp:Panel ID="pnlPointPreview" runat="server" CssClass="File_Image" Width="350px" Height="350px" BackImageUrl="">
                                            <div class="File_Command" style="text-align-last:center; width:100%;">
                                                <asp:ImageButton  ID="btnEditPointPreview" runat="server" ImageUrl="resources/images/icons/edit_white_16.png"/>
                                                <asp:ImageButton  ID="btnDeletePointPreview" runat="server" ImageUrl="resources/images/icons/delete_white_16.png"/>
                                                <Ajax:ConfirmButtonExtender ID="cfmPointDeletePreview" runat="server" ConfirmText="Are you sure to reset to main tag overview?" BehaviorID="btnDeletePointPreview" TargetControlID="btnDeletePointPreview"></Ajax:ConfirmButtonExtender>
                                            </div>  
                                        </asp:Panel>  
                                        <div class="File_Title" id="divTitlePointPreview" runat="server"><asp:Label ID="lblTitlePointPreview" runat="server"></asp:Label></div>
                                        <asp:Button ID="btnRefreshPointPreview" runat="server" Style="display:none;" CommandName="Refresh" />    
                                         <asp:TextBox ID="txtPointPreviewID" runat="server" style="display:none;"></asp:TextBox>                
                                    </div>
                                     <!-------------- Point Preview ---------------->
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <uc:UC_PictureAlbum runat="server" ID="Point_File" HeaderTitle="Picture Attachment" />
                                </td>
                            </tr>
                        </table>
                          
                          <p align="right" style="padding-right:20px; margin-top:20px; margin-bottom:20px; Style="float:right;">
                            <asp:Button ID="btnCancelPoint_Bottom" runat="server" CssClass="button" Text="Back to see all CMLs" style="margin-right:10px;" />
                            <asp:Button ID="btnOKPoint" runat="server" CssClass="button" Text="OK" />
                          </p> 
                      </asp:Panel>
                      
                       <asp:Panel ID="pnlBindingPointError" runat="server" CssClass="notification attention png_bg">
                          <asp:ImageButton ID="btnBindingPointError" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                          <div><asp:Label ID="lblBindingPointError" runat="server"></asp:Label></div>
                       </asp:Panel>
                      
                 </asp:Panel>
                   
                        

                  <asp:Panel ID="pnlSaveTag" runat="server">
                        
                        <p>
                            <label class="column-left" style="width:120px; margin-left:20px; padding-top:10px;" >Routine Route : </label>
                            <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                                 ID="ddl_Route" runat="server">
                            </asp:DropDownList>
                        </p>
                        <p style="width:100%; margin-left:20px; margin-top:20px; float:left;">
                            <label class="column-left" style="width:120px;">Available &nbsp;</label>
                            <asp:CheckBox ID="chkAvailable" runat="server" Text=""/>
                        </p>
                        <asp:Panel ID="pnlValidation" runat="server" CssClass="notification error png_bg" Style="float:unset;">
                            <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                            <div>
                                <asp:Label ID="lblValidation" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                        <p align="right" style="padding-right:20px; margin-top:20px; margin-bottom:20px; Style="float:right;">
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Back to see all tag" style="margin-right:10px;" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                        </p>     
                  </asp:Panel>
                        
                       
                 
                 
                          		
             </asp:Panel>
		  </div>		  

     <script type="text/javascript">
        function resizeTabPoint() {
            if (!document.getElementById("posOverview")) return;
            $('#posOverview').height($('#posOverview').width());
        }
    </script>

</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>
