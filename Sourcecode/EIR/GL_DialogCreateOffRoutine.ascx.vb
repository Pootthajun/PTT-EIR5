﻿Imports System.Data
Imports System.Data.SqlClient
Public Class GL_DialogCreateOffRoutine
    Inherits System.Web.UI.UserControl

    Dim BL As New EIR_BL

    Public Event UpdateCompleted(ByRef sender As GL_DialogCreateOffRoutine, ByVal RPT_Year As Integer, ByVal RPT_No As Integer)
    Public Event CloseCompleted(ByRef sender As GL_DialogCreateOffRoutine)


#Region "Dynamic Property"

    Public Property RPT_Year() As Integer
        Get
            Return Me.Attributes("RPT_Year")
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_Year") = value
        End Set
    End Property

    Public Property RPT_No() As Integer
        Get
            Return Me.Attributes("RPT_No")
        End Get
        Set(ByVal value As Integer)
            Me.Attributes("RPT_No") = value
        End Set
    End Property

    Public Property REPORT_TYPE() As EIR_BL.Report_Type
        Get
            If IsNumeric(Me.Attributes("REPORT_TYPE")) Then
                Return Me.Attributes("REPORT_TYPE")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As EIR_BL.Report_Type)
            Me.Attributes("REPORT_TYPE") = value
            PLANT_ID = PLANT_ID
        End Set
    End Property

    Public Property PLANT_ID() As Integer
        Get
            If ddl_Plant.SelectedIndex >= 0 Then
                Return ddl_Plant.Items(ddl_Plant.SelectedIndex).Value
            Else
                Return -1
            End If
        End Get
        Set(ByVal value As Integer)
            BL.BindDDlPlant(ddl_Plant, value)
            AREA_ID = -1
            TAG_ID = -1
        End Set
    End Property

    Public Property AREA_ID() As Integer
        Get
            If ddl_Area.SelectedIndex >= 0 Then
                Return ddl_Area.Items(ddl_Area.SelectedIndex).Value
            Else
                Return -1
            End If
        End Get
        Set(ByVal value As Integer)
            'Select Case REPORT_TYPE
            '    Case EIR_BL.Report_Type.Stationary_Routine_Report, EIR_BL.Report_Type.Stationary_Off_Routine_Report
            '        BL.BindDDlArea(PLANT_ID, ddl_Route, value)
            '    Case EIR_BL.Report_Type.Rotating_Routine_Report, EIR_BL.Report_Type.Rotating_Off_Routine_Report
            '        BL.BindDDl_RO_Route(PLANT_ID, ddl_Route, value)
            'End Select
            BL.BindDDlArea(PLANT_ID, ddl_Area, value)
            TAG_ID = -1
        End Set
    End Property

    Public Property TAG_ID() As Integer
        Get
            If ddl_Tag.SelectedIndex >= 0 Then
                Return ddl_Tag.Items(ddl_Tag.SelectedIndex).Value
            Else
                Return -1
            End If
        End Get
        Set(ByVal value As Integer)
            Select Case REPORT_TYPE
                Case EIR_BL.Report_Type.Stationary_Routine_Report, EIR_BL.Report_Type.Stationary_Off_Routine_Report
                    BL.BindDDl_ST_Tag(ddl_Tag, PLANT_ID, , AREA_ID, , , value)
                Case EIR_BL.Report_Type.Rotating_Routine_Report, EIR_BL.Report_Type.Rotating_Off_Routine_Report
                    BL.BindDDl_RO_Tag(ddl_Tag, PLANT_ID, , AREA_ID, , , value)
            End Select
            '--------------------Report Tag Type---------------
            Dim DT As New DataTable
            Dim DA As New SqlDataAdapter("SELECT TAG_TYPE_Name FROM VW_ALL_ACTIVE_RO_TAG WHERE TAG_ID=" & value, BL.ConnStr)
            DA.Fill(DT)
            If DT.Rows.Count > 0 AndAlso Not IsDBNull(DT.Rows(0).Item("TAG_TYPE_Name")) Then
                lbl_TagType.Text = "( " & DT.Rows(0).Item("TAG_TYPE_Name") & " )"
            Else
                lbl_TagType.Text = ""
            End If

        End Set
    End Property

#End Region

#Region "Static Property"

    Public Property UNIQUE_POPUP_ID() As String
        Get
            Return Me.Attributes("UNIQUE_POPUP_ID")
        End Get
        Set(ByVal value As String)
            Me.Attributes("UNIQUE_POPUP_ID") = value
        End Set
    End Property

    Private ReadOnly Property USER_LEVEL() As EIR_BL.User_Level
        Get
            Return Session("USER_LEVEL")
        End Get
    End Property

#End Region

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CloseDialog()
    End Sub

    Public Sub CloseDialog()
        Me.Visible = False
        RaiseEvent CloseCompleted(Me)
    End Sub

    Public Sub ShowDialog(Optional ByVal Init_REPORT_TYPE As EIR_BL.Report_Type = EIR_BL.Report_Type.Stationary_Routine_Report,
                            Optional ByVal Init_PLANT_ID As Integer = -1,
                            Optional ByVal Init_AREA_ID As Integer = -1,
                            Optional ByVal Init_TAG_ID As Integer = -1)

        REPORT_TYPE = Init_REPORT_TYPE
        PLANT_ID = Init_PLANT_ID
        AREA_ID = Init_AREA_ID
        TAG_ID = Init_TAG_ID
        Me.Visible = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        pnlValidation.Visible = False
        '---------------- Add Trigger ----------------
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        '--------------Validate --------------
        If PLANT_ID = 0 Then
            lblValidation.Text = "Please select plant."
            pnlValidation.Visible = True
            Exit Sub
        End If

        If AREA_ID = 0 Then
            lblValidation.Text = "Please select area."
            pnlValidation.Visible = True
            Exit Sub
        End If

        If TAG_ID = 0 Then
            lblValidation.Text = "Please select TAG."
            pnlValidation.Visible = True
            Exit Sub
        End If

        '--------------Check Previous Incomplete Report ---------
        Dim PT As DataTable = BL.SP_Get_Previous_Incompleted_ST_Off_Routine_Report(TAG_ID)
        If PT.Rows.Count > 0 Then
            Dim Col() As String = {"RPT_CODE"}
            PT.DefaultView.Sort = "RPT_CODE"
            PT = PT.DefaultView.ToTable(True, Col).Copy
            Dim ReportNo As String = ""
            For r As Integer = 0 To PT.Rows.Count - 1
                ReportNo &= "," & PT.Rows(r).Item("RPT_CODE")
            Next
            lblValidation.Text = "This report cannot be edited due to follow these reasons<br>" & vbNewLine
            lblValidation.Text &= "<li>Some of previous report has not completed to updating information<li>Try to check report <b>" & ReportNo.Substring(1) & "</b>" & vbNewLine
            pnlValidation.Visible = True
            Exit Sub
        End If


        '---------- Create Starter Detail ---------
        Dim Sql As String = ""
        If REPORT_TYPE = EIR_BL.Report_Type.Stationary_Off_Routine_Report Then
            Sql = "SELECT * FROM RPT_ST_Header WHERE 1=0 "
        Else
            Sql = "SELECT * FROM RPT_RO_Header WHERE 1=0 "
        End If

        Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow = DT.NewRow
        RPT_Year = Now.Year + 543
        RPT_No = BL.GetNewReportNumber(RPT_Year)

        DR("RPT_Year") = RPT_Year
        DR("RPT_No") = RPT_No

        DR("RPT_Type_ID") = REPORT_TYPE
        DR("PLANT_ID") = PLANT_ID
        DR("AREA_ID") = AREA_ID
        '----------- Get Route ID------------
        Dim TMP As DataTable
        If REPORT_TYPE = EIR_BL.Report_Type.Stationary_Off_Routine_Report Then
            TMP = BL.Get_Active_Stationary_Tag(PLANT_ID,, AREA_ID,,)
        Else
            TMP = BL.Get_Active_Rotating_Tag(PLANT_ID,, AREA_ID,,)
        End If
        TMP.DefaultView.RowFilter = "TAG_ID=" & TAG_ID
        If TMP.DefaultView.Count > 0 Then
            DR("ROUTE_ID") = TMP.DefaultView(0).Item("ROUTE_ID")
        End If
        '-------------------------------------

        DR("RPT_Period_Start") = Now
        DR("RPT_Subject") = "รายงานการตรวจสอบอุปกรณ์ " & ddl_Tag.Items(ddl_Tag.SelectedIndex).Text & " ภายในพื้นที่ " & ddl_Plant.Items(ddl_Plant.SelectedIndex).Text
        DR("RPT_To") = "ผจ.ตร."
        DR("RPT_Cause") = "การตรวจสอบ Stationary Off Routine อุปกรณ์ " & ddl_Tag.Items(ddl_Tag.SelectedIndex).Text & " ภายในพื้นที่ " & ddl_Plant.Items(ddl_Plant.SelectedIndex).Text
        DR("RPT_Result") = ""
        'DR("RPT_STEP") = 1

        '--------------------Set RPT_LOCK_BY---------------
        Select Case USER_LEVEL
            Case EIR_BL.User_Level.Administrator

                DR("RPT_STEP") = EIR_BL.User_Level.Collector
                'DR("RPT_LOCK_BY") = Session("USER_ID")
                DR("RPT_COL_By") = Session("USER_ID")
                DR("RPT_COL_Date") = Now
                'DR("RPT_INSP_By
                'DR("RPT_INSP_Date
                'DR("RPT_INSP_Comment
                'DR("RPT_ANL_By
                'DR("RPT_ANL_Date
                'DR("RPT_ANL_Comment
                'DR("RPT_STEP
                'DR("RPT_LOCK_BY

            Case EIR_BL.User_Level.Collector
                DR("RPT_STEP") = EIR_BL.User_Level.Collector
                DR("RPT_LOCK_BY") = Session("USER_ID")
                DR("RPT_COL_By") = Session("USER_ID")
                DR("RPT_COL_Date") = Now
                'DR("RPT_INSP_By
                'DR("RPT_INSP_Date
                'DR("RPT_ANL_By
                'DR("RPT_ANL_Date
                'DR("RPT_STEP
                'DR("RPT_LOCK_BY
            Case EIR_BL.User_Level.Inspector

                DR("RPT_STEP") = EIR_BL.User_Level.Inspector
                DR("RPT_LOCK_BY") = Session("USER_ID")
                'DR("RPT_COL_By") = Session("USER_ID")
                'DR("RPT_COL_Date") = Now
                DR("RPT_INSP_By") = Session("USER_ID")
                DR("RPT_INSP_Date") = Now
                'DR("RPT_ANL_By")= Session("USER_ID")
                'DR("RPT_ANL_Date")= Now

            Case EIR_BL.User_Level.Approver
                DR("RPT_STEP") = EIR_BL.User_Level.Approver
                DR("RPT_LOCK_BY") = Session("USER_ID")
                'DR("RPT_COL_By") = Session("USER_ID")
                'DR("RPT_COL_Date") = Now
                'DR("RPT_INSP_By") = Session("USER_ID")
                'DR("RPT_INSP_Date") = Now
                DR("RPT_ANL_By") = Session("USER_ID")
                DR("RPT_ANL_Date") = Now

            Case EIR_BL.User_Level.Viewer
                lblValidation.Text = "You have no authorization to create report."
                pnlValidation.Visible = True
                Exit Sub
        End Select


        DR("RPT_By") = Session("USER_ID")
        DR("RPT_Date") = Now

        DR("Created_By") = Session("USER_ID")
        DR("Created_Time") = Now
        DR("Update_Time") = Now
        DR("Update_By") = Session("USER_ID")
        '------------ Lock Report ---------------
        If Session("USER_ID") <> 0 Then DR.Item("RPT_LOCK_BY") = Session("USER_ID")

        DT.Rows.Add(DR)
        Dim CMD As New SqlCommandBuilder(DA)
        DA.Update(DT)

        '---------- Create Tag Detail ---------
        Select Case REPORT_TYPE
            Case EIR_BL.Report_Type.Stationary_Routine_Report, EIR_BL.Report_Type.Stationary_Off_Routine_Report
                BL.Construct_ST_Report_Detail(Session("USER_ID"), RPT_Year, RPT_No, TAG_ID)
            Case EIR_BL.Report_Type.Rotating_Routine_Report, EIR_BL.Report_Type.Rotating_Off_Routine_Report
                BL.Construct_RO_Report_Detail(Session("USER_ID"), RPT_Year, RPT_No, TAG_ID)
        End Select
        Me.Visible = False
        RaiseEvent UpdateCompleted(Me, RPT_Year, RPT_No)
    End Sub

    Protected Sub ddl_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Plant.SelectedIndexChanged
        PLANT_ID = PLANT_ID
    End Sub

    Protected Sub ddl_Area_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Area.SelectedIndexChanged
        AREA_ID = AREA_ID
    End Sub

    Protected Sub ddl_Tag_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Tag.SelectedIndexChanged
        TAG_ID = TAG_ID
    End Sub

End Class