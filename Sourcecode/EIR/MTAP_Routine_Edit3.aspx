﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="MTAP_Routine_Edit3.aspx.vb" Inherits="EIR.MTAP_Routine_Edit3" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="GL_DialogInputValue.ascx" tagname="GL_DialogInputValue" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<!-- Page Head -->
    <asp:UpdatePanel ID="UDPMain" runat="Server">
	    <ContentTemplate>
	    
	    
			<h2>Edit MTAP Report</h2>
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server">Report Detail</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server" CssClass="default-tab current">Report 
                            Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">

  
				  <div class="tab-content current">
				  
				  		<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
										
							    <p style="font-weight:bold; font-size:16px;">
								<label class="column-left" style="width:120px; font-size:16px;" >Report for: </label>
								     <asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader" ></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Year" CssClass="EditReportHeader"></asp:Label>
								    | Round <asp:Label ID="lbl_Round" runat="server" Text="Round" CssClass="EditReportHeader"></asp:Label>
								    | Period <asp:Label ID="lbl_Period" runat="server" Text="Period" CssClass="EditReportHeader" ></asp:Label>
							    </p>
										
							<ul class="shortcut-buttons-set">
							     
								  <%--<li>
								    <asp:LinkButton ID="lnkRefresh" runat="server" CssClass="shortcut-button">
								      <span>
									        <img src="resources/images/icons/refresh_48.png" alt="icon" width="48" height="48" /><br />
									    Reset this tab
									    </span>
								    </asp:LinkButton>
								  </li>
								  <li>
								    <asp:LinkButton ID="lnkSave" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/save_48.png" alt="icon" width="48" height="48" /><br />
								         Save changed
								        </span>
								    </asp:LinkButton>
								  </li>--%>
								  <li>
								    <asp:LinkButton ID="lnkPreview" runat="server" CssClass="shortcut-button">
								        <span>
									        <img src="resources/images/icons/print_48.png" alt="icon" width="48" height="48" /><br />
									        Preview report
								        </span>
								    </asp:LinkButton>
								  </li>
					        </ul>
					         
					         
					         
					         <div class="clear"></div><!-- End .clear -->
                                <p>&nbsp;</p>
                                <table>
                                    <thead>
                                         <tr >
                                          <th colspan="4" style="height:24px;">Problem Summary</th>
                                        </tr>
                                        <tr>
                                          <th style="background-color:#ffff99; text-align:center; width:150px; ">Tag.No.</th>
                                          <th style="background-color:#ffff99; text-align:center; width:200px;">Problems</th>
                                          <th style="background-color:#ffff99; text-align:center;">Comment</th>
                                          <th style="background-color:#ffff99; text-align:center; width:80px;">Sap No.</th>
                                         </tr>
                                    </thead>
                                    <asp:Repeater ID="rptSummary" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                              <td style="vertical-align:top; border-bottom:1px solid #EFEFEF; border-right:1px solid #EFEFEF; border-left:1px solid #EFEFEF; background-color:#ccccff;"><asp:label ID="lblTag" Font-Size="14px" BackColor="" runat="server"></asp:label></td>
                                              <td style="vertical-align:top; border-bottom:1px solid #EFEFEF; border-right:1px solid #EFEFEF; background-color:#ccccff;"><asp:label ID="lblProblem" Font-Size="14px" runat="server"></asp:label></td>
                                              <td style="vertical-align:top; border-bottom:1px solid #EFEFEF; border-right:1px solid #EFEFEF; background-color:#ccccff;"><asp:TextBox ID="txtComment" Width="100%" Font-Size="14px" BorderStyle="None" runat="server" BackColor="Transparent"></asp:TextBox></td>
                                              <td style="vertical-align:top; border-bottom:1px solid #EFEFEF; border-right:1px solid #EFEFEF; background-color:#ccccff;">
                                              <asp:Button style="display:none;" ID="btnEdit" runat="server" CommandName="Save" />
                                              <asp:TextBox Width="100%" style="border:none; text-align:center; font-size:14px;" ID="txtResponse" MaxLength="50" runat="server" BackColor="Transparent"></asp:TextBox></td>                                              
                                         </tr>
                                    </ItemTemplate>
                                    </asp:Repeater>                                
                                </table>
								<p>&nbsp;</p>
					         
					         
					         
					         <div class="clear"></div><!-- End .clear -->
                                <p>
                                    <label style="width:300px;">
                                    Recommendation (ข้อเสนอแนะ):
                                    </label>
                                    <asp:TextBox ID="txt_RPT_Result" runat="server" AutoPostBack="true" 
                                        CssClass="text-input" Height="80px" MaxLength="1000" TextMode="MultiLine" 
                                        Width="800px"></asp:TextBox>
                            </p>
			
								<asp:Panel ID="pnl_Collector" runat="server">
                                    <p>
                                        <label class="column-left" style="width:120px;">
                                        Colleted date:
                                        </label>
                                        <asp:TextBox ID="txt_RPT_COL_Date" runat="server" 
                                            CssClass="text-input small-input" ReadOnly="true"></asp:TextBox>
                                    </p>
                                    <p>
                                        <label class="column-left" style="width:120px;">
                                        Colleted by:
                                        </label>
                                        <asp:TextBox ID="txt_RPT_COL_By" runat="server" 
                                            CssClass="text-input small-input" ReadOnly="true"></asp:TextBox>
                                    </p>
                                    <p>
                                        <label style="width:300px;">
                                        Collector Recommendation:
                                        </label>
                                        <asp:TextBox ID="txt_RPT_COL_Comment" runat="server" CssClass="text-input" 
                                            Height="80px" MaxLength="500" TextMode="MultiLine" Width="800px"></asp:TextBox>
                                    </p>
                            </asp:Panel>
                            <asp:Panel ID="pnl_Inspector" runat="server">
                                <p>
                                    <label class="column-left" style="width:120px;">
                                    Inspected date:
                                    </label>
                                    <asp:TextBox ID="txt_RPT_INSP_Date" runat="server" 
                                        CssClass="text-input small-input" ReadOnly="true"></asp:TextBox>
                                </p>
                                <p>
                                    <label class="column-left" style="width:120px;">
                                    Inspector:
                                    </label>
                                    <asp:TextBox ID="txt_RPT_INSP_By" runat="server" 
                                        CssClass="text-input small-input" ReadOnly="true"></asp:TextBox>
                                </p>
                                <p>
                                    <label style="width:300px;">
                                    Inspector Recommendation:
                                    </label>
                                    <asp:TextBox ID="txt_RPT_INSP_Comment" runat="server" CssClass="text-input" 
                                        Height="80px" MaxLength="500" TextMode="MultiLine" Width="800px"></asp:TextBox>
                                </p>
                            </asp:Panel>
                            <asp:Panel ID="pnl_Analyst" runat="server">
                                <p>
                                    <label class="column-left" style="width:120px;">
                                    Approved date:
                                    </label>
                                    <asp:TextBox ID="txt_RPT_ANL_Date" runat="server" 
                                        CssClass="text-input small-input" ReadOnly="true"></asp:TextBox>
                                </p>
                                <p>
                                    <label class="column-left" style="width:120px;">
                                    Approver:
                                    </label>
                                    <asp:TextBox ID="txt_RPT_ANL_By" runat="server" 
                                        CssClass="text-input small-input" ReadOnly="true"></asp:TextBox>
                                </p>
                                <p>
                                    <label style="width:300px;">
                                    Approver Recommendation:
                                    </label>
                                    <asp:TextBox ID="txt_RPT_ANL_Comment" runat="server" CssClass="text-input" 
                                        Height="80px" MaxLength="500" TextMode="MultiLine" Width="800px"></asp:TextBox>
                                </p>
                            </asp:Panel>
                            <div class="clear">
                            </div>
                            <!-- End .clear -->
                            <p align="right">
                                <asp:Button ID="btn_Back" runat="server" CssClass="button" Text="Back" />
                                <asp:Button ID="btn_Send_Collector" runat="server" CssClass="button" 
                                    Text="Send to collector" />
                                <asp:Button ID="btn_Send_Inspector" runat="server" CssClass="button" 
                                    Text="Send to inspector" />
                                <asp:Button ID="btn_Send_Analyst" runat="server" CssClass="button" 
                                    Text="send to approver" />
                                <asp:Button ID="btn_Posted" runat="server" CssClass="button" 
                                    Text="Post approved" />
                            </p>
			        
							</fieldset>
		            </div>
		          				   
		       	 <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
                
			        <uc1:GL_DialogInputValue ID="DialogInput" runat="server" Visible="false" />
                
			</ContentTemplate>
	    </asp:UpdatePanel>
	    
</asp:Content>