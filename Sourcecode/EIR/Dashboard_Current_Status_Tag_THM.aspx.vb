﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Current_Status_Tag_THM
    Inherits System.Web.UI.Page
    Dim BL As New EIR_BL

    Private Property RPT_Year() As Integer
        Get
            If IsNumeric(ViewState("RPT_Year")) Then
                Return ViewState("RPT_Year")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_Year") = value
        End Set
    End Property
    Private Property RPT_No() As Integer
        Get
            If IsNumeric(ViewState("RPT_No")) Then
                Return ViewState("RPT_No")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RPT_No") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            RPT_Year = Request.QueryString("RPT_Year")
            RPT_No = Request.QueryString("RPT_No")

            Dim Sql As String = ""

            Sql &= "SELECT dbo.UDF_RPT_Code(RPT_Year, RPT_No) RPT_CODE,THM_TYPE_Name,PLANT_Name,ROUTE_Code," & vbNewLine
            Sql &= "/*Ref_Report_No,*/Result_FileName,/*Inspected_Date,*/RPT_THM_Header.THM_TYPE_ID,RPT_THM_Header.PLANT_ID,RPT_THM_Header.ROUTE_ID" & vbNewLine
            Sql &= "FROM RPT_THM_Header " & vbNewLine
            Sql &= "LEFT JOIN MS_THM_Type " & vbNewLine
            Sql &= "ON RPT_THM_Header.THM_TYPE_ID = MS_THM_Type.THM_TYPE_ID" & vbNewLine
            Sql &= "LEFT JOIN MS_Plant " & vbNewLine
            Sql &= "ON RPT_THM_Header.PLANT_ID = MS_Plant.PLANT_ID" & vbNewLine
            Sql &= "LEFT JOIN MS_THM_Route" & vbNewLine
            Sql &= "ON RPT_THM_Header.ROUTE_ID = MS_THM_Route.ROUTE_ID" & vbNewLine
            Sql &= "WHERE RPT_Year=" & RPT_Year & " and RPT_No=" & RPT_No
            Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                lblReportNo.Text = DT.Rows(0).Item("RPT_CODE").ToString
                lblEquipement.Text = DT.Rows(0).Item("THM_TYPE_Name").ToString
                lblPlant.Text = DT.Rows(0).Item("PLANT_Name").ToString
                lblRoute.Text = DT.Rows(0).Item("ROUTE_Code").ToString
                'lblRef.Text = DT.Rows(0).Item("Ref_Report_No").ToString
                lblResultFileName.Text = DT.Rows(0).Item("Result_FileName").ToString
                'lblIspectedDate.Text = BL.ReportProgrammingDate(DT.Rows(0).Item("Inspected_Date"))

                lblResultFileName.Attributes.Add("onClick", "window.open('ViewfileTHM.aspx?RPT_Year=" & RPT_Year & "&RPT_No=" & RPT_No & "','Dialog_Tag_THM',' scrollbars,resizable,center');")

                Sql = ""
                Sql &= "SELECT MS_THM_Tag.TAG_ID,TAG_CODE,TAG_NAME,TAG_STATUS,Result_FileName  " & vbCrLf
                Sql &= "FROM MS_THM_Tag" & vbCrLf
                Sql &= "Left Join" & vbCrLf
                Sql &= "(" & vbCrLf
                Sql &= "    SELECT TAG_ID,TAG_STATUS,Result_FileName  FROM RPT_THM_DETAIL" & vbCrLf
                Sql &= "    WHERE(RPT_Year = " & RPT_Year & " And RPT_No = " & RPT_No & ")" & vbCrLf
                Sql &= ") TagStatus " & vbCrLf
                Sql &= "ON MS_THM_Tag.TAG_ID = TagStatus.TAG_ID" & vbCrLf
                Sql &= "WHERE THM_TYPE_ID = " & DT.Rows(0).Item("THM_TYPE_ID").ToString & " AND PLANT_ID = " & DT.Rows(0).Item("PLANT_ID").ToString & " AND ROUTE_ID = " & DT.Rows(0).Item("ROUTE_ID").ToString & vbCrLf
                Sql &= "ORDER BY TAG_CODE" & vbCrLf

                DT = New DataTable
                DA = New SqlDataAdapter(Sql, BL.ConnStr)
                DA.Fill(DT)
                rptTag.DataSource = DT
                rptTag.DataBind()
            End If


        End If

    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblTagCode As Label = e.Item.FindControl("lblTagCode")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim btnOK As HtmlInputButton = e.Item.FindControl("btnOK")
        Dim btnAB As HtmlInputButton = e.Item.FindControl("btnAB")
        Dim btnNA As HtmlInputButton = e.Item.FindControl("btnNA")

        Dim tr As HtmlTableRow = e.Item.FindControl("tr")

        lblTagCode.Text = e.Item.DataItem("TAG_CODE").ToString
        lblTagName.Text = e.Item.DataItem("TAG_NAME").ToString
        lblTagCode.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID").ToString

        If IsDBNull(e.Item.DataItem("TAG_STATUS")) Then
            btnNA.Attributes("class") = "btnNA"
        ElseIf e.Item.DataItem("TAG_STATUS") = 0 Then
            btnOK.Attributes("class") = "btnOK"
        Else
            btnAB.Attributes("class") = "btnAB"
        End If

        If Not IsDBNull(e.Item.DataItem("Result_FileName")) Then
            Dim FileName As String = BL.PostedReport_Path & "\THM\" & lblReportNo.Text & "\" & e.Item.DataItem("Result_FileName")
            If IO.File.Exists(FileName) Then
                tr.Attributes("style") = "cursor:pointer;"
                tr.Attributes("onclick") = "window.open('Render_THM_File.aspx?Mode=TAG&File=" & e.Item.DataItem("Result_FileName").ToString & "','THM_" & e.Item.DataItem("Result_FileName") & "');"
            End If
        End If

    End Sub

End Class