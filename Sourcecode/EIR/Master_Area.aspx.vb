﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Master_Area
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetArea(Nothing, Nothing)
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindArea()

        Dim SQL As String = "SELECT " & vbNewLine
        SQL &= " MS_AREA.AREA_ID," & vbNewLine
        SQL &= "  AREA_Code," & vbNewLine
        SQL &= " AREA_Name," & vbNewLine
        SQL &= " PLANT_Code," & vbNewLine
        SQL &= " COUNT(TAG_ID) TotalTag," & vbNewLine
        SQL &= " AREA_Description," & vbNewLine
        SQL &= " MS_AREA.active_status," & vbNewLine
        SQL &= " MS_AREA.Update_By," & vbNewLine
        SQL &= " MS_AREA.Update_Time" & vbNewLine
        SQL &= " FROM MS_AREA" & vbNewLine
        SQL &= " LEFT JOIN MS_Plant ON MS_Plant.PLANT_ID=MS_AREA.PLANT_ID " & vbNewLine
        SQL &= " LEFT JOIN (SELECT * FROM MS_ST_TAG WHERE Active_Status=1 UNION ALL SELECT * FROM MS_RO_TAG WHERE Active_Status=1) MS_TAG ON MS_TAG.AREA_ID=MS_AREA.AREA_ID" & vbNewLine
        Dim WHERE As String = ""
        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " MS_Plant.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND "
        End If
        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If
        SQL &= " GROUP BY " & vbNewLine
        SQL &= " MS_AREA.AREA_ID," & vbNewLine
        SQL &= " AREA_Code," & vbNewLine
        SQL &= " AREA_Name," & vbNewLine
        SQL &= "  PLANT_Code," & vbNewLine
        SQL &= " AREA_Description," & vbNewLine
        SQL &= " MS_AREA.active_status," & vbNewLine
        SQL &= " MS_AREA.Update_By," & vbNewLine
        SQL &= " MS_AREA.Update_Time" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("MS_Area") = DT

        Navigation.SesssionSourceName = "MS_Area"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptArea
    End Sub

    Protected Sub rptArea_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptArea.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblAreaCode As Label = e.Item.FindControl("lblAreaCode")
        Dim lblPlantCode As Label = e.Item.FindControl("lblPlantCode")
        Dim lblAreaName As Label = e.Item.FindControl("lblAreaName")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblDesc As Label = e.Item.FindControl("lblDesc")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblAreaCode.Text = e.Item.DataItem("AREA_Code")
        lblPlantCode.Text = e.Item.DataItem("Plant_Code")
        lblAreaName.Text = e.Item.DataItem("AREA_NAME")
        lblTag.Text = e.Item.DataItem("TotalTag")
        lblDesc.Text = BL.ReportGridDescription(e.Item.DataItem("AREA_Description"), 20)
        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.Attributes("Area_ID") = e.Item.DataItem("Area_ID")

    End Sub

    Protected Sub rptArea_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptArea.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim Area_ID As Integer = btnEdit.Attributes("Area_ID")
        Select Case e.CommandName
            Case "Edit"
                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True

                'txtAreaCode.ReadOnly = True
                txtAreaName.Focus()
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListArea.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT * FROM MS_AREA WHERE AREA_ID=" & Area_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "Area Not Found"
                    pnlBindingError.Visible = True
                    BindArea()
                    Exit Sub
                End If

                txtAreaCode.Text = DT.Rows(0).Item("AREA_CODE")
                txtAreaCode.Attributes("AreaID") = DT.Rows(0).Item("AREA_ID")
                txtAreaName.Text = DT.Rows(0).Item("AREA_Name")
                If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then
                    BL.BindDDlPlant(ddl_Edit_Plant, DT.Rows(0).Item("PLANT_ID"))
                Else
                    BL.BindDDlPlant(ddl_Edit_Plant)
                End If
                txtDesc.Text = DT.Rows(0).Item("AREA_Description")
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")

                btnSave.Focus()

            Case "ToggleStatus"
                Dim SQL As String = "UPDATE MS_AREA Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  AREA_ID=" & Area_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindArea()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
        End Select


    End Sub

    Protected Sub ResetArea(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindArea()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------
        BL.BindDDlPlant(ddl_Search_Plant)

        pnlListArea.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""
        txtAreaCode.Text = ""
        txtAreaCode.Attributes("AreaID") = "0"
        txtAreaName.Text = ""
        BL.BindDDlPlant(ddl_Edit_Plant)
        txtDesc.Text = ""
        chkAvailable.Checked = True

        btnCreate.Visible = True

    End Sub


    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        ClearPanelEdit()
        pnlEdit.Visible = True

        btnCreate.Visible = False

        txtAreaCode.ReadOnly = False
        txtAreaCode.Focus()
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListArea.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtAreaCode.Text = "" Then
            lblValidation.Text = "Please insert Area Code"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If txtAreaName.Text = "" Then
            lblValidation.Text = "Please insert Area Name"
            pnlValidation.Visible = True
            Exit Sub
        End If
        If ddl_Edit_Plant.SelectedIndex < 1 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If

        Dim Area_ID As Integer = txtAreaCode.Attributes("AreaID")

        Dim SQL As String = "SELECT * FROM MS_AREA WHERE Area_Code='" & txtAreaCode.Text.Replace("'", "''") & "' AND Area_ID<>" & Area_ID & " AND PLANT_ID=" & ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblValidation.Text = "This Area Code is already exists"
            pnlValidation.Visible = True
            Exit Sub
        End If

        SQL = "SELECT * FROM MS_AREA WHERE Area_ID=" & Area_ID
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            Area_ID = GetNewAreaID()
            DR("AREA_ID") = Area_ID
        Else
            DR = DT.Rows(0)
        End If

        DR("AREA_ID") = Area_ID
        DR("PLANT_ID") = ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value
        DR("AREA_Code") = txtAreaCode.Text
        DR("AREA_Name") = txtAreaName.Text
        DR("AREA_Description") = txtDesc.Text
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID") ' Remain
        DR("Update_Time") = Now


        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetArea(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

    End Sub

    Private Function GetNewAreaID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(AREA_ID),0)+1 FROM MS_AREA "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function
    
    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BindArea()
    End Sub


End Class