﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_PIPE_CUI_ERO_Class.ascx.vb" Inherits="EIR.UC_PIPE_CUI_ERO_Class" %>

<table id="CUI_ERO_Class">                
        <tr>
            <td style="background-color:lightsteelblue; text-align:center;" colspan="5">
                <b>Class Specification 
                    <asp:LinkButton ID="lnkSuggessClass" runat="server"></asp:LinkButton>
                </b>
            </td>                        
        </tr>
        <tr>
            <td style="text-align:center; background-color:#f5f5f5;" colspan="2">Level</td>
            <td style="text-align:center; background-color:#f5f5f5;">Action</td>
            <td style="text-align:center; background-color:#f5f5f5;">Reiod</td>
            <td style="text-align:center; background-color:#f5f5f5;">Responsibility</td>
        </tr>
        <tr id="trClassA" runat="server">
            <td>
                <b><asp:RadioButton ID="rdoClassA" runat="server" GroupName="class" AutoPostBack="true" style="margin-right:10px;" /> Class A : Major</b>
            </td>
            <td>
                <img src="resources/images/PipeClassA.png" style="height:100px;" />
            </td>
            <td> 
                <li> Stop leak/change bolt &nut</li>
                <li> Cut & change</li>
                <li> Clean & paint</li> 
            </td>
            <td>With in 7 days</td>
            <td>
                <li> Engineering Div.</li> 
                <li> Maintenance Div.</li> 
            </td>
        </tr>
        <tr id="trClassB" runat="server">
            <td>
                <b><asp:RadioButton ID="rdoClassB" runat="server" GroupName="class" AutoPostBack="true" style="margin-right:10px;"  /> Class B : Middle</b>
            </td>
            <td>
                <img src="resources/images/PipeClassB.png" style="height:100px;" />
            </td>
            <td> 
                <li> Clean & paint</li>
                <li> Change insulation</li>
                <li> Change nut& bolt</li> 
                <li> Monitor condition</li> 
                <li> Belzona</li> 
            </td>
            <td>With in 30 days</td>
            <td>
                <li> Engineering Div.</li> 
                <li> Maintenance Div.</li> 
            </td>
        </tr>
        <tr id="trClassC" runat="server">
            <td>
                <b><asp:RadioButton ID="rdoClassC" runat="server" GroupName="class" AutoPostBack="true" style="margin-right:10px;"  /> Class C : Minor</b>
            </td>
            <td>
                <img src="resources/images/PipeClassC.png" style="height:100px;" />
            </td>
            <td> 
                <li> Clean & paint</li>
            </td>
            <td>With in 60 days</td>
            <td>
                <li> Engineering Div.</li>
                <li> Maintenance Div.</li>
            </td>
        </tr>
        <tr id="trClassNormal" runat="server" >
            <td colspan="5"><b><asp:RadioButton ID="rdoNormal" runat="server" GroupName="class" AutoPostBack="true" style="margin-right:10px;"  /> No Problem</b></td>                      
        </tr>
</table>    