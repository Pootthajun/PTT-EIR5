﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Stationary_Routine_Edit1.aspx.vb" Inherits="EIR.Stationary_Routine_Edit1" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

	<!-- Page Head -->
	 <asp:UpdatePanel ID="UDPMain" runat="Server">
	   	    <ContentTemplate>
	    
	    
			<h2>Edit Stationary Routine Report</h2>
			
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3><asp:Label ID="lblReportCode" runat="server"></asp:Label></h3>
					
					
					<ul class="content-box-tabs">
						<li><asp:LinkButton id="HTabHeader" runat="server" CssClass="default-tab current">Report Header</asp:LinkButton></li> <!-- href must be unique and match the id of target div -->
						<li><asp:LinkButton id="HTabDetail" runat="server">Report Detail</asp:LinkButton></li>
						<li><asp:LinkButton id="HTabSummary" runat="server">Report Summary</asp:LinkButton></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
				 	<!--tabHeader -->
				 	<div class="tab-content current">
				 
				         <fieldset> 
								
								<p style="font-weight:bold;">
								<label class="column-left" style="width:120px;" >Report for: </label>
								<asp:Label ID="lbl_Plant" runat="server" Text="Plant" CssClass="EditReportHeader" ></asp:Label>
								<asp:Label ID="lbl_Route" runat="server" Text="Route" CssClass="EditReportHeader" ></asp:Label>
								    | Year <asp:Label ID="lbl_Year" runat="server" Text="Route" CssClass="EditReportHeader"></asp:Label>
								    | Round <asp:Label ID="lbl_Round" runat="server" Text="Round" CssClass="EditReportHeader"></asp:Label>
								    | Period <asp:Label ID="lbl_Period" runat="server" Text="Period" CssClass="EditReportHeader" ></asp:Label>
								</p>								
								
							<div class="clear"></div><!-- End .clear -->
								<p>
									<label class="column-left" style="width:120px;" >Subject: </label>
									<asp:TextBox Width="536px" CssClass="text-input" ID="txt_RPT_Subject" MaxLength="500" runat="server"></asp:TextBox>									
								</p>
								
								<p>
									<label class="column-left" style="width:120px;" >To: </label>
									<asp:TextBox Width="536px" CssClass="text-input" ID="txt_RPT_To" MaxLength="100" runat="server"></asp:TextBox>
								</p>
								
								<p>
									<label style="width:150px;" >Title: </label>
									<asp:TextBox Width="800px" TextMode="MultiLine" ID="txt_RPT_Cause"  Height="80px" CssClass="text-input" MaxLength="1000" runat="server"></asp:TextBox>
									
								</p>
								
								<p>
									<label class="column-left" style="width:120px;" >Created date: </label>
									<asp:TextBox CssClass="text-input small-input" ID="txt_Created_Time" ReadOnly="true" runat="server"></asp:TextBox>
									
								</p>
								
								<p>
									<label class="column-left" style="width:120px;" >Created by: </label>
									<asp:TextBox CssClass="text-input small-input" ID="txt_Created_By" ReadOnly="true" runat="server"></asp:TextBox>
								</p>
								<p>
									<label class="column-left" style="width:120px;" >Step : </label>
									<asp:TextBox CssClass="text-input small-input" ID="txt_RPT_STEP" 
                                        ReadOnly="true" runat="server" Font-Bold="True"></asp:TextBox>
								</p>								
							
							<h3 style="margin-top:20px;">Officer</h3>
							
							    <p>
									<label class="column-left" style="width:120px;" >Collector : </label>
									<asp:DropDownList CssClass="text-input small-input" ID="cmbCollector" Width="250px"
                                       runat="server" Font-Bold="True"></asp:DropDownList>									
								</p>
							    <p>
									<label class="column-left" style="width:120px;" >Inspector : </label>
									<asp:DropDownList CssClass="text-input small-input" ID="cmbInspector" Width="250px"
                                       runat="server" Font-Bold="True"></asp:DropDownList>									
								</p>
								<p>
									<label class="column-left" style="width:120px;" >Engineer : </label>
									<asp:DropDownList CssClass="text-input small-input" ID="cmbEngineer" Width="250px"
                                       runat="server" Font-Bold="True"></asp:DropDownList>									
								</p>
								<p>
									<label class="column-left" style="width:120px;" >Approver : </label>
									<asp:DropDownList CssClass="text-input small-input" ID="cmbAnalyst" Width="250px"
                                       runat="server" Font-Bold="True"></asp:DropDownList>									
								</p>								
							
							<p align="right">
								<asp:Button ID="btn_Next" runat="server" CssClass="button" Text="Next" />
							</p>
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
				   </div>
			   
		       	 <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg">
                    <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" 
                        ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                    <div>
                        <asp:Label ID="lblValidation" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
                </div>
			</ContentTemplate>
	 </asp:UpdatePanel>
			
			
</asp:Content>
