﻿Imports System.IO
Imports System.Data.SqlClient

Public Class LAW_Document_Summary
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim CL As New LawClass
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ClearPanelSearch()
            BindDocumentPlan()
            BindNotificationList()

            Dim TempPath As String = CL.UploadTempPath() & Session("USER_Name") & "\"
            If Directory.Exists(TempPath) = True Then
                Directory.Delete(TempPath, True)
            End If
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        'pnlValidation.Visible = False
        pnlBindingError.Visible = False
        'pnlBindingSuccess.Visible = False
    End Sub

    'Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
    '    pnlValidation.Visible = False
    'End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    'Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
    '    pnlBindingSuccess.Visible = False
    'End Sub
#End Region

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()

        Dim SQL As String = "select  2550  Min_Year  ,DATEPART(YYYY,GETDATE()) + 543+10  Max_Year "
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        DA.Fill(DT)
        ddlSearchYear.Items.Clear()
        ddlSearchYear.Items.Add("All  Year...")
        For i As Integer = DT.Rows(0).Item("Min_Year") To DT.Rows(0).Item("Max_Year")
            ddlSearchYear.Items.Add(i)
        Next
        ddlSearchYear.Text = Now.Year + 543



        CL.BindAllDocumentList(ddlSearchDocTemplete)
        BL.BindDDlPlant(ddlSearchPlant, False)
        CL.BindDDLTagID(ddlSearchTag, 0, 0, 0)
    End Sub

    Private Sub ddlSearchPlant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchPlant.SelectedIndexChanged
        If ddlSearchPlant.SelectedIndex > -1 Then
            CL.BindDDLTagID(ddlSearchTag, ddlSearchPlant.SelectedValue, 0, 0)
            BindDocumentPlan()
            BindNotificationList()
        End If
    End Sub

    Private Sub ddlSearchDocTemplete_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchDocTemplete.SelectedIndexChanged
        If ddlSearchDocTemplete.SelectedIndex > -1 Then
            BindDocumentPlan()
            BindNotificationList()
        End If
    End Sub

    Private Sub ddlSearchYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchYear.SelectedIndexChanged
        If ddlSearchYear.SelectedIndex > -1 Then
            BindDocumentPlan()
            BindNotificationList()
        End If
    End Sub

    Private Sub ddlSearchTag_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSearchTag.SelectedIndexChanged
        If ddlSearchTag.SelectedIndex > -1 Then
            BindDocumentPlan()
            BindNotificationList()
        End If
    End Sub


#End Region

#Region "Document Plan"
    Public Sub BindDocumentPlan()
        Dim SearchYear As Integer = 0
        If ddlSearchYear.SelectedIndex > 0 Then
            SearchYear = ddlSearchYear.Items(ddlSearchYear.SelectedIndex).Value
        End If

        Dim Template_Name As String = ""
        If ddlSearchDocTemplete.SelectedIndex > 0 Then
            Template_Name = ddlSearchDocTemplete.SelectedItem.ToString
        End If

        Dim dt As DataTable = CL.GetListDocumentAllPlan(Template_Name, SearchYear, ddlSearchPlant.SelectedValue, ddlSearchTag.SelectedValue)
        rpt.DataSource = dt
        rpt.DataBind()

        'pnlDashboard.Visible = False
        If dt.Rows.Count > 0 Then

            Dim Sum_PercentWeight As Object = 0
            Sum_PercentWeight = dt.Compute("SUM(percent_weight)", " percent_weight IS NOT NULL")
            If Sum_PercentWeight > 0 Then
                'pnlDashboard.Visible = True

            Else


            End If
            DisplayChart()
        End If

        lblTopTitle.Text = "Overall progress on " & ddlSearchYear.SelectedItem.ToString()
        If ddlSearchPlant.SelectedIndex > 0 Then
            lblTopTitle.Text += " " & "Plant :" & ddlSearchPlant.SelectedItem.ToString()
        End If
        If ddlSearchTag.SelectedIndex > 0 Then
            lblTopTitle.Text += " " & "TAG CODE :" & ddlSearchTag.SelectedItem.ToString()
        End If

    End Sub

    Private Sub rpt_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rpt.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblDocumentName As Label = e.Item.FindControl("lblDocumentName")
        Dim lblDocumentPlanID As Label = e.Item.FindControl("lblDocumentPlanID")
        Dim lblDocumentYear As Label = e.Item.FindControl("lblDocumentYear")
        Dim lblPlantCode As Label = e.Item.FindControl("lblPlantCode")
        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblPlanStatus As Label = e.Item.FindControl("lblPlanStatus")
        Dim lblPercentComplete As Label = e.Item.FindControl("lblPercentComplete")
        Dim txtPercentWeight As TextBox = e.Item.FindControl("txtPercentWeight")
        Dim lblNoticeDate As Label = e.Item.FindControl("lblNoticeDate")
        Dim lblCriticalDate As Label = e.Item.FindControl("lblCriticalDate")
        Dim lblUploadDate As Label = e.Item.FindControl("lblUploadDate")

        lblDocumentName.Text = e.Item.DataItem("document_name")
        lblDocumentPlanID.Text = e.Item.DataItem("document_plan_id")
        lblDocumentYear.Text = e.Item.DataItem("document_year")
        lblPlantCode.Text = e.Item.DataItem("plant_code")
        lblTagNo.Text = e.Item.DataItem("tag_no")

        Dim pStatus As DocumentPlanStatusClass = CL.GetDocumentPlanStatus(e.Item.DataItem("document_plan_id"))
        lblPercentComplete.Text = pStatus.PercentComplete


        If Convert.IsDBNull(e.Item.DataItem("percent_weight")) = False Then txtPercentWeight.Text = e.Item.DataItem("percent_weight")

        'lblNoticeDate.Text = pStatus.NoticeDate.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("th-TH"))
        'lblCriticalDate.Text = pStatus.CriticalDate.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("th-TH"))

        If Convert.ToDateTime(pStatus.NoticeDate).Year <> 1 Then
            lblNoticeDate.Text = BL.ReportGridTime(pStatus.NoticeDate)
        End If
        If Convert.ToDateTime(pStatus.CriticalDate).Year <> 1 Then
            lblCriticalDate.Text = BL.ReportGridTime(pStatus.CriticalDate)
        End If

        'lblNoticeDate.Text = BL.ReportGridTime(pStatus.NoticeDate)
        'lblCriticalDate.Text = BL.ReportGridTime(pStatus.CriticalDate)

        If pStatus.UploadDate.Year <> 1 Then
            lblUploadDate.Text = BL.ReportGridTime(pStatus.UploadDate)
        End If

        Select Case pStatus.PlanStatus
            Case "WAITING"
                lblPlanStatus.Text = "<span style='color:blue'><b>WAITING</b></span>"
            Case "NOTICE"
                lblPlanStatus.Text = "<span style='color:orange'><b>NOTICE</b></span>"
            Case "CRITICAL"
                lblPlanStatus.Text = "<span style='color:red'><b>LATE</b></span><img src='resources/images/icons/alert.gif' />"
            Case "COMPLETE"
                lblPlanStatus.Text = "<span style='color:green'><b>COMPLETE</b></span>"
        End Select

        ImplementJavaNumericText(txtPercentWeight)

        'Dim pDt As DataTable = CL.GetListDocumentPlanPaper(lblDocumentPlanID.Text)
        'Dim UC_DocumentTreePlanPaper1 As UC_DocumentTreePlanPaper = e.Item.FindControl("UC_DocumentTreePlanPaper1")
        'UC_DocumentTreePlanPaper1.SetPlanPaperData(pDt)
    End Sub

    Protected Sub UC_DocumentTreePlanPaper1_SetPercentComplete(sender As UC_DocumentTreePlanPaper, PercentComplete As Integer)
        Dim UC As UC_DocumentTreePlanPaper = DirectCast(sender, UC_DocumentTreePlanPaper)
        Dim RowItem As RepeaterItem = UC.Parent.Parent.Parent

        Dim lblPercentComplete As Label = RowItem.FindControl("lblPercentComplete")
        Dim lblUploadDate As Label = RowItem.FindControl("lblUploadDate")

        lblPercentComplete.Text = PercentComplete
        lblUploadDate.Text = DateTime.Now.ToString("dd-MMM-yyyy", New Globalization.CultureInfo("th-TH"))
    End Sub


    'Dim urlExpand As String = "resources/images/tree_expanded.png"
    'Dim urlCollapsed As String = "resources/images/tree_collapsed.png"
    'Dim urlNoChild As String = "resources/images/tree_nochild.png"

    Protected Sub btnEdit_Click(sender As Object, e As EventArgs)
        Dim img As ImageButton = DirectCast(sender, ImageButton)
        Dim RowItem As RepeaterItem = img.Parent
        Dim lblDocumentPlanID As Label = RowItem.FindControl("lblDocumentPlanID")

        Response.Redirect("LAW_Document_Summary_Detail.aspx?id=" & lblDocumentPlanID.Text)
    End Sub

    Protected Sub txtPercentWeight_TextChanged(sender As Object, e As System.EventArgs)
        Dim itm As RepeaterItem = DirectCast(sender, TextBox).Parent
        Dim lblDocumentPlanID As Label = itm.FindControl("lblDocumentPlanID")
        Dim PercentWeight As Integer

        If IsNumeric(DirectCast(sender, TextBox).Text) Then
            If DirectCast(sender, TextBox).Text > 0 Then
                PercentWeight = DirectCast(sender, TextBox).Text
            Else
                PercentWeight = 0
            End If
        Else
            PercentWeight = 0
        End If

        'Dim ret As String = CL.UpdatePlanPercentWeight(lblDocumentPlanID.Text, DirectCast(sender, TextBox).Text)

        Dim ret As String = CL.UpdatePlanPercentWeight(lblDocumentPlanID.Text, PercentWeight)
        If ret.ToLower <> "true" Then
            lblValidation.Text = ret
            pnlValidation.Visible = True
            Exit Sub
        Else
            lblValidation.Text = ""
            pnlValidation.Visible = False
            'BindDocumentPlan()

            'Dim ChartDT As DataTable = CL.GetDocumentChartYearData(ddlSearchYear.Items(ddlSearchYear.SelectedIndex).Value)
            '    DisplayChart(ChartDT)
            '
            BindDocumentPlan()
            BindNotificationList()
        End If
    End Sub
#End Region

#Region "Chart"
    Protected Sub DisplayChart()
        Dim SearchYear As Integer = 0
        If ddlSearchYear.SelectedIndex > 0 Then
            SearchYear = ddlSearchYear.Items(ddlSearchYear.SelectedIndex).Value
        End If
        Dim template_Name As String = ""
        If ddlSearchDocTemplete.SelectedIndex > 0 Then
            template_Name = ddlSearchDocTemplete.SelectedItem.ToString()
        End If

        ChartYear.ChartAreas("ChartArea1").AxisX.CustomLabels.Clear()
        'ChartYear.Titles("Title1").Text = "Overall progress on " & ddlSearchYear.SelectedItem.ToString()




        Dim culture As New System.Globalization.CultureInfo("en-US")
        Dim C As New Converter

        Dim PlanValue As Double = 0
        Dim ActualValue As Double = 0

        '=====================
        '---หาวันทั้งหมดที่มี action
        Dim DT_Date As DataTable = CL.GetAllDate_YearData(SearchYear)
        Dim DT_StartDate As DataTable = CL.GetStartDate_YearData(SearchYear)
        Dim DR_Start As DataRow
        If DT_StartDate.Rows.Count > 0 Then
            DR_Start = DT_Date.NewRow
            DR_Start("Action_date") = DT_StartDate.Rows(0).Item("Action_date")
            DT_Date.Rows.Add(DR_Start)
        End If
        DT_Date.DefaultView.Sort = "Action_date asc"
        DT_Date = DT_Date.DefaultView.ToTable

        '----จัด Data Plan----
        Dim DT_Plan As New DataTable
        DT_Plan.Columns.Add("plan_date", GetType(Date))
        DT_Plan.Columns.Add("paper_name")
        DT_Plan.Columns.Add("plan_percent", GetType(Double))
        Dim DR As DataRow
        Dim DT As New DataTable
        DT = CL.GetDocumentChartYearData_Plan(SearchYear, template_Name, ddlSearchPlant.SelectedValue, ddlSearchTag.SelectedValue)

        DT.DefaultView.Sort = " plan_date asc"
        DT = DT.DefaultView.ToTable
        Dim _plan_notice_date As String = ""
        For i As Integer = 0 To DT.Rows.Count - 1
            If (_plan_notice_date <> DT.Rows(i).Item("plan_date").ToString()) Then
                _plan_notice_date = DT.Rows(i).Item("plan_date").ToString()

                Dim DT_paper_Name As New DataTable
                DT.DefaultView.RowFilter = "plan_date='" & DT.Rows(i).Item("plan_date").ToString() & "'"
                DT_paper_Name = DT.DefaultView.ToTable
                Dim List_paper_Name As String = ""
                If (DT_paper_Name.Rows.Count > 1) Then
                    For paper As Integer = 0 To DT_paper_Name.Rows.Count - 1
                        List_paper_Name &= DT_paper_Name.Rows(paper).Item("paper_name").ToString() & " : " & ModuleGlobal.FormatNumericText(DT_paper_Name.Rows(paper).Item("plan_percent")) & " %" & vbNewLine
                    Next
                Else
                    List_paper_Name = DT.Rows(i).Item("paper_name").ToString() & " : " & ModuleGlobal.FormatNumericText(DT.Rows(i).Item("plan_percent")) & " %"
                End If
                DR = DT_Plan.NewRow
                DR("plan_date") = DT.Rows(i).Item("plan_date")
                DR("paper_name") = List_paper_Name
                DR("plan_percent") = DT.Compute("SUM(plan_percent)", " plan_date='" & DT.Rows(i).Item("plan_date") & "'")
                DT_Plan.Rows.Add(DR)

            End If


        Next

        If DT_StartDate.Rows.Count > 0 Then
            DR_Start = DT_Plan.NewRow
            DR_Start("plan_date") = DT_StartDate.Rows(0).Item("Action_date")
            DR_Start("paper_name") = ""
            DR_Start("plan_percent") = 0
            DT_Plan.Rows.Add(DR_Start)
        End If
        DT_Plan.DefaultView.Sort = "plan_date asc"
        DT_Plan = DT_Plan.DefaultView.ToTable


        '----จัด Data Actual----
        Dim DT_Actual As New DataTable
        DT_Actual.Columns.Add("Actual_date", GetType(Date))
        DT_Actual.Columns.Add("paper_name")
        DT_Actual.Columns.Add("actual_percent", GetType(Double))

        DT = CL.GetDocumentChartYearData_Actual(SearchYear, template_Name, ddlSearchPlant.SelectedValue, ddlSearchTag.SelectedValue)
        DT.DefaultView.Sort = " actual_date asc"
        DT.DefaultView.RowFilter = " actual_date IS NOT NULL"
        DT = DT.DefaultView.ToTable
        'For i As Integer = 0 To DT.Rows.Count - 1
        '    DR = DT_Actual.NewRow
        '    DR("Actual_date") = DT.Rows(i).Item("Actual_date")
        '    DR("paper_name") = DT.Rows(i).Item("paper_name").ToString() & " : " & (DT.Rows(i).Item("actual_percent")) & " %"
        '    DR("actual_percent") = DT.Rows(i).Item("actual_percent")
        '    DT_Actual.Rows.Add(DR)
        'Next
        Dim SUM_actual_percent As Integer = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            If (_plan_notice_date <> DT.Rows(i).Item("Actual_date").ToString()) Then
                _plan_notice_date = DT.Rows(i).Item("Actual_date").ToString()

                Dim DT_paper_Name As New DataTable
                DT.DefaultView.RowFilter = "Actual_date='" & DT.Rows(i).Item("Actual_date").ToString() & "'"
                DT_paper_Name = DT.DefaultView.ToTable
                Dim List_paper_Name As String = ""
                If (DT_paper_Name.Rows.Count > 1) Then
                    For paper As Integer = 0 To DT_paper_Name.Rows.Count - 1
                        List_paper_Name &= DT_paper_Name.Rows(paper).Item("paper_name").ToString() & " : " & ModuleGlobal.FormatNumericText(DT_paper_Name.Rows(paper).Item("actual_percent"), 2) & " %" & vbNewLine
                    Next
                Else
                    List_paper_Name = DT.Rows(i).Item("paper_name").ToString() & " : " & ModuleGlobal.FormatNumericText(DT.Rows(i).Item("Actual_percent")) & " %"
                End If
                DR = DT_Actual.NewRow
                DR("Actual_date") = DT.Rows(i).Item("Actual_date")
                DR("paper_name") = List_paper_Name
                DR("actual_percent") = DT.Compute("SUM(actual_percent)", " Actual_date='" & DT.Rows(i).Item("Actual_date") & "'")
                SUM_actual_percent = SUM_actual_percent + DR("actual_percent")
                DT_Actual.Rows.Add(DR)

            End If
        Next

        If DT_StartDate.Rows.Count > 0 Then
            DR_Start = DT_Actual.NewRow
            DR_Start("Actual_date") = DT_StartDate.Rows(0).Item("Action_date")
            DR_Start("paper_name") = ""
            DR_Start("actual_percent") = 0
            DT_Actual.Rows.Add(DR_Start)
        End If
        DT_Actual.DefaultView.Sort = "Actual_date asc"
        DT_Actual = DT_Actual.DefaultView.ToTable


        ''---หาวันทั้งหมดที่มี action
        '----ตรวจสอบ
        If (SUM_actual_percent < 100) Then
            '---หาวันที่ปัจจุบัน เพื่อแสดง series3
            'Dim _Now As String = Convert.ToDateTime(Now).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))
            'Dim DT_Now As DataTable = DT_Date
            'DT_Now.DefaultView.RowFilter = "Action_date='" & _Now.ToString() & "'"
            'If DT_Now.DefaultView.Count = 0 Then
            '    DR_Start = DT_Date.NewRow
            '    DR_Start("Action_date") = Convert.ToDateTime(Now).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))
            '    DT_Date.Rows.Add(DR_Start)
            'End If
        End If

        DT_Date.DefaultView.RowFilter = ""
        DT_Date.DefaultView.Sort = "Action_date asc"
        DT_Date = DT_Date.DefaultView.ToTable


        Dim Actual_Point As Integer = 0
        For i As Integer = 0 To DT_Date.Rows.Count - 1
            Dim XDate As String = Convert.ToDateTime(DT_Date.Rows(i).Item("Action_date")).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US"))

            If XDate = Convert.ToDateTime(Now).ToString("dd-MMM-yyyy", New System.Globalization.CultureInfo("en-US")) Then
                ChartYear.Series("Series3").Points.AddXY(i + 1, 0)
                ChartYear.Series("Series3").Points.AddXY(i + 1, 100)
                ChartYear.Series("Series3").BorderDashStyle = DataVisualization.Charting.ChartDashStyle.DashDot
                ChartYear.Series("Series3").Points(0).ToolTip = "Today"
            End If

            'Plan
            DT_Plan.DefaultView.RowFilter = "plan_date='" & DT_Date.Rows(i).Item("Action_date").ToString() & "'"
            If DT_Plan.DefaultView.Count > 0 Then
                PlanValue = (PlanValue + DT_Plan.DefaultView(0).Item("plan_percent"))
                Dim P As Integer = DT_Plan.Rows.IndexOf(DT_Plan.DefaultView(0).Row)
                ChartYear.Series("Series1").Points.AddXY(i + 1, PlanValue)

                If i > 0 Then
                    ChartYear.Series("Series1").Points(P).ToolTip = "Plan = " & PlanValue & " %" & vbNewLine & XDate & vbNewLine '& DT_Plan.DefaultView(0)("paper_name").ToString()
                End If

                ChartYear.Series("Series1").MarkerStyle = DataVisualization.Charting.MarkerStyle.Circle
                ChartYear.Series("Series1").MarkerSize = 6
            End If

            'Actual
            DT_Actual.DefaultView.RowFilter = "Actual_date='" & DT_Date.Rows(i).Item("Action_date").ToString() & "'"
            If DT_Actual.DefaultView.Count > 0 Then

                For j As Integer = 0 To DT_Actual.DefaultView.Count - 1

                    ActualValue = (ActualValue + DT_Actual.DefaultView(j).Item("actual_percent"))
                    Dim A As Integer = DT_Actual.Rows.IndexOf(DT_Actual.DefaultView(j).Row)

                    ChartYear.Series("Series2").Points.AddXY(i + 1, ActualValue)
                    If i > 0 Then
                        ChartYear.Series("Series2").Points(Actual_Point).ToolTip = "Actual = " & ActualValue & " %" & vbNewLine & XDate & vbNewLine '& DT_Actual.DefaultView(j)("paper_name").ToString()
                    End If

                    ChartYear.Series("Series2").Points(Actual_Point).MarkerSize = 6
                    ChartYear.Series("Series2").MarkerStyle = DataVisualization.Charting.MarkerStyle.Circle

                    Actual_Point = Actual_Point + 1
                Next

            Else

                If DT_Actual.Rows.Count > 0 Then
                    If (DT_Date.Rows(i).Item("Action_date") < DT_Actual.Rows(DT_Actual.Rows.Count - 1).Item("Actual_date")) Then
                        ChartYear.Series("Series2").Points.AddXY(i + 1, ActualValue)
                        If i > 0 Then
                            ChartYear.Series("Series2").Points(Actual_Point).ToolTip = ""
                        End If

                        ChartYear.Series("Series2").Points(Actual_Point).MarkerSize = 3
                        Actual_Point = Actual_Point + 1
                        'End If
                    End If
                End If



            End If

                If i = 0 Then
                ChartYear.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(i, i + 2, "")
            Else
                ChartYear.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(i, i + 2, XDate)
            End If


            ChartYear.ChartAreas("ChartArea1").AxisX.LabelStyle.Angle = -45
        Next

    End Sub
#End Region

#Region "Notification List"
    Private Sub BindNotificationList()

        Dim template_Name As String = ""
        Dim Plant_Code As String = ""
        Dim Tag_Code As String = ""
        If ddlSearchDocTemplete.SelectedIndex > 0 Then
            template_Name = ddlSearchDocTemplete.SelectedItem.ToString()
        End If
        If ddlSearchPlant.SelectedIndex > 0 Then
            Plant_Code = ddlSearchPlant.SelectedItem.ToString()
        End If
        If ddlSearchTag.SelectedIndex > 0 Then
            Tag_Code = ddlSearchTag.SelectedItem.ToString()
        End If
        Dim SearchYear As Integer = 0
        If ddlSearchYear.SelectedIndex > 0 Then
            SearchYear = ddlSearchYear.Items(ddlSearchYear.SelectedIndex).Value
        End If

        Dim dt As DataTable = CL.GetDocumentNotificationList(SearchYear, template_Name, Plant_Code, Tag_Code)

        If dt.Rows.Count > 0 Then
            rptNotification.DataSource = dt
            rptNotification.DataBind()
        Else
            rptNotification.DataSource = Nothing
            rptNotification.DataBind()
        End If
    End Sub

    Private Sub rptNotification_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptNotification.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblNoticeDocumentName As Label = e.Item.FindControl("lblNoticeDocumentName")
        Dim lblOrgName As Label = e.Item.FindControl("lblOrgName")
        Dim lblNoticeTemplateName As Label = e.Item.FindControl("lblNoticeTemplateName")
        Dim lblNoticePlantCode As Label = e.Item.FindControl("lblNoticePlantCode")
        Dim lblNoticeTagNo As Label = e.Item.FindControl("lblNoticeTagNo")
        Dim lblNoticeDate As Label = e.Item.FindControl("lblNoticeDate")
        Dim btnNoticeEdit As ImageButton = e.Item.FindControl("btnNoticeEdit")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        Select Case e.Item.DataItem("Status").ToString
            Case "NOTICE"
                lblStatus.Text = "<img src='resources/images/icons/warningPNG.png' title ='NOTICE' />"
            Case "CRITICAL"
                lblStatus.Text = "<img src='resources/images/icons/AlertPNG.png'  title ='LATE' />"

        End Select

        lblNoticeDocumentName.Text = e.Item.DataItem("paper_name")
        lblOrgName.Text = e.Item.DataItem("org_name")
        lblNoticeTemplateName.Text = e.Item.DataItem("template_name")
        lblNoticePlantCode.Text = e.Item.DataItem("PLANT_Code")

        lblNoticeTagNo.Text = e.Item.DataItem("Tag_Code")
        lblNoticeDate.Text = BL.ReportGridTime(e.Item.DataItem("plan_notice_date"))
        btnNoticeEdit.CommandArgument = e.Item.DataItem("document_plan_id")
    End Sub

    Private Sub rptNotification_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptNotification.ItemCommand
        Dim btnNoticeEdit As ImageButton = e.Item.FindControl("btnNoticeEdit")
        Select Case e.CommandName
            Case "NoticeEdit"
                Response.Redirect("LAW_Document_Summary_Detail.aspx?id=" & btnNoticeEdit.CommandArgument)
        End Select
    End Sub
#End Region
End Class