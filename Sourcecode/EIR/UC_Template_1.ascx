﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Template_1.ascx.vb" Inherits="EIR.UC_Template_1" %>
<%@ Register Src="~/Summernote/UC_SummerNote.ascx" TagPrefix="uc1" TagName="UC_SummerNote" %>

<asp:Label ID="lblProperty" runat="server" Text="" Style="display: none;"></asp:Label>

<tr style="text-align: center">
    <td style="width: 50%; text-align: center;" colspan="2">
         <table style="background-color: white;">
            <tr style="border-style: solid; border-width: thin; border-color: #C0C0C0;">
                <td style="vertical-align: middle;text-align :center ;">
                    <asp:ImageButton ID="ImgPreview1" onError="this.src='resources/images/File_Sector.png'" runat="server" AlternateText="..." Style="cursor: pointer; text-align: center; max-width: 100%; min-width: 50%;" />
                   <asp:Button ID="btnRefreshImage1" runat="server" Style="display: none;" />
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="2">
        <table style="background-color: white;">
            <tr>
                <td>
                    <uc1:UC_SummerNote runat="server" ID="TextEditor1" />
                </td>
            </tr>
        </table>
    </td>

</tr>
