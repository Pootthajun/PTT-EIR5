﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="ST_TA_TAG_Spec.aspx.vb" Inherits="EIR.ST_TA_TAG_Spec" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<%@ Register Src="PageNavigation.ascx" TagName="PageNavigation" TagPrefix="uc1" %>
<%@ Register Src="~/UC_ST_TA_TAG_Spec.ascx" TagName="UC_ST_TA_TAG_Spec" TagPrefix="uc" %>
 

<asp:Content ID="ContentHeader" runat="server" ContentPlaceHolderID="ContentPlaceHolderHead">
    <link rel="stylesheet" href="resources/css/StylePipe.css" type="text/css" media="all" /> 
    <link rel="stylesheet" href="resources/css/Dropdown_Popover.css" type="text/css" media="all" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UDPMain" runat="Server">
        <ContentTemplate>

            <!-- Page Head -->
            <h2>Tag Setting </h2>

            <div class="clear"></div>
            <!-- End .clear -->

            <div class="content-box">
                <!-- Start Content Box -->
                <!-- End .content-box-header -->
                <asp:Panel ID="pnlSearch" runat="server">
                    <div class="content-box-header" style="height: auto; padding-top: 5px; padding-bottom: 5px;">
                        <div>
                            <h3 style="margin-top: -5px; width: 120px;">Search TAG</h3>
                             <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                          ID="ddl_Search_Tag_Type" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
    				
    				<asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Route" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Area" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                            <asp:TextBox runat="server" ID="txt_search_Insulation_Thickness" AutoPostBack="True"
                                CssClass="text-input small-input " Width="100px" PlaceHolder="Ins Thickness"></asp:TextBox>

                        </div>
                         
                    </div>
                    <div class="content-box-content">
                        <div class="tab-content default-tab">
                            <asp:Panel ID="pnlListTag" runat="server">
                                <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                                    <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                                    <div>
                                        <asp:Label ID="lblBindingError" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>

                                <table>
                                    <thead>
                                        <tr>
                                            <th><a href="#">Tag-No </a></th>
                                            <th><a href="#">Tag Name </a></th>
                                            <th><a href="#">Equipement-Type</a></th>
                                            <th><a href="#">Status</a></th>
                                            <th><a href="#">Updated</a> </th>
                                            <th><a href="#">Action</a></th>
                                        </tr>
                                    </thead>

                                    <asp:Repeater ID="rptTag" runat="server">
                                        <HeaderTemplate>

                                            <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblTagNo" runat="server"></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblTagName" runat="server"></asp:Label>
                                                    <asp:Label ID="lblTo_Table" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTagType" runat="server"></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                                <td>
                                                    <asp:Label ID="lblUpdateTime" runat="server"></asp:Label></td>
                                                <td>
                                                    <!-- Icons -->
                                                    <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                                    <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />

                                                </td>
                                            </tr>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </FooterTemplate>
                                    </asp:Repeater>



                                    <tfoot>
                                        <tr>
                                            <td colspan="6">
                                                <div class="bulk-actions align-left">
                                                    <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                                                </div>
                                                <uc1:PageNavigation ID="Navigation" runat="server" />
                                                <!-- End .pagination -->
                                                <div class="clear"></div>
                                            </td>
                                    </tfoot>
                                </table>
                                <div class="clear"></div>
                                <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">
                                    <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                                    <div>
                                        <asp:Label ID="lblBindingSuccess" runat="server"></asp:Label>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>

                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pnlEdit" runat="server">

                    <!-- This is the target div. id must match the href of this div's tab -->
                    <div class="content-box-header">
                        <h3>
                            <asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Tag Info :
                            <asp:Label ID="lblTagCode" runat="server"></asp:Label></h3>
                       
                        <div class="clear"></div>
                    </div>

                    <asp:Panel ID="TabTag" runat="server">

                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 60%; text-align-last: start; vertical-align: top;">
                                    <uc:UC_ST_TA_TAG_Spec ID="Tag_Info" runat="server" />
                                </td>
                                <td style="width: 40%; text-align-last: start; vertical-align: top;">
                                    <div style="width: 100%; vertical-align: middle; line-height: 30px; padding-left: 10px; font-size: 14px; font-weight: bold;">
                                        Tag Overview
                                    </div>
                                    <!-------------- Tag Preview ---------------->
                                    <div class="File_Thumbnail" style="width: 350px; height: 350px;">
                                        <a class="File_Zoom_Mask" id="aZoomMaskMainPreview" runat="server" target="_blank" style="width: 350px; height: 330px;"></a>
                                        <asp:Panel ID="pnlMainPreview" runat="server" CssClass="File_Image" Width="350px" Height="350px" BackImageUrl="">
                                            <div class="File_Command" style="text-align-last: center; width: 100%;">
                                                <asp:ImageButton ID="btnEditMainPreview" runat="server" ImageUrl="resources/images/icons/edit_white_16.png" />
                                                <asp:ImageButton ID="btnDeleteMainPreview" runat="server" ImageUrl="resources/images/icons/delete_white_16.png" />
                                                <Ajax:ConfirmButtonExtender ID="cfmMainDeletePreview" runat="server" ConfirmText="Are you sure to delete tag overview?" BehaviorID="btnDeleteMainPreview" TargetControlID="btnDeleteMainPreview"></Ajax:ConfirmButtonExtender>
                                            </div>
                                        </asp:Panel>
                                        <div class="File_Title" id="divTitleMainPreview" runat="server">
                                            <asp:Label ID="lblTitleMainPreview" runat="server"></asp:Label>
                                        </div>
                                        <asp:Button ID="btnRefreshMainPreview" runat="server" Style="display: none;" CommandName="Refresh" />
                                        <asp:TextBox ID="txtMainPreviewID" runat="server" Style="display: none;"></asp:TextBox>
                                    </div>
                                    <!-------------- Tag Preview ---------------->
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>



                    <asp:Panel ID="pnlSaveTag" runat="server">

                        
                        <p style="width: 100%; margin-left: 20px; margin-top: 20px; float: left;">
                            <label class="column-left" style="width: 120px;">Available &nbsp;</label>
                            <asp:CheckBox ID="chkAvailable" runat="server" Text="" />
                        </p>
                        <asp:Panel ID="pnlValidation" runat="server" CssClass="notification error png_bg" Style="float: unset;">
                            <asp:ImageButton ID="btnValidationClose" runat="server" CssClass="close" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" />
                            <div>
                                <asp:Label ID="lblValidation" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                        <p align="right" style="padding-right: 20px; margin-top: 20px; margin-bottom: 20px;" style="float: right;">
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Back to see all tag" Style="margin-right: 10px;" />
                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                        </p>
                    </asp:Panel>





                </asp:Panel>
            </div>



        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
