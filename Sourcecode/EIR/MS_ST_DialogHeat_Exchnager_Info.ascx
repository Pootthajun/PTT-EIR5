﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MS_ST_DialogHeat_Exchnager_Info.ascx.vb" Inherits="EIR.MS_ST_DialogHeat_Exchnager_Info" %>
        <tr>
            <td class="propertyCaption">Initial Year </td>
            <td  style="border-right-style: none;">
                <asp:TextBox ID="txt_Initial_Year" runat="server" MaxLength="50" Font-Size="10" Style="text-align: left; margin-left: 7px;"></asp:TextBox>

            </td>
            <td colspan="2" style="border-left-style: none;"></td>
        </tr>
        <tr>
            <td class="propertyGroup" colspan="4" style="border: none; padding :0px;">

                        <table cellpadding="0" cellspacing="0" >
            <tbody>
                <%--Head Table--%>
                <tr>
                    <td class="propertyCaption" style="text-align:center;width: 140px;"><b>SPEC</b></td>
                    <td class="propertyCaption" style="text-align:center;width: 100px;"><b>SHELL SIDE</b></td>
                    <td class="propertyCaption" colspan="2" style="text-align:center;"><b style="text-align:center;">TUBE SIDE</b></td>
                    <td class="propertyCaption" style="text-align:center;width: 180px;"><b>REMARK</b></td>
                </tr>
                <%--Table Content--%>
              
        <%--Table Content--%>
        <tr>              
	        <td class="propertyCaption">Design Temp</td>	
	        <td><asp:TextBox ID="txt_SHELL_DESIGN_TEMP_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_DESIGN_TEMP_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">° F</td>
	        <td><asp:TextBox ID="txt_REMARK_DESIGN_TEMP" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Design Pressure</td>	
	        <td><asp:TextBox ID="txt_SHELL_DESIGN_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_DESIGN_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">Bar.g</td>
	        <td><asp:TextBox ID="txt_REMARK_DESIGN_PRESSURE" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Operate Temp</td>	
	        <td><asp:TextBox ID="txt_SHELL_OPERATING_TEMP_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_OPERATING_TEMP_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">° C</td>
	        <td><asp:TextBox ID="txt_REMARK_OPERATING_TEMP" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Operate Pressure</td>	
	        <td><asp:TextBox ID="txt_SHELL_OPERATING_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_OPERATING_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">Bar.g</td>
	        <td><asp:TextBox ID="txt_REMARK_OPERATING_PRESSURE" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Hydro. Test Pressure</td>	
	        <td><asp:TextBox ID="txt_SHELL_HYDRO_TEST_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_HYDRO_TEST_PRESSURE_MIN" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">Bar.g</td>
	        <td><asp:TextBox ID="txt_REMARK_HYDRO_TEST_PRESSURE" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">M.A.W.P</td>	
	        <td><asp:TextBox ID="txt_SHELL_MAWP" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_MAWP" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">Bar.g</td>
	        <td><asp:TextBox ID="txt_REMARK_MAWP" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Material</td>	
	        <td><asp:TextBox ID="txt_SHELL_MATERIAL" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td colspan="2"> <asp:TextBox ID="txt_TUBE_MATERIAL" runat="server" MaxLength="50"></asp:TextBox></td>            
	
	        <td><asp:TextBox ID="txt_REMARK_MATERIAL" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Nom.Thk</td>	
	        <td><asp:TextBox ID="txt_SHELL_NOM_THK" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_NOM_THK" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">mm</td>
	        <td><asp:TextBox ID="txt_REMARK_NOM_THK" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Corrosion Allow</td>	
	        <td><asp:TextBox ID="txt_SHELL_CORROSION_ALLOW" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_CORROSION_ALLOW" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">mm</td>
	        <td><asp:TextBox ID="txt_REMARK_CORROSION_ALLOW" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Min. All. Thk.</td>	
	        <td><asp:TextBox ID="txt_SHELL_MIN_ALL_THK" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td><asp:TextBox ID="txt_TUBE_MIN_ALL_THK" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td style="text-align:center;">mm</td>
	        <td><asp:TextBox ID="txt_REMARK_MIN_ALL_THK" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Size</td>	
	        <td><asp:TextBox ID="txt_SHELL_SIZE" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td colspan="2"> <asp:TextBox ID="txt_TUBE_SIZE" runat="server" MaxLength="50"></asp:TextBox></td>            
	
	        <td><asp:TextBox ID="txt_REMARK_SIZE" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>

        <tr>              
	        <td class="propertyCaption">Quantity</td>	
	        <td><asp:TextBox ID="txt_SHELL_QUANTITY" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td colspan="2"> <asp:TextBox ID="txt_TUBE_QUANTITY" runat="server" MaxLength="50"></asp:TextBox></td>            
	
	        <td><asp:TextBox ID="txt_REMARK_QUANTITY" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>
        <tr>              
	        <td class="propertyCaption">Fluid</td>	
	        <td><asp:TextBox ID="txt_SHELL_FLUID" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td colspan="2"> <asp:TextBox ID="txt_TUBE_FLUID" runat="server" MaxLength="50"></asp:TextBox></td>            
	
	        <td><asp:TextBox ID="txt_REMARK_FLUID" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>
        <tr>              
	        <td class="propertyCaption">NO. Passes</td>	
	        <td><asp:TextBox ID="txt_SHELL_NO_PASSES" runat="server" MaxLength="50"></asp:TextBox></td>            
	        <td colspan="2"> <asp:TextBox ID="txt_TUBE_NO_PASSES" runat="server" MaxLength="50"></asp:TextBox></td>            
	
	        <td><asp:TextBox ID="txt_REMARK_NO_PASSES" runat="server" MaxLength="50"></asp:TextBox></td>
        </tr>


            </tbody>
        </table>
                </td>
            </tr>