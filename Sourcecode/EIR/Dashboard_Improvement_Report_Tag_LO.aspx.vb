﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Dashboard_Improvement_Report_Tag_LO
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL
    Dim Dashboard As New DashboardClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            '--------------Check Initialize Report--------------
            Dim PLANT_ID As Integer = Request.QueryString("PLANT_ID")
            Dim Month As Integer = Request.QueryString("MM")
            Dim Year As Integer = Request.QueryString("YY")
            Dim Improve As Integer = Request.QueryString("Improve")

            Dim Month_F As Integer = Request.QueryString("Month_F")
            Dim Month_T As Integer = Request.QueryString("Month_T")
            Dim Year_F As Integer = Request.QueryString("Year_F")
            Dim Year_T As Integer = Request.QueryString("Year_T")

            If Year > 2000 Then
                Year = Year - 543
            End If
            Dim Eqm As Integer = Request.QueryString("Eqm")

            lblBack.PostBackUrl = "Dashboard_Improvement_Report_Plant.aspx?PLANT_ID=" & PLANT_ID & "&MONTH_F=" & Month_F & "&MONTH_T=" & Month_T & "&YEAR_F=" & Year_F & "&YEAR_T=" & Year_T & "&EQUIPMENT=" & Eqm

            Dim Sql As String = ""
            Dim PlantName As String = ""
            Sql = "SELECT PLANT_NAME FROM MS_Plant WHERE PLANT_ID =" & PLANT_ID
            Dim DA As New SqlDataAdapter(Sql, BL.ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            PlantName = DT.Rows(0).Item("PLANT_NAME").ToString

            Sql = ""
            Sql &= "SELECT RPT_Year,RPT_No,LO_TAG_ID TAG_ID,LO_TAG_NO TAG_CODE,INSPECTED_DATE,PLANT_ID," & vbNewLine
            Sql &= "TAN_ISSUE,OX_ISSUE,WATER_ISSUE,VANISH_ISSUE" & vbNewLine
            Sql &= "FROM VW_DASHBOARD_LO" & vbNewLine
            Sql &= "WHERE (TAN_ISSUE = 3 OR OX_ISSUE = 3 OR WATER_ISSUE = 3 OR VANISH_ISSUE = 3) AND" & vbNewLine
            Sql &= "MONTH(INSPECTED_DATE) = " & Month & " AND YEAR(INSPECTED_DATE) = " & Year & " AND" & vbNewLine
            Sql &= "PLANT_ID = " & PLANT_ID & vbNewLine
            Sql &= "ORDER BY TAG_CODE" & vbNewLine
            DA = New SqlDataAdapter(Sql, BL.ConnStr)
            DT = New DataTable
            DA.Fill(DT)

            lblHead.Text = "Found " & Improve & " Lube Oil Equipement Problem(S) Improved Completely <br> At " & PlantName & " on " & Dashboard.FindMonthNameEng(Month) & " " & Year

            rptData.DataSource = DT
            rptData.DataBind()

        End If

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim tbTag As HtmlTableRow = e.Item.FindControl("tbTag")
        Dim lblTag As Label = e.Item.FindControl("lblTag")
        Dim lblTAN As Label = e.Item.FindControl("lblTAN")
        Dim lblOx As Label = e.Item.FindControl("lblOx")
        Dim lblVarnish As Label = e.Item.FindControl("lblVarnish")
        Dim lblWater As Label = e.Item.FindControl("lblWater")

        lblTag.Text = e.Item.DataItem("TAG_CODE")

        If e.Item.DataItem("TAN_ISSUE").ToString = "3" Then
            lblTAN.Text = "Fix"
        End If
        If e.Item.DataItem("OX_ISSUE").ToString = "3" Then
            lblOx.Text = "Fix"
        End If
        If e.Item.DataItem("WATER_ISSUE").ToString = "3" Then
            lblWater.Text = "Fix"
        End If
        If e.Item.DataItem("VANISH_ISSUE").ToString = "3" Then
            lblVarnish.Text = "Fix"
        End If
        tbTag.Attributes.Add("onClick", "window.open('Dashboard_Current_Status_Tag_LO.aspx?RPT_Year=" & e.Item.DataItem("RPT_Year") & "&RPT_No=" & e.Item.DataItem("RPT_No") & "&Tag_ID=" & e.Item.DataItem("Tag_ID") & "','Dialog_Tag_LO_" & e.Item.DataItem("TAG_ID") & "',' scrollbars,resizable,center');")
    End Sub

End Class