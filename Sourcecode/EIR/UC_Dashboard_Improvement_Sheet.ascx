﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UC_Dashboard_Improvement_Sheet.ascx.vb" Inherits="EIR.UC_Dashboard_Improvement_Sheet" %>

<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<table cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef; width:100%;">
    <tr>
        <td style="vertical-align:top; width:280px;">
            <asp:Chart ID="ChartMain_Y" runat="server" Width="280px" Height="400px" CssClass="ChartHighligh"
                RightToLeft="Inherit"  >
                <Series>
                    <asp:Series ChartArea="ChartArea1" Color="Peru" LegendText="Improvement" 
                        Name="Series1">
                    </asp:Series>
                </Series>
                <titles>
                   <asp:Title Name="Title1">
                   </asp:Title>
                </titles>
   
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
        </td> 
        <td style="vertical-align:top; width:600px;">
            <div>
                <asp:Chart ID="ChartMain_M" runat="server" Height="400px" RightToLeft="Inherit" CssClass="ChartHighligh"
                  Width="600px" BorderlineColor="" EnableTheming="True">
                  <borderskin backcolor="" bordercolor="" pagecolor="" />
                  <Series>
                      <asp:Series ChartArea="ChartArea1" ChartType="Column" Color="DodgerBlue" 
                          Legend="Legend1" LegendText="Monthly Improvement" Name="Series1">
                      </asp:Series>
                      <asp:Series ChartArea="ChartArea1" ChartType="Line" Color="Green" 
                          Legend="Legend1" LegendText="Sum of Improvement" Name="Series2" BorderWidth="2">
                    </asp:Series>
                    </Series>
                    <titles>
                        <asp:Title Name="Title1">
                        </asp:Title>
                    </titles>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1">
                          <axisy intervalautomode="VariableCount">
                          </axisy>
                          <axisx intervalautomode="VariableCount">
                          </axisx>
                          <axisx2 intervalautomode="VariableCount">
                          </axisx2>
                          <axisy2 intervalautomode="VariableCount">
                          </axisy2>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Image ID="Image1" runat="server" 
                ImageUrl="~/resources/images/LegendImprovementSheet.png" />
            </div>
        </td>
        <td style="vertical-align:top; width:100%;">
            <center>
                <div style="width: 180px">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:160px;">
                        <tr>
                            <td style="text-align:center; font-size:12px; padding-bottom:5px; padding-top:5px; font-family:Arial; background-color: #000066; border-bottom:1px solid #003366;">
                                <asp:Label ID="lbl_Y" runat="server" Text="-" ForeColor="White" ></asp:Label>
                            </td>
                        </tr>
                    </table> 
                    <table border="0" cellpadding="0" cellspacing="0" style="border:1px solid #efefef; width:160px;">
                        <tr>
                            <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px;  border-bottom:1px solid #003366;">
                                Year</td>
                            <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px;  border-bottom:1px solid #003366;">
                               Issues Improved</td>
                        </tr>
                        <asp:Repeater ID="rptData_Y" runat="server">
                        <ItemTemplate>
                            <tr style="border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                                </td>
                                <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                    <asp:Label ID="lblImprovement" runat="server" ForeColor = "Peru"></asp:Label>
                                </td>
                             </tr>
                        </ItemTemplate>
                      </asp:Repeater>
                    </table>
                </div> 
                <br />
                <div style="width: 180px">
                    <table border="0" cellpadding="0" cellspacing="0" style="width:160px;">
                        <tr>
                            <td style="text-align:center; font-size:12px; padding-bottom:5px; padding-top:5px; font-family:Arial; background-color: #000066; border-bottom:1px solid #003366;">
                                <asp:Label ID="lbl_M" runat="server" Text="-" ForeColor="White" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0" style="border:1px solid #efefef; width:160px;">
                        <tr>
                            <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px;  border-bottom:1px solid #003366;">
                              Month</td>
                            <td style="text-align:center; background-color:#003366; color:White; font-size:12px; font-family:Arial; padding-bottom:5px; padding-top:5px;  border-bottom:1px solid #003366;">
                              Issues Improved</td>
                        </tr>
                        <asp:Repeater ID="rptData_M" runat="server">
                            <ItemTemplate>
                                <tr style="border-bottom:solid 1px #efefef" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                    <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                                     </td>
                                    <td style="text-align:center; font-size:12px; font-family:Arial; border-bottom:#efefef 1px solid; padding-bottom:5px; padding-top:5px;">
                                        <asp:Label ID="lblImprovement" runat="server" ForeColor = "DodgerBlue"></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </center>
        </td>
    </tr>
</table>