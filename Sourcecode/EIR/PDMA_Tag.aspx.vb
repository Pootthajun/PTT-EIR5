﻿Imports System.Data
Imports System.Data.SqlClient
Public Class PDMA_Tag
    Inherits System.Web.UI.Page

    Dim BL As New EIR_BL

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("USER_Full_Name") = "" Or IsNothing(Session("USER_ID")) Or IsNothing(Session("USER_LEVEL")) Then
            Response.Redirect("Login.aspx", True)
        End If

        If Not IsPostBack Then
            ResetTag(Nothing, Nothing)
            ClearPanelSearch()
            ImplementJavaIntegerText(txtTagVoltage, False,, "Left")
            ImplementJavaIntegerText(txtTagFrequency, False,, "Left")
            ImplementJavaMoneyText(txtTagCurrent,, "Left")
            ImplementJavaMoneyText(txtTagPowerFactor,, "Left")
            ImplementJavaMoneyText(txtTagKW,, "Left")
            ImplementJavaIntegerText(txtTagRPM, False,, "Left")
        End If

        HideValidator()
    End Sub

#Region "Hide Validator"
    Private Sub HideValidator()
        pnlValidation.Visible = False
        pnlBindingError.Visible = False
        pnlBindingSuccess.Visible = False
    End Sub

    Protected Sub btnValidationClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnValidationClose.Click
        pnlValidation.Visible = False
    End Sub

    Protected Sub btnBindingErrorClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingErrorClose.Click
        pnlBindingError.Visible = False
    End Sub

    Protected Sub btnBindingSuccessClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBindingSuccessClose.Click
        pnlBindingSuccess.Visible = False
    End Sub
#End Region

    Private Sub BindTag()

        Dim SQL As String = "SELECT MS_PLANT.PLANT_Code,MS_PDMA_ROUTE.ROUTE_Code,TAG_ID,TAG_Code TAG_No,TAG_Name,MS_PDMA_TAG.Active_Status,MS_PDMA_TAG.Update_Time,TAG_Voltage,PLANT_Code,ROUTE_Code" & vbNewLine
        SQL &= "   FROM MS_PDMA_TAG" & vbNewLine
        SQL &= "   LEFT JOIN MS_PDMA_ROUTE ON MS_PDMA_TAG.ROUTE_ID=MS_PDMA_ROUTE.ROUTE_ID" & vbNewLine
        SQL &= "   LEFT JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_PDMA_ROUTE.PLANT_ID" & vbNewLine
        Dim WHERE As String = ""

        If ddl_Search_Plant.SelectedIndex > 0 Then
            WHERE &= " MS_PLANT.PLANT_ID=" & ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value & " AND "
        End If
        If ddl_Search_Route.SelectedIndex > 0 Then
            WHERE &= " MS_PDMA_ROUTE.ROUTE_ID=" & ddl_Search_Route.Items(ddl_Search_Route.SelectedIndex).Value & " AND "
        End If

        If WHERE <> "" Then
            SQL &= " WHERE " & WHERE.Substring(0, WHERE.Length - 4) & vbNewLine
        End If

        SQL &= "ORDER BY TAG_Code" & vbNewLine

        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable

        Try
            DA.Fill(DT)
        Catch ex As Exception
            pnlBindingError.Visible = True
            lblBindingError.Text = "Invalid parameter"
            Exit Sub
        End Try

        Session("MS_PDMA_TAG") = DT

        Navigation.SesssionSourceName = "MS_PDMA_TAG"
        Navigation.RenderLayout()

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptTag
    End Sub

    Protected Sub rptTag_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTag.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblPlant As Label = e.Item.FindControl("lblPlant")
        Dim lblRoute As Label = e.Item.FindControl("lblRoute")
        Dim lblTagNo As Label = e.Item.FindControl("lblTagNo")
        Dim lblTagName As Label = e.Item.FindControl("lblTagName")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblUpdateTime As Label = e.Item.FindControl("lblUpdateTime")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnToggle As ImageButton = e.Item.FindControl("btnToggle")

        lblPlant.Text = e.Item.DataItem("PLANT_Code")
        lblRoute.Text = e.Item.DataItem("ROUTE_Code")
        lblTagNo.Text = e.Item.DataItem("TAG_No")
        lblTagName.Text = e.Item.DataItem("TAG_Name")

        If Not IsDBNull(e.Item.DataItem("Active_Status")) AndAlso e.Item.DataItem("Active_Status") Then
            lblStatus.Text = "Available"
            lblStatus.ForeColor = Drawing.Color.Green
            btnToggle.ImageUrl = "resources/images/icons/tick.png"
        Else
            lblStatus.Text = "Unavailable"
            lblStatus.ForeColor = Drawing.Color.OrangeRed
            btnToggle.ImageUrl = "resources/images/icons/cross.png"
        End If

        lblUpdateTime.Text = BL.ReportGridTime(e.Item.DataItem("Update_Time"))

        btnEdit.Attributes("TAG_ID") = e.Item.DataItem("TAG_ID")

        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        btnDelete.Visible = Session("USER_LEVEL") = EIR_BL.User_Level.Administrator
    End Sub

    Protected Sub rptTag_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptTag.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim TAG_ID As Integer = btnEdit.Attributes("TAG_ID")

        Select Case e.CommandName
            Case "Edit"

                ClearPanelEdit()
                btnCreate.Visible = False
                pnlEdit.Visible = True
                lblUpdateMode.Text = "Update"

                '------------------------------------
                pnlListTag.Enabled = False

                '--------------Bind Value------------
                Dim SQL As String = "SELECT MS_PDMA_TAG.* ,MS_PLANT.PLANT_ID" & vbNewLine
                SQL &= " FROM MS_PDMA_TAG LEFT JOIN MS_PDMA_Route ON MS_PDMA_TAG.ROUTE_ID=MS_PDMA_Route.ROUTE_ID" & vbNewLine
                SQL &= " LEFT JOIN MS_PLANT ON MS_PLANT.PLANT_ID=MS_PDMA_Route.PLANT_ID" & vbNewLine
                SQL &= " WHERE MS_PDMA_TAG.TAG_ID=" & TAG_ID
                Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    lblBindingError.Text = "TAG Not Found"
                    pnlBindingError.Visible = True
                    BindTag()
                    Exit Sub
                End If

                Dim PLANT_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("PLANT_ID")) Then PLANT_ID = DT.Rows(0).Item("PLANT_ID")
                Dim ROUTE_ID As Integer = 0 : If Not IsDBNull(DT.Rows(0).Item("ROUTE_ID")) Then ROUTE_ID = DT.Rows(0).Item("ROUTE_ID")

                BL.BindDDlPlant(ddl_Edit_Plant, PLANT_ID)
                BL.BindDDl_PDMA_Route(PLANT_ID, ddl_Edit_Route, ROUTE_ID)
                txtTagCode.Text = DT.Rows(0).Item("TAG_CODE").ToString
                txtTagCode.Attributes("TagID") = TAG_ID
                txtTagName.Text = DT.Rows(0).Item("TAG_NAME").ToString
                txtTagBrand.Text = DT.Rows(0).Item("TAG_Brand").ToString
                txtTagVoltage.Text = DT.Rows(0).Item("TAG_Voltage").ToString
                txtTagFrequency.Text = DT.Rows(0).Item("TAG_Frequency").ToString
                txtTagType.Text = DT.Rows(0).Item("TAG_Type").ToString
                If DT.Rows(0).Item("TAG_Current").ToString <> "" Then
                    txtTagCurrent.Text = FormatNumber(DT.Rows(0).Item("TAG_Current"))
                Else
                    txtTagCurrent.Text = ""
                End If
                If DT.Rows(0).Item("TAG_Power_Factor").ToString <> "" Then
                    txtTagPowerFactor.Text = FormatNumber(DT.Rows(0).Item("TAG_Power_Factor"))
                Else
                    txtTagPowerFactor.Text = ""
                End If
                If DT.Rows(0).Item("TAG_KW").ToString <> "" Then
                    txtTagKW.Text = FormatNumber(DT.Rows(0).Item("TAG_KW"))
                Else
                    txtTagKW.Text = ""
                End If

                txtTagRPM.Text = DT.Rows(0).Item("TAG_RPM").ToString
                txtTagInsulationClass.Text = DT.Rows(0).Item("TAG_Insulation_Class").ToString
                txtDesc.Text = DT.Rows(0).Item("TAG_Description").ToString
                chkAvailable.Checked = DT.Rows(0).Item("Active_Status")

                btnSave.Focus()

            Case "ToggleStatus"

                Dim SQL As String = "UPDATE MS_PDMA_TAG Set active_status=CASE active_status WHEN 1 THEN 0 ELSE 1 END" & vbNewLine
                SQL &= " WHERE  TAG_ID=" & TAG_ID
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTag()

                lblBindingSuccess.Text = "Change status successfully"
                pnlBindingSuccess.Visible = True
            Case "Delete"
                Dim SQL As String = ""
                SQL &= "DELETE FROM RPT_PDMA_Detail WHERE  TAG_ID=" & TAG_ID & vbNewLine
                SQL &= "DELETE FROM MS_PDMA_TAG WHERE  TAG_ID=" & TAG_ID & vbNewLine
                Dim Command As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnStr)
                Try
                    Conn.Open()
                    With Command
                        .Connection = Conn
                        .CommandType = CommandType.Text
                        .CommandText = SQL
                        .ExecuteNonQuery()
                        .Dispose()
                    End With
                    Conn.Close()
                    Conn.Dispose()
                Catch ex As Exception
                    lblBindingError.Text = "Invalid parameter"
                    pnlBindingError.Visible = True
                    Exit Sub
                End Try
                BindTag()

                lblBindingSuccess.Text = "Delete successfully"
                pnlBindingSuccess.Visible = True
        End Select

    End Sub

    Protected Sub ResetTag(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        BindTag()
        '-----------------------------------
        ClearPanelEdit()
        '----------------Bind Panel Search-------------------

        pnlListTag.Enabled = True
    End Sub

    Private Sub ClearPanelEdit()
        pnlEdit.Visible = False
        lblUpdateMode.Text = ""

        BL.BindDDlPlant(ddl_Edit_Plant, False)
        BL.BindDDl_PDMA_Route(0, ddl_Edit_Route)
        txtTagCode.Text = ""
        txtTagCode.Attributes("TagID") = "0"
        txtTagName.Text = ""
        txtTagBrand.Text = ""
        txtTagVoltage.Text = ""
        txtTagFrequency.Text = ""
        txtTagType.Text = ""
        txtTagCurrent.Text = ""
        txtTagPowerFactor.Text = ""
        txtTagKW.Text = ""
        txtTagRPM.Text = ""
        txtTagInsulationClass.Text = ""
        txtDesc.Text = ""
        chkAvailable.Checked = True

        btnCreate.Visible = True
        pnlListTag.Enabled = True
        ddl_Search_Plant.Enabled = True
        ddl_Search_Route.Enabled = True
        '-------------InStall Javascript------------
        'PTClass.ImplementJavaIntegerText(txtTagNo)

    End Sub

#Region "Panel Search & DropDown"
    Private Sub ClearPanelSearch()
        BL.BindDDlPlant(ddl_Search_Plant, False)
        BL.BindDDl_PDMA_Route(0, ddl_Search_Route)
    End Sub

    Protected Sub ddl_Search_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Plant.SelectedIndexChanged
        BL.BindDDl_PDMA_Route(ddl_Search_Plant.Items(ddl_Search_Plant.SelectedIndex).Value, ddl_Search_Route)
        BindTag()
    End Sub

    Protected Sub ddl_Edit_Plant_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Edit_Plant.SelectedIndexChanged
        BL.BindDDl_PDMA_Route(ddl_Edit_Plant.Items(ddl_Edit_Plant.SelectedIndex).Value, ddl_Edit_Route)
        BindTag()
    End Sub

    Protected Sub ddl_Search_Route_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Route.SelectedIndexChanged
        BindTag()
    End Sub

#End Region

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click

        ClearPanelEdit()
        ddl_Edit_Plant.SelectedIndex = ddl_Search_Plant.SelectedIndex
        ddl_Edit_Plant_SelectedIndexChanged(Nothing, Nothing)
        ddl_Edit_Route.SelectedIndex = ddl_Search_Route.SelectedIndex
        pnlEdit.Visible = True
        btnCreate.Visible = False
        ddl_Search_Plant.Enabled = False
        ddl_Search_Route.Enabled = False
        txtTagCode.ReadOnly = False
        lblUpdateMode.Text = "Create"

        '-----------------------------------
        pnlListTag.Enabled = False
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ddl_Edit_Plant.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Plant"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If ddl_Edit_Route.SelectedIndex = 0 Then
            lblValidation.Text = "Please select Route"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtTagCode.Text = "" Then
            lblValidation.Text = "Please insert Tag number"
            pnlValidation.Visible = True
            Exit Sub
        End If

        If txtTagName.Text = "" Then
            lblValidation.Text = "Please insert Tag name"
            pnlValidation.Visible = True
            Exit Sub
        End If

        'If txtTagBrand.Text = "" Then
        '    lblValidation.Text = "Please insert Brand"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        If txtTagVoltage.Text = "" Then
            lblValidation.Text = "Please insert Voltage (V)"
            pnlValidation.Visible = True
            Exit Sub
        End If

        'If txtTagFrequency.Text = "" Then
        '    lblValidation.Text = "Please insert Frequency (Hz)"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        'If txtTagType.Text = "" Then
        '    lblValidation.Text = "Please insert Type"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        'If txtTagCurrent.Text = "" Then
        '    lblValidation.Text = "Please insert Current (A)"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        'If txtTagPowerFactor.Text = "" Then
        '    lblValidation.Text = "Please insert Power Factor"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        'If txtTagKW.Text = "" Then
        '    lblValidation.Text = "Please insert KW"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        'If txtTagRPM.Text = "" Then
        '    lblValidation.Text = "Please insert RPM"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        'If txtTagInsulationClass.Text = "" Then
        '    lblValidation.Text = "Please insert Insulation Class"
        '    pnlValidation.Visible = True
        '    Exit Sub
        'End If

        Dim TagID As Integer = txtTagCode.Attributes("TagID")

        Dim SQL As String = ""
        SQL = "SELECT * FROM MS_PDMA_TAG WHERE TAG_ID=" & TagID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            SQL = "SELECT * FROM MS_PDMA_TAG WHERE TAG_CODE = '" & txtTagCode.Text.Replace("'", "''") & "'"
            Dim DA_ As New SqlDataAdapter(SQL, BL.ConnStr)
            Dim DT_ As New DataTable
            DA_.Fill(DT_)
            If DT_.Rows.Count > 0 Then
                lblValidation.Text = "This tag is already existed"
                pnlValidation.Visible = True
                Exit Sub
            End If

            DR = DT.NewRow
            TagID = GetNewTagID()
            DR("TAG_ID") = TagID
        Else
            DR = DT.Rows(0)
        End If

        DR("TAG_Code") = txtTagCode.Text
        DR("ROUTE_ID") = ddl_Edit_Route.Items(ddl_Edit_Route.SelectedIndex).Value
        DR("TAG_Name") = txtTagName.Text
        DR("TAG_Brand") = txtTagBrand.Text
        If IsNumeric(txtTagVoltage.Text.Replace(",", "")) Then
            DR("TAG_Voltage") = txtTagVoltage.Text.Replace(",", "")
        Else
            DR("TAG_Voltage") = DBNull.Value
        End If
        If IsNumeric(txtTagFrequency.Text.Replace(",", "")) Then
            DR("TAG_Frequency") = txtTagFrequency.Text.Replace(",", "")
        Else
            DR("TAG_Frequency") = DBNull.Value
        End If
        DR("TAG_Type") = txtTagType.Text
        If IsNumeric(txtTagCurrent.Text.Replace(",", "")) Then
            DR("TAG_Current") = txtTagCurrent.Text.Replace(",", "")
        Else
            DR("TAG_Current") = DBNull.Value
        End If
        If IsNumeric(txtTagPowerFactor.Text.Replace(",", "")) Then
            DR("TAG_Power_Factor") = txtTagPowerFactor.Text.Replace(",", "")
        Else
            DR("TAG_Power_Factor") = DBNull.Value
        End If
        If IsNumeric(txtTagKW.Text.Replace(",", "")) Then
            DR("TAG_KW") = txtTagKW.Text.Replace(",", "")
        Else
            DR("TAG_KW") = DBNull.Value
        End If
        If IsNumeric(txtTagRPM.Text.Replace(",", "")) Then
            DR("TAG_RPM") = txtTagRPM.Text.Replace(",", "")
        Else
            DR("TAG_RPM") = DBNull.Value
        End If
        DR("TAG_Insulation_Class") = txtTagInsulationClass.Text
        DR("TAG_Description") = txtDesc.Text
        DR("TAG_Order") = TagID
        DR("Active_Status") = chkAvailable.Checked
        DR("Update_By") = Session("USER_ID")
        DR("Update_Time") = Now

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)

        Dim cmd As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            lblValidation.Text = "Invalid parameter"
            pnlValidation.Visible = True
            Exit Sub
        End Try

        ResetTag(Nothing, Nothing)

        lblBindingSuccess.Text = "Save successfully"
        pnlBindingSuccess.Visible = True

        '------------ Focus Last Edit -----------
        DT = Session("MS_PDMA_TAG")
        DT.DefaultView.RowFilter = "TAG_ID=" & TagID
        If DT.DefaultView.Count > 0 Then
            Dim RowIndex As Integer = DT.Rows.IndexOf(DT.DefaultView(0).Row)
            DT.DefaultView.RowFilter = ""
            Navigation.CurrentPage = Math.Ceiling((RowIndex + 1) / Navigation.PageSize)
        End If
        ClearPanelEdit()
    End Sub

    Private Function GetNewTagID() As Integer

        Dim SQL As String = "SELECT IsNull(MAX(TAG_ID),0)+1 FROM MS_PDMA_TAG "
        Dim DA As New SqlDataAdapter(SQL, BL.ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)

    End Function

End Class