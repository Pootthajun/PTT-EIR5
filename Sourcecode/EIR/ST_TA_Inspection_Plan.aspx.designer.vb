﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ST_TA_Inspection_Plan
    
    '''<summary>
    '''UDPMain control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UDPMain As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''ddl_Search_Year control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddl_Search_Year As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddl_Search_Plant control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddl_Search_Plant As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''pnlBindingError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlBindingError As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''btnBindingErrorClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBindingErrorClose As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''lblBindingError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBindingError As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlListPlan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlListPlan As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''rptPlan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptPlan As Global.System.Web.UI.WebControls.Repeater
    
    '''<summary>
    '''btnCreate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCreate As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''Navigation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Navigation As Global.EIR.PageNavigation
    
    '''<summary>
    '''pnlBindingSuccess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlBindingSuccess As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''btnBindingSuccessClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBindingSuccessClose As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''lblBindingSuccess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBindingSuccess As Global.System.Web.UI.WebControls.Label
End Class
