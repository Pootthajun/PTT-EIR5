﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="LO_Tag.aspx.vb" Inherits="EIR.LO_Tag" %>

<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UDPMain" runat="Server">
<ContentTemplate>
			
			<!-- Page Head -->
			<h2>Lube Oil Tag Setting </h2>
			
						
			<div class="clear"></div> <!-- End .clear -->
			
			<div class="content-box"><!-- Start Content Box -->
			  <!-- End .content-box-header -->
              <div class="content-box-header">
                <h3>Display condition </h3>
				
				    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                          ID="ddl_Search_LO_Type" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
    				<asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                      ID="ddl_Search_Plant" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                          ID="ddl_Search_TagType" runat="server" AutoPostBack="True">
                            <asp:ListItem Value="0">Choose Tag Type ...</asp:ListItem>
                            <asp:ListItem Value="1">Balance of Plant</asp:ListItem>
                            <asp:ListItem Value="2">Critical Machine</asp:ListItem>
                    </asp:DropDownList>
                    
                    <div class="clear"></div>
                  </div>
			  

             
              <div class="content-box-content">
                <div class="tab-content default-tab" id="tab1">
                  <!-- This is the target div. id must match the href of this div's tab -->
                   <asp:Panel ID="pnlBindingError" runat="server" CssClass="notification attention png_bg">
                      <asp:ImageButton ID="btnBindingErrorClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                      <div> <asp:Label ID="lblBindingError" runat="server"></asp:Label></div>
                  </asp:Panel>
                  <asp:Panel ID="pnlListTag" runat="server">
                  <table>
                    <thead>
                      <tr>
                        <th><a href="#">Plant</a></th>  
                        <th><a href="#">Tag No.</a></th>
                        <th><a href="#">Equipment Name </a></th>
                        <th><a href="#">Oil Type</a></th>
                        <th><a href="#">Tag Type</a></th>                                         
                        <th><a href="#">Status</a></th>
                        <th><a href="#">Action</a></th>
                      </tr>
                    </thead>
                   
                    <asp:Repeater ID="rptTag" runat="server">
                           <HeaderTemplate>
                           
                            <tbody>
                           </HeaderTemplate>
                           <ItemTemplate>
                                  <tr>
                                    <td><asp:Label ID="lblPlantName" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTagNo" runat="server"></asp:Label></td>
                                    <td><asp:Label ID="lblTagName" runat="server"></asp:Label></td>                                    
                                    <td><asp:Label ID="lblTagType" runat="server"></asp:Label></td> 
                                    <td><asp:Label ID="lblRoute" runat="server"></asp:Label></td>                                    
                                    <td><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                    <td><!-- Icons -->
                                          <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ImageUrl="resources/images/icons/pencil.png" />
                                          <asp:ImageButton ID="btnToggle" CommandName="ToggleStatus" runat="server" ImageUrl="resources/images/icons/cross.png" />
                                    </td>
                                  </tr>
                                  
                             </ItemTemplate>
                            <FooterTemplate>
                             </tbody>
                            </FooterTemplate>
                           </asp:Repeater>
                     <tfoot>
                      <tr>
                        <td colspan="8">
                        <div class="bulk-actions align-left">                             
                            <asp:LinkButton ID="btnCreate" runat="server" CssClass="button" Text="Create new"></asp:LinkButton>
                        </div>
                            <uc1:PageNavigation ID="Navigation" runat="server" />
                          <!-- End .pagination -->
                          <div class="clear"></div>  
                        </td>
                      </tr>
                    </tfoot>
                  </table>
				  
				     <div class="clear"></div>
				    </asp:Panel>
				    
				 <asp:Panel ID="pnlBindingSuccess" runat="server" CssClass="notification success png_bg">  
                      <asp:ImageButton ID="btnBindingSuccessClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
				      <div><asp:Label ID="lblBindingSuccess" runat="server"></asp:Label></div>
				 </asp:Panel>
			    
			    <asp:Panel ID="pnlEdit" runat="server">
                  <div class="content-box-header">
                    <h3><asp:Label ID="lblUpdateMode" runat="server"></asp:Label>&nbsp;Tag </h3>
                    <div class="clear"></div>
                  </div>

                    <fieldset>
                    <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
                    <p>&nbsp;</p>
                    
                      <p>
                      <label class="column-left" style="width:120px;" >Plant : </label>
                        <asp:DropDownList CssClass="select" style="position:relative; top:5px;" 
                             ID="ddl_Edit_Plant" runat="server">
                        </asp:DropDownList>
                    </p>
                    
                    <p>
                      <label class="column-left" style="width:120px;" >Tag No : </label>
                     	 <asp:TextBox runat="server" ID="txtTagNo" CssClass="text-input small-input " Width="200px" MaxLength="50"></asp:TextBox>
					</p>
                                    
				    <p>
                      <label class="column-left" style="width:120px;" >Eqipment Name : </label>
                      <asp:TextBox runat="server" ID="txtTagName" CssClass="text-input small-input " Width="400px" MaxLength="100"></asp:TextBox>
			          <!-- End .clear -->
                    </p>
                    
                     <p>
                       <label class="column-left" style="width:120px;" >Oil Type: </label>
                        <asp:DropDownList CssClass="select" ID="ddl_Edit_Type" runat="server">
                      </asp:DropDownList>
                     </p>
                     
                    <p>
                       <label class="column-left" style="width:120px;" >Tag Type: </label>
                        <asp:DropDownList CssClass="select" ID="ddl_Edit_TagType" runat="server">
                            <asp:ListItem Value="0">Choose Tag Type ...</asp:ListItem>
                            <asp:ListItem Value="1">Balance of Plant</asp:ListItem>
                            <asp:ListItem Value="2">Critical Machine</asp:ListItem>
                      </asp:DropDownList>
                     </p>                    
                    
					 <p>
				      <label style="width:300px;" >Description: </label>
				        <asp:TextBox runat="server" ID="txtDesc" TextMode="MultiLine" CssClass="text-input" Width="100%" Height="80px" MaxLength="500"></asp:TextBox>
					</p>
					<p>
                      <label class="column-left" style="width:120px;" >Available : </label>
                        &nbsp;<asp:CheckBox ID="chkAvailable" runat="server" Text="" />
				    </p>
				    <asp:Panel ID="pnlValidation" runat="server" class="notification error png_bg"> 
				        <asp:ImageButton ID="btnValidationClose" runat="server" ImageUrl="resources/images/icons/cross_grey_small.png" ToolTip="Close" CssClass="close" />
                        <div> <asp:Label ID="lblValidation" runat="server"></asp:Label> </div>
                    </asp:Panel>
                    <p align="right">
                      <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                      <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" />
                    </p>
                    </fieldset></asp:Panel>
                </div>
                <!-- End #tab1 -->
					
				
			  </div> <!-- End .content-box-content -->
				
		  </div>
		  
</ContentTemplate>
</asp:UpdatePanel>  
</asp:Content>
