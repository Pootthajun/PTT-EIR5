﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage.master" CodeBehind="Dashboard_Summary_Plant.aspx.vb" Inherits="EIR.Dashboard_Summary_Plant" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="System.Web.DataVisualization" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<%@ Register src="UC_Dashboard_Summary_Plant.ascx" tagname="UC_Dashboard_Summary_Plant" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UDP1" runat="server">
<ContentTemplate>


			<!-- Page Head -->
			<h2>Total Problem by month</h2>
        	<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border:1px solid #efefef;">
			  <tr>
				<td>          
                    <asp:LinkButton ID="lblBack" runat="server" Text="Back to see all plants"></asp:LinkButton>
                </td>
			  </tr>
			  <tr>
			    <td>
			        <h3>
                        <asp:Label ID="Label3" runat="server" ForeColor="#009933" Text="From"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddl_Month_F" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                        &nbsp;<asp:DropDownList ID="ddl_Year_F" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                        &nbsp;<asp:Label ID="Label4" runat="server" ForeColor="#009933" Text="To"></asp:Label>
                &nbsp;<asp:DropDownList ID="ddl_Month_T" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                        &nbsp;<asp:DropDownList ID="ddl_Year_T" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                        &nbsp;<asp:Label ID="Label2" runat="server" ForeColor="#009933" 
                            Text="Equipment Category"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddl_Equipment" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </h3>          
			    </td>
			  </tr>
			  <tr>
	                <td>
	    <asp:Panel ID="pnlDashboard" runat="server" >
	        <uc1:UC_Dashboard_Summary_Plant ID="UC_Dashboard_Summary_Plant1" runat="server" />
        </asp:Panel>
    </td>
		      </tr>
			</table>
            <div style="visibility: hidden">
                <asp:Label ID="lblDisplayText" runat="server" Text="Label"></asp:Label>
                <asp:Label ID="lblPlantID" runat="server" Text="Label"></asp:Label>
            </div>
            
             <h2 align="right">
                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="button" style="width:100px;" OnClick="btnExport_Click" />  
                <asp:TextBox ID="txtHTML" runat="server" style="display:none;" TextMode="MultiLine"></asp:TextBox>
                <div id="ScriptContainer" runat="server" style="display:none;"></div>
                 <h2>
                 </h2>
            </h2>
            
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
