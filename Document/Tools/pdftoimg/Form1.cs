﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

namespace pdftoimg
{
    
    public partial class Form1 : Form
    {
        PDFConvertor pdf;
         

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }

        private void btnBrowseIn_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                txtInput.Text = openFileDialog1.FileName;

        }

        private void btnBrowseOut_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1 .ShowDialog() == DialogResult.OK)
                txtOutPut.Text = folderBrowserDialog1.SelectedPath;

        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            ImageFormat imageFormat = new ImageFormat(Guid.Empty);
            switch (comboBox1.SelectedItem.ToString() )
            {
                case "Jpeg": imageFormat = ImageFormat.Jpeg; break;
                case "Bmp": imageFormat = ImageFormat.Bmp; break;
                case "Png": imageFormat = ImageFormat.Png; break;
                case "Gif": imageFormat = ImageFormat.Gif; break;
            }

            pdf = new PDFConvertor();
            pdf.ExportProgressChanging += new ProgressChangingEventHandler(p_ExportProgressChanging);
      
            progressBar1.Visible = true;
            int filescount= pdf.Convert(txtInput.Text, txtOutPut.Text, imageFormat);
            progressBar1.Visible = false ;
            progressBar1.Value = 0;
            this.Text = filescount + " Items Exported!";
            lblCurrentFileName.Text = "";

           System.Diagnostics.Process.Start(txtOutPut.Text);
           
        }

        void p_ExportProgressChanging(object sender, string e)
        {
            
            lblCurrentFileName.Text = " Extracting " + e.Substring(e.LastIndexOf("\\")) + " !";
            progressBar1.Maximum = pdf.pageCount;
            progressBar1.Value += 1;
            this.Text=lblCount.Text =string.Format("{0}/{1} Extracted!",progressBar1.Value,progressBar1.Maximum);

            lblCount.Update();
            lblCurrentFileName.Update();
        }

        

       
    }
}
