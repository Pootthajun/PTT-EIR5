USE [EIR]
GO
/****** Object:  UserDefinedFunction [dbo].[UDF_Calculate_Incomplete_Tag_Info_In_Report]    Script Date: 10/7/2015 5:21:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[UDF_Calculate_Incomplete_Tag_Info_In_Report]
(
@RPT_Year As INT,
@RPT_No As INT,
@Tag_ID As INT
)
RETURNS INT
AS
BEGIN


------------- Get Report Type ----------------
DECLARE @ReportType AS varchar(5)=
				(
					SELECT 'ST' FROM RPT_ST_Header WHERE RPT_Year=@RPT_Year AND RPT_No=@RPT_No
					UNION ALL
					SELECT 'RO' FROM RPT_RO_Header WHERE RPT_Year=@RPT_Year AND RPT_No=@RPT_No
				)


DECLARE @RESULT AS INT 


IF @ReportType='ST'
BEGIN
		SET @RESULT=(SELECT SUM(
						CASE WHEN EXC.INSP_ID IS NULL THEN --------- ต้อง Upload รูป
							CASE WHEN HIST.DETAIL_ID IS NOT NULL OR 
									RPT_ST_Detail.ICLS_ID IS NOT NULL OR 
									RPT_ST_Detail.STATUS_ID IS NOT NULL OR 
									ISNULL(RPT_ST_Detail.PROB_Detail,'')<>'' THEN
									CASE WHEN RPT_ST_Detail.ICLS_ID IS NULL OR 
										RPT_ST_Detail.STATUS_ID IS NULL OR 
										ISNULL(RPT_ST_Detail.PROB_Detail,'')='' OR
										(ISNULL(RPT_ST_Detail.PIC_Detail1,0)=0 AND ISNULL(RPT_ST_Detail.PIC_Detail2,0)=0) THEN
												1
									ELSE
												0
									END		
							ELSE 
								0
							END
						ELSE
							CASE WHEN HIST.DETAIL_ID IS NOT NULL OR 
									RPT_ST_Detail.ICLS_ID IS NOT NULL OR 
									RPT_ST_Detail.STATUS_ID IS NOT NULL OR 
									ISNULL(RPT_ST_Detail.PROB_Detail,'')<>'' THEN
									CASE WHEN RPT_ST_Detail.ICLS_ID IS NULL OR 
										RPT_ST_Detail.STATUS_ID IS NULL OR 
										ISNULL(RPT_ST_Detail.PROB_Detail,'')='' THEN
												1
									ELSE
												0
									END
							ELSE 
								0
							END
						END	)
					FROM RPT_ST_Detail 
					LEFT JOIN RPT_ST_Detail HIST ON RPT_ST_Detail.LAST_DETAIL_ID=HIST.DETAIL_ID			
					LEFT JOIN VW_Not_Require_Inspection_Picture EXC ON RPT_ST_Detail.INSP_ID=EXC.INSP_ID			
					WHERE RPT_ST_Detail.RPT_Year=@RPT_Year AND RPT_ST_Detail.RPT_No=@RPT_No AND RPT_ST_Detail.TAG_ID=@Tag_ID)
END
IF @ReportType='RO'
BEGIN
					SET @RESULT=(SELECT 
					SUM(
						 CASE WHEN EXC.INSP_ID IS NULL THEN --------- ต้อง Upload รูป
							CASE WHEN HIST.DETAIL_ID IS NOT NULL OR 
									RPT_RO_Detail.ICLS_ID IS NOT NULL OR 
									RPT_RO_Detail.STATUS_ID IS NOT NULL OR 
									ISNULL(RPT_RO_Detail.PROB_Detail,'')<>'' THEN
									CASE WHEN RPT_RO_Detail.ICLS_ID IS NULL OR 
										RPT_RO_Detail.STATUS_ID IS NULL OR 
										ISNULL(RPT_RO_Detail.PROB_Detail,'')='' OR
										(ISNULL(RPT_RO_Detail.PIC_Detail1,0)=0 AND ISNULL(RPT_RO_Detail.PIC_Detail2,0)=0) THEN
												1
									ELSE
												0
									END		
							ELSE 
								0
							END
						ELSE ------------ ไม่ต้อง Upload รูป --------------
							CASE WHEN RPT_RO_Detail.INSP_ID=11 THEN
								0						
							--WHEN RPT_RO_Detail.INSP_ID=12 THEN
							--	CASE WHEN HIST.DETAIL_ID IS NOT NULL AND (RPT_RO_Detail.STATUS_ID IS NULL OR RPT_RO_Detail.ICLS_ID IS NULL ) Then
							--	1
							--	WHEN RPT_RO_Detail.STATUS_ID IS NOT NULL AND RPT_RO_Detail.ICLS_ID IS NULL  THEN
							--	1
							--	WHEN RPT_RO_Detail.STATUS_ID IS NULL AND RPT_RO_Detail.ICLS_ID IS NOT NULL THEN
							--	1
							--	ELSE
							--	0
							--	END
							WHEN HIST.DETAIL_ID IS NOT NULL OR (RPT_RO_Detail.INSP_ID=12 AND RPT_RO_Detail.ZONE_ID IS NOT NULL) OR
									RPT_RO_Detail.ICLS_ID IS NOT NULL OR 
									RPT_RO_Detail.STATUS_ID IS NOT NULL OR 
									ISNULL(RPT_RO_Detail.PROB_Detail,'')<>'' THEN
									CASE WHEN RPT_RO_Detail.ICLS_ID IS NULL OR 
										RPT_RO_Detail.STATUS_ID IS NULL OR 
										ISNULL(RPT_RO_Detail.PROB_Detail,'')='' OR 
										(RPT_RO_Detail.INSP_ID = 12 And RPT_RO_Detail.ZONE_ID IS NULL)
										THEN
												1
									ELSE
												0
									END
							ELSE 
								0
							END
						END	
						) ISSUE
					FROM RPT_RO_Detail 
					LEFT JOIN RPT_RO_Detail HIST ON RPT_RO_Detail.LAST_DETAIL_ID=HIST.DETAIL_ID			
					LEFT JOIN VW_Not_Require_Inspection_Picture EXC ON RPT_RO_Detail.INSP_ID=EXC.INSP_ID			
					WHERE RPT_RO_Detail.RPT_Year=@RPT_Year AND RPT_RO_Detail.RPT_No=@RPT_No AND RPT_RO_Detail.TAG_ID=@Tag_ID)

END
	
	
	
RETURN @RESULT;
END
