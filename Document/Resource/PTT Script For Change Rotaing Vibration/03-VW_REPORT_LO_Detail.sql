﻿USE [EIR]
GO

/****** Object:  View [dbo].[VW_REPORT_LO_DETAIL]    Script Date: 10/6/2015 10:42:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[VW_REPORT_LO_DETAIL]
AS
SELECT dbo.UDF_RPT_Code(RPT_LO_Header.RPT_Year, RPT_LO_Header.RPT_No) AS RPT_CODE, Detail.DETAIL_ID, 
					  RPT_LO_Header.RPT_Year, RPT_LO_Header.RPT_No,RPT_LO_Header.RPT_Month, 
					  CAST(RPT_LO_Header.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(RPT_LO_Header.RPT_Month AS VARCHAR(2)),2) RPT_KEY,
					  MS_LO_TAG.LO_TAG_ID, MS_LO_TAG.LO_TAG_NO,MS_LO_TAG.LO_TAG_Name, 
                      ISNULL(Detail.OIL_TYPE_ID,MS_LO_TAG.Oil_TYPE_ID) Oil_TYPE_ID, 
                      ISNULL(Detail.OIL_TYPE_NAME,MS_LO_Oil_Type.Oil_TYPE_Name) Oil_TYPE_Name,
                      ISNULL(Detail.LO_TAG_TYPE,MS_LO_TAG.LO_TAG_TYPE) LO_TAG_TYPE,
                      CASE ISNULL(Detail.LO_TAG_TYPE,MS_LO_TAG.LO_TAG_TYPE) WHEN 1 THEN 'Balance of Plant' WHEN 2 THEN 'Critical Machine' ELSE '' END LO_TAG_TYPE_Name,
                      ISNULL(Detail.PLANT_ID,MS_LO_TAG.PLANT_ID) PLANT_ID,MS_Plant.PLANT_Name,MS_Plant.PLANT_Code,
                      ISNULL(Detail.Oil_Cat,MS_LO_Oil_Type.Oil_Cat) Oil_Cat,
                      --------------------value -------------------
                      Detail.TAN_Value,Detail.TAN_Date,
                      Detail.OX_Value,Detail.OX_Date,
                      Detail.VANISH_Value,Detail.VANISH_Date,                     
                      Detail.PART_COUNT_Value,Detail.PART_COUNT_Date,
                      CASE Detail.PART_COUNT_Value 
                      WHEN 1 THEN 'NAS 1'
                      WHEN 2 THEN 'NAS 2'
                      WHEN 3 THEN 'NAS 3'
                      WHEN 4 THEN 'NAS 4'
                      WHEN 5 THEN 'NAS 5'
                      WHEN 6 THEN 'NAS 6'
                      WHEN 7 THEN 'NAS 7'
                      WHEN 8 THEN 'NAS 8'
                      WHEN 9 THEN 'NAS 9'
                      WHEN 10 THEN 'NAS 10'
                      WHEN 11 THEN 'NAS 11'
                      WHEN 12 THEN 'NAS 12'
                      Else ''
                      END PART_COUNT_Display,
                      Detail.WATER_Value,Detail.WATER_Date,
                      
                      CASE WHEN Detail.TAN_Value>=1.5 THEN 1 ELSE 0 END TAN_Abnormal,
                      CASE Detail.Oil_Cat 
							WHEN 1 THEN ------ Mineral Oxidation -------
								CASE WHEN Detail.Ox_Value>=30 THEN 1 ELSE 0 END
							WHEN 2 THEN --- Synthetic Anti-Oxidation ---
								CASE WHEN Detail.Ox_Value<=30 THEN 1 ELSE 0 END
					  ELSE
							0
					  END OX_Abnormal, 
                      CASE WHEN Detail.VANISH_Value>30 THEN 1 ELSE 0 END VANISH_Abnormal,
                      CASE Detail.Oil_Cat 
							WHEN 1 THEN ------ Mineral Oxidation -------
								CASE WHEN Detail.Water_Value>=750 THEN 1 ELSE 0 END
							WHEN 2 THEN --- Synthetic Anti-Oxidation ---
								CASE WHEN Detail.Water_Value>=1500 THEN 1 ELSE 0 END
						ELSE
							0
						END WATER_Abnormal,
						
                      CASE WHEN Detail.TAN_Value>=1.5 OR 
								(Detail.Oil_Cat=1 AND Detail.Ox_Value>=30) OR  ---------- Oxidation Abnormal----------
								(Detail.Oil_Cat=2 AND Detail.Ox_Value<=30) OR 
								Detail.VANISH_Value>30 OR ---------- Varnish Abnormal----------
								(Detail.Oil_Cat=1 AND Detail.WATER_Value>=750) OR
								(Detail.Oil_Cat=2 AND Detail.WATER_Value>=1500)
					  THEN 1 ELSE 0 END Is_Abnormal                                            
                      ,Detail.Recomment
                      ,CASE Detail.LO_TAG_TYPE 
							  WHEN 1 THEN ------Balance of Plant
								   CASE WHEN RPT_LO_Header.RPT_Period_Type=2 ------Quaterly
										AND 
										(
											Detail.PART_COUNT_Value IS NULL OR 
											Detail.PART_COUNT_Date IS NULL OR
											Detail.VANISH_Value IS NULL OR 
											Detail.VANISH_Date IS NULL
										)
								   THEN 0
								   ELSE
										1
								   END
							  WHEN 2 THEN------Critical Machine
								   CASE WHEN RPT_LO_Header.RPT_Period_Type=1 ------Monthly
										AND 
										(
										 Detail.TAN_Value IS NULL OR
										 Detail.TAN_DATE IS NULL OR 
										 Detail.OX_Value IS NULL OR 
										 Detail.OX_Date IS NULL OR 
										 Detail.WATER_Value IS NULL OR 
										 Detail.WATER_Date IS NULL OR 
										 Detail.PART_COUNT_Value IS NULL OR 
										 Detail.PART_COUNT_Date IS NULL 
										)
									THEN 0
									WHEN RPT_LO_Header.RPT_Period_Type=2 ------Quaterly
										AND
										(
										 Detail.TAN_Value IS NULL OR
										 Detail.TAN_DATE IS NULL OR 
										 Detail.OX_Value IS NULL OR  -----------¡Ã³Õ¢Í§ Oxidation µéÍ§¶ÒÁ PTT ÍÕ¡·Õ------------
										 Detail.OX_Date IS NULL OR 
										 Detail.WATER_Value IS NULL OR 
										 Detail.WATER_Date IS NULL OR 
										 Detail.PART_COUNT_Value IS NULL OR 
										 Detail.PART_COUNT_Date IS NULL OR
										 Detail.VANISH_Value IS NULL OR
										 Detail.VANISH_Date IS NULL										 
										)
									THEN 0
									ELSE 1
									END
							  END IS_Complete
							  
							  ,HIST_TAN_ID
							  ,HIST_OX_ID
							  ,HIST_WATER_ID
							  ,HIST_VANISH_ID
							  ,HIST_PART_ID
							  ,Detail.Update_Time,RPT_LO_Header.RPT_Period_Start
                      
FROM RPT_LO_Header 
INNER JOIN RPT_LO_Detail Detail ON RPT_LO_Header.RPT_Year = Detail.RPT_Year 
							AND RPT_LO_Header.RPT_No = Detail.RPT_No 
LEFT JOIN MS_LO_TAG ON Detail.LO_TAG_ID = dbo.MS_LO_TAG.LO_TAG_ID 
LEFT JOIN MS_LO_Oil_Type ON MS_LO_TAG.Oil_TYPE_ID = MS_LO_Oil_Type.OIL_TYPE_ID 
INNER JOIN MS_Plant ON ISNULL(Detail.PLANT_ID,MS_LO_TAG.PLANT_ID)=MS_Plant.PLANT_ID

-------------------- TAN History ------------------------
--LEFT JOIN RPT_LO_Header Head_TAN ON CAST(RPT_LO_Header.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(RPT_LO_Header.RPT_Month AS VARCHAR(2)),2)>CAST(Head_TAN.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(Head_TAN.RPT_Month AS VARCHAR(2)),2)
--								AND Head_TAN.RPT_No>0
--LEFT JOIN RPT_LO_Detail HIST_TAN ON MS_LO_TAG.LO_TAG_ID=HIST_TAN.LO_TAG_ID 
--								AND HIST_TAN.RPT_Year=Head_TAN.RPT_Year AND HIST_TAN.RPT_No=Head_TAN.RPT_No
--								AND HIST_TAN.TAN_Value IS NOT NULL

-------------------- OX History -------------------------
--LEFT JOIN RPT_LO_Header Head_OX ON CAST(RPT_LO_Header.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(RPT_LO_Header.RPT_Month AS VARCHAR(2)),2)>CAST(Head_OX.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(Head_OX.RPT_Month AS VARCHAR(2)),2)
--								AND Head_OX.RPT_No>0								
--LEFT JOIN RPT_LO_Detail HIST_OX ON MS_LO_TAG.LO_TAG_ID=HIST_OX.LO_TAG_ID 
--								AND HIST_OX.RPT_Year=Head_OX.RPT_Year AND HIST_OX.RPT_No=Head_OX.RPT_No
--								AND HIST_OX.OX_Value IS NOT NULL
-------------------- WATER History -------------------------
--LEFT JOIN RPT_LO_Header Head_WATER ON CAST(RPT_LO_Header.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(RPT_LO_Header.RPT_Month AS VARCHAR(2)),2)>CAST(Head_WATER.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(Head_WATER.RPT_Month AS VARCHAR(2)),2)
--								AND Head_WATER.RPT_No>0		
--LEFT JOIN RPT_LO_Detail HIST_WATER ON MS_LO_TAG.LO_TAG_ID=HIST_WATER.LO_TAG_ID 
--								AND HIST_WATER.RPT_Year=Head_WATER.RPT_Year AND HIST_WATER.RPT_No=Head_WATER.RPT_No
--								AND HIST_WATER.WATER_Value IS NOT NULL

------------------ VARNISH History -------------------------
--LEFT JOIN RPT_LO_Header Head_VANISH ON CAST(RPT_LO_Header.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(RPT_LO_Header.RPT_Month AS VARCHAR(2)),2)>CAST(Head_VANISH.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(Head_VANISH.RPT_Month AS VARCHAR(2)),2)
--								AND Head_VANISH.RPT_No>0		
--LEFT JOIN RPT_LO_Detail HIST_VANISH ON MS_LO_TAG.LO_TAG_ID=HIST_VANISH.LO_TAG_ID 
--								AND HIST_VANISH.RPT_Year=Head_VANISH.RPT_Year AND HIST_VANISH.RPT_No=Head_VANISH.RPT_No
--								AND HIST_VANISH.VANISH_Value IS NOT NULL
------------------ PARTICLE COUNT History -------------------------
--LEFT JOIN RPT_LO_Header Head_PART ON CAST(RPT_LO_Header.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(RPT_LO_Header.RPT_Month AS VARCHAR(2)),2)>CAST(Head_PART.RPT_Year AS VARCHAR(4)) + RIGHT('0'+CAST(Head_PART.RPT_Month AS VARCHAR(2)),2)
--								AND Head_PART.RPT_No>0	
--LEFT JOIN RPT_LO_Detail HIST_PART ON MS_LO_TAG.LO_TAG_ID=HIST_PART.LO_TAG_ID 
--								AND HIST_PART.RPT_Year=Head_PART.RPT_Year AND HIST_PART.RPT_No=Head_PART.RPT_No
--								AND HIST_PART.PART_COUNT_Value IS NOT NULL
-----------------------------------------------------------------------------			

WHERE Detail.RPT_No>0

--GROUP BY 
--RPT_LO_Header.RPT_Year,RPT_LO_Header.RPT_Month, RPT_LO_Header.RPT_No, Detail.DETAIL_ID, RPT_LO_Header.RPT_Period_Type,
--RPT_LO_Header.RPT_Year, RPT_LO_Header.RPT_No, MS_LO_TAG.LO_TAG_ID, MS_LO_TAG.LO_TAG_NO,MS_LO_TAG.LO_TAG_Name,
--Detail.OIL_TYPE_ID,MS_LO_TAG.Oil_TYPE_ID,Detail.OIL_TYPE_NAME,MS_LO_Oil_Type.Oil_TYPE_Name,Detail.LO_TAG_TYPE,MS_LO_TAG.LO_TAG_TYPE,
--Detail.PLANT_ID,MS_LO_TAG.PLANT_ID,MS_Plant.PLANT_Name,MS_Plant.PLANT_Code,Detail.Oil_Cat,MS_LO_Oil_Type.Oil_Cat,Detail.Recomment,
--Detail.TAN_Value,Detail.TAN_Date,Detail.OX_Value,Detail.OX_Date,Detail.VANISH_Value,
--Detail.VANISH_Date,Detail.PART_COUNT_Value,Detail.PART_COUNT_Date,
--Detail.WATER_Value,Detail.WATER_Date,Detail.Update_Time,RPT_LO_Header.RPT_Period_Start


GO


