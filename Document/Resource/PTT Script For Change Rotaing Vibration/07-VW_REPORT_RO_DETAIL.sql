--USE [EIR]
--GO

/****** Object:  View [dbo].[VW_REPORT_RO_DETAIL]    Script Date: 10/7/2015 4:00:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[VW_REPORT_RO_DETAIL]
AS
SELECT     dbo.UDF_RPT_Code(dbo.RPT_RO_Header.RPT_Year, dbo.RPT_RO_Header.RPT_No) AS RPT_CODE, dbo.RPT_RO_Detail.DETAIL_ID, dbo.RPT_RO_Header.RPT_Year, 
                      dbo.RPT_RO_Header.RPT_No, dbo.MS_RO_TAG.TAG_ID, dbo.MS_Area.AREA_Code + '-' + dbo.MS_Process.PROC_Code + '-' + dbo.MS_RO_TAG.TAG_No AS TAG_Code,
                       dbo.MS_RO_TAG.TAG_Name, dbo.MS_RO_TAG.TAG_TYPE_ID, dbo.MS_RO_TAG_TYPE.TAG_TYPE_Name, MDI.INSP_ID, MDI.INSP_Name, 
                      dbo.RPT_RO_Detail.COMP_NO AS CURRENT_COMPONENT, MDS.STATUS_ID AS CURRENT_STATUS_ID, MDS.STATUS_Name AS CURRENT_STATUS_Name, 
                      dbo.ISPT_Class.ICLS_Level AS CURRENT_LEVEL,RPT_RO_Detail.ZONE_ID, dbo.ISPT_Class.ICLS_Description AS CURRENT_LEVEL_Desc, dbo.RPT_RO_Detail.Fixed AS CURRENT_Fixed, 
                      dbo.RPT_RO_Detail.PROB_Detail AS CURRENT_PROB_Detail, dbo.RPT_RO_Detail.PROB_Recomment AS CURRENT_PROB_Recomment, 
                      dbo.RPT_RO_Detail.PIC_Detail1 AS CURRENT_PICTURE1, dbo.RPT_RO_Detail.PIC_Detail2 AS CURRENT_PICTURE2, LST.RPT_Year AS LAST_RPT_YEAR, 
                      LST.RPT_No AS LAST_RPT_NO, LST.COMP_NO AS LAST_COMPONENT, LST_STATUS.STATUS_ID AS LAST_STATUS_ID, 
                      LST_STATUS.STATUS_Name AS LAST_STATUS_Name, LST_LEVEL.ICLS_Level AS LAST_LEVEL, LST_LEVEL.ICLS_Description AS LAST_LEVEL_Desc, 
                      LST.Fixed AS LAST_Fixed, LST.PROB_Detail AS LAST_PROB_Detail, LST.PROB_Recomment AS LAST_PROB_Recomment, LST.DETAIL_ID AS LAST_DETAIL_ID, 
                      LST.PIC_Detail1 AS LAST_PICTURE1, LST.PIC_Detail2 AS LAST_PICTURE2, REF.REF_INSP_ID, REF.REF_STATUS_ID, ISNULL(dbo.RPT_RO_Detail.Responsible, 
                      LST.Responsible) AS Responsible, dbo.RPT_RO_Detail.Create_Flag, 
					  CASE 
						WHEN isnull(LST_LEVEL.ICLS_Level, 0) = 0 AND isnull(ISPT_Class.ICLS_Level, 0) > 0 THEN 1 
						WHEN isnull(LST_LEVEL.ICLS_Level, 0) > 0 AND isnull(RPT_RO_Detail.Fixed, 0) = 0 THEN 2 
						WHEN isnull(LST_LEVEL.ICLS_Level, 0) > 0 AND ISPT_Class.ICLS_Level = 0 THEN 3 
						WHEN isnull(LST_LEVEL.ICLS_Level, 0) > 0 AND ISPT_Class.ICLS_Level > 0 THEN 4 
						ELSE 0 
					  END AS ISSUE,
                      CASE WHEN RPT_RO_Detail.PIC_Detail1 = 1 OR
                      RPT_RO_Detail.PIC_Detail2 = 1 OR
                      LST.PIC_Detail1 = 1 OR
                      LST.PIC_Detail2 = 1 THEN 1 ELSE 0 END AS HasPicture, CASE WHEN ISNULL(RPT_RO_Detail.PROB_Detail, '') 
                      = '' THEN LST.PROB_Detail ELSE ISNULL(RPT_RO_Detail.PROB_Detail, '') END AS PROBLEM_DETAIL, 
                      dbo.RPT_RO_Detail.PROB_Recomment AS PROBLEM_RECOMMENT, CASE WHEN ISNULL(ISPT_Class.ICLS_Level, '') 
                      = '' THEN LST_LEVEL.ICLS_Level ELSE ISNULL(ISPT_Class.ICLS_Level, '') END AS LEVEL_CODE, CASE WHEN ISNULL(ISPT_Class.ICLS_Description, '') 
                      = '' THEN LST_LEVEL.ICLS_Description ELSE ISNULL(ISPT_Class.ICLS_Description, '') END AS LEVEL_DESC, CASE WHEN ISNULL(RPT_RO_Detail.PIC_Detail1, 0) 
                      = 0 THEN CASE WHEN ISNULL(LST.PIC_Detail1, 0) = 0 THEN NULL ELSE CONVERT(image, '') END ELSE CONVERT(image, '') END AS PICTURE1, 
                      CASE WHEN ISNULL(RPT_RO_Detail.PIC_Detail2, 0) = 0 THEN CASE WHEN ISNULL(LST.PIC_Detail2, 0) = 0 THEN NULL ELSE CONVERT(image, '') 
                      END ELSE CONVERT(image, '') END AS PICTURE2
FROM         dbo.RPT_RO_Header INNER JOIN
                      dbo.RPT_RO_Detail ON dbo.RPT_RO_Header.RPT_Year = dbo.RPT_RO_Detail.RPT_Year AND 
                      dbo.RPT_RO_Header.RPT_No = dbo.RPT_RO_Detail.RPT_No LEFT OUTER JOIN
                      dbo.ISPT_Class ON dbo.RPT_RO_Detail.ICLS_ID = dbo.ISPT_Class.ICLS_ID INNER JOIN
                      dbo.MS_RO_TAG ON dbo.RPT_RO_Detail.TAG_ID = dbo.MS_RO_TAG.TAG_ID INNER JOIN
                      dbo.MS_RO_TAG_TYPE ON dbo.MS_RO_TAG.TAG_TYPE_ID = dbo.MS_RO_TAG_TYPE.TAG_TYPE_ID INNER JOIN
                      dbo.MS_Area ON dbo.MS_RO_TAG.AREA_ID = dbo.MS_Area.AREA_ID INNER JOIN
                      dbo.MS_Process ON dbo.MS_RO_TAG.PROC_ID = dbo.MS_Process.PROC_ID LEFT OUTER JOIN
                      dbo.MS_RO_Default_Inspection AS MDI ON dbo.RPT_RO_Detail.INSP_ID = MDI.INSP_ID LEFT OUTER JOIN
                      dbo.MS_RO_Default_Inspection_Status AS MDS ON dbo.RPT_RO_Detail.STATUS_ID = MDS.STATUS_ID LEFT OUTER JOIN
                      dbo.RPT_RO_Detail AS LST ON dbo.RPT_RO_Detail.LAST_DETAIL_ID = LST.DETAIL_ID LEFT OUTER JOIN
                      dbo.MS_RO_Default_Inspection_Status AS LST_STATUS ON LST.STATUS_ID = LST_STATUS.STATUS_ID LEFT OUTER JOIN
                      dbo.ISPT_Class AS LST_LEVEL ON LST.ICLS_ID = LST_LEVEL.ICLS_ID 
					  LEFT OUTER JOIN
                          (SELECT DISTINCT TAG_TYPE_ID, INSP_ID, REF_INSP_ID, REF_STATUS_ID
                            FROM          dbo.MS_RO_TAG_Inspection) AS REF ON dbo.MS_RO_TAG_TYPE.TAG_TYPE_ID = REF.TAG_TYPE_ID AND MDI.INSP_ID = REF.INSP_ID



GO


