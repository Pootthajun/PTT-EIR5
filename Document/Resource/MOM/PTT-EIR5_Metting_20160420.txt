# Off Route ให้สามารถใช้ Text Editor เพื่อเป็นหน้า Key in สำหรับบันทึกข้อมูลได้ เช่น ทำรูปภาพ สร้างเส้นลูกศร ฯลฯ
# Class A มีเฉพาะใน Stationary
	# Tempolary หลังซ่อมเสร็จ (backlog for Shut Down(SD) หรือ Turn Around (TA)) 
	# Normal

# ปัญหาชนิด Class A จะถูก Define เป็น Off Route ทั้งหมด
# ให้มีหน้าจอสำหรับค้นหาข้อมูลได้ เช่น ค้นหาจากเลข TAG เพื่อแสดงประวัติของการเกิดปัญหา
# ทุกรายงานให้แสดงเลข Notification (SAP)


#Piping เพิ่มอีก 1 Module คล้ายกับ Module Stationary
	# แสดงข้อมูล Pie ตามโรงแยก
	# ตั้งรอบการตรวจได้
	# การตั้งค่า Setting คล้ายกับ Stationary 

# การลงข้อมูล Pipe
	# ทาง PTT จะทำ List Profile ของท่อ ให้เป็นไฟล์ Excel เพื่อให้ Import เข้าระบบ
	# กรณีข้อมูลท่อเดียวกันแต่มีเส้นยาวพาดผ่าน 2 โรงแยก ให้ Define Code ตามต้นทาง

# Piping Inspection
	# Routine (ทุก 6 เดือน)  (Config)  การทำงานคล้ายกับ Stationary
	# Corrosion / Erosion (ท่อหุ้มฉนอน จะต้องมีการเปิด ฉนอนเพื่อวัดความหนา)
		# ความหนาจากการวัด
		# Corrosion Rate
		# Isometric แสดงตำแหน่งที่วัด
		# Remaining Life
		# Next Inspation รอบการตรวจครั้งถัดไป (กรอกวันที่เอง ไม่ต้องคำนวณ)
	# Off Route (Class A)   การทำงานคล้ายกับ Stationary
	
	# เครื่องมือวัดค่า Calibrate เรียกว่า Step Weight มี 2 ชนิดคือ Stanless Steal กับ Carbon Steal 
	
	# Mark ที่จะใช้วาด ตำแหน่งของรูปภาพ ให้ทำได้เหมือนกับ Off Route ประกอบไปด้วย วงกลม สี่เหลี่ยม เส้นประ เส้นทึบ และข้อความ
	
	# ข้อมูล Thickness มีตัวอย่างในไฟล์ Excel B001_3522-LFG-02017-2-D1101.xls
	# หน้าค้นหา ให้ค้นหาได้จาก ปีที่ระบุใน Next inspection year
	
	# remaining Life
		(T initial - T actual)/Time
	